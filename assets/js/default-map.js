var markers = [];
var primary_marker = [];
var map;
var enablemarker;
var polygons = [];
//
// init map
//
function initMap(id, city, allowmarker = true) {
  dom = id;
  enablemarker = allowmarker;

  map = new google.maps.Map(dom.find('#map')[0], {
    zoom: 12,
    mapTypeControl: true,
    zoomControlOptions: {position: google.maps.ControlPosition.TOP_LEFT},
    streetViewControl: false
  });

  google.maps.event.addListenerOnce(map, 'idle', function() {
    google.maps.event.trigger(map, 'resize');
    map.setCenter(city);
  });

  addSearchBox(map);

  return map;
};

function addMarker(location, name, primary) {
  var marker = new google.maps.Marker({
    position: location,
    map: map,
    optimized: false,
    draggable:true
  });

  if(!primary) {
    markers.push(marker);

  } else {
    primary_marker.push(marker);

  }

  map.panTo(marker.getPosition());
  map.setZoom(15);

  attachInfoWindowToMarker(marker, name);

}

//
// marker
//
function setMaponAll(){
  $.each(markers, function(index, value) {
    value.setMap(map);
  });
}

function clearMarkers(){
  $.each(markers, function(index, value) {
    value.setMap(null);
  });
}

function showMarker(){
  setMaponAll();
}

function deleteMarker(){
  clearMarkers();
  markers = [];
}

//
// indo window
//
function attachInfoWindowToMarker(marker, desc){

  var infowindow = new google.maps.InfoWindow({
    content:  desc +
    '<br> Longitude :' + marker.getPosition().lng() +
    '<br> Latitude :' + marker.getPosition().lat()
  });

  infowindow.open(marker.get('map'), marker);
}

//
// Create the search box and link it to the UI element.
//
function addSearchBox() {

  if(dom.find('#map')[0] != 'null' || dom.find('#map')[0] != 'undifined') {
    dom.find('#map').parent().append('<input id="pac-input" class="controls form-control" type="text" placeholder="Search Box" style="width:400px;margin-top:10px;">');
  }

  var input = dom.find('#pac-input')[0];
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  searchBoxListener(searchBox);
}

function searchBoxListener(searchBox){
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    } else if(places.length > 1) {
      sweetAlert("Gagal", "Pilih salah satu dari daftar pencarian", "error");
      return;
    }

    deleteMarker();

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      if (!place.geometry) {
        sweetAlert("Gagal","Gemotry tidak ditemukan", "error");
        return;
      }
      if(enablemarker) {
        addMarker(place.geometry.location, 'Tempat Baru');
      }
      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });
}

//
// overlay
//
function drawPolygonOverlay(coordinates, color, intensity){

  var kelurahan = new google.maps.Polygon({
    paths: coordinates,
    strokeColor: '#2c3e50',
    strokeOpacity: 1,
    strokeWeight: 0.7,
    fillColor: color,
    fillOpacity: intensity
  });
  polygons.push(kelurahan);
  kelurahan.setMap(map);
  if(enablemarker){
    polygonOverlayListener(kelurahan);
  }
  return kelurahan;
}

function polygonOverlayListener(polygon){
  google.maps.event.addListener(polygon, 'click', function (e) {

    dom.find('#latitude').val(e.latLng.lat());
    dom.find('#longitude').val(e.latLng.lng());
    deleteMarker();
    addMarker(e.latLng, 'Tempat Baru');
  });
}

function deletePolygonOverlay() {
  polygons.forEach(function (kelurahan) {
    kelurahan.setMap(null);
  })
}

function addMapControl(position, dom){
  map.controls[google.maps.ControlPosition[position]].push(dom);
}
