<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

 <link href="<?=base_url("/AdminLTE2/bower_components/bootstrap/dist/css/bootstrap.min.css")?>" rel="stylesheet" >
  <!-- Font Awesome -->
  <link  href="<?=base_url("/AdminLTE2/bower_components/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet">
  <!-- Ionicons -->
  <link  href="<?=base_url("/AdminLTE2/bower_components/Ionicons/css/ionicons.min.css")?>" rel="stylesheet">
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> -->
  
     <link  href="<?=base_url("/AdminLTE2/dist/css/AdminLTE.min.css")?>" rel="stylesheet">
    


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>

<body>


<script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>



<div class="col-md-12" id="chart_opening">
  <CENTER><h1 id="header_page_1">TARGET DAN REALISASI PAJAK DAERAH KOTA MALANG</h1></CENTER>
  <br><br>

    <center>
        <div class="col-md-6">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">TABEL TARGET DAN REALISASI PAJAK DAERAH KOTA MALANG</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped table-bordered" id="chart-table">
                      <thead id="t_head">
                        
                      </thead>
                      <tbody id="t_body">
                        
                      </tbody>
                        
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title" id="grafik_detail">GRAFIK PERBANDINGAN REALISASI DAN TARGET PAJAK __</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_jenis" style="height: 500px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            <!-- /.box-body -->
    </center>
</div>

<div class="col-md-12" id="chart_opening">
    <center>
        <div class="col-md-12">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">GRAFIK STATUS AKREDITASI SEKOLAH DI KOTA MALANG BERDASARKAN ZONA</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_all" style="height: 1050px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </center>
</div>

<!-- <input type="radio" name="ok" checked=""> -->

<img class="background-ncc-logo-full" src="<?php echo base_url("assets/images/ncc-logo-grayscale.png "); ?>" alt="NCC logo">



<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/datatables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/url.min.js") ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>

<!-- CHART -->
<script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>

<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>

<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

<script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>

<!-- /CHART -->

<!-- jQuery 3 -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery/dist/jquery.min.js")?>"> </script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/bootstrap/dist/js/bootstrap.min.js")?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/fastclick/lib/fastclick.js")?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/adminlte.min.js")?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js")?>"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url("/AdminLTE2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")?>"></script>
<script src="<?php echo base_url("/AdminLTE2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js")?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/chart.js/Chart.js")?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/pages/dashboard2.js")?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/demo.js")?>"></script>

    <!-- <label ></label> -->


    <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        var data_json_perbandingan = JSON.parse('<?php print_r($data_json_perbandingan);?>');
        $(document).ready(function(){
          set_all_data();
          set_graph_all(data_main);
          set_graph_bulat(data_json_perbandingan[0]);
        });

        function currency(x){
          return x.toLocaleString('us-EG');
        }


        function set_all_data(){
          $("#header_page_1").html("TARGET DAN REALISASI PAJAK DAERAH KOTA MALANG");

          var title_header = data_main.title;

          var header_table = "";

          header_table += "<tr>"+
              "<th>No.</th>"+
              "<th>Jenis Pajak</th>"+
              "<th>Target</th>"+
              "<th>Realisasi</th>";

          
          header_table += "</tr>";

          $("#t_head").html(header_table);

          var str_body = "";
          var t_target = 0;
          var t_realisasi = 0;
          for (let element_item in data_main) {
            // console.log(data_main[element_item]);
            if(element_item == 0){
              str_body += "<tr><td>"+(parseInt(element_item)+1)+"</td>"+
                "<td>"+
                "<input type=\"radio\" checked=\"\" name=\"choose\" id=\"choose_"+element_item+"\" value=\""+element_item+"\" onclick=\"set_graph('"+element_item+"')\">"+
                "<label for=\"choose_"+element_item+"\">&nbsp;"+data_main[element_item].jenis_pajak+"</label></td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].target))+"</td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].realisasi))+"</td></tr>";
            }else{
              str_body += "<tr><td>"+(parseInt(element_item)+1)+"</td>"+
                "<td>"+
                "<input type=\"radio\" name=\"choose\" id=\"choose_"+element_item+"\" value=\""+element_item+"\" onclick=\"set_graph('"+element_item+"')\">"+
                "<label for=\"choose_"+element_item+"\">&nbsp;"+data_main[element_item].jenis_pajak+"</label></td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].target))+"</td>"+
                "<td align=\"right\">Rp. "+currency(parseFloat(data_main[element_item].realisasi))+"</td></tr>";
            }
            

            t_target += parseFloat(data_main[element_item].target);
            t_realisasi += parseFloat(data_main[element_item].realisasi);
          }

          // str_body += "<tr><td colspan='2'><b>Total</b></td><td align=\"right\">Rp. "+currency(t_target)+"</td><td align=\"right\">Rp. "+currency(t_realisasi)+"</td></tr>";


          $("#t_body").html(str_body);
        }

        function set_graph_all(data_val){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

           // Create chart instance
          var chart = am4core.create("chartdiv_all", am4charts.XYChart);

          // Add data
          chart.data = data_val;

          // Create axes
          var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "jenis_pajak";
          categoryAxis.numberFormatter.numberFormat = "#";
          categoryAxis.renderer.inversed = true;
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.renderer.cellStartLocation = 0.1;
          categoryAxis.renderer.cellEndLocation = 0.9;

          var  valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
          valueAxis.renderer.opposite = true;

          // Create series
          function createSeries(field, name) {
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueX = field;
            series.dataFields.categoryY = "jenis_pajak";
            series.name = name;
            series.columns.template.tooltipText = "{name}: [bold]{valueX}[/]";
            series.columns.template.height = am4core.percent(100);
            series.sequencedInterpolation = true;

            var valueLabel = series.bullets.push(new am4charts.LabelBullet());
            valueLabel.label.text = "{valueX}";
            valueLabel.label.horizontalCenter = "left";
            valueLabel.label.dx = 10;
            valueLabel.label.hideOversized = false;
            valueLabel.label.truncate = false;

            var categoryLabel = series.bullets.push(new am4charts.LabelBullet());
            categoryLabel.label.text = "{name}";
            categoryLabel.label.horizontalCenter = "right";
            categoryLabel.label.dx = -10;
            categoryLabel.label.fill = am4core.color("#fff");
            categoryLabel.label.hideOversized = false;
            categoryLabel.label.truncate = false;
          }

          createSeries("realisasi", "Realisasi");
          createSeries("target", "Target");

          });
        }

        function set_graph_bulat(data_val){
          console.log(data_val);

          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

          var chart = am4core.create("chartdiv_jenis", am4charts.XYChart);
          chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

          chart.data = data_val;


          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.dataFields.category = "title";
          categoryAxis.renderer.minGridDistance = 40;

          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

          var series = chart.series.push(new am4charts.CurvedColumnSeries());
          series.dataFields.categoryX = "title";
          series.dataFields.valueY = "val";
          series.tooltipText = "{valueY.value}"
          series.columns.template.strokeOpacity = 0;
          series.columns.template.tension = 1;

          series.columns.template.fillOpacity = 0.75;

          var hoverState = series.columns.template.states.create("hover");
          hoverState.properties.fillOpacity = 1;
          hoverState.properties.tension = 0.8;

          chart.cursor = new am4charts.XYCursor();

          // Add distinctive colors for each column using adapter
          series.columns.template.adapter.add("fill", function(fill, target) {
            return chart.colors.getIndex(target.dataItem.index);
          });

          chart.scrollbarX = new am4core.Scrollbar();
          chart.scrollbarY = new am4core.Scrollbar();

          }); // end am4core.ready()
        }

        function set_graph(param){

          // $("#choose_"+param).prop("checked", true);
          // data_main
          $("#grafik_detail").html("GRAFIK PERBANDINGAN REALISASI DAN TARGET PAJAK "+ data_main[param].jenis_pajak.toUpperCase());

          set_graph_bulat(data_json_perbandingan[param]);
          // console.log("ok");
        }

    </script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>








































<!-- copyright-edited by NCCSQUAD -->