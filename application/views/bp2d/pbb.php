<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

    <link href="<?=base_url("/AdminLTE2/bower_components/bootstrap/dist/css/bootstrap.min.css")?>" rel="stylesheet" >
    <!-- Font Awesome -->
    <link  href="<?=base_url("/AdminLTE2/bower_components/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet">
    <!-- Ionicons -->
    <link  href="<?=base_url("/AdminLTE2/bower_components/Ionicons/css/ionicons.min.css")?>" rel="stylesheet">
    <!-- jvectormap -->
    <!-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> -->
  
    <link  href="<?=base_url("/AdminLTE2/dist/css/AdminLTE.min.css")?>" rel="stylesheet">
    
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
</head>

<body>
<img class="background-ncc-logo-full" src="<?php echo base_url("assets/images/background_pbb.jpg"); ?>" alt="NCC logo">
      
    <!-- Main content -->
    <section class="content" >
      <!-- right column -->
      <div class="box-header with-border">
          <div class="container py-3">
            <h1 class="headline text-yellow"> CEK PAJAK BUMI DAN BANGUNAN KOTA MALANG</h1><br>
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  
                  <div class="col-sm-6">
                    <label for="inputEmail3" class="control-label">NOP</label>
                    <input type="number" class="form-control" id="nop" name="nop" placeholder="Nomor Objek Pajak">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-6">
                    <label for="inputPassword3" class="control-label">NAMA_WP</label>
                    <input type="text" class="form-control" id="nama_wp" name="nama_wp" placeholder="Nama Wajib Pajak">
                  </div>
                </div>
               
              </div>
              <!-- /.box-body -->
              <div class="col-sm-6 text-right">
                <button type="button" id="btn_batal" class="btn btn-default text-right">Batal</button>&nbsp;&nbsp;
                <button type="button" id="cek_pbb" class="btn btn-info text-right">Cek PBB</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
      </div>
     
    </section>
    <div class="container py-3"><BR>
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detail PBB</h1>
      </div>
      <div class="card">
        <div class="card shadow mb-6">
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"></h6>
          </div>
          <div class="row">
            <div class="col-md-6 px-3 mt-4">
              <div class="card-block px-3">
                <table class="table table-hover">
                  <tbody> 
                    <tr>
                      <td><b>NOP</b></td>
                      <td>: <span id="out_nop">337637537623567</span></b></td>
                    </tr>
                    <tr>
                      <td><b>Nama WP</b></td>
                      <td>: <span id="out_nama_wp"></span></td>
                    </tr>
                    <tr>
                      <td><b>Alamat</b></td>
                      <td>: <span id="out_alamat"></span></td>
                    </tr>
                    <!-- <tr>
                      <td><b>Tahun Tunggakan</b></td>
                      <td>: </td>
                    </tr> -->
                  </tbody>
                </table>      
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-7">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Detail Tunggakan PBB Masyarakat Kota Malang</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                  <!-- <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div> -->
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Tahun</th>
                    <th>Jumlah Tunggakan</th> 
                  </tr>
                </thead>
                <tbody id="out_list_body">
                  <tr>
                    <td>2019</td>
                    <td>55000</td>
                  </tr>
                  <tr>
                    <td>2018</td>
                    <td>150000</td> 
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </div>


<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/datatables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/url.min.js") ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>

<!-- CHART -->
<script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>

<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>

<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

<!-- /CHART -->

<!-- jQuery 3 -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery/dist/jquery.min.js")?>"> </script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/bootstrap/dist/js/bootstrap.min.js")?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/fastclick/lib/fastclick.js")?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/adminlte.min.js")?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js")?>"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url("/AdminLTE2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")?>"></script>
<script src="<?php echo base_url("/AdminLTE2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js")?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/chart.js/Chart.js")?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/pages/dashboard2.js")?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/demo.js")?>"></script>

<script type="text/javascript">
  $(document).ready(function(){

  });

  $("#cek_pbb").click(function(){
    get_data();
  });

  $("#btn_batal").click(function(){

  });

  function clear_data(){
    $("#out_nop").html("");
    $("#out_nama_wp").html("");
    $("#out_alamat").html("");
    $("#out_list_body").html("");
  }

  function get_data(){
    var data_main =  new FormData();
    data_main.append('nama_wp', $("#nama_wp").val());
    data_main.append('nop', $("#nop").val()); 

    $.ajax({
        url: "<?php echo base_url()."/bp2d/bppdapi/get_pbb/";?>", // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: data_main,                         
        type: 'post',
        success: function(res){
            // console.log(res);
            show_resposne(res);
        }
    });
  }

  function show_resposne(res){
    var data = JSON.parse(res);

    if(data.msg_main.status){
        var main_data = data.msg_detail;
        
        $("#out_nop").html(main_data.list_result.nop);
        $("#out_nama_wp").html(main_data.list_result.nama_wp);
        $("#out_alamat").html(main_data.list_result.alamat_op);

        var list_tunggakan = main_data.list_result.tahun_tunggakan;

        var str_table = "";
        for (let element_item in list_tunggakan) {
          for (let it in list_tunggakan[element_item]) {
            str_table += "<tr><td>"+it+"</td><td>"+list_tunggakan[element_item][it]+"</td></tr>";
          }
        }

        $("#out_list_body").html(str_table);
        
    }else{
        clear_data();
    }
  }
</script>

</body>
</html>




















































<!-- copyright-edited by NCCSQUAD -->