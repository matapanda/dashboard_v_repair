<script>
    $(document).ready(function () {
        $(".currency").toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");

        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "namapajak"},
            {"data": "satuan"},
            {"data": "target"},
            {"data": "realisasi"},
            {"data": "sumberdata"},
            {"data": "keterangan"},
            {"data": "username"},
            {"data": null}
        ];
        var columnDefs = [{
            "render": $.fn.dataTable.render.number(',', '.', 2),
            "targets": [2, 3]
        }]

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/bp2d/statistik-sektoral/datatable') ?>", columns, columnDefs);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();
            data_id = data_for_table.idbp2d;
            $('#tanggal').val(data_for_table.tanggal);
            $('#namapajak').val(data_for_table.namapajak);
            $('#satuan').val(data_for_table.satuan);
            $('#target').val(formatCurrency(parseFloat(data_for_table.target)));
            $('#realisasi').val(formatCurrency(parseFloat(data_for_table.realisasi)));
            $('#sumberdata').val(data_for_table.sumberdata);
            $('#keterangan').val(data_for_table.keterangan);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/bp2d/statistik-sektoral/') ?>" + data.idbp2d, datatables);
        });
    });

    function showAdd() {
    }

    function formatToCurrency(element) {
        var p = parseFloat(element.value.replace(/,/g, ''));
        var final = formatCurrency(p)
        element.value = final;

    }

    function formatCurrency(num) {
        var p = num.toFixed(2).split(".");
        return p[0].split("").reverse().reduce(function (acc, num, i, orig) {
                return num == "-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
            }, "") + "." + p[1];
    }

    function add() {
        save_new_data("<?php echo base_url('/api/bp2d/statistik-sektoral/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/bp2d/statistik-sektoral/') ?>" + data_id, datatables);
    }
</script>
