<div class="form-group">
  <label for="tanggal">Tanggal</label>
  <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
  <label for="namapajak">Nama Pajak</label>
  <input name="namapajak" type="text" id="namapajak" placeholder="Nama Pajak" class="form-control" required>
</div>
<div class="form-group">
  <label for="satuan">Satuan</label>
  <select name="satuan" id="satuan" class="form-control"><option value="Rp">Rp</option></select>
</div>
<div class="form-group">
  <table class="table table-bordered text-center">
      <tr>
          <th>Target</th>
          <th>Realisasi</th>
      </tr>
      <tr>
          <td>
              <input name="target" type="text" id="target" placeholder="Target" class="form-control currency" required onchange="formatToCurrency(this)">
          </td>
          <td>
              <input name="realisasi" type="text" id="realisasi" placeholder="Realisasi" class="form-control currency" required onchange="formatToCurrency(this)"
          </td>
      </tr>
  </table>
</div>
<div class="form-group">
  <label for="sumberdata">Sumber Data</label>
  <input name="sumberdata" type="text" class="form-control" id="sumberdata" placeholder="Sumber Data" required>
</div>
<div class="form-group">
  <label for="keterangan">Keterangan</label>
  <input name="keterangan" type="text" class="form-control" id="keterangan" placeholder="Keterangan" required>
</div>
