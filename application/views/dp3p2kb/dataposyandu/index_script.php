<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "kelurahan"},
            {"data": "jumlahposyandu"},
            {"data": "tanggal"},
            {"data": "username"},
            {"data": null}
        ];
        console.log(columns);
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dp3p2kb/data-posyandu/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.iddataposyandu;
            $('#tanggal').val(data_for_table.tanggal);
            $('#jumlahposyandu').val(data_for_table.jumlahposyandu);
            $('#keterangan').val(data_for_table.keterangan);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dp3p2kb/data-posyandu/') ?>" + data.iddataposyandu, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dp3p2kb/data-posyandu') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dp3p2kb/data-posyandu/') ?>" + data_id, datatables);
    }
</script>
