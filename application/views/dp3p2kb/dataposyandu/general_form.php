<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="jumlahposyandu">Jumlah Posyandu</label>
    <input name="jumlahposyandu" type="number" id="jumlahposyandu" placeholder="Jumlah Posyandu" class="form-control" required>
</div>
