<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "kecamatan"},
            {"data": "tanggal"},
            {"data": "status_pus"},
            {"data": "statuspusmupar"},
            {"data": "jumlahkeluarga"},
            {"data": "jumlahanggotakeluarga_balita"},
            {"data": "jumlahanggotakeluarga_anak"},
            {"data": "jumlahanggotakeluarga_remaja"},
            {"data": "jumlahanggotakeluarga_dewasa"},
            {"data": "jumlahanggotakeluarga_lansia"},
            {"data": "pasangansubur_mow"},
            {"data": "pasangansubur_mop"},
            {"data": "pasangansubur_iud"},
            {"data": "pasangansubur_implan"},
            {"data": "pasangansubur_suntik"},
            {"data": "pasangansubur_pil"},
            {"data": "pasangansubur_kondom"},
            {"data": "pasangansubur_tradisional"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga-tahu/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.id_hasilpendapatankeluargatahu;
            $('#tanggal').val(data_for_table.tanggal);
            $('#jumlahkeluarga').val(data_for_table.jumlahkeluarga);
            $('#jumlahanggotakeluarga_balita').val(data_for_table.jumlahanggotakeluarga_balita);
            $('#jumlahanggotakeluarga_anak').val(data_for_table.jumlahanggotakeluarga_anak);
            $('#jumlahanggotakeluarga_remaja').val(data_for_table.jumlahanggotakeluarga_remaja);
            $('#jumlahanggotakeluarga_dewasa').val(data_for_table.jumlahanggotakeluarga_dewasa);
            $('#jumlahanggotakeluarga_lansia').val(data_for_table.jumlahanggotakeluarga_lansia);
            $('#status_pus').val(data_for_table.status_pus);
            $('#statuspusmupar').val(data_for_table.statuspusmupar);
            $('#pasangansubur_mow').val(data_for_table.pasangansubur_mow);
            $('#pasangansubur_mop').val(data_for_table.pasangansubur_mop);
            $('#pasangansubur_iud').val(data_for_table.pasangansubur_iud);
            $('#pasangansubur_implan').val(data_for_table.pasangansubur_implan);
            $('#pasangansubur_suntik').val(data_for_table.pasangansubur_suntik);
            $('#pasangansubur_pil').val(data_for_table.pasangansubur_pil);
            $('#pasangansubur_kondom').val(data_for_table.pasangansubur_kondom);
            $('#pasangansubur_tradisional').val(data_for_table.pasangansubur_tradisional);

            fill_select_master('kecamatan', "<?php echo base_url() ?>", [$("#idkecamatan")], data_for_table);



        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga-tahu/') ?>"+data.id_hasilpendapatankeluargatahu, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kecamatan', "<?php echo base_url() ?>", [$(".form-add #idkecamatan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga-tahu') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga-tahu/') ?>"+data_id, datatables);
    }
</script>
