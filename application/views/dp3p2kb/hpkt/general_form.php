<div class="form-group">
    <label for="idkecamatan">Kecamatan</label><br>
    <select name="idkecamatan" id="idkecamatan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="jumlahkeluarga">Jumlah Keluarga</label>
    <input name="jumlahkeluarga" type="text" id="jumlahkeluarga" placeholder="Jumlah Keluarga"
           class="form-control" required>
</div>
<div class="form-group">
    <label for="status_pus">Status PUS</label>
    <input name="status_pus" type="text" id="status_pus" placeholder="Status PUS" class="form-control"
           required>
</div>
<div class="form-group">
    <label for="statuspusmupar">Status PUSMUPAR</label>
    <input name="statuspusmupar" type="text" id="statuspusmupar" placeholder="Status PUSMUPAR"
           class="form-control" required>
</div>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="5">
            Jumlah Anggota Keluarga
        </th>
    </tr>
    <tr>
        <td>
            <label for="jumlahanggotakeluarga_balita">Balita</label>
            <input name="jumlahanggotakeluarga_balita" type="text" id="jumlahanggotakeluarga_balita"
                   placeholder="Jumlah Anggota Keluarga Balita" class="form-control" required>
        </td>
        <td>
            <label for="jumlahanggotakeluarga_anak">Anak</label>
            <input name="jumlahanggotakeluarga_anak" type="text" id="jumlahanggotakeluarga_anak"
                   placeholder="Jumlah Anggota Keluarga Anak" class="form-control" required>
        </td>
        <td>
            <label for="jumlahanggotakeluarga_remaja">Remaja</label>
            <input name="jumlahanggotakeluarga_remaja" type="text" id="jumlahanggotakeluarga_remaja"
                   placeholder="Jumlah Anggota Keluarga Remaja" class="form-control" required>
        </td>
        <td>
            <label for="jumlahanggotakeluarga_dewasa">Dewasa</label>
            <input name="jumlahanggotakeluarga_dewasa" type="text" id="jumlahanggotakeluarga_dewasa"
                   placeholder="Jumlah Anggota Keluarga Dewasa" class="form-control" required>
        </td>
        <td>
            <label for="jumlahanggotakeluarga_lansia">Lansia</label>
            <input name="jumlahanggotakeluarga_lansia" type="text" id="jumlahanggotakeluarga_lansia"
                   placeholder="Jumlah Anggota Keluarga Lansia" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="4">
            Jumlah Pasangan Subur
        </th>
    </tr>
    <tr>
        <td>
            <label for="pasangansubur_mow">MOW</label>
            <input name="pasangansubur_mow" type="text" id="pasangansubur_mow"
                   placeholder="Pasangan Subur MOW" class="form-control" required>
        </td>
        <td>
            <label for="pasangansubur_mop">MOP</label>
            <input name="pasangansubur_mop" type="text" id="pasangansubur_mop"
                   placeholder="Pasangan Subur MOP" class="form-control" required>
        </td>
        <td>
            <label for="pasangansubur_iud">IUD</label>
            <input name="pasangansubur_iud" type="text" id="pasangansubur_iud"
                   placeholder="Pasangan Subur IUD" class="form-control" required>
        </td>
        <td>
            <label for="pasangansubur_implan">Implan</label>
            <input name="pasangansubur_implan" type="text" id="pasangansubur_implan"
                   placeholder="Pasangan Subur Implan" class="form-control" required>
        </td>
    </tr>
    <tr>
        <td>
            <label for="pasangansubur_suntik">Suntik</label>
            <input name="pasangansubur_suntik" type="text" id="pasangansubur_suntik"
                   placeholder="Pasangan Subur Suntik" class="form-control" required>
        </td>
        <td>
            <label for="pasangansubur_pil">Pil</label>
            <input name="pasangansubur_pil" type="text" id="pasangansubur_pil"
                   placeholder="Pasangan Subur Pil" class="form-control" required>
        </td>
        <td>
            <label for="pasangansubur_kondom">Kondom</label>
            <input name="pasangansubur_kondom" type="text" id="pasangansubur_kondom"
                   placeholder="Pasangan Subur Kondom" class="form-control" required>
        </td>
        <td>
            <label for="pasangansubur_tradisional">Tradisional</label>
            <input name="pasangansubur_tradisional" type="text" id="pasangansubur_tradisional"
                   placeholder="Pasangan Subur Tradisional" class="form-control" required>
        </td>
    </tr>
</table>
