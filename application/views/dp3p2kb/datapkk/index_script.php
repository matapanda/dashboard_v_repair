<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns =  [
            {"data": "no"},
            {"data": "kecamatan"},
            {"data": "tanggal"},
            {"data": "kelurahan"},
            {"data": "jumlahkelompok_pkkrw"},
            {"data": "jumlahkelompok_pkkrt"},
            {"data": "jumlahkelompok_dasawisma"},
            {"data": "keterangan"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dp3p2kb/data-pkk/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.iddatapkk;
            $('#tanggal').val(data_for_table.tanggal);
            $('#kelurahan').val(data_for_table.kelurahan);
            $('#jumlahkelompok_pkkrw').val(data_for_table.jumlahkelompok_pkkrw);
            $('#jumlahkelompok_pkkrt').val(data_for_table.jumlahkelompok_pkkrt);
            $('#jumlahkelompok_dasawisma').val(data_for_table.jumlahkelompok_dasawisma);
            $('#keterangan').val(data_for_table.keterangan);

            fill_select_master('kecamatan', "<?php echo base_url() ?>", [$("#idkecamatan")], data_for_table);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dp3p2kb/data-pkk/') ?>"+data.iddatapkk, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kecamatan', "<?php echo base_url() ?>", [$(".form-add #idkecamatan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dp3p2kb/data-pkk/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dp3p2kb/data-pkk/') ?>"+data_id, datatables);
    }
</script>