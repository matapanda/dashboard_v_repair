<div class="form-group">
    <label for="idkecamatan">Kecamatan</label><br>
    <select name="idkecamatan" id="idkecamatan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="kelurahan">Kelurahan</label>
    <input name="kelurahan" type="number" class="form-control" id="kelurahan" placeholder="Jumlah Kelurahan" required>
</div>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="3">
            Jumlah Kelompok
        </th>
    </tr>
    <tr>
        <th>PKK RW</th>
        <th>PKK RT</th>
        <th>Dasawisma</th>
    </tr>
    <tr>
        <th>
            <input name="jumlahkelompok_pkkrw" type="number" id="jumlahkelompok_pkkrw" placeholder="Jumlah Kelompok PKK RW" class="form-control" required>
        </th>
        <th>
            <input name="jumlahkelompok_pkkrt" type="number" id="jumlahkelompok_pkkrt" placeholder="Jumlah Kelompok PKK RT" class="form-control" required>
        </th>
        <th>
            <input name="jumlahkelompok_dasawisma" type="number" id="jumlahkelompok_dasawisma" placeholder="Jumlah Kelompok Dasawisma" class="form-control" required>
        </th>
    </tr>
</table>
<div class="form-group">
    <label for="keterangan">Keterangan</label>
    <input name="keterangan" type="text" id="keterangan" placeholder="Keterangan" class="form-control" required>
</div>