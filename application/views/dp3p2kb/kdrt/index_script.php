<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kecamatan"},
            {"data": "jeniskasus"},
            {"data": "jeniskorban"},
            {"data": "jumlah_korban"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dp3p2kb/kdrt/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idkdrt;

            $('#tanggal').val(data_for_table.tanggal);
            $('#jumlah_korban').val(data_for_table.jumlah_korban);

            fill_select_master('kecamatan', "<?php echo base_url() ?>", [$("#idkecamatan")], data_for_table);

            $("#idjeniskasus").empty();
            //get data jenis kasus
            $.ajax({
                url: "<?php echo base_url('api/dp3p2kb/kdrt/jenis-kasus') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if(data.success){
                    $('#modal-edit select').css('width', '100%');
                    $("#idjeniskasus").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function() {
                        $("#idjeniskasus").append($("<option />").val(this.idjeniskasus).text(this.jeniskasus));
                    });
                    $('#idjeniskasus').val(data_for_table.idjeniskasus);
                }else{
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan", "error");
            });

            $("#idjeniskorban").empty();
            //get data jenis korban
            $.ajax({
                url: "<?php echo base_url('api/dp3p2kb/kdrt/jenis-korban') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if(data.success){
                    $('#modal-edit select').css('width', '100%');
                    $("#idjeniskorban").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function() {
                        $("#idjeniskorban").append($("<option />").val(this.idjeniskorban).text(this.jeniskorban));
                    });
                    $('#idjeniskorban').val(data_for_table.idjeniskorban);
                }else{
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan", "error");
            });
        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dp3p2kb/kdrt/') ?>"+data.idkdrt, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kecamatan', "<?php echo base_url() ?>", [$(".form-add #idkecamatan")]);

        $(".form-add #idjeniskasus").empty();
        //get data jenis kasus
        $.ajax({
            url: "<?php echo base_url('api/dp3p2kb/kdrt/jenis-kasus') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if(data.success){
                $('#modal-add select').css('width', '100%');
                $(".form-add #idjeniskasus").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function() {
                    $(".form-add #idjeniskasus").append($("<option />").val(this.idjeniskasus).text(this.jeniskasus));
                });
            }else{
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan", "error");
        });

        $(".form-add #idjeniskorban").empty();
        //get data jenis korban
        $.ajax({
            url: "<?php echo base_url('api/dp3p2kb/kdrt/jenis-korban') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if(data.success){
                $('#modal-add select').css('width', '100%');
                $(".form-add #idjeniskorban").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function() {
                    $(".form-add #idjeniskorban").append($("<option />").val(this.idjeniskorban).text(this.jeniskorban));
                });
            }else{
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan", "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dp3p2kb/kdrt') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dp3p2kb/kdrt/') ?>"+data_id, datatables);
    }
</script>
