<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="jumlah_korban">Jumlah Korban</label>
    <input name="jumlah_korban" type="number" id="jumlah_korban" placeholder="Jumlah Korban"
           class="form-control" required>
</div>

<div class="form-group">
    <label for="idkecamatan">kecamatan</label><br>
    <select name="idkecamatan" id="idkecamatan" class="form-control"></select>
</div>

<div class="form-group">
    <label for="idjeniskasus">Jenis Kasus</label><br>
    <select name="idjeniskasus" id="idjeniskasus" class="form-control"></select>
</div>

<div class="form-group">
    <label for="idjeniskorban">Jenis Korban</label><br>
    <select name="idjeniskorban" id="idjeniskorban" class="form-control"></select>
</div>

