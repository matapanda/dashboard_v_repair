<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kecamatan"},
            {"data": "jumlahkepalakeluarga_didata"},
            {"data": "rt_ada"},
            {"data": "rt_didata"},
            {"data": "dusun_rw_ada"},
            {"data": "dusun_rw_didata"},
            {"data": "desa_kelurahan_ada"},
            {"data": "desa_kelurahan_didata"},
            {"data": "bukanpesertakb_hamil"},
            {"data": "bukanpesertakb_inginanaksegera"},
            {"data": "bukanpesertakb_inginanakditunda"},
            {"data": "bukanpesertakb_tidakinginanaklagi"},
            {"data": "kesertaandalampoktan_bkp"},
            {"data": "kesertaandalampoktan_bkr"},
            {"data": "kesertaandalampoktan_bkl"},
            {"data": "kesertaandalampoktan_lppks"},
            {"data": "kesertaandalampoktan_pik_rm"},
            {"data": "tahapankeluargasejahtera_prasejahtera"},
            {"data": "tahapankeluargasejahtera_sejahtera1"},
            {"data": "tahapankeluargasejahtera_sejahtera2"},
            {"data": 'username'},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idhasilpendapatankeluarga;
            $('#tanggal').val(data_for_table.tanggal);
            $('#desa_kelurahan_ada').val(data_for_table.desa_kelurahan_ada);
            $('#desa_kelurahan_didata').val(data_for_table.desa_kelurahan_didata);
            $('#dusun_rw_ada').val(data_for_table.dusun_rw_ada);
            $('#dusun_rw_didata').val(data_for_table.dusun_rw_didata);
            $('#rt_ada').val(data_for_table.rt_ada);
            $('#rt_didata').val(data_for_table.rt_didata);
            $('#jumlahkepalakeluarga_didata').val(data_for_table.jumlahkepalakeluarga_didata);
            $('#bukanpesertakb_hamil').val(data_for_table.bukanpesertakb_hamil);
            $('#bukanpesertakb_inginanaksegera').val(data_for_table.bukanpesertakb_inginanaksegera);
            $('#bukanpesertakb_inginanakditunda').val(data_for_table.bukanpesertakb_inginanakditunda);
            $('#bukanpesertakb_tidakinginanaklagi').val(data_for_table.bukanpesertakb_tidakinginanaklagi);
            $('#kesertaandalampoktan_bkp').val(data_for_table.kesertaandalampoktan_bkp);
            $('#kesertaandalampoktan_bkr').val(data_for_table.kesertaandalampoktan_bkr);
            $('#kesertaandalampoktan_bkl').val(data_for_table.kesertaandalampoktan_bkl);
            $('#kesertaandalampoktan_lppks').val(data_for_table.kesertaandalampoktan_lppks);
            $('#kesertaandalampoktan_pik_rm').val(data_for_table.kesertaandalampoktan_pik_rm);
            $('#tahapankeluargasejahtera_prasejahtera').val(data_for_table.tahapankeluargasejahtera_prasejahtera);
            $('#tahapankeluargasejahtera_sejahtera1').val(data_for_table.tahapankeluargasejahtera_sejahtera1);
            $('#tahapankeluargasejahtera_sejahtera2').val(data_for_table.tahapankeluargasejahtera_sejahtera2);


            fill_select_master('kecamatan', "<?php echo base_url() ?>", [$("#idkecamatan")], data_for_table);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga/') ?>" + data.idhasilpendapatankeluarga, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kecamatan', "<?php echo base_url() ?>", [$(".form-add #idkecamatan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dp3p2kb/hasil-pendapatan-keluarga/') ?>" + data_id, datatables);
    }
</script>
