<div class="form-group">
    <label for="idkecamatan">Kecamatan</label><br>
    <select name="idkecamatan" id="idkecamatan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="jumlahkepalakeluarga_didata">Jumlah Kepala Keluarga Didata</label>
    <input name="jumlahkepalakeluarga_didata" type="number" id="jumlahkepalakeluarga_didata" placeholder="Jumlah Kepala Keluarga Didata"
           class="form-control" required>
</div>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Jumlah Desa Kelurahan
        </th>
    </tr>
    <tr>
        <td>
            <label for="desa_kelurahan_ada">Ada</label>
            <input name="desa_kelurahan_ada" type="text" id="desa_kelurahan_ada" placeholder="Jumlah Desa Kelurahan Ada"
                   class="form-control" required>
        </td>
        <td>
            <label for="desa_kelurahan_didata">Didata</label>
            <input name="desa_kelurahan_didata" type="text" id="desa_kelurahan_didata" placeholder="Jumlah Desa Kelurahan Ada"
                   class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Jumlah Dusun RW
        </th>
    </tr>
    <tr>
        <td>
            <label for="dusun_rw_ada">Ada</label>
            <input name="dusun_rw_ada" type="text" id="dusun_rw_ada" placeholder="Jumlah Dusun RW Ada"
                   class="form-control" required>
        </td>
        <td>
            <label for="dusun_rw_didata">Didata</label>
            <input name="dusun_rw_didata" type="text" id="dusun_rw_didata" placeholder="Jumlah Dusun RW Didata"
                   class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Jumlah Dusun RT
        </th>
    </tr>
    <tr>
        <td>
            <label for="rt_ada">Ada</label>
            <input name="rt_ada" type="text" id="rt_ada" placeholder="Jumlah RT Ada"
                   class="form-control" required>
        </td>
        <td>
            <label for="rt_didata">Didata</label>
            <input name="rt_didata" type="text" id="rt_didata" placeholder="Jumlah RT Didata"
                   class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
       <th colspan="4">
           Jumlah Bukan Peserta KB
       </th>
    </tr>
    <tr>
        <th>Hamil</th>
        <th>Ingin Anak Segera</th>
        <th>Ingin Anak Ditunda</th>
        <th>Tidak Ingin Anak Lagi</th>
    </tr>
    <tr>
        <td>
            <input name="bukanpesertakb_hamil" type="text" id="bukanpesertakb_hamil" placeholder="Jumlah Bukan Peserta KB Hamil"
                   class="form-control" required>
        </td>
        <td>
            <input name="bukanpesertakb_inginanaksegera" type="text" id="bukanpesertakb_inginanaksegera" placeholder="Jumlah Bukan Peserta KB Ingin Anak Segera"
                   class="form-control" required>
        </td>
        <td>
            <input name="bukanpesertakb_inginanakditunda" type="text" id="bukanpesertakb_inginanakditunda" placeholder="Jumlah Bukan Peserta KB Ingin Anak Ditunda"
                   class="form-control" required>
        </td>
        <td>
            <input name="bukanpesertakb_tidakinginanaklagi" type="text" id="bukanpesertakb_tidakinginanaklagi" placeholder="Jumlah Bukan Peserta KB Tidak Ingin Anak Lagi"
                   class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="5">
            Jumlah Kesertaan Dalam POKTAN
        </th>
    </tr>
    <tr>
        <td>
            <label for="kesertaandalampoktan_bkp">BKP</label>
            <input name="kesertaandalampoktan_bkp" type="text" id="kesertaandalampoktan_bkp" placeholder="Jumlah Kesertaan Dalam POKTAN BKP"
                   class="form-control" required>
        </td>
        <td>
            <label for="kesertaandalampoktan_bkr">BKR</label>
            <input name="kesertaandalampoktan_bkr" type="text" id="kesertaandalampoktan_bkr" placeholder="Jumlah Kesertaan Dalam POKTAN BKR"
                   class="form-control" required>
        </td>
        <td>
            <label for="kesertaandalampoktan_bkl">BKL</label>
            <input name="kesertaandalampoktan_bkl" type="text" id="kesertaandalampoktan_bkl" placeholder="Jumlah Kesertaan Dalam POKTAN BKL"
                   class="form-control" required>
        </td>
        <td>
            <label for="kesertaandalampoktan_lppks">LPPKS</label>
            <input name="kesertaandalampoktan_lppks" type="text" id="kesertaandalampoktan_lppks" placeholder="Jumlah Kesertaan Dalam POKTAN LPPKS"
                   class="form-control" required>
        </td>
        <td>
            <label for="kesertaandalampoktan_pik_rm">PIK RM</label>
            <input name="kesertaandalampoktan_pik_rm" type="text" id="kesertaandalampoktan_pik_rm" placeholder="Jumlah Kesertaan Dalam POKTAN PIK RM"
                   class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
       <th colspan="3">
           Jumlah Tahapan Keluarga
       </th>
    </tr>
    <tr>
        <td>
            <label for="tahapankeluargasejahtera_prasejahtera">Pra Sejahtera</label>
            <input name="tahapankeluargasejahtera_prasejahtera" type="text" id="tahapankeluargasejahtera_prasejahtera" placeholder="Jumlah Tahapan Keluarga Pra Sejahtera"
                   class="form-control" required>
        </td>
        <td>
            <label for="tahapankeluargasejahtera_sejahtera1">Sejahtera 1</label>
            <input name="tahapankeluargasejahtera_sejahtera1" type="text" id="tahapankeluargasejahtera_sejahtera1" placeholder="Jumlah Tahapan Keluarga Sejahtera 1"
                   class="form-control" required>
        </td>
        <td>
            <label for="tahapankeluargasejahtera_sejahtera2">Sejahtera 2</label>
            <input name="tahapankeluargasejahtera_sejahtera2" type="text" id="tahapankeluargasejahtera_sejahtera2" placeholder="Sejahtera 2"
                   class="form-control" required>
        </td>
    </tr>
</table>
