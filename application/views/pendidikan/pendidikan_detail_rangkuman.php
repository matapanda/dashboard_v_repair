<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title_header ? " | $title_header" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

    <link href="<?=base_url("/AdminLTE2/bower_components/bootstrap/dist/css/bootstrap.min.css")?>" rel="stylesheet" >
    <!-- Font Awesome -->
    <link  href="<?=base_url("/AdminLTE2/bower_components/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet">
    <!-- Ionicons -->
    <link  href="<?=base_url("/AdminLTE2/bower_components/Ionicons/css/ionicons.min.css")?>" rel="stylesheet">
    <!-- jvectormap -->
    <!-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> -->
  
    <link  href="<?=base_url("/AdminLTE2/dist/css/AdminLTE.min.css")?>" rel="stylesheet">
    


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>

<body>

  <?php
    // print_r("<pre>");
    // print_r($main_data["title_header"]);

  ?>

    <img class="background-ncc-logo-full" src="<?php echo base_url("assets/images/ncc-logo-grayscale.png "); ?>" alt="NCC logo">

    <div class="col-md-3">
        <h3><?php echo "GRAFIK ".$main_data["title_header"]; ?></h3>
        <hr>
        <p>
            <?= $option;?>
        </p>
        <table class="table table-striped table-bordered" id="chart-table">
            <!-- <tr>
              <td colspan='5' align="left" id="nama_dinas"><b>Berdasarkan Bentuk Pendidikan <br> (TK/KB/TPA/SPS/RA)</b></td>
            </tr> -->

            <!-- Tabel PAUD (TK, KB, TPA, SPS, RA)-->
            <!-- <tr class='PAUD_main_1'>

              <td colspan='5' align="center"><label for="filter_gl"><i><b>TK</b></i></label></td>
            </tr> -->
            <thead id="main_header_table">
              
            </thead>
            <tbody id="main_table">
              
            </tbody>
          

        </table>
        <hr>
        <p class="small pull-right">
            <a href="<?php echo base_url('') ?>">NCC</a> |
            <a href="<?php echo base_url('katalog') ?>">Katalog</a>
        </p>
    </div>

    <script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <div class="col-md-9" id="chart_opening">

        <CENTER>
            <h1 id="title_data"> JUMLAH SATUAN PENDIDIKAN (SEKOLAH) <br>Berdasarkan Bentuk Pendidikan (TK/KB/TPA/SPS/RA)</h1>
        </CENTER>
        <br>
        <br>

        


        <!-- GRAFIK ZONA KEC LOWOKWARU -->
        <center>
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title" id="chart_header_title"> GRAFIK JUMLAH SATUAN PENDIDIKAN MENURUT ZONA </h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_detail" style="height: 550px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
    </center>


    </div>
    <script src="<?php echo base_url("assets/js/jquery.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.loading.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.dataTables.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/datatables.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/select2.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/sweetalert.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/app.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/url.min.js ") ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>

    <!-- CHART -->
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>

    <!-- <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script> -->

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>

    <!-- /CHART -->

    <!-- jQuery 3 -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/jquery/dist/jquery.min.js ")?>">
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/bootstrap/dist/js/bootstrap.min.js ")?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/fastclick/lib/fastclick.js ")?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url("AdminLTE2/dist/js/adminlte.min.js ")?>"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js ")?>"></script>
    <!-- jvectormap  -->
    <script src="<?php echo base_url("AdminLTE2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")?>"></script>
    <script src="<?php echo base_url("AdminLTE2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js")?>"></script>
    <!-- ChartJS -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/chart.js/Chart.js ")?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo base_url("AdminLTE2/dist/js/pages/dashboard2.js")?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url("AdminLTE2/dist/js/demo.js ")?>"></script>

    
    <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        $(document).ready(function(){
          var key = data_main.item;
          
          $("#select_tipe").val(Object.keys(key)[0]);

          var val_data = $("#select_tipe").val();
          get_data_x(val_data);

          // console.log(Object.keys(key)[0]);
          // console.log(data_main);
        });

        function get_data_x(val_data){
          console.log(data_main);
          $("#title_data").html(data_main.title_header +" "+ data_main.title_main);
          render_table(val_data);
        }

        function render_table(val_data){
          var data_table = data_main.item[val_data];
          // console.log(data_table);

          $("#chart_header_title").html("AGREGAT " + data_main.title_header +" "+ data_table.nama + " MENURUT ZONA");
          
          var str_header = "<tr class='PAUD_jenis_main_1' align=\"center\">"+
                  "<h3><td colspan='5'><b id=\"title_table\">JUMLAH SATUAN PENDIDIKAN</b></td></h3>"+
              "</tr>"+
              "<tr><td><b>KECAMATAN</b></td>";

          for (var i = 0; i < data_main.length; i++) {
            Things[i]
          }
                  
          str_header += "</tr>"; 

          $("#main_header_table").html("JUMLAH SATUAN PENDIDIKAN \n ("+ data_table.nama +")");

          var table_item = data_main.item[val_data].item;

          var str_table = "";
          for (var i = 0; i < table_item.length; i++) {
            str_table += "<tr><td>"+table_item[i].nama_kecamatan+"</td><td>"+table_item[i].swasta+"</td><td>"+table_item[i].negeri+"</td><td>"+(table_item[i].negeri+table_item[i].swasta)+"</td></tr>";  
          }
          // str_table += "";

          $("#main_table").html(str_table);

          set_graph(table_item);
        }

        $("#select_tipe").change(function(){

          var val_data = $("#select_tipe").val();
          get_data_x(val_data);
        });

      //   $("#show_for_detail").click(function(){
      //     window.location.href = "<?php print_r(base_url()."Bpkadapi/show_detail");?>";
      //   });

      //   $("#jenis_instansi").change(function(e){
      //     var val_data = $("#jenis_instansi").val();
      //     get_data(val_data);
          
      //   });

      //   function get_data(i){
      //     console.log(data_main[i]);
      //     $("#jml_golongan_1").html(data_main[i].sum_golongan[0].value);
      //     $("#jml_golongan_2").html(data_main[i].sum_golongan[1].value);
      //     $("#jml_golongan_3").html(data_main[i].sum_golongan[2].value);
      //     $("#jml_golongan_4").html(data_main[i].sum_golongan[3].value);


      //     $("#jml_golongan_1a").html(data_main[i].jumlah_golongan_I[0].value);
      //     $("#jml_golongan_1b").html(data_main[i].jumlah_golongan_I[1].value);
      //     $("#jml_golongan_1c").html(data_main[i].jumlah_golongan_I[2].value);
      //     $("#jml_golongan_1d").html(data_main[i].jumlah_golongan_I[3].value);
          
      //     $("#jml_golongan_2a").html(data_main[i].jumlah_golongan_II[0].value);
      //     $("#jml_golongan_2b").html(data_main[i].jumlah_golongan_II[1].value);
      //     $("#jml_golongan_2c").html(data_main[i].jumlah_golongan_II[2].value);
      //     $("#jml_golongan_2d").html(data_main[i].jumlah_golongan_II[3].value);

      //     $("#jml_golongan_3a").html(data_main[i].jumlah_golongan_III[0].value);
      //     $("#jml_golongan_3b").html(data_main[i].jumlah_golongan_III[1].value);
      //     $("#jml_golongan_3c").html(data_main[i].jumlah_golongan_III[2].value);
      //     $("#jml_golongan_3d").html(data_main[i].jumlah_golongan_III[3].value);

      //     $("#jml_golongan_4a").html(data_main[i].jumlah_golongan_IV[0].value);
      //     $("#jml_golongan_4b").html(data_main[i].jumlah_golongan_IV[1].value);
      //     $("#jml_golongan_4c").html(data_main[i].jumlah_golongan_IV[2].value);
      //     $("#jml_golongan_4d").html(data_main[i].jumlah_golongan_IV[3].value);


      //     $("#jml_gender_l").html(data_main[i].gender[0].value);
      //     $("#jml_gender_p").html(data_main[i].gender[1].value);


      //     $("#jml_ag_is").html(data_main[i].agama[0].value);
      //     $("#jml_ag_kr").html(data_main[i].agama[1].value);
      //     $("#jml_ag_ka").html(data_main[i].agama[2].value);
      //     $("#jml_ag_hi").html(data_main[i].agama[3].value);
      //     $("#jml_ag_bu").html(data_main[i].agama[4].value);


      //     $("#jml_sts_k").html(data_main[i].status_kawin[0].value);
      //     $("#jml_sts_bk").html(data_main[i].status_kawin[1].value);


      //     $("#t_karyawan").html(data_main[i].main[0].value+" (Karyawan)");
      //     $("#nama_dinas").html("<b>"+data_main[i].main[0].keterangan+"</b>");


      //     set_graph("chartdiv_gol_all", data_main[i].sum_golongan, am4core.useTheme(am4themes_material));

      //     set_graph("chartdiv_gol_I", data_main[i].jumlah_golongan_I, am4core.useTheme(am4themes_material));
      //     set_graph("chartdiv_gol_II", data_main[i].jumlah_golongan_II, am4core.useTheme(am4themes_material));
      //     set_graph("chartdiv_gol_III", data_main[i].jumlah_golongan_III, am4core.useTheme(am4themes_dataviz));
      //     set_graph("chartdiv_gol_IV", data_main[i].jumlah_golongan_IV, am4core.useTheme(am4themes_kelly));


      //     set_graph("chartdiv_jk", data_main[i].gender,am4core.useTheme(am4themes_material));

      //     set_graph("chartdiv_agama", data_main[i].agama,am4core.useTheme(am4themes_material));

      //     set_graph("chartdiv_sts", data_main[i].status_kawin);
      //   }

        function set_graph(data_main){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

          // Create chart instance
          var chart = am4core.create("chartdiv_detail", am4charts.XYChart);


          // Add data
          chart.data = data_main;

          // Create axes
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "nama_kecamatan";
          categoryAxis.renderer.grid.template.location = 0;


          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.renderer.inside = true;
          valueAxis.renderer.labels.template.disabled = true;
          valueAxis.min = 0;

          // Create series
          function createSeries(field, name) {
            
            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "nama_kecamatan";
            series.sequencedInterpolation = true;
            
            // Make it stacked
            series.stacked = true;
            
            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
            
            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            
            return series;
          }

          createSeries("negeri", "Negeri");
          createSeries("swasta", "Swasta");

          // Legend
          chart.legend = new am4charts.Legend();

          });
        }

        
    </script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>
