<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

 <link href="<?=base_url("/AdminLTE2/bower_components/bootstrap/dist/css/bootstrap.min.css")?>" rel="stylesheet" >
  <!-- Font Awesome -->
  <link  href="<?=base_url("/AdminLTE2/bower_components/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet">
  <!-- Ionicons -->
  <link  href="<?=base_url("/AdminLTE2/bower_components/Ionicons/css/ionicons.min.css")?>" rel="stylesheet">
  <!-- jvectormap -->
  <!-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> -->
  
     <link  href="<?=base_url("/AdminLTE2/dist/css/AdminLTE.min.css")?>" rel="stylesheet">
    


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>

<body>


<script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>

<div class="col-md-12" id="chart_opening">
  <CENTER><h1 id="header_page_1"></h1></CENTER>
  <br><br>

    <!-- <center>
        <div class="col-md-12">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">Filter Data</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <div class="pad">
                        <div class="row">
                          <div class="col-md-4 text-left">
                            <label for="stsek">Status Sekolah</label>
                            <select name="stsek" id="stsek" class="form-control">
                              <option value="0">Semua</option>
                              <option value="1">Negri</option>
                              <option value="2">Swasta</option>
                            </select>
                          </div>
                          <div class="col-md-4 text-left">
                            <label for="tahundata">Tahun Data</label>
                            <select name="tahundata" id="tahundata" class="form-control">
                              <?php
                              if(!empty($data_select)){
                                foreach ($data_select as $key => $value) {
                                  print_r("<option value=\"".$value->t_data."\">".$value->label."</option>");
                                }
                              }
                                
                              ?>
                            </select>
                          </div>
                          <div class="col-md-4 text-left">
                            <label for="jenjang">Jenjang</label>
                            <select name="jenjang" id="jenjang" class="form-control">
                              <option value="0">Semua</option>
                              <option value="5">Sekolah Dasar</option>
                              <option value="9">Madrasah Iptidaiyah</option>
                            </select>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </center> -->
</div>

<div class="col-md-12" id="chart_opening">
  <CENTER><h1 id="header_page_1"></h1></CENTER>
  <br><br>

    <center>
        <div class="col-md-6">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">GRAFIK STATUS AKREDITASI SEKOLAH DI KOTA MALANG</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_akred" style="height: 350px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">GRAFIK SERTIFIKASI/SERTIFIKASI</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_iso" style="height: 350px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            <!-- /.box-body -->
    </center>
</div>

<div class="col-md-12" id="chart_opening">
    <center>
        <div class="col-md-6">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">GRAFIK STATUS AKREDITASI SEKOLAH DI KOTA MALANG BERDASARKAN ZONA</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_akred_z" style="height: 350px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title">GRAFIK SERTIFIKASI/SERTIFIKASI BERDASARKAN ZONA</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv_iso_z" style="height: 350px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
            <!-- /.box-body -->
    </center>
</div>

<img class="background-ncc-logo-full" src="<?php echo base_url("assets/images/ncc-logo-grayscale.png "); ?>" alt="NCC logo">

<div class="col-md-12">
  <center><h3 id="header_page_2"></h3></center>
  <hr>
  <p><?= $str_select;?></p>
    <table class="table table-striped table-bordered" id="chart-table">
      <thead id="t_head">
        
      </thead>
      <tbody id="t_body">
        
      </tbody>
        
    </table>
  <hr>
  <p class="small pull-right">
    <a href="<?php echo base_url('') ?>">NCC</a> |
    <a href="<?php echo base_url('katalog') ?>">Katalog</a>
  </p>
</div>

<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/datatables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/url.min.js") ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>

<!-- CHART -->
<script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>

<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>

<script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

<script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>

<!-- /CHART -->

<!-- jQuery 3 -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery/dist/jquery.min.js")?>"> </script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/bootstrap/dist/js/bootstrap.min.js")?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/fastclick/lib/fastclick.js")?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/adminlte.min.js")?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js")?>"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url("/AdminLTE2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")?>"></script>
<script src="<?php echo base_url("/AdminLTE2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js")?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url("/AdminLTE2/bower_components/chart.js/Chart.js")?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/pages/dashboard2.js")?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url("/AdminLTE2/dist/js/demo.js")?>"></script>





    <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        $(document).ready(function(){
          set_all_data();
        });

        // $("#stsek").change(function(){
        //   // console.log("apik");
        //   set_request();
        //   set_all_data();
        // });


        // $("#tahundata").change(function(){
        //   // console.log("apik");
        //   set_request();
        //   set_all_data();
        // });


        // $("#jenjang").change(function(){
        //   // console.log("apik");
        //   set_request();
        //   set_all_data();
        // });


        function set_request(){
          var data_main =  new FormData();
          data_main.append('stsek', $("#stsek").val());
          data_main.append('tahundata', $("#tahundata").val());
          data_main.append('jenjang', $("#jenjang").val());
                                                
          $.ajax({
              url: "<?php echo base_url()."pendidikan_new/pendidikan/index_ma_req";?>",
              dataType: 'html',  // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: data_main,                         
              type: 'post',
              success: function(res){
                  // console.log(res);
                  change_data(res);
                  // set_val_update_strata(res, param);
              }
          });
        }

        function change_data(res){
          data_main_new = JSON.parse(res);
          data_main = data_main_new.msg_detail.item;
          console.log(data_main_new.msg_detail.item);
        }

        function set_all_data(){
          $("#header_page_1").html("Grafik "+data_main.title_header+" \n "+data_main.title_main);

          var title_header = data_main.title;

          var header_table = "<tr class='PAUD_jenis_main_1' align=\"center\">"+
            "<h3><td colspan='12'><b>"+data_main.title_header+" ("+data_main.title_main+")"+
            "</b></td></h3></tr>";

          header_table += "<tr>"+
              "<td width='3%' align='center' rowspan='3'><b>No.</b></td>"+
              "<td rowspan='3' align='center'><b>KECAMATAN</b></td>"+
              "<td rowspan='3' align='center'><b>Jumlah<br>Sekolah</b></td>";

          for (let element in data_main.item) {  
            header_table += "<td colspan='4' align='center'><b>"+data_main.item[element]["nama"]+"</b></td>";
          }
          header_table += "</tr>";

          header_table += "<tr>";
          for (let element in data_main.title) {  
            for (let element_sub in data_main.title[element]) {
              header_table += "<td rowspan='2' align='center'><b>"+data_main.title[element][element_sub]+"</b></td>";
            }
          }
          header_table += "</tr>";

          $("#t_head").html(header_table);

          var data_sum_kec = [];
          var data_all = [];

          var array_sum_item = [];
          var array_sum_sklh = [];

          var array_table = [];
          var no_x = 0; 
          for (let element_item in data_main.item) {
            var no_element_sub = 0;
            
            array_sum_item[element_item] = [];

            var sum_sekolah = 0;

            var tmp_all = [];
            for (let element_sub in data_main.item[element_item]["item"]) {
              var body_table = "";

              // array_sum_item[element_item][element_sub] = [];
              
              if(no_x == 0){
                body_table += "<td align='center'>"+(no_element_sub+1)+"</td><td>"+data_main.item[element_item]["item"][element_sub].nama_kecamatan+"</td><td align='center'>"+data_main.item[element_item]["item"][element_sub].jml_sek+"</td>";

                sum_sekolah += data_main.item[element_item]["item"][element_sub].jml_sek;

                var tmp_sum_kec = {
                  "nama_kecamatan":data_main.item[element_item]["item"][element_sub].nama_kecamatan,
                  "val":data_main.item[element_item]["item"][element_sub].jml_sek
                };

                data_sum_kec.push(tmp_sum_kec);
              }

              tmp_all[no_element_sub] = {};
              // tmp_all[no_x][no_element_sub] = {};
              
              tmp_all[no_element_sub]["nama_kecamatan"] = data_main.item[element_item]["item"][element_sub].nama_kecamatan;


              var no_element_sub_x = 0;
              for (let element_sub_x in data_main.item[element_item]["item"][element_sub]) {
                // console.log(data_main.item[element_item]["item"][element_sub][element_sub_x]);
                if(no_element_sub_x > 1){
                  body_table += "<td align='center'>"+data_main.item[element_item]["item"][element_sub][element_sub_x]+"</td>";

                  if((element_sub_x in array_sum_item[element_item])){
                    array_sum_item[element_item][element_sub_x] += data_main.item[element_item]["item"][element_sub][element_sub_x];
                  }else{
                    array_sum_item[element_item][element_sub_x] = data_main.item[element_item]["item"][element_sub][element_sub_x];
                  }

                  tmp_all[no_element_sub][element_sub_x] = data_main.item[element_item]["item"][element_sub][element_sub_x];
                  // console.log(tmp_all[no_element_sub][element_sub_x]);
                }

                no_element_sub_x++;
              }

              // console.log(tmp_all);

              data_all[no_x] = [];
              data_all[no_x].push(tmp_all);

              if((no_element_sub in array_table)){
                array_table[no_element_sub] += body_table;
              }else{
                array_table[no_element_sub] = body_table;
              }
              
              no_element_sub++;
            }

            if(no_x == 0){
              array_sum_sklh[no_x] = sum_sekolah;    
            }

            no_x++;
          }

          // console.log(array_sum_item);
          var item_graph_sum_all = {};

          var fix_table = "";
          for (let e in array_table) {
            fix_table += "<tr>"+array_table[e]+"</tr>";
          }

          fix_table += "<tr><td colspan='2' align='center'><b>Jumlah Keseluruhan</b></td><td align='center'><b>"+array_sum_sklh[0]+"</b></td>";

          var item_graph_all = {};

          for (let z in array_sum_item) {

            item_graph_all[z] = [];

            // tmp_item_graph_all = 
            for (let n in array_sum_item[z]) {
              var tmp_item_graph_all = {};
              fix_table += "<td align='center'><b>"+array_sum_item[z][n]+"</b></td>";

              if(z == "akred"){
                tmp_item_graph_all["category"] = "AKREDITASI "+n.split("_")[1].toUpperCase();
                tmp_item_graph_all["val"] = array_sum_item[z][n];
              }else if(z == "iso"){
                tmp_item_graph_all["category"] = "SERTIFIKASI "+n.split("_")[1].toUpperCase();
                tmp_item_graph_all["val"] = array_sum_item[z][n];
              }

              // console.log(tmp_item_graph_all);

              item_graph_all[z].push(tmp_item_graph_all);
            }            
          }

          fix_table += "</tr>";

          // console.log(data_all[0][0]);
          // console.log(array_sum_item);

          console.log(data_all);
          set_graph_acred_all(item_graph_all.akred);
          set_graph_iso_all(item_graph_all.iso);

          set_graph_acred(data_all[0][0]);
          set_graph_iso(data_all[1][0]);

          $("#t_body").html(fix_table);
        }

        function set_graph_acred_all(data_main){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

          var chart = am4core.create("chartdiv_akred", am4charts.PieChart3D);
          chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

          chart.legend = new am4charts.Legend();

          chart.data = data_main;

          var series = chart.series.push(new am4charts.PieSeries3D());
          series.dataFields.value = "val";
          series.dataFields.category = "category";

          });
        }

        function set_graph_iso_all(data_main){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

          var chart = am4core.create("chartdiv_iso", am4charts.PieChart3D);
          chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

          chart.legend = new am4charts.Legend();

          chart.data = data_main;

          var series = chart.series.push(new am4charts.PieSeries3D());
          series.dataFields.value = "val";
          series.dataFields.category = "category";

          });
        }


        function set_graph_acred(data_main){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

          // Create chart instance
          var chart = am4core.create("chartdiv_akred_z", am4charts.XYChart);


          // Add data
          chart.data = data_main;

          // Create axes
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "nama_kecamatan";
          categoryAxis.renderer.grid.template.location = 0;


          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.renderer.inside = true;
          valueAxis.renderer.labels.template.disabled = true;
          valueAxis.min = 0;

          // Create series
          function createSeries(field, name) {
            
            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "nama_kecamatan";
            series.sequencedInterpolation = true;
            
            // Make it stacked
            series.stacked = true;
            
            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
            
            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            
            return series;
          }

          createSeries("akred_a", "AKREDITASI A");
          createSeries("akred_b", "AKREDITASI B");
          createSeries("akred_c", "AKREDITASI C");
          createSeries("akred_tt", "AKREDITASI TT");

          // Legend
          chart.legend = new am4charts.Legend();

          });
        }

        function set_graph_iso(data_main){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

          // Create chart instance
          var chart = am4core.create("chartdiv_iso_z", am4charts.XYChart);


          // Add data
          chart.data = data_main;

          // Create axes
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "nama_kecamatan";
          categoryAxis.renderer.grid.template.location = 0;


          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.renderer.inside = true;
          valueAxis.renderer.labels.template.disabled = true;
          valueAxis.min = 0;

          // Create series
          function createSeries(field, name) {
            
            // Set up series
            var series = chart.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "nama_kecamatan";
            series.sequencedInterpolation = true;
            
            // Make it stacked
            series.stacked = true;
            
            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
            
            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            
            return series;
          }

          createSeries("iso_1", "SERTIFIKASI ISO 9001:2000");
          createSeries("iso_2", "SERTIFIKASI 9001:2008");
          createSeries("iso_3", "SERTIFIKASI Proses Sertifikasi");
          createSeries("iso_4", "SERTIFIKASI Belum Bersertifikat");

          // Legend
          chart.legend = new am4charts.Legend();

          });
        }

    </script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>








































<!-- copyright-edited by NCCSQUAD -->