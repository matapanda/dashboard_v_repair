
<!DOCTYPE html>
<html>
<head>
	
	<title>Choropleth Tutorial - Leaflet</title>

	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- <link rel="shortcut icon" type="image/x-icon" href="docs/images/favicon.ico" /> -->

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>


	<style>
		html, body {
			height: 100%;
			margin: 0;
		}
		#map {
			width: 600px;
			height: 400px;
		}
	</style>

	<style>#map { width: 100%; height: 700px; }
.info { padding: 6px 8px; font: 14px/16px Arial, Helvetica, sans-serif; background: white; background: rgba(255,255,255,0.8); box-shadow: 0 0 15px rgba(0,0,0,0.2); border-radius: 5px; } .info h4 { margin: 0 0 5px; color: #777; }
.legend { text-align: left; line-height: 18px; color: #555; } .legend i { width: 18px; height: 18px; float: left; margin-right: 8px; opacity: 0.7; }</style>
</head>
<body>

<div id='map'></div>

<!-- <script type="text/javascript" src="us-states.js"></script> -->

<script type="text/javascript">
	// console.log('<?php print_r($data_main);?>');
	var statesData = JSON.parse('<?php print_r($data_main);?>');
	var map = L.map('map').setView([-7.9771206, 112.6340291], 12);

	console.log(statesData);


	L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
		  subdomains: ['a', 'b', 'c']
		}).addTo( map );


	// control that shows state info on hover
	var info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h4>Populasi Penduduk Kota Malang</h4>' +  (props ?
			'<b>' + props.name + '</b><br />' + props.density + ' Jumlah Penduduk'
			: 'Arahkan Ke Area Kelurahan');
	};

	info.addTo(map);


	// get color depending on population density value
	function getColor(d) {
		var color;
		switch(d) {
		  case "LOWOKWARU":
		    // code block
		    color = "#2800F2";
		    break;

		  case "BLIMBING":
		    // code block
		    color = "#F20018";
		    break;

		  case "KLOJEN":
		    // code block
		    color = "#00EAF2";
		    break;

		  case "KEDUNGKANDANG":
		    // code block
		    color = "#00F220";
		    break;

		  case "SUKUN":
		    // code block
		    color = "#F2C100";
		    break;

		  default:
		    // code block
		    color = "#ACACAB";
		}

		return color;
	}

	function style(feature) {
		return {
			weight: 1,
			opacity: 1,
			color: 'black',
			fillOpacity: 0.5,
			fillColor: getColor(feature.properties.kecamatan)
		};
	}

	function highlightFeature(e) {
		var layer = e.target;

		layer.setStyle({
			weight: 2,
			color: 'white',
			dashArray: '5',
			fillOpacity: 0.7
		});

		if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
			layer.bringToFront();
		}

		info.update(layer.feature.properties);
	}

	var geojson;

	function resetHighlight(e) {
		geojson.resetStyle(e.target);
		info.update();
	}

	function zoomToFeature(e) {
		map.fitBounds(e.target.getBounds());
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight,
			click: highlightFeature
		});
	}

	geojson = L.geoJson(statesData, {
		style: style,
		onEachFeature: onEachFeature
	}).addTo(map);

	map.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">US Census Bureau</a>');


	var legend = L.control({position: 'bottomright'});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend'),
			grades = ["LOWOKWARU", "BLIMBING", "KLOJEN", "KEDUNGKANDANG", "SUKUN"],
			labels = [],
			from, to;

		for (var i = 0; i < grades.length; i++) {
			from = grades[i];

			labels.push(
				'<i style="background:' + getColor(from) + '"></i> ' +
				from);
		}

		div.innerHTML = labels.join('<br><br>');
		return div;
	};

	legend.addTo(map);

</script>



</body>
</html>
