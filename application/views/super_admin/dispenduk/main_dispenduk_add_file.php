<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php print_r($page);?></title>
</head>
<body>

<div id="container">
	<?php print_r(form_open_multipart('super_admin/dispenduk/maindispenduk/add_data'));?>
		<h1><?php print_r($page);?></h1>
		<label>Periode Bulan: </label>
		<select name="bln_periode" id="bln_periode">
			<?php
				if($month){
					foreach ($month as $key => $value) {
						if($key >= 1){
							print_r("<option value=\"".$key."\">".$value."</option>");
						}
						
					}
				}
			?>
		</select>

		<label>Periode Tahun: </label><input type="number" name="bln_th" id="bln_th" required="">
		<br><br>
		<label>Data File: </label><input type="file" name="file_upload" id="file_upload" required="">
		<br><br>
		<label>Keterangan: </label><textarea name="keterangan" id="keterangan" required=""></textarea>
		<br><br><br>
		
		<input type="submit" name="do_upload" id="do_upload" value="Upload">
	</form>

	<br><br><br>
	<table width="100%" border="1">
		<thead>
			<tr>
				<th>No.</th>
				<th>Periode</th>
				<th>Keterangan</th>
				<th>Input Date</th>
				<th>Lokasi File Excel</th>
				<th>Lokasi File JSON</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
			<!-- <a href="">Data Json . . .</a> -->


			<?php
				if($data_json){
					$no = 1;
					foreach ($data_json as $key => $value) {
						$data_periode = explode("-", $value->periode);

						$str_periode = $month[$data_periode[0]].", ".$data_periode[1];
						$main_url_excel = base_url()."assets/excel_upload/";
						$main_url_json = base_url()."assets/json_upload/";
						print_r("<tr>
									<td>".$no."</td>
									<td>".$str_periode."</td>
									<td>".$value->keterangan."</td>
									<td>".$value->input_date."</td>
									<td><a href=\"".$main_url_excel.$value->loc_file_excel."\" target=\"_blank\">Data Excel . . .</a></td>
									<td><a href=\"".$main_url_json.$value->loc_file_json."\" target=\"_blank\">Data Json . . .</a></td>
									<td>
										<button id=\"delete_data\" class=\"ok\" onclick=\"delete_data_fc('".hash('sha512', $value->id_file_data)."')\">delete</button>
										<button id=\"update_data\" class=\"ok\" onclick=\"update_data_fc('".hash('sha512', $value->id_file_data)."')\">update</button>
									</td>
								</tr>");
						$no++;
					}
				}
			?>
		</tbody>		
	</table>
</div>

<script src="<?php print_r(base_url()."assets/js/jquery-3.2.1.js");?>"></script>
<script type="text/javascript">
	function update_data_fc(){

	}

	function delete_data_fc(param){
		var data_main = new FormData();
        data_main.append('param', param);
       

        $.ajax({
            url: "<?php echo base_url()."super_admin/dispenduk/maindispenduk/delete_data";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_delete(res);
            }
        });
	}

	function response_delete(res) {
		var json_data = JSON.parse(res);
			var data_main = json_data.msg_main;
			var data_detail = json_data.msg_detail;

		if(data_main.status){
			
		}else{
			var param_res = data_detail.param;
			console.log(param_res);
		}
	}
</script>
</body>
</html>