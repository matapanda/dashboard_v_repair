<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control select-kelurahan"></select>
</div>
<div class="form-group">
    <label for="kategori">Kategori</label><br>
    <select name="idkategori" id="idkategori" class="form-control select-kategori"></select>
</div>
<div class="form-group">
    <label for="jumlah">Jumlah</label>
    <input name="jumlah" type="text" id="jumlah" placeholder="Jumlah" class="form-control" required>
</div>