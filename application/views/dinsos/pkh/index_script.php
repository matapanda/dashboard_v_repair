<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns =  [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kategori"},
            {"data": "jumlah"},
            {"data": "kelurahan"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/pkh/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idpkh;
            $('#tanggal').val(data_for_table.tanggal);
            $('#jumlah').val(data_for_table.jumlah);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);

            //get kategori pkh data
            $("#idkategori").empty();
            $.ajax({
                url: "<?php echo base_url('api/dinsos/pkh/kategori-pkh') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if(data.success){
                    $('#modal-edit select').css('width', '100%');
                    $("#idkategori").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function() {
                        $("#idkategori").append($("<option />").val(this.idkategori).text(this.kategori));
                    });
                    $('#idkategori').val(data_for_table.idkategori);
                }else{
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil kategori PKH', "error");
            });

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/pkh/') ?>"+data.idpkh, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);

        //get kategori pkh data
        $(".form-add #idkategori").empty();
        $.ajax({
            url: "<?php echo base_url('api/dinsos/pkh/kategori-pkh') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if(data.success){
                $('#modal-add select').css('width', '100%');
                $(".form-add #idkategori").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function() {
                    $(".form-add #idkategori").append($("<option />").val(this.idkategori).text(this.kategori));
                });
            }else{
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil kategori PKH', "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/pkh') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/pkh/') ?>"+data_id, datatables);
    }
</script>
