<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "no_kk"},
            {"data": "no_nik"},
            {"data": "nama"},
            {"data": "hubungankeluarga"},
            {"data": "jeniskelamin"},
            {"data": "tanggal_lahir"},
            {"data": "pendidikan"},
            {"data": "alamat"},
            {"data": "kelurahan"},
            {"data": "status"},
            {"data": "statuskeluarga"},
            {"data": "statusindividu"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/pps/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.iddinsos_pps;
            $('#tanggal').val(data_for_table.tanggal);
            $('#alamat').val(data_for_table.alamat);
            $('#no_kk').val(data_for_table.no_kk);
            $('#no_nik').val(data_for_table.no_nik);
            $('#nama').val(data_for_table.nama);
            $('#tanggal_lahir').val(data_for_table.tanggal_lahir);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);
            fill_select_master('hubungankeluarga', "<?php echo base_url() ?>", [$("#idhubungankeluarga")], data_for_table);
            fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$("#idjeniskelamin")], data_for_table);
            fill_select_master('statuskawin', "<?php echo base_url() ?>", [$("#idstatuskawin")], data_for_table);
            fill_select_master('pendidikan', "<?php echo base_url() ?>", [$("#idpendidikan")], data_for_table);
            fill_select_master('statuskeluarga', "<?php echo base_url() ?>", [$("#idstatuskeluarga")], data_for_table);
            fill_select_master('statusindividu', "<?php echo base_url() ?>", [$("#idstatusindividu")], data_for_table);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/pps/') ?>" + data.iddinsos_pps, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);
        fill_select_master('hubungankeluarga', "<?php echo base_url() ?>", [$(".form-add #idhubungankeluarga")]);
        fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$(".form-add #idjeniskelamin")]);
        fill_select_master('statuskawin', "<?php echo base_url() ?>", [$(".form-add #idstatuskawin")]);
        fill_select_master('pendidikan', "<?php echo base_url() ?>", [$(".form-add #idpendidikan")]);
        fill_select_master('statuskeluarga', "<?php echo base_url() ?>", [$(".form-add #idstatuskeluarga")]);
        fill_select_master('statusindividu', "<?php echo base_url() ?>", [$(".form-add #idstatusindividu")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/pps') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/pps/') ?>" + data_id, datatables);
    }
</script>
