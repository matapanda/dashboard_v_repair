<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control form" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control select-kelurahan"></select>
</div>
<div class="form-group">
    <label for="alamat">Alamat</label>
    <input name="alamat" type="text" id="alamat" placeholder="Alamat" class="form-control" required>
</div>
<div class="form-group">
    <label for="no_kk">Nomor KK</label>
    <input name="no_kk" type="number" id="no_kk" placeholder="Nomor KK" class="form-control" required>
</div>
<div class="form-group">
    <label for="no_nik">Nomor NIK</label>
    <input name="no_nik" type="number" id="no_nik" placeholder="Nomor NIK" class="form-control" required>
</div>
<div class="form-group">
    <label for="nama">Nama</label>
    <input name="nama" type="text" id="nama" placeholder="Nama" class="form-control" required>
</div>
<div class="form-group">
    <label for="idhubungankeluarga">Hubungan Keluarga</label><br>
    <select name="idhubungankeluarga" id="idhubungankeluarga" class="form-control select-hubungankeluarga"></select>
</div>
<div class="form-group">
    <label for="idjeniskelamin">Jenis Kelamin</label><br>
    <select name="idjeniskelamin" id="idjeniskelamin" class="form-control select-jeniskelamin"></select>
</div>
<div class="form-group">
    <label for="tanggal_lahir">Tanggal Lahir</label>
    <input name="tanggal_lahir" type="date" class="form-control" id="tanggal_lahir" placeholder="Tanggal Lahir" required>
</div>
<div class="form-group">
    <label for="idstatuskawin">Status Kawin</label><br>
    <select name="idstatuskawin" id="idstatuskawin" class="form-control select-statuskawin"></select>
</div>
<div class="form-group">
    <label for="idpendidikan">Pendidikan</label><br>
    <select name="idpendidikan" id="idpendidikan" class="form-control select-pendidikan"></select>
</div>
<div class="form-group">
    <label for="idstatuskeluarga">Status Keluarga</label><br>
    <select name="idstatuskeluarga" id="idstatuskeluarga" class="form-control select-statuskeluarga"></select>
</div>
<div class="form-group">
    <label for="idstatusindividu">Status Individu</label><br>
    <select name="idstatusindividu" id="idstatusindividu" class="form-control select-statusindividu"></select>
</div>
