<div class="form-group">
    <label for="tanggal">Tanggal Razia</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal Razia" required>
</div>
<div class="form-group">
    <label for="iskota">
        <input name="iskota" type="checkbox" id="iskota" value="1" class="minimal">
        &nbsp;Kota
    </label>
</div>
<div class="form-group">
    <label for="jumlah">Jumlah</label>
    <input name="jumlah" type="number" id="jumlah" placeholder="Jumlah" class="form-control" required>
</div>
<div class="form-group">
    <label for="idkategorirazia">Kategori Razia</label><br>
    <select name="idkategorirazia" id="idkategorirazia" class="form-control select-kategorirazia"></select>
</div>
<div class="form-group">
    <label for="idjeniskelamin">Jenis Kelamin</label><br>
    <select name="idjeniskelamin" id="idjeniskelamin" class="form-control select-jeniskelamin"></select>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control select-kelurahan"></select>
</div>
<div class="form-group">
    <label for="location">Location </label>
    <div id="map" style="height:400px; width:100%;"></div>
    <input name="longitude" type="hidden" id="longitude" placeholder="longitude" class="form-control" required>
    <input name="latitude" type="hidden" id="latitude" placeholder="latitude" class="form-control" required>
</div>
