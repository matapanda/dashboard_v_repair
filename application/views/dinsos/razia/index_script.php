<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1Y4FlA5LzNm_spEk-NIyGlamjftmfA0U&libraries=places">
</script>

<script src="<?= base_url('assets/js/default-map.js') ?>">
</script>

<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "razia"},
            {"data": "jeniskelamin"},
            {"data": "jumlah"},
            {
                "data": "iskota",
                "render": function (data, type, row) {
                    if (row["iskota"] == 1) {
                        return '<span class="label label-info">Kota</span>'
                    }
                    return '<span class="label label-danger">Bukan Kota</span>'
                }
            },
            {"data": "kelurahan"},
            {"data": "latitude"},
            {"data": "longitude"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/razia/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idrazia;
            $('#tanggal').val(data_for_table.tanggal);
            if (data_for_table.iskota == 1) {
              $('#iskota').parents().addClass('checked');
            } else {
              $('#iskota').parents().removeClass('checked');
            }
            $('#jumlah').val(data_for_table.jumlah);
            $('#latitude').val(data_for_table.latitude);
            $('#longitude').val(data_for_table.longitude);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);
            fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$("#idjeniskelamin")], data_for_table);

            $("#idkategorirazia").empty();
            //get data kategorirazia
            $.ajax({
                url: "<?php echo base_url('/api/dinsos/razia/kategori-razia') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idkategorirazia").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idkategorirazia").append($("<option />").val(this.idkategorirazia).text(this.razia));
                    });
                    $('#idkategorirazia').val(data_for_table.idkategorirazia);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });

            $('#map').empty();
            var city = new google.maps.LatLng(data_for_table.latitude, data_for_table.longitude);
            initMap($(".form-update"), city);
            addMarker(city, 'Tempat Lama', true);
            $.getJSON({
                url: "<?=base_url('assets/js/kotamalang.geojson') ?>"
            }).done(function (data) {

              var kecamatan = {
                "nama" : [],
                "color" : []
              };

              $.each( data.features, function(key, value) {
                var index = kecamatan.nama.indexOf(value.properties.KECAMATAN);
                if ( index === -1 ) {
                  kecamatan.nama.push(value.properties.KECAMATAN);
                  var color = '#'+ Math.round(Math.random()*16777215).toString(16);
                  kecamatan.color.push(color);
                } else {
                  var color = kecamatan.color[index];
                }

                drawPolygonOverlay(value.geometry.coordinates[0], color, 0.2);
              });
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("gagal", "Gagal mengambil data overlay map", "error");
            })

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/razia/') ?>" + data.idrazia, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);
        fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$(".form-add #idjeniskelamin")]);

        $(".form-add #idkategorirazia").empty();
        //get data kategorirazia
        $.ajax({
            url: "<?php echo base_url('/api/dinsos/razia/kategori-razia') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idkategorirazia").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idkategorirazia").append($("<option />").val(this.idkategorirazia).text(this.razia));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });

        $('.form-add #map').empty();
        var city = new google.maps.LatLng(-7.996744, 112.6191631);
        initMap($(".form-add"), city);
        $.getJSON({
            url: "<?=base_url('assets/js/kotamalang.geojson') ?>"
        }).done(function (data) {

          var kecamatan = {
            "nama" : [],
            "color" : []
          };

          $.each( data.features, function(key, value) {
            var index = kecamatan.nama.indexOf(value.properties.KECAMATAN);
            if ( index === -1 ) {
              kecamatan.nama.push(value.properties.KECAMATAN);
              var color = '#'+ Math.round(Math.random()*16777215).toString(16);
              kecamatan.color.push(color);
            } else {
              var color = kecamatan.color[index];
            }

            drawPolygonOverlay(value.geometry.coordinates[0], color, 0.2);
          });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("gagal", "Gagal mengambil data overlay map", "error");
        })
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/razia') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/razia/') ?>" + data_id, datatables);
    }
</script>
