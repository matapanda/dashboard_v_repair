<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="nama">Nama</label>
    <input name="nama" type="text" id="nama" placeholder="Nama" class="form-control" required>
</div>
<div class="form-group">
    <label for="tanggal_lahir">Tanggal Lahir</label>
    <input name="tanggal_lahir" type="date" class="form-control" id="tanggal_lahir" placeholder="Tanggal Lahir" required>
</div>
<div class="form-group">
    <label for="nama_ppk">Nama PPK</label>
    <input name="nama_ppk" type="text" id="nama_ppk" placeholder="Nama PPK" class="form-control" required>
</div>
<div class="form-group">
    <label for="alamat">Alamat</label>
    <input name="alamat" type="text" id="alamat" placeholder="Alamat" class="form-control" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control"></select>
</div>