<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "nama"},
            {"data": "tanggal_lahir"},
            {"data": "nama_ppk"},
            {"data": "alamat"},
            {"data": "kelurahan"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/kispbid/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idkispbid;
            $('#tanggal').val(data_for_table.tanggal);
            $('#nama').val(data_for_table.nama);
            $('#tanggal_lahir').val(data_for_table.tanggal_lahir);
            $('#alamat').val(data_for_table.alamat);
            $('#nama_pmodal-editpk').val(data_for_table.nama_ppk);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);


        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/kispbid/') ?>" + data.idkispbid, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/kispbid/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/kispbid/') ?>" + data_id, datatables);
    }
</script>
