<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control form" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="alamat">Alamat</label>
    <input name="alamat" type="text" id="alamat" placeholder="Alamat" class="form-control" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="no_kk">Nomor KK</label>
    <input name="no_kk" type="number" id="no_kk" placeholder="Nomor KK" class="form-control" required>
</div>
<div class="form-group">
    <label for="nik">Nomor NIK</label>
    <input name="nik" type="number" id="nik" placeholder="Nomor NIK" class="form-control" required>
</div>
<div class="form-group">
    <label for="nama_anggota_keluarga">Nama Anggota Keluarga</label>
    <input name="nama_anggota_keluarga" type="text" id="nama_anggota_keluarga" placeholder="Nama Anggota Keluarga" class="form-control" required>
</div>
<div class="form-group">
    <label for="tanggal_lahir">Tanggal Lahir</label>
    <input name="tanggal_lahir" type="date" class="form-control form" id="tanggal_lahir" placeholder="Tanggal Lahir" required>
</div>
<table class="table text-center table-bordered">
    <tr>
        <th colspan="3">
            Status
        </th>
    </tr>
    <tr>
        <th>
           Status Lama
        </th>
        <th>
            Status Sekarang
        </th>
        <th>
            Status Keterangan
        </th>
    </tr>
    <tr>
        <td>
            <select name="status_lama" id="status_lama" class="form-control"></select>
        </td>
        <td>
            <select name="status_sekarang" id="status_sekarang" class="form-control"></select>
        </td>
        <td>
            <select name="status_keterangan" id="status_keterangan" class="form-control"></select>
        </td>
    </tr>
</table>
