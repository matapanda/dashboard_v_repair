<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "no_kk"},
            {"data": "nik"},
            {"data": "nama_anggota_keluarga"},
            {"data": "tanggal_lahir"},
            {"data": "alamat"},
            {"data": "kelurahan"},
            {"data": "status_status_lama"},
            {"data": "status_status_sekarang"},
            {"data": "status_status_keterangan"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/kispbin/datatable/') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idkispbin;
            $('#tanggal').val(data_for_table.tanggal);
            $('#alamat').val(data_for_table.alamat);
            $('#no_kk').val(data_for_table.no_kk);
            $('#nik').val(data_for_table.nik);
            $('#nama_anggota_keluarga').val(data_for_table.nama_anggota_keluarga);
            $('#tanggal_lahir').val(data_for_table.tanggal_lahir);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);
            fill_select_master('statuskis', "<?php echo base_url() ?>", [$("#status_lama"), $("#status_sekarang"), $("#status_keterangan")], data_for_table);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/kispbin/') ?>" + data.idkispbin, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);
        fill_select_master('statuskis', "<?php echo base_url() ?>", [$(".form-add #status_lama"), $(".form-add #status_sekarang"), $(".form-add #status_keterangan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/kispbin/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/kispbin/') ?>" + data_id, datatables);
    }
</script>
