<script>
    $(document).ready(function () {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")]);
        fill_select_master('statuskis', "<?php echo base_url() ?>", [$("#status_lama"), $("#status_sekarang"), $("#status_keterangan")]);
    });

    function save() {
        var fields = [
            $('#tanggal'),
            $('#alamat'),
            $('#no_kk'),
            $('#nik'),
            $('#nama_anggota_keluarga'),
            $('#tanggal_lahir')
        ];
        save_new_data("<?php echo base_url('/api/dinsos/kispbin') ?>", fields);
    }
</script>
