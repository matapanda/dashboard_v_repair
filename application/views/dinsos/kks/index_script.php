<script src="<?= base_url('assets/js/default-map.js') ?>">
</script>

<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "no_kks"},
            {"data": "tanggal"},
            {"data": "kelurahan"},
            {"data": "lagitude"},
            {"data": "latitude"},
            {"data": "pengurus"},
            {"data": "alamat"},
            {"data": "krt"},
            {"data": "pkrt"},
            {"data": "artl"},
            {"data": "jumlah_art"},
            {
                "data": "is_pkh",
                "render": function (data, type, row) {
                    if (row["is_pkh"] == 1) {
                        return '<span class="label label-info">PKH</span>'
                    }
                    return '<span class="label label-danger">Bukan PKH</span>'
                }
            },
            {
                "data": "is_pbdt",
                "render": function (data, type, row) {
                    if (row["is_pbdt"] == 1) {
                        return '<span class="label label-info">PDTT</span>'
                    }
                    return '<span class="label label-danger">Bukan PDTT</span>'
                }
            },
            {"data": "no_pkh"},
            {"data": "no_pbdt"},
            {
                "data": "is_batal_kks",
                "render": function (data, type, row) {
                    if (row["is_batal_kks"] == 1) {
                        return '<span class="label label-info">Batal KKS</span>'
                    }
                    return '<span class="label label-danger">Bukan Batal KKS</span>'
                }
            },
            {
                "data": "is_pengajuan_pkh",
                "render": function (data, type, row) {
                    if (row["is_pengajuan_pkh"] == 1) {
                        return '<span class="label label-info">Pengajuan PKH</span>'
                    }
                    return '<span class="label label-danger">Bukan Pengajuan PKH</span>'
                }
            },
            {"data": "username"},
            {"data": null}
        ];

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/kks/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();
            data_id = data_for_table.idkks;
            $('#tanggal').val(data_for_table.tanggal);
            $('#idkelurahan').val(data_for_table.idkelurahan);
            $('#longitude').val(data_for_table.lagitude);
            $('#latitude').val(data_for_table.latitude);
            $('#no_kks').val(data_for_table.no_kks);
            $('#pengurus').val(data_for_table.pengurus);
            $('#alamat').val(data_for_table.alamat);
            $('#krt').val(data_for_table.krt);
            $('#pkrt').val(data_for_table.pkrt);
            $('#artl').val(data_for_table.artl);
            $('#jumlah_art').val(data_for_table.jumlah_art);
            $('#no_pkh').val(data_for_table.no_pbdt);
            $('#no_pbdt').val(data_for_table.no_pbdt);

            if (data_for_table.is_pkh == 1) {
                $('#is_pkh').parents().addClass('checked');
            } else {
                $('#is_pkh').parents().removeClass('checked');
            }
            if (data_for_table.is_pbdt == 1) {
                $('#is_pbdt').parents().addClass('checked');
            } else {
                $('#is_pbdt').parents().removeClass('checked');
            }
            if (data_for_table.is_batal_kks == 1) {
                $('#is_batal_kks').parents().addClass('checked');
            } else {
                $('#is_batal_kks').parents().removeClass('checked');
            }
            if (data_for_table.is_pengajuan_pkh == 1) {
                $('#is_pengajuan_pkh').parents().addClass('checked');
            } else {
                $('#is_pengajuan_pkh').parents().removeClass('checked');
            }

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);

            $('.form-update #map').empty();
            var city = new google.maps.LatLng(data_for_table.latitude, data_for_table.lagitude);
            initMap($(".form-update"), city);
            addMarker(city, 'Tempat Lama', true);
            $.getJSON({
                url: "<?=base_url('assets/js/kotamalang.geojson') ?>"
            }).done(function (data) {

              var kecamatan = {
                "nama" : [],
                "color" : []
              };

              $.each( data.features, function(key, value) {
                var index = kecamatan.nama.indexOf(value.properties.KECAMATAN);
                if ( index === -1 ) {
                  kecamatan.nama.push(value.properties.KECAMATAN);
                  var color = '#'+ Math.round(Math.random()*16777215).toString(16);
                  kecamatan.color.push(color);
                } else {
                  var color = kecamatan.color[index];
                }

                drawPolygonOverlay(value.geometry.coordinates[0], color, 0.2);
              });
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("gagal", "Gagal mengambil data overlay map", "error");
            })

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/kks/') ?>" + data.idkks, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);
        $('.form-add #map').empty();
        var city = new google.maps.LatLng(-7.996744, 112.6191631);
        initMap($(".form-add"), city);
        $.getJSON({
            url: "<?=base_url('assets/js/kotamalang.geojson') ?>"
        }).done(function (data) {

          var kecamatan = {
            "nama" : [],
            "color" : []
          };

          $.each( data.features, function(key, value) {
            var index = kecamatan.nama.indexOf(value.properties.KECAMATAN);
            if ( index === -1 ) {
              kecamatan.nama.push(value.properties.KECAMATAN);
              var color = '#'+ Math.round(Math.random()*16777215).toString(16);
              kecamatan.color.push(color);
            } else {
              var color = kecamatan.color[index];
            }

            drawPolygonOverlay(value.geometry.coordinates[0], color, 0.2);
          });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("gagal", "Gagal mengambil data overlay map", "error");
        })
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/kks/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/kks/') ?>" + data_id, datatables);
    }
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1Y4FlA5LzNm_spEk-NIyGlamjftmfA0U&libraries=places">
</script>
