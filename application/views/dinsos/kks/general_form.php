<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control select-kelurahan"></select>
</div>
<div class="form-group">
    <label for="no_kks">Nomor KKS</label>
    <input name="no_kks" type="text" id="no_kks" placeholder="Nomor KKS" class="form-control" required>
</div>
<div class="form-group">
    <label for="pengurus">Nama Pengurus</label>
    <input name="pengurus" type="text" id="pengurus" placeholder="Nama Pengurus" class="form-control" required>
</div>
<div class="form-group">
    <label for="alamat">Alamat</label>
    <input name="alamat" type="text" id="alamat" placeholder="Alamat" class="form-control" required>
</div>
<div class="form-group">
    <label for="krt">Nama Kepala Rumah Tangga</label>
    <input name="krt" type="text" id="krt" placeholder="Nama Kepala Rumah Tangga" class="form-control" required>
</div>
<div class="form-group">
    <label for="pkrt">Nama Pembantu Rumah Tangga</label>
    <input name="pkrt" type="text" id="pkrt" placeholder="Nama Pembantu Rumah Tangga" class="form-control" required>
</div>
<div class="form-group">
    <label for="artl">Anggota Rumah Tangga Lain</label>
    <input name="artl" type="text" id="artl" placeholder="Anggota Rumah Tangga Lain" class="form-control" required>
</div>
<div class="form-group">
    <label for="jumlah_art">Jumlah Anggota Keluarga</label>
    <input name="jumlah_art" type="number" id="jumlah_art" placeholder="Jumlah Anggota Keluarga" class="form-control"
           required>
</div>
<div class="form-group">
    <label for="no_pbdt">Nomor PKH</label>
    <input name="no_pkh" type="number" id="no_pkh" placeholder="Nomor PKH" class="form-control" required>
</div>
<div class="form-group">
    <label for="no_pbdt">Nomor PBDT</label>
    <input name="no_pbdt" type="number" id="no_pbdt" placeholder="Nomor PBDT" class="form-control" required>
</div>
<div class="form-group">
    <label for="is_pkh" class="checkbox-inline">
        <input name="is_pkh" type="checkbox" id="is_pkh" value="1" class="minimal"> PKH
    </label>
    <label for="is_pbdt" class="checkbox-inline">
        <input name="is_pbdt" type="checkbox" id="is_pbdt" value="1" class="minimal"> PBDT
    </label>
    <label for="is_batal_kks" class="checkbox-inline">
        <input name="is_batal_kks" type="checkbox" id="is_batal_kks" value="1" class="minimal"> Batal KKS
    </label>
    <label for="is_pengajuan_pkh" class="checkbox-inline">
        <input name="is_pengajuan_pkh" type="checkbox" id="is_pengajuan_pkh" value="1" class="minimal"> Pengajuan PKH
    </label>
</div>
<div class="form-group">
    <label for="Location">Location </label>
    <div id="map" style="height:400px ; width:100%;"></div>
    <input name="longitude" type="hidden" id="longitude" placeholder="Longitude" class="form-control" required>
    <input name="latitude" type="hidden" id="latitude" placeholder="Latitude" class="form-control" required>
</div>
