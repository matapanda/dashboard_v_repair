<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns =  [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kategori"},
            {"data": "jumlah"},
            {"data": "kelurahan"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/resos/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idresos;
            $('#tanggal').val(data_for_table.tanggal);
            $('#jumlah').val(data_for_table.jumlah);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);

            $("#idkategoriresos").empty();

            //get data kategori resos
            $.ajax({
                url: "<?php echo base_url('api/dinsos/resos/kategori-resos') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if(data.success){
                    $('#modal-edit select').css('width', '100%');
                    $("#idkategoriresos").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function() {
                        $("#idkategoriresos").append($("<option />").val(this.idkategoriresos).text(this.kategori));
                    });
                    $('#idkategoriresos').val(data_for_table.idkategoriresos);
                }else{
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan", "error");
            });


        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/resos/') ?>"+data.idresos, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);

        $(".form-add #idkategoriresos").empty();

        //get data kategori resos
        $.ajax({
            url: "<?php echo base_url('api/dinsos/resos/kategori-resos') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if(data.success){
                $('#modal-add select').css('width', '100%');
                $(".form-add #idkategoriresos").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function() {
                    $(".form-add #idkategoriresos").append($("<option />").val(this.idkategoriresos).text(this.kategori));
                });
            }else{
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan", "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/resos') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/resos/') ?>"+data_id, datatables);
    }
</script>
