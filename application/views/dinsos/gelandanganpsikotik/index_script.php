<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns =  [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "jumlah"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinsos/gelandangan-psikotik/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            data_id = data.idgelandanganpsikotik;

            $('#tanggal').val(data.tanggal);
            $('#jumlah').val(data.jumlah);
        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinsos/gelandangan-psikotik/') ?>"+data.idgelandanganpsikotik, datatables);
        });
    });

    function showAdd(){
        //nothing
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinsos/gelandangan-psikotik/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinsos/gelandangan-psikotik/') ?>"+data_id, datatables);
    }
</script>