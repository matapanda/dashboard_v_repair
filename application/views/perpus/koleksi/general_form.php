<div class="form-group">
    <label for="idbahasa">Bahasa</label><br>
    <select name="idbahasa" id="idbahasa" class="form-control"></select>
</div>
<div class="form-group">
    <label for="idjeniskoleksi">Jenis Koleksi</label><br>
    <select name="idjeniskoleksi" id="idjeniskoleksi" class="form-control"></select>
</div>
<div class="form-group">
    <label for="idkelas">Kelas</label><br>
    <select name="idkelas" id="idkelas" class="form-control"></select>
</div>
<div class="form-group">
    <label for="judul">Judul</label>
    <input name="judul" type="number" id="judul" placeholder="Judul" class="form-control" required>
</div>
<div class="form-group">
    <label for="eksemplar">Eksemplar</label>
    <input name="eksemplar" type="number" id="eksemplar" placeholder="Eksemplar" class="form-control" required>
</div>
<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>