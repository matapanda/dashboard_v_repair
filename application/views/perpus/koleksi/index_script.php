<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "bahasa"},
            {"data": "jeniskoleksi"},
            {"data": "kelas"},
            {"data": "judul"},
            {"data": "eksemplar"},
            {"data": "tanggal"},
            {"data": "username"},
            {"data": "systemtime"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/perpus/rekapitulasi-koleksi/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idkoleksi;

            $("#idbahasa").empty();
            //get data bahasa
            $.ajax({
                url: "<?php echo base_url('api/perpus/master/bahasa') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idbahasa").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idbahasa").append($("<option />").val(this.idbahasa).text(this.bahasa));
                    });
                    $('#idbahasa').val(data_for_table.idbahasa);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data bahasa", "error");
            });

            $("#idjeniskoleksi").empty();

            //get data jeniskoleksi
            $.ajax({
                url: "<?php echo base_url('api/perpus/master/jenis-koleksi') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idjeniskoleksi").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idjeniskoleksi").append($("<option />").val(this.idjeniskoleksi).text(this.jeniskoleksi));
                    });
                    $('#idjeniskoleksi').val(data_for_table.idjeniskoleksi);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data jenis koleksi", "error");
            });

            $("#idkelas").empty();

            //get data kelas
            $.ajax({
                url: "<?php echo base_url('api/perpus/master/kelas') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idkelas").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idkelas").append($("<option />").val(this.idkelas).text(this.kelas));
                    });
                    $('#idkelas').val(data_for_table.idkelas);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data kelas", "error");
            });

            $('#judul').val(data_for_table.judul);
            $('#eksemplar').val(data_for_table.eksemplar);
            $('#tanggal').val(data_for_table.tanggal);

            
        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/perpus/rekapitulasi-koleksi/') ?>"+data.idkoleksi, datatables);
        });
    });

    function showAdd(){
        $(".form-add #idbahasa").empty();
        //get data bahasa
        $.ajax({
            url: "<?php echo base_url('api/perpus/master/bahasa') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idbahasa").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idbahasa").append($("<option />").val(this.idbahasa).text(this.bahasa));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data bahasa", "error");
        });

        $(".form-add #idjeniskoleksi").empty();

        //get data jeniskoleksi
        $.ajax({
            url: "<?php echo base_url('api/perpus/master/jenis-koleksi') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idjeniskoleksi").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idjeniskoleksi").append($("<option />").val(this.idjeniskoleksi).text(this.jeniskoleksi));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data jenis koleksi", "error");
        });

        $(".form-add #idkelas").empty();

        //get data kelas
        $.ajax({
            url: "<?php echo base_url('api/perpus/master/kelas') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idkelas").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idkelas").append($("<option />").val(this.idkelas).text(this.kelas));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data kelas", "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('/api/perpus/rekapitulasi-koleksi') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/perpus/rekapitulasi-koleksi/') ?>"+data_id, datatables);
    }
</script>