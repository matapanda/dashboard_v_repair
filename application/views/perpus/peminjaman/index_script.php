<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kelas"},
            {"data": "jeniskoleksi"},
            {"data": "jumlah"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/perpus/statistik-peminjaman/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idstatistikpeminjaman;

            $("#idjeniskoleksi").empty();
            $("#idkelas").empty();

            //get data jeniskoleksi
            $.ajax({
                url: "<?php echo base_url('api/perpus/master/jenis-koleksi') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idjeniskoleksi").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idjeniskoleksi").append($("<option />").val(this.idjeniskoleksi).text(this.jeniskoleksi));
                    });
                    $('#idjeniskoleksi').val(data_for_table.idjeniskoleksi);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data jenis koleksi", "error");
            });

            //get data kelas
            $.ajax({
                url: "<?php echo base_url('api/perpus/master/kelas') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idkelas").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idkelas").append($("<option />").val(this.idkelas).text(this.kelas));
                    });
                    $('#idkelas').val(data_for_table.idkelas);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data kelas", "error");
            });

            $('#judul').val(data_for_table.judul);
            $('#eksemplar').val(data_for_table.eksemplar);
            $('#tanggal').val(data_for_table.tanggal);
            $('#jumlah').val(data_for_table.jumlah);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/perpus/statistik-peminjaman/') ?>"+data.idstatistikpeminjaman, datatables);
        });
    });

    function showAdd(){
        $(".form-add #idjeniskoleksi").empty();
        $(".form-add #idkelas").empty();

        //get data jeniskoleksi
        $.ajax({
            url: "<?php echo base_url('api/perpus/master/jenis-koleksi') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idjeniskoleksi").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idjeniskoleksi").append($("<option />").val(this.idjeniskoleksi).text(this.jeniskoleksi));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data jenis koleksi", "error");
        });

        //get data kelas
        $.ajax({
            url: "<?php echo base_url('api/perpus/master/kelas') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idkelas").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idkelas").append($("<option />").val(this.idkelas).text(this.kelas));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data kelas", "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('/api/perpus/statistik-peminjaman') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/perpus/statistik-peminjaman/') ?>"+data_id, datatables);
    }
</script>