<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="idkelas">Kelas</label><br>
    <select name="idkelas" id="idkelas" class="form-control"></select>
</div>
<div class="form-group">
    <label for="idjeniskoleksi">Jenis Koleksi</label><br>
    <select name="idjeniskoleksi" id="idjeniskoleksi" class="form-control"></select>
</div>
<div class="form-group">
    <label for="jumlah">Jumlah</label><br>
    <input type="number" name="jumlah" id="jumlah" class="form-control" placeholder="Jumlah">
</div>