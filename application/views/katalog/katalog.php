<?php
function item_template($video_file, $thumbnail_file, $text_title, $text_description)
{
    ?>
    <div class="column is-6">
        <div class="card">
            <div class="card-image">
                <video class="video-katalog" loop onclick="toggle_video(this)">
                    <source src="<?php echo base_url("/assets/videos/" . $video_file); ?>" type="video/mp4">
                    <source src="<?php echo base_url("/assets/videos/" . $video_file); ?>" type="video/mp4">
                    Browser tidak mensupport pemutaran vidio.
                </video>
            </div>
            <div class="card-content">
                <div class="media">
                    <div class="media-left">
                        <figure class="image is-48x48">
                            <img src="<?php echo base_url("/assets/images/" . $thumbnail_file); ?>" alt="Image">
                        </figure>
                    </div>
                    <div class="media-content">
                        <p class="title is-4" style="cursor: pointer"
                           onclick="show_modal('<?php echo $text_title ?>', '<?php echo $video_file ?>')"><?php echo $text_title ?></p>
                    </div>
                </div>

                <div class="content">
                    <?php echo $text_description ?>
                </div>
                <button class="button is-primary"
                        onclick="show_modal('<?php echo $text_title ?>', '<?php echo $video_file ?>')">Selengkapnya
                </button>
            </div>
        </div>
    </div>
    <?php
}

?>
<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bulma.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">

    <style>
        html, body {
            background-color: #FBFBFB
        }

        .modal-open {
            overflow: hidden;
        }

        .video-modal {
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            background-size: cover;
            overflow: hidden;
            object-fit: cover;
        }

        #main-content {
            margin-top: 20px;
        }

        #video-primary {
            margin-left: auto;
            margin-right: auto;
            display: block;
            width: 700px;
        }

        .footer {
            margin-top: 30px;
        }

        #sound-control {
            position: fixed;
            right: 0;
            bottom: 0;
            z-index: 1000;
        }
    </style>

</head>
<body>
<div id="sound-control">
    <button id="sound-off" onclick="sound_toggle()" class="button is-danger" title="Mute"><span
                class="fa fa-volume-down"></span></button>
    <button id="sound-on" onclick="sound_toggle()" class="button is-primary" title="Unmute"><span
                class="fa fa-volume-up"></span></button>
</div>
<!--start modal item katalog-->
<div id="modal-katalog" class="modal">
    <div class="modal-background" onclick="close_modal()">
        <video class="video-modal" loop>
            <source src="<?php echo base_url("/assets/videos/BUMPER.mp4"); ?>" type="video/mp4">
            Browser tidak mensupport pemutaran vidio.
        </video>
    </div>
    <div class="modal-content"  style="width: 75%">
        <div id="modal-content-cover">
            <div class="has-text-centered">
                <button class="button is-white is-outlined is-medium" title="Hide Content"
                        onclick="toggle_modal_content()"><span class="fa fa-eye"></span>&nbsp;Tampilkan Katalog
                </button>
            </div>
        </div>
        <div id="modal-content-content" class="box" style="opacity: 0.98">
            <button class="delete is-pulled-right" title="Close Modal" onclick="close_modal()"></button>
            <h1 class="title is-3 modal-title">Judul</h1>
            <div id="katalog-table-wrapper"></div>
            <div class="has-text-centered">
                <button class="button is-primary" title="Hide Content" onclick="toggle_modal_content()"><span
                            class="fa fa-eye-slash"></span>&nbsp;Sembunyikan Katalog
                </button>
            </div>
        </div>
    </div>
</div>
<!--end modal-->
<!--start modal item search-->
<div id="modal-search" class="modal">
    <div class="modal-background" onclick="close_modal()" style="opacity: 0.9"></div>
    <div class="modal-content" style="width: 85%">
        <div class="box">
            <button class="delete is-pulled-right" title="Close Modal" onclick="close_modal()"></button>
            <h1 class="title is-3 modal-title">Hasil Pencarian "<span id="modal-keyword">-</span>"</h1>
            <div id="search-table-wrapper"></div>
        </div>
    </div>
</div>
<!--end modal-->

<nav class="nav has-shadow">
    <div class="container">
        <div class="nav-left">
            <a href="<?php echo base_url() ?>" class="nav-item">
                <img src="<?php echo base_url("/assets/images/ncc-logo.png"); ?>" alt="NCC logo">
            </a>
            <a class="nav-item is-tab is-hidden-mobile is-active">Katalog</a>
        </div>
        </span>
    </div>
</nav>

<section class="hero is-primary is-medium">
    <div class="hero-body">
        <div class="container">
            <div class="is-pulled-left">
                <h1 class="title">
                    NCC Katalog Menu
                </h1>
                <hr style="width: 40%">
                <h2 class="subtitle">
                    Katalog menu NCC Kota Malang
                </h2>
                <br>
                <div id="search-container" class="field has-addons">
                    <p class="control has-icons-left">
                        <input id="input-search" class="input is-medium" type="text" placeholder="Cari katalog" on>
                        <span class="icon is-left">
                          <i class="fa fa-list"></i>
                        </span>
                    </p>
                    <p class="control">
                        <button class="button is-primary is-outlined is-inverted is-medium" onclick="search()">
                            <span class="icon"><i class="fa fa-search"></i></span>
                        </button>
                    </p>
                </div>
            </div>
            <video class="card is-pulled-right" id="video-primary" loop onclick="toggle_video(this)">
                <source src="<?php echo base_url("/assets/videos/BUMPER.mp4"); ?>" type="video/mp4">
                <source src="<?php echo base_url("/assets/videos/BUMPER.mp4"); ?>" type="video/mp4">
                Browser tidak mensupport pemutaran vidio.
            </video>
        </div>
    </div>
</section>

<div class="container" id="main-content">
    <div class="columns is-multiline">
        <?php
        for ($i = 0; $i < count($list_text_title); $i++) {
            item_template($list_videos[$i], $list_thumbnails[$i], $list_text_title[$i], $list_description[$i]);
        }
        ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="content has-text-centered">
            <p>
                © <?php echo date("Y") ?> NCC. All rights reserved.<br>
                <strong>NCC</strong> by <a href="#">Pemerintah Kota Malang</a>.
            </p>
            <br>
            <br>
            <p>
                <img width="200px" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>" alt="NCC logo">
            </p>
        </div>
    </div>
</footer>
</body>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>

<script>
    var $video_modal = $('.video-modal');
    var $modal = $('.modal');
    var $modal_katalog = $('#modal-katalog');
    var $modal_search = $('#modal-search');
    var $modal_content = $('#modal-content-content');
    var $modal_content_cover = $('#modal-content-cover');
    var $modal_title = $('.modal-title');
    var $video = $('video');
    var sound = true;
    var modal_content_show = false;

    $(function () {
        //loading screen
        jQuery(document).ajaxStart(function () {
            $('body').loading({
                message: 'Proses...'
            });
        });
        jQuery(document).ajaxStop(function () {
            $('body').loading('stop');
        });

        $('#sound-on').hide();
        $modal_content_cover.hide();
    });

    function toggle_video(vid) {
        if (vid.paused) {
            vid.play();
            return;
        }
        vid.pause();
    }

    function sound_toggle() {
        sound = !sound;
        if (sound) {
            $('#sound-on').hide();
            $('#sound-off').show();

            $video.prop('muted', false)
        } else {
            $('#sound-on').show();
            $('#sound-off').hide();

            $video.prop('muted', true)
        }
    }

    function show_modal(title, video) {
        var $katalog_table_wrapper = $('#katalog-table-wrapper');
        $video_modal.empty();
        $video_modal.html('<source src="<?php echo base_url("/assets/videos"); ?>/' + video + '" type="video/mp4">');
        $modal_title.html(title);
        $video_modal.get(0).load();
        $video_modal.get(0).play();

        var api_url = '<?php echo base_url('api/app/katalog/0/') ?>';
        switch (title) {
            case 'Pelayanan':
                api_url += '1';
                break;
            case 'Administrasi & Manajemen':
                api_url += '2';
                break;
            case 'Legislasi':
                api_url += '3';
                break;
            case 'Pembangunan':
                api_url += '4';
                break;
            case 'Keuangan':
                api_url += '5';
                break;
            case 'Kepegawaian':
                api_url += '6';
                break;
            case 'Dinas & Lembaga':
                api_url += '7';
                break;
            case 'Kewilayahan':
                api_url += '8';
                break;
            case 'Kemasyarakatan':
                api_url += '9';
                break;
            case 'Sarana Prasarana':
                api_url += '10';
                break;
            case 'Media Center':
                api_url += '11';
                break;
            case 'Pendidikan':
                api_url += '12';
                break;
            case 'BP2D':
                api_url += '13';
                break;
        }

        $.ajax({
            url: api_url,
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            var final_datas = data.data;
            console.log(final_datas);
            if (final_datas.length > 0) {
                set_table_katalog_to_default();
                var $katalog_item = $('#katalog-item');
                var number = 1;
                final_datas.forEach(function (item) {
                    var tipe = '-';
                    switch (item.idtipe) {
                        case '1':
                            tipe = '<span class="icon"><i class="fa fa-bar-chart"></i></span>';
                            break;
                        case '2':
                            tipe = '<span class="icon"><i class="fa fa-map-marker"></i></span>';
                            break;
                    }
                    $katalog_item.append('' +
                        '<tr>' +
                        '<td>' + number + '</td>' +
                        '<td>' + item.judul + '</td>' +
                        '<td>' + item.deskripsi + '</td>' +
                        '<td>' + tipe + '</td>' +
                        '<td><a href="' + item.url + '" target="_blank">Tautan</a></td>' +
                        '</tr>'
                    );
                    number++;
                    $("html").css("overflow", "hidden");
                    $modal_katalog.addClass('is-active');
                })
            } else {
                console.log('else');
                $katalog_table_wrapper.html('<div class="notification is-info" style="margin-bottom: 20px">Belum ada data terkait kategori ini</div>');
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        })

    }

    function set_table_katalog_to_default() {
        var $katalog_table_wrapper = $('#katalog-table-wrapper');
        $katalog_table_wrapper.html('' +
            '<table id="katalog-table" class="table is-bordered is-striped">' +
            '<thead>' +
            '<tr>' +
            '<th>#</th>' +
            '<th>Judul</th>' +
            '<th>Deskripsi</th>' +
            '<th>Tipe</th>' +
            '<th>Tautan</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="katalog-item">' +
            '</tbody>' +
            '</table>'
        );
    }

    function close_modal() {
        $modal_content_cover.hide();
        modal_content_show = false;
        $('html').css('overflow', 'auto');
        $modal.removeClass('is-active');
        $video_modal.get(0).pause();
        $video_modal.get(0).currentTime = 0;
        $modal_content.show();
    }

    function toggle_modal_content() {
        modal_content_show = !modal_content_show;
        if (modal_content_show) {
            $modal_content.slideUp();
            $modal_content_cover.show();
        } else {
            $modal_content.slideDown();
            $modal_content_cover.hide();
        }
    }

    $("#input-search").keyup(function(event){
        if(event.keyCode == 13){
            search();
        }
    });

    function search() {
        var $input_search = $('#input-search');
        var api_url = '<?php echo base_url('api/app/katalog/0/0/0/') ?>' + $input_search.val();


        $.ajax({
            url: api_url,
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            var final_datas = data.data;
            console.log(final_datas);
            if (final_datas.length > 0) {
                set_table_search_to_default();
                $('#modal-keyword').html($input_search.val());
                var $search_item = $('#search-item');
                var number = 1;
                final_datas.forEach(function (item) {
                    var tipe = '-';
                    switch (item.idtipe) {
                        case '1':
                            tipe = '<span class="icon"><i class="fa fa-bar-chart"></i></span>';
                            break;
                        case '2':
                            tipe = '<span class="icon"><i class="fa fa-map-marker"></i></span>';
                            break;
                    }
                    $search_item.append('' +
                        '<tr>' +
                        '<td>' + number + '</td>' +
                        '<td>' + item.judul + '</td>' +
                        '<td>' + item.deskripsi + '</td>' +
                        '<td>' + tipe + '</td>' +
                        '<td>' + item.kategori + '</td>' +
                        '<td><a href="' + item.url + '" target="_blank">Tautan</a></td>' +
                        '</tr>'
                    );
                    number++;
                });
                $('html').css('overflow', 'hidden');
                $modal_search.addClass('is-active');
            } else {
                console.log('else');
                sweetAlert("Tidak Ditemukan", 'Katalog dengan kata kunci "' + $input_search.val() + '" tidak ditemukan', "error");

            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        })
    }

    function set_table_search_to_default() {
        var $search_table_wrapper = $('#search-table-wrapper');
        $search_table_wrapper.html('' +
            '<table id="search-table" class="table is-bordered is-striped">' +
            '<thead>' +
            '<tr>' +
            '<th>#</th>' +
            '<th>Judul</th>' +
            '<th>Deskripsi</th>' +
            '<th>Tipe</th>' +
            '<th>Kategori</th>' +
            '<th>Tautan</th>' +
            '</tr>' +
            '</thead>' +
            '<tbody id="search-item">' +
            '</tbody>' +
            '</table>'
        );
    }

</script>
</html>
