<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?= base_url('/assets/images/favicon.ico') ?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bulma.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

    <style>
        html, body {
            background-color: #FBFBFB;
            width: 100%;
            height: 100%;
        }

        /*scrollbar*/
        .scroll-style::-webkit-scrollbar-track {
            background-color: #00D9B3;
        }

        .scroll-style::-webkit-scrollbar {
            width: 2px;
            height: 5px;
            background-color: #FBFBFB;
        }

        .scroll-style::-webkit-scrollbar-thumb {
            background-color: #FBFBFB;
        }

        /*end of scrollbar*/

        #video-primary {
            margin-left: auto;
            margin-right: auto;
            display: block;
            width: 700px;
        }

        #sound-control {
            position: fixed;
            right: 0;
            bottom: 0;
            z-index: 1000;
        }

        .dark-text {
            color: #2B2B2B !important;
        }

        #card-chart-detail {
            font-size: 14px;
        }

        .modal-background {
            opacity: 0.6;
        }
    </style>

</head>
<body>
<img class="background-ncc-logo-full" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>"
     alt="NCC logo">
<div id="sound-control">
    <button id="sound-off" onclick="sound_toggle()" class="button is-danger" title="Mute"><span
                class="fa fa-volume-down"></span></button>
    <button id="sound-on" onclick="sound_toggle()" class="button is-white" title="Unmute"><span
                class="fa fa-volume-up"></span></button>
</div>

<section class="hero is-primary is-medium dark-text">
    <div class="hero-body" style="padding-top: 30px; padding-bottom: 30px; width: 100%; height: 100%">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <video class="card" id="video-primary" loop muted autoplay onclick="toggle_video(this)"
                           style="margin-bottom: 40px">
                        <source src="<?php echo base_url("/assets/videos/" . $video); ?>" type="video/mp4">
                        <source src="<?php echo base_url("/assets/videos/" . $video); ?>" type="video/mp4">
                        Browser tidak mensupport pemutaran vidio.
                    </video>
                    <div id="desc-katalog">
                        <h1 class="title dark-text">
                            NCC Katalog <?php echo $text_title ?>
                        </h1>
                        <hr style="width: 40%">
                        <h2 class="subtitle dark-text">
                            <?php echo $text_description ?>
                        </h2>

                    </div>
                    <?php if ($id == 0) { ?>
                        <div id="desc-esambat">
                            <h1 class="title dark-text">
                                <b>E-Sambat</b>
                            </h1>
                            <hr style="width: 40%">
                            <h2 class="subtitle dark-text" style="font-size: 16px">
                                E-sambat adalah Sistem aplikasi masyarakat Bertanya Terpadu Online Pemerintah Kota
                                Malang.<br>
                                <br>
                                Bagi anda yang memiliki saran, kritik, pengaduan atau pertanyaan sampaikan kepada kami.
                                Semua pengaduan yang masuk baik melalui website atau sms akan tercatat dalam database.
                                Anda tidak perlu khawatir pengaduan anda akan hilang. Kami pastikan pengaduan anda akan
                                sampai kepada pihak yang terkait.
                            </h2>
                        </div>
                    <?php } ?>
                    <div style="margin-top: 10px">
                        <a class="button is-white  " href="<?php echo base_url('dashboard') ?>"><span
                                    class="fa fa-dashboard"></span>&nbsp;Dashboard</a>
                        <?php if ($id == 0) { ?>
                            <button id="button-esambat" class="button is-white  "
                                    onclick="toggle_esambat()">
                                <span class="fa fa-comments"></span>&nbsp;E-Sambat
                            </button>
                        <?php } ?>
                    </div>
                </div>
                <div class="column" id="katalog-table-wrapper" style="height: 100%; overflow: auto;">
                    <?php if (count($list_katalog) >= 0) { ?>
                        <table class="table is-striped box" style="font-size: 14px">
                            <thead>
                            <tr>
                                <th colspan="6" style="text-align: center">Katalog</th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Tipe</th>
                                <th>Tautan</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                             #add surya
                             $no_spm = 0;
                             #add surya
                             
                             foreach ($list_katalog as $katalog) { ?>
                                <tr>
                                    <td><?php echo $katalog['no'] ?></td>
                                    <td><?php echo $katalog['judul'] ?></td>
                                    <td><?php echo $katalog['deskripsi'] ?></td>
                                    <td>
                                        <?php if ($katalog['idtipe'] == 1) { ?>
                                            <span title="Grafik" class="fa fa-bar-chart"></span>
                                        <?php } else { ?>
                                            <span title="Peta" class="fa fa-map-marker"></span>
                                        <?php } ?>
                                    </td>
                                    <td><a target="_blank" class="button is-link"
                                           href="<?php echo base_url($katalog['url']) ?>">Tautan</a></td>
                                    <td>
                                        <button onclick="remove_katalog('<?php echo $katalog['delete_url'] ?>')"
                                                title="Hapus Katalog" class="button is-danger is-small"><span
                                                    class="fa fa-trash"></span></button>
                                    </td>
                                </tr>
                            <?php 
                            #add surya
                            $no_spm = $katalog['no'];
                            #add surya
                            
                            } ?>
                            
                            <?php
                            #add surya
                                if($id == 8){?>
                                    <tr>
                                    <td><?php echo $no_spm; ?></td>
                                    <td><?php echo "Grafil Dinas Sosial Surat Pernyataan Miskin Elektronik"; ?></td>
                                    <td><?php echo "Informasi Tentang Grafik Pengoahan Surat Pernyataan Miskin Elektronik"; ?></td>
                                    <td>
                                            <span title="Grafik" class="fa fa-bar-chart"></span>
                                    </td>
                                    <td><a target="_blank" class="button is-link"
                                           href="<?php echo base_url("spm/"); ?>">Tautan</a></td>
                                    <td>
                                        <button onclick=""
                                                title="Hapus Katalog" class="button is-danger is-small"><span
                                                    class="fa fa-trash"></span></button>
                                    </td>
                                </tr>
                            <?php    }
                            #add surya
                            ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="notification is-info">
                            Belum ada data untuk kategori <?php echo $text_title ?>
                        </div>
                    <?php } ?>
                </div>
                <?php if ($id == 0) { ?>
                    <!--                    MODAL-->

                    <div id="modal-esambat" class="modal">
                        <div class="modal-background"></div>
                        <div id="modal-esambat-content" class="modal-content" style="width: 85vw; height: 90vh">
                            <!-- Any other Bulma elements you want -->
                        </div>
                        <button class="modal-close is-large" aria-label="close"></button>
                    </div>

                    <div id="esambat-wrapper" class="column is-8 esambat-item">
                        <div id="esambat-menu" class="has-text-centered">
                            <button id="button-esambat-detail-auto" class="button is-white  "
                                    onclick="show_esambat_menu('auto')">
                                <span class="fa fa-home"></span>&nbsp;Umum
                            </button>
                            <button id="button-esambat-detail-web" class="button is-white  "
                                    onclick="show_esambat_menu('web')">
                                <span class="fa fa-globe"></span>&nbsp;Sambat Web
                            </button>
                            <button id="button-esambat-detail-sms" class="button is-white  "
                                    onclick="show_esambat_menu('sms')">
                                <span class="fa fa-mobile"></span>&nbsp;Sambat SMS
                            </button>
                            <button id="button-esambat-chart" class="button is-white  "
                                    onclick="show_esambat_menu('chart')">
                                <span class="fa fa-bar-chart"></span>&nbsp;Grafik
                            </button>
                        </div>
                        <hr>
                        <div class=" scroll-style" style="height: 80%; overflow: auto;">
                            <div id="esambat-detail-auto">
                                <div id="esambat-detail-auto-control">
                                    <button class="button is-small is-white  "
                                            onclick="auto_change('prev')">
                                        <span class="fa fa-chevron-left"></span>
                                    </button>
                                    <button class="button is-small is-white  "
                                            onclick="toggle_auto_play()">
                                        <span id="auto-play-button" class="fa fa-pause"></span>
                                    </button>
                                    <button class="button is-small is-white  "
                                            onclick="auto_change('next')">
                                        <span class="fa fa-chevron-right"></span>
                                    </button>
                                </div>
                                <div id="esambat-detail-auto-content"></div>
                            </div>
                            <div id="esambat-detail-web">
                                <div id="esambat-web-control" style="position: fixed">
                                    <button id="button-web-prev"
                                            class="button is-small is-white  "
                                            onclick="detail_change('web', 'prev')">
                                        <span class="fa fa-chevron-left"></span>
                                    </button>
                                    <button id="button-web-next"
                                            class="button is-small is-white  "
                                            onclick="detail_change('web', 'next')">
                                        <span class="fa fa-chevron-right"></span>
                                    </button>
                                    &nbsp;<span id="page-info-web" style="font-size: 12px">1</span>
                                </div>
                                <br><br>
                                <div id="esambat-detail-web-content"></div>
                            </div>
                            <div id="esambat-detail-sms">
                                <div id="esambat-sms-control" style="position: fixed">
                                    <button id="button-sms-prev"
                                            class="button is-small is-white  "
                                            onclick="detail_change('sms', 'prev')">
                                        <span class="fa fa-chevron-left"></span>
                                    </button>
                                    <button id="button-sms-next"
                                            class="button is-small is-white  "
                                            onclick="detail_change('sms', 'next')">
                                        <span class="fa fa-chevron-right"></span>
                                    </button>
                                    &nbsp;<span id="page-info-sms" style="font-size: 12px">1</span>
                                </div>
                                <br><br>
                                <div id="esambat-detail-sms-content"></div>
                            </div>
                            <div id="esambat-chart">
                                <div id="esambat-chart-control">
                                    <button id="button-chart-sum"
                                            class="button is-small is-white  "
                                            onclick="chart_change('sum')">
                                        Keseluruhan
                                    </button>
                                    <button id="button-chart-category"
                                            class="button is-small is-white  "
                                            onclick="chart_change('category')">
                                        Category
                                    </button>
                                    <button id="button-chart-bulan-sms"
                                            class="button is-small is-white  "
                                            onclick="chart_change('bulan_sms')">
                                        Bulan
                                    </button>
                                </div>
                                <canvas id="chart-sum" width="100%" height="50%"></canvas>
                                <canvas id="chart-category" width="100%" height="50%"></canvas>
                                <canvas id="chart-bulan-sms" width="100%" height="50%"></canvas>
                                <div class="">
                                    <table id="table-chart-legend" class="table" style="margin-top: 20px">
                                        <thead>
                                        <tr>
                                            <td>Simbol</td>
                                            <td>Keterangan</td>
                                        </tr>
                                        </thead>
                                        <tbody id="table-chart-legend-body">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
</body>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url('/assets/js/chart.min.js') ?>"></script>

<script>
    var $video = $('video');
    var sound = false;
    var show_esambat = true;
    var esambat_detail_content_count = 5;
    var esambat_detail_web = [];
    var esambat_detail_web_page = 1;
    var esambat_detail_sms = [];
    var esambat_detail_sms_page = 1;
    var esambat_count = undefined;
    var auto_on = false;
    var auto_index_web = 0;
    var auto_index_sms = 0;
    var bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];


    $(function () {
        //loading screen
        jQuery(document).ajaxStart(function () {
            $('body').loading({
                message: 'Proses...'
            });
        });
        jQuery(document).ajaxStop(function () {
            $('body').loading('stop');
        });

        $('#sound-off').hide();
//        set_table_katalog_to_default()

        /*
        *E-Sambat thing
         */
        if (window.location.hash == '#e-sambat') show_esambat = false;
        toggle_esambat();
        <?php if($id == 0){ ?>
//detail auto
        init_auto_esambat();
        auto_index_web++;
        auto_index_sms++;
        window.setInterval(function () {
            if (auto_on) {
                init_auto_esambat();
                auto_index_web++;
                auto_index_sms++;
            }
        }, 10000);
        init_detail_esambat();
        init_chart();
        <?php } ?>
        $('#esambat-detail-web').hide();
        $('#esambat-detail-sms').hide();
        $('#esambat-chart').hide();
        $('.modal-close').on('click', function () {
            $('.modal').removeClass('is-active')
        });
        $('.modal-background').on('click', function () {
            $('.modal').removeClass('is-active')
        });
        /*
        E-Sambat thing end
         */
    });

    function toggle_video(vid) {
        if (vid.paused) {
            vid.play();
            return;
        }
        vid.pause();
    }

    function sound_toggle() {
        sound = !sound;
        if (sound) {
            $('#sound-on').hide();
            $('#sound-off').show();

            $video.prop('muted', false)
        } else {
            $('#sound-on').show();
            $('#sound-off').hide();

            $video.prop('muted', true)
        }
    }

    function remove_katalog(api_url) {
        swal({
                title: "Konfirmasi Hapus?",
                text: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: api_url,
                    type: "DELETE"
                }).done(function (result) {
                    var data = jQuery.parseJSON(JSON.stringify(result));
                    if (data.success) {
                        swal({
                                title: "Berhasil",
                                text: data.message,
                                type: "success"
                            },
                            function () {
                                location.reload();
                            }
                        );
                    } else {
                        sweetAlert("gagal", data.message, "error");
                    }
                }).fail(function (xhr, status, errorThrown) {
                    sweetAlert("Gagal", status, "error");
                })
            });
    }

    /*
    * E-Sambat thing
    * */
    function toggle_esambat() {
        show_esambat = !show_esambat;
        if (show_esambat) {
            $('#katalog-table-wrapper').slideUp('fast', function () {
                $('.esambat-item').slideDown('fast');
                $('#desc-katalog').slideUp('fast');
                $('#desc-esambat').slideDown('fast');
                auto_on = true;

                show_esambat_menu('auto');

                $('#button-esambat').removeClass('is-white  ');
                $('#button-esambat').addClass('is-warning');
            });
        } else {
            $('.esambat-item').slideUp('fast', function () {
                $('#katalog-table-wrapper').slideDown('fast');
                $('#button-esambat').removeClass('is-warning');
                $('#button-esambat').addClass('is-white  ');
            });
            $('#desc-esambat').slideUp('fast');
            $('#desc-katalog').slideDown('fast');
        }
    }

    function detail_change(type, par) {
        $detail_web_content = $('#esambat-detail-web-content');
        $detail_sms_content = $('#esambat-detail-sms-content');

        $btn_web_prev = $('#button-web-prev');
        $btn_web_next = $('#button-web-next');
        $btn_sms_prev = $('#button-sms-prev');
        $btn_sms_next = $('#button-sms-next');

        switch (type) {
            case 'web':
                par == 'next' ? esambat_detail_web_page++ : esambat_detail_web_page--;
                if (esambat_detail_web_page < 1) esambat_detail_web_page = 1;
                $('#page-info-web').html(esambat_detail_web_page);
                init_detail_esambat();
                break;
            case 'sms':
                par == 'next' ? esambat_detail_sms_page++ : esambat_detail_sms_page--;
                if (esambat_detail_sms_page < 1) esambat_detail_sms_page = 1;
                $('#page-info-sms').html(esambat_detail_sms_page);
                init_detail_esambat();
                break;
        }
    }

    function toggle_auto_play() {
        auto_on = !auto_on;
        if (auto_on) {
            $('#auto-play-button').removeClass('fa-play');
            $('#auto-play-button').addClass('fa-pause');
        } else {
            $('#auto-play-button').removeClass('fa-pause');
            $('#auto-play-button').addClass('fa-play');
        }
    }

    function auto_change(par) {
        auto_on = false;
        $('#auto-play-button').removeClass('fa-pause');
        $('#auto-play-button').addClass('fa-play');

        switch (par) {
            case 'prev':
                auto_index_sms--;
                auto_index_web--;
                break;
            case 'next':
                auto_index_sms++;
                auto_index_web++;
                break;
        }
        init_auto_esambat();
    }

    function show_esambat_menu(menu) {
        $detail_auto = $('#esambat-detail-auto');
        $detail_sms = $('#esambat-detail-sms');
        $detail_web = $('#esambat-detail-web');
        $chart = $('#esambat-chart');

        $button_auto = $('#button-esambat-detail-auto');
        $button_web = $('#button-esambat-detail-web');
        $button_sms = $('#button-esambat-detail-sms');
        $button_chart = $('#button-esambat-chart');

        auto_on = false;
        $button_auto.removeClass('is-warning');
        $button_auto.addClass('is-white  ');
        $button_sms.removeClass('is-warning');
        $button_sms.addClass('is-white  ');
        $button_web.removeClass('is-warning');
        $button_web.addClass('is-white  ');
        $button_chart.removeClass('is-warning');
        $button_chart.addClass('is-white  ');
        switch (menu) {
            case 'auto':
                $button_auto.removeClass('is-white  ');
                $button_auto.addClass('is-warning');

                auto_on = true;
                $detail_sms.slideUp('fast');
                $detail_web.slideUp('fast');
                $chart.slideUp('fast');
                $detail_auto.slideDown('fast');

                break;
            case 'web':
                $button_web.removeClass('is-white  ');
                $button_web.addClass('is-warning');

                $detail_auto.slideUp('fast');
                $detail_sms.slideUp('fast');
                $chart.slideUp('fast');
                $detail_web.slideDown('fast');
                break;
            case 'sms':
                $button_sms.removeClass('is-white  ');
                $button_sms.addClass('is-warning');

                $detail_auto.slideUp('fast');
                $detail_web.slideUp('fast');
                $chart.slideUp('fast');
                $detail_sms.slideDown('fast');
                break;
            case 'chart':
                $button_chart.removeClass('is-white  ');
                $button_chart.addClass('is-warning');

                $detail_auto.slideUp('fast');
                $detail_web.slideUp('fast');
                $detail_sms.slideUp('fast');
                $chart.slideDown('fast');


                setTimeout(function () {
                    chart_change('sum');
                }, 300);

                break;
        }
    }

    function init_auto_esambat() {
        if (auto_index_web < 1) auto_index_web = 1;
        if (auto_index_sms < 1) auto_index_sms = 1;
        var url_detail_web = '<?php echo base_url('/katalog/katalog/esambat_detail_web/') ?>' + 1 + '/' + auto_index_web;
        var url_detail_sms = '<?php echo base_url('/katalog/katalog/esambat_detail_sms/') ?>' + 1 + '/' + auto_index_sms;

        $('#esambat-detail-auto-content').html('<br><br>');
        //auto WEB
        $.ajax({
            url: url_detail_web,
            type: "GET"
        }).done(function (result) {
            if (esambat_detail_web_page < 1) esambat_detail_web_page = 1;
            var esambat_detail_web_terjawab = JSON.parse(result).terjawab;
            esambat_detail_web_terjawab.forEach(function (item) {
                $('#esambat-detail-auto-content').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd, item.tanggapan));
            });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Gagal mengambil data sambat web', "error");
        });

        //auto SMS
        $.ajax({
            url: url_detail_sms,
            type: "GET"
        }).done(function (result) {
            if (esambat_detail_sms_page < 1) esambat_detail_sms_page = 1;
            var esambat_detail_sms_terjawab = JSON.parse(result).terjawab;
            esambat_detail_sms_terjawab.forEach(function (item) {
                $('#esambat-detail-auto-content').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd, item.tanggapan));
            });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Gagal mengambil data sambat web', "error");
        });

    }

    function init_detail_esambat() {
        var url_detail_web = '<?php echo base_url('/katalog/katalog/esambat_detail_web/') ?>' + esambat_detail_content_count + '/' + ((esambat_detail_web_page - 1) * esambat_detail_content_count + 1);
        var url_detail_sms = '<?php echo base_url('/katalog/katalog/esambat_detail_sms/') ?>' + esambat_detail_content_count + '/' + ((esambat_detail_sms_page - 1) * esambat_detail_content_count + 1);
        var url_count = '<?php echo base_url('/katalog/katalog/esambat_count') ?>';

        //detail WEB
        $.ajax({
            url: url_detail_web,
            type: "GET"
        }).done(function (result) {
            if (esambat_detail_web_page < 1) esambat_detail_web_page = 1;

            $('#esambat-detail-web-content').html('');
            var esambat_detail_web_terjawab = JSON.parse(result).terjawab;
            var esambat_detail_web_tidak_terjawab = JSON.parse(result).tidak_terjawab;
            esambat_detail_web_terjawab.forEach(function (item) {
                $('#esambat-detail-web-content').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd, item.tanggapan));
            });
            esambat_detail_web_tidak_terjawab.forEach(function (item) {
                $('#esambat-detail-web-content').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd));
            });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Gagal mengambil data sambat web', "error");
        });

        //detail SMS
        $.ajax({
            url: url_detail_sms,
            type: "GET"
        }).done(function (result) {
            if (esambat_detail_sms_page < 1) esambat_detail_sms_page = 1;

            $('#esambat-detail-sms-content').html('');
            var esambat_detail_sms_terjawab = JSON.parse(result).terjawab;
            var esambat_detail_sms_tidak_terjawab = JSON.parse(result).tidak_terjawab;
            esambat_detail_sms_terjawab.forEach(function (item) {
                $('#esambat-detail-sms-content').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd, item.tanggapan));
            });
            esambat_detail_sms_tidak_terjawab.forEach(function (item) {
                $('#esambat-detail-sms-content').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd));
            });

        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Gagal mengambil data sambat web', "error");
        });
    }

    function auto_template() {

    }

    function esambat_detail_template(time, sender, body, skpd, tanggapan) {
        var via = 'SMS';
        if (tanggapan) {
            return '' +
                '<div>' +
                '<b><span>' + skpd + '</span></b><br>' +
                '<p>' + body + '</p>' +
                '<b class="is-pulled-right">' + time + ', ' + sender + '</b>' +
                '</div><br>' +
                '<div style="margin-left:20px">' +
                '<p><b>Tanggapan:</b><br>' + tanggapan + '</p><br>' +
                '</div>' +
                '<hr>';
        } else {
            return '' +
                '<div>' +
                '<b><span>' + skpd + '</span></b><br>' +
                '<p>' + body + '</p>' +
                '<b class="is-pulled-right">' + time + ', ' + sender + '</b>' +
                '</div><br>' +
                '<div style="margin-left:20px">' +
                '<p><b>Belum Ada Tangggapan</b></p><br>' +
                '</div>' +
                '<hr>';
        }

    }

    var legend_sum = [];
    legend_sum['A'] = 'Sambat Web';
    legend_sum['B'] = 'Sambat SMS';
    var legend_category = [];
    var legend_date = [];
    var char = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];


    function chart_change(par) {
        $chart_sum = $('#chart-sum');
        $chart_category = $('#chart-category');
        $chart_bulan_sms = $('#chart-bulan-sms');

        $btn_sum = $('#button-chart-sum');
        $btn_category = $('#button-chart-category');
        $btn_bulan_sms = $('#button-chart-bulan-sms');

        $table_legend = $('#table-chart-legend');
        $table_legend_body = $('#table-chart-legend-body');
        $table_legend_body.html('');
        $table_legend.slideUp('fast');

        $chart_sum.slideUp('fast');
        $chart_category.slideUp('fast');
        $chart_bulan_sms.slideUp('fast');

        $btn_sum.removeClass('is-warning');
        $btn_sum.addClass('is-white  ');
        $btn_category.removeClass('is-warning');
        $btn_category.addClass('is-white  ');
        $btn_bulan_sms.removeClass('is-warning');
        $btn_bulan_sms.addClass('is-white  ');

        switch (par) {
            case 'sum':
                $btn_sum.removeClass('is-white  ');
                $btn_sum.addClass('is-warning');
                $chart_sum.slideDown('fast');
                break;
            case 'category':
                $btn_category.removeClass('is-white  ');
                $btn_category.addClass('is-warning');
                $chart_category.slideDown('fast');
                for (var key in legend_category) {
                    $table_legend_body.append('' +
                        '<tr>' +
                        '<td>' + key + '</td>' +
                        '<td>' + legend_category[key] + '</td>' +
                        '</tr>');
                }
                $table_legend.slideDown('fast');
                break;
            case 'bulan_sms':
                $btn_bulan_sms.removeClass('is-white  ');
                $btn_bulan_sms.addClass('is-warning');
                $chart_bulan_sms.slideDown('fast');
                for (var key in legend_date) {
                    $table_legend_body.append('' +
                        '<tr>' +
                        '<td>' + key + '</td>' +
                        '<td>' + legend_date[key] + '</td>' +
                        '</tr>');
                }
                $table_legend.slideDown('fast');
                break;
        }
    }

    function init_chart() {
        Chart.defaults.global.defaultFontColor = "#2B2B2B";
        Chart.defaults.global.defaultFontSize = 14;

        var api_url_bulk = '<?php echo base_url('katalog/katalog/esambat_count/0') ?>';
        var api_url_web = '<?php echo base_url('katalog/katalog/esambat_count/web') ?>';
        var api_url_sms = '<?php echo base_url('katalog/katalog/esambat_count/sms') ?>';

        var esambat_count = null;
        var data_web = null;
        var data_sms = null;

        $.ajax({
            url: api_url_bulk,
            type: "GET"
        }).done(function (result) {
            console.log(result);
            esambat_count = JSON.parse(result);
            var ctx_category = document.getElementById("chart-category").getContext('2d');
            var ctx_bulan = document.getElementById("chart-bulan-sms").getContext('2d');


            //chart category
            var labels_category = [];
            var data_category = [];
            var backgroundColor_category = [];
            var borderColor_category = [];
            var count = 0;
            esambat_count.kategori.forEach(function (item) {
                legend_category[char[count]] = item.kategori;
                labels_category.push(char[count]);
                data_category.push(item.jumlah);
                backgroundColor_category.push('rgba(232,54,72, 0.9)');
                borderColor_category.push('rgba(232,54,72, 1)');
                count++;
            });
            var chart_category = new Chart(ctx_category, {
                type: 'bar',
                data: {
                    labels: labels_category,
                    datasets: [
                        {
                            label: 'Jumlah Category',
                            data: data_category,
                            backgroundColor: backgroundColor_category,
                            borderColor: borderColor_category,
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    onClick: chart_category_click
                }
            });


            //chart bulan web
            var labels_bulan_web = [];
            var data_bulan_web = [];
            var backgroundColor_bulan_web = [];
            var borderColor_bulan_web = [];
            count = 0;
            esambat_count.webBulan.forEach(function (item) {
                legend_date[char[count]] = item.tahun + ' - ' + bulan[item.bulan - 1];
                labels_bulan_web.push(char[count]);
                data_bulan_web.push(item.jumlah);
                backgroundColor_bulan_web.push('rgba(255, 206, 86, 0.9)');
                borderColor_bulan_web.push('rgba(255, 206, 86, 1)');
                count++;
            });


            //chart bulan sms
            var data_bulan_sms = [];
            var backgroundColor_bulan_sms = [];
            var borderColor_bulan_sms = [];
            esambat_count.smsBulan.forEach(function (item) {
                data_bulan_sms.push(item.jumlah);
                backgroundColor_bulan_sms.push('rgba(232,54,72, 0.9)');
                borderColor_bulan_sms.push('rgba(232,54,72, 1)');
            });

            //chart bulan
            var chart_bulan = new Chart(ctx_bulan, {
                type: 'bar',
                data: {
                    labels: labels_bulan_web,
                    datasets: [
                        {
                            label: 'Jumlah Bulan - WEB',
                            data: data_bulan_web,
                            backgroundColor: backgroundColor_bulan_web,
                            borderColor: borderColor_bulan_web,
                            borderWidth: 1
                        },
                        {
                            label: 'Jumlah Bulan - SMS',
                            data: data_bulan_sms,
                            backgroundColor: backgroundColor_bulan_sms,
                            borderColor: borderColor_bulan_sms,
                            borderWidth: 1
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    onClick: chart_date_click
                }
            });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });

        $.ajax({
            url: api_url_web,
            type: "GET"
        }).done(function (result) {
            console.log('WEB');
            console.log(result);
            data_web = JSON.parse(result);
            $.ajax({
                url: api_url_sms,
                type: "GET"
            }).done(function (result) {
                console.log('SMS');
                console.log(result);
                data_sms = JSON.parse(result);
                var ctx_sum = document.getElementById("chart-sum").getContext('2d');
//chart count web and sms
                var chart_sum = new Chart(ctx_sum, {
                    type: 'bar',
                    data: {
                        labels: [
                            'Sambat Web',
                            'Sambat SMS'
                        ],
                        datasets: [
                            {
                                label: 'Jumlah Keseluruhan',
                                data: [
                                    (data_web.terjawab + data_web.tidak_terjawab),
                                    (data_sms.terjawab + data_sms.tidak_terjawab)
                                ],
                                backgroundColor: [
                                    'rgba(232,54,72, 0.9)',
                                    'rgba(232,54,72, 0.9)'
                                ],
                                borderColor: [
                                    'rgba(232,54,72, 1)',
                                    'rgba(232,54,72, 1)'
                                ],
                                borderWidth: 1
                            }
                        ]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        },
                        onClick: chart_sum_click
                    }
                });
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });

        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });

    }

    var current_url = '';
    var current_limit = 5;
    var current_offset = 0;
    var modal_content = $('#modal-esambat-content');

    function chart_sum_click(event, array) {
        current_offset = 0;
        $('#modal-esambat').addClass('is-active');
        var api_url_web = '<?php echo base_url('katalog/katalog/esambat_count/web') ?>';
        var api_url_sms = '<?php echo base_url('katalog/katalog/esambat_count/sms') ?>';
        var url_detail_sms = '<?php echo base_url('/katalog/katalog/esambat_detail_sms/') ?>' + 10 + '/' + 1;
        if (array[0]._model.label == 'Sambat Web') {
            $.ajax({
                url: api_url_web,
                type: "GET"
            }).done(function (result) {
                result = JSON.parse(result);
                detail_chart_template('Sambat WEB', result);

                //detail WEB
                current_url = '<?php echo base_url('/katalog/katalog/esambat_detail_web/') ?>';
                set_detail_card();
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });
        } else if (array[0]._model.label == 'Sambat SMS') {
            $.ajax({
                url: api_url_sms,
                type: "GET"
            }).done(function (result) {
                result = JSON.parse(result);
                detail_chart_template('Sambat SMS', result);

                //detail SMS
                current_url = '<?php echo base_url('/katalog/katalog/esambat_detail_sms/') ?>';
                set_detail_card();
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });
        }
    }

    function chart_category_click(event, array) {
        current_offset = 0;
        $('#modal-esambat').addClass('is-active');
        var api_category_list = '<?php echo base_url('katalog/katalog/esambat_category_list') ?>';
        $.ajax({
            url: api_category_list,
            type: "GET"
        }).done(function (result) {
            result = JSON.parse(result);
            result.forEach(function (item) {
                if (item.kategori == legend_category[array[0]._model.label]) {
                    var par = encodeURIComponent('kategori?id_kategori=' + item.id_kategori);
                    var api_url_category = '<?php echo base_url('katalog/katalog/esambat_count/') ?>' + par;
                    $.ajax({
                        url: api_url_category,
                        type: "GET"
                    }).done(function (result) {
                        result = JSON.parse(result);
                        detail_chart_template('Sambat ' + item.kategori, result);
                        //detail Category
                        current_url = '<?php echo base_url('katalog/katalog/esambat_detail_by_category/') ?>' + item.id_kategori + '/';
                        set_detail_card();
                    }).fail(function (xhr, status, errorThrown) {
                        sweetAlert("Gagal", status, "error");
                    });
                }
            });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
    }

    function chart_date_click(event, array) {
        current_offset = 0;
        $('#modal-esambat').addClass('is-active');
        var raw_date = legend_date[array[0]._model.label].split(" - ");
        var selected_year = raw_date[0];
        var selected_month = (bulan.indexOf(raw_date[1]) + 1);

        var par = encodeURIComponent('bulan?tahun=' + selected_year + '&bulan=' + selected_month);
        var api_url_category = '<?php echo base_url('katalog/katalog/esambat_count/') ?>' + par;
        $.ajax({
            url: api_url_category,
            type: "GET"
        }).done(function (result) {
            result = JSON.parse(result);
            detail_chart_template('Sambat Bulanan ' + bulan[selected_month - 1] + ' ' + selected_year, result);
            //detail Category
            current_url = '<?php echo base_url('katalog/katalog/esambat_detail_by_month/') ?>' + selected_year + '/' + selected_month + '/';
            set_detail_card();
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });

    }

    function detail_chart_template(title, result) {
        var content = '' +
            '<div class="card card-chart-detail-content">' +
            '<header class="card-header">' +
            '<p class="card-header-title">' + title + '</p>' +
            '</header>' +
            '<div class="card-content">' +
            '<div class="content">' +
            '<p>' +
            'Sambatan Terjawab : <b>' + result.terjawab + '</b><br>' +
            'Sambatan Belum Terjawab : <b>' + result.tidak_terjawab + '</b><br>' +
            '<p>' +
            '<hr>' +
            '<div id="chart-detail-control" class="has-text-centered">' +
            '<button onclick="chart_detail_change(0)" class="button is-primary is-small"><span class="fa fa-chevron-left"></span></button>' +
            '<button onclick="chart_detail_change(1)" class="button is-primary is-small"><span class="fa fa-chevron-right"></span></button>' +
            '</div><br>' +
            '<div id="card-chart-detail"></div>' +
            '</div>' +
            '</div>' +
            '<div>';
        modal_content.html(content);
    }

    function set_detail_card() {
        $.ajax({
            url: current_url + current_limit + '/' + current_offset,
            type: "GET"
        }).done(function (result) {
//            console.log(result);
            $('#card-chart-detail').fadeOut('fast');
            $('#card-chart-detail').html('');
            var esambat_detail_terjawab = JSON.parse(result).terjawab;
            var esambat_detail_tidak_terjawab = JSON.parse(result).tidak_terjawab;
            esambat_detail_terjawab.forEach(function (item) {
                $('#card-chart-detail').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd, item.tanggapan));
            });
            esambat_detail_tidak_terjawab.forEach(function (item) {
                $('#card-chart-detail').append(esambat_detail_template(item.tanggal_pengiriman, item.pengirim, item.tiket, item.nama_skpd));
            });
            $('#card-chart-detail').fadeIn('fast');
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Gagal mengambil data sambat web', "error");
        });
    }

    function chart_detail_change(par) {
        console.log('okeee');
        switch (par) {
            case 1:
                console.log('NEXTT');
                current_offset += current_limit;
                break;
            case 0:
                current_offset -= current_limit;
                break;
        }
        if (current_offset < 0) current_offset = 0;
        set_detail_card();
    }

    /*
    * End of E-Sambat thing
    * */

</script>
</html>

