<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>

<body>
<img class="background-ncc-logo-full" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>" alt="NCC logo">

<div class="col-md-2">
    <h3><?php echo "Grafik Agregat Jumlah Aparatur Sipil Negara Pemerintah Kota Malang"; ?></h3>
    <hr>
    <p><?= $str_select;?></p>
    <table class="table table-striped table-bordered" id="chart-table">
        			<tr>
	   					<td colspan='4' align="left" id="nama_dinas"><b>Dinas Olahraga dan Kebudayaan</b></td>
	   					<td align="center" id="t_karyawan" width="20%"><b>0</b></td>
	   				</tr>
	   				<tr class='gol_main_1'>
	   					<td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_gl" value="gl"></td>
	   					<td colspan='3' align="center"><label for="filter_gl"><i><b>Berdasarkan Golongan</b></i></label></td>
	   				</tr>
	   				<tr class='gol_jenis_main_1'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Golongan I</td>
	   					<td align="center" id="jml_golongan_1">20</td>
	   				</tr>
	   				<tr class='gol_data_1'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan I-A</td>
	   					<td align="center" id="jml_golongan_1a">40</td>
	   				</tr>
	   				<tr class='gol_data_1'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan I-B</td>
	   					<td align="center" id="jml_golongan_1b">40</td>
	   				</tr>
	   				<tr class='gol_data_1'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan I-C</td>
	   					<td align="center" id="jml_golongan_1c">40</td>
	   				</tr>
	   				<tr class='gol_data_1'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan I-D</td>
	   					<td align="center" id="jml_golongan_1d">40</td>
	   				</tr>



	   				
	   				<tr class='gol_jenis_main_2'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Golongan II</td>
	   					<td align="center" id="jml_golongan_2">20</td>
	   				</tr>
	   				<tr class='gol_data_2'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan II-A</td>
	   					<td align="center" id="jml_golongan_2a">40</td>
	   				</tr>
	   				<tr class='gol_data_2'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan II-B</td>
	   					<td align="center" id="jml_golongan_2b">40</td>
	   				</tr>
	   				<tr class='gol_data_2'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan II-C</td>
	   					<td align="center" id="jml_golongan_2c">40</td>
	   				</tr>
	   				<tr class='gol_data_2'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan II-D</td>
	   					<td align="center" id="jml_golongan_2d">40</td>
	   				</tr>



	   				
	   				<tr class='gol_jenis_main_3'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Golongan III</td>
	   					<td align="center" id="jml_golongan_3">20</td>
	   				</tr>
	   				<tr class='gol_data_3'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan III-A</td>
	   					<td align="center" id="jml_golongan_3a">40</td>
	   				</tr>
	   				<tr class='gol_data_3'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan III-B</td>
	   					<td align="center" id="jml_golongan_3b">40</td>
	   				</tr>
	   				<tr class='gol_data_3'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan III-C</td>
	   					<td align="center" id="jml_golongan_3c">40</td>
	   				</tr>
	   				<tr class='gol_data_3'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan III-D</td>
	   					<td align="center" id="jml_golongan_3d">40</td>
	   				</tr>



	   			
	   				<tr class='gol_jenis_main_4'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Golongan IV</td>
	   					<td align="center" id="jml_golongan_4">20</td>
	   				</tr>
	   				<tr class='gol_data_4'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan IV-A</td>
	   					<td align="center" id="jml_golongan_4a">40</td>
	   				</tr>
	   				<tr class='gol_data_4'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan IV-B</td>
	   					<td align="center" id="jml_golongan_4b">40</td>
	   				</tr>
	   				<tr class='gol_data_4'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan IV-C</td>
	   					<td align="center" id="jml_golongan_4c">40</td>
	   				</tr>
	   				<tr class='gol_data_4'>
	   					<td colspan='3' align="right">#&nbsp;&nbsp;&nbsp;</td>
	   					<td>Golongan IV-D</td>
	   					<td align="center" id="jml_golongan_4d">40</td>
	   				</tr>

	   				

	   				<tr class='gender_main'>
	   					<td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_jk" value="jk"></td>
	   					<td colspan='3' align="center" ><label for="filter_jk"><i><b>Berdasarkan Gender</b></i></label></td>
	   				</tr>
	   				<tr class='gender_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Laki-Laki</td>
	   					<td align="center" id="jml_gender_l">20</td>
	   				</tr>
	   				<tr class='gender_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Perempuan</td>
	   					<td align="center" id="jml_gender_p">20</td>
	   				</tr>



	   				<tr class='agama_main'>
	   					<td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_ag" value="ag"></td>
	   					<td colspan='3' align="center" ><label for="filter_ag"><i><b>Berdasarkan Agama</b></i></label></td>
	   				</tr>
	   				<tr class='agama_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Islam</td>
	   					<td align="center" id="jml_ag_is">20</td>
	   				</tr>
	   				<tr class='agama_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Kristen</td>
	   					<td align="center" id="jml_ag_kr">20</td>
	   				</tr>
	   				<tr class='agama_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Katholik</td>
	   					<td align="center" id="jml_ag_ka">20</td>
	   				</tr>
	   				<tr class='agama_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Hindu</td>
	   					<td align="center" id="jml_ag_hi">20</td>
	   				</tr>
	   				<tr class='agama_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Budha</td>
	   					<td align="center" id="jml_ag_bu">20</td>
	   				</tr>



	   				<tr class='status_main'>
	   					<td colspan='2' align="left" width="7%"><input type="checkbox" name="filter" id="filter_sts" value="sts"></td>
	   					<td colspan='3' align="center" ><label for="filter_sts"><i><b>Berdasarkan Status Perkawinan</b></i></label></td>
	   				</tr>
	   				<tr class='status_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Kawin</td>
	   					<td align="center" id="jml_sts_k">20</td>
	   				</tr>
	   				<tr class='status_jenis_main'>
	   					<td colspan='2' align="center">&nbsp;&nbsp;&nbsp;-</td>
	   					<td colspan='2'>Belum Kawin</td>
	   					<td align="center" id="jml_sts_bk">20</td>
	   				</tr>
    </table>
    <hr>
    <p class="small pull-right">
        <a href="<?php echo base_url('') ?>">NCC</a> |
        <a href="<?php echo base_url('katalog') ?>">Katalog</a>
    </p>
</div>
<script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
<div class="col-md-10" id="chart_opening">
    <div class="box-body">
    	<table width="100%">
			<tr>
				<td colspan="2">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Semua Golongan</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_gol_all" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td width="50%">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Golongan I</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_gol_I" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
				<td width="50%">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Golongan II</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_gol_II" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td width="50%">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Golongan III</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_gol_III" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
				<td width="50%">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Golongan IV</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_gol_IV" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<table width="100%">
			<tr>
				<td colspan="2">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Berdasarkan Jenis Kelamin</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_jk" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<table width="100%">
			<tr>
				<td colspan="2">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Berdasarkan Agama</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_agama" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<table width="100%">
			<tr>
				<td colspan="2">
					<table width="100%" border="1">
						<tr>
							<td align="center"><b>Berdasarkan Status Kawin</b></td>
						</tr>
						<tr>
							<td><div id="chartdiv_sts" style="height: 350px; width: 100%;"></div></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
    </div>
</div>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/datatables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/url.min.js") ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>


	<script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script>

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

<script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
<script>
      	var data_main = JSON.parse('<?php print_r($data_json);?>');
      	$(document).ready(function(){
      		$("#jenis_instansi").val("1");

      		var val_data = $("#jenis_instansi").val();
      		get_data(val_data);
      	});

      	$("#show_for_detail").click(function(){
        	window.location.href = "<?php print_r(base_url()."Bpkadapi/show_detail");?>";
      	});

      	$("#jenis_instansi").change(function(e){
      		var val_data = $("#jenis_instansi").val();
      		get_data(val_data);
      		
      	});

      	function get_data(i){
      		console.log(data_main[i]);
      		$("#jml_golongan_1").html(data_main[i].sum_golongan[0].value);
      		$("#jml_golongan_2").html(data_main[i].sum_golongan[1].value);
      		$("#jml_golongan_3").html(data_main[i].sum_golongan[2].value);
      		$("#jml_golongan_4").html(data_main[i].sum_golongan[3].value);


      		$("#jml_golongan_1a").html(data_main[i].jumlah_golongan_I[0].value);
      		$("#jml_golongan_1b").html(data_main[i].jumlah_golongan_I[1].value);
      		$("#jml_golongan_1c").html(data_main[i].jumlah_golongan_I[2].value);
      		$("#jml_golongan_1d").html(data_main[i].jumlah_golongan_I[3].value);
      		
      		$("#jml_golongan_2a").html(data_main[i].jumlah_golongan_II[0].value);
      		$("#jml_golongan_2b").html(data_main[i].jumlah_golongan_II[1].value);
      		$("#jml_golongan_2c").html(data_main[i].jumlah_golongan_II[2].value);
      		$("#jml_golongan_2d").html(data_main[i].jumlah_golongan_II[3].value);

      		$("#jml_golongan_3a").html(data_main[i].jumlah_golongan_III[0].value);
      		$("#jml_golongan_3b").html(data_main[i].jumlah_golongan_III[1].value);
      		$("#jml_golongan_3c").html(data_main[i].jumlah_golongan_III[2].value);
      		$("#jml_golongan_3d").html(data_main[i].jumlah_golongan_III[3].value);

      		$("#jml_golongan_4a").html(data_main[i].jumlah_golongan_IV[0].value);
      		$("#jml_golongan_4b").html(data_main[i].jumlah_golongan_IV[1].value);
      		$("#jml_golongan_4c").html(data_main[i].jumlah_golongan_IV[2].value);
      		$("#jml_golongan_4d").html(data_main[i].jumlah_golongan_IV[3].value);


      		$("#jml_gender_l").html(data_main[i].gender[0].value);
      		$("#jml_gender_p").html(data_main[i].gender[1].value);


      		$("#jml_ag_is").html(data_main[i].agama[0].value);
      		$("#jml_ag_kr").html(data_main[i].agama[1].value);
      		$("#jml_ag_ka").html(data_main[i].agama[2].value);
      		$("#jml_ag_hi").html(data_main[i].agama[3].value);
      		$("#jml_ag_bu").html(data_main[i].agama[4].value);


      		$("#jml_sts_k").html(data_main[i].status_kawin[0].value);
      		$("#jml_sts_bk").html(data_main[i].status_kawin[1].value);


      		$("#t_karyawan").html(data_main[i].main[0].value+" (Karyawan)");
      		$("#nama_dinas").html("<b>"+data_main[i].main[0].keterangan+"</b>");


      		set_graph("chartdiv_gol_all", data_main[i].sum_golongan, am4core.useTheme(am4themes_material));

      		set_graph("chartdiv_gol_I", data_main[i].jumlah_golongan_I, am4core.useTheme(am4themes_material));
      		set_graph("chartdiv_gol_II", data_main[i].jumlah_golongan_II, am4core.useTheme(am4themes_material));
      		set_graph("chartdiv_gol_III", data_main[i].jumlah_golongan_III, am4core.useTheme(am4themes_dataviz));
      		set_graph("chartdiv_gol_IV", data_main[i].jumlah_golongan_IV, am4core.useTheme(am4themes_kelly));


      		set_graph("chartdiv_jk", data_main[i].gender,am4core.useTheme(am4themes_material));

      		set_graph("chartdiv_agama", data_main[i].agama,am4core.useTheme(am4themes_material));

      		set_graph("chartdiv_sts", data_main[i].status_kawin);
      	}

      	function set_graph(id_chart_div, data_main, animated){
      		am4core.ready(function() {

      		animated;
			am4core.useTheme(am4themes_animated);

			var chart = am4core.create(id_chart_div, am4charts.PieChart3D);
			chart.hiddenState.properties.opacity = 5; // this creates initial fade-in

			chart.legend = new am4charts.Legend();

			chart.data = data_main;

			var series = chart.series.push(new am4charts.PieSeries3D());
				series.dataFields.value = "value";
				series.dataFields.category = "keterangan";
			});

			console.log(data_main);
      	}

      	$("input[name='filter']").change(function(){
      		var array_filter_active = [];
      		$.each($("input[name='filter']:checked"), function(){            
                array_filter_active.push($(this).val());
            });

            console.log(array_filter_active);
      	});
    </script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>
