
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Kepegawaian</title>
    <!-- <style>
      #chartdiv {
        width: 100%;
        height: 400px;
      }
    </style> -->
  </head>
  <body>
    <input type="radio" name="radio_choose" id="radio_choose" class="category_choose" value="1">
      <label for="radio_choose">filter berdasarkan golongan</label>
    <input type="radio" name="radio_choose" id="radio_choose1" class="category_choose" value="2">
      <label for="radio_choose1">filter berdasarkan jenis kelamin</label>
    <input type="radio" name="radio_choose" id="radio_choose2" class="category_choose" value="3">
      <label for="radio_choose2">filter berdasarkan agama</label>
    <input type="radio" name="radio_choose" id="radio_choose3" class="category_choose" value="4">
      <label for="radio_choose3">filter berdasarkan status</label>
    <input type="radio" name="radio_choose" id="radio_choose4" class="category_choose" value="5">
      <label for="radio_choose4">filter berdasarkan jumlah karyawan</label>

    <button id="show_for_detail">Tampilkan Detail Masing-Masing OPD</button>

    <div id="chartdiv" style="height: 3000px; width: 100%;"></div>
    
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

    <script src="<?php print_r(base_url()."assets/js/jquery-3.2.1.js");?>"></script>

    <script>
      var data_main = JSON.parse('<?php print_r($data_json);?>');
      $(document).ready(function(){
        set_gologan(data_main.golongan);
      });

      $("#show_for_detail").click(function(){
        window.location.href = "<?php print_r(base_url()."bkd/Bpkadapi/show_detail");?>";
      });

      $('input[name=radio_choose]').change(function(){
        var param_radio = $('input[name=radio_choose]:checked').val();
        // console.log(param_radio)
        if(param_radio == 1){
          set_gologan(data_main.golongan);
          console.log(data_main.golongan);
        }else if(param_radio == 2){
          set_jenis_kelamin(data_main.jenis_kelamin);
          console.log(data_main.jenis_kelamin);
        }else if(param_radio == 3){
          set_agama(data_main.agama);
          console.log(data_main.agama);
        }else if(param_radio == 4){
          set_status(data_main.status);
          console.log(data_main.status);
        }else if(param_radio == 5){
          set_jml_total(data_main.jumlah_total);
          console.log(data_main.jumlah_total);
        }

      });

      function set_gologan(data_array){
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.grid.template.opacity = 0;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 10;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = false;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          labelBullet.locationX = 1;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
        }

        createSeries("jumlah_golongan_I"  , "Jumlah Golongan I");
        createSeries("jumlah_golongan_II" , "Jumlah Golongan II");
        createSeries("jumlah_golongan_III", "Jumlah Golongan III");
        createSeries("jumlah_golongan_IV" , "Jumlah Golongan IV"); 
      }

      function set_jenis_kelamin(data_array){
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.grid.template.opacity = 0;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 5;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = true;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          labelBullet.locationX = 0.5;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
        }

        createSeries("laki"  , "Laki-laki");
        createSeries("perempuan" , "Perempuan");
      }

      function set_agama(data_array){
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.grid.template.opacity = 0;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 5;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = true;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          labelBullet.locationX = 0.5;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
        }

        createSeries("agama_islam"  , "Agama Islam");
        createSeries("agama_kristen" , "Agama Kristen");
        createSeries("agama_katholik", "Agama Katholik");
        createSeries("agama_hindu" , "Agama Hindu");
        createSeries("agama_budha" , "Agama Budha"); 
      }

      function set_status(data_array){
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.grid.template.opacity = 0;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 5;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = true;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          labelBullet.locationX = 0.5;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
        }

        createSeries("status_kawin"  , "Status Kawin");
        createSeries("status_belum_kawin" , "Status Belum Kawin");
      }

      function set_jml_total(data_array){
        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        // Create chart instance
        var chart = am4core.create("chartdiv", am4charts.XYChart);

        // Add data
        chart.data = data_array;

        chart.legend = new am4charts.Legend();
        chart.legend.position = "right";

        // Create axes
        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "category";
        categoryAxis.renderer.grid.template.opacity = 0;

        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.renderer.grid.template.opacity = 0;
        valueAxis.renderer.ticks.template.strokeOpacity = 0.5;
        valueAxis.renderer.ticks.template.stroke = am4core.color("#495C43");
        valueAxis.renderer.ticks.template.length = 5;
        valueAxis.renderer.line.strokeOpacity = 0.5;
        valueAxis.renderer.baseGrid.disabled = true;
        valueAxis.renderer.minGridDistance = 40;

        // Create series
        function createSeries(field, name) {
          var series = chart.series.push(new am4charts.ColumnSeries());
          series.dataFields.valueX = field;
          series.dataFields.categoryY = "category";
          series.stacked = true;
          series.name = name;
          
          var labelBullet = series.bullets.push(new am4charts.LabelBullet());
          labelBullet.locationX = 0.5;
          labelBullet.label.text = "{valueX}";
          labelBullet.label.fill = am4core.color("#fff");
        }

        createSeries("jml"  , "Jumlah Total");
        
      }
    </script>
  </body>
</html>