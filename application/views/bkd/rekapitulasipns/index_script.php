<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "unitkerja"},
            {"data": "islam"},
            {"data": "kristen"},
            {"data": "katholik"},
            {"data": "hindu"},
            {"data": "budha"},
            {"data": "kawin"},
            {"data": "belum_kawin"},
            {"data": "janda"},
            {"data": "duda"},
            {"data": "sd"},
            {"data": "sltp"},
            {"data": "slta"},
            {"data": "d"},
            {"data": "s1"},
            {"data": "s2"},
            {"data": "s3"},
            {"data": "iia"},
            {"data": "iib"},
            {"data": "iiia"},
            {"data": "iiib"},
            {"data": "iva"},
            {"data": "ivb"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/bkd/rekapitulasi-pns/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            swal({
                    title: "Tidak Tersedia",
                    text: "Tidak tersedia update data untuk \"Rekapitulasi PNS\", silahkan hapus data yang ingin dirubah dan lakukan penambahan data",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonText: "Oke",
                    closeOnConfirm: true
                },
                function () {
                    $('#modal-edit').modal('hide');
                });
//            var data_for_table = datatables.row($(this).parents('tr')).data();
//
//            data_id = data_for_table.idrekapitulasipns;
//
//            $("#idunitkerja").empty();
//            //get data unit kerja
//            $.ajax({
//                url: "<?php //echo base_url('api/bkd/unit-kerja') ?>//",
//                type: "GET"
//            }).done(function (result) {
//                var data = jQuery.parseJSON(JSON.stringify(result));
//                if (data.success) {
//                    $('#modal-edit select').css('width', '100%');
//                    $("#idunitkerja").select2({
//                        dropdownParent: $('#modal-edit')
//                    });
//                    $.each(data.data, function () {
//                        $("#idunitkerja").append($("<option />").val(this.idunitkerja).text(this.unitkerja));
//                    });
//                    $('#idunitkerja').val(data_for_table.idunitkerja);
//                } else {
//                    sweetAlert("gagal", 'Terjadi kesalahan saat mengambil data unit kerja', "error");
//                }
//            }).fail(function (xhr, status, errorThrown) {
//                sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data unit kerja", "error");
//            });
//
//            $('#tanggal').val(data_for_table.tanggal);
//            $('#islam').val(data_for_table.islam);
//            $('#katholik').val(data_for_table.katholik);
//            $('#kristen').val(data_for_table.kristen);
//            $('#hindu').val(data_for_table.hindu);
//            $('#budha').val(data_for_table.budha);
//            $('#kawin').val(data_for_table.kawin);
//            $('#belum_kawin').val(data_for_table.belum_kawin);
//            $('#janda').val(data_for_table.janda);
//            $('#duda').val(data_for_table.duda);
//            $('#sd').val(data_for_table.sd);
//            $('#sltp').val(data_for_table.sltp);
//            $('#slta').val(data_for_table.slta);
//            $('#d').val(data_for_table.d);
//            $('#s1').val(data_for_table.s1);
//            $('#s2').val(data_for_table.s2);
//            $('#s3').val(data_for_table.s3);
//            $('#iia').val(data_for_table.iia);
//            $('#iib').val(data_for_table.iib);
//            $('#iiia').val(data_for_table.iiia);
//            $('#iiib').val(data_for_table.iiib);
//            $('#iva').val(data_for_table.iva);
//            $('#ivb').val(data_for_table.ivb);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/bkd/rekapitulasi-pns/') ?>" + data.idunitkerja+'/'+data.tanggal, datatables);
        });
    });

    function showAdd() {
        $(".form-add #idagama").empty();
        //get data agama
        $.ajax({
            url: "<?php echo base_url('api/bkd/agama') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idagama").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idagama").append($("<option />").val(this.idagama).text(this.agama));
                });
            } else {
                sweetAlert("gagal", 'Terjadi kesalahan saat mengambil data agama', "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data agama", "error");
        });

        $(".form-add #ideselon").empty();
        //get data eselon
        $.ajax({
            url: "<?php echo base_url('api/bkd/eselon') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #ideselon").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #ideselon").append($("<option />").val(this.ideselon).text(this.eselon));
                });
            } else {
                sweetAlert("gagal", 'Terjadi kesalahan saat mengambil data eselon', "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data eselon", "error");
        });

        $(".form-add #idgolongan").empty();
        //get data golongan
        $.ajax({
            url: "<?php echo base_url('api/bkd/golongan') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idgolongan").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idgolongan").append($("<option />").val(this.idgolongan).text(this.golongan));
                });
            } else {
                sweetAlert("gagal", 'Terjadi kesalahan saat mengambil data golongan', "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data golongan", "error");
        });

        $(".form-add #idunitkerja").empty();
        //get data unit kerja
        $.ajax({
            url: "<?php echo base_url('api/bkd/unit-kerja') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idunitkerja").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idunitkerja").append($("<option />").val(this.idunitkerja).text(this.unitkerja));
                });
            } else {
                sweetAlert("gagal", 'Terjadi kesalahan saat mengambil data unit kerja', "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", "Terjadi kesalahan saat mengambil data unit kerja", "error");
        });

        fill_select_master('pendidikan', "<?php echo base_url() ?>", [$(".form-add #idpendidikan")]);
        fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$(".form-add #idjeniskelamin")]);
        fill_select_master('statuskawin', "<?php echo base_url() ?>", [$(".form-add #idstatuskawin")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/bkd/rekapitulasi-pns') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/bkd/rekapitulasi-pns/') ?>" + data_id, datatables);
    }
</script>