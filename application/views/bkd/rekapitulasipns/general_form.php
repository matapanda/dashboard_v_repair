<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="idunitkerja">Unit Kerja</label><br>
    <select name="idunitkerja" id="idunitkerja" class="form-control"></select>
</div>
<table class="table">
    <thead>
    <tr>
        <th colspan="3" class="text-center">Jumlah Berdasarkan Agama</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <label for="islam">Islam</label><br>
            <input name="islam" type="number" class="form-control" id="islam" placeholder="Islam" required>
        </td>
        <td>
            <label for="kristen">Kristen</label><br>
            <input name="kristen" type="number" class="form-control" id="kristen" placeholder="Kristen" required>
        </td>
        <td>
            <label for="katholik">Katholik</label><br>
            <input name="katholik" type="number" class="form-control" id="katholik" placeholder="Katholik" required>
        </td>
    </tr>
    <tr>
        <td>
            <label for="hindu">Hindu</label><br>
            <input name="hindu" type="number" class="form-control" id="hindu" placeholder="Hindu" required>
        </td>
        <td>
            <label for="budha">Budha</label><br>
            <input name="budha" type="number" class="form-control" id="budha" placeholder="Budha" required>
        </td>
    </tr>
    </tbody>
</table>
<table class="table">
    <thead>
    <tr>
        <th colspan="4" class="text-center">Jumlah Berdasarkan Status Kawin</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <label for="kawin">Kawin</label><br>
            <input name="kawin" type="number" class="form-control" id="kawin" placeholder="Kawin" required>
        </td>
        <td>
            <label for="belum_kawin">Belum Kawin</label><br>
            <input name="belum_kawin" type="number" class="form-control" id="belum_kawin" placeholder="Belum Kawin"
                   required>
        </td>
        <td>
            <label for="janda">Janda</label><br>
            <input name="janda" type="number" class="form-control" id="janda" placeholder="Janda" required>
        </td>
        <td>
            <label for="duda">Duda</label><br>
            <input name="duda" type="number" class="form-control" id="duda" placeholder="Duda" required>
        </td>
    </tr>
    </tbody>
</table>
<table class="table">
    <thead>
    <tr>
        <th colspan="4" class="text-center">Jumlah Berdasarkan Pendidikan</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <label for="sd">SD</label><br>
            <input name="sd" type="number" class="form-control" id="sd" placeholder="SD" required>
        </td>
        <td>
            <label for="sltp">SLTP</label><br>
            <input name="sltp" type="number" class="form-control" id="sltp" placeholder="SLTP" required>
        </td>
        <td>
            <label for="slta">SLTA</label><br>
            <input name="slta" type="number" class="form-control" id="slta" placeholder="SLTA" required>
        </td>
        <td>
            <label for="d">D</label><br>
            <input name="d" type="number" class="form-control" id="d" placeholder="D" required>
        </td>
    </tr>
    <tr>
        <td>
            <label for="s1">S-1</label><br>
            <input name="s1" type="number" class="form-control" id="s1" placeholder="S-1" required>
        </td>
        <td>
            <label for="s2">S-2</label><br>
            <input name="s2" type="number" class="form-control" id="s2" placeholder="S-2" required>
        </td>
        <td>
            <label for="s3">S-3</label><br>
            <input name="s3" type="number" class="form-control" id="s3" placeholder="S-3" required>
        </td>
    </tr>
    </tbody>
</table>

<table class="table">
    <thead>
    <tr>
        <th colspan="3" class="text-center">Jumlah Berdasarkan Golongan</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <label for="iia">II/A</label><br>
            <input name="iia" type="number" class="form-control" id="iia" placeholder="II/A" required>
        </td>
        <td>
            <label for="iib">II/B</label><br>
            <input name="iib" type="number" class="form-control" id="iib" placeholder="II/B" required>
        </td>
        <td>
            <label for="iiia">III/A</label><br>
            <input name="iiia" type="number" class="form-control" id="iiia" placeholder="III/A" required>
        </td>
    </tr>
    <tr>
        <td>
            <label for="iiib">III/B</label><br>
            <input name="iiib" type="number" class="form-control" id="iiib" placeholder="III/B" required>
        </td>
        <td>
            <label for="iva">IV/A</label><br>
            <input name="iva" type="number" class="form-control" id="iva" placeholder="IV/A" required>
        </td>
        <td>
            <label for="ivb">IV/B</label><br>
            <input name="ivb" type="number" class="form-control" id="ivb" placeholder="IV/B" required>
        </td>
    </tr>
    </tbody>
</table>