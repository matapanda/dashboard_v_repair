<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/select2.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/login.css"); ?>" rel="stylesheet">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
    <script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
    <script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
    <script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
    <script src="<?php echo base_url("/assets/js/datatables.min.js") ?>"></script>
    <script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
</head>
<body>

<div class="row">
    <div class="login-page">
        <div class="form">
            <h3><b>NCC</b> Login</h3>
            <hr>
            <form class="login-form">
                <input type="text" name="username" placeholder="username"/>
                <input type="password" name="password" placeholder="password"/>
                <button>login</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('form').on('submit', function (e) {
                $('body').loading({
                    message: "Proses..."
                });
                e.preventDefault();

                $.ajax({
                    type: 'post',
                    url: '<?=base_url('sys/app/authenticate/')?>',
                    data: $('form').serialize(),
                    type: 'POST'
                }).done(function (result) {
                    var data = jQuery.parseJSON(JSON.stringify(result));
                    $('body').loading('stop');
                    if (data.success) {
                        window.location.href = data.message;
                    } else {
                        sweetAlert("gagal", data.message, "error");
                    }
                }).fail(function (xhr, status, errorThrown) {
                    sweetAlert("Gagal", status, "error");
                })
            });

        });
    </script>
</div>
</body>
</html>
