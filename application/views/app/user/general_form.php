<div id="user">
  <div class="form-group">
    <label for="username">Username</label>
    <input name="username" type="text" class="form-control" id="username"
    placeholder="Username" required>
  </div>

  <div class="form-group">
    <label for="password">Password</label>
    <input name="password" type="password" class="form-control" id="password"
    placeholder="Password" required>
  </div>
  <div class="form-group">
    <label for="skpd">SKPD</label>
    <select name="idskpd" type="text" class="form-control" id="idskpd" placeholder="SKPD" required/>
  </div>
  <div class="form-group" style="margin-top:10px;">
    <label for="admin" class="checkbox-inline">
      <input name="admin" type="checkbox" id="admin" class="minimal"> Admin
    </label>
    <label for="statusaktif" class="checkbox-inline">
      <input name="statusaktif" type="checkbox" id="statusaktif" class="minimal"> Aktif
    </label>
  </div>
</div>

<label for="hak-pengguna">Hak Pengguna</label>
<div id="permissions">
  <div class="content-inner">
    <table id="datatables-permissions" class="table table-hover" style="width: 100%;">
      <thead>
        <tr>
          <th>Submodule</th>
          <th>Write</th>
          <th>Read</th>
          <th>Modules</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
