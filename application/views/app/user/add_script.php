<script>
$(document).ready(function () {
  fill_select_master('skpd', "<?php echo base_url() ?>", [$("#idskpd")]);

  var datatables;
  datatables = $('#datatables-permissions').DataTable({
    "ajax": "<?=base_url('/sys/manage/submodule/get/')?>" + $('#idapp_user').val(),
    "deferRender": true,
    "columns": [
      {"data": "idsubmodule", "visible": false},
      {"data": "submodule"},
      {"data": null},
      {"data": null},
      {"data": "modules"}
    ],
    "columnDefs": [{
      "render" : function(data, type, row){
        return "<input type='checkbox' name='permissions["+ row['idsubmodule'] +"][write]' class='select-permissions'>";
      },
      "targets" : [2]
    },{
      "render" : function(data, type, row){
        return "<input type='checkbox' name='permissions["+ row['idsubmodule'] +"][read]' class='select-permissions'>";
      },
      "targets" : [3]
    }]
  });
});

function save(){
  var fields = [
      $('#idapp_user'),
      $('#username'),
      $('#password'),
      $('#admin'),
      $('#statusaktif'),
      $('.select-permissions')
  ];
  save_new_data("<?php echo base_url('/sys/manage/user/store') ?>", fields);

};
</script>
