<script>
    var datatables;
    var data_id;
    $(document).ready(function () {
        datatables = $('#datatables').DataTable({
            "ajax": "<?=base_url()?>sys/manage/user/get",
            "deferRender": true,
            "responsive": true,
            "autoWidth": false,
            "columns": [
                {"data": "username"},
                {"data": "password"},
                {"data": "ismultiskpd"},
                {"data": "skpd"},
                {"data": "statusaktif"},
                {"data": null}
            ],
            "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "" +
                "<button class='update_btn btn btn-default btn-sm' data-toggle='modal' data-target='#modal-edit'><span class='fa fa-pencil'></span></button>"
            }, {
                "render": function (data, type, row) {
                    if (row['ismultiskpd'] == 1) {
                        return data + '  <span class="label label-info">Admin</span>';
                    } else {
                        return data;
                    }
                },
                "targets": [0]
            }, {
                "render": function (data, type, row) {
                    if (row['statusaktif'] == 1) {
                        return "<span class='label label-success'>Aktif</span>";
                    } else {
                        return "<span class='label label-danger'>Tidak Aktif</span>";
                    }
                },
                "targets": [4]
            }, {
                "visible": false,
                "targets": [2]
            }]
        });

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            data_id = data.idapp_user;
            $('#idapp_user').val(data.idapp_user);
            $('#username').val(data.username);
            $('#password').val(data.password);
            $('#idskpd').val(data.skpd);

            if (data.ismultiskpd == 1) {
                $('.form-update #admin').attr('checked', 'checked');
                $('.form-update #admin').parents().addClass('checked');
            } else {
                $('.form-update #admin').checked = true;
                $('.form-update #admin').parents().removeClass('checked');
            }

            if (data.statusaktif == 1) {
                $('.form-update #statusaktif').attr('checked', 'checked');
                $('.form-update #statusaktif').parents().addClass('checked');
            } else {
                $('.form-update #statusaktif').parents().removeClass('checked');
            }

            fill_select_master('skpd', "<?php echo base_url() ?>", [$(".form-update #idskpd")], data);

            $('.form-update #datatables-permissions').DataTable().destroy();
            datatablespermissions = $('.form-update #datatables-permissions').DataTable({
                "ajax": "<?=base_url('/sys/manage/usermodules/get/')?>" + data_id,
                "deferRender": true,
                "columns": [
                    {"data": "submodule"},
                    {"data": null},
                    {"data": null},
                    {"data": "modules"}
                ],
                "columnDefs": [{
                    "render": function (data, type, row) {
                        if (data['iswrite'] == 1) {
                            return "<input type='hidden' name='permissions[" + row['idsubmodule'] + "][write]' value='0'>" +
                                "<input type='checkbox' name='permissions[" + row['idsubmodule'] + "][write]' class='select-permissions' checked='checked'>";
                        }
                        return "<input type='checkbox' name='permissions[" + row['idsubmodule'] + "][write]' class='select-permissions'>";
                    },
                    "targets": [1]
                }, {
                    "render": function (data, type, row) {
                        if (data['isread'] == 1) {
                            return "<input type='hidden' name='permissions[" + row['idsubmodule'] + "][read]' value='0'>" +
                                "<input type='checkbox' name='permissions[" + row['idsubmodule'] + "][read]' class='select-permissions' checked='checked'>";
                        }
                        return "<input type='checkbox' name='permissions[" + row['idsubmodule'] + "][read]' class='select-permissions'>";
                    },
                    "targets": [2]
                }]
            });
        });

    });
    function update() {
        update_data("<?php echo base_url('sys/manage/user/update/') ?>" + data_id, datatables);
    };

    function showAdd() {
        fill_select_master('skpd', "<?php echo base_url() ?>", [$(".form-add #idskpd")]);

        var datatables;

        datatables = $('.form-add #datatables-permissions').DataTable({
            "ajax": "<?=base_url('/sys/manage/submodule/get/')?>",
            "deferRender": true,
            "columns": [
                {"data": "submodule"},
                {"data": null},
                {"data": null},
                {"data": "modules"}
            ],
            "columnDefs": [{
                "render": function (data, type, row) {
                    return "<input type='checkbox' name='permissions[" + row['idsubmodule'] + "][write]' class='select-permissions'>";
                },
                "targets": [1]
            }, {
                "render": function (data, type, row) {
                    return "<input type='checkbox' name='permissions[" + row['idsubmodule'] + "][read]' class='select-permissions'>";
                },
                "targets": [2]
            }]
        });
    }

    function add() {
        var fields = [
            $('#idapp_user'),
            $('#username'),
            $('#password'),
            $('#admin'),
            $('#statusaktif'),
            $('.select-permissions')
        ];
        save_new_data("<?php echo base_url('/sys/manage/user/store') ?>", datatables);

    };
</script>
