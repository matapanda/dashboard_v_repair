<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="content">
  <header class="clearfix">
    <div class="col-xs-5 col-sm-3 col-md-3"><b>Daftar User</b></div>
    <div class="col-xs-7 col-sm-9 col-md-9">
      <a type="button" class="btn btn-xs btn-primary pull-right" href="gelandangan-psikotik/add">Tambah Data
        Baru</a>
      </div>
    </header>
    <!-- Modal -->
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <spanaria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Edit Data User</h4>
          </div>
          <div class="modal-body">
            <form onsubmit="event.preventDefault(); update();">
              <input type="hidden" id="idapp_user">
              <div class="form-group">
                <label for="username">Username</label>
                <input name="username" type="text" class="form-control" id="input_username"
                placeholder="Username" required>
              </div>

              <div class="form-group">
                <label for="password">Password</label>
                <input name="password" type="password" class="form-control" id="input_password"
                placeholder="Password" required>
              </div>

              <div class="form-group">
                <label for="skpd">SKPD</label>
                <select name="idskpd" type="text" class="form-control" id="input_skpd" placeholder="Tanggal" required />
              </div>

              <div class="form-group">
                <label for="admin">
                  <input type="checkbox" name="admin" id="input_admin"> Admin
                  <input type="checkbox" name="aktif" id="input_aktif"> Aktif
                </label>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-edit-permissions" tabindex="-1" role="dialog" aria-labelledby="modal-edit-permissions">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <spanaria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Edit Hak Pengguna</h4>
          </div>
          <div class="modal-body">
            <div class="content-inner">
              <table id="datatables2" class="table table-hover">
                <thead>
                  <tr>
                    <th>Submodule</th>
                    <th>Modules</th>
                    <th>Write</th>
                    <th>Read</th>
                    <th data-priority="1">Aksi</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="content-inner">
      <table id="datatables" class="table table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Username</th>
            <th>Password</th>
            <th>Admin</th>
            <th>SKPD</th>
            <th>Aktif</th>
            <th data-priority="1">Aksi</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
