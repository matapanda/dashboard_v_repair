<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bulma.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">

    <style>
        ::-webkit-scrollbar {
            display: none;
        }

        html, body {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        video {
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%;
            min-height: 100%;
            width: auto;
            height: auto;
            z-index: -100;
            background-size: cover;
            overflow: hidden;
            object-fit: cover;

            /*-webkit-filter: blur(3px);*/
            /*-moz-filter: blur(3px);*/
            /*-o-filter: blur(3px);*/
            /*-ms-filter: blur(3px);*/
            /*filter: blur(3px);*/
        }

        #top-layer {
            width: 100%;
            height: 100%;
            /*background-color: rgba(0, 0, 0, .2);*/
        }

        #top-layer-content {
            opacity: 1;
        }

        #main-content {
            width: 100%;
            height: 250px;
            text-align: center;

            position: absolute; /*it can be fixed too*/
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;

            /*this to solve "the content will not be cut when the window is smaller than the content": */
            max-width: 100%;
            max-height: 100%;
            overflow: auto;
            background-color: rgba(255, 255, 255, 0.3);
            padding: 27px;
        }

        #main-content-login {
            width: 100%;
            height: 350px;

            position: absolute; /*it can be fixed too*/
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            margin: auto;

            /*this to solve "the content will not be cut when the window is smaller than the content": */
            max-width: 100%;
            max-height: 100%;
            overflow: auto;
        }

        .sound-control {
            position: fixed;
            right: 0;
            bottom: 0;
        }

        #toggle-menu {
            position: fixed;
        }

        #toggle-cover {
            text-align: center;
            position: fixed;
            display: block;
            left: 0;
            right: 0
        }

        #toggle-menu {
            display: inline
        }

        #toggle-uncover {
            text-align: center;
        }

        #menu-wrapper {
            overflow-y: scroll;
        }

        .menu-item-bottom {
            padding: 15px;
        }

        #card-menu {
            margin: 10px;
            background-color: rgba(255, 255, 255, .95);
            padding: 15px;
            border-radius: 5px;
            z-index: 999;
        }

        .menu-item {
            width: 100%;
            margin-bottom: 3px
        }

        #menu-hr {
            margin: 20px
        }

    </style>

</head>
<body>
<p id="toggle-cover">
    <button title="Show Menu" onclick="layer_toggle()" class="button is-white is-outlined is-center"><i
                class="fa fa-chevron-circle-down"></i></button>
</p>
<div id="top-layer">
    <div id="top-layer-content" onclick="background_click()">
        <div id="top-menu">
            <p id="toggle-menu">
                <button title="Show Action Menu" onclick="menu_toggle()"
                        class="button is-white is-outlined"><i
                            class="fa fa-bars"></i></button>
            </p>
            <p id="toggle-uncover">
                <button title="Hide Menu" onclick="layer_toggle()"
                        class="button is-white is-outlined is-center"><i
                            class="fa fa-chevron-circle-up"></i></button>
            </p>
        </div>

        <div id="menu-wrapper">
            <div class="menu columns">
                <div class="column is-3">
                    <div id="card-menu" class="card">
                        <p class="has-text-centered">
                            <span id="menu-head-icon" class="icon is-large"><i class="fa fa-bars"></i></span><br>
                            <span id="menu-head-text" class="subtitle is-6">NCC Menu</span>
                        </p>
                        <hr id="menu-hr">
                        <?php if ($is_login) { ?>
                            <a href="<?php echo base_url('dashboard') ?>" onmouseenter="hover_menu('manage')"
                               onmouseleave="hover_menu_out()"
                               class="menu-item button is-primary is-outlined">
                                <span class="icon"><i class="fa fa-briefcase"></i></span><span>Manage</span>
                            </a>
                        <?php } ?>
                        <a href="<?php echo base_url('katalog') ?>" onmouseenter="hover_menu('katalog')"
                           onmouseleave="hover_menu_out()"
                           class="menu-item button is-primary is-outlined">
                            <span class="icon"><i class="fa fa-list"></i></span><span>Katalog</span>
                        </a>
                        <?php if (!$is_login) { ?>
                            <a href="#login" onclick="login_toggle()" onmouseenter="hover_menu('login')"
                               onmouseleave="hover_menu_out()"
                               class="menu-item button is-primary is-outlined">
                                <span class="icon"><i class="fa fa-sign-in"></i></span><span>Log in</span>
                            </a>
                        <?php } else { ?>
                            <a href="<?php echo base_url('sys/app/logout') ?>" onmouseenter="hover_menu('logout')"
                               onmouseleave="hover_menu_out()"
                               class="menu-item button is-primary is-outlined">
                                <span class="icon"><i class="fa fa-sign-out"></i></span><span>Log out</span>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>


        </div>

        <div id="main-content">
            <img width="500px"  src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>" alt="NCC logo">
            <!--            <h1 class="title is-1" style="color:white;">NCC</h1>-->
            <!--            <h3 class="subtitle" style="color: white;">Ngalam Command Center</h3>-->
        </div>

        <div id="main-content-login">
            <div class="columns">
                <div class="column is-4 is-offset-4">
                    <div class="box is-clearfix">
                        <?php if ($is_login) { ?>
                            <div class="has-text-centered">
                                <h1 class="subtitle">Halo <?php echo ucfirst($user['username']) ?></h1>
                                Silahkan akses menu dengan klik tombol dibawah<br>
                                <br>
                                <button class="button is-primary" onclick="menu_toggle()">Menu</button>
                            </div>
                        <?php } else { ?>
                            <form class="login-form">
                                <a href="#" class="delete is-pulled-right" onclick="login_toggle()"></a>
                                <h1 class="title is-4"><b>NCC</b> | Login</h1>
                                <hr>
                                <div class="field">
                                    <label class="label">Username</label>
                                    <p class="control has-icons-left has-icons-right">
                                        <input name="username" class="input is-success" type="text"
                                               placeholder="Username">
                                        <span class="icon is-small is-left"><i class="fa fa-user"></i></span>
                                    </p>
                                </div>
                                <div class="field">
                                    <label class="label">Password</label>
                                    <p class="control has-icons-left has-icons-right">
                                        <input name="password" class="input is-success" type="password"
                                               placeholder="Password">
                                        <span class="icon is-small is-left"><i class="fa fa-key"></i></span>
                                    </p>
                                </div>
                                <div class="field">
                                    <button type="submit" class="button is-primary is-pulled-right">Login</button>
                                </div>
                            </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

        <button title="Unmute" id="sound-control-on" onclick="sound_toggle()" class="sound-control button is-primary"><i
                    class="fa fa-volume-up"></i></button>
        <button title="Mute" id="sound-control-off" onclick="sound_toggle()" class="sound-control button is-danger"><i
                    class="fa fa-volume-off"></i></button>
    </div>
</div>
<video autoplay loop muted>
    <source src="<?php echo base_url("/assets/videos/BUMPER.mp4"); ?>" type="video/mp4">
    <source src="<?php echo base_url("/assets/videos/BUMPER.mp4"); ?>" type="video/mp4">
    Browser tidak mensupport pemutaran vidio.
</video>
</body>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>

<script>
    var $video = $('video'),
        $window = $(window);
    var show = true;
    var sound = false;
    var menu = false;
    var login = false;

    $(function () {
        $('#toggle-cover').hide();
        $('#sound-control-off').hide();
        $('#main-content-login').hide();
        $('.menu').hide();

        if (window.location.hash) {
            var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
            if (hash == 'login') {
                login_toggle();
            } else if (hash == 'menu') {
                menu_toggle();
            }
            // hash found
        } else {
            // No hash found
        }

        //login
        $('form').on('submit', function (e) {
            $('body').loading({
                message: "Proses..."
            });
            e.preventDefault();

            $.ajax({
                type: 'post',
                url: '<?=base_url('sys/app/authenticate/')?>',
                data: $('form').serialize(),
                type: 'POST'
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                $('body').loading('stop');
                if (data.success) {
                    window.location.href = data.message;
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            })
        });
    });

    function layer_toggle() {
        var $top_layer = $('#top-layer');
        $top_layer.slideToggle();
        show = !show;
        if (show) {
            $('#toggle-cover').fadeOut();
            $('#main-content-login').hide();
            $('#main-content').slideDown();
            $('.sound-control').slideDown();
            $('.menu').hide();
            menu = false;
            if (sound) {
                $('#sound-control-on').hide();
                $('#sound-control-off').show();
            } else {
                $('#sound-control-on').show();
                $('#sound-control-off').hide();
            }
//            $video.css({
//                '-webkit-filter': 'blur(3px)',
//                '-moz-filter': 'blur(3px)',
//                '-o-filter': 'blur(3px)',
//                '-ms-filter': 'blur(3px)',
//                'filter': 'blur(3px)'
//            });
        } else {
            $('#toggle-cover').fadeIn();
            $('#main-content').slideUp();
            $('#main-content-login').slideUp();
            $('.sound-control').slideUp();
//            $video.css({
//                '-webkit-filter': 'blur(0px)',
//                '-moz-filter': 'blur(0px)',
//                '-o-filter': 'blur(0px)',
//                '-ms-filter': 'blur(0px)',
//                'filter': 'blur(0px)'
//            });
        }
    }

    function sound_toggle() {
        sound = !sound;
        if (sound) {
            $('#sound-control-on').hide();
            $('#sound-control-off').show();

            $video.prop('muted', false)
        } else {
            $('#sound-control-on').show();
            $('#sound-control-off').hide();

            $video.prop('muted', true)
        }
    }

    function menu_toggle() {
        menu = !menu;
        window.location = "#";

        if (menu) {
            $('.menu').slideDown('fast');
//            $('#main-content').slideUp();
//            $('#main-content-login').slideUp();
            login = false;
        } else {
            $('.menu').slideUp('fast');
            $('#main-content').slideDown();
        }
    }

    function background_click() {
//        $('.menu').slideUp('fast');
    }

    function login_toggle() {
        login = !login;
        if (login) {
            $('#main-content').slideUp();
            $('.menu').slideUp();
            $('#main-content-login').slideDown();
            menu = false;
        } else {
            $('#main-content').slideDown();
            $('#main-content-login').slideUp();
        }
    }

    function hover_menu(par) {
        var $head_icon = $('#menu-head-icon');
        var $head_text = $('#menu-head-text');
        switch (par) {
            case 'manage':
                $head_icon.html('<i class="fa fa-briefcase"></i>');
                $head_text.html('Manage');
                break;
            case 'katalog':
                $head_icon.html('<i class="fa fa-list"></i>');
                $head_text.html('Katalog');
                break;
            case 'login':
                $head_icon.html('<i class="fa fa-sign-in"></i>');
                $head_text.html('Log in');
                break;
            case 'logout':
                $head_icon.html('<i class="fa fa-sign-out"></i>');
                $head_text.html('Log out');
                break;
            default:
                hover_menu_out();
                break;
        }
    }

    function hover_menu_out() {
        var $head_icon = $('#menu-head-icon');
        var $head_text = $('#menu-head-text');

        $head_icon.html('<i class="fa fa-bars"></i>');

        $head_text.html('NCC Menu');
    }

</script>
</html>
