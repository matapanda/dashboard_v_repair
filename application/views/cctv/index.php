<?php
function template($id, $judul, $source)
{
    ?>
    <div class="col-md-4">
        <div class="box box-primary clearfix">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $judul ?></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-body -->
            <embed type="application/x-vlc-plugin" pluginspage="http://www.videolan.org" autoplay="yes" loop="no"
                   style="width: 100%" target="<?php echo $source; ?>"/>
            <object classid="clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921"
                    codebase="http://download.videolan.org/pub/videolan/vlc/last/win32/axvlc.cab"
                    style="display:none;"></object>

<!--            <video style="width: 100%;" muted onclick="toggle_video(this)">-->
<!--                <source src="--><?php //echo $source; ?><!--" type="video/mp4">-->
<!--                Browser tidak mensupport pemutaran vidio.-->
<!--            </video>-->

            <div class="btn-group-sm" style="padding: 5px">
                <a target="_blank" href="<?php echo base_url('cctv/' . $id) ?>" class="btn btn-primary btn-sm"><span
                            class="fa fa-video-camera"></span> Lihat</a>
                <div class="pull-right">
                    <button class="btn btn-default btn-sm"
                            onclick="show_modal_edit('<?php echo $id ?>','<?php echo $judul ?>','<?php echo $source ?>')">
                        <span class="fa fa-pencil"></span> Edit
                    </button>
                    <button class="btn btn-danger btn-sm"
                            onclick="remove_cctv('<?php echo base_url('api/app/cctv/' . $id) ?>')">
                        <span class="fa fa-trash"></span> Hapus
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<img class="background-ncc-logo" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>" alt="NCC logo">
<!-- Modal add-->
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
    <div class="modal-dialog" role="document">
        <div class="modal-content clearfix">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4>Tambah CCTV</h4>
            </div>
            <form class="form-add" onsubmit="event.preventDefault(); save_cctv();">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" name="judul" class="form-control" placeholder="Judul">
                    </div>
                    <div class="form-group">
                        <label for="source">Source</label>
                        <input type="text" name="source" class="form-control" placeholder="Source">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span
                                class="fa fa-check"></span> Simpan
                    </button>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><span
                                class="fa fa-times"></span> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal edit-->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content clearfix">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4>Edit CCTV</h4>
            </div>
            <form class="form-edit" onsubmit="event.preventDefault(); update_cctv();">
                <div class="modal-body">
                    <input type="hidden" name="idcctv" id="idcctv">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input id="input-judul" type="text" name="judul" class="form-control" placeholder="Judul">
                    </div>
                    <div class="form-group">
                        <label for="source">Source</label>
                        <input id="input-source" type="text" name="source" class="form-control" placeholder="Source">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><span
                                class="fa fa-check"></span> Simpan
                    </button>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal"><span
                                class="fa fa-times"></span> Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div style="position: absolute; right: 10px">
    <button class="btn btn-success" data-toggle='modal' data-target='#modal-add'>
        <span class="fa fa-plus-square"></span>&nbsp;&nbsp;Tambah CCTV
    </button>
</div>
<br>
<br>
<div>
    <?php foreach ($list_cctv as $cctv) {
        template($cctv->idcctv, $cctv->judul, $cctv->source);
    } ?>
</div>

<script>
    function save_cctv() {
        var $form_add = $('.form-add');
        $.ajax({
            url: "<?php echo base_url('api/app/cctv') ?>",
            data: $form_add.serialize(),
            type: "POST"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                swal({
                        title: "Berhasil",
                        text: data.message,
                        type: "success"
                    },
                    function () {
                        location.reload();
                    }
                );
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        })
    }


    function toggle_video(vid) {
        if (vid.paused) {
            vid.play();
            return;
        }
        vid.pause();
    }

    function show_modal_edit(id, judul, source) {
        $('#idcctv').val(id);
        $('#input-judul').val(judul);
        $('#input-source').val(source);
        $('#modal-edit').modal('show');
    }

    function update_cctv() {
        var $form_edit = $('.form-edit');
        $.ajax({
            url: "<?php echo base_url('api/app/cctv/') ?>" + $('#idcctv').val(),
            data: $form_edit.serialize(),
            type: "POST"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                swal({
                        title: "Berhasil",
                        text: data.message,
                        type: "success"
                    },
                    function () {
                        location.reload();
                    }
                );
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        })
    }

    function remove_cctv(api_url) {
        swal({
                title: "Konfirmasi Hapus?",
                text: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: api_url,
                    type: "DELETE"
                }).done(function (result) {
                    var data = jQuery.parseJSON(JSON.stringify(result));
                    if (data.success) {
                        swal({
                                title: "Berhasil",
                                text: data.message,
                                type: "success"
                            },
                            function () {
                                location.reload();
                            }
                        );
                    } else {
                        sweetAlert("gagal", data.message, "error");
                    }
                }).fail(function (xhr, status, errorThrown) {
                    sweetAlert("Gagal", status, "error");
                })
            });
    }
</script>