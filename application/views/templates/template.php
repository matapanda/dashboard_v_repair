<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->
    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">

    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/AdminLTE.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/skins/skin-blue.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/select2.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/alt/AdminLTE-select2.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/datepicker3.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/icheck/_all.css"); ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.css"/>

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="<?php echo base_url("assets/css/default.css"); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini fixed">
<div class="wrapper">
    <?php if (isset($template_header)) echo $template_header ?>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?php echo base_url("/assets/images/kota-malang.png"); ?>" class="" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?php echo ucfirst($user['username']) ?></p>
                    <a><i class="fa fa-circle text-success"></i> <?php echo ucfirst($user['skpd_skpd']) ?></a>
                </div>
            </div>
            <ul class="sidebar-menu">
                <?php if (isset($template_sidebar)) echo $template_sidebar ?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?php echo $title ?>
            </h1>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url() ?>"<i class="fa fa-home"></i> Home</a></li>
                            <li class="active"><?php echo $submodule ?></li>
                        </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <?php if ($submodule == 'Dashboard' || $submodule == 'cctv' ) : ?>
                    <?php if (isset($main_content)) echo $main_content ?>
                <?php else : ?>
                    <div class="col-xs-12">
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><?php echo $submodule ?></h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <?php if (isset($main_content)) echo $main_content ?>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                <?php endif; ?>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        <?php if (isset($sub_content)) : ?>
            <!-- Sub content -->
            <section class="content">
                <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo $sub_title; ?></h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <?php if (isset($sub_content)) echo $sub_content ?>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        <?php endif; ?>

    </div>
    <?php if (isset($template_footer)) echo $template_footer ?>
</div>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-3.1.3/pdfmake-0.1.27/dt-1.10.15/af-2.2.0/b-1.3.1/b-colvis-1.3.1/b-flash-1.3.1/b-html5-1.3.1/b-print-1.3.1/cr-1.3.3/fc-3.2.2/fh-3.1.2/kt-2.2.1/r-2.1.1/rg-1.0.0/rr-1.2.0/sc-1.4.2/se-1.2.2/datatables.min.js"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.slimscroll.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/fastclick.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap-datepicker.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/icheck.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/default.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/dropzone.js") ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<?php if (isset($script_plus)) echo $script_plus ?>
<?php if (isset($sub_script_plus)) echo $sub_script_plus ?>
</body>
</html>
