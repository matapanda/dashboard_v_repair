<!-- Modal Edit-->
<div class="modal fade" id="sub-modal-edit" tabindex="-1" role="dialog" aria-labelledby="sub-modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit <?php echo $sub_title ?></h4>
            </div>
            <form class="sub-form-update" onsubmit="event.preventDefault(); sub_update();">
                <div class="modal-body">
                    <?php echo $sub_general_form ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="fa fa-times"></span> Close
                    </button>
                    <button class="btn btn-primary"><span class="fa fa-save"></span> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Add-->
<div class="modal fade" id="sub-modal-add" tabindex="-1" role="dialog" aria-labelledby="sub-modal-add">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Tambah <?php echo $sub_title ?></h4>
            </div>
            <form class="sub-form-add" onsubmit="event.preventDefault(); sub_add();">
                <div class="modal-body">
                    <?php echo $sub_general_form ?>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span>
                        Close
                    </button>
                    <button class="btn btn-success"><span class="fa fa-plus"></span> Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="content-inner">
    <div style="margin-bottom:10px;">
        <button class='update_btn btn btn-success pull-right' onclick="sub_show_add()" data-toggle='modal'
                data-target='#sub-modal-add'><span class='fa fa-plus'>Tambah Data</span></button>
        <br>
    </div>
    <div class="table-responsive">
        <table id="sub-datatables" class="table table-bordered table-striped display nowrap dt-responsive">
            <thead>
            <tr>
                <th>No.</th>
                <?php echo $sub_index_datatables ?>
            </tr>
            </thead>
        </table>
    </div>
</div>
