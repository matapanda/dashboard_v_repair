<li class="header">Umum</li>
<li class="treeview <?php echo $submodule == 'Dashboard' ? 'active' : '' ?>" xmlns="http://www.w3.org/1999/html">
    <a href="<?php echo base_url().'dashboard' ?>">
        <i class="fa fa-dashboard fa-lg"></i><span> Dashboard</span>
    </a>
</li>
<li class="treeview" xmlns="http://www.w3.org/1999/html">
    <a target="_blank" href="<?php echo base_url().'map' ?>">
        <i class="fa fa-map-marker fa-lg"></i><span> Peta</span>
    </a>
</li>
<li class="treeview" xmlns="http://www.w3.org/1999/html">
    <a target="_blank" href="<?php echo base_url().'chart' ?>">
        <i class="fa fa-bar-chart fa-lg"></i><span> Grafik</span>
    </a>
</li>
<li class="treeview" xmlns="http://www.w3.org/1999/html">
    <a target="_blank" href="<?php echo base_url('katalog/0#e-sambat') ?>">
        <i class="fa fa-comments fa-lg"></i><span> E-Sambat</span>
    </a>
</li>
<li class="treeview" xmlns="http://www.w3.org/1999/html">
    <a target="_blank" href="<?php echo base_url().'katalog' ?>">
        <i class="fa fa-list fa-lg"></i><span> Katalog</span>
    </a>
</li>
<?php if ($user['ismultiskpd']): ?>

    <li class="header">Admin</li>
    <li class="treeview <?php echo $submodule == 'manage-user' ? 'active' : '' ?>">
        <a href="<?php echo base_url('sys/manage/user') ?>"><i class="fa fa-group"></i><span>Manajemen User</span></a>
    </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('gelandangan psikotik', true) OR
    $this->auth->check_read('kis pbi-d', true) OR
    $this->auth->check_read('kis pbi-n', true) OR
    $this->auth->check_read('kks', true) OR
    $this->auth->check_read('pkh', true) OR
    $this->auth->check_read('pps', true) OR
    $this->auth->check_read('razia', true) OR
    $this->auth->check_read('pos resos', true)
):
    ?>
    <li class="header">DINSOS</span></li>
<?php endif; ?>

<?php if ($this->auth->check_read('gelandangan psikotik', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'gelandangan psikotik' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/gelandangan-psikotik') ?>">
            <i class="fa fa-book"></i><span>Gelandangan Psikotik</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('kis pbi-d', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'kis pbi-d' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/kispbid') ?>">
            <i class="fa fa-book"></i><span>KIS PBI-D</span></a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('kis pbi-n', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'kis pbi-n' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/kispbin') ?>">
            <i class="fa fa-book"></i> <span>KIS PBI-N</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('kks', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'kks' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/kks') ?>">
            <i class="fa fa-book"></i> <span>KKS</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('pkh', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'pkh' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/pkh') ?>">
            <i class="fa fa-book"></i> <span>PKH</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('pps', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'pps' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/pps') ?>">
            <i class="fa fa-book"></i> <span>PPS</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('razia', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'razia' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/razia') ?>">
            <i class="fa fa-book"></i> <span>Razia</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('pos resos', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'pos resos' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinsos/resos') ?>">
            <i class="fa fa-book"></i> <span>POS RESOS</span>
        </a>
    </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('data pkk', true) OR
    $this->auth->check_read('data posyandu', true) OR
    $this->auth->check_read('hasil pendapatan keluarga', true) OR
    $this->auth->check_read('hasil pendapatan keluarga tahu', true) OR
    $this->auth->check_read('kdrt', true)
):
    ?>
    <li class="header">DP3P2KB</li>
<?php endif; ?>

<?php if ($this->auth->check_read('data pkk', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'data pkk' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dp3p2kb/data-pkk') ?>">
            <i class="fa fa-book"></i> <span>Data PKK</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('data posyandu', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'data posyandu' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dp3p2kb/data-posyandu') ?>">
            <i class="fa fa-book"></i> <span>Data Posyandu</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('hasil pendapatan keluarga', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'hasil pendapatan keluarga' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dp3p2kb/hasil-pendapatan-keluarga') ?>">
            <i class="fa fa-book"></i> <span>Hasil Pendataan Keluarga</span>
        </a>
    </li>
<?php endif; ?>


<?php if ($this->auth->check_read('hasil pendapatan keluarga tahu', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'hasil pendapatan keluarga tahu' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dp3p2kb/hasil-pendapatan-keluarga-tahu') ?>">
            <i class="fa fa-book"></i> <span>Hasil Pendataan Keluarga Tahu</span>
        </a>
    </li>
<?php endif; ?>


<?php if ($this->auth->check_read('kdrt', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'kdrt' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dp3p2kb/kdrt') ?>">
            <i class="fa fa-book"></i> <span>KDRT</span>
        </a>
    </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('rekapitulasi koleksi', true) OR
    $this->auth->check_read('statistik peminjaman', true)
):
    ?>
    <li class="header">Dinas Perpustakaan</li>
<?php endif; ?>

<?php if ($this->auth->check_read('rekapitulasi koleksi', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'rekapitulasi koleksi' ? 'active' : '' ?>">
        <a href="<?php echo base_url('perpus/rekapitulasi-koleksi') ?>">
            <i class="fa fa-book"></i> <span>Rekapitulasi Koleksi</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('statistik peminjaman', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'statistik peminjaman' ? 'active' : '' ?>">
        <a href="<?php echo base_url('perpus/statistik-peminjaman') ?>">
            <i class="fa fa-book"></i> <span>Statistik Peminjaman</span>
        </a>
    </li>
<?php endif; ?>
<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('pertamanan', true) OR
    $this->auth->check_read('pemakaman', true) OR
    $this->auth->check_read('rusunawa', true)
    ):
?>

    <li class="header">Dinas Perumahan</li>
<?php endif; ?>

<?php if ($this->auth->check_read('perumahan', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'pertamanan' ? 'active' : '' ?>">
        <a href="<?php echo base_url('perkim/pertamanan') ?>">
            <i class="fa fa-book"></i><span>Pertamanan</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('pemakaman', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'pemakaman' ? 'active' : '' ?>">
        <a href="<?php echo base_url('perkim/pemakaman') ?>">
            <i class="fa fa-book"></i><span>Pemakaman</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('rusunawa', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'rusunawa' ? 'active' : '' ?>">
        <a href="<?php echo base_url('perkim/rusunawa') ?>">
            <i class="fa fa-book"></i><span>Rusunawa</span>
        </a>
    </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('perwal', true) OR
    $this->auth->check_read('perda', true)
):
?>
    <li class="header">Bagian Hukum</li>
<?php endif; ?>

<?php if ($this->auth->check_read('perwal', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'perwal' ? 'active' : '' ?>">
        <a href="<?php echo base_url('hukum/perwal') ?>">
            <i class="fa fa-book"></i><span>Hukum Perwal</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('perda', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'perda' ? 'active' : '' ?>">
        <a href="<?php echo base_url('hukum/perda') ?>">
            <i class="fa fa-book"></i><span>Hukum Perda</span>
        </a>
    </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('akta catatan sipil', true) OR
    $this->auth->check_read('orang asing', true) OR
    $this->auth->check_read('penduduk', true)
):
    ?>
    <li class="header">Dinas Kependudukan</li>
<?php endif; ?>

<?php if ($this->auth->check_read('akta catatan sipil', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'akta catatan sipil' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinas-kependudukan/akta-catatan-sipil') ?>">
            <i class="fa fa-book"></i><span>Akta Catatan Sipil</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('orang asing', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'orang asing' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinas-kependudukan/orang-asing') ?>">
            <i class="fa fa-book"></i><span>Jumlah Penduduk</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('penduduk', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'penduduk' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinas-kependudukan/penduduk') ?>">
            <i class="fa fa-book"></i><span>Agregat Data Penduduk</span>
        </a>
    </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('rekapitulasi pns', true)
):
    ?>
    <li class="header">BKD</li>
<?php endif; ?>

<?php if ($this->auth->check_read('rekapitulasi pns', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'rekapitulasi pns' ? 'active' : '' ?>">
        <a href="<?php echo base_url('bkd/rekapitulasi-pns') ?>">
            <i class="fa fa-book"></i><span>Rekapitulasi PNS</span>
        </a>
    </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
$this->auth->check_read('statistik sektoral', true)
):
?>
<li class="header">Badan Pelayanan Pajak Daerah</li>
<?php endif; ?>

<?php if ($this->auth->check_read('statistik sektoral', true)): ?>
  <li class="treeview <?php echo strtolower($submodule) == 'statistik sektoral' ? 'active' : '' ?>">
    <a href="<?php echo base_url('bp2d/statistik-sektoral') ?>">
      <i class="fa fa-book"></i><span>Statistik Sektoral</span>
    </a>
  </li>
<?php endif; ?>

<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('data bumil', true) OR
    $this->auth->check_read('data gizi buruk', true) OR
    $this->auth->check_read('data tb', true)
):
    ?>
    <li class="header">Dinas Kesehatan</li>
<?php endif; ?>

<?php if ($this->auth->check_read('data bumil', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'data bumil' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinkes/data-bumil') ?>">
            <i class="fa fa-book"></i><span>Data Bumil</span>
        </a>
    </li>
<?php endif; ?>
<?php if ($this->auth->check_read('data gizi buruk', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'data gizi buruk' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinkes/data-gizi-buruk') ?>">
            <i class="fa fa-book"></i><span>Data Gizi Buruk</span>
        </a>
    </li>
<?php endif; ?>
<?php if ($this->auth->check_read('data tb', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'data tb' ? 'active' : '' ?>">
        <a href="<?php echo base_url('dinkes/data-tb') ?>">
            <i class="fa fa-book"></i><span>Data TB</span>
        </a>
    </li>
<?php endif; ?>


<?php
if ($user['ismultiskpd'] OR
    $this->auth->check_read('jasa akomodasi', true) OR
    $this->auth->check_read('data dan profil', true)  OR
    $this->auth->check_read('cagar budaya', true) OR
    $this->auth->check_read('pad kota', true) 
):
    ?>
    <li class="header">Dinas Kebudayaan dan Pariwisata</li>
<?php endif; ?>

<?php if ($this->auth->check_read('jasa akomodasi', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'jasa akomodasi' ? 'active' : '' ?>">
        <a href="<?php echo base_url('budpar/jasa-akomodasi') ?>">
            <i class="fa fa-book"></i><span>Jasa Akomodasi dan Penginapan</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('data dan profil', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'data dan profil' ? 'active' : '' ?>">
        <a href="<?php echo base_url('budpar/data-profil') ?>">
            <i class="fa fa-book"></i><span>Data dan Profil Kunjungan Wisata</span>
        </a>
    </li>
<?php endif; ?>

<?php if ($this->auth->check_read('cagar budaya', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'cagar budaya' ? 'active' : '' ?>">
        <a href="<?php echo base_url('budpar/cagar-budaya') ?>">
            <i class="fa fa-book"></i><span>Cagar Budaya</span>
        </a>
    </li>
<?php endif; ?>

 <?php if ($this->auth->check_read('pad kota', true)): ?>
    <li class="treeview <?php echo strtolower($submodule) == 'pad kota' ? 'active' : '' ?>">
        <a href="<?php echo base_url('budpar/pad-kota') ?>">
            <i class="fa fa-book"></i><span>PAD Kota Malang</span>
        </a>
    </li>
<?php endif; ?>