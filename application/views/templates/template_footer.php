<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.1.0
    </div>
    Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo base_url() ?>">Ngalam Command Center</a>. All rights reserved
</footer>