<!-- Modal Edit-->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit <?php echo $title ?></h4>
            </div>
            <form class="form-update" onsubmit="event.preventDefault(); update();">
                <div class="modal-body">
                    <?php echo $general_form ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="fa fa-times"></span> Close
                    </button>
                    <button class="btn btn-primary"><span class="fa fa-save"></span> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Add-->
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Tambah <?php echo $title ?></h4>
            </div>
            <form class="form-add" onsubmit="event.preventDefault(); add();">
                <div class="modal-body">
                    <?php echo $general_form ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span>
                        Close
                    </button>
                    <button class="btn btn-success"><span class="fa fa-plus"></span> Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="content-inner">
    <div style="margin-bottom:10px;">
        <div class="clearfix">

            <?php if (isset($api_url_upload)): ?>
                <div class="pull-left">
                    <form id="form_upload_excel" action="<?php echo $api_url_upload; ?>" method="post"
                          enctype="multipart/form-data" class="form-inline">
                        <input id="excel_file" type="file" name="excel_file" class="form-control"/>
                        <button type="submit" class='btn btn-success'>
                            <span class='fa fa-file-excel-o'> Upload Excel</span>
                        </button>
                        <br>
                        <a target="_blank" href="<?php echo $url_file_format ?>">Contoh Format Excel</a>
                    </form>
                </div>
            <?php endif; ?>

            <!--<button class='add_btn btn btn-success pull-right' onclick="showAdd()" data-toggle='modal'
                    data-target='#modal-add'><span class='fa fa-plus'> Tambah Data</span></button> -->
        </div>

    </div>
    <div class="table-responsive">
        <table id="datatables" class="table table-bordered table-striped display nowrap dt-responsive">
            <thead>
            <tr>
                <?php if ($submodule != 'manage-user') {
                    //echo '<th>No.</th>';
                }
                 ?>
                
                <?php echo $index_datatables ?>
            </tr>
            </thead>
        </table>
    </div>
    <?php if (isset($data_count)) { ?>
        <br>
        <div class="alert alert-info alert-lg" style="display: inline-block;">
            <b><?php echo $data_count ?> Data</b>
            <?php if (isset($data_count_special)) {
                echo '( Sama dengan jumlah <b>CELL</b> terisi (TIDAK KOSONG) pada <b>Excel <span class="fa fa-file-excel-o"></span></b> )';
            } else {
                echo '( Sama dengan jumlah <b>BARIS</b> terisi (TIDAK KOSONG)  pada <b>Excel <span class="fa fa-file-excel-o"></span></b> )';
            } ?>
        </div>
    <?php } ?>
</div>
