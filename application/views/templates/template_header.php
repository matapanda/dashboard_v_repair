<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url() ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
<!--        <span class="logo-mini">NCC |</span>-->
        <img class="logo-mini" src="<?php echo base_url("/assets/images/ncc-logo-white.png"); ?>" alt="NCC logo">
        <!-- logo for regular state and mobile devices -->
<!--        <span class="logo-lg">NCC |</span>-->
        <img height="50px" style="padding: 5px" src="<?php echo base_url("/assets/images/ncc-logo-white.png"); ?>" alt="NCC logo">


    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url("/assets/images/kota-malang.png"); ?>" class="user-image" alt="User Image">
                        <span class="hidden-xs"><?php echo ucfirst($user['username']) ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url("/assets/images/kota-malang.png"); ?>" alt="User Image">

                            <p>
                                <?php echo ucfirst($user['username']).' - '.ucfirst($user['skpd_skpd']) ?>
                                <br>
                                <?php if($user['statusaktif'] == 1){
                                    echo '<small>Akun Aktif</small>';
                                }else{
                                    echo '<small>Tidak Aktif</small>';
                                } ?>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="row">
                                <div class="col-xs-6 text-center">
                                    <a href="<?php echo base_url() ?>"><span class="fa fa-home"></span> Home</a>
                                </div>
                                <div class="col-xs-6 text-center">
                                    <a href="<?php echo base_url().'katalog' ?>"><span class="fa fa-list"></span> Katalog</a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <?php if ($user['ismultiskpd'] == 1): ?>
                                <div class="pull-left">
                                    <a href="<?php echo  base_url('/sys/manage/user') ?>" class="btn btn-info btn-flat"><span><span class="fa fa-briefcase"></span> Manajemen User</a>
                                </div>
                            <?php endif; ?>
                            <div class="pull-right">
                                <a href="<?= base_url() ?>sys/app/logout" class="btn btn-danger btn-flat"><span class="fa fa-power-off"></span> Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </nav>
</header>