<style>
    .info-box:hover {
        box-shadow: 0 3px 20px rgba(0,0,0,0.08), 0 3px 20px rgba(0,0,0,0.08);
        /*font-weight: bold;*/
        background-color: #F7F7F7;
    }
    .info-box{
        border-radius: 5px;
    }
</style>
<img class="background-ncc-logo" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>" alt="NCC logo">

 <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                  <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?php echo base_url("/assets/images/coba.png"); ?>" alt="First slide">

                    <div class="carousel-caption">
                      Dashboard
                    </div>
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url("/assets/images/coba.png"); ?>" alt="Second slide">

                    <div class="carousel-caption">
                     Dashboard
                    </div>
                  </div>
                  <div class="item">
                    <img src="<?php echo base_url("/assets/images/coba.png"); ?>" alt="Third slide">

                    <div class="carousel-caption">
                      Dashboard
                    </div>
                  </div>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
            <br>
            <br>
            <!-- /.box-body -->
<?php
function item_template($index, $video_file, $thumbnail_file, $text_title, $count)
{
    ?>
    <a target="_blank" href="<?php echo base_url('katalog/' . $index) ?>" style="color: black"
       title="<?php echo $text_title ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon"><img class="img-responsive" style="height: 100%; border-radius: 5px"
                                                 src="<?php echo base_url("/assets/images/" . $thumbnail_file); ?>"></span>
                <div class="info-box-content text-center">
                    <p style="font-size: 16px; padding-top: 10px"><?php echo $text_title ?></p>
                    <?php if ($count <= 0) { ?>
                        <span class="info-box-number label label-warning"><small>Kosong</small></span>
                    <?php } else { ?>
                        <span class="info-box-number label label-primary"><?php echo $count ?> <small>Data</small></span>
                    <?php } ?>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </a>
    <?php
}

?>
<?php
for ($i = 0; $i < count($list_text_title); $i++) {
    item_template($i, $list_videos[$i], $list_thumbnails[$i], $list_text_title[$i], $list_count[$i]);
}
?>

<script>

</script>