<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?= base_url('/assets/images/favicon.ico') ?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bulma.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

    <style>
        html, body {
            background-color: #FBFBFB;
            width: 100%;
            height: 100%;
        }

        #video-primary {
            margin-left: auto;
            margin-right: auto;
            display: block;
            width: 700px;
        }

        #sound-control {
            position: fixed;
            right: 0;
            bottom: 0;
            z-index: 1000;
        }
    </style>

</head>
<body>
<img class="background-ncc-logo-full" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>"
     alt="NCC logo">
<div id="sound-control">
    <button id="sound-off" onclick="sound_toggle()" class="button is-danger" title="Mute"><span
            class="fa fa-volume-down"></span></button>
    <button id="sound-on" onclick="sound_toggle()" class="button is-white" title="Unmute"><span
            class="fa fa-volume-up"></span></button>
</div>

<section class="hero is-primary is-medium">
    <div class="hero-body" style="padding-top: 30px; padding-bottom: 30px; width: 100%; height: 100%;">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <video class="card" id="video-primary" loop muted autoplay onclick="toggle_video(this)"
                           style="margin-bottom: 40px">
                        <source src="<?php echo base_url("/assets/videos/" . $video); ?>" type="video/mp4">
                        <source src="<?php echo base_url("/assets/videos/" . $video); ?>" type="video/mp4">
                        Browser tidak mensupport pemutaran vidio.
                    </video>
                    <div class="is-pulled-left">
                        <h1 class="title">
                            NCC Katalog <?php echo $text_title ?>
                        </h1>
                        <hr style="width: 40%">
                        <h2 class="subtitle">
                            <?php echo $text_description ?>
                        </h2>
                        <a class="button is-primary is-outlined is-inverted" href="<?php echo base_url('dashboard') ?>"><span
                                class="fa fa-dashboard"></span>&nbsp;Dashboard</a>
                    </div>
                </div>
                <div class="column" id="katalog-table-wrapper" style="height: 100%; overflow: auto;">
                    <?php if (count($list_katalog) > 0) { ?>
                        <table class="table is-striped box" style="font-size: 14px">
                            <thead>
                            <tr>
                                <th colspan="6" style="text-align: center">Katalog</th>
                            </tr>
                            <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Tipe</th>
                                <th>Tautan</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list_katalog as $katalog) { ?>
                                <tr>
                                    <td><?php echo $katalog['no'] ?></td>
                                    <td><?php echo $katalog['judul'] ?></td>
                                    <td><?php echo $katalog['deskripsi'] ?></td>
                                    <td>
                                        <?php if ($katalog['idtipe'] == 1) { ?>
                                            <span title="Grafik" class="fa fa-bar-chart"></span>
                                        <?php } else { ?>
                                            <span title="Peta" class="fa fa-map-marker"></span>
                                        <?php } ?>
                                    </td>
                                    <td><a target="_blank" class="button is-link"
                                           href="<?php echo base_url($katalog['url']) ?>">Tautan</a></td>
                                    <td>
                                        <button onclick="remove_katalog('<?php echo $katalog['delete_url'] ?>')"
                                                title="Hapus Katalog" class="button is-danger is-small"><span
                                                class="fa fa-trash"></span></button>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="notification is-info">
                            Belum ada data untuk kategori <?php echo $text_title ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>

<script>
    var $video = $('video');
    var sound = false;

    $(function () {
        //loading screen
        jQuery(document).ajaxStart(function () {
            $('body').loading({
                message: 'Proses...'
            });
        });
        jQuery(document).ajaxStop(function () {
            $('body').loading('stop');
        });

        $('#sound-off').hide();
//        set_table_katalog_to_default()
    });

    function toggle_video(vid) {
        if (vid.paused) {
            vid.play();
            return;
        }
        vid.pause();
    }

    function sound_toggle() {
        sound = !sound;
        if (sound) {
            $('#sound-on').hide();
            $('#sound-off').show();

            $video.prop('muted', false)
        } else {
            $('#sound-on').show();
            $('#sound-off').hide();

            $video.prop('muted', true)
        }
    }

    function remove_katalog(api_url) {
        swal({
                title: "Konfirmasi Hapus?",
                text: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: api_url,
                    type: "DELETE"
                }).done(function (result) {
                    var data = jQuery.parseJSON(JSON.stringify(result));
                    if (data.success) {
                        swal({
                                title: "Berhasil",
                                text: data.message,
                                type: "success"
                            },
                            function() {
                                location.reload();
                            }
                        );
                    } else {
                        sweetAlert("gagal", data.message, "error");
                    }
                }).fail(function (xhr, status, errorThrown) {
                    sweetAlert("Gagal", status, "error");
                })
            });
    }
</script>
</html>
