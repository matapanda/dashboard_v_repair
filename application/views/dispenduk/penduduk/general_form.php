<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" id="tanggal" placeholder="Tanggal " class="form-control" required>
</div>
<div class="form-group">
    <label for="idkecamatan">kecamatan</label><br>
    <select name="idkecamatan" id="idkecamatan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="jumlahkk">Jumlah KK</label>
    <input name="jumlahkk" type="number" id="jumlahkk" placeholder="Jumlah KK" class="form-control" required>
</div>
<table class="table table-bordered text-center">
    <tr>
        <th>Penduduk Akhir Bulan Ini</th>
    </tr>
    <tr>
        <td>
            <label for="pendudukakhirbulanini_l">Laki-Laki</label>
            <input name="pendudukakhirbulanini_l" type="number" id="pendudukakhirbulanini_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="pendudukakhirbulanini_p">Perempuan</label>
            <input name="pendudukakhirbulanini_p" type="number" id="pendudukakhirbulanini_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th>Wajib Memiliki KTP</th>
    </tr>
    <tr>
        <td>
            <label for="wajibmemilikiktp_l">Laki-Laki</label>
            <input name="wajibmemilikiktp_l" type="number" id="wajibmemilikiktp_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="wajibmemilikiktp_p">Perempuan</label>
            <input name="wajibmemilikiktp_p" type="number" id="wajibmemilikiktp_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>
<div class="form-group">
    <label for="memilikiktp">Memiliki KTP</label>
    <input name="memilikiktp" type="number" id="memilikiktp" placeholder="Memiliki KTP" class="form-control" required>
</div>
<div class="form-group">
    <label for="belumemilikiktp">Belum Memiliki KTP</label>
    <input name="belumemilikiktp" type="number" id="belumemilikiktp" placeholder="Belum Memiliki KTP" class="form-control" required>
</div>
<div class="form-group">
    <label for="wajibmemilikiaktekelahiran">Wajib Memiliki Akta Kelahiran</label>
    <input name="wajibmemilikiaktekelahiran" type="number" id="wajibmemilikiaktekelahiran" placeholder="Wajib Memiliki Akta Kelahiran" class="form-control" required>
</div>
<div class="form-group">
    <label for="memilikiaktakelahiran">Memiliki Akta Kelahiran</label>
    <input name="memilikiaktakelahiran" type="number" id="memilikiaktakelahiran" placeholder="Memiliki Akta Kelahiran" class="form-control" required>
</div>
