<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns =  [
            {"data": "no"},
            {"data": "kecamatan"},
            {"data": "tanggal"},
            {"data": "jumlahkk"},
            {"data": "pendudukakhirbulanini_l"},
            {"data": "pendudukakhirbulanini_p"},
            {"data": "wajibmemilikiktp_l"},
            {"data": "wajibmemilikiktp_p"},
            {"data": "memilikiktp"},
            {"data": "belumemilikiktp"},
            {"data": "wajibmemilikiaktekelahiran"},
            {"data": "memilikiaktakelahiran"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinas-kependudukan/penduduk/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idpenduduk;

            $('#tanggal').val(data_for_table.tanggal);
            $('#idkecamatan').val(data_for_table.idkecamatan);
            $('#jumlahkk').val(data_for_table.jumlahkk);
            $('#pendudukakhirbulanini_l').val(data_for_table.pendudukakhirbulanini_l);
            $('#pendudukakhirbulanini_p').val(data_for_table.pendudukakhirbulanini_p);
            $('#wajibmemilikiktp_l').val(data_for_table.wajibmemilikiktp_l);
            $('#wajibmemilikiktp_p').val(data_for_table.wajibmemilikiktp_p);
            $('#memilikiktp').val(data_for_table.memilikiktp);
            $('#belumemilikiktp').val(data_for_table.belumemilikiktp);
            $('#wajibmemilikiaktekelahiran').val(data_for_table.wajibmemilikiaktekelahiran);
            $('#memilikiaktakelahiran').val(data_for_table.memilikiaktakelahiran);

            fill_select_master('kecamatan', "<?php echo base_url() ?>", [$("#idkecamatan")], data_for_table);


        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinas-kependudukan/penduduk/') ?>"+data.idpenduduk, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kecamatan', "<?php echo base_url() ?>", [$(".form-add #idkecamatan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinas-kependudukan/penduduk/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinas-kependudukan/penduduk/') ?>"+data_id, datatables);
    }
</script>