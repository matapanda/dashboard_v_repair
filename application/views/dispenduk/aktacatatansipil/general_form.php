<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="idkecamatan">Kecamatan</label><br>
    <select name="idkecamatan" id="idkecamatan" class="form-control"></select>
</div>
<table class="table table-bordered text-center">
    <tr>
       <th colspan="2">
            Akta Kelahiran
       </th>
    </tr>
    <tr>
        <td>
            <label for="aktakelahiran_umum">Umum</label>
            <input name="aktakelahiran_umum" type="number" id="aktakelahiran_umum" placeholder="Umum" class="form-control" required>
        </td>
        <td>
            <label for="aktakelahiran_terlambat">Terlambat</label>
            <input name="aktakelahiran_terlambat" type="number" id="aktakelahiran_terlambat" placeholder="Umum" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Akta Perkawinan
        </th>
    </tr>
    <tr>
        <td>
            <label for="aktaperkawinan_bulanyanglalu">Bulan Lalu</label>
            <input name="aktaperkawinan_bulanyanglalu" type="number" id="aktaperkawinan_bulanyanglalu" placeholder="Bulan Lalu" class="form-control" required>
        </td>
        <td>
            <label for="aktaperkawinan_bulanini">Bulain Ini</label>
            <input name="aktaperkawinan_bulanini" type="number" id="aktaperkawinan_bulanini" placeholder="Bulan Ini" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Akta Perceraian
        </th>
    </tr>
    <tr>
        <td>
            <label for="aktaperceraian_bulanyanglalu">Bulan Lalu</label>
            <input name="aktaperceraian_bulanyanglalu" type="number" id="aktaperceraian_bulanyanglalu" placeholder="Bulan Lalu" class="form-control" required>
        </td>
        <td>
            <label for="aktaperceraian_bulanini">Bulain Ini</label>
            <input name="aktaperceraian_bulanini" type="number" id="aktaperceraian_bulanini" placeholder="Bulan Ini" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Akta Kematian
        </th>
    </tr>
    <tr>
        <td>
            <label for="aktakematian_bulanyanglalu">Bulan Lalu</label>
            <input name="aktakematian_bulanyanglalu" type="number" id="aktakematian_bulanyanglalu" placeholder="Bulan Lalu" class="form-control" required>
        </td>
        <td>
            <label for="aktakematian_bulanini">Bulain Ini</label>
            <input name="aktakematian_bulanini" type="number" id="aktakematian_bulanini" placeholder="Bulan Ini" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Akta Pengangkatan
        </th>
    </tr>
    <tr>
        <td>
            <label for="aktapengangkatan_bulanyanglalu">Bulan Lalu</label>
            <input name="aktapengangkatan_bulanyanglalu" type="number" id="aktapengangkatan_bulanyanglalu" placeholder="Bulan Lalu" class="form-control" required>
        </td>
        <td>
            <label for="aktapengangkatan_bulanini">Bulain Ini</label>
            <input name="aktapengangkatan_bulanini" type="number" id="aktapengangkatan_bulanini" placeholder="Bulan Ini" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Akta Pengakuan
        </th>
    </tr>
    <tr>
        <td>
            <label for="aktapengakuan_bulanyanglalu">Bulan Lalu</label>
            <input name="aktapengakuan_bulanyanglalu" type="number" id="aktapengakuan_bulanyanglalu" placeholder="Bulan Lalu" class="form-control" required>
        </td>
        <td>
            <label for="aktapengakuan_bulanini">Bulain Ini</label>
            <input name="aktapengakuan_bulanini" type="number" id="aktapengakuan_bulanini" placeholder="Bulan Ini" class="form-control" required>
        </td>
    </tr>
</table>
<table class="table table-bordered text-center">
    <tr>
        <th colspan="2">
            Akta Pengesahan
        </th>
    </tr>
    <tr>
        <td>
            <label for="aktapengesahan_bulanyanglalu">Bulan Lalu</label>
            <input name="aktapengesahan_bulanyanglalu" type="number" id="aktapengesahan_bulanyanglalu" placeholder="Bulan Lalu" class="form-control" required>
        </td>
        <td>
            <label for="aktapengesahan_bulanini">Bulain Ini</label>
            <input name="aktapengesahan_bulanini" type="number" id="aktapengesahan_bulanini" placeholder="Bulan Ini" class="form-control" required>
        </td>
    </tr>
</table>