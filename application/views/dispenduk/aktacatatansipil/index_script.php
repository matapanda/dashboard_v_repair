<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns =  [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kecamatan"},
            {"data": "aktakelahiran_umum"},
            {"data": "aktakelahiran_terlambat"},
            {"data": "aktaperkawinan_bulanyanglalu"},
            {"data": "aktaperkawinan_bulanini"},
            {"data": "aktaperceraian_bulanyanglalu"},
            {"data": "aktaperceraian_bulanini"},
            {"data": "aktakematian_bulanyanglalu"},
            {"data": "aktakematian_bulanini"},
            {"data": "aktapengangkatan_bulanyanglalu"},
            {"data": "aktapengangkatan_bulanini"},
            {"data": "aktapengakuan_bulanyanglalu"},
            {"data": "aktapengakuan_bulanini"},
            {"data": "aktapengesahan_bulanyanglalu"},
            {"data": "aktapengesahan_bulanini"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinas-kependudukan/akta-catatan-sipil/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idaktacatatansipil;

            $('#tanggal').val(data_for_table.tanggal);
            $('#idkecamatan').val(data_for_table.idkecamatan);
            $('#aktakelahiran_umum').val(data_for_table.aktakelahiran_umum);
            $('#aktakelahiran_terlambat').val(data_for_table.aktakelahiran_terlambat);
            $('#aktaperkawinan_bulanyanglalu').val(data_for_table.aktaperkawinan_bulanyanglalu);
            $('#aktaperkawinan_bulanini').val(data_for_table.aktaperkawinan_bulanini);
            $('#aktaperceraian_bulanyanglalu').val(data_for_table.aktaperceraian_bulanyanglalu);
            $('#aktaperceraian_bulanini').val(data_for_table.aktaperceraian_bulanini);
            $('#aktakematian_bulanyanglalu').val(data_for_table.aktakematian_bulanyanglalu);
            $('#aktakematian_bulanini').val(data_for_table.aktakematian_bulanini);
            $('#aktapengangkatan_bulanyanglalu').val(data_for_table.aktapengangkatan_bulanyanglalu);
            $('#aktapengangkatan_bulanini').val(data_for_table.aktapengangkatan_bulanini);
            $('#aktapengakuan_bulanyanglalu').val(data_for_table.aktapengakuan_bulanyanglalu);
            $('#aktapengakuan_bulanini').val(data_for_table.aktapengakuan_bulanini);
            $('#aktapengesahan_bulanyanglalu').val(data_for_table.aktapengesahan_bulanyanglalu);
            $('#aktapengesahan_bulanini').val(data_for_table.aktapengesahan_bulanini);

            fill_select_master('kecamatan', "<?php echo base_url() ?>", [$("#idkecamatan")], data_for_table);


        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinas-kependudukan/akta-catatan-sipil/') ?>"+data.idaktacatatansipil, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kecamatan', "<?php echo base_url() ?>", [$(".form-add #idkecamatan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinas-kependudukan/akta-catatan-sipil/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinas-kependudukan/akta-catatan-sipil/') ?>"+data_id, datatables);
    }
</script>