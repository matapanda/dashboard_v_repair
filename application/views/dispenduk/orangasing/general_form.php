<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="idkecamatan">Kecamatan</label><br>
    <select name="idkecamatan" id="idkecamatan" class="form-control"></select>
</div>
<table class="table-bordered table text-center">
    <tr>
        <th colspan="2">Penduduk Awal Bulan Ini</th>
    </tr>
    <tr>
        <td>
            <label for="pendudukawalbulanini_l">Laki-laki</label>
            <input name="pendudukawalbulanini_l" type="number" id="pendudukawalbulanini_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="pendudukawalbulanini_p">Perempuan</label>
            <input name="pendudukawalbulanini_p" type="number" id="pendudukawalbulanini_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>

<table class="table-bordered table text-center">
    <tr>
        <th colspan="2">Lahir Bulan Ini</th>
    </tr>
    <tr>
        <td>
            <label for="lahirbulanini_l">Laki-laki</label>
            <input name="lahirbulanini_l" type="number" id="lahirbulanini_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="lahirbulanini_p">Perempuan</label>
            <input name="lahirbulanini_p" type="number" id="lahirbulanini_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>

<table class="table-bordered table text-center">
    <tr>
        <th colspan="2">Mati Bulan Ini</th>
    </tr>
    <tr>
        <td>
            <label for="matibulanini_l">Laki-laki</label>
            <input name="matibulanini_l" type="number" id="matibulanini_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="matibulanini_p">Perempuan</label>
            <input name="matibulanini_p" type="number" id="matibulanini_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>

<table class="table-bordered table text-center">
    <tr>
        <th colspan="2">Pendatang Bulan Ini</th>
    </tr>
    <tr>
        <td>
            <label for="pendatangbulanini_l">Laki-laki</label>
            <input name="pendatangbulanini_l" type="number" id="pendatangbulanini_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="pendatangbulanini_p">Perempuan</label>
            <input name="pendatangbulanini_p" type="number" id="pendatangbulanini_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>

<table class="table-bordered table text-center">
    <tr>
        <th colspan="2">Pindah Bulan Ini</th>
    </tr>
    <tr>
        <td>
            <label for="pindahbulanini_l">Laki-laki</label>
            <input name="pindahbulanini_l" type="number" id="pindahbulanini_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="pindahbulanini_p">Perempuan</label>
            <input name="pindahbulanini_p" type="number" id="pindahbulanini_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>

<table class="table-bordered table text-center">
    <tr>
        <th colspan="2">Penduduk Akhir Bulan Ini</th>
    </tr>
    <tr>
        <td>
            <label for="pendudukakhirbulanini_l">Laki-laki</label>
            <input name="pendudukakhirbulanini_l" type="number" id="pendudukakhirbulanini_l" placeholder="Laki-laki" class="form-control" required>
        </td>
        <td>
            <label for="pendudukakhirbulanini_p">Perempuan</label>
            <input name="pendudukakhirbulanini_p" type="number" id="pendudukakhirbulanini_p" placeholder="Perempuan" class="form-control" required>
        </td>
    </tr>
</table>