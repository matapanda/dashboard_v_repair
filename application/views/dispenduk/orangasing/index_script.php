<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns =  [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kecamatan"},
            {"data": "pendudukawalbulanini_l"},
            {"data": "pendudukawalbulanini_p"},
            {"data": "lahirbulanini_l"},
            {"data": "lahirbulanini_p"},
            {"data": "matibulanini_l"},
            {"data": "matibulanini_p"},
            {"data": "pendatangbulanini_l"},
            {"data": "pendatangbulanini_p"},
            {"data": "pindahbulanini_l"},
            {"data": "pindahbulanini_p"},
            {"data": "pendudukakhirbulanini_l"},
            {"data": "pendudukakhirbulanini_p"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinas-kependudukan/orang-asing/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idorangasing;

            $('#tanggal').val(data_for_table.tanggal);
            $('#idkecamatan').val(data_for_table.idkecamatan);
            $('#pendudukawalbulanini_l').val(data_for_table.pendudukawalbulanini_l);
            $('#pendudukawalbulanini_p').val(data_for_table.pendudukawalbulanini_p);
            $('#lahirbulanini_l').val(data_for_table.lahirbulanini_l);
            $('#lahirbulanini_p').val(data_for_table.lahirbulanini_p);
            $('#matibulanini_l').val(data_for_table.matibulanini_l);
            $('#matibulanini_p').val(data_for_table.matibulanini_p);
            $('#pendatangbulanini_l').val(data_for_table.pendatangbulanini_l);
            $('#pendatangbulanini_p').val(data_for_table.pendatangbulanini_p);
            $('#pindahbulanini_l').val(data_for_table.pindahbulanini_l);
            $('#pindahbulanini_p').val(data_for_table.pindahbulanini_p);
            $('#pendudukakhirbulanini_l').val(data_for_table.pendudukakhirbulanini_l);
            $('#pendudukakhirbulanini_p').val(data_for_table.pendudukakhirbulanini_p);

            fill_select_master('kecamatan', "<?php echo base_url() ?>", [$("#idkecamatan")], data_for_table);


        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinas-kependudukan/orang-asing/') ?>"+data.idorangasing, datatables);
        });
    });

    function showAdd(){
        fill_select_master('kecamatan', "<?php echo base_url() ?>", [$(".form-add #idkecamatan")]);
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinas-kependudukan/orang-asing/') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinas-kependudukan/orang-asing/') ?>"+data_id, datatables);
    }
</script>