<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "perwal(tentang)"},
            {
                "data": "is_asli",
                "render": function (data, type, row) {
                    if (row["is_asli"] == 1) {
                        return '<span class="label label-info">Asli</span>'
                    }
                    return '<span class="label label-danger">Tidak Asli</span>'
                }
            },
            {
                "data": "is_paraf",
                "render": function (data, type, row) {
                    if (row["is_paraf"] == 1) {
                        return '<span class="label label-info">Paraf</span>'
                    }
                    return '<span class="label label-danger">Tidak Berparaf</span>'
                }
            },
            {
                "data": "is_salinan",
                "render": function (data, type, row) {
                    if (row["is_salinan"] == 1) {
                        return '<span class="label label-info">Salinan</span>'
                    }
                    return '<span class="label label-danger">Bukan Salinan</span>'
                }
            },
            {"data": "username"},
            {"data": null}
        ];

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/hukum/perwal/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();
            data_id = data_for_table.idperwal;
            $('#tanggal').val(data_for_table.tanggal);
            $('#perwal').val(data_for_table['perwal(tentang)']);

            if (data_for_table.is_asli == 1) {
                $('#is_asli').parents().addClass('checked');
            } else {
                $('#is_asli').parents().removeClass('checked');
            }

            if (data_for_table.is_paraf == 1) {
                $('#is_paraf').parents().addClass('checked');
            } else {
                $('#is_paraf').parents().removeClass('checked');
            }

            if (data_for_table.is_salinan == 1) {
                $('#is_salinan').parents().addClass('checked');
            } else {
                $('#is_salinan').parents().removeClass('checked');
            }

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/hukum/perwal/') ?>" + data.idperwal, datatables);
        });
    });

    function showAdd() {

    }

    function add() {
        save_new_data("<?php echo base_url('/api/hukum/perwal') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/hukum/perwal/') ?>" + data_id, datatables);
    }
</script>
