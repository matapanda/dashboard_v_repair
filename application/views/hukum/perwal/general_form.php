<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="perwal">Tentang</label>
    <input name="perwal" type="text" id="perwal" placeholder="Tentang" class="form-control" required>
</div>
<div class="form-group">
    <label for="is_asli" class="checkbox-inline">
        <input name="is_asli" type="checkbox" id="is_asli" class="minimal"> Asli
    </label>
    <label for="is_salinan" class="checkbox-inline">
        <input name="is_salinan" type="checkbox" id="is_salinan" class="minimal"> Salinan
    </label>
    <label for="is_paraf" class="checkbox-inline">
        <input name="is_paraf" type="checkbox" id="is_paraf" class="minimal"> Paraf
    </label>
</div>
