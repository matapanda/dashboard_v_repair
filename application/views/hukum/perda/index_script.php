<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "perda"},
            {"data": "keterangan"},
            {"data": "username"},
            {"data": null}
        ];

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/hukum/perda/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();
            data_id = data_for_table.idperda;
            $('#tanggal').val(data_for_table.tanggal);
            $('#perda').val(data_for_table['perda']);
            $('#keterangan').val(data_for_table.keterangan);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/hukum/perda/') ?>" + data.idperda, datatables);
        });
    });

    function showAdd() {
        return;
    }

    function add() {
        save_new_data("<?php echo base_url('/api/hukum/perda') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/hukum/perda/') ?>" + data_id, datatables);
    }
</script>
