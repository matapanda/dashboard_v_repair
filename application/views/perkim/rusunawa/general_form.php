<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="jumlahhunian">Jumlah Hunian</label>
    <input name="jumlahhunian" type="number" id="jumlahhunian" placeholder="Jumlah Hunian" class="form-control" required>
</div>
<div class="form-group">
    <label for="jumlahpenghuni">Jumlah Penghuni</label>
    <input name="jumlahpenghuni" type="number" id="jumlahpenghuni" placeholder="Jumlah Penghuni" class="form-control" required>
</div>
<div class="form-group">
    <label for="idlokasi">Lokasi</label><br>
    <select name="idlokasi" id="idlokasi" class="form-control"></select>
</div>
<div class="form-group">
    <label for="keterangan">Keterangan</label>
    <input name="keterangan" type="text" id="keterangan" placeholder="Keterangan" class="form-control" required>
</div>
