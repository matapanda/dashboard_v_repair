<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "lokasi"},
            {"data": "jumlahhunian"},
            {"data": "jumlahpenghuni"},
            {"data": "keterangan"},
            {"data": "tanggal"},
            {"data": "username"},
            {"data": null}
        ];

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/perkim/rusunawa/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idrusunawa;
            $('#tanggal').val(data_for_table.tanggal);
            $('#jumlahpenghuni').val(data_for_table.jumlahpenghuni);
            $('#jumlahhunian').val(data_for_table.jumlahhunian);
            $('#keterangan').val(data_for_table.keterangan);
            $('#lokasi').val(data_for_table.lokasi);

            $.ajax({
                url: "<?php echo base_url('api/perkim/lokasi-rusunawa') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                $(".form-update #idlokasi").empty();
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $(".form-update #idlokasi").select2();
                    for (var i = 0; i < data.data.length; i++) {
                        $(".form-update #idlokasi").append($("<option />").val(data.data[i].idlokasi).text(data.data[i].lokasi));
                    }
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil lokasi rusunawa', "error");
            });

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/perkim/rusunawa/') ?>" + data.idrusunawa, datatables);
        });
    });

    function showAdd() {
        $.ajax({
            url: "<?php echo base_url('api/perkim/lokasi-rusunawa') ?>",
            type: "GET"
        }).done(function (result) {
            $(".form-add #idlokasi").empty();
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idlokasi").select2();
                for (var i = 0; i < data.data.length; i++) {
                    $(".form-add #idlokasi").append($("<option />").val(data.data[i].idlokasi).text(data.data[i].lokasi));
                }
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil lokasi rusunawa', "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('/api/perkim/rusunawa') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/perkim/rusunawa/') ?>" + data_id, datatables);
    }
</script>
