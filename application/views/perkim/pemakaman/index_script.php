<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1Y4FlA5LzNm_spEk-NIyGlamjftmfA0U&libraries=places">
</script>

<script src="<?= base_url('assets/js/default-map.js') ?>">
</script>

<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tahun"},
            {"data": "namatpu"},
            {"data": "luastpu"},
            {"data": "luaslahanmakam"},
            {"data": "jumlahseluruhmakam"},
            {"data": "fasum"},
            {"data": "sisalahanmakam"},
            {"data": "keterangan"},
            {"data": null}
        ];

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/perkim/pemakaman/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();
            data_id = data_for_table.idpemakaman;
            $('#tahun').val(data_for_table.tahun);
            $('#namatpu').val(data_for_table.namatpu);
            $('#luastpu').val(data_for_table.luastpu);
            $('#luaslahanmakam').val(data_for_table.luaslahanmakam);
            $('#jumlahseluruhmakam').val(data_for_table.jumlahseluruhmakam);
            $('#fasum').val(data_for_table.fasum);
            $('#sisalahanmakam').val(data_for_table.sisalahanmakam);
            $('#keterangan').val(data_for_table.keterangan);

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/perkim/pemakaman/') ?>" + data.idpemakaman, datatables);
        });
    });

    function update() {
        update_data("<?php echo base_url('/api/perkim/pemakaman/') ?>" + data_id, datatables);
    }

    function showAdd() {
        return;
    }

    function add() {
        var fields = [
            $('#tahun'),
            $('#namatpu'),
            $('#luastpu'),
            $('#luaslahanmakam'),
            $('#jumlahseluruhmakam'),
            $('#fasum'),
            $('#sisalahanmakam'),
            $('#keterangan')
        ];
        save_new_data("<?php echo base_url('/api/perkim/pemakaman') ?>", datatables);
    }

</script>
