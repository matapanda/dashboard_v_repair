<div class="form-group">
    <label for="namatpu">Nama TPU</label>
    <input name="namatpu" type="text" id="namatpu" placeholder="Nama TPU" class="form-control" required>
</div>
<div class="form-group">
    <label for="tahun">Tahun</label>
    <input name="tahun" type="date" class="form-control" id="tahun" placeholder="Tahun" required>
</div>
<div class="form-group">
    <label for="luastpu">Luas TPU</label>
    <input name="luastpu" type="number" id="luastpu" placeholder="Luas TPU" class="form-control" required>
</div>
<div class="form-group">
    <label for="luaslahanmakam">Luas Lahan Makam</label>
    <input name="luaslahanmakam" type="number" id="luaslahanmakam" placeholder="Luas Lahan Makam" class="form-control" required>
</div>
<div class="form-group">
    <label for="jumlahseluruhmakam">Jumlah Seluruh Makam</label>
    <input name="jumlahseluruhmakam" type="number" id="jumlahseluruhmakam" placeholder="Jumlah Seluruh Makam" class="form-control" required>
</div>
<div class="form-group">
    <label for="fasum">Fasum ( Tahun )</label>
    <input name="fasum" type="number" id="fasum" placeholder="Fasum ( Tahun )" class="form-control" required>
</div>
<div class="form-group">
    <label for="sisalahanmakam">Sisa Lahan Makam ( Tahun )</label>
    <input name="sisalahanmakam" type="number" id="sisalahanmakam" placeholder="Sisa Lahan Makam ( Tahun )" class="form-control" required>
</div>
<div class="form-group">
    <label for="keterangan">keterangan</label>
    <input name="keterangan" type="text" id="keterangan" placeholder="Keterangan" class="form-control" required>
</div>
