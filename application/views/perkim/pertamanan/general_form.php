<div class="form-group">
    <label>Tanggal</label>
    <div class="input-group date">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input type="text" class="form-control pull-right datepicker" id="datepicker" name="tanggal">
    </div>
    <!-- /.input group -->
</div>
<div class="form-group">
    <label for="nama_taman">Nama Taman</label>
    <input name="nama_taman" type="text" id="nama_taman" placeholder="Nama Taman" class="form-control">
</div>
<div class="form-group">
    <label for="urlgambarfasilitas">Gambar Fasilitas</label>
    <input name="urlgambarfasilitas" type="file" id="urlgambarfasilitas" class="form-control">
</div>
<div class="form-group">
    <label for="deskripsi">Deskripsi</label>
    <input name="deskripsi" type="text" id="deskripsi" placeholder="Deskripsi" class="form-control">
</div>
<div class="form-group">
    <label for="idjenispertamanan">Jenis Pertamanan</label>
    <select name="idjenispertamanan" id="idjenispertamanan" class="form-control select-jenis-pertamanan select2"
            style="width: 100%;" tabindex="-1" aria-hidden="true"></select>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control select-kelurahan select2" style="width: 100%;"
            tabindex="-1" aria-hidden="true"></select>
</div>
<div class="form-group">
    <label for="location">Location </label>
    <div id="map" style="height:400px; width:100%;"></div>
    <input name="longitude" type="hidden" id="longitude" placeholder="longitude" class="form-control">
    <input name="latitude" type="hidden" id="latitude" placeholder="latitude" class="form-control">
</div>
