<script src="<?= base_url('assets/js/default-map.js') ?>">
</script>

<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        $("form.form-update")
            .attr("enctype", "multipart/form-data")
            .attr("encoding", "multipart/form-data")
        ;
        $("form.form-add")
            .attr("enctype", "multipart/form-data")
            .attr("encoding", "multipart/form-data")
        ;
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "kelurahan"},
            {"data": "nama_taman"},
            {
              "data": "urlgambarfasilitas",
              "render" : function(data, type, row){
                return "<a href='<?=base_url()?>"+row['urlgambarfasilitas']+"' target='_blank'><img class='img-thumbnail' src='<?=base_url()?>"+row['urlgambarfasilitas']+"'/></a>";
              }
            },
            {"data": "deskripsi"},
            {"data": "jenispertamanan"},
            {"data": "langitude"},
            {"data": "longitude"},
            {"data": "username"},
            {"data": null}
        ];

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/perkim/pertamanan/datatable') ?>", columns);
        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.idpertamanan;
            $('#tanggal').val(data_for_table.tanggal);
            $('#nama_taman').val(data_for_table.nama_taman);
            $('#urlgambarfasilitas').val(data_for_table.urlgambarfasilitas);
            $('#idkelurahan').val(data_for_table.idkelurahan);
            $('#langitude').val(data_for_table.langitude);
            $('#longitude').val(data_for_table.longitude);
            $('#deskripsi').val(data_for_table.deskripsi);
            $('#idjenispertamanan').val(data_for_table.idjenispertamanan);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);

            $.ajax({
                url: "<?php echo base_url('api/perkim/jenis-pertamanan') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $(".form-update #idjenispertamanan").select2({
                        dropdownParent: $('#modal-update')
                    });
                    $.each(data.data, function () {
                        $(".form-update #idjenispertamanan").append($("<option />").val(this.idjenispertamanan).text(this.jenispertamanan));

                    });
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });

            init_datepicker($(".form-update"));

            $('.form-update #map').empty();
            var city = new google.maps.LatLng(data_for_table.langitude, data_for_table.longitude);
            initMap($(".form-update"), city);
            addMarker(city, 'Tempat Lama', true);
            $.getJSON({
                url: "<?=base_url('assets/js/kotamalang.geojson') ?>"
            }).done(function (data) {

                var kecamatan = {
                    "nama": [],
                    "color": []
                };

                $.each(data.features, function (key, value) {
                    var index = kecamatan.nama.indexOf(value.properties.KECAMATAN);
                    if (index === -1) {
                        kecamatan.nama.push(value.properties.KECAMATAN);
                        var color = '#' + Math.round(Math.random() * 16777215).toString(16);
                        kecamatan.color.push(color);
                    } else {
                        var color = kecamatan.color[index];
                    }

                    drawPolygonOverlay(value.geometry.coordinates[0], color, 0.2);
                });
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("gagal", "Gagal mengambil data overlay map", "error");
            })

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/perkim/pertamanan/') ?>" + data.idpertamanan, datatables);
        });
    });

    function update() {
        var xhr = function() {
          var myXhr = $.ajaxSettings.xhr();
          return myXhr;
        };
        update_data("<?php echo base_url('/api/perkim/pertamanan/') ?>" + data_id, datatables, '', xhr, true);
    }

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);

        // $("div#urlgambarfasilitas").dropzone({url: '<?=base_url("api/perkim/pertamanan/upload-images")?>'});
        $.ajax({
            url: "<?php echo base_url('api/perkim/jenis-pertamanan') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $(".form-add #idjenispertamanan").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idjenispertamanan").append($("<option />").val(this.idjenispertamanan).text(this.jenispertamanan));

                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });


        $('.form-add #map').empty();
        var city = new google.maps.LatLng(-7.996744, 112.6191631);
        initMap($(".form-add"), city);
        $.getJSON({
            url: "<?=base_url('assets/js/kotamalang.geojson') ?>"
        }).done(function (data) {

            var kecamatan = {
                "nama": [],
                "color": []
            };

            $.each(data.features, function (key, value) {
                var index = kecamatan.nama.indexOf(value.properties.KECAMATAN);
                if (index === -1) {
                    kecamatan.nama.push(value.properties.KECAMATAN);
                    var color = '#' + Math.round(Math.random() * 16777215).toString(16);
                    kecamatan.color.push(color);
                } else {
                    var color = kecamatan.color[index];
                }

                drawPolygonOverlay(value.geometry.coordinates[0], color, 0.2);
            });
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("gagal", "Gagal mengambil data overlay map", "error");
        })

        init_datepicker($(".form-add"));
    }

    function add() {
      var xhr = function() {
        var myXhr = $.ajaxSettings.xhr();
        return myXhr;
      };
      save_new_data("<?php echo base_url('/api/perkim/pertamanan') ?>", datatables, '', xhr, true);
    }

</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1Y4FlA5LzNm_spEk-NIyGlamjftmfA0U&libraries=places">
</script>
