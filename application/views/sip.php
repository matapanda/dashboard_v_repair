<!DOCTYPE html>
<html>
<head>
	<title>sip</title>
	<!-- <link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" /> -->
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"/>

	<!-- <script type='text/javascript' src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script> -->

	<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" ></script>

	<script type="text/javascript" src="<?php print_r(base_url()."assets/js/jquery-3.2.1.js");?>"></script>
</head>
<body>
	<div id="map" style="width: 100%; height: 650px;"></div>


	<script type="text/javascript">
		var data_main = JSON.parse('<?php print_r($data_coordinate);?>');
		var data_main_property = JSON.parse('<?php print_r($data_property);?>');
		var data_color = ["#66CCFF","#66CCFF","#66CCFF","#66CCFF","#66CCFF",
							"#66FFD7","#66FFD7","#66FFD7","#66FFD7","#66FFD7",
							"#3BEE77","#3BEE77","#3BEE77","#3BEE77","#3BEE77",
							"#DBE218","#DBE218","#DBE218","#DBE218","#DBE218",
							"#DBE218","#DBE218","#DBE218","#DBE218","#DBE218",
							"#EEA019","#EEA019","#EEA019","#EEA019","#EEA019",
							"#EEA019","#EEA019","#EEA019","#EEA019","#EEA019",
							"#F5431F","#F5431F","#F5431F","#F5431F","#F5431F",
							"#F5431F","#F5431F","#F5431F","#F5431F","#F5431F",
							"#66CCFF","#66CCFF","#66CCFF","#66CCFF","#66CCFF",
							"#2800F2","#2800F2","#2800F2","#2800F2","#2800F2",
							"#2800F2","#2800F2","#2800F2","#2800F2","#2800F2"];

		// console.log(data_main.length);

		for (var i =  0; i < data_main.length; i++) {
			console.log(data_main[i]);
			console.log(data_main_property[i]);
			console.log(data_color[i]);

			set_layer[data_main[i], data_color[i], "apa"];
		}
    	
		var map = L.map( 'map', {
		  center: [-7.98, 112.63],
		  minZoom: 1,
		  zoom: 2
		})
	

		L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
		  subdomains: ['a', 'b', 'c']
		}).addTo( map );


		function onEachFeature(feature, layer) {
			var popupContent = "<p>I started out as a GeoJSON " +
					feature.geometry.type + ", but now I'm a Leaflet vector!</p>";

			if (feature.properties && feature.properties.popupContent) {
				popupContent += feature.properties.popupContent;
			}

			layer.bindPopup(popupContent);
		}

		

		function set_layer(data_val, color, label){
			var campus = {
			    "type": "Feature",
			    "properties": {
			        "popupContent": label,
			        "style": {
			            weight: 2,
			            color: "#999",
			            opacity: 1,
			            fillColor: color,
			            fillOpacity: 0.8
			        }
			    },
			    "geometry": {
			        "type": "MultiPolygon",
			        "coordinates": data_val
			    }
			};

			L.geoJSON([campus], {

				style: function (feature) {
					return feature.properties && feature.properties.style;
				},

				onEachFeature: onEachFeature,

				pointToLayer: function (feature, latlng) {
					return L.circleMarker(latlng, {
						radius: 8,
						fillColor: "#ff7800",
						color: "#000",
						weight: 1,
						opacity: 1,
						fillOpacity: 0.8
					});
				}
			}).addTo(map);


		}
		
			

		
		
		


    </script>
</body>
</html>