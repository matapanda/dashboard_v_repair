<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1Y4FlA5LzNm_spEk-NIyGlamjftmfA0U&libraries=places"></script>
<script src="<?=base_url('assets/js/default-map.js')?>"></script>

<script>
var filter;
var url;
var tingkat;
var datatables;
var partialdata;
Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
        ].join('/');
};
$(document).ready(function () {
    //loading screen
    jQuery(document).ajaxStart(function () {
        $('body').loading({
            message: 'Proses...'
        });
    });
    jQuery(document).ajaxStop(function () {
        $('body').loading('stop');
    });

  $('#partial-data').addClass('hidden');
  $('#selected-statistic').addClass('hidden');
  initFilter();
  initNCCMap();
  var datatables_columns = [
      {"data": "judul"},
      {"data": "deskripsi"},
      {
          "data": "url",
          "render": function (data, type, row) {
              return '<a href="' + data + '" target="_blank">Tautan</a>'
          }
      },
      {"data": 'kategori'},
      {"data": null}
  ];
  datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/app/katalog/0/0/2') ?>", datatables_columns);
  $('#datatables tbody').on('click', '.remove_btn', function () {
    var data_tables = datatables.row($(this).parents('tr')).data();
    $.ajax({
        url: "<?php echo base_url('api/app/katalog/') ?>"+data_tables.idkatalog,
        type: "DELETE"
    }).done(function (result) {
        var data = jQuery.parseJSON(JSON.stringify(result));
        if (data.success) {
            sweetAlert("Berhasil", data.message, "success");
            $('#modal-add').modal('hide');
            datatables.ajax.reload();
        } else {
            sweetAlert("gagal", data.message, "error");
        }
    }).fail(function (xhr, status, errorThrown) {
        sweetAlert("Gagal", status, "error");
    })
  })
})

function initURL(){
  // $(document).ajaxStop(function() {
  //   var idsubmodule = getUrlParameter('idsubmodule');
  //   var tingkat = getUrlParameter('idsubmodule')
  // })
}

function initFilter(){
  var params = getUrlParameter();
  var enddate
  var startdate
  if(params.wherelist != undefined){
    params.wherelist.forEach(function(params_kategori_item) {
      for(params_kategori_table in params_kategori_item){
        if(params_kategori_table == 'tanggal'){
          enddate = new Date(params_kategori_item[params_kategori_table]['enddate']);
          startdate = new Date(params_kategori_item[params_kategori_table]['startdate']);
        }
      }
    })
  }
  initDateRangePicker($("#tanggal"), startdate, enddate);
  if(enddate && startdate){
    if(enddate.yyyymmdd() && startdate.yyyymmdd()){
      $("#tanggal")[0].startdate = startdate.yyyymmdd();
      $("#tanggal")[0].enddate = enddate.yyyymmdd();
      $("#tanggal").find('span').html(startdate.yyyymmdd() + ' - ' + enddate.yyyymmdd());
    }
  }
  initSubmodule();

  function initSubmoduleData() {
    var api_url = '<?= base_url("/api/user/submodules")?>';
    var deferred = $.Deferred();
    var df = $.Deferred();
    var data = [];
    $.ajax({
      url: api_url,
      type: "GET"
    }).done(function (result) {
      if (!result.success) {
        sweetAlert("Gagal", 'Kesalahan ketika mengambil data Sub SKPD', "error");
      }

      var data = result.data;
      var submodules_list = [];
      $.when(
        ajaxLoad('<?=base_url('api/master/status-kis')?>'),
        ajaxLoad('<?=base_url('api/master/hubungan-keluarga')?>'),
        ajaxLoad('<?=base_url('api/master/jenis-kelamin')?>'),
        ajaxLoad('<?=base_url('api/master/pendidikan')?>'),
        ajaxLoad('<?=base_url('api/master/status-kawin')?>'),
        ajaxLoad('<?=base_url('api/master/status-keluarga')?>'),
        ajaxLoad('<?=base_url('api/master/status-individu')?>'),
        ajaxLoad('<?=base_url('api/dinsos/pkh/kategori-pkh')?>'),
        ajaxLoad('<?=base_url('api/dinsos/razia/kategori-razia')?>'),
        ajaxLoad('<?=base_url('api/dinsos/resos/kategori-resos')?>'),
        ajaxLoad('<?=base_url("api/dp3p2kb/kdrt/jenis-korban")?>'),
        ajaxLoad('<?=base_url("api/dp3p2kb/kdrt/jenis-kasus")?>'),
        ajaxLoad('<?=base_url("api/perkim/jenis-pertamanan")?>'),
        ajaxLoad('<?=base_url("api/dinkes/data-bumil/keterangan-bpjs")?>'),
        ajaxLoad('<?=base_url("api/dinkes/data-bumil/rencana-persalinan-biaya")?>'),
        ajaxLoad('<?=base_url("api/dinkes/data-bumil/rencana-persalinan-kendaraan")?>'),
        ajaxLoad('<?=base_url("api/dinkes/data-gizi-buruk/puskesmas")?>'),
        ajaxLoad('<?=base_url("api/dinkes/data-gizi-buruk/balita")?>'),
        ajaxLoad('<?=base_url("api/dinkes/fayankes")?>'),
        ajaxLoad('<?=base_url("api/dinkes/jenis-fayankes")?>'),
        ajaxLoad('<?=base_url("api/dinkes/kode-rejimen")?>'),
        ajaxLoad('<?=base_url("api/dinkes/klasifikasi-penyakit")?>'),
        ajaxLoad('<?=base_url("api/dinkes/tipe-pasien")?>'),
        ajaxLoad('<?=base_url("api/dinkes/validasi-data")?>')
      ).done(function(statuskis, hubungankeluarga, jeniskelamin, pendidikan,
        statuskawin, statuskeluarga, statusindividu, kategoripkh,
        kategorirazia, kategoriresos, kategorikorban, kategorikasus,
        kategoripertamanan, kategoribpjs, kategoribiayapersalinan,
        kategorikendaraanpersalinan, kategoripuskesmas, kategoribalita,
        kategorifayankes, kategorijenisfayankes, kategorirejimen,
        kategoripenyakit, kategoripasien, kategorivalidasi)
        {
          var statuskis_data = [];
          statuskis.data.forEach(function(item) {
            statuskis_data.push({'value' : item.idstatuskis, 'html' : item.status});
          })

          var hubungankeluarga_data = [];
          hubungankeluarga.data.forEach(function(item) {
            hubungankeluarga_data.push({'value' : item.idhubungankeluarga, 'html' : item.hubungankeluarga});
          })

          var jeniskelamin_data = [];
          jeniskelamin.data.forEach(function(item) {
            jeniskelamin_data.push({'value' : item.idjeniskelamin, 'html' : item.jeniskelamin});
          })

          var pendidikan_data = [];
          pendidikan.data.forEach(function(item) {
            pendidikan_data.push({'value' : item.idpendidikan, 'html' : item.pendidikan});
          })

          var statuskawin_data = [];
          statuskawin.data.forEach(function(item) {
            statuskawin_data.push({'value' : item.idstatuskawin, 'html' : item.status});
          })

          var statuskeluarga_data = [];
          statuskeluarga.data.forEach(function(item) {
            statuskeluarga_data.push({'value' : item.idstatuskeluarga, 'html' : item.statuskeluarga});
          })

          var statusindividu_data = [];
          statusindividu.data.forEach(function(item) {
            statusindividu_data.push({'value' : item.idstatusindividu, 'html' : item.statusindividu});
          })

          var kategoripkh_data = [];
          kategoripkh.data.forEach(function(item) {
            kategoripkh_data.push({'value' : item.idkategori, 'html' : item.kategori});
          })

          var kategorirazia_data = [];
          kategorirazia.data.forEach(function(item) {
            kategorirazia_data.push({'value' : item.idkategorirazia, 'html' : item.razia});
          })

          var kategoriresos_data = [];
          kategoriresos.data.forEach(function(item) {
            kategoriresos_data.push({'value' : item.idkategoriresos, 'html' : item.kategori});
          })

          var kategorikasus_data = [];
          kategorikasus.data.forEach(function(item) {
            kategorikasus_data.push({'value' : item.idjeniskasus, 'html' : item.jeniskasus});
          })

          var kategorikorban_data = [];
          kategorikorban.data.forEach(function(item) {
            kategorikorban_data.push({'value' : item.idjeniskorban, 'html' : item.jeniskorban});
          })

          var kategoripertamanan_data = [];
          kategoripertamanan.data.forEach(function(item) {
            kategoripertamanan_data.push({'value' : item.idjenispertamanan, 'html' : item.jenispertamanan});
          })

          var kategorikendaraanpersalinan_data = [];
          kategorikendaraanpersalinan.data.forEach(function(item){
            kategorikendaraanpersalinan_data.push({'value': item.idrencana_persalinan_kendaraan, 'html': item.rencana_persalinan_kendaraan});
          })

          var kategoribiayapersalinan_data = [];
          kategoribiayapersalinan.data.forEach(function(item){
            kategoribiayapersalinan_data.push({'value': item.idrencana_persalinan_biaya, 'html': item.rencana_persalinan_biaya});
          })

          var kategoribpjs_data = [];
          kategoribpjs.data.forEach(function(item){
            kategoribpjs_data.push({'value': item.idketerangan_bpjs, 'html': item.keterangan_bpjs});
          })

          var kategoripuskesmas_data = [];
          kategoripuskesmas.data.forEach(function (item) {
            kategoripuskesmas_data.push({'value': item.idpuskesmas, 'html': item.puskesmas});
          })

          var kategoribalita_data = [];
          kategoribalita.data.forEach(function (item) {
            kategoribalita_data.push({'value': item.idbalita, 'html': item.nama_balita +'-'+ item.nama_orang_tua});
          })

          var kategorifayankes_data = [];
          kategorifayankes.data.forEach(function(item) {
            kategorifayankes_data.push({'value': item.idfayankes, 'html': item.fayankes})
          })
          var kategorijenisfayankes_data = [];
          kategorijenisfayankes.data.forEach(function(item) {
            kategorijenisfayankes_data.push({'value': item.idjenis_fayankes, 'html': item.jenis_fayankes})
          })
          var kategorirejimen_data = [];
          kategorirejimen.data.forEach(function(item) {
            kategorirejimen_data.push({'value': item.idkode_paduan_rejimen_yang_diberikan, 'html': item.kode_paduan_rejimen_yang_diberikan})
          })
          var kategoripenyakit_data = [];
          kategoripenyakit.data.forEach(function(item) {
            kategoripenyakit_data.push({'value': item.idklasifikasi_penyakit, 'html': item.klasifikasi_penyakit})
          })
          var kategoripasien_data = [];
          kategoripasien.data.forEach(function(item) {
            kategoripasien_data.push({'value': item.idtipe_pasien, 'html': item.tipe_pasien})
          })
          var kategorivalidasi_data = [];
          kategorivalidasi.data.forEach(function(item) {
            kategorivalidasi_data.push({'value': item.idvalidasi_data, 'html': item.validasi_data})
          })

          var kategorivalue_item = {};
          kategorivalue_item['statuskis'] = statuskis_data;
          kategorivalue_item['hubungankeluarga'] = hubungankeluarga_data;
          kategorivalue_item['jeniskelamin'] = jeniskelamin_data;
          kategorivalue_item['pendidikan'] = pendidikan_data;
          kategorivalue_item['statuskawin'] = statuskawin_data;
          kategorivalue_item['statuskeluarga'] = statuskeluarga_data;
          kategorivalue_item['statusindividu'] = statusindividu_data;
          kategorivalue_item['boolean'] = [
            {'value' : '1', 'html' : 'Ya'},
            {'value' : '0', 'html' : 'Tidak'}
          ];
          kategorivalue_item['kategoripkh'] = kategoripkh_data;
          kategorivalue_item['kategorirazia'] = kategorirazia_data;
          kategorivalue_item['kategoriresos'] = kategoriresos_data;
          kategorivalue_item['kategorikasus'] = kategorikasus_data;
          kategorivalue_item['kategorikorban'] = kategorikorban_data;
          kategorivalue_item['kategoripertamanan'] = kategoripertamanan_data;
          kategorivalue_item['kategorikendaraanpersalinan'] = kategorikendaraanpersalinan_data;
          kategorivalue_item['kategoribiayapersalinan'] = kategoribiayapersalinan_data;
          kategorivalue_item['kategoribpjs'] = kategoribpjs_data;
          kategorivalue_item['kategoripuskesmas'] = kategoripuskesmas_data;
          kategorivalue_item['kategoribalita'] = kategoribalita_data;
          kategorivalue_item['kategorifayankes'] = kategorifayankes_data;
          kategorivalue_item['kategorijenisfayankes'] = kategorijenisfayankes_data;
          kategorivalue_item['kategorirejimen'] = kategorirejimen_data;
          kategorivalue_item['kategoripenyakit'] = kategoripenyakit_data;
          kategorivalue_item['kategoripasien'] = kategoripasien_data;
          kategorivalue_item['kategorivalidasi'] = kategorivalidasi_data;
          df.resolve(kategorivalue_item);

        }
      ).fail(function(msg1, msg2, msg3, msg4, msg5, msg6, msg7,
        msg8, msg9, msg10, msg11, msg12, msg13, msg14, msg15, msg16,
        msg17, msg18) {
          var allmsg = [];
          allmsg.push(msg1,msg2,msg3,msg4,msg5,msg6,msg7,msg8,msg9,msg10,msg11,
            msg12,msg13,msg14,msg15,msg16,msg17,msg18);
            allmsg.forEach(function(x){
              if(x != undefined){
                sweetAlert("Gagal", x, "error");
              }
            })
          }
        )

        $.when(df).done(function(kategorivalue_item){
          for (var i = 0; i < data.length; i++) {
            var kategori_item = [];
            switch (data[i].idsubmodule) {
              case '1':
              case '14':
              case '15':
              case '19':
              case '20':
              case '22':
              case '23':
              case '24':
              case '25':
              break;

              case '2':
              var select = {
                'this' : ['nama', 'nama_ppk', 'alamat', 'tanggal_lahir'],
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'}
              ];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '3':
              kategori_item.push(
                {'value' : 'status_lama', 'html' : 'Status Lama', 'item' : kategorivalue_item['statuskis']}
              );
              kategori_item.push(
                {'value' : 'status_sekarang', 'html' : 'Status Sekarang', 'item' : kategorivalue_item['statuskis']}
              );
              kategori_item.push(
                {'value' : 'status_keterangan', 'html' : 'Status Keterangan', 'item' : kategorivalue_item['statuskis']}
              );

              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'statuskis as sk1', 'dest_column' : 'idstatuskis', 'src_table' : 'this', 'src_column' : 'status_lama'},
                {'dest_table' : 'statuskis as sk2', 'dest_column' : 'idstatuskis', 'src_column' : 'status_sekarang'}, // src_table : 'this' bersifat optional jika table srcnya adalah default / tablename
                {'dest_table' : 'statuskis as sk3', 'dest_column' : 'idstatuskis', 'src_column' : 'status_keterangan'} // src_table : 'this' bersifat optional jika table srcnya adalah default / tablename
              ];

              var select = {
                'this' : ['nama_anggota_keluarga', 'no_kk', 'nik'],
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'sk1' : ['status as status_lama'],
                'sk2' : ['status as status_sekarang'],
                'sk3' : ['status as status_keterangan']
              };

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal', 'kategori'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };

              submodules_list.push(temp);
              break;

              case '4':
              kategori_item.push({
                'value' : 'is_pkh',
                'html' : 'PKH',
                'item' : kategorivalue_item['boolean']
              });
              kategori_item.push({
                'value' : 'is_pbdt',
                'html' : 'PBDT',
                'item' : kategorivalue_item['boolean']
              });
              kategori_item.push({
                'value' : 'is_batal_kks',
                'html' : 'Batal KKS',
                'item' : kategorivalue_item['boolean']
              });
              kategori_item.push({
                'value' : 'is_pengajuan_pkh',
                'html' : 'Penagajuan PKH',
                'item' : kategorivalue_item['boolean']
              });
              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'this' : ['no_kks', 'is_pkh','is_pbdt','is_batal_kks','is_pengajuan_pkh']
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'}
              ];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal', 'kategori'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '5':
              kategori_item.push(
                {'value' : 'idkategori', 'html' : 'Status Lama', 'item' : kategorivalue_item['kategoripkh']}
              );

              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'dinsos_kategoripkh', 'dest_column' : 'idkategori'},
              ];

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'dinsos_kategoripkh' : ['kategori'],
                'this': ['jumlah']
              };
              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal', 'kategori'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '6':
              kategori_item.push({
                'value' : 'idhubungankeluarga',
                'html' : 'Hubungan Keluarga',
                'item' : kategorivalue_item['hubungankeluarga']
              });
              kategori_item.push({
                'value' : 'idjeniskelamin',
                'html' : 'Jenis Kelamin',
                'item' : kategorivalue_item['jeniskelamin']
              });
              kategori_item.push({
                'value' : 'idpendidikan',
                'html' : 'Pendidikan',
                'item' : kategorivalue_item['pendidikan']
              });
              kategori_item.push({
                'value' : 'idstatuskawin',
                'html' : 'Status Kawin',
                'item' : kategorivalue_item['statuskawin']
              });
              kategori_item.push({
                'value' : 'idstatuskeluarga',
                'html' : 'Status Keluarga',
                'item' : kategorivalue_item['statuskeluarga']
              });
              kategori_item.push({
                'value' : 'idstatusindividu',
                'html' : 'Status Individu',
                'item' : kategorivalue_item['statusindividu']
              });
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'hubungankeluarga', 'dest_column' : 'idhubungankeluarga'},
                {'dest_table' : 'jeniskelamin', 'dest_column' : 'idjeniskelamin'},
                {'dest_table' : 'statuskawin', 'dest_column' : 'idstatuskawin'},
                {'dest_table' : 'pendidikan', 'dest_column' : 'idpendidikan'},
                {'dest_table' : 'statuskeluarga', 'dest_column' : 'idstatuskeluarga'},
                {'dest_table' : 'statusindividu', 'dest_column' : 'idstatusindividu'}
              ];

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'this': ['no_kk', 'no_nik', 'nama'],
                'jeniskelamin' : ['jeniskelamin as jeniskelamin'] //contoh penggunaan as
              };

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal', 'kategori'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '7':
              kategori_item.push({
                'value' : 'iskota',
                'html' : 'Kota',
                'item' : kategorivalue_item['boolean']
              });
              kategori_item.push({
                'value' : 'idjeniskelamin',
                'html' : 'Jenis Kelamin',
                'item' : kategorivalue_item['jeniskelamin']
              });
              kategori_item.push({
                'value' : 'idkategorirazia',
                'html' : 'Kategori Razia',
                'item' : kategorivalue_item['kategorirazia']
              });

              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'jeniskelamin', 'dest_column' : 'idjeniskelamin'},
                {'dest_table' : 'dinsos_kategorirazia', 'dest_column' : 'idkategorirazia'}
              ];

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'jeniskelamin' : ['jeniskelamin'],
                'dinsos_kategorirazia' : ['razia'],
                'this' : ['tanggal', 'iskota', 'jumlah']
              };

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal', 'kategori'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '8':
              kategori_item.push({
                'value' : 'idkategoriresos',
                'html' : 'Kategori Resos',
                'item' : kategorivalue_item['kategoriresos']
              });

              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'dinsos_kategoriresos', 'dest_column' : 'idkategoriresos'}
              ];

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'dinsos_kategoriresos' : ['kategori'],
                'this' : ['tanggal', 'jumlah']
              };

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal', 'kategori'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '9':
              var join = [
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan'}
              ];
              var select = {
                'kecamatan' : ['kecamatan'],
                'this': ['jumlahkelompok_pkkrw','jumlahkelompok_pkkrt','jumlahkelompok_dasawisma']
              };
              var count = [
                'jumlahkelompok_pkkrw','jumlahkelompok_pkkrt','jumlahkelompok_dasawisma'
              ]

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : [ 'kecamatan', 'tanggal'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join, 'count' : count}
              };
              submodules_list.push(temp);
              break;

              case '10':
              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'this' : ['jumlahposyandu']
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'}
              ];
              var count = ['jumlahposyandu'];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join, 'count' : count}
              };
              submodules_list.push(temp);
              break;

              case '11':
              var select = {
                'kecamatan' : ['kecamatan'],
                'this': ['jumlahkepalakeluarga_didata']
              };
              var join = [
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan'}
              ];
              var count = ['jumlahkepalakeluarga_didata'];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join, 'count' : count}
              };
              submodules_list.push(temp);
              break;

              case '12':
              var select = {
                'kecamatan' : ['kecamatan'],
                'this': ['jumlahkeluarga', 'jumlahanggotakeluarga_balita',
                'jumlahanggotakeluarga_anak', 'jumlahanggotakeluarga_remaja',
                'jumlahanggotakeluarga_dewasa', 'jumlahanggotakeluarga_lansia']
              };
              var join = [
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan'}
              ];
              var count = ['jumlahkeluarga'];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join, 'count': count}
              };
              submodules_list.push(temp);
              break;

              case '13':
              kategori_item.push({
                'value' : 'idjeniskorban',
                'html' : 'Jenis Korban',
                'item' : kategorivalue_item['kategorikorban']
              });
              kategori_item.push({
                'value' : 'idjeniskasus',
                'html' : 'Jenis Kasus',
                'item' : kategorivalue_item['kategorikasus']
              });

              var select = {
                'kecamatan' : ['kecamatan'],
                'this': ['jumlah_korban'],
                'dp3p2kb_jeniskorban': ['jeniskorban'],
                'dp3p2kb_jeniskasus': ['jeniskasus']
              };
              var join = [
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan'},
                {'dest_table' : 'dp3p2kb_jeniskorban', 'dest_column' : 'idjeniskorban'},
                {'dest_table' : 'dp3p2kb_jeniskasus', 'dest_column' : 'idjeniskasus'}
              ];
              var count = ['jumlah_korban'];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kecamatan', 'tanggal'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join, 'count': count}
              };
              submodules_list.push(temp);
              break;

              case '16':
              var select = {
                'kecamatan' : ['kecamatan']
              };
              var join = [
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan'}
              ];
              var count = ['aktakelahiran_umum', 'aktakelahiran_terlambat',
              'aktaperkawinan_bulanini', 'aktaperceraian_bulanini',
              'aktapengangkatan_bulanini', 'aktapengakuan_bulanini',
              'aktapengesahan_bulanini', 'aktakematian_bulanini']

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join, 'count': count}
              };
              submodules_list.push(temp);
              break;

              case '17':
              var select = {
                'kecamatan' : ['kecamatan']
              };
              var join = [
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan'}
              ];
              var count = ['pendudukakhirbulanini_p', 'pendudukakhirbulanini_l']

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join, 'count': count}
              };
              submodules_list.push(temp);
              break;

              case '18':
              var select = {
                'kecamatan' : ['kecamatan']
              };
              var join = [
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan'}
              ];
              var count = ['pendudukakhirbulanini_p', 'pendudukakhirbulanini_l']

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join, 'count': count}
              };
              submodules_list.push(temp);
              break;

              case '21':
              kategori_item.push({
                'value' : 'idjenispertamanan',
                'html' : 'Jenis Pertamanan',
                'item' : kategorivalue_item['kategoripertamanan']
              });

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'perum_jenispertamanan' : ['jenispertamanan']
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'perum_jenispertamanan', 'dest_column' : 'idjenispertamanan'}
              ];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '26':
              kategori_item.push({
                'value' : 'idrencana_persalinan_biaya',
                'html' : 'Jenis Biaya Persalinan',
                'item' : kategorivalue_item['kategoribiayapersalinan']
              });
              kategori_item.push({
                'value' : 'idrencana_persalinan_kendaraan',
                'html' : 'Jenis Kendaraan Persalinan',
                'item' : kategorivalue_item['kategorikendaraanpersalinan']
              });
              kategori_item.push({
                'value' : 'idketerangan_bpjs',
                'html' : 'Jenis BPJS',
                'item' : kategorivalue_item['kategoribpjs']
              });

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'dinkes_rencana_persalinan_biaya': ['idrencana_persalinan_biaya'],
                'dinkes_rencana_persalinan_kendaraan': ['idrencana_persalinan_kendaraan'],
                'dinkes_keterangan_bpjs': ['idketerangan_bpjs']
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'dinkes_rencana_persalinan_biaya', 'dest_column': 'idrencana_persalinan_biaya'},
                {'dest_table' : 'dinkes_rencana_persalinan_kendaraan', 'dest_column': 'idrencana_persalinan_kendaraan'},
                {'dest_table' : 'dinkes_keterangan_bpjs', 'dest_column': 'idketerangan_bpjs'}
              ];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '27':
              kategori_item.push({
                'value' : 'idbalita',
                'html' : 'Balita',
                'item' : kategorivalue_item['kategoribalita']
              });
              kategori_item.push({
                'value' : 'idpuskesmas',
                'html' : 'Puskesmas',
                'item' : kategorivalue_item['kategoripuskesmas']
              });

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'dinkes_balita': ['*'],
                'dinkes_puskesmas': ['*']
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'dinkes_balita', 'dest_column': 'idbalita'},
                {'dest_table' : 'dinkes_puskesmas', 'dest_column': 'idpuskesmas'}
              ];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              case '28':
              kategori_item.push({
                'value' : 'idtipe_pasien',
                'html' : 'Tipe Pasien',
                'item' : kategorivalue_item['kategoripasien']
              });
              kategori_item.push({
                'value' : 'idklasifikasi_penyakit',
                'html' : 'Klasifikasi Penyakit',
                'item' : kategorivalue_item['kategoripenyakit']
              });
              kategori_item.push({
                'value' : 'idkode_paduan_rejimen_yang_diberikan',
                'html' : 'Kode Rejimen',
                'item' : kategorivalue_item['kategorirejimen']
              });
              kategori_item.push({
                'value' : 'idjenis_fayankes',
                'html' : 'Jenis Fayankes',
                'item' : kategorivalue_item['kategorijenisfayankes']
              });
              kategori_item.push({
                'value' : 'idfayankes',
                'html' : 'Fayankes',
                'item' : kategorivalue_item['kategorifayankes']
              });
              kategori_item.push({
                'value' : 'idvalidasi_data',
                'html' : 'Validasi Data',
                'item' : kategorivalue_item['kategorivalidasi']
              });
              kategori_item.push({
                'value' : 'idjeniskelamin',
                'html' : 'Jenis Kelamin',
                'item' : kategorivalue_item['jeniskelamin']
              });

              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan'],
                'dinkes_fayankes': ['fayankes'],
                'dinkes_jenis_fayankes': ['jenis_fayankes'],
                'dinkes_kode_paduan_rejimen_yang_diberikan': ['kode_paduan_rejimen_yang_diberikan'],
                'dinkes_klasifikasi_penyakit': ['klasifikasi_penyakit'],
                'dinkes_tipe_pasien': ['tipe_pasien'],
                'dinkes_validasi_data': ['validasi_data'],
                'jeniskelamin': ['jeniskelamin']
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'},
                {'dest_table' : 'dinkes_fayankes', 'dest_column': 'idfayankes'},
                {'dest_table' : 'dinkes_jenis_fayankes', 'dest_column': 'idjenis_fayankes'},
                {'dest_table' : 'dinkes_kode_paduan_rejimen_yang_diberikan', 'dest_column': 'idkode_paduan_rejimen_yang_diberikan'},
                {'dest_table' : 'dinkes_klasifikasi_penyakit', 'dest_column': 'idklasifikasi_penyakit'},
                {'dest_table' : 'dinkes_tipe_pasien', 'dest_column': 'idtipe_pasien'},
                {'dest_table' : 'dinkes_validasi_data', 'dest_column': 'idvalidasi_data'},
                {'dest_table' : 'jeniskelamin', 'dest_column': 'idjeniskelamin'}
              ];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal'],
                'kategori' : kategori_item,
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;

              default:
              var select = {
                'kelurahan' : ['kelurahan'],
                'kecamatan' : ['kecamatan']
              };
              var join = [
                {'dest_table' : 'kelurahan', 'dest_column' : 'idkelurahan'},
                {'dest_table' : 'kecamatan', 'dest_column' : 'idkecamatan', 'src_table' : 'kelurahan'}
              ];

              var temp = {
                'submodule' :{'value' : data[i].idsubmodule, 'html' : data[i].submodule},
                'filter' : ['kelurahan', 'kecamatan', 'tanggal'],
                'kategori' : [],
                'param' : {'select' : select, 'join' : join}
              };
              submodules_list.push(temp);
              break;
            }
          }
          deferred.resolve(submodules_list);
        }).fail(function(msg) {
          sweetAlert("Gagal", msg, "error");
        })
      }).fail(function () {
        sweetAlert("Gagal", 'Kesalahan ketika mengambil data Sub SKPD', "error");
      })
      return deferred.promise();
    }

    function initSubmodule() {
      var submodule = $("#submodule");

      initSubmoduleData().then(
        function(data){
          filter = data;
          params = getUrlParameter();
          if(params != undefined) {
            generateURL(params.select, params.group, params.join, params.where, params.count);
          }
          data.forEach(function(submodule_item) {
            function optionTemplate(value, text, selected){
              if(!selected) {
                return '<option value='+value+'>'+
                text+'</option>';
              } else if (selected){
                return '<option value='+value+' selected>'+
                text+'</option>';
              }
            }
            var selected = false;
            if (submodule_item.submodule.value == params['idsubmodule']){
              selected = true;
            }
            submodule.append(
              optionTemplate(submodule_item.submodule.value, submodule_item.submodule.html, selected)
            )
            if(selected){
              onChange('submodule');
            }
          })
        }
      )
    }

    function initDateRangePicker(dom, startdate, enddate){
      dom.daterangepicker(
        {
          ranges: {
            'Hari Ini': [moment(), moment()],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('day')],
            'Tahun Ini': [moment().startOf('year'), moment().endOf('day')],
            'Tahun Lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
            'Semua Tanggal': [moment(), 'all']
          },
          "startDate": startdate ? startdate:moment(),
          "endDate": enddate ? enddate:'all'
        },
        function (start, end) {
          if(end._i === 'all') {
            dom.find('span').html('Semua Tanggal');
            dom[0].startdate = null;
            dom[0].enddate = null;
          } else {
            dom.find('span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
            dom[0].startdate = start.format('YYYY/MM/DD');
            dom[0].enddate = end.format('YYYY/MM/DD');
          }
          onChange();
        }
      )
    }

    function ajaxLoad(url) {
      var df = $.Deferred()
      $.ajax({
        url: url,
        type: "GET"
      }).done(function(msg){
        // if(msg.success){
        df.resolve(msg);
        // } else {
        //   df.reject('Kesalahan dari server :'+msg.message);
        // }
      }).fail(function(msg) {
        df.reject('URL yang diberikan salah : '+url);
      })
      return df.promise();
    }
  }

  function initNCCMap() {
    var city = new google.maps.LatLng(-7.996744, 112.6191631);
    infoWindow = new google.maps.InfoWindow;

    map = initMap($('#map').parent(), city, false);
    // map.mapTypes.set('styled_map', new google.maps.StyledMapType(mapstyle));
    // map.setMapTypeId('styled_map');


    // addMapControl('TOP_RIGHT', $('#based').parent()[0]);
    // addMapControl('TOP_RIGHT', $('#idsubmodule').parent()[0]);
    // addMapControl('RIGHT_TOP', $('#param').parent()[0]);
    // addMapControl('RIGHT_TOP', $('#kategori').parent()[0]);
    // addMapControl('RIGHT_TOP', $('#kategori_value').parent()[0]);
    // addMapControl('RIGHT_BOTTOM', $('#partial-data-control')[0]);
    addMapControl('RIGHT_TOP', $('#selected-statistic')[0]);
    addMapControl('LEFT_BOTTOM', $('#legend')[0]);
  }

  function drawNCCMap(idsubmodule){
    deletePolygonOverlay();
    infoWindow.close();

    getMapData(idsubmodule).then(
      function(kota){
        proceedMapData(kota, tingkat.val());
      }
    )
  }

  function generateURL(select, group, join, where, count){
    var params = {
      'selectlist' : select,
      'grouplist' : group,
      'joinlist' : join,
      'wherelist' : where,
      'countlist' : count
    };
    return $.param(params);
  }

  function getMapData(idsubmodule) {
    var deferred = $.Deferred();
    var api_url = '<?php echo base_url("map/data/'+idsubmodule+'") ?>' +'?'+ url;
    var kota = {
      "nama_kota" : "Malang",
      "data" : [] ,
      "jumlah" : 0,
      "titik_atas"  : Number.MIN_VALUE,
      "titik_bawah" : Number.MAX_VALUE
    };

    $.ajax({
      url: api_url,
      type: "GET"
    }).then(function (msg) {
      kota.data = msg.data;
      kota.data.forEach(function(item_data) {
        if(item_data.jumlah > kota.titik_atas) {
          kota.titik_atas = parseInt(item_data.jumlah);
        }
        if(item_data.jumlah < kota.titik_bawah) {
          kota.titik_bawah = parseInt(item_data.jumlah);
        }
        kota.jumlah += parseInt(item_data.jumlah);
      })
      deferred.resolve(kota);
    }).fail(function (xhr, status, errorThrown) {
      sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data untuk map', "error");
    })
    return deferred.promise();
  }

  function proceedMapData(kota, group) {
    $.getJSON({
      url: "<?=base_url('assets/js/ncc-kotamalang.min.geojson') ?>"
    }).then(function (geojson) {
      var selectedGroupData = [];
      if(group == 'kelurahan'){
        geojson.forEach(function(kecamatan) {
          kecamatan.properties.forEach(function(kelurahan) {
            selectedGroupData.push(kelurahan);
          })
        })
      } else if(group == 'kecamatan'){
        geojson.forEach(function(kecamatan) {
          selectedGroupData.push(kecamatan);
        })
      }

      selectedGroupData.forEach(function(selectedGroupDataItem) {
        var area = {'item': 0, 'data': 0, "polygon":0 };
        var intensity;
        var color;
        var found;

        kota.data.forEach(function(item) {
          if(selectedGroupDataItem[group] === item[group]) {
            var density = (item.jumlah - kota.titik_bawah) / (kota.titik_atas - kota.titik_bawah);
            if( density < 0.2 ) {
              color = "#16a085";
              intensity = 0.6;
            } else if(density < 0.40 ) {
              color = "#f1c40f";
              intensity = 0.6;
            } else if(density < 0.60 ) {
              color = "#f39c12";
              intensity = 0.6;
            } else if(density < 0.80 ) {
              color = "#d35400";
              intensity = 0.6;
            } else {
              color = "#c0392b";
              intensity = 0.6;
            }
            found = true;
            area.item = item;
            area.data = selectedGroupDataItem[group];
            area.density = density;
          }

        });

        if(!found) {
          color = "#87D37C";
          intensity = 0.6;
        }

        area.polygon = drawPolygonOverlay(selectedGroupDataItem.coordinates, color, intensity);
        area.polygon.addListener('click', function(e){
          var capitalize = function(str) {
            return str.substr(0, 1).toUpperCase() + str.substr(1);
          }
          var contentSelectedArea = capitalize(group)+" <b>"+selectedGroupDataItem[group] +"</b>";
          var pielabel = selectedGroupDataItem[group];
          infoWindow.close();
          infoWindow.setContent(contentSelectedArea);
          infoWindow.setPosition(e.latLng);
          infoWindow.open(map);
          var latLng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
          map.panTo(latLng);

          var selectedpieammount = $('#selected-pie-amount');
          selectedpieammount.parent().removeClass('hidden');
          selectedpieammount.html(selectedpieammount);
          selectedpieammount.height(function(){ return selectedpieammount.width()*.8});
          $.plot(
            '#selected-pie-amount',
            [{"label": pielabel, data: area.item.jumlah ? area.item.jumlah: 0},
            {"label": 'Malang', data: kota.jumlah-area.item.jumlah}],
            {
              series: {
                pie: {
                  show: true,
                  radius: .5,
                  offset: {
                    top: 1
                  },
                  label: {
                    show: true,
                    radius: 0.50,
                    formatter: function(label, series){
                      return '<div style="font-size:14px; padding: 2px; text-align:center; color: #fff; font-weight: 10;">'
                      + label + '<br>'
                      + "<b>"
                      + Math.round(series.percent*100)/100 + "%</b></div>"
                    },
                    background: {
                      opacity: 1
                    }
                  }
                }
              },
              legend: {
                show: false
              }
            }
          );
          $('#selected-place').html(contentSelectedArea);
          var contentSelectedAmount = "Jumlah data <b>";
          if(area.item.jumlah != undefined) {
            contentSelectedAmount += area.item.jumlah;
          } else {
            contentSelectedAmount += 0;
          }
          contentSelectedAmount += "</b> dari <b>"+kota.jumlah +"</b>";
          $('#selected-amount').html(contentSelectedAmount);

          var contentSelectedAverage = "Berdasarkan tingkat <b><span style='text-transform: lowercase'>"+tingkat.val()+"</span></b> ";
          var intensityText = area.density*100 <= 20 ? '<span class="label" style="font-size:14px;background-color:#16a085">rendah sekali </span>':
                              area.density*100 <= 40 ? '<span class="label" style="font-size:14px;background-color:#f1c40f">rendah </span>':
                              area.density*100 <= 60 ? '<span class="label" style="font-size:14px;background-color:#f39c12">sedang </span>':
                              area.density*100 <= 80 ? '<span class="label" style="font-size:14px;background-color:#d35400">tinggi </span>':
                              '<span class="label" style="font-size:14px;background-color:#c0392b">tinggi sekali </span>';
          contentSelectedAverage += "nilai intensitas datanya <b>"+intensityText+"</b>";
          $('#selected-average').html(contentSelectedAverage);

          api_url = '<?php echo base_url("map/data/") ?>'+ partialDataURL();
          $.ajax({
            url: api_url,
            method: 'GET'
          }).done(function(x) {
            if(x.data.length == 0) {
              selectedpieammount.parent().addClass('hidden');
              $('#partial-data-control').addClass('hidden');
              return;
            } else {
              $('#partial-data-control').removeClass('hidden');
            }
            $('#partial-data').empty();
            var tableHeaders = '';
            var data_header = [];
            var data_row = [];
            x.data.forEach(function (data, i) {
              temp_data = [];
              for(var colname in data){
                if(i==0){
                  tableHeaders += "<th>" + colname + "</th>";
                  data_header.push([colname]);
                }
                temp_data.push(data[colname]);
              }
              data_row.push(temp_data);
            })
            if(partialdata!=undefined){
              partialdata.destroy();
              $('#partial-data').html('<thead><tr>' + tableHeaders + '</tr></thead>');
            } else {
              $('#partial-data').html('<thead><tr>' + tableHeaders + '</tr></thead>');
            }
            partialdata = $('#partial-data').DataTable({
                "data": data_row,
                "deferRender": true,
                "responsive": true,
                "autoWidth": false,
                "order": [[ 1, 'asc' ]],
            });
            $('#partial-data').removeClass('hidden');
          })

          function partialDataURL(){
            var submodule = $("#submodule");
            tingkat = $('#tingkat');
            var kategori = $('#kategori');
            var kategorivalue = $('#kategorivalue');
            var tanggal = $('#tanggal');

            var idsubmodule = submodule.val();
            var count = ['null'];
            var where = [];
            var temp = {};
            temp[tingkat.val()] = area.item[tingkat.val()];
            where.push({[tingkat.val()] : temp});

            var group = null;
            var join = [];

            var select = {};

            if(tanggal[0].enddate != null && tanggal[0].startdate != null) {
              where.push({'tanggal' : {'startdate' : tanggal[0].startdate, 'enddate' : tanggal[0].enddate}});
            }

            if(kategorivalue.val()!=null){
              var temp = {};
              temp[kategori.val()] = kategorivalue.val();
              where.push({'this' : temp});
            }

            filter.forEach(function(item) {
              if(idsubmodule == item.submodule.value) {
                if(item.param.select != undefined) {
                  Object.assign(select, select, item.param.select)
                }
                if(item.param.join != undefined) {
                  join = item.param.join;
                }
              }
            })

            return idsubmodule +'?'+ generateURL(select, group, join, where, count)
          }
        });
      });

    }).fail(function (xhr, status, errorThrown) {
      sweetAlert("gagal", "Gagal membuat data overlay map!", "error");
    })
  }

  function onChange(type){
    $('#partial-data-control').addClass('hidden');
    $('#selected-statistic').addClass('hidden');
    var tanggal = $('#tanggal');
    var params = getUrlParameter();
    var selected_kategori = undefined;
    var selected_kategorivalue = undefined;
    // if(params.wherelist!=undefined){
    //   for(params_kategori in params.wherelist[0].this){
    //     if(params_kategori!= undefined){
    //       selected_kategori = params_kategori;
    //       selected_kategorivalue = params.wherelist[0].this[params_kategori];
    //     }
    //   }
    // }
    if(params.wherelist!=undefined){
      params.wherelist.forEach(function(params_kategori_item) {
        for(params_kategori_table in params_kategori_item){
          if(params_kategori_table == 'tanggal'){

            // var enddate = new Date(params_kategori_item[params_kategori_table]['enddate']);
            // var startdate = new Date(params_kategori_item[params_kategori_table]['startdate']);
            // $("#tanggal").unbind('.datepicker');
            // $("#tanggal").data('daterangepicker').remove();
            // initDateRangePicker($("#tanggal"), startdate, enddate);
          } else {
            for(params_kategori_column in params_kategori_item[params_kategori_table]){
              if(params_kategori_column!= undefined){
                if(params_kategori_table == 'this'){
                  selected_kategori = params_kategori_column;
                  selected_kategorivalue = params_kategori_item[params_kategori_table][params_kategori_column];
                }
              }
            }
          }
        }
      })
    }
    var submodule = $("#submodule");
    tingkat = $('#tingkat');
    var kategori = $('#kategori');
    var kategorivalue = $('#kategorivalue');

    if(type == 'submodule') onChangeSubmodule();
    else if(type == 'kategori') onChangeKategori();

    function onChangeSubmodule(){
      tingkat.empty();
      tanggal.parent().addClass('hidden');
      kategori.empty();
      kategori.parent().addClass('hidden');
      kategorivalue.empty();
      kategorivalue.parent().addClass('hidden');

      filter.forEach(function(item) {
        if(submodule.val() == item.submodule.value) {
          var first = true;
          item.filter.forEach(function(filter_item){
            var selected = false;
            if(filter_item == 'tanggal') {
              tanggal.parent().removeClass('hidden');
            }

            if(filter_item == 'kecamatan') {
              if(params.grouplist == 'kecamatan'){
                selected = true;
              }
              tingkat.parent().removeClass('hidden');
              tingkat.append(optionTemplate('kecamatan', "Kecamatan",selected));
            }

            if(filter_item == 'kelurahan') {
              if(params.grouplist == 'kelurahan'){
                selected = true;
              }
              tingkat.parent().removeClass('hidden');
              tingkat.append(optionTemplate('kelurahan', "Kelurahan",selected));
            }
          })

          item.kategori.forEach(function(kategori_item){
            if(first) {
              kategori.parent().removeClass('hidden');
              kategori.append(optionTemplate(false, 'Pilih Kategori'));
              first = false;
            }
            var selected = false;
            if(selected_kategori == kategori_item.value){
              selected = true;
            }
            kategori.append(optionTemplate(kategori_item.value, kategori_item.html, selected));
            if(selected){
              kategori.val(selected_kategori)
              onChangeKategori();
            }
          })
        }
      })

    }

    function onChangeKategori(){
      kategorivalue.empty();
      kategorivalue.parent().addClass('hidden');
      filter.forEach(function(item) {
        if(submodule.val() == item.submodule.value) {
          item.kategori.forEach(function(kategori_item){
            if(kategori.val() == kategori_item.value){
              kategorivalue.parent().removeClass('hidden');
              kategori_item.item.forEach(function(kategorivalue_item) {
                var selected = false;
                if(selected_kategorivalue == kategorivalue_item.value){
                  selected = true;
                }
                kategorivalue.append(
                  optionTemplate(kategorivalue_item.value, kategorivalue_item.html, selected)
                )
                if(selected){
                  kategorivalue.val(selected_kategorivalue);
                }
              })
            }
          })
        }
      })
    }

    var idsubmodule = submodule.val();
    var count = [];
    var where = [];
    var group = tingkat.val();
    var join = [];

    var select = {};
    select[group] = [group];
    if(tanggal[0].enddate != null && tanggal[0].startdate != null) {
      where.push({'tanggal' : {'startdate' : tanggal[0].startdate, 'enddate' : tanggal[0].enddate}});
    }

    if(kategorivalue.val()!=null){
      var temp = {};
      temp[kategori.val()] = kategorivalue.val();
      where.push({'this' : temp});
    }

    filter.forEach(function(item) {
      if(idsubmodule == item.submodule.value) {
        if(item.param.select != undefined) {
          // select = item.param.select;
        }
        if(item.param.join != undefined) {
          join = item.param.join;
        }
        if(item.param.count != undefined) {
          count = item.param.count;
        }
      }
    })

    url = generateURL(select, group, join, where, count);
    drawNCCMap(idsubmodule);

    function optionTemplate(value, text, selected){
      if(!selected) {
        return '<option value='+value+'>'+
        text+'</option>';
      } else if (selected == true){
        return '<option value='+value+' selected>'+
        text+'</option>';
      }
    }

    if(submodule.val()!=undefined){
      var windowURL = '?idsubmodule=' + submodule.val() +'&'+ url;
      window.history.pushState('', 'NCC - Map', '<?=base_url("map")?>'+windowURL);
    }
  }

  function getUrlParameter() {
    var sPageURL = decodeURIComponent(window.location.search.substring(1));
    return deparam(sPageURL);
  }

  function initModalURL() {
    var idkategori = $('#idkategori');
    var input_url = $('#input-url');

    idkategori.empty();
    input_url.val('map/'+'?'+$.param(getUrlParameter()));

    //get data kategori
    $.ajax({
      url: "<?php echo base_url('api/app/katalog-kategori') ?>",
      type: "GET"
    }).done(function (result) {
      var data = jQuery.parseJSON(JSON.stringify(result));
      if (data.success) {
        $('#modal-add select').css('width', '100%');
        idkategori.select2({
          dropdownParent: $('#modal-add')
        });
        $.each(data.data, function () {
          idkategori.append($("<option/>").val(this.idkategori).text(this.kategori));
        });
      } else {
        sweetAlert("gagal", data.message, "error");
      }
    }).fail(function (xhr, status, errorThrown) {
      sweetAlert("Gagal", status, "error");
    });
  }

  function saveURL() {
    var $form_add = $('.form-add');
    $.ajax({
        url: "<?php echo base_url('api/app/katalog') ?>",
        data: $form_add.serialize(),
        type: "POST"
    }).done(function (result) {
        var data = jQuery.parseJSON(JSON.stringify(result));
        if (data.success) {
            sweetAlert("Berhasil", data.message, "success");
            $('#modal-add').modal('hide');
            $form_add[0].reset();
            datatables.ajax.reload();
        } else {
            sweetAlert("gagal", data.message, "error");
        }
    }).fail(function (xhr, status, errorThrown) {
        sweetAlert("Gagal", status, "error");
    })
  }
  function init_datatables(datatables, api_url, columns) {
      var colDef = {
          "targets": -1,
          "data": null,
          "defaultContent": "" +
          "<button class='remove_btn btn btn-danger btn-sm' title='Delete Katalog'><span class='fa fa-trash-o'></span></button>"
      };

      return datatables.DataTable({
          "ajax": api_url,
          "deferRender": true,
          "columns": columns,
          "responsive": true,
          "autoWidth": false,
          "order": [[ 1, 'asc' ]],
          "columnDefs": [colDef],
          'iDisplayLength': 5
      });
  }
  </script>
