<html>
<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>NCC <?php echo $title ? " | $title" : '' ?></title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- development progress -->
  <meta http-equiv="cache-control" content="max-age=0"/>
  <meta http-equiv="cache-control" content="no-cache"/>
  <meta http-equiv="expires" content="0"/>
  <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
  <meta http-equiv="pragma" content="no-cache"/>
  <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
  <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("/assets/css/select2.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">

  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  <link href="<?php echo base_url("assets/css/default.css"); ?>" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
  </style>
</head>

<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-4">
        <div class="input-parameter">
          <form class="form-data" onsubmit="event.preventDefault();">
            <div class="row bg-primary" style="padding:10px;margin:5px;">
              <div class="form-group col-xs-12 col-md-6">
                <label>DATA :</label>
                <select id="submodule" name="submodule" class="form-control select2" onchange="onChange('submodule')">
                  <option disabled value="" selected>Pilih Data</option>
                </select>
              </div>
              <div class="form-group col-xs-12 col-md-6 hidden">
                <label>Tingkat :</label>
                <select id="tingkat" name="tingkat" class="form-control bg-info" onchange="onChange()">
                  <option disabled value="" selected >Pilih Dasar</option>
                </select>
              </div>
              <div class="form-group col-xs-12 col-md-6 hidden">
                <label>Parameter :</label>
                <select id='kategori' name='kategori' class="col-xs-12 col-md-6 form-control" onchange="onChange('kategori')" style="width:100%">
                  <option disabled value='' selected >Kategori</option>
                </select>
              </div>
              <div class="form-group col-xs-12 col-md-6 hidden">
                <label>Nilai Parameter :</label>
                <select id='kategorivalue' name='kategorivalue' class='col-xs-12 col-md-6 form-control' onchange="onChange()" style="width:100%">
                  <option disabled value='' selected >Kategori Yang dicari</option>
                </select>
              </div>
              <div class="form-group col-xs-12 col-md-12 hidden">
                <button type="button" class="btn btn-warning" id="tanggal">
                  <span>Tanggal</span>
                  <i class="fa fa-caret-down"></i>
                </button>
                <button type="button" class="btn btn-success text-right" id="simpan" data-toggle="modal" data-target="#modal-add" onclick="initModalURL()">
                  <span>Simpan</span>
                </button>
              </div>
            </div>
          </form>
        </div>
        <div id="legend">
          <ul class="list-unstyled text-left" style="padding:10px;opacity:1;">
            <li><h5><span class="label" style="background-color:#c0392b"> Tinggi Sekali</span></h5></li>
            <li><h5><span class="label" style="background-color:#d35400"> Tinggi</span></h5></li>
            <li><h5><span class="label" style="background-color:#f39c12"> Menengah</span></h5></li>
            <li><h5><span class="label" style="background-color:#f1c40f"> Rendah</span></h5></li>
            <li><h5><span class="label" style="background-color:#16a085"> Rendah Sekali</span></h5></li>
            <li><h5><span class="label" style="background-color:#87D37C"> Kosong</span></h5></li>
          </ul>
        </div>
        <div class="container-fluid row">
          <p class="title h4">Data Katalog Map</p>
          <div class="container card col-xs-12">
            <table id="datatables" class="table table-bordered table-striped display nowrap dt-responsive" cellspacing="0" width='100%'>
              <thead>
                <th data-priority='1'>Judul</th>
                <th>Deskripsi</th>
                <th>Tautan</th>
                <th>Kategori</th>
                <th data-priority='1'>Aksi</th>
              </thead>
            </table>
              <hr>
              <p class="small pull-right">
                  <a href="<?php echo base_url('') ?>">NCC</a> |
                  <a href="<?php echo base_url('katalog') ?>">Katalog</a>
              </p>
          </div>
          <div class="col-xs-4 well well-sm hidden" id="selected-statistic" style="font-size:14px;">
            <div class="col-xs-12" id="selected-pie-amount"></div>
            <div class="col-xs-12" id="selected-place"></div>
            <div class="col-xs-12" id="selected-amount"></div>
            <div class="col-xs-12" id="selected-average"></div>
            <div class="col-xs-12" id="selected-data"></div>
          </div>
        </div>
      </div>
      <div id="map" style="height:100%;" class="col-xs-8"></div>
      <div class="table-responsive container card col-xs-12 well well-xm" id='partial-data-control'>
        <table id='partial-data' class="hidden table table-bordered table-striped display nowrap dt-responsive">
        </table>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Simpan URL</h4>
      </div>
      <form class="form-add" onsubmit="event.preventDefault(); saveURL();">
        <div class="modal-body">
          <input type="hidden" name="idtipe" value="2">
          <input type="hidden" id="input-url" name="url_data">
          <div class="form-group">
            <label for="input-judul">Judul</label>
            <input class="form-control" id="input-judul" type="text" name="judul" placeholder="Judul...">
          </div>
          <div class="form-group">
            <label for="input-deskripsi">Deskripsi</label>
            <input class="form-control" id="input-deskripsi" type="text" name="deskripsi" placeholder="Deskripsi...">
          </div>
          <div class="form-group">
            <label for="idkategori">Kategori</label><br>
            <select name="idkategori" id="idkategori" class="form-control"></select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span>
            Close
          </button>
          <button class="btn btn-success"><span class="fa fa-plus"></span> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.slimscroll.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery-deparam.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/fastclick.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap-datepicker.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/icheck.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/default.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/dropzone.js") ?>"></script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.pie.min.js"></script>
<script type="text/javascript"
src="https://cdn.datatables.net/v/bs/dt-1.10.13/fc-3.2.2/fh-3.1.2/r-2.1.1/se-1.2.0/datatables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>
