<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

    <link href="<?=base_url("/AdminLTE2/bower_components/bootstrap/dist/css/bootstrap.min.css")?>" rel="stylesheet" >
    <!-- Font Awesome -->
    <link  href="<?=base_url("/AdminLTE2/bower_components/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet">
    <!-- Ionicons -->
    <link  href="<?=base_url("/AdminLTE2/bower_components/Ionicons/css/ionicons.min.css")?>" rel="stylesheet">
    <!-- jvectormap -->
    <!-- <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css"> -->
  
    <link  href="<?=base_url("/AdminLTE2/dist/css/AdminLTE.min.css")?>" rel="stylesheet">
    


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>

<body>
    <img class="background-ncc-logo-full" src="<?php echo base_url("assets/images/ncc-logo-grayscale.png "); ?>" alt="NCC logo">

    <div class="col-md-3">
        <h3><?php echo "Grafik Agregat Data Fasilitas Kesehatan Kota Malang"; ?></h3>
        <hr>
        <p>
            <?= $option;?>
        </p>
        <table class="table table-striped table-bordered" id="chart-table">
            <thead>
              <tr class='PAUD_jenis_main_1' align="center">
                  <h3><td colspan='3'><b id="title_table">Data Fasilitas Kesehatan</b></td></h3>
              </tr>
              <tr>
                  <td><b>No. </b></td>
                  <td><b>Jenis Fasilitas Kesehatan</b></td>
                  <td><b>Jumlah Fasilitas</b></td>
              </tr>
            </thead>
            <tbody id="main_table">

              <?php
                if($data_json){
                  $data = json_decode($data_json);
                  $no = 1;
                  foreach ($data as $key => $value) {
                    print_r("<tr>
                              <td>".$no."</td>
                              <td>".$value->param."</td>
                              <td align='right'>".$value->val."</td>
                            </tr>");

                    $no++;
                    // print_r($value);
                  }
                }
              ?>
            </tbody>
        </table>
        <hr>
        <p class="small pull-right">
            <a href="<?php echo base_url('') ?>">NCC</a> |
            <a href="<?php echo base_url('katalog') ?>">Katalog</a>
        </p>
    </div>

    <script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
    <div class="col-md-9" id="chart_opening">

        <CENTER>
            <h1 id="title_data">Grafik Data Fasilitas Kesehatan Kota Malang</h1>
        </CENTER>
        <br>
        <br>

        


        <!-- GRAFIK ZONA KEC LOWOKWARU -->
        <center>
            <div class="box box-info" style=" width: 100%;">
                <div class="box-header with-border">
                    <h3 class="box-title" id="chart_header_title"> Grafik Data Fasilitas Kesehatan Kota Malang</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <!-- <div class="row"> -->
                    <div class="pad">
                        <!-- Grafik Chart [Pie] Disini -->
                        <table width="100%" border="1">
                            <tr>
                                <td>
                                    <div id="chartdiv" style="height: 550px; width: 100%;"></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
    </center>


    </div>
    <script src="<?php echo base_url("assets/js/jquery.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.loading.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.dataTables.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/datatables.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/select2.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/sweetalert.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/app.min.js ") ?>"></script>
    <script src="<?php echo base_url("assets/js/url.min.js ") ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>

    <!-- CHART -->
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/core.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/charts.js"></script>

    <!-- <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/dataviz.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/material.js"></script>
    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/kelly.js"></script> -->

    <script src="<?php print_r(base_url()."assets/amcharts4");?>/themes/animated.js"></script>

    <script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>

    <!-- /CHART -->

    <!-- jQuery 3 -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/jquery/dist/jquery.min.js ")?>">
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/bootstrap/dist/js/bootstrap.min.js ")?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/fastclick/lib/fastclick.js ")?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url("AdminLTE2/dist/js/adminlte.min.js ")?>"></script>
    <!-- Sparkline -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js ")?>"></script>
    <!-- jvectormap  -->
    <script src="<?php echo base_url("AdminLTE2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")?>"></script>
    <script src="<?php echo base_url("AdminLTE2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")?>"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js")?>"></script>
    <!-- ChartJS -->
    <script src="<?php echo base_url("AdminLTE2/bower_components/chart.js/Chart.js ")?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo base_url("AdminLTE2/dist/js/pages/dashboard2.js")?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url("AdminLTE2/dist/js/demo.js ")?>"></script>

    
    <script>
        var data_main = JSON.parse('<?php print_r($data_json);?>');
        $(document).ready(function(){
          set_graph(data_main)
        });

        function set_graph(data_val){
          am4core.ready(function() {

          // Themes begin
          am4core.useTheme(am4themes_animated);
          // Themes end

          // Create chart instance
          var chart = am4core.create("chartdiv", am4charts.XYChart3D);

          // Add data
          chart.data = data_val;

          // Create axes
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
          categoryAxis.dataFields.category = "param";
          categoryAxis.renderer.labels.template.rotation = 270;
          categoryAxis.renderer.labels.template.hideOversized = false;
          categoryAxis.renderer.minGridDistance = 20;
          categoryAxis.renderer.labels.template.horizontalCenter = "right";
          categoryAxis.renderer.labels.template.verticalCenter = "middle";
          categoryAxis.tooltip.label.rotation = 270;
          categoryAxis.tooltip.label.horizontalCenter = "right";
          categoryAxis.tooltip.label.verticalCenter = "middle";

          var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
          valueAxis.title.text = "Countries";
          valueAxis.title.fontWeight = "bold";

          // Create series
          var series = chart.series.push(new am4charts.ColumnSeries3D());
          series.dataFields.valueY = "val";
          series.dataFields.categoryX = "param";
          series.name = "Visits";
          series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
          series.columns.template.fillOpacity = .8;
          series.columns.template.propertyFields.fill = "color";

          var columnTemplate = series.columns.template;
          columnTemplate.strokeWidth = 2;
          columnTemplate.strokeOpacity = 1;
          columnTemplate.stroke = am4core.color("#FFFFFF");

          chart.cursor = new am4charts.XYCursor();
          chart.cursor.lineX.strokeOpacity = 0;
          chart.cursor.lineY.strokeOpacity = 0;

          // Enable export
          chart.exporting.menu = new am4core.ExportMenu();

          }); // end am4core.ready()
        }

        
    </script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>
