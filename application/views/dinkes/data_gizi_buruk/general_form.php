<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="no_kelurahan">Nomor Kelurahan</label>
    <input name="no_kelurahan" type="number" id="no_kelurahan" placeholder="Nomor Kelurahan" class="form-control" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="idpuskesmas">Puskesmas</label><br>
    <select name="idpuskesmas" id="idpuskesmas" class="form-control"></select>
</div>
<div class="form-group">
    <label for="idbalita">Balita</label><br>
    <select name="idbalita" id="idbalita" class="form-control"></select>
</div>
<div class="form-group">
    <label for="alamat">Alamat</label>
    <input name="alamat" type="text" id="alamat" placeholder="Alamat" class="form-control" required>
</div>
<div class="form-group">
    <label for="rt">RT</label>
    <input name="rt" type="text" id="rt" placeholder="RT" class="form-control" required>
</div>
<div class="form-group">
    <label for="rw">RW</label>
    <input name="rw" type="text" id="rw" placeholder="RW" class="form-control" required>
</div>
<div class="form-group">
    <label for="umur_bulan">Umur (Bulan)</label>
    <input name="umur_bulan" type="number" id="umur_bulan" placeholder="Umur (Bulan)" class="form-control" required>
</div>
<div class="form-group">
    <label for="bb_kg">Berat Badan (KG)</label>
    <input name="bb_kg" type="number" id="bb_kg" placeholder="Berat Badan (KG)" class="form-control" required>
</div>
<div class="form-group">
    <label for="tb_cm">Tinggi Badan (CM)</label>
    <input name="tb_cm" type="number" id="tb_cm" placeholder="Tinggi Badan (CM)" class="form-control" required>
</div>
<div class="form-group">
    <label for="bulan_masuk">Bulan Masuk (Angka)</label>
    <input name="bulan_masuk" number="text" id="bulan_masuk" placeholder="Bulan Masuk (Angka)" class="form-control" required>
</div>
<div class="form-group">
    <label for="bulan_keluar">Bulan Keluar (Angka)</label>
    <input name="bulan_keluar" type="number" id="bulan_keluar" placeholder="Bulan Keluar (Angka)" class="form-control" required>
</div>
<div class="form-group">
    <label for="status_keluarga_gakin">
        <input name="status_keluarga_gakin" type="checkbox" id="status_keluarga_gakin" value="1" class="minimal">
        &nbsp;Keluarga GAKIN
    </label>
</div>
