<script>
    var sub_datatables;
    var sub_data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "nama_balita"},
            {"data": "nama_orang_tua"},
            {"data": "jeniskelamin"},
            {"data": null}
        ];
        sub_datatables = init_datatables($('#sub-datatables'), "<?php echo base_url('api/dinkes/data-gizi-buruk/balita/datatable') ?>", columns, null, null, true);

        $('#sub-datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = sub_datatables.row($(this).parents('tr')).data();

            sub_data_id = data_for_table.idbalita;
            $('#nama_balita').val(data_for_table.nama_balita);
            $('#nama_orang_tua').val(data_for_table.nama_orang_tua);

            fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$(".sub-form-update #idjeniskelamin")], data_for_table, true);

        }).on('click', '.remove_btn', function () {
            var data = sub_datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('api/dinkes/data-gizi-buruk/balita/') ?>" + data.idbalita, sub_datatables, true);
        });
    });

    function sub_show_add() {
        fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$(".sub-form-add #idjeniskelamin")], null, true);
    }

    function sub_add() {
        save_new_data("<?php echo base_url('api/dinkes/data-gizi-buruk/balita') ?>", sub_datatables, true);
    }

    function sub_update() {
        update_data("<?php echo base_url('api/dinkes/data-gizi-buruk/balita/') ?>" + sub_data_id, sub_datatables, true);
    }
</script>
