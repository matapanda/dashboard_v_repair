<div class="form-group">
    <label for="nama_balita">Nama Balita</label>
    <input name="nama_balita" type="text" id="nama_balita" placeholder="Nama Balita" class="form-control" required>
</div>
<div class="form-group">
    <label for="nama_orang_tua">Nama Orang Tua</label>
    <input name="nama_orang_tua" type="text" id="nama_orang_tua" placeholder="Nama Orang Tua" class="form-control" required>
</div>
<div class="form-group">
    <label for="idjeniskelamin">Jenis Kelamin</label><br>
    <select name="idjeniskelamin" id="idjeniskelamin" class="form-control"></select>
</div>