<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "no_kelurahan"},
            {"data": "kelurahan"},
            {"data": "puskesmas"},
            {"data": "nama_balita"},
            {"data": "nama_orang_tua"},
            {"data": "alamat"},
            {"data": "rt"},
            {"data": "rw"},
            {"data": "umur_bulan"},
            {"data": "bb_kg"},
            {"data": "tb_cm"},
            {"data": "bulan_masuk"},
            {"data": "bulan_keluar"},
            {
                "data": "status_keluarga_gakin",
                "render": function (data, type, row) {
                    if (data == 1) {
                        return '<span class="label label-info">GAKIN</span>'
                    }
                    return '<span class="label label-success">NONGAKIN</span>'
                }
            },
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('api/dinkes/data-gizi-buruk/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.iddata_gizi_buruk;
            $('#tanggal').val(data_for_table.tanggal);
            $('#no_kelurahan').val(data_for_table.no_kelurahan);
            $('#nama_orang_tua').val(data_for_table.nama_orang_tua);
            $('#alamat').val(data_for_table.alamat);
            $('#rt').val(data_for_table.rt);
            $('#rw').val(data_for_table.rw);
            $('#umur_bulan').val(data_for_table.umur_bulan);
            $('#bb_kg').val(data_for_table.bb_kg);
            $('#tb_cm').val(data_for_table.tb_cm);
            $('#bulan_masuk').val(data_for_table.bulan_masuk);
            $('#bulan_keluar').val(data_for_table.bulan_keluar);
            $('#status_keluarga_gakin').val(data_for_table.status_keluarga_gakin);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);

            //get puskesmas
            $("#idpuskesmas").empty();
            $.ajax({
                url: "<?php echo base_url('api/dinkes/data-gizi-buruk/puskesmas') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idpuskesmas").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idpuskesmas").append($("<option />").val(this.idpuskesmas).text(this.no_puskesmas+'. '+this.puskesmas));
                    });
                    $('#idpuskesmas').val(data_for_table.idpuskesmas);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data puskesmas', "error");
            });

            //get balita
            $("#idbalita").empty();
            $.ajax({
                url: "<?php echo base_url('api/dinkes/data-gizi-buruk/balita') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idbalita").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idbalita").append($("<option />").val(this.idbalita).text(this.nama_balita + ' - '+ this.nama_orang_tua));
                    });
                    $('#idbalita').val(data_for_table.idbalita);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data balita', "error");
            });

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('api/dinkes/data-gizi-buruk/') ?>" + data.iddata_gizi_buruk, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);

        //get puskesmas
        $(".form-add #idpuskesmas").empty();
        $.ajax({
            url: "<?php echo base_url('api/dinkes/data-gizi-buruk/puskesmas') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if(data.success){
                $('#modal-add select').css('width', '100%');
                $(".form-add #idpuskesmas").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function() {
                    $(".form-add #idpuskesmas").append($("<option />").val(this.idpuskesmas).text(this.no_puskesmas+'. '+this.puskesmas));
                });
            }else{
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data puskesmas', "error");
        });

        //get persalinan kendaraan
        $(".form-add #idbalita").empty();
        $.ajax({
            url: "<?php echo base_url('api/dinkes/data-gizi-buruk/balita') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idbalita").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idbalita").append($("<option />").val(this.idbalita).text(this.nama_balita+' - '+this.nama_orang_tua));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data balita', "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('api/dinkes/data-gizi-buruk') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('api/dinkes/data-gizi-buruk/') ?>" + data_id, datatables);
    }
</script>
