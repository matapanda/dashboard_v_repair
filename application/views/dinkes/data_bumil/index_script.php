<script>
    var datatables;
    var data_id;

    $(document).ready(function () {
        var columns = [
            {"data": "no"},
            {"data": "tanggal"},
            {"data": "puskesmas"},
            {"data": "kelurahan"},
            {"data": "nama_ibu"},
            {"data": "nama_suami"},
            {"data": "alamat"},
            {"data": "rt"},
            {"data": "rw"},
            {"data": "no_nik_ibu"},
            {"data": "hpht"},
            {"data": "tafsiran_persalinan"},
            {"data": "umur_ibu"},
            {"data": "kehamilan_minggu"},
            {"data": "hamil_ke"},
            {"data": "bb"},
            {"data": "tb"},
            {"data": "lila_imt"},
            {"data": "hb_golda"},
            {"data": "tensi"},
            {"data": "pendeteksi_resiko_nakes"},
            {"data": "pendeteksi_resiko_masyarakat"},
            {"data": "faktor_resiko"},
            {"data": "jarak_kehamilan"},
            {"data": "imunisasi_tt_status_imunisasi_tt"},
            {"data": "imunisasi_tt_pemberian_imunisasi_tt"},
            {"data": "rencana_persalinan_tempat"},
            {"data": "rencana_persalinan_biaya"},
            {"data": "rencana_persalinan_kendaraan"},
            {"data": "anc_terpadu_albumin"},
            {"data": "anc_terpadu_reduksi"},
            {"data": "anc_terpadu_gds"},
            {"data": "anc_terpadu_hbsag"},
            {"data": "anc_terpadu_hiv"},
            {"data": "keterangan_bpjs"},
            {"data": "username"},
            {"data": null}
        ];
        datatables = init_datatables($('#datatables'), "<?php echo base_url('api/dinkes/data-bumil/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {
            var data_for_table = datatables.row($(this).parents('tr')).data();

            data_id = data_for_table.iddata_bumil;
            $('#tanggal').val(data_for_table.tanggal);
            $('#puskesmas').val(data_for_table.puskesmas);
            $('#nama_ibu').val(data_for_table.nama_ibu);
            $('#nama_suami').val(data_for_table.nama_suami);
            $('#alamat').val(data_for_table.alamat);
            $('#rt').val(data_for_table.rt);
            $('#rw').val(data_for_table.rw);
            $('#no_nik_ibu').val(data_for_table.no_nik_ibu);
            $('#hpht').val(data_for_table.hpht);
            $('#tafsiran_persalinan').val(data_for_table.tafsiran_persalinan);
            $('#umur_ibu').val(data_for_table.umur_ibu);
            $('#kehamilan_minggu').val(data_for_table.kehamilan_minggu);
            $('#hamil_ke').val(data_for_table.hamil_ke);
            $('#bb').val(data_for_table.bb);
            $('#tb').val(data_for_table.tb);
            $('#lila_imt').val(data_for_table.lila_imt);
            $('#hb_golda').val(data_for_table.hb_golda);
            $('#tensi').val(data_for_table.tensi);
            $('#pendeteksi_resiko_nakes').val(data_for_table.pendeteksi_resiko_nakes);
            $('#pendeteksi_resiko_masyarakat').val(data_for_table.pendeteksi_resiko_masyarakat);
            $('#faktor_resiko').val(data_for_table.faktor_resiko);
            $('#jarak_kehamilan').val(data_for_table.jarak_kehamilan);
            $('#imunisasi_tt_status_imunisasi_tt').val(data_for_table.imunisasi_tt_status_imunisasi_tt);
            $('#imunisasi_tt_pemberian_imunisasi_tt').val(data_for_table.imunisasi_tt_pemberian_imunisasi_tt);
            $('#rencana_persalinan_tempat').val(data_for_table.rencana_persalinan_tempat);
            $('#anc_terpadu_albumin').val(data_for_table.anc_terpadu_albumin);
            $('#anc_terpadu_reduksi').val(data_for_table.anc_terpadu_reduksi);
            $('#anc_terpadu_gds').val(data_for_table.anc_terpadu_gds);
            $('#anc_terpadu_hbsag').val(data_for_table.anc_terpadu_hbsag);
            $('#anc_terpadu_hiv').val(data_for_table.anc_terpadu_hiv);

            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$("#idkelurahan")], data_for_table);

            //get persalinan biaya
            $("#idrencana_persalinan_biaya").empty();
            $.ajax({
                url: "<?php echo base_url('api/dinkes/data-bumil/rencana-persalinan-biaya') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idrencana_persalinan_biaya").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idrencana_persalinan_biaya").append($("<option />").val(this.idrencana_persalinan_biaya).text(this.rencana_persalinan_biaya));
                    });
                    $('#idrencana_persalinan_biaya').val(data_for_table.idrencana_persalinan_biaya);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data biaya persalinan', "error");
            });

            //get persalinan kendaraan
            $("#idrencana_persalinan_kendaraan").empty();
            $.ajax({
                url: "<?php echo base_url('api/dinkes/data-bumil/rencana-persalinan-kendaraan') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idrencana_persalinan_kendaraan").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idrencana_persalinan_kendaraan").append($("<option />").val(this.idrencana_persalinan_kendaraan).text(this.rencana_persalinan_kendaraan));
                    });
                    $('#idrencana_persalinan_kendaraan').val(data_for_table.idrencana_persalinan_kendaraan);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data kendaraan persalinan', "error");
            });

            //get keterangan bpjs
            $("#idketerangan_bpjs").empty();
            $.ajax({
                url: "<?php echo base_url('api/dinkes/data-bumil/keterangan-bpjs') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $("#idketerangan_bpjs").select2({
                        dropdownParent: $('#modal-edit')
                    });
                    $.each(data.data, function () {
                        $("#idketerangan_bpjs").append($("<option />").val(this.idketerangan_bpjs).text(this.keterangan_bpjs));
                    });
                    $('#idketerangan_bpjs').val(data_for_table.idketerangan_bpjs);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data kendaraan persalinan', "error");
            });

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('api/dinkes/data-bumil/') ?>" + data.iddata_bumil, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")]);

        //get kategori pkh data
        $(".form-add #idrencana_persalinan_biaya").empty();
        $.ajax({
            url: "<?php echo base_url('api/dinkes/data-bumil/rencana-persalinan-biaya') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if(data.success){
                $('#modal-add select').css('width', '100%');
                $(".form-add #idrencana_persalinan_biaya").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function() {
                    $(".form-add #idrencana_persalinan_biaya").append($("<option />").val(this.idrencana_persalinan_biaya).text(this.rencana_persalinan_biaya));
                });
            }else{
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data biaya persalinan', "error");
        });

        //get persalinan kendaraan
        $(".form-add #idrencana_persalinan_kendaraan").empty();
        $.ajax({
            url: "<?php echo base_url('api/dinkes/data-bumil/rencana-persalinan-kendaraan') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idrencana_persalinan_kendaraan").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idrencana_persalinan_kendaraan").append($("<option />").val(this.idrencana_persalinan_kendaraan).text(this.rencana_persalinan_kendaraan));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data kendaraan persalinan', "error");
        });

        //get keterangan bpjs
        $(".form-add #idketerangan_bpjs").empty();
        $.ajax({
            url: "<?php echo base_url('api/dinkes/data-bumil/keterangan-bpjs') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $(".form-add #idketerangan_bpjs").select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $(".form-add #idketerangan_bpjs").append($("<option />").val(this.idketerangan_bpjs).text(this.keterangan_bpjs));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan saat mengambil data kendaraan persalinan', "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('api/dinkes/data-bumil') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('api/dinkes/data-bumil/') ?>" + data_id, datatables);
    }
</script>
