<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" placeholder="Tanggal" required>
</div>
<div class="form-group">
    <label for="puskesmas">Puskesmas</label>
    <input name="puskesmas" type="text" id="puskesmas" placeholder="Puskesmas" class="form-control" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label><br>
    <select name="idkelurahan" id="idkelurahan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="rencana_persalinan_tempat">Tempat Rencana Persalinan</label>
    <input name="rencana_persalinan_tempat" type="text" class="form-control" id="rencana_persalinan_tempat" placeholder="Rencana Persalinan Tempat" required>
</div>
<div class="form-group">
    <label for="idrencana_persalinan_biaya">Biaya Rencana Persalinan</label><br>
    <select name="idrencana_persalinan_biaya" id="idrencana_persalinan_biaya" class="form-control"></select>
</div>
<div class="form-group">
    <label for="idrencana_persalinan_kendaraan">Kendaraan Rencana Persalinan</label><br>
    <select name="idrencana_persalinan_kendaraan" id="idrencana_persalinan_kendaraan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="idketerangan_bpjs">Keterangan BPJS</label><br>
    <select name="idketerangan_bpjs" id="idketerangan_bpjs" class="form-control"></select>
</div>
<div class="form-group">
    <label for="nama_ibu">Nama Ibu</label>
    <input name="nama_ibu" type="text" id="nama_ibu" placeholder="Nama Ibu" class="form-control" required>
</div>
<div class="form-group">
    <label for="nama_suami">Nama Suami</label>
    <input name="nama_suami" type="text" id="nama_suami" placeholder="Nama Suami" class="form-control" required>
</div>
<div class="form-group">
    <label for="alamat">Alamat</label>
    <input name="alamat" type="text" id="alamat" placeholder="Alamat" class="form-control" required>
</div>
<div class="form-group">
    <label for="rt">RT</label>
    <input name="rt" type="number" id="rt" placeholder="RT" class="form-control" required>
</div>
<div class="form-group">
    <label for="rw">RW</label>
    <input name="rw" type="number" id="rw" placeholder="RW" class="form-control" required>
</div>
<div class="form-group">
    <label for="no_nik_ibu">No. NIK Ibu</label>
    <input name="no_nik_ibu" type="text" id="no_nik_ibu" placeholder="NIK Ibu" class="form-control" required>
</div>
<div class="form-group">
    <label for="hpht">HPHT</label>
    <input name="hpht" type="date" class="form-control" id="hpht" placeholder="HPHT" required>
</div>
<div class="form-group">
    <label for="tafsiran_persalinan">Tafsiran Persalinan</label>
    <input name="tafsiran_persalinan" type="date" class="form-control" id="tafsiran_persalinan" placeholder="Tafsiran Persalinan" required>
</div>
<div class="form-group">
    <label for="umur_ibu">Umur Ibu</label>
    <input name="umur_ibu" type="number" class="form-control" id="umur_ibu" placeholder="Umur Ibu" required>
</div>
<div class="form-group">
    <label for="kehamilan_minggu">Minggu Kehamilan</label>
    <input name="kehamilan_minggu" type="number" class="form-control" id="kehamilan_minggu" placeholder="Minggu Kehamilan" required>
</div>
<div class="form-group">
    <label for="hamil_ke">Hamil Ke-</label>
    <input name="hamil_ke" type="number" class="form-control" id="hamil_ke" placeholder="Hamil Ke-" required>
</div>
<div class="form-group">
    <label for="bb">BB</label>
    <input name="bb" type="number" class="form-control" id="bb" placeholder="BB" required>
</div>
<div class="form-group">
    <label for="tb">TB</label>
    <input name="tb" type="number" class="form-control" id="tb" placeholder="TB" required>
</div>
<div class="form-group">
    <label for="lila_imt">LILA/IMT</label>
    <input name="lila_imt" type="number" class="form-control" id="lila_imt" placeholder="LILA/IMT" required>
</div>
<div class="form-group">
    <label for="hb_golda">HB/GOLDA</label>
    <input name="hb_golda" type="text" class="form-control" id="hb_golda" placeholder="HB/GOLDA" required>
</div>
<div class="form-group">
    <label for="tensi">Tensi</label>
    <input name="tensi" type="text" class="form-control" id="tensi" placeholder="Tensi" required>
</div>
<div class="form-group">
    <label for="pendeteksi_resiko_nakes">Pendeteksi Resiko (NAKES)</label>
    <input name="pendeteksi_resiko_nakes" type="number" class="form-control" id="pendeteksi_resiko_nakes" placeholder="Pendeteksi Resiko (NAKES)" required>
</div>
<div class="form-group">
    <label for="pendeteksi_resiko_masyarakat">Pendeteksi Resiko (Masyarakat)</label>
    <input name="pendeteksi_resiko_masyarakat" type="number" class="form-control" id="pendeteksi_resiko_masyarakat" placeholder="Pendeteksi Resiko (Masyarakat)" required>
</div>
<div class="form-group">
    <label for="faktor_resiko">Faktor Resiko</label>
    <input name="faktor_resiko" type="text" class="form-control" id="faktor_resiko" placeholder="Faktor Resiko" required>
</div>
<div class="form-group">
    <label for="jarak_kehamilan">Jarak Kehamilan</label>
    <input name="jarak_kehamilan" type="text" class="form-control" id="jarak_kehamilan" placeholder="Jarak Kehamilan" required>
</div>
<div class="form-group">
    <label for="imunisasi_tt_status_imunisasi_tt">Status Imunisasi TT</label>
    <input name="imunisasi_tt_status_imunisasi_tt" type="text" class="form-control" id="imunisasi_tt_status_imunisasi_tt" placeholder="Status Imunisasi TT" required>
</div>
<div class="form-group">
    <label for="imunisasi_tt_pemberian_imunisasi_tt">Pemberian Imunisasi TT</label>
    <input name="imunisasi_tt_pemberian_imunisasi_tt" type="text" class="form-control" id="imunisasi_tt_pemberian_imunisasi_tt" placeholder="Pemberian Imunisasi TT" required>
</div>
<div class="form-group">
    <label for="anc_terpadu_albumin">ANC Terpadu (Albumin)</label>
    <input name="anc_terpadu_albumin" type="text" class="form-control" id="anc_terpadu_albumin" placeholder="Albumin" required>
</div>
<div class="form-group">
    <label for="anc_terpadu_reduksi">ANC Terpadu (Reduksi)</label>
    <input name="anc_terpadu_reduksi" type="text" class="form-control" id="anc_terpadu_reduksi" placeholder="Reduksi" required>
</div>
<div class="form-group">
    <label for="anc_terpadu_gds">ANC Terpadu (GDS)</label>
    <input name="anc_terpadu_gds" type="number" class="form-control" id="anc_terpadu_gds" placeholder="GDS" required>
</div>
<div class="form-group">
    <label for="anc_terpadu_hbsag">ANC Terpadu (HBSAG)</label>
    <input name="anc_terpadu_hbsag" type="text" class="form-control" id="anc_terpadu_hbsag" placeholder="HBSAG" required>
</div>
<div class="form-group">
    <label for="anc_terpadu_hiv">ANC Terpadu (HIV)</label>
    <input name="anc_terpadu_hiv" type="text" class="form-control" id="anc_terpadu_hiv" placeholder="HIV" required>
</div>
