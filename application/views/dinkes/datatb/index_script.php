<script>
    $(document).ready(function () {

        var columns = [
            {"data": "no"},
            {'data': 'tanggal'},
            {'data': 'validasi_data'},
            {'data': 'nik'},
            {'data': 'nama_pasien'},
            {'data': 'idpasien'},
            {'data': 'jeniskelamin'},
            {'data': 'tanggal_lahir'},
            {'data': 'umur'},
            {'data': 'nomor_registrasi_kab_kota'},
            {'data': 'kelurahan'},
            {'data': 'dirujuk_oleh'},
            {'data': 'fayankes'},
            {'data': 'jenis_fayankes'},
            {'data': 'tanggal_mulai_pengobatan'},
            {'data': 'bb_kg'},
            {'data': 'tb_cm'},
            {'data': 'total_scoring_pada_tb_anak'},
            {'data': 'kode_paduan_rejimen_yang_diberikan'},
            {'data': 'klasifikasi_penyakit'},
            {'data': 'tipe_pasien'},
            {'data': 'sebelum_pengobatan_hasil_dahak'},
            {'data': 'sebelum_pengobatan_no_reg_lab'},
            {'data': 'akhir_tahap_awal_hasil_dahak'},
            {'data': 'akhir_tahap_awal_no_reg_lab'},
            {'data': 'akhir_sisipan_hasil_dahak'},
            {'data': 'akhir_sisipan_no_reg_lab'},
            {'data': 'akhir_bulan_ke_5_7_hasil_dahak'},
            {'data': 'akhir_bulan_ke_5_7_no_reg_lab'},
            {'data': 'akhir_pengobatan_hasil_dahak'},
            {'data': 'akhir_pengobatan_no_reg_lab'},
            {'data': 'hasil_pengobatan_default'},
            {'data': 'hasil_pengobatan_gagal'},
            {'data': 'hasil_pengobatan_lengkap'},
            {'data': 'hasil_pengobatan_meninggal'},
            {'data': 'hasil_pengobatan_pindah'},
            {'data': 'hasil_pengobatan_sembuh'},
            {'data': 'riwayat_tes_hiv_hasil_tes'},
            {'data': 'riwayat_tes_hiv_tanggal_tes_hiv_terakhir'},
            {'data': 'layanan_konseling_hasil_test'},
            {'data': 'layanan_konseling_tanggal_dianjurkan'},
            {'data': 'layanan_konseling_tanggal_pasca_test_konseling'},
            {'data': 'layanan_konseling_tanggal_pretest_konseling'},
            {'data': 'layanan_konseling_tanggal_test_hiv'},
            {'data': 'layanan_konseling_tempat_test'},
            {'data': 'area_sentinel_hasil_test'},
            {'data': 'area_sentinel_tanggal_dianjurkan'},
            {'data': 'area_sentinel_tanggal_pretest_konseling'},
            {'data': 'area_sentinel_tanggal_test_hiv'},
            {'data': 'layanan_pdp_tanggal_mulai_art'},
            {'data': 'layanan_pdp_tanggal_mulai_ppk'},
            {'data': 'layanan_pdp_tanggal_rujukan_pdp'},
            {'data': 'username'},
            {"data": null}
        ];

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/dinkes/data-tb/datatable') ?>", columns);

        $('#datatables tbody').on('click', '.update_btn', function () {

            var data_for_table = datatables.row($(this).parents('tr')).data();
            data_id = data_for_table.iddata_tb;


            fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-update #idkelurahan")], data_for_table, false, true);
            fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$(".form-update #idjeniskelamin")], data_for_table, false, true);

            $('#nik').val(data_for_table.nik);
            $('#nama_pasien').val(data_for_table.nama_pasien);
            $('#tanggal_lahir').val(data_for_table.tanggal_lahir);
            $('#umur').val(data_for_table.umur);
            $('#nomor_registrasi_kab_kota').val(data_for_table.nomor_registrasi_kab_kota);
            $('#dirujuk_oleh').val(data_for_table.dirujuk_oleh);
            $('#tanggal_mulai_pengobatan').val(data_for_table.tanggal_mulai_pengobatan);
            $('#bb_kg').val(data_for_table.bb_kg);
            $('#tb_cm').val(data_for_table.tb_cm);
            $('#total_scoring_pada_tb_anak').val(data_for_table.total_scoring_pada_tb_anak);
            $('#sebelum_pengobatan_no_reg_lab').val(data_for_table.sebelum_pengobatan_no_reg_lab);
            $('#sebelum_pengobatan_hasil_dahak').val(data_for_table.sebelum_pengobatan_hasil_dahak);
            $('#akhir_tahap_awal_no_reg_lab').val(data_for_table.akhir_tahap_awal_no_reg_lab);
            $('#akhir_tahap_awal_hasil_dahak').val(data_for_table.akhir_tahap_awal_hasil_dahak);
            $('#akhir_bulan_ke_5_7_no_reg_lab').val(data_for_table.akhir_bulan_ke_5_7_no_reg_lab);
            $('#akhir_bulan_ke_5_7_hasil_dahak').val(data_for_table.akhir_bulan_ke_5_7_hasil_dahak);
            $('#akhir_pengobatan_no_reg_lab').val(data_for_table.akhir_pengobatan_no_reg_lab);
            $('#akhir_pengobatan_hasil_dahak').val(data_for_table.akhir_pengobatan_hasil_dahak);
            $('#hasil_pengobatan_sembuh').val(data_for_table.hasil_pengobatan_sembuh);
            $('#hasil_pengobatan_lengkap').val(data_for_table.hasil_pengobatan_lengkap);
            $('#hasil_pengobatan_default').val(data_for_table.hasil_pengobatan_default);
            $('#hasil_pengobatan_gagal').val(data_for_table.hasil_pengobatan_gagal);
            $('#hasil_pengobatan_meninggal').val(data_for_table.hasil_pengobatan_meninggal);
            $('#hasil_pengobatan_pindah').val(data_for_table.hasil_pengobatan_pindah);
            $('#riwayat_tes_hiv_tanggal_tes_hiv_terakhir').val(data_for_table.riwayat_tes_hiv_tanggal_tes_hiv_terakhir);
            $('#riwayat_tes_hiv_hasil_tes').val(data_for_table.riwayat_tes_hiv_hasil_tes);
            $('#layanan_konseling_tanggal_test_hiv').val(data_for_table.layanan_konseling_tanggal_test_hiv);
            $('#layanan_konseling_tempat_test').val(data_for_table.layanan_konseling_tempat_test);
            $('#layanan_konseling_hasil_test').val(data_for_table.layanan_konseling_hasil_test);
            $('#layanan_konseling_tanggal_pasca_test_konseling').val(data_for_table.layanan_konseling_tanggal_pasca_test_konseling);
            $('#layanan_konseling_tanggal_pretest_konseling').val(data_for_table.layanan_konseling_tanggal_pretest_konseling);
            $('#layanan_konseling_tanggal_dianjurkan').val(data_for_table.layanan_konseling_tanggal_dianjurkan);
            $('#layanan_pdp_tanggal_rujukan_pdp').val(data_for_table.layanan_pdp_tanggal_rujukan_pdp);
            $('#layanan_pdp_tanggal_mulai_ppk').val(data_for_table.layanan_pdp_tanggal_mulai_ppk);
            $('#layanan_pdp_tanggal_mulai_art').val(data_for_table.layanan_pdp_tanggal_mulai_art);
            $('#tanggal').val(data_for_table.tanggal);
            $('#idpasien').val(data_for_table.idpasien);
            $('#akhir_sisipan_no_reg_lab').val(data_for_table.akhir_sisipan_no_reg_lab);
            $('#akhir_sisipan_hasil_dahak').val(data_for_table.akhir_sisipan_hasil_dahak);
            $('#area_sentinel_tanggal_pretest_konseling').val(data_for_table.area_sentinel_tanggal_pretest_konseling);
            $('#area_sentinel_tanggal_dianjurkan').val(data_for_table.area_sentinel_tanggal_dianjurkan);
            $('#area_sentinel_tanggal_test_hiv').val(data_for_table.area_sentinel_tanggal_test_hiv);
            $('#area_sentinel_hasil_test').val(data_for_table.area_sentinel_hasil_test);
            $('#layanan_pdp_tanggal_rujukan_pdp').val(data_for_table.layanan_pdp_tanggal_rujukan_pdp);
            $('#layanan_pdp_tanggal_mulai_ppk').val(data_for_table.layanan_pdp_tanggal_mulai_ppk);
            $('#layanan_pdp_tanggal_mulai_art').val(data_for_table.layanan_pdp_tanggal_mulai_art);

            $.ajax({
                url: "<?php echo base_url('/api/dinkes/validasi-data') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $.each(data.data, function () {
                        $("#idvalidasi_data").append($("<option />").val(this.idvalidasi_data).text(this.validasi_data));
                    });
                    $('#idvalidasi_data').val(data_for_table.idvalidasi_data);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });
            $.ajax({
                url: "<?php echo base_url('/api/dinkes/fayankes') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $.each(data.data, function () {
                        $("#idfayankes").append($("<option />").val(this.idfayankes).text(this.fayankes));
                    });
                    $('#idfayankes').val(data_for_table.idfayankes);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });
            $.ajax({
                url: "<?php echo base_url('/api/dinkes/jenis-fayankes') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $.each(data.data, function () {
                        $("#idjenis_fayankes").append($("<option />").val(this.idjenis_fayankes).text(this.jenis_fayankes));
                    });
                    $('#idjenis_fayankes').val(data_for_table.idjenis_fayankes);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });
            $.ajax({
                url: "<?php echo base_url('/api/dinkes/kode-rejimen') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $.each(data.data, function () {
                        $("#idkode_paduan_rejimen_yang_diberikan").append($("<option />").val(this.idkode_paduan_rejimen_yang_diberikan).text(this.kode_paduan_rejimen_yang_diberikan));
                    });
                    $('#idkode_paduan_rejimen_yang_diberikan').val(data_for_table.idkode_paduan_rejimen_yang_diberikan);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });
            $.ajax({
                url: "<?php echo base_url('/api/dinkes/klasifikasi-penyakit') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $.each(data.data, function () {
                        $("#idklasifikasi_penyakit").append($("<option />").val(this.idklasifikasi_penyakit).text(this.klasifikasi_penyakit));
                    });
                    $('#idklasifikasi_penyakit').val(data_for_table.idklasifikasi_penyakit);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });
            $.ajax({
                url: "<?php echo base_url('/api/dinkes/tipe-pasien') ?>",
                type: "GET"
            }).done(function (result) {
                var data = jQuery.parseJSON(JSON.stringify(result));
                if (data.success) {
                    $('#modal-edit select').css('width', '100%');
                    $.each(data.data, function () {
                        $("#idtipe_pasien").append($("<option />").val(this.idtipe_pasien).text(this.tipe_pasien));
                    });
                    $('#idtipe_pasien').val(data_for_table.idtipe_pasien);
                } else {
                    sweetAlert("gagal", data.message, "error");
                }
            }).fail(function (xhr, status, errorThrown) {
                sweetAlert("Gagal", status, "error");
            });

        }).on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_data("<?php echo base_url('/api/dinkes/data-tb/') ?>" + data.iddata_tb, datatables);
        });
    });

    function showAdd() {
        fill_select_master('kelurahan', "<?php echo base_url() ?>", [$(".form-add #idkelurahan")], false, false, true);
        fill_select_master('jeniskelamin', "<?php echo base_url() ?>", [$(".form-add #idjeniskelamin")], false, false, true);
        $.ajax({
            url: "<?php echo base_url('/api/dinkes/validasi-data') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $.each(data.data, function () {
                    $(".form-add #idvalidasi_data").append($("<option />").val(this.idvalidasi_data).text(this.validasi_data));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
        $.ajax({
            url: "<?php echo base_url('/api/dinkes/fayankes') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $.each(data.data, function () {
                    $(".form-add #idfayankes").append($("<option />").val(this.idfayankes).text(this.fayankes));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
        $.ajax({
            url: "<?php echo base_url('/api/dinkes/jenis-fayankes') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $.each(data.data, function () {
                    $(".form-add #idjenis_fayankes").append($("<option />").val(this.idjenis_fayankes).text(this.jenis_fayankes));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
        $.ajax({
            url: "<?php echo base_url('/api/dinkes/kode-rejimen') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $.each(data.data, function () {
                    $(".form-add #idkode_paduan_rejimen_yang_diberikan").append($("<option />").val(this.idkode_paduan_rejimen_yang_diberikan).text(this.kode_paduan_rejimen_yang_diberikan));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
        $.ajax({
            url: "<?php echo base_url('/api/dinkes/klasifikasi-penyakit') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $.each(data.data, function () {
                    $(".form-add #idklasifikasi_penyakit").append($("<option />").val(this.idklasifikasi_penyakit).text(this.klasifikasi_penyakit));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
        $.ajax({
            url: "<?php echo base_url('/api/dinkes/tipe-pasien') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $.each(data.data, function () {
                    $(".form-add #idtipe_pasien").append($("<option />").val(this.idtipe_pasien).text(this.tipe_pasien));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
    }

    function add() {
        save_new_data("<?php echo base_url('/api/dinkes/data-tb') ?>", datatables);
    }

    function update() {
        update_data("<?php echo base_url('/api/dinkes/data-tb/') ?>" + data_id, datatables);
    }
</script>
