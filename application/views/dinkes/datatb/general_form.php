<div class="form-group">
    <label for="tanggal">Tanggal</label>
    <input name="tanggal" type="date" class="form-control" id="tanggal" required>
</div>
<div class="form-group">
    <label for="idpasien">Id Pasien</label>
    <input name="idpasien" type="text" class="form-control" id="idpasien" placeholder="Id Pasien" required>
</div>
<div class="form-group">
  <label for="idvalidasi_data">Validasi Data</label>
  <select name="idvalidasi_data" id="idvalidasi_data" class="form-control"></select>
</div>
<div class="form-group">
  <label for="nik">NIK</label>
  <input name="nik" type="text" class="form-control" id="nik" placeholder="NIK" required>
</div>
<div class="form-group">
  <label for="nama_pasien">Nama Pasien</label>
  <input name="nama_pasien" type="text" class="form-control" id="nama_pasien" placeholder="Nama Pasien" required>
</div>
<div class="form-group">
  <label for="idjeniskelamin">Jenis Kelamin</label>
  <select name="idjeniskelamin" id="idjeniskelamin" class="form-control"></select>
</div>
<table class="table text-center table-bordered">
  <tr>
    <th>Tanggal Lahir</th>
    <th>Umur</th>
  </tr>
  <tr>
    <td><input name="tanggal_lahir" type="date" class="form-control" id="tanggal_lahir" placeholder="Tanggal Lahir"></td>
    <td><input name="umur" type="number" id="umur" placeholder="Umur" class="form-control"></td>
  </tr>
</table>
<div class="form-group">
  <label for="nomor_registrasi_kab_kota">Nomor Register Kabupaten/Kota</label>
  <input name="nomor_registrasi_kab_kota" type="text" class="form-control" id="nomor_registrasi_kab_kota" placeholder="Nomor Register Kabupaten/Kota" required>
</div>
<div class="form-group">
    <label for="idkelurahan">Kelurahan</label>
    <select name="idkelurahan" id="idkelurahan" class="form-control"></select>
</div>
<div class="form-group">
    <label for="dirujuk_oleh">Dirujuk Oleh</label>
    <input name="dirujuk_oleh" type="text" id="dirujuk_oleh" placeholder="Dirujuk Oleh" class="form-control">
</div>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Fasyankes</th>
  </tr>
  <tr>
    <th>Nama</th>
    <th>Jenis</th>
  </tr>
  <tr>
    <td><select name="idfayankes" id="idfayankes" class="form-control"></select></td>
    <td><select name="idjenis_fayankes" id="idjenis_fayankes" class="form-control"></select></td>
  </tr>
</table>
<div class="form-group">
  <label for="tanggal_mulai_pengobatan">Tanggal Mulai Pengobatan</label>
  <input name="tanggal_mulai_pengobatan" type="date" class="form-control" id="tanggal_mulai_pengobatan" placeholder="Tanggal Mulai Pengobatan" required>
</div>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Fisik</th>
  </tr>
  <tr>
    <th>Berat Badan (kg)</th>
    <th>Tinggi Badan (cm)</th>
  </tr>
  <tr>
    <td><input name="bb_kg" type="number" class="form-control" id="bb_kg" placeholder="Berat Badan (kg)"></td>
    <td><input name="tb_cm" type="number" id="tb_cm" placeholder="Tinggi Badan (cm)" class="form-control"></td>
  </tr>
</table>
<div class="form-group">
  <label for="total_scoring_pada_tb_anak">Total Scoring pada TB Anak</label>
  <input name="total_scoring_pada_tb_anak" type="text" class="form-control" id="total_scoring_pada_tb_anak" placeholder="Total Scoring pada TB Anak" required>
</div>
<div class="form-group">
  <label for="idkode_paduan_rejimen_yang_diberikan">Kode Paduan Rejimen yang Diberikan</label>
  <select name="idkode_paduan_rejimen_yang_diberikan" id="idkode_paduan_rejimen_yang_diberikan" class="form-control"></select>
</div>
<div class="form-group">
  <label for="idklasifikasi_penyakit">Klasifikasi Penyakit</label>
  <select name="idklasifikasi_penyakit" id="idklasifikasi_penyakit" class="form-control"></select>
</div>
<div class="form-group">
  <label for="idtipe_pasien">Tipe Pasien</label>
  <select name="idtipe_pasien" id="idtipe_pasien" class="form-control"></select>
</div>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Sebelum Pengobatan</th>
  </tr>
  <tr>
    <th>No Reg Lab</th>
    <th>Hasil Dahak</th>
  </tr>
  <tr>
    <td><input name="sebelum_pengobatan_no_reg_lab" type="number" class="form-control" id="sebelum_pengobatan_no_reg_lab" placeholder="No Reg Lab"></td>
    <td><input name="sebelum_pengobatan_hasil_dahak" type="text" id="sebelum_pengobatan_hasil_dahak" placeholder="Hasil Dahak" class="form-control"></td>
  </tr>
</table>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Akhir Tahap Awal</th>
  </tr>
  <tr>
    <th>No Reg Lab</th>
    <th>Hasil Dahak</th>
  </tr>
  <tr>
    <td><input name="akhir_tahap_awal_no_reg_lab" type="text" class="form-control" id="akhir_tahap_awal_no_reg_lab" placeholder="No Reg Lab"></td>
    <td><input name="akhir_tahap_awal_hasil_dahak" type="text" id="akhir_tahap_awal_hasil_dahak" placeholder="Hasil Dahak" class="form-control"></td>
  </tr>
</table>
<table class="table text-center table-bordered">
    <tr>
        <th colspan="2">Akhir Sisipan</th>
    </tr>
    <tr>
        <th>No REG Lab</th>
        <th>Hasil Dahak</th>
    </tr>
    <tr>
        <td><input name="akhir_sisipan_no_reg_lab" type="text" class="form-control" id="akhir_sisipan_no_reg_lab" placeholder="No REG LAb"></td>
        <td><input name="akhir_sisipan_hasil_dahak" type="text" id="akhir_sisipan_hasil_dahak" placeholder="Hasil Dahak" class="form-control"></td>
    </tr>
</table>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Akhir Bulan ke 5/7</th>
  </tr>
  <tr>
    <th>No Reg Lab</th>
    <th>Hasil Dahak</th>
  </tr>
  <tr>
    <td><input name="akhir_bulan_ke_5_7_no_reg_lab" type="text" class="form-control" id="akhir_bulan_ke_5_7_no_reg_lab" placeholder="No Reg Lab"></td>
    <td><input name="akhir_bulan_ke_5_7_hasil_dahak" type="text" id="akhir_bulan_ke_5_7_hasil_dahak" placeholder="Hasil Dahak" class="form-control"></td>
  </tr>
</table>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Akhir Pengobatan</th>
  </tr>
  <tr>
    <th>No Reg Lab</th>
    <th>Hasil Dahak</th>
  </tr>
  <tr>
    <td><input name="akhir_pengobatan_no_reg_lab" type="text" class="form-control" id="akhir_pengobatan_no_reg_lab" placeholder="No Reg Lab"></td>
    <td><input name="akhir_pengobatan_hasil_dahak" type="text" id="akhir_pengobatan_hasil_dahak" placeholder="Hasil Dahak" class="form-control"></td>
  </tr>
</table>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Hasil Pengobatan</th>
  </tr>
  <tr>
    <th>Sembuh</th>
    <th>Lengkap</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><input name="hasil_pengobatan_sembuh" type="date" class="form-control" id="hasil_pengobatan_sembuh" placeholder="Sembuh"></td>
    <td><input name="hasil_pengobatan_lengkap" type="date" id="hasil_pengobatan_lengkap" placeholder="Lengkap" class="form-control"></td>
    <td><input name="hasil_pengobatan_default" type="date" class="form-control" id="hasil_pengobatan_default" placeholder="Default"></td>
  </tr>
    <tr>
        <th>Gagal</th>
        <th>Meninggal</th>
        <th>Pindah</th>
    </tr>
    <tr>
        <td><input name="hasil_pengobatan_gagal" type="date" id="hasil_pengobatan_gagal" placeholder="Gagal" class="form-control"></td>
        <td><input name="hasil_pengobatan_meninggal" type="date" class="form-control" id="hasil_pengobatan_meninggal" placeholder="Meninggal"></td>
        <td><input name="hasil_pengobatan_pindah" type="date" id="hasil_pengobatan_pindah" placeholder="Pindah" class="form-control"></td>
    </tr>
</table>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Riwayat Tes HIV</th>
  </tr>
  <tr>
    <th>Tanggal Terakhir Tes HIV</th>
    <th>Hasil Tes</th>
  </tr>
  <tr>
    <td><input name="riwayat_tes_hiv_tanggal_tes_hiv_terakhir" type="date" class="form-control" id="riwayat_tes_hiv_tanggal_tes_hiv_terakhir" placeholder="Tanggal Terakhir Tes HIV"></td>
    <td><input name="riwayat_tes_hiv_hasil_tes" type="text" id="riwayat_tes_hiv_hasil_tes" placeholder="Hasil Tes" class="form-control"></td>
  </tr>
</table>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Layanan Konseling dan Tes Sukarela</th>
  </tr>
  <tr>
    <th>Tanggal Pretest Konseling</th>
    <th>Tanggal Dianjurkan</th>
    <th>Tanggal Tes HIV</th>
  </tr>
  <tr>
    <td><input name="layanan_konseling_tanggal_pretest_konseling" type="date" class="form-control" id="layanan_konseling_tanggal_pretest_konseling" placeholder="Tanggal Pretest Konseling"></td>
    <td><input name="layanan_konseling_tanggal_dianjurkan" type="date" id="layanan_konseling_tanggal_dianjurkan" placeholder="Tanggal Dianjurkan" class="form-control"></td>
    <td><input name="layanan_konseling_tanggal_test_hiv" type="date" class="form-control" id="layanan_konseling_tanggal_test_hiv" placeholder="Tanggal Tes HIV"></td>
  </tr>
  <tr>
    <th>Tempat Tes</th>
    <th>Hasil Test</th>
    <th>Tanggal Pasca Tes Konseling</th>
  </tr>
  <tr>
    <td><input name="layanan_konseling_tempat_test" type="text" id="layanan_konseling_tempat_test" placeholder="Tempat Tes" class="form-control"></td>
    <td><input name="layanan_konseling_hasil_test" type="text" class="form-control" id="layanan_konseling_hasil_test" placeholder="Hasil Test"></td>
    <td><input name="layanan_konseling_tanggal_pasca_test_konseling" type="date" id="layanan_konseling_tanggal_pasca_test_konseling" placeholder="Tanggal Pasca Tes Konseling" class="form-control"></td>
  </tr>
</table>
<table class="table text-center table-bordered">
    <tr>
        <th colspan="2">Khusus Area Sentinel TB-HIV Surveilans</th>
    </tr>
    <tr>
        <th>Tanggal Pretest Konseling</th>
        <th>Tanggal Dianjurkan</th>
    </tr>
    <tr>
        <td><input name="area_sentinel_tanggal_pretest_konseling" type="date" class="form-control" id="area_sentinel_tanggal_pretest_konseling"></td>
        <td><input name="area_sentinel_tanggal_dianjurkan" type="date" class="form-control" id="area_sentinel_tanggal_dianjurkan"></td>
    </tr>
    <tr>
        <th>Tanggal Test HIV</th>
        <th>Hasil Test</th>
    </tr>
    <tr>
        <td><input name="area_sentinel_tanggal_test_hiv" type="date" class="form-control" id="area_sentinel_tanggal_test_hiv"></td>
        <td><input name="area_sentinel_hasil_test" type="text" class="form-control" id="area_sentinel_hasil_test" placeholder="Hasil Test"></td>
    </tr>
</table>
<table class="table text-center table-bordered">
  <tr>
    <th colspan="3">Layanan PDP</th>
  </tr>
  <tr>
    <th>Tanggal Rujukan PDP</th>
    <th>Tanggal Mulai ART</th>
    <th>Tanggal Mulai PPK</th>
  </tr>
  <tr>
    <td><input name="layanan_pdp_tanggal_rujukan_pdp" type="date" class="form-control" id="layanan_pdp_tanggal_rujukan_pdp" placeholder="Tanggal Rujukan PDP"></td>
    <td><input name="layanan_pdp_tanggal_mulai_ppk" type="date" id="layanan_pdp_tanggal_mulai_ppk" placeholder="Tanggal Mulai PPK" class="form-control"></td>
    <td><input name="layanan_pdp_tanggal_mulai_art" type="date" id="layanan_pdp_tanggal_mulai_art" placeholder="Tanggal Mulai ART" class="form-control"></td>
  </tr>
</table>
