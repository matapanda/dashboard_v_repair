
        <div class="box box-solid box-primary" >
            <div class="box-header with-border">
              <center><h3 class="box-title">
                <?php if(isset($title)){
                    echo $title;
                }?>
              </h3></center>
              <div class="box-tools pull-right">
              </div>
            </div>
            
              <div class="box-body chart-responsive" >
                <div id="chartdiv" style="width:100%;"></div>
              </div>
        </div>


<script src="<?php echo base_url();?>assets/js/chart_new.min.js" type="text/javascript"></script>
<script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
<script>
//alert("suray");
$(document).ready(function(){
    //alert($(window).height());
    $("#chartdiv").css({'height':($(window).height()-100)+'px'});
});

var chart;
var graph;
var categoryAxis;

<?php if(isset($chart_js)){
                    echo $chart_js;
                }?>
/*var chartData = 
[
    {
	  "country": "Kedungkandang",
		"visits": 10,
    "color": "#3c8dbc"
	},
	{
		"country": "Sukun",
		"visits": 70,
    "color": "#3c8dbc"
	}, 
	{
		"country": "Klojen",
		"visits": 80,
    "color": "#3c8dbc"
	}, 
	{
	  "country": "Blimbing",
		"visits": 100,
    "color": "#3c8dbc"
	}, 
	{
		"country": "Lowokwaru",
		"visits": 50,
    "color": "#3c8dbc"
	},
];*/


AmCharts.ready(function () {
  chart = new AmCharts.AmSerialChart();
	chart.dataProvider = chartData;
	chart.categoryField = "country";
  chart.position = "left";
  chart.angle = 30;
	chart.depth3D = 15;
  chart.startDuration = 1;
  
  categoryAxis = chart.categoryAxis;
	categoryAxis.labelRotation = 0;
  categoryAxis.dashLength = 5; //
  categoryAxis.gridPosition = "start";
  categoryAxis.autoGridCount = false;
	categoryAxis.gridCount = chartData.length;
  
    
	graph = new AmCharts.AmGraph();
	graph.valueField = "visits";
	graph.type = "column";	
  graph.colorField = "color";
	graph.lineAlpha = 0;
  graph.fillAlphas = 0.8;
  graph.balloonText = "[[category]]: <b>[[value]]</b>";
  
  chart.addGraph(graph);
  
  chart.write('chartdiv');
});



// Reminder: you need to put https://www.google.com/jsapi in the head of your document or as an external resource on codepen //
    
</script>
