<script>
    var $select_subskpd = $('#select-subskpd');
    var $select_based = $('#select-based');
    var $input_year = $('#input-year');
    var $select_month = $('#select-month');
    var $select_param = $('#select-param');
    var $checkbox_year_all = $('#checkbox-year-all');
    var $checkbox_month_all = $('#checkbox-month-all');

    var $select_subskpd_wrapper = $('#select-subskpd-wrapper');
    var $select_based_wrapper = $('#select-based-wrapper');
    var $input_year_wrapper = $('#input-year-wrapper');
    var $select_month_wrapper = $('#select-month-wrapper');
    var $select_param_wrapper = $('#select-param-wrapper');

    var $url_display = $('#url_display');
    var $btn_save_url = $('#btn-save-url');

    var add_param = undefined;

    var final_url = '';

    var no_lain = false;

    var datatables = undefined;
    var datatables_columns = [
        {"data": "judul"},
        {"data": "deskripsi"},
        {
            "data": "url",
            "render": function (data, type, row) {
                return '<a href="' + data + '" target="_blank">Tautan</a>'
            }
        },
        {"data": 'kategori'},
        {"data": null}
    ];


    $(function () {
        //loading screen
        jQuery(document).ajaxStart(function () {
            $('body').loading({
                message: 'Proses...'
            });
        });
        jQuery(document).ajaxStop(function () {
            $('body').loading('stop');
        });

        set_url_display();
        $select_based_wrapper.hide();
        $input_year_wrapper.hide();
        $select_month_wrapper.hide();
        $select_param_wrapper.hide();
        $('select').select2();

        set_select_subskpd();

        datatables = init_datatables($('#datatables'), "<?php echo base_url('/api/app/katalog/0/0/1') ?>", datatables_columns);

        $('#datatables tbody').on('click', '.remove_btn', function () {
            var data = datatables.row($(this).parents('tr')).data();
            remove_katalog("<?php echo base_url('/api/app/katalog/') ?>" + data.idkatalog, datatables);
        });
    });

    function set_url_display(url) {
        if (url) {
            $btn_save_url.attr('disabled', false);
            $url_display.attr('href', url);
            $url_display.html(url);
        } else {
            $btn_save_url.attr('disabled', true);
            $url_display.html('URL akan tampil disini, tekan tombol <b>"Simpan"</b> dibawah untuk menyimpan kedalam katalog');
        }
    }

    function init_datatables(datatables, api_url, columns) {
        var colDef = {
            "targets": -1,
            "data": null,
            "defaultContent": "" +
            "<button class='remove_btn btn btn-danger btn-sm' title='Delete Katalog'><span class='fa fa-trash-o'></span></button>"
        };

        return datatables.DataTable({
            "ajax": api_url,
            "deferRender": true,
            "columns": columns,
            "responsive": true,
            "columnDefs": [colDef],
            "iDisplayLength": 5,
            "autoWidth": false
        });
    }

    function remove_katalog(api_url, datatables) {
        swal({
                title: "Konfirmasi Hapus?",
                text: "Data yang sudah dihapus tidak dapat dikembalikan lagi.",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batal",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                closeOnConfirm: false
            },
            function () {
                $.ajax({
                    url: api_url,
                    type: "DELETE"
                }).done(function (result) {
                    var data = jQuery.parseJSON(JSON.stringify(result));
                    if (data.success) {
                        sweetAlert("Berhasil", data.message, "success");
                        datatables.ajax.reload();
                    } else {
                        sweetAlert("gagal", data.message, "error");
                    }
                }).fail(function (xhr, status, errorThrown) {
                    sweetAlert("Gagal", status, "error");
                })
            });
    }

    function save_url() {
        var $form_add = $('.form-add');
        $.ajax({
            url: "<?php echo base_url('api/app/katalog') ?>",
            data: $form_add.serialize(),
            type: "POST"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                sweetAlert("Berhasil", data.message, "success");
                $('#modal-add').modal('hide');
                $form_add[0].reset();
                datatables.ajax.reload();
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        })
    }

    function show_add() {
        if (final_url == '') {
            sweetAlert("Buat Grafik", 'Pilih data Grafik terlebih dahulu', "warning")
            return;
        }

        var $idkategori = $('#idkategori');
        var $input_url = $('#input-url');

        $idkategori.empty();
        $input_url.val(final_url);

        //get data kategori
        $.ajax({
            url: "<?php echo base_url('api/app/katalog-kategori') ?>",
            type: "GET"
        }).done(function (result) {
            var data = jQuery.parseJSON(JSON.stringify(result));
            if (data.success) {
                $('#modal-add select').css('width', '100%');
                $idkategori.select2({
                    dropdownParent: $('#modal-add')
                });
                $.each(data.data, function () {
                    $idkategori.append($("<option/>").val(this.idkategori).text(this.kategori));
                });
            } else {
                sweetAlert("gagal", data.message, "error");
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", status, "error");
        });
    }

    function set_select_subskpd() {
        var api_url = '<?php echo base_url('/api/user/permissions/' . $this->auth->get_user("idapp_user"))?>';
        $.ajax({
            url: api_url,
            type: "GET"
        }).done(function (result) {
            if (!result.success) {
                sweetAlert("Gagal", 'Kesalahan ketika mengambil data Sub SKPD', "error");
            }
            var data = result.data;
            for (var i = 0; i < data.length; i++) {
                $select_subskpd.append($("<option />").val(data[i].idsubmodule).text(data[i].submodule));
            }
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Kesalahan ketika mengambil data Sub SKPD', "error");
        });
    }

    function set_select_param(param_list) {
        $select_param.empty();
        if (param_list) {
            param_list.forEach(function (item) {
                if (item.column) {
                    $select_param.append($("<option data-col=\"" + item.column + "\"/>").val(item.value).text(item.text));
                } else {
                    $select_param.append($("<option/>").val(item.value).text(item.text));
                }
            });
        }
    }

    function set_select_based(based_list) {
        $select_based.empty();
        $select_based.append($("<option/>").val('tahun').text('Tahun'));
        $select_based.append($("<option/>").val('bulan').text('Bulan'));
        if (based_list) {
            based_list.forEach(function (item) {
                $select_based.append($("<option/>").val(item.value).text(item.text));
            })
        }
        $(".select2").width('100%');
    }

    function onchange_select_based() {
        $select_param_wrapper.slideUp('fast');
        $checkbox_year_all.prop('checked', true);
        $checkbox_month_all.prop('checked', true);
        onchange_checkbox_year_all();
        $input_year_wrapper.slideDown('fast');
        if ($select_based.val() == 'bulan') {
            $checkbox_year_all.prop('checked', false);
            $checkbox_year_all.prop('disabled', true);
            onchange_checkbox_year_all();
            $select_month_wrapper.slideDown('fast');
        } else {
            $checkbox_year_all.prop('disabled', false);
            $select_month_wrapper.slideUp('fast');
        }
        set_select_param(add_param);
        $select_param_wrapper.slideDown('fast');
    }

    function onchange_checkbox_year_all() {
        if ($checkbox_year_all.prop('checked')) {
            $input_year.prop('disabled', true);
            $input_year.val(0);
        } else {
            $input_year.val(2017);
            $input_year.prop('disabled', false);
        }
    }

    function onchange_select_subskpd() {
        no_lain = false;
        $select_based_wrapper.slideUp('fast');
        $input_year_wrapper.slideUp('fast');
        $select_month_wrapper.slideUp('fast');
        $select_param_wrapper.slideUp('fast');

        var add_based = null;
        add_param = [];

        /*
         *Filted SUBSKPD
         */
        switch ($select_subskpd.val()) {
                /*
                 *Filter DINSOS
                 *  1	Gelandangan Psikotik
                 *  2	KIS PBI-D
                 *  3	KIS PBI-N
                 *  4	KKS
                 *  5	PKH
                 *  6	PPS
                 *  7	Razia
                 *  8	POS RESOS
                 */
            case '1':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'}
                ];
                break;
            case '2':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'}
                ];
                break;
            case '3':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'status_lama', 'text': 'Status Lama'},
                    {'value': 'status_sekarang', 'text': 'Status Sekarang'},
                    {'value': 'status_keterangan', 'text': 'Status Keterangan'}
                ];
                break;
            case '4':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'is_pkh', 'text': 'Status PKH'},
                    {'value': 'is_pbdt', 'text': 'Status PBDT'},
                    {'value': 'is_batal_kks', 'text': 'Status Batal KKS'},
                    {'value': 'is_pengajuan_pkh', 'text': 'Status Pengajuan PKH'}
                ];
                break;
            case '5':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'idkategoripkh', 'text': 'Kategori'}
                ];
                break;
            case '6':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'idhubungankeluarga', 'text': 'Hubungan Keluarga'},
                    {'value': 'idjeniskelamin', 'text': 'Jenis Kelamin'},
                    {'value': 'idstatuskawin', 'text': 'Status Kawin'},
                    {'value': 'idpendidikan', 'text': 'Pendidikan'},
                    {'value': 'idstatuskeluarga', 'text': 'Status Keluarga'},
                    {'value': 'idstatusindividu', 'text': 'Status Individu'}
                ];
                break;
            case '7':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'iskota', 'text': 'Status Kota'},
                    {'value': 'idkategorirazia', 'text': 'Kategori'},
                    {'value': 'idjeniskelamin', 'text': 'Jenis Kelamin'}
                ];
                break;
            case '8':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'idkategoriresos', 'text': 'Kategori'}
                ];
                break;
                /*
                 * Akhir Filter DINSOS
                 */

                /*
                 * Filter DP3P2KB
                 * 9 	Data PKK
                 * 10	Data Posyandu
                 * 11	Hasil Pendapatan Keluarga
                 * 12	Hasil Pendapatan Keluarga Tahu
                 * 13	KDRT
                 */
            case '9':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (PKK RW)', 'column': 'jumlahkelompok_pkkrw'},
                    {'value': 'all', 'text': 'Keseluruhan (PKK RT)', 'column': 'jumlahkelompok_pkkrt'},
                    {'value': 'all', 'text': 'Keseluruhan (Dasawisma)', 'column': 'jumlahkelompok_dasawisma'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (PKK RW)', 'column': 'jumlahkelompok_pkkrw'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (PKK RT)', 'column': 'jumlahkelompok_pkkrt'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Dasawisma)', 'column': 'jumlahkelompok_dasawisma'}
                ];
                break;
            case '10':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan', 'column': 'jumlahposyandu'},
                    {'value': 'kelurahan', 'text': 'Kelurahan', 'column': 'jumlahposyandu'},
                    {'value': 'kecamatan', 'text': 'Kecamatan', 'column': 'jumlahposyandu'}
                ];
                break;
            case '11':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (Desa Kelurahan Ada)', 'column': 'desa_kelurahan_ada'},
                    {'value': 'all', 'text': 'Keseluruhan (Desa Kelurahan Didata)', 'column': 'desa_kelurahan_didata'},
                    {'value': 'all', 'text': 'Keseluruhan (Dusun RW Ada)', 'column': 'dusun_rw_ada'},
                    {'value': 'all', 'text': 'Keseluruhan (Dusun RW Didata)', 'column': 'dusun_rw_didata'},
                    {'value': 'all', 'text': 'Keseluruhan (RT Ada)', 'column': 'rt_ada'},
                    {'value': 'all', 'text': 'Keseluruhan (RT Didata)', 'column': 'rt_didata'},
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Kepala Keluarga Didata)',
                        'column': 'jumlahkepalakeluarga_didata'
                    },
                    {'value': 'all', 'text': 'Keseluruhan (Bukan Peserta KB Hamil)', 'column': 'bukanpesertakb_hamil'},
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Bukan Peserta KB Ingin Segera)',
                        'column': 'bukanpesertakb_inginanaksegera'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Bukan Peserta Ingin Anak Ditunda)',
                        'column': 'bukanpesertakb_inginanakditunda'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Bukan Peserta KB Tidak Ingin Anak Lagi)',
                        'column': 'bukanpesertakb_tidakinginanaklagi'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Kesertaan Dalam POKTAN BKP)',
                        'column': 'kesertaandalampoktan_bkp'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Kesertaan Dalam POKTAN BKR)',
                        'column': 'kesertaandalampoktan_bkr'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Kesertaan Dalam POKTAN BKL)',
                        'column': 'kesertaandalampoktan_bkl'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Kesertaan Dalam POKTAN LPPKS)',
                        'column': 'kesertaandalampoktan_lppks'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Kesertaan Dalam POKTAN PIK RM)',
                        'column': 'kesertaandalampoktan_pik_rm'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Tahapan Keluarga Sejahtera - Prasejahtera)',
                        'column': 'tahapankeluargasejahtera_prasejahtera'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Tahapan Keluarga Sejahtera - Sejahtera 1)',
                        'column': 'tahapankeluargasejahtera_sejahtera1'
                    },
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Tahapan Keluarga Sejahtera - Sejahtera 2)',
                        'column': 'tahapankeluargasejahtera_sejahtera2'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Desa Kelurahan Ada)',
                        'column': 'desa_kelurahan_ada'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Desa Kelurahan Didata)',
                        'column': 'desa_kelurahan_didata'
                    },
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Dusun RW Ada)', 'column': 'dusun_rw_ada'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Dusun RW Didata)', 'column': 'dusun_rw_didata'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (RT Ada)', 'column': 'rt_ada'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (RT Didata)', 'column': 'rt_didata'},
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Kepala Keluarga Didata)',
                        'column': 'jumlahkepalakeluarga_didata'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Bukan Peserta KB Hamil)',
                        'column': 'bukanpesertakb_hamil'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Bukan Peserta KB Ingin Segera)',
                        'column': 'bukanpesertakb_inginanaksegera'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Bukan Peserta KB Ingin Ditunda)',
                        'column': 'bukanpesertakb_inginanakditunda'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Bukan Peserta KB Tidak Ingin Anak Lagi)',
                        'column': 'bukanpesertakb_tidakinginanaklagi'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Kesertaan Dalam POKTAN BKP)',
                        'column': 'kesertaandalampoktan_bkp'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Kesertaan Dalam POKTAN BKR)',
                        'column': 'kesertaandalampoktan_bkr'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Kesertaan Dalam POKTAN BKL)',
                        'column': 'kesertaandalampoktan_bkl'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Kesertaan Dalam Poktan LPPKS)',
                        'column': 'kesertaandalampoktan_lppks'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Kesertaan Dalam POKTAN PIK RM)',
                        'column': 'kesertaandalampoktan_pik_rm'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Tahapan Keluarga Sejahtera - Prasejahtera)',
                        'column': 'tahapankeluargasejahtera_prasejahtera'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Tahapan Keluarga Sejahtera - Sejahtera 1)',
                        'column': 'tahapankeluargasejahtera_sejahtera1'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Tahapan Keluarga Sejahtera - Sejahtera 2)',
                        'column': 'tahapankeluargasejahtera_sejahtera2'
                    }
                ];
                break;
            case '12':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (Keluarga)', 'column': 'jumlahkeluarga'},
                    {'value': 'all', 'text': 'Keseluruhan (Anggota Balita)', 'column': 'jumlahanggotakeluarga_balita'},
                    {'value': 'all', 'text': 'Keseluruhan (Anggota Anak)', 'column': 'jumlahanggotakeluarga_anak'},
                    {'value': 'all', 'text': 'Keseluruhan (Anggota Remaja)', 'column': 'jumlahanggotakeluarga_remaja'},
                    {'value': 'all', 'text': 'Keseluruhan (Anggota Dewasa)', 'column': 'jumlahanggotakeluarga_dewasa'},
                    {'value': 'all', 'text': 'Keseluruhan (Anggota Lansia)', 'column': 'jumlahanggotakeluarga_lansia'},
                    {'value': 'all', 'text': 'Keseluruhan (Status PUS)', 'column': 'status_pus'},
                    {'value': 'all', 'text': 'Keseluruhan (Status PUSMUPAR)', 'column': 'statuspusmupar'},
                    {'value': 'all', 'text': 'Keseluruhan (Status PUS)', 'column': 'pasangansubur_mow'},
                    {'value': 'all', 'text': 'Keseluruhan (Pasangan Subur MOP)', 'column': 'pasangansubur_mop'},
                    {'value': 'all', 'text': 'Keseluruhan (Pasangan Subur IUD)', 'column': 'pasangansubur_iud'},
                    {'value': 'all', 'text': 'Keseluruhan (Pasangan Subur Implan)', 'column': 'pasangansubur_implan'},
                    {'value': 'all', 'text': 'Keseluruhan (Pasangan Subur Suntik)', 'column': 'pasangansubur_suntik'},
                    {'value': 'all', 'text': 'Keseluruhan (Pasangan Subur Pil)', 'column': 'pasangansubur_pil'},
                    {'value': 'all', 'text': 'Keseluruhan (Pasangan Subur Kondom)', 'column': 'pasangansubur_kondom'},
                    {
                        'value': 'all',
                        'text': 'Keseluruhan (Pasangan Subur Tradisional)',
                        'column': 'pasangansubur_tradisional'
                    },
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Keluarga)', 'column': 'jumlahkeluarga'},
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Anggota Balita)',
                        'column': 'jumlahanggotakeluarga_balita'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Anggota Anak)',
                        'column': 'jumlahanggotakeluarga_anak'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Anggota Remaja)',
                        'column': 'jumlahanggotakeluarga_remaja'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Anggota Dewasa)',
                        'column': 'jumlahanggotakeluarga_dewasa'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Anggota Lansia)',
                        'column': 'jumlahanggotakeluarga_lansia'
                    },
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Status PUS)', 'column': 'status_pus'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Status PUSMUPAR)', 'column': 'statuspusmupar'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Status PUS)', 'column': 'pasangansubur_mow'},
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Pasangan Subur MOP)',
                        'column': 'pasangansubur_mop'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Pasangan Subur IUD)',
                        'column': 'pasangansubur_iud'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Pasangan Subur Implan)',
                        'column': 'pasangansubur_implan'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Pasangan Subur Suntik)',
                        'column': 'pasangansubur_suntik'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Pasangan Subur Pil)',
                        'column': 'pasangansubur_pil'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Pasangan Subur Kondom)',
                        'column': 'pasangansubur_kondom'
                    },
                    {
                        'value': 'kecamatan-only',
                        'text': 'Kecamatan (Pasangan Subur Tradisional)',
                        'column': 'pasangansubur_tradisional'
                    }
                ];
                break;
            case '13':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan', 'column': 'jumlah_korban'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan', 'column': 'jumlah_korban'},
                    {'value': 'idjeniskasus', 'text': 'Jenis Kasus', 'column': 'jumlah_korban'},
                    {'value': 'idjeniskorban', 'text': 'Jenis Korban', 'column': 'jumlah_korban'}
                ];
                break;

                /*
                 * Filter PERPUS
                 * 14 	Rekapitulasi Koleksi
                 * 15	Statistik Peminjaman
                 */
            case '14':
                no_lain = true;
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'idbahasa', 'text': 'Bahasa'},
                    {'value': 'idjeniskoleksi', 'text': 'Jenis Koleksi'},
                    {'value': 'idkelas', 'text': 'Kelas'}
                ];
                break;
            case '15':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'idjeniskoleksi', 'text': 'Jenis Koleksi'},
                    {'value': 'idkelas', 'text': 'Kelas'}
                ];
                break;

                /*
                 * Filter Dispenduk
                 * 16	Akta Catatan Sipil
                 * 17	Orang Asing
                 * 18	Penduduk
                 */
            case '16':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (Akta Kelahiran Umum)', 'column': 'aktakelahiran_umum'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Kelahiran Terlambat)', 'column': 'aktakelahiran_terlambat'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Perkawinan Bulan Lalu)', 'column': 'aktaperkawinan_bulanyanglalu'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Perkawinan Bulan Ini)', 'column': 'aktaperkawinan_bulanini'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Perceraian Bulan Lalu)', 'column': 'aktaperceraian_bulanyanglalu'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Perceraian Bulan Ini)', 'column': 'aktaperceraian_bulanini'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Kematian Bulan Lalu)', 'column': 'aktakematian_bulanyanglalu'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Kematian Bulan Ini)', 'column': 'aktakematian_bulanini'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Pengangkatan Bulan Lalu)', 'column': 'aktapengangkatan_bulanyanglalu'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Pengangkatan Bulan Ini)', 'column': 'aktapengangkatan_bulanini'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Pengakuan Bulan Lalu)', 'column': 'aktapengakuan_bulanyanglalu'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Pengakuan Bulan Ini)', 'column': 'aktapengakuan_bulanini'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Pengesahan Bulan Lalu)', 'column': 'aktapengesahan_bulanyanglalu'},
                    {'value': 'all', 'text': 'Keseluruhan (Akta Pengesahan Bulan Ini)', 'column': 'aktapengesahan_bulanini'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Kelahiran Umum)', 'column': 'aktakelahiran_umum'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Kelahiran Terlambat)', 'column': 'aktakelahiran_terlambat'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Perkawinan Bulan Lalu)', 'column': 'aktaperkawinan_bulanyanglalu'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Perkawinan Bulan Ini)', 'column': 'aktaperkawinan_bulanini'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Perceraian Bulan Lalu)', 'column': 'aktaperceraian_bulanyanglalu'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Perceraian Bulan Ini)', 'column': 'aktaperceraian_bulanini'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Kematian Bulan Lalu)', 'column': 'aktakematian_bulanyanglalu'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Kematian Bulan Ini)', 'column': 'aktakematian_bulanini'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Pengangkatan Bulan Lalu)', 'column': 'aktapengangkatan_bulanyanglalu'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Pengangkatan Bulan Ini)', 'column': 'aktapengangkatan_bulanini'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Pengakuan Bulan Lalu)', 'column': 'aktapengakuan_bulanyanglalu'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Pengakuan Bulan Ini)', 'column': 'aktapengakuan_bulanini'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Pengesahan Bulan Lalu)', 'column': 'aktapengesahan_bulanyanglalu'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Akta Pengesahan Bulan Ini)', 'column': 'aktapengesahan_bulanini'}
                ];
                break;
            case '17':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (Awal Bulan Ini (P))', 'column': 'pendudukawalbulanini_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Awal Bulan Ini (L))', 'column': 'pendudukawalbulanini_l'},
                    {'value': 'all', 'text': 'Keseluruhan (Lahir BUlan Ini (P))', 'column': 'lahirbulanini_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Lahir BUlan Ini (L))', 'column': 'lahirbulanini_l'},
                    {'value': 'all', 'text': 'Keseluruhan (Mati Bulan Ini (P))', 'column': 'matibulanini_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Mati Bulan Ini (L))', 'column': 'matibulanini_l'},
                    {'value': 'all', 'text': 'Keseluruhan (Pendatang Bulan Ini (P))', 'column': 'pendatangbulanini_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Pendatang Bulan Ini (L))', 'column': 'pendatangbulanini_l'},
                    {'value': 'all', 'text': 'Keseluruhan (Pindah Bulan Ini (P))', 'column': 'pindahbulanini_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Pindah Bulan Ini (L))', 'column': 'pindahbulanini_l'},
                    {'value': 'all', 'text': 'Keseluruhan (Penduduk Akhir Bulan Ini (P))', 'column': 'pendudukakhirbulanini_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Penduduk Akhir Bulan Ini (L))', 'column': 'pendudukakhirbulanini_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Awal Bulan Ini (P))', 'column': 'pendudukawalbulanini_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Awal Bulan Ini (L))', 'column': 'pendudukawalbulanini_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Lahir BUlan Ini (P))', 'column': 'lahirbulanini_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Lahir BUlan Ini (L))', 'column': 'lahirbulanini_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Mati Bulan Ini (P))', 'column': 'matibulanini_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Mati Bulan Ini (L))', 'column': 'matibulanini_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Pendatang Bulan Ini (P))', 'column': 'pendatangbulanini_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Pendatang Bulan Ini (L))', 'column': 'pendatangbulanini_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Pindah Bulan Ini (P))', 'column': 'pindahbulanini_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Pindah Bulan Ini (L))', 'column': 'pindahbulanini_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Penduduk Akhir Bulan Ini (P))', 'column': 'pendudukakhirbulanini_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Penduduk Akhir Bulan Ini (L))', 'column': 'pendudukakhirbulanini_l'},
                ];
                break;
            case '18':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (Jumlah KK)', 'column': 'jumlahkk'},
                    {'value': 'all', 'text': 'Keseluruhan (Penduduk (P))', 'column': 'pendudukakhirbulanini_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Penduduk (L))', 'column': 'pendudukakhirbulanini_l'},
                    {'value': 'all', 'text': 'Keseluruhan (Wajib Memiliki KTP (L))', 'column': 'wajibmemilikiktp_l'},
                    {'value': 'all', 'text': 'Keseluruhan (Wajib Memiliki KTP (P))', 'column': 'wajibmemilikiktp_p'},
                    {'value': 'all', 'text': 'Keseluruhan (Memiliki KTP)', 'column': 'memilikiktp'},
                    {'value': 'all', 'text': 'Keseluruhan (Belum Memiliki KTP)', 'column': 'belumemilikiktp'},
                    {'value': 'all', 'text': 'Keseluruhan (Wajib Memiliki Akta Kelahiran)', 'column': 'wajibmemilikiaktekelahiran'},
                    {'value': 'all', 'text': 'Keseluruhan (Memiliki Akta Kelahiran)', 'column': 'memilikiaktakelahiran'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Jumlah KK)', 'column': 'jumlahkk'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Penduduk (P))', 'column': 'pendudukakhirbulanini_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Penduduk (L))', 'column': 'pendudukakhirbulanini_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Wajib Memiliki KTP (L))', 'column': 'wajibmemilikiktp_l'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Wajib Memiliki KTP (P))', 'column': 'wajibmemilikiktp_p'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Memiliki KTP)', 'column': 'memilikiktp'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Belum Memiliki KTP)', 'column': 'belumemilikiktp'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Wajib Memiliki Akta Kelahiran)', 'column': 'wajibmemilikiaktekelahiran'},
                    {'value': 'kecamatan-only', 'text': 'Kecamatan (Memiliki Akta Kelahiran)', 'column': 'memilikiaktakelahiran'}
                ];
                break;

                /*
                 * Filter Hukum
                 * 19   Perwal
                 * 20   Perda
                 */
            case '19':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'is_asli', 'text': 'Status Asli'},
                    {'value': 'is_paraf', 'text': 'Status Paraf'},
                    {'value': 'is_salinan', 'text': 'Status Salinan'}
                ];
                break;
            case '20':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'}
                ];
                break;
                /*
                 * Filter PERKIM
                 * 21   Pertamanan
                 * 22   Pemakaman
                 * 23   Rusunawa
                 */
            case '21':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'idjenispertamanan', 'text': 'Jenis Pertamanan'}
                ];
                break;
            case '22':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (Luas TPU)', 'column': 'luastpu'},
                    {'value': 'all', 'text': 'Keseluruhan (Luas Lahan Makam)', 'column': 'luaslahanmakam'},
                    {'value': 'all', 'text': 'Keseluruhan (Jumlah Seluruh Makam)', 'column': 'jumlahseluruhmakam'},
                    {'value': 'all', 'text': 'Keseluruhan (FASUM)', 'column': 'fasum'},
                    {'value': 'all', 'text': 'Keseluruhan (Sisa Lahan Makam)', 'column': 'sisalahanmakam'}
                ];
                break;
            case '23':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan (Hunian)', 'column': 'jumlahhunian'},
                    {'value': 'all', 'text': 'Keseluruhan (Penghuni)', 'column': 'jumlahpenghuni'},
                    {'value': 'idlokasi', 'text': 'Lokasi'}
                ];
                break;

                /*
                 * Filter BKD
                 * 24   Rekapitulasi PNS
                 */
            case '24':
                no_lain = true;
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'idpendidikan', 'text': 'Pendidikan'},
                    {'value': 'idstatuskawin', 'text': 'Status Kawin'},
                    {'value': 'idunitkerja', 'text': 'Unit Kerja'},
//                    {'value': 'ideselon', 'text': 'Eselon'},
                    {'value': 'idagama', 'text': 'Agama'},
                    {'value': 'idgolongan', 'text': 'Golongan'}
                ];
                break;

                /*
                 * Filter BP2D
                 * 25   Statistik Sektoral
                 */
            case '25':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'}
                ];
                break;

                /*
                 * Filter DINKES
                 * 26   Data Bumil
                 * 27   Data Gizi Buruk
                 * 28   Data TB
                 */
            case '26':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'idrencana_persalinan_biaya', 'text': 'Rencana Persalinan Biaya'},
                    {'value': 'idrencana_persalinan_kendaraan', 'text': 'Rencana Persalinan Kendara'},
                    {'value': 'idketerangan_bpjs', 'text': 'BPJS'}
                ];
                break;
            case '27':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'kecamatan', 'text': 'Kecamatan'},
                    {'value': 'idpuskesmas', 'text': 'Puskesmas'}
                ];
                break;
            case '28':
                add_param = [
                    {'value': 'all', 'text': 'Keseluruhan'},
                    {'value': 'kelurahan', 'text': 'Kelurahan'},
                    {'value': 'idtipe_pasien', 'text': 'Tipe Pasien'},
                    {'value': 'idklasifikasi_penyakit', 'text': 'Klasifikasi Penyakit'},
                    {'value': 'idkode_paduan_rejimen_yang_diberikan', 'text': 'Paduan Rejimen'},
                    {'value': 'idjenis_fayankes', 'text': 'Jenis Fayankes'},
                    {'value': 'idfayankes', 'text': 'Fayankes'},
                    {'value': 'idvalidasi_data', 'text': 'Validasi Data'},
                    {'value': 'idjeniskelamin', 'text': 'Jenis Kelamin'}
                ];
                break;

        }
        set_select_based(add_based);
        $select_based_wrapper.slideDown('fast');
        onchange_select_based();
    }

    function generate_url() {
        var idsubmodule = $select_subskpd.val();

        var selectlist = {}; //selectlist[table]:col
        var grouplist = []; //one dimension
        var countlist = []; //one dimension
        var joinlist = [];  //joinlist[0][dest_table]:kelurahan
        var wherelist = []; //wherelist[0][tanggal][startdate]:2016-01-01
        var url = '/'+idsubmodule+'/?';

        var data_col = $select_param.select2().find(":selected").data("col");

        /*
         *Filter BASED
         */
        if ($select_based.val() == 'tahun') {
            selectlist['skip'] = 'year(tanggal) as tahun';
            if ($input_year.val() != 0) {
                wherelist.push({
                    'tanggal': {
                        'startdate': $input_year.val() + '-01-01',
                        'enddate': $input_year.val() + '-12-31'
                    }
                });
            }
            grouplist.push('year(tanggal)');
        } else {
            selectlist['skip'] = 'month(tanggal) as bulan';
            var month = $select_month.val() - 1;
            var last_date = new Date($input_year.val(), month + 1, 0);
            var last_date = '31';

            if ($select_month.val() == 0) {
                wherelist.push({
                    'tanggal': {
                        'startdate': $input_year.val() + '-01-01',
                        'enddate': $input_year.val() + '-12-31'
                    }
                });
            } else {
                wherelist.push({
                    'tanggal': {
                        'startdate': $input_year.val() + '-' + $select_month.val() + '-01',
                        'enddate': $input_year.val() + '-' + $select_month.val() + '-' + last_date
                    }
                });
            }
            grouplist.push('month(tanggal)');
        }

        /*
         *Filter PARAM
         */
        switch ($select_param.val()) {
                /*
                 *General Filter
                 */
            case 'all':
                if (data_col) {
                    countlist.push(data_col);
                }
                break;
            case 'kelurahan':
                if (data_col) {
                    countlist.push(data_col);
                }
                selectlist['kelurahan'] = 'kelurahan as kategori';
                joinlist.push({'dest_table': 'kelurahan', 'dest_column': 'idkelurahan'});
                grouplist.push('kelurahan');
                break;
            case 'kecamatan':
                if (data_col) {
                    countlist.push(data_col);
                }
                selectlist['kecamatan'] = 'kecamatan as kategori';
                joinlist.push({'dest_table': 'kelurahan', 'dest_column': 'idkelurahan'});
                joinlist.push({'dest_table': 'kecamatan', 'dest_column': 'idkecamatan', 'src_table': 'kelurahan'});
                grouplist.push('kecamatan');
                break;
            case 'kecamatan-only':
                if (data_col) {
                    countlist.push(data_col);
                }
                selectlist['kecamatan'] = 'kecamatan as kategori';
                joinlist.push({'dest_table': 'kecamatan', 'dest_column': 'idkecamatan'});
                grouplist.push('kecamatan');
                break;
            case 'idjeniskelamin':
                if (data_col) {
                    countlist.push(data_col);
                }
                selectlist['jeniskelamin'] = 'jeniskelamin as kategori';
                joinlist.push({'dest_table': 'jeniskelamin', 'dest_column': 'idjeniskelamin'});
                grouplist.push('jeniskelamin');
                break;
            case 'idhubungankeluarga':
                selectlist['hubungankeluarga'] = 'hubungankeluarga as kategori';
                joinlist.push({
                    'dest_table': 'hubungankeluarga',
                    'dest_column': 'idhubungankeluarga'
                });
                grouplist.push('hubungankeluarga');
                break;
            case 'idstatuskawin':
                selectlist['statuskawin'] = 'status as kategori';
                joinlist.push({
                    'dest_table': 'statuskawin',
                    'dest_column': 'idstatuskawin'
                });
                grouplist.push('status');
                break;
            case 'idpendidikan':
                selectlist['pendidikan'] = 'pendidikan as kategori';
                joinlist.push({
                    'dest_table': 'pendidikan',
                    'dest_column': 'idpendidikan'
                });
                grouplist.push('pendidikan');
                break;
            case 'idstatuskeluarga':
                selectlist['statuskeluarga'] = 'statuskeluarga as kategori';
                joinlist.push({
                    'dest_table': 'statuskeluarga',
                    'dest_column': 'idstatuskeluarga'
                });
                grouplist.push('statuskeluarga');
                break;
            case 'idstatusindividu':
                selectlist['statusindividu'] = 'statusindividu as kategori';
                joinlist.push({
                    'dest_table': 'statusindividu',
                    'dest_column': 'idstatusindividu'
                });
                grouplist.push('statusindividu');
                break;

                /*
                 *spesific DINSOS KISPBIN Filter
                 */
            case 'status_lama':
                selectlist['statuskis'] = 'status as kategori';
                joinlist.push({'dest_table': 'statuskis', 'dest_column': 'idstatuskis', 'src_column': 'status_lama'});
                grouplist.push('status');
                break;
            case 'status_sekarang':
                selectlist['statuskis'] = 'status as kategori';
                joinlist.push({
                    'dest_table': 'statuskis',
                    'dest_column': 'idstatuskis',
                    'src_column': 'status_sekarang'
                });
                grouplist.push('status');
                break;
            case 'status_keterangan':
                selectlist['statuskis'] = 'status as kategori';
                joinlist.push({
                    'dest_table': 'statuskis',
                    'dest_column': 'idstatuskis',
                    'src_column': 'status_keterangan'
                });
                grouplist.push('status');
                break;

                /*
                 *spesific DINSOS KKS Filter
                 */
            case 'is_pkh':
                selectlist['this'] = 'is_pkh as kategori';
                grouplist.push('is_pkh');
                break;
            case 'is_pbdt':
                selectlist['this'] = 'is_pbdt as kategori';
                grouplist.push('is_pbdt');
                break;
            case 'is_batal_kks':
                selectlist['this'] = 'is_batal_kks as kategori';
                grouplist.push('is_batal_kks');
                break;
            case 'is_pengajuan_pkh':
                selectlist['this'] = 'is_pengajuan_pkh as kategori';
                grouplist.push('is_pengajuan_pkh');
                break;

                /*
                 *spesific DINSOS PKH Filter
                 */
            case 'idkategoripkh':
                selectlist['dinsos_kategoripkh'] = 'kategori as kategori';
                joinlist.push({
                    'dest_table': 'dinsos_kategoripkh',
                    'dest_column': 'idkategori'
                });
                grouplist.push('kategori');
                break;

                /*
                 *spesific DINSOS Razia Filter
                 */
            case 'idkategorirazia':
                selectlist['dinsos_kategorirazia'] = 'razia as kategori';
                joinlist.push({
                    'dest_table': 'dinsos_kategorirazia',
                    'dest_column': 'idkategorirazia'
                });
                grouplist.push('kategori');
                break;
            case 'iskota':
                selectlist['this'] = 'iskota as kategori';
                grouplist.push('iskota');
                break;

                /*
                 *spesific DINSOS RESOS Filter
                 */
            case 'idkategoriresos':
                selectlist['dinsos_kategoriresos'] = 'kategori as kategori';
                joinlist.push({
                    'dest_table': 'dinsos_kategoriresos',
                    'dest_column': 'idkategoriresos'
                });
                grouplist.push('kategori');
                break;

                /*
                 *spesific DP3P2KB KDRT Filter
                 */
            case 'idjeniskasus':
                selectlist['dp3p2kb_jeniskasus'] = 'jeniskasus as kategori';
                joinlist.push({
                    'dest_table': 'dp3p2kb_jeniskasus',
                    'dest_column': 'idjeniskasus'
                });
                countlist.push('jumlah_korban');
                grouplist.push('kategori');
                break;
            case 'idjeniskorban':
                selectlist['dp3p2kb_jeniskorban'] = 'jeniskorban as kategori';
                joinlist.push({
                    'dest_table': 'dp3p2kb_jeniskorban',
                    'dest_column': 'idjeniskorban'
                });
                countlist.push('jumlah_korban');
                grouplist.push('kategori');
                break;

                /*
                *spesific PERPUS
                */
            case 'idjeniskoleksi':
                selectlist['perpus_jeniskoleksi'] = 'jeniskoleksi as kategori';
                joinlist.push({
                    'dest_table': 'perpus_jeniskoleksi',
                    'dest_column': 'idjeniskoleksi'
                });
                grouplist.push('kategori');
                break;
            case 'idkelas':
                selectlist['perpus_kelas'] = 'kelas as kategori';
                joinlist.push({
                    'dest_table': 'perpus_kelas',
                    'dest_column': 'idkelas'
                });
                grouplist.push('kategori');
                break;
            case 'idbahasa':
                selectlist['perpus_bahasa'] = 'bahasa as kategori';
                joinlist.push({
                    'dest_table': 'perpus_bahasa',
                    'dest_column': 'idbahasa'
                });
                grouplist.push('kategori');
                break;

                /*
                 *spesific Hukum
                 */
            case 'is_asli':
                selectlist['this'] = 'is_asli as kategori';
                grouplist.push('is_asli');
                break;
            case 'is_paraf':
                selectlist['this'] = 'is_paraf as kategori';
                grouplist.push('is_paraf');
                break;
            case 'is_salinan':
                selectlist['this'] = 'is_salinan as kategori';
                grouplist.push('is_salinan');
                break;

                /*
                 *spesific PERKIM
                 */
            case 'idlokasi':
                selectlist['perum_lokasi'] = 'lokasi as kategori';
                joinlist.push({
                    'dest_table': 'perum_lokasi',
                    'dest_column': 'idlokasi'
                });
                grouplist.push('kategori');
                break;
            case 'idjenispertamanan':
                selectlist['perum_jenispertamanan'] = 'jenispertamanan as kategori';
                joinlist.push({
                    'dest_table': 'perum_jenispertamanan',
                    'dest_column': 'idjenispertamanan'
                });
                grouplist.push('kategori');
                break;

                /*
                 *spesific BKD
                 */
            case 'idunitkerja':
                selectlist['bkd_unitkerja'] = 'unitkerja as kategori';
                joinlist.push({
                    'dest_table': 'bkd_unitkerja',
                    'dest_column': 'idunitkerja'
                });
                grouplist.push('kategori');
                break;
            case 'ideselon':
                selectlist['bkd_eselon'] = 'eselon as kategori';
                joinlist.push({
                    'dest_table': 'bkd_eselon',
                    'dest_column': 'ideselon'
                });
                grouplist.push('kategori');
                break;
            case 'idagama':
                selectlist['bkd_agama'] = 'agama as kategori';
                joinlist.push({
                    'dest_table': 'bkd_agama',
                    'dest_column': 'idagama'
                });
                grouplist.push('kategori');
                break;
            case 'idgolongan':
                selectlist['bkd_golongan'] = 'golongan as kategori';
                joinlist.push({
                    'dest_table': 'bkd_golongan',
                    'dest_column': 'idgolongan'
                });
                grouplist.push('kategori');
                break;

                /*
                 *spesific DINKES
                 */
            case 'idrencana_persalinan_biaya':
                selectlist['dinkes_rencana_persalinan_biaya'] = 'rencana_persalinan_biaya as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_rencana_persalinan_biaya',
                    'dest_column': 'idrencana_persalinan_biaya'
                });
                grouplist.push('kategori');
                break;
            case 'idrencana_persalinan_kendaraan':
                selectlist['dinkes_rencana_persalinan_kendaraan'] = 'rencana_persalinan_kendaraan as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_rencana_persalinan_kendaraan',
                    'dest_column': 'idrencana_persalinan_kendaraan'
                });
                grouplist.push('kategori');
                break;
            case 'idketerangan_bpjs':
                selectlist['dinkes_keterangan_bpjs'] = 'keterangan_bpjs as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_keterangan_bpjs',
                    'dest_column': 'idketerangan_bpjs'
                });
                grouplist.push('kategori');
                break;
            case 'idpuskesmas':
                selectlist['dinkes_puskesmas'] = 'puskesmas as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_puskesmas',
                    'dest_column': 'idpuskesmas'
                });
                grouplist.push('kategori');
                break;
            case 'idtipe_pasien':
                selectlist['dinkes_tipe_pasien'] = 'tipe_pasien as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_tipe_pasien',
                    'dest_column': 'idtipe_pasien'
                });
                grouplist.push('kategori');
                break;
            case 'idklasifikasi_penyakit':
                selectlist['dinkes_klasifikasi_penyakit'] = 'klasifikasi_penyakit as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_klasifikasi_penyakit',
                    'dest_column': 'idklasifikasi_penyakit'
                });
                grouplist.push('kategori');
                break;
            case 'idkode_paduan_rejimen_yang_diberikan':
                selectlist['dinkes_kode_paduan_rejimen_yang_diberikan'] = 'kode_paduan_rejimen_yang_diberikan as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_kode_paduan_rejimen_yang_diberikan',
                    'dest_column': 'idkode_paduan_rejimen_yang_diberikan'
                });
                grouplist.push('kategori');
                break;
            case 'idjenis_fayankes':
                selectlist['dinkes_jenis_fayankes'] = 'jenis_fayankes as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_jenis_fayankes',
                    'dest_column': 'idjenis_fayankes'
                });
                grouplist.push('kategori');
                break;
            case 'idfayankes':
                selectlist['dinkes_fayankes'] = 'fayankes as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_fayankes',
                    'dest_column': 'idfayankes'
                });
                grouplist.push('kategori');
                break;
            case 'idvalidasi_data':
                selectlist['dinkes_validasi_data'] = 'validasi_data as kategori';
                joinlist.push({
                    'dest_table': 'dinkes_validasi_data',
                    'dest_column': 'idvalidasi_data'
                });
                grouplist.push('kategori');
                break;
            default:
                sweetAlert("Pilih Parameter", 'Mohon Pilih Parameter yang Tersedia', "error");
        }
        var no_lain_val = 0;
        if(no_lain) no_lain_val = 1;
        var url_params = {
            'selectlist': selectlist,
            'grouplist': grouplist,
            'countlist': countlist,
            'joinlist': joinlist,
            'wherelist': wherelist,
            'no_lain': no_lain_val
        };
        return url += $.param(url_params);
    }

    function submit_condition() {
        final_url = generate_url();

        /*
         *Check input validity
         */
        if (!$select_subskpd.val()) {
            sweetAlert("Pilih SubSKPD", 'Pilih Salah satu subskpd yang tersedia', "warning");
            return;
        } else if (!$select_based.val()) {
            sweetAlert("Pilih Dasar", 'Pilih Salah satu dasar grafik yang tersedia', "warning");
            return;
        } else if (!$select_param.val()) {
            sweetAlert("Pilih Dasar", 'Pilih Salah satu parameter grafik yang tersedia', "warning");
            return;
        }
        if ($select_based.val() == 'bulan' && !$input_year.val()) {
            sweetAlert("Pilih Tahun", 'Pilih Salah tahun grafik', "warning");
            return;
        }

        $.ajax({
            url: '<?php echo base_url() ?>/map/data/'+final_url,
            type: "GET"
        }).done(function (data) {
            if (data.data.length <= 0) {
                sweetAlert("Data Tidak Tersedia", 'Data grafik berdasarkan parameter yang anda masukkan tidak tersedia', "error");
                set_url_display();
                return;
            }
            $('html, body').animate({
                scrollTop: $("#chart-preview").offset().top
            }, 500);
            set_url_display(final_url);
            create_chart(data.data);
        }).fail(function (xhr, status, errorThrown) {
            set_url_display();
            sweetAlert("Gagal", 'Terjadi kesalahan pada URL', "error");
        })
    }

    function create_chart(data) {
        clear_chart();
        var master_bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

        /*
         *Process Data
         */
        {
            var based_list = [];
            var category_list = [];
            var color_list = [];
            var x_label = '';
            var final_datasets = [];

            /*
             *Create label for axis
             */
            if ($select_based.val() == 'tahun') {
                x_label = 'Tahun';
            } else {
                x_label = 'Bulan (' + $input_year.val() + ')';
            }

            /*
             *Find All based and category
             */
            data.forEach(function (item) {
                var rgb = get_random_rgb();
                if ($select_based.val() == 'tahun') {
                    if (jQuery.inArray(item.tahun, based_list) == -1) {
                        based_list.push(item.tahun);
                    }
                } else {
                    if (jQuery.inArray(master_bulan[+item.bulan - 1], based_list) == -1) {
                        based_list.push(master_bulan[+item.bulan - 1]);
                    }
                }
                if (!item.kategori) {
                    if (jQuery.inArray('Total', category_list) == -1) {
                        category_list.push('Total');
                        color_list.push('rgba(' + rgb[0] + ', ' + rgb[1] + ', ' + rgb[2] + ', 0.8)');
                    }
                } else {
                    if (jQuery.inArray(item.kategori, category_list) == -1) {
                        if(no_lain && (item.kategori.toLowerCase() == 'lain-lain' || item.kategori.toLowerCase() == '-')){
                            return;
                        }
                        category_list.push(item.kategori);
                        color_list.push('rgba(' + rgb[0] + ', ' + rgb[1] + ', ' + rgb[2] + ', 0.8)');
                    }
                }
            });

            /*
             *Create Datasets
             */
            category_list.forEach(function (category_item) {
                var dataset = {};
                if (category_list.length == 2 && jQuery.inArray('0', category_list) != -1 && jQuery.inArray('1', category_list) != -1) {
                    if (category_item == '1') dataset['label'] = 'Ya';
                    if (category_item == '0') dataset['label'] = 'Tidak';
                } else {
                    dataset['label'] = category_item;
                }
                var rgb = get_random_rgb();
                dataset['backgroundColor'] = 'rgba(' + rgb[0] + ', ' + rgb[1] + ', ' + rgb[2] + ', 0.8)';
                dataset['borderWidth'] = 1;
                dataset['data'] = [];
                based_list.forEach(function (based_item) {
                    var found = false;
                    data.forEach(function (data_item) {
                        if ($select_based.val() == 'tahun') {
                            if (data_item.tahun == based_item) {
                                if (!data_item.kategori) {
                                    dataset['data'].push(data_item.jumlah);
                                    found = true;
                                } else {
                                    if (data_item.kategori == category_item) {
                                        dataset['data'].push(data_item.jumlah);
                                        found = true;
                                    }
                                }
                            }
                        } else {
                            if (master_bulan[+data_item.bulan - 1] == based_item) {
                                if (!data_item.kategori) {
                                    dataset['data'].push(data_item.jumlah);
                                    found = true;
                                } else {
                                    if (data_item.kategori == category_item) {
                                        dataset['data'].push(data_item.jumlah);
                                        found = true;
                                    }
                                }
                            }
                        }
                    });
                    if (!found) {
                        dataset['data'].push(0);
                    }
                });
                final_datasets.push(dataset);
            });
        }

        /*
         *Create table
         */
        clear_chart_table();
        var $chart_table = $('#chart-table');
        var $chart_table_body = $('#chart-table-body');
        var $chart_table_foot = $('#chart-table-foot');
        var count_based = 0;
        var grand_total = 0;
        based_list.forEach(function (based_item) {
            var count_dataset = 0;
            var total = 0;
            final_datasets.forEach(function (dataset_item) {
                if (count_dataset == 0) {
                    $chart_table_body.append(
                        '<tr id="' + count_based + '-' + count_dataset + '">' +
                        '<td rowspan="' + (+category_list.length + 1) + '">' + based_item + '</td>'
                    );
                } else {
                    $chart_table_body.append(
                        '<tr id="' + count_based + '-' + count_dataset + '">'
                    );
                }
                total += +dataset_item.data[count_based];
                grand_total += +dataset_item.data[count_based];
                if (category_list[count_dataset] == 'Total') {
                    $('#' + count_based + '-' + count_dataset).append(
                        '<td>Seluruh</td>' +
                        '<td class="text-center">' + dataset_item.data[count_based] + '</td>'
                    );
                } else {
                    $('#' + count_based + '-' + count_dataset).append(
                        '<td>' + category_list[count_dataset] + '</td>' +
                        '<td class="text-center">' + dataset_item.data[count_based] + '</td>'
                    );
                }
                count_dataset++;
            });
            $chart_table_body.append(
                '<tr class="warning">' +
                '<th class="text-center">Total</th>' +
                '<th class="text-center">' + total + '</th>' +
                '</tr>'
            )
            ;
            count_based++;
        });
        $chart_table_foot.html(
            '<tr class="success">' +
            '<th colspan="2" class="text-center">Grand Total</th>' +
            '<th class="text-center">' + grand_total + '</th>' +
            '</tr>'
        );

        /*
         *Create Chart
         */
        var ctx = document.getElementById("ncc-chart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: based_list,
                datasets: final_datasets
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                "hover": {
                    "animationDuration": 0
                },
                "animation": {
                    "duration": 1,
                    "onComplete": function () {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;

                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(dataset.label + ' : ' + data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                }, layout: {
                    padding: {
                        top: 50
                    }
                },
                legend: {
                    "display": true,
                    'position': 'top'
                },
                tooltips: {
                    "enabled": true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Jumlah'
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: x_label
                        }
                    }]
                }
            }
        });
    }

    function clear_chart() {
        var $chart = $('.chart');
        $chart.empty();
        $chart.append('<canvas id="ncc-chart"></canvas>');
        ctx = $('#ncc-chart');
    }

    function clear_chart_table() {
        var $chart_table_body = $('#chart-table-body');
        $chart_table_body.empty();
    }

    function get_random_rgb() {
        rgb = [];
        rgb.push(Math.floor(Math.random() * (254 - 1 + 1)) + 1);
        rgb.push(Math.floor(Math.random() * (254 - 1 + 1)) + 1);
        rgb.push(Math.floor(Math.random() * (254 - 1 + 1)) + 1);

        return rgb;
    }

</script>
