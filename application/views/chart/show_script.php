<script>
    $(function () {
        //loading screen
        jQuery(document).ajaxStart(function () {
            $('body').loading({
                message: 'Proses...'
            });
        });
        jQuery(document).ajaxStop(function () {
            $('body').loading('stop');
        });

        $.ajax({
            url: '<?php echo base_url('map/data/'.$katalog_url_data) ?>',
            type: "GET"
        }).done(function (data) {
            if (data.data.length <= 0) {
                sweetAlert("Data Tidak Tersedia", 'Data grafik berdasarkan parameter yang anda masukkan tidak tersedia', "error");
                return;
            }
            create_chart(data.data);
        }).fail(function (xhr, status, errorThrown) {
            sweetAlert("Gagal", 'Terjadi kesalahan pada URL', "error");
        });
    });

    function has_query(query, keyword) {
        var url_array = '<?php echo $katalog_url_data ?>'.split('&');
        for (var i = 0; i < url_array.length; i++) {
            if (url_array[i].indexOf(query) != -1 && url_array[i].indexOf(keyword) != -1) {
                return true;
            }
            if (keyword == 'tahun' && url_array[i].indexOf(query) != -1 && url_array[i].indexOf('year(tanggal)') != -1) {
                return true;
            }
        }
        return false;
    }

    function create_chart(data) {
        clear_chart();
        var master_bulan = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'];

        /*
         *Process Data
         */
        var based_list = [];
        var category_list = [];
        var color_list = [];
        var x_label = '';
        var final_datasets = [];

        /*
         *Create label for axis
         */
        if (has_query('grouplist', 'tahun')) {
            x_label = 'Tahun';
        } else {
            x_label = 'Bulan';
        }

        /*
         *Find All based and category
         */
        data.forEach(function (item) {
            var rgb = get_random_rgb();
            if (has_query('grouplist', 'tahun')) {
                if (jQuery.inArray(item.tahun, based_list) == -1) {
                    based_list.push(item.tahun);
                }
            } else {
                if (jQuery.inArray(master_bulan[+item.bulan - 1], based_list) == -1) {
                    based_list.push(master_bulan[+item.bulan - 1]);
                }
            }
            if (!item.kategori) {
                if (jQuery.inArray('Total', category_list) == -1) {
                    category_list.push('Total');
                    color_list.push('rgba(' + rgb[0] + ', ' + rgb[1] + ', ' + rgb[2] + ', 0.8)');
                }
            } else {
                if (jQuery.inArray(item.kategori, category_list) == -1) {
                    if(has_query('no_lain', '1') && (item.kategori.toLowerCase() == 'lain-lain' || item.kategori.toLowerCase() == '-')){
                        return;
                    }
                    category_list.push(item.kategori);
                    color_list.push('rgba(' + rgb[0] + ', ' + rgb[1] + ', ' + rgb[2] + ', 0.8)');
                }
            }
        });

        /*
         *Create Datasets
         */
        category_list.forEach(function (category_item) {
            var dataset = {};
            if (category_list.length == 2 && jQuery.inArray('0', category_list) != -1 && jQuery.inArray('1', category_list) != -1) {
                if (category_item == '1') dataset['label'] = 'Ya';
                if (category_item == '0') dataset['label'] = 'Tidak';
            } else {
                dataset['label'] = category_item;
            }
            var rgb = get_random_rgb();
            dataset['backgroundColor'] = 'rgba(' + rgb[0] + ', ' + rgb[1] + ', ' + rgb[2] + ', 0.8)';
            dataset['borderWidth'] = 1;
            dataset['data'] = [];
            based_list.forEach(function (based_item) {
                var found = false;
                data.forEach(function (data_item) {
                    if (has_query('grouplist', 'tahun')) {
                        if (data_item.tahun == based_item) {
                            if (!data_item.kategori) {
                                dataset['data'].push(data_item.jumlah);
                                found = true;
                            } else {
                                if (data_item.kategori == category_item) {
                                    dataset['data'].push(data_item.jumlah);
                                    found = true;
                                }
                            }
                        }
                    } else {
                        if (master_bulan[+data_item.bulan - 1] == based_item) {
                            if (!data_item.kategori) {
                                dataset['data'].push(data_item.jumlah);
                                found = true;
                            } else {
                                if (data_item.kategori == category_item) {
                                    dataset['data'].push(data_item.jumlah);
                                    found = true;
                                }
                            }
                        }
                    }
                });
                if (!found) {
                    dataset['data'].push(0);
                }
            });
            final_datasets.push(dataset);
        });

        /*
         *Create table
         */
        var $chart_table = $('#chart-table');
        var $chart_table_body = $('#chart-table-body');
        var $chart_table_foot = $('#chart-table-foot');
        var count_based = 0;
        var grand_total = 0;
        based_list.forEach(function (based_item) {
            var count_dataset = 0;
            var total = 0;
            final_datasets.forEach(function (dataset_item) {
                if (count_dataset == 0) {
                    $chart_table_body.append(
                        '<tr id="' + count_based + '-' + count_dataset + '">' +
                        '<td rowspan="' + (+category_list.length + 1) + '">' + based_item + '</td>'
                    );
                } else {
                    $chart_table_body.append(
                        '<tr id="' + count_based + '-' + count_dataset + '">'
                    );
                }
                total += +dataset_item.data[count_based];
                grand_total += +dataset_item.data[count_based];
                if (category_list[count_dataset] == 'Total') {
                    $('#' + count_based + '-' + count_dataset).append(
                        '<td>Seluruh</td>' +
                        '<td class="text-center">' + dataset_item.data[count_based] + '</td>'
                    );
                } else {
                    $('#' + count_based + '-' + count_dataset).append(
                        '<td>' + category_list[count_dataset] + '</td>' +
                        '<td class="text-center">' + dataset_item.data[count_based] + '</td>'
                    );
                }
                count_dataset++;
            });
            $chart_table_body.append(
                '<tr class="warning">' +
                '<th class="text-center">Total</th>' +
                '<th class="text-center">' + total + '</th>' +
                '</tr>'
            )
            ;
            count_based++;
        });
        $chart_table_foot.html(
            '<tr class="success">' +
            '<th colspan="2" class="text-center">Grand Total</th>' +
            '<th class="text-center">' + grand_total + '</th>' +
            '</tr>'
        );


        /*
         *Create Chart
         */
        var ctx = document.getElementById("ncc-chart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: based_list,
                datasets: final_datasets
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                "hover": {
                    "animationDuration": 0
                },
                "animation": {
                    "duration": 1,
                    "onComplete": function () {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;

                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(dataset.label + ' : ' + data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                }, layout: {
                    padding: {
                        top: 50
                    }
                },
                legend: {
                    "display": true,
                    'position': 'top'
                },
                tooltips: {
                    "enabled": true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Jumlah'
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: x_label
                        }
                    }]
                }
            }
        });
    }

    function clear_chart() {
        var $chart = $('.chart');
        $chart.empty();
        $chart.append('<canvas id="ncc-chart"></canvas>');
        ctx = $('#ncc-chart');
    }

    function get_random_rgb() {
        rgb = [];
        rgb.push(Math.floor(Math.random() * (254 - 1 + 1)) + 1);
        rgb.push(Math.floor(Math.random() * (254 - 1 + 1)) + 1);
        rgb.push(Math.floor(Math.random() * (254 - 1 + 1)) + 1);

        return rgb;
    }
</script>