<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/default.css"); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>

<body>
<img class="background-ncc-logo-full" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>" alt="NCC logo">

<div class="col-md-2">
    <h3><?php echo "Grafik Pelayanan Surat Pernyataan Miskin Elektronik (E-SPM)"; ?></h3>
    <hr>
    <p><?php echo "Informasi Tentang Grafik Pelayanan Surat Pernyataan Miskin Elektronik (E-SPM)"; ?></p>
    <table class="table table-striped table-bordered" id="chart-table">
        <thead>
        <tr>
            <th>Dasar</th>
            <th>Parameter</th>
            <th>Jumlah</th>
        </tr>
        </thead>
        <tbody id="chart-table-body">
            
        </tbody>
        <tfoot id="chart-table-foot">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <th colspan="3">List Data (Double Click Untuk Hasil Maksimal)</th>
            </tr>
            
            <tr>
                <td colspan="3">
                    <input type="radio" name="list_data" id="list-0" value="0" class="radio_list"/> &nbsp;&nbsp;&nbsp;<label for="list-0">Sebaran Rumah Sakit</label><br />
                    <input type="radio" name="list_data" id="list-1" value="1" class="radio_list"/> &nbsp;&nbsp;&nbsp;<label for="list-1">Sebaran Pasien Rujukan Penyakit SPM</label><br />
                    <input type="radio" name="list_data" id="list-2" value="2" class="radio_list"/> &nbsp;&nbsp;&nbsp;<label for="list-2">Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)</label><br />
                    <input type="radio" name="list_data" id="list-3" value="3" class="radio_list"/> &nbsp;&nbsp;&nbsp;<label for="list-3">Sebaran Pengajuan SPM(Surat Pernyataan Miskin)</label><br />
                    <input type="radio" name="list_data" id="list-4" value="4" class="radio_list"/> &nbsp;&nbsp;&nbsp;<label for="list-4">Sebaran Pengajuan SPM(Surat Pernyataan Miskin) di Terima</label><br />
                    
                </td>
            </tr>
            
        </tfoot>
    </table>
    <hr>
    <p class="small pull-right">
        <a href="<?php echo base_url('') ?>">NCC</a> |
        <a href="<?php echo base_url('katalog') ?>">Katalog</a>
    </p>
</div>
<script src="https://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
<div class="col-md-10" id="chart_opening">
    <div class="box-body">
        <div class="chart" style="height: 100%; padding: 5px 0 5px 0">
            
        </div>
    </div>
</div>
<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/datatables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/url.min.js") ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>

<script src="<?= base_url();?>assets/js/jquery-3.2.1.js"></script>
<script>
        
            
            var list_url = ["<?php echo base_url()."spm/get_sebaran_rs";?>",
                            "<?php echo base_url()."spm/get_sebaran_penyakit";?>",
                            "<?php echo base_url()."spm/get_sebaran_pbi";?>",
                            "<?php echo base_url()."spm/get_sebaran_pengajuan_SPM";?>",
                            "<?php echo base_url()."spm/get_sebaran_pengajuan_SPM_diterima";?>"];
                            
            var list_url_chart = ["<?php echo base_url()."spm/get_rs_chart";?>",
                            "<?php echo base_url()."spm/get_penyakit_chart";?>",
                            "<?php echo base_url()."spm/get_pbi_chart";?>",
                            "<?php echo base_url()."spm/get_SPM_chart";?>",
                            "<?php echo base_url()."spm/get_SPM_diterima_chart";?>"];
            
            $(".radio_list").click(function(){
                //alert(x[1]);
                //($(this).val());
                var id = $(this).val();
                //alert(id);
                var url_send = list_url[id];
                

                var dumb_chart;
                var dumb_table;
                
                var form_data = new FormData();
                    form_data.append('id', id);
                    $.ajax({
                        url: list_url_chart[id], // point to server-side PHP script 
                        dataType: 'html',  // what to expect back from the PHP script, if anything
                        cache: false,
                        async: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        type: 'post',
                        success: function(res){
                            //alert("surya");
                            // console.log(list_url_chart[id]);
                              
                            dumb_chart = res;
                              
                        },
                        complete: function(){
                            $("#chart_opening").html(dumb_chart);
                        }
                    }).done(function() {
                          
                    });

                    $.ajax({
                        url: url_send, // point to server-side PHP script 
                        dataType: 'html',  // what to expect back from the PHP script, if anything
                        cache: false,
                        async: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        type: 'post',
                        success: function(res){
                          //alert("surya");
                          // console.log(list_url[id]);
                          dumb_table = res;
                          
                        },
                        complete: function(){
                            $("#chart-table-body").html(dumb_table);
                        }
                    }).done(function() {
                          
                    });
                    
                    
                //alert(list_url[id]);
            });
            
            
            
</script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>
