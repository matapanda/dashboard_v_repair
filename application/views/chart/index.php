<html>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NCC <?php echo $title ? " | $title" : '' ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <!--  -->

    <link rel="icon" href="<?=base_url('/assets/images/favicon.ico')?>" type="image/gif">
    <link href="<?php echo base_url("/assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/font-awesome.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/AdminLTE.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/select2.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/jquery.loading.min.css"); ?>" rel="stylesheet">
    <link href="<?php echo base_url("/assets/css/sweetalert.css"); ?>" rel="stylesheet">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <link href="<?php echo base_url("assets/css/default.css"); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        table {
            font-size: 13px;
        }

        td {
            word-wrap: break-word;
            word-break: break-all;
        }
    </style>
</head>

<body>
<img class="background-ncc-logo-full" src="<?php echo base_url("/assets/images/ncc-logo-grayscale.png"); ?>" alt="NCC logo">
<!-- Modal Simpan-->
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="modal-add">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Simpan Grafik</h4>
            </div>
            <form class="form-add" onsubmit="event.preventDefault(); save_url();">
                <div class="modal-body">
                    <input type="hidden" name="idtipe" value="1">
                    <input type="hidden" id="input-url" name="url_data" value="-">
                    <div class="form-group">
                        <label for="input-judul">Judul</label>
                        <input class="form-control" id="input-judul" type="text" name="judul" placeholder="Judul...">
                    </div>
                    <div class="form-group">
                        <label for="input-deskripsi">Deskripsi</label>
                        <input class="form-control" id="input-deskripsi" type="text" name="deskripsi"
                               placeholder="Deskripsi...">
                    </div>
                    <div class="form-group">
                        <label for="idkategori">Kategori</label>
                        <label for="idkategori">Kategori</label><br>
                        <select name="idkategori" id="idkategori" class="form-control"></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-times"></span>
                        Close
                    </button>
                    <button class="btn btn-success"><span class="fa fa-plus"></span> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <!-- CHART -->
        <div class="chart-wrapper">
            <div class="col-md-12" id="chart-data-wrapper">
                <div class="box box-primary">
                    <div class="box-header with-border" style="padding: 20px; background-color: #EFEFEF">
                        <h3 class="box-title" style="font-size: 15px;">
                            <span class="fa fa-bar-chart"></span> <b>Grafik</b>&nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="<?php echo base_url('dashboard') ?>"><span class="fa fa-briefcase"></span>
                                Manage</a>
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                        class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="input-parameter col-md-2 well">
                            <form class="form-data" onsubmit="event.preventDefault(); submit_condition();">
                                <fieldset>
                                    <div class="form-group" id="select-subskpd-wrapper">
                                        <label for="select-subskpd">DATA : </label>
                                        <select id="select-subskpd" name="idsubmodule" class="form-control"
                                                onchange="onchange_select_subskpd()">
                                            <option disabled value="" selected>Pilih Data</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="select-based-wrapper"
                                         onchange="onchange_select_based()">
                                        <label for="select-based">Berdasarkan : </label>
                                        <select id="select-based" name="based" class="form-control">
                                        </select>
                                    </div>
                                    <div class="form-group" id="input-year-wrapper">
                                        <label for="input-year">Tahun : </label>
                                        <br>
                                        <label for="checkbox-year-all">
                                            <input type="checkbox" name="year-all" id="checkbox-year-all"
                                                   onchange="onchange_checkbox_year_all()"> Seluruh Tahun
                                        </label>
                                        <input class="form-control" id="input-year" name="input_tahun" type="number"
                                               min="1900"
                                               max="2099" step="1" value="2017">
                                    </div>
                                    <div class="form-group" id="select-month-wrapper">
                                        <label for="select-month">Bulan : </label>

                                        <select id="select-month" name="input_bulan" class="form-control">
                                            <option id="select-month-0" value="0" selected>Seluruh</option>
                                            <option id="select-month-1" value="1">Januari</option>
                                            <option id="select-month-2" value="2">Februari</option>
                                            <option id="select-month-3" value="3">Maret</option>
                                            <option id="select-month-4" value="4">April</option>
                                            <option id="select-month-5" value="5">Mei</option>
                                            <option id="select-month-6" value="6">Juni</option>
                                            <option id="select-month-7" value="7">Juli</option>
                                            <option id="select-month-8" value="8">Agustus</option>
                                            <option id="select-month-9" value="9">September</option>
                                            <option id="select-month-10" value="10">Oktober</option>
                                            <option id="select-month-11" value="11">November</option>
                                            <option id="select-month-12" value="12">Desember</option>
                                        </select>
                                    </div>
                                    <div class="form-group" id="select-param-wrapper">
                                        <label for="select-param">Parameter : </label>
                                        <select id="select-param" name="param" class="form-control"></select>
                                    </div>
                                    <button class="btn btn-warning btn-block"><span class="fa fa-eye"></span>
                                        Lihat Grafik
                                    </button>
                                </fieldset>
                            </form>
                            <button id="btn-save-url" class="btn btn-success btn-block" onclick="show_add()"
                                    data-toggle='modal'
                                    data-target='#modal-add'>
                                <span class="fa fa-floppy-o"></span> Simpan
                            </button>
                        </div>
                        <div class="url-box col-md-10">
                            <div class="table-responsive" style="overflow-x: hidden">
                                <table id="datatables"
                                       class="table table-bordered table-striped display nowrap dt-responsive">
                                    <thead>
                                    <th>Judul</th>
                                    <th>Deskripsi</th>
                                    <th>Tautan</th>
                                    <th>Kategori</th>
                                    <th>Aksi</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div id="chart-area" style="padding: 10px">
                <div class="col-md-2">
                    <table class="table table-striped table-bordered" id="chart-table">
                        <thead>
                        <tr>
                            <th colspan="3" class="text-center">Tabel Data</th>
                        </tr>
                        <tr>
                            <th>Dasar</th>
                            <th>Parameter</th>
                            <th>Jumlah</th>
                        </tr>
                        </thead>
                        <tbody id="chart-table-body">

                        </tbody>
                        <tfoot id="chart-table-foot">

                        </tfoot>
                    </table>
                </div>
                <div id="chart-preview" class="col-md-10">
                    <div class="chart" style="height: 100%; padding: 5px 0 5px 0">
                        <div id="chart_opening" class="text-center alert col-md-4 col-md-offset-4"
                             style="margin-top: 10px">
                            <b class="lead">Area Grafik</b><br><br>
                            Silahkan tentukan data grafik yang ingin ditampilkan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url("/assets/js/jquery.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/bootstrap.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.loading.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/jquery.dataTables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/datatables.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/select2.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/sweetalert.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/app.min.js") ?>"></script>
<script src="<?php echo base_url("/assets/js/url.min.js") ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.min.js"></script>
<?php if (isset($script_plus)) echo $script_plus ?>
</body>
</html>
