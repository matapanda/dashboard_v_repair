<?php

class Hpkt_model extends MY_Model
{

    public $id_hasilpendapatankeluargatahu;
    public $idkecamatan;
    public $tanggal;
    public $jumlahkeluarga;
    public $jumlahanggotakeluarga_balita;
    public $jumlahanggotakeluarga_anak;
    public $jumlahanggotakeluarga_remaja;
    public $jumlahanggotakeluarga_dewasa;
    public $jumlahanggotakeluarga_lansia;
    public $status_pus;
    public $statuspusmupar;
    public $pasangansubur_mow;
    public $pasangansubur_mop;
    public $pasangansubur_iud;
    public $pasangansubur_implan;
    public $pasangansubur_suntik;
    public $pasangansubur_pil;
    public $pasangansubur_kondom;
    public $pasangansubur_tradisional;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('dp3p2kb_hasilpendapatankeluargatahu');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dp3p2kb_hasilpendapatankeluargatahu.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dp3p2kb_hasilpendapatankeluargatahu');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dp3p2kb_hasilpendapatankeluargatahu.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_hasilpendapatankeluargatahu.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->id_hasilpendapatankeluargatahu === null) {
            return $this->db->get()->result();
        }
        $this->db->where('id_hasilpendapatankeluargatahu', $this->id_hasilpendapatankeluargatahu);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dp3p2kb_hasilpendapatankeluargatahu', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('idkecamatan', $this->idkecamatan);
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('jumlahkeluarga', $this->jumlahkeluarga);
        $this->db->set('jumlahanggotakeluarga_balita', $this->jumlahanggotakeluarga_balita);
        $this->db->set('jumlahanggotakeluarga_anak', $this->jumlahanggotakeluarga_anak);
        $this->db->set('jumlahanggotakeluarga_remaja', $this->jumlahanggotakeluarga_remaja);
        $this->db->set('jumlahanggotakeluarga_dewasa', $this->jumlahanggotakeluarga_dewasa);
        $this->db->set('jumlahanggotakeluarga_lansia', $this->jumlahanggotakeluarga_lansia);
        $this->db->set('status_pus', $this->status_pus);
        $this->db->set('statuspusmupar', $this->statuspusmupar);
        $this->db->set('pasangansubur_mow', $this->pasangansubur_mow);
        $this->db->set('pasangansubur_mop', $this->pasangansubur_mop);
        $this->db->set('pasangansubur_iud', $this->pasangansubur_iud);
        $this->db->set('pasangansubur_implan', $this->pasangansubur_implan);
        $this->db->set('pasangansubur_suntik', $this->pasangansubur_suntik);
        $this->db->set('pasangansubur_pil', $this->pasangansubur_pil);
        $this->db->set('pasangansubur_kondom', $this->pasangansubur_kondom);
        $this->db->set('pasangansubur_tradisional', $this->pasangansubur_tradisional);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('id_hasilpendapatankeluargatahu', $this->id_hasilpendapatankeluargatahu);
        if ($this->db->update('dp3p2kb_hasilpendapatankeluargatahu')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('id_hasilpendapatankeluargatahu', $this->id_hasilpendapatankeluargatahu);
        if ($this->db->delete('dp3p2kb_hasilpendapatankeluargatahu')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'kecamatan',
            'tanggal',
            'status_pus',
            'statuspusmupar',
            'jumlahkeluarga',
            'jumlahanggotakeluarga_balita',
            'jumlahanggotakeluarga_anak',
            'jumlahanggotakeluarga_remaja',
            'jumlahanggotakeluarga_dewasa',
            'jumlahanggotakeluarga_lansia',
            'pasangansubur_mow',
            'pasangansubur_mop',
            'pasangansubur_iud',
            'pasangansubur_implan',
            'pasangansubur_suntik',
            'pasangansubur_pil',
            'pasangansubur_kondom',
            'pasangansubur_tradisional',
            'username'
        );
        $column_search = array(
            'kecamatan',
            'tanggal',
            'status_pus',
            'statuspusmupar',
            'jumlahkeluarga',
            'jumlahanggotakeluarga_balita',
            'jumlahanggotakeluarga_anak',
            'jumlahanggotakeluarga_remaja',
            'jumlahanggotakeluarga_dewasa',
            'jumlahanggotakeluarga_lansia',
            'pasangansubur_mow',
            'pasangansubur_mop',
            'pasangansubur_iud',
            'pasangansubur_implan',
            'pasangansubur_suntik',
            'pasangansubur_pil',
            'pasangansubur_kondom',
            'pasangansubur_tradisional',
            'username'
        );

        $order = array('id_hasilpendapatankeluargatahu' => 'asc'); // default order

        $this->db->select('dp3p2kb_hasilpendapatankeluargatahu.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dp3p2kb_hasilpendapatankeluargatahu');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dp3p2kb_hasilpendapatankeluargatahu.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_hasilpendapatankeluargatahu.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dp3p2kb_hasilpendapatankeluargatahu');
        return $this->db->count_all_results();
    }
}
