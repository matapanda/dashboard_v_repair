<?php

class DataPosyandu_model extends MY_Model
{

    public $iddataposyandu;
    public $jumlahposyandu;
    public $idkelurahan;
    public $tanggal;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('dp3p2kb_dataposyandu');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dp3p2kb_dataposyandu.*,
    kelurahan.*,
    kecamatan.*,
    kota.*,
    propinsi.*,
    app_user.idapp_user,
    app_user.username');
        $this->db->from('dp3p2kb_dataposyandu');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dp3p2kb_dataposyandu.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_dataposyandu.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->iddataposyandu === null) {
            return $this->db->get()->result();
        }
        $this->db->where('$iddataposyandu', $this->iddataposyandu);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dp3p2kb_dataposyandu', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('jumlahposyandu', $this->jumlahposyandu);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('iddataposyandu', $this->iddataposyandu);
        if ($this->db->update('dp3p2kb_dataposyandu')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('iddataposyandu', $this->iddataposyandu);
        if ($this->db->delete('dp3p2kb_dataposyandu')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'kelurahan',
            'jumlahposyandu',
            'tanggal',
            'username'
        );
        $column_search = array(
            'kelurahan',
            'jumlahposyandu',
            'tanggal',
            'username'
        );

        $order = array('iddataposyandu' => 'asc'); // default order

        $this->db->select('dp3p2kb_dataposyandu.*,
    kelurahan.*,
    kecamatan.*,
    kota.*,
    propinsi.*,
    app_user.idapp_user,
    app_user.username');
        $this->db->from('dp3p2kb_dataposyandu');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dp3p2kb_dataposyandu.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_dataposyandu.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dp3p2kb_dataposyandu');
        return $this->db->count_all_results();
    }
}
