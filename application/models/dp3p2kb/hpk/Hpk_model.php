<?php

class Hpk_model extends MY_Model
{

    public $idhasilpendapatankeluarga;
    public $idkecamatan;
    public $tanggal;
    public $desa_kelurahan_ada;
    public $desa_kelurahan_didata;
    public $dusun_rw_ada;
    public $dusun_rw_didata;
    public $rt_ada;
    public $rt_didata;
    public $jumlahkepalakeluarga_didata;
    public $bukanpesertakb_hamil;
    public $bukanpesertakb_inginanaksegera;
    public $bukanpesertakb_inginanakditunda;
    public $bukanpesertakb_tidakinginanaklagi;
    public $kesertaandalampoktan_bkp;
    public $kesertaandalampoktan_bkr;
    public $kesertaandalampoktan_bkl;
    public $kesertaandalampoktan_lppks;
    public $kesertaandalampoktan_pik_rm;
    public $tahapankeluargasejahtera_prasejahtera;
    public $tahapankeluargasejahtera_sejahtera1;
    public $tahapankeluargasejahtera_sejahtera2;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('dp3p2kb_hasilpendapatankeluarga');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dp3p2kb_hasilpendapatankeluarga.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dp3p2kb_hasilpendapatankeluarga');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dp3p2kb_hasilpendapatankeluarga.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_hasilpendapatankeluarga.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idhasilpendapatankeluarga === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idhasilpendapatankeluarga', $this->idhasilpendapatankeluarga);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dp3p2kb_hasilpendapatankeluarga', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('idkecamatan', $this->idkecamatan);
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('desa_kelurahan_ada', $this->desa_kelurahan_ada);
        $this->db->set('desa_kelurahan_didata', $this->desa_kelurahan_didata);
        $this->db->set('dusun_rw_ada', $this->dusun_rw_ada);
        $this->db->set('dusun_rw_didata', $this->dusun_rw_didata);
        $this->db->set('rt_ada', $this->rt_ada);
        $this->db->set('rt_didata', $this->rt_didata);
        $this->db->set('jumlahkepalakeluarga_didata', $this->jumlahkepalakeluarga_didata);
        $this->db->set('bukanpesertakb_hamil', $this->bukanpesertakb_hamil);
        $this->db->set('bukanpesertakb_inginanaksegera', $this->bukanpesertakb_inginanaksegera);
        $this->db->set('bukanpesertakb_inginanakditunda', $this->bukanpesertakb_inginanakditunda);
        $this->db->set('bukanpesertakb_tidakinginanaklagi', $this->bukanpesertakb_tidakinginanaklagi);
        $this->db->set('kesertaandalampoktan_bkp', $this->kesertaandalampoktan_bkp);
        $this->db->set('kesertaandalampoktan_bkr', $this->kesertaandalampoktan_bkr);
        $this->db->set('kesertaandalampoktan_bkl', $this->kesertaandalampoktan_bkl);
        $this->db->set('kesertaandalampoktan_lppks', $this->kesertaandalampoktan_lppks);
        $this->db->set('kesertaandalampoktan_pik_rm', $this->kesertaandalampoktan_pik_rm);
        $this->db->set('tahapankeluargasejahtera_prasejahtera', $this->tahapankeluargasejahtera_prasejahtera);
        $this->db->set('tahapankeluargasejahtera_sejahtera1', $this->tahapankeluargasejahtera_sejahtera1);
        $this->db->set('tahapankeluargasejahtera_sejahtera2', $this->tahapankeluargasejahtera_sejahtera2);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('idhasilpendapatankeluarga', $this->idhasilpendapatankeluarga);
        if ($this->db->update('dp3p2kb_hasilpendapatankeluarga')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idhasilpendapatankeluarga', $this->idhasilpendapatankeluarga);
        if ($this->db->delete('dp3p2kb_hasilpendapatankeluarga')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'kecamatan',
            'jumlahkepalakeluarga_didata',
            'rt_ada',
            'rt_didata',
            'dusun_rw_ada',
            'dusun_rw_didata',
            'desa_kelurahan_ada',
            'desa_kelurahan_didata',
            'bukanpesertakb_hamil',
            'bukanpesertakb_inginanaksegera',
            'bukanpesertakb_inginanakditunda',
            'bukanpesertakb_tidakinginanaklagi',
            'kesertaandalampoktan_bkp',
            'kesertaandalampoktan_bkr',
            'kesertaandalampoktan_bkl',
            'kesertaandalampoktan_lppks',
            'kesertaandalampoktan_pik_rm',
            'tahapankeluargasejahtera_prasejahtera',
            'tahapankeluargasejahtera_sejahtera1',
            'tahapankeluargasejahtera_sejahtera2',
            'username'
        );
        $column_search = array(
            'tanggal',
            'kecamatan',
            'jumlahkepalakeluarga_didata',
            'rt_ada',
            'rt_didata',
            'dusun_rw_ada',
            'dusun_rw_didata',
            'desa_kelurahan_ada',
            'desa_kelurahan_didata',
            'bukanpesertakb_hamil',
            'bukanpesertakb_inginanaksegera',
            'bukanpesertakb_inginanakditunda',
            'bukanpesertakb_tidakinginanaklagi',
            'kesertaandalampoktan_bkp',
            'kesertaandalampoktan_bkr',
            'kesertaandalampoktan_bkl',
            'kesertaandalampoktan_lppks',
            'kesertaandalampoktan_pik_rm',
            'tahapankeluargasejahtera_prasejahtera',
            'tahapankeluargasejahtera_sejahtera1',
            'tahapankeluargasejahtera_sejahtera2',
            'username'
        );

        $order = array('idhasilpendapatankeluarga' => 'asc'); // default order

        $this->db->select('dp3p2kb_hasilpendapatankeluarga.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dp3p2kb_hasilpendapatankeluarga');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dp3p2kb_hasilpendapatankeluarga.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_hasilpendapatankeluarga.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dp3p2kb_hasilpendapatankeluarga');
        return $this->db->count_all_results();
    }
}
