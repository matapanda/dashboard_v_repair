<?php

class Kdrt_model extends MY_Model
{
    public $idkdrt;
    public $tanggal;
    public $jumlah_korban;
    public $idkecamatan;
    public $idjeniskasus;
    public $idjeniskorban;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('dp3p2kb_kdrt');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {

        $this->db->select('dp3p2kb_kdrt.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dp3p2kb_jeniskasus.*,
        dp3p2kb_jeniskorban.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dp3p2kb_kdrt');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dp3p2kb_kdrt.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dp3p2kb_jeniskasus', 'dp3p2kb_jeniskasus.idjeniskasus = dp3p2kb_kdrt.idjeniskasus');
        $this->db->join('dp3p2kb_jeniskorban', 'dp3p2kb_jeniskorban.idjeniskorban = dp3p2kb_kdrt.idjeniskorban');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_kdrt.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idkdrt === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idkdrt', $this->idkdrt);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        if ($this->db->insert('dp3p2kb_kdrt', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('jumlah_korban', $this->jumlah_korban);
        $this->db->set('idkecamatan', $this->idkecamatan);
        $this->db->set('idjeniskasus', $this->idjeniskasus);
        $this->db->set('idjeniskorban', $this->idjeniskorban);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('idkdrt', $this->idkdrt);
        if ($this->db->update('dp3p2kb_kdrt')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idkdrt', $this->idkdrt);
        if ($this->db->delete('dp3p2kb_kdrt')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'kecamatan',
            'jeniskasus',
            'jeniskorban',
            'jumlah_korban',
            'username'
        );
        $column_search = array(
            'tanggal',
            'kecamatan',
            'jeniskasus',
            'jeniskorban',
            'jumlah_korban',
            'username'
        );
        $order = array('idkdrt' => 'asc'); // default order

        $this->db->select('dp3p2kb_kdrt.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dp3p2kb_jeniskasus.*,
        dp3p2kb_jeniskorban.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dp3p2kb_kdrt');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = dp3p2kb_kdrt.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dp3p2kb_jeniskasus', 'dp3p2kb_jeniskasus.idjeniskasus = dp3p2kb_kdrt.idjeniskasus');
        $this->db->join('dp3p2kb_jeniskorban', 'dp3p2kb_jeniskorban.idjeniskorban = dp3p2kb_kdrt.idjeniskorban');
        $this->db->join('app_user', 'app_user.idapp_user = dp3p2kb_kdrt.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dp3p2kb_kdrt');
        return $this->db->count_all_results();
    }
}
