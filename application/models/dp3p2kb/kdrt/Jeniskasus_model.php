<?php

class Jeniskasus_model extends CI_Model
{
    public $idjeniskasus;
    public $jeniskasus;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idjeniskasus === null){
            return $this->db->get('dp3p2kb_jeniskasus')->result();
        }
        $this->db->where('idjeniskasus', $this->idjeniskasus);
        return $this->db->get('dp3p2kb_jeniskasus')->row();
    }

    public function add(){
        if($this->db->insert('dp3p2kb_jeniskasus', $this)){
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('jeniskasus', $this->jeniskasus);
        $this->db->where('idjeniskasus', $this->idjeniskasus);
        if($this->db->update('dp3p2kb_jeniskasus')){
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idjeniskasus', $this->idjeniskasus);
        if($this->db->delete('dp3p2kb_kdrt')){
            $this->db->where('idjeniskasus', $this->idjeniskasus);
            if($this->db->delete('dp3p2kb_jeniskasus')){
                return $this;
            }
        }
        return FALSE;
    }
}