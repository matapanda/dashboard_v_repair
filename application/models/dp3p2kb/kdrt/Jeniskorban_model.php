<?php

class Jeniskorban_model extends CI_Model
{
    public $idjeniskorban;
    public $jeniskorban;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idjeniskorban === null){
            return $this->db->get('dp3p2kb_jeniskorban')->result();
        }
        $this->db->where('idjeniskorban', $this->idjeniskorban);
        return $this->db->get('dp3p2kb_jeniskorban')->row();
    }

    public function add(){
        if($this->db->insert('dp3p2kb_jeniskorban', $this)){
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('jeniskorban', $this->jeniskorban);
        $this->db->where('idjeniskorban', $this->idjeniskorban);
        if($this->db->update('dp3p2kb_jeniskorban')){
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idjeniskorban', $this->idjeniskorban);
        if($this->db->delete('dp3p2kb_kdrt')){
            $this->db->where('idjeniskorban', $this->idjeniskorban);
            if($this->db->delete('dp3p2kb_jeniskorban')){
                return $this;
            }
        }
        return FALSE;
    }
}