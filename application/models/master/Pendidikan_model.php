<?php
class Pendidikan_model extends CI_Model {
    public $idpendidikan;
    public $pendidikan;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('pendidikan');
        if($this->idpendidikan) $this->db->where('idpendidikan', $this->idpendidikan);
        if($this->pendidikan) $this->db->where('pendidikan', $this->pendidikan);
        $query = $this->db->get();
        if($this->idpendidikan || $this->pendidikan){
            return $query->row();
        }
        return $query->result();
    }
}