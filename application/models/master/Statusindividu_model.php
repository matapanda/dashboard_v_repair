<?php
class Statusindividu_model extends CI_Model {
    public $idstatusindividu;
    public $statusindividu;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('statusindividu');
        if($this->idstatusindividu) $this->db->where('idstatusindividu', $this->idstatusindividu);
        if($this->statusindividu) $this->db->where('statusindividu', $this->statusindividu);
        $query = $this->db->get();
        if($this->idstatusindividu || $this->statusindividu){
            return $query->row();
        }
        return $query->result();
    }
}