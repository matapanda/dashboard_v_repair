<?php
class Statuskawin_model extends CI_Model {
    public $idstatuskawin;
    public $status;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('statuskawin');
        if($this->idstatuskawin) $this->db->where('idstatuskawin', $this->idstatuskawin);
        if($this->status) $this->db->where('status', $this->status);
        $query = $this->db->get();
        if($this->idstatuskawin || $this->status){
            return $query->row();
        }
        return $query->result();
    }
}