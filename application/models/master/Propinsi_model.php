<?php
class Propinsi_model extends CI_Model {
    public $idpropinsi;
    public $propinsi;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('propinsi');
        if($this->idpropinsi) $this->db->where('idpropinsi', $this->idpropinsi);
        if($this->propinsi) $this->db->where('propinsi', $this->propinsi);
        $query = $this->db->get();
        if($this->idpropinsi || $this->propinsi){
            return $query->row();
        }
        return $query->result();
    }
}