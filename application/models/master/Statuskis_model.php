<?php
class Statuskis_model extends CI_Model {
    public $idstatuskis;
    public $status;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('statuskis');
        if($this->idstatuskis) $this->db->where('idstatuskis', $this->idstatuskis);
        if($this->status) $this->db->where('status', $this->status);
        $query = $this->db->get();
        if($this->idstatuskis || $this->status){
            return $query->row();
        }
        return $query->result();
    }
}
