<?php
class Kecamatan_model extends CI_Model {
    public $idkecamatan;
    public $kecamatan;
    public $idkota;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('kecamatan');
        if($this->idkecamatan) $this->db->where('idkecamatan', $this->idkecamatan);
        if($this->kecamatan) $this->db->where('kecamatan', $this->kecamatan);
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $query = $this->db->get();
        if($this->idkecamatan || $this->kecamatan){
            return $query->row();
        }
        return $query->result();
    }
}