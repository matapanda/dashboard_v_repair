<?php
class Statuskeluarga_model extends CI_Model {
    public $idstatuskeluarga;
    public $statuskeluarga;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('statuskeluarga');
        if($this->idstatuskeluarga) $this->db->where('idstatuskeluarga', $this->idstatuskeluarga);
        if($this->statuskeluarga) $this->db->where('statuskeluarga', $this->statuskeluarga);
        $query = $this->db->get();
        if($this->idstatuskeluarga || $this->statuskeluarga){
            return $query->row();
        }
        return $query->result();
    }
}