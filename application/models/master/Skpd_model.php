<?php
class Skpd_model extends CI_Model {
    public $idskpd;
    public $skpd;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('skpd');
        if($this->idskpd) $this->db->where('idskpd', $this->idskpd);
        if($this->skpd) $this->db->where('skpd', $this->skpd);
        $query = $this->db->get();
        if($this->idskpd || $this->skpd){
            return $query->row();
        }
        return $query->result();
    }
}