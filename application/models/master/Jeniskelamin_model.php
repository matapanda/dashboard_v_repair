<?php
class Jeniskelamin_model extends CI_Model {
    public $idjeniskelamin;
    public $jeniskelamin;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('jeniskelamin');
        if($this->idjeniskelamin) $this->db->where('idjeniskelamin', $this->idjeniskelamin);
        if($this->jeniskelamin) $this->db->where('jeniskelamin', $this->jeniskelamin);
        $query = $this->db->get();
        if($this->idjeniskelamin || $this->jeniskelamin){
            return $query->row();
        }
        return $query->result();
    }
}