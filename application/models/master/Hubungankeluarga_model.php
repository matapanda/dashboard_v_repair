<?php
class Hubungankeluarga_model extends CI_Model {
    public $idhubungankeluarga;
    public $hubungankeluarga;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('hubungankeluarga');
        if($this->idhubungankeluarga) $this->db->where('idhubungankeluarga', $this->idhubungankeluarga);
        if($this->hubungankeluarga) $this->db->where('hubungankeluarga', $this->hubungankeluarga);
        $query = $this->db->get();
        if($this->idhubungankeluarga || $this->hubungankeluarga){
            return $query->row();
        }
        return $query->result();
    }
}