<?php
class Kota_model extends CI_Model {
    public $idkota;
    public $kota;
    public $idpropinsi;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('kota');
        if($this->idkota) $this->db->where('idkota', $this->idkota);
        if($this->kota) $this->db->where('kota', $this->kota);
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $query = $this->db->get();
        if($this->idkota || $this->kota){
            return $query->row();
        }
        return $query->result();
    }
}