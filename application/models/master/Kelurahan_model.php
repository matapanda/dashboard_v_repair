<?php
class Kelurahan_model extends CI_Model {
    public $idkelurahan;
    public $kelurahan;
    public $idkecamatan;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('kelurahan');
        if($this->idkelurahan) $this->db->where('idkelurahan', $this->idkelurahan);
        if($this->kelurahan) $this->db->where('kelurahan', $this->kelurahan);
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $query = $this->db->get();
        if($this->idkelurahan || $this->kelurahan){
            return $query->row();
        }
        return $query->result();
    }
}