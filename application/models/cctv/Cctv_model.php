<?php

class Cctv_model extends CI_Model
{

    public $idcctv;
    public $judul;
    public $source;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('app_cctv.*');
        $this->db->from('app_cctv');

        if ($this->idcctv) {
            $this->db->where('app_cctv.idcctv', $this->idcctv);
            return $this->db->get()->row();
        } elseif ($this->judul) {
            $this->db->where('app_cctv.judul', $this->judul);
            return $this->db->get()->result();
        } else {
            return $this->db->get()->result();
        }
    }

    public function add()
    {
        if ($this->db->insert('app_cctv', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('judul', $this->judul);
        $this->db->set('source', $this->source);
        $this->db->where('idcctv', $this->idcctv);
        if ($this->db->update('app_cctv')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idcctv', $this->idcctv);
        if ($this->db->delete('app_cctv')) {
            return $this;
        }
        return FALSE;
    }
}