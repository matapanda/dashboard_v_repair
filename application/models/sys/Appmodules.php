<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppModules extends CI_Model {

  public $idmodules;
  public $modules;

  public function get($idapp_user=null, $limit = null, $page = null)
  {
    $this->db->select('
      DISTINCT(app_modules.modules) as modules,
      app_modules.idmodules
    ');

    $this->db->from('app_modules');
    $this->db->join('app_submodule', 'app_submodule.idmodules = app_modules.idmodules', 'left');
    $this->db->join('app_usermodules', "app_usermodules.idapp_user is null or app_usermodules.idapp_user = '".$idapp_user."' and app_usermodules.idsubmodule = app_submodule.idsubmodule", 'left');
    $this->db->join('app_user', 'app_user.idapp_user = app_usermodules.idapp_user', 'left');

    if(is_numeric($limit) && is_numeric($page)){
      $offset = ($page - 1) * $limit;
      return $this->db->limit($limit, $offset)->get()->result();
    }

    if($idapp_user !== null){
      $this->db->where('app_usermodules.idapp_user', $idapp_user);
    }
    return $this->db->get()->result();
  }

}
