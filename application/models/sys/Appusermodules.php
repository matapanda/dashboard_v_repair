<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appusermodules extends CI_Model {
  var $idapp_user;
  var $iswrite;
  var $isread;
  var $idsubmodule;

  public function add()
  {
    $this->db->db_debug = FALSE; 
    if($this->db->insert('app_usermodules', $this)) return true;
    return false;
  }

  public function update()
  {
    $this->db->set('iswrite', $this->iswrite);
    $this->db->set('isread', $this->isread);
    $this->db->where('idapp_user', $this->idapp_user);
    $this->db->where('idsubmodule', $this->idsubmodule);
    if($this->db->update('app_usermodules')) return true;
    return false;
  }

  public function get($limit = null, $page = null)
  {
    $this->db->select(
      '
      app_user.username,
      app_usermodules.*,
      app_submodule.*,
      app_modules.*
      '
    );
    $this->db->from('app_modules');
    $this->db->join('app_submodule', 'app_submodule.idmodules = app_modules.idmodules', 'left');
    $this->db->join('app_usermodules', 'app_usermodules.idapp_user is null or app_usermodules.idapp_user ='.$this->idapp_user.' and app_usermodules.idsubmodule = app_submodule.idsubmodule', 'left');
    $this->db->join('app_user', 'app_user.idapp_user = app_usermodules.idapp_user', 'left');

    if(is_numeric($limit) && is_numeric($page)){
      $offset = ($page - 1) * $limit;
      return $this->db->limit($limit, $offset)->get()->result();
    }
    if($this->idapp_user !== null){
      $this->db->where('app_user.idapp_user =', $this->idapp_user);
      $this->db->or_where('app_user.idapp_user =', null);
    }
    return $this->db->get()->result();
  }


}
