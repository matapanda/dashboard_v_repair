<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppSubmodule extends CI_Model {

  public $idsubmodule;

  public function get($limit = null, $page = null)
  {
    $this->db->select('
      app_submodule.*,
      app_modules.*
    ');

    $this->db->from('app_submodule');
    $this->db->join('app_modules', 'app_submodule.idmodules = app_modules.idmodules');

    if(is_numeric($limit) && is_numeric($page)){
      $offset = ($page - 1) * $limit;
      return $this->db->limit($limit, $offset)->get()->result();
    }
    
    if($this->idsubmodule !== null){

      $this->db->where('app_submodule.idsubmodule', $this->idsubmodule);
    }
    return $this->db->get()->result();
  }

}
