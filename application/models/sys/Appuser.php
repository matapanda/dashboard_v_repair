<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppUser extends CI_Model {
  var $idapp_user;
  var $username;
  var $password;
  var $ismultiskpd;
  var $idskpd;
  var $statusaktif;

  public function __construct()
  {
    $this->load->database();
  }

  public function get($limit = null, $page = null){
    $this->db->select(
      'app_user.*,
      skpd.*'
    );
    $this->db->from('app_user');
    $this->db->join('skpd', 'app_user.idskpd = skpd.idskpd');
    if(is_numeric($limit) && is_numeric($page)){
      $offset = ($page - 1) * $limit;
      return $this->db->limit($limit, $offset)->get()->result();
    }
    if ($this->username !== null ){
      $this->db->where('username', $this->username);
    } else if($this->idapp_user !== null){
      $this->db->where('idapp_user', $this->idapp_user);
    } else {
      return $this->db->get()->result();
    }
    return $this->db->get()->row();
  }

  public function add()
  {
    if($this->db->insert('app_user', $this)) return true;
    return false;
  }

  public function update()
  {
    $data = array (
      'username' => $this->username,
      'password' => $this->password,
      'ismultiskpd' => $this->ismultiskpd,
      'idskpd' => $this->idskpd,
      'statusaktif' => $this->statusaktif
    );
    $this->db->where('idapp_user', $this->idapp_user);
    if ($this->db->update('app_user', $data)) return true;
    return false;
  }

  public function delete()
  {
    $tables = array('app_user', 'app_usermodules');

    $this->db->where('idapp_user', $this->idapp_user);
    $this->db->delete($tables);
  }

}
