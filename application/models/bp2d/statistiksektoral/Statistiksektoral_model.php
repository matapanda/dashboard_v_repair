<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class StatistikSektoral_Model extends MY_Model
{

    public $idbp2d;
    public $tanggal;
    public $namapajak;
    public $satuan;
    public $target;
    public $realisasi;
    public $sumberdata;
    public $keterangan;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('bp2d_statistiksektoral');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      bp2d_statistiksektoral.* ,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('bp2d_statistiksektoral');
        $this->db->join('app_user', 'app_user.idapp_user = bp2d_statistiksektoral.idapp_user');

        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idbp2d === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idbp2d', $this->idbp2d);
        return $this->db->get()->row();

    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('bp2d_statistiksektoral', $this)) return $this;
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('namapajak', $this->namapajak);
        $this->db->set('satuan', $this->satuan);
        $this->db->set('target', $this->target);
        $this->db->set('realisasi', $this->realisasi);
        $this->db->set('sumberdata', $this->sumberdata);
        $this->db->set('keterangan', $this->keterangan);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idbp2d', $this->idbp2d);
        if ($this->db->update('bp2d_statistiksektoral')) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idbp2d', $this->idbp2d);
        if ($this->db->delete('bp2d_statistiksektoral')) return $this;
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'namapajak',
            'satuan',
            'target',
            'realisasi',
            'sumberdata',
            'keterangan',
            'username',
            'username'
        );
        $column_search = array(
            'tanggal',
            'namapajak',
            'satuan',
            'target',
            'realisasi',
            'sumberdata',
            'keterangan',
            'username',
            'username'
        );
        $order = array('idbp2d' => 'asc'); // default order

        $this->db->select('
      bp2d_statistiksektoral.* ,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('bp2d_statistiksektoral');
        $this->db->join('app_user', 'app_user.idapp_user = bp2d_statistiksektoral.idapp_user');
        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('bp2d_statistiksektoral');
        return $this->db->count_all_results();
    }
}
