<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pemakaman_model extends MY_Model
{

    public $idpemakaman;
    public $tanggal;
    public $namatpu;
    public $luastpu;
    public $luaslahanmakam; // (tahun)
    public $jumlahseluruhmakam;
    public $fasum; // (tahun)
    public $sisalahanmakam; // (tahun)
    public $keterangan;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('perum_pemakaman');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
    perum_pemakaman.idpemakaman ,
    YEAR(perum_pemakaman.tanggal) as tahun,
    perum_pemakaman.namatpu,
    perum_pemakaman.luastpu,
    perum_pemakaman.`luaslahanmakam` as luaslahanmakam,
    perum_pemakaman.jumlahseluruhmakam,
    perum_pemakaman.`fasum`,
    perum_pemakaman.`sisalahanmakam` ,
    perum_pemakaman.keterangan,
    app_user.idapp_user,
    app_user.username
    ');
        $this->db->from('perum_pemakaman');
        $this->db->join('app_user', 'app_user.idapp_user = perum_pemakaman.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idpemakaman === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idpemakaman', $this->idpemakaman);
        return $this->db->get()->row();

    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        $query = $this->db->query(
            'insert into perum_pemakaman ' .
            'values (
        ' . $this->idpemakaman . ',
        "' . $this->tanggal . '",
        "' . $this->namatpu . '",
        ' . $this->luastpu . ',
        ' . $this->luaslahanmakam . ',
        ' . $this->jumlahseluruhmakam . ',
        ' . $this->fasum . ',
        ' . $this->sisalahanmakam . ',
        "' . $this->keterangan . '",
        ' . $this->idapp_user . ',
        "' . $this->systemtime . '"
      )'
        );
        if ($query) return $this;
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        $query = $this->db->query(
            'update perum_pemakaman ' .
            'set
        `tanggal`="' . $this->tanggal . '",
        `namatpu`="' . $this->namatpu . '",
        `luastpu`=' . $this->luastpu . ',
        `luaslahanmakam`=' . $this->luaslahanmakam . ',
        `jumlahseluruhmakam`=' . $this->jumlahseluruhmakam . ',
        `fasum`=' . $this->fasum . ',
        `sisalahanmakam`=' . $this->sisalahanmakam . ',
        "`keterangan`=' . $this->keterangan . '",
        `idapp_user`=' . $this->idapp_user . ',
        `systemtime`="' . $this->systemtime . '"
      where idpemakaman=' . $this->idpemakaman . '
      '
        );

        if ($query) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idpemakaman', $this->idpemakaman);
        if ($this->db->delete('perum_pemakaman')) return $this;
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tahun',
            'namatpu',
            'luastpu',
            'luaslahanmakam',
            'jumlahseluruhmakam',
            'fasum',
            'sisalahanmakam',
            'keterangan'
        );
        $column_search = array(
            'tahun',
            'namatpu',
            'luastpu',
            'luaslahanmakam',
            'jumlahseluruhmakam',
            'fasum',
            'sisalahanmakam',
            'keterangan'
        );

        $order = array('idpemakaman' => 'asc'); // default order

        $this->db->select('
    perum_pemakaman.idpemakaman ,
    YEAR(perum_pemakaman.tanggal) as tahun,
    perum_pemakaman.namatpu,
    perum_pemakaman.luastpu,
    perum_pemakaman.`luaslahanmakam` as luaslahanmakam,
    perum_pemakaman.jumlahseluruhmakam,
    perum_pemakaman.`fasum`,
    perum_pemakaman.`sisalahanmakam` ,
    perum_pemakaman.keterangan,
    app_user.idapp_user,
    app_user.username
    ');
        $this->db->from('perum_pemakaman');
        $this->db->join('app_user', 'app_user.idapp_user = perum_pemakaman.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('perum_pemakaman');
        return $this->db->count_all_results();
    }
}
