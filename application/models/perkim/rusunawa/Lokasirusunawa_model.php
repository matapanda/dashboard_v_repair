<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LokasiRusunawa_model extends MY_Model {

  public $idlokasi;
  public $jenispertamanan;

  public function __construct()
  {
    $this->load->database();
  }

  public function get($limit = null, $page = null){

    if(is_numeric($limit) && is_numeric($page)){
      $offset = ($page - 1) * $limit;
      return $this->db->limit($limit, $offset)->get('perum_lokasi')->result();
    }
    if($this->idlokasi === null){
      return $this->db->get('perum_lokasi')->result();
    }
    $this->db->where('idlokasi', $this->idlokasi);
    return $this->db->get('perum_lokasi')->row();

  }
}
