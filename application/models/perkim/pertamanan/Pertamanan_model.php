<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pertamanan_model extends MY_Model
{

    public $idpertamanan;
    public $tanggal;
    public $langitude;
    public $longitude;
    public $nama_taman;
    public $urlgambarfasilitas;
    public $deskripsi;
    public $idjenispertamanan;
    public $idkelurahan;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('perum_pertamanan');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      perum_pertamanan.* ,
      perum_jenispertamanan.*,
      kelurahan.*,
      kecamatan.*,
      kota.*,
      propinsi.*,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('perum_pertamanan');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = perum_pertamanan.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('perum_jenispertamanan', 'perum_jenispertamanan.idjenispertamanan = perum_pertamanan.idjenispertamanan');
        $this->db->join('app_user', 'app_user.idapp_user = perum_pertamanan.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idpertamanan === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idpertamanan', $this->idpertamanan);
        return $this->db->get()->row();

    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('perum_pertamanan', $this)) return $this;
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('langitude', $this->langitude);
        $this->db->set('longitude', $this->longitude);
        $this->db->set('nama_taman', $this->nama_taman);
        $this->db->set('urlgambarfasilitas', $this->urlgambarfasilitas);
        $this->db->set('deskripsi', $this->deskripsi);
        $this->db->set('idjenispertamanan', $this->idjenispertamanan);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idpertamanan', $this->idpertamanan);
        if ($this->db->update('perum_pertamanan')) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idpertamanan', $this->idpertamanan);
        if ($this->db->delete('perum_pertamanan')) return $this;
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'kelurahan',
            'nama_taman',
            'urlgambarfasilitas',
            'deskripsi',
            'jenispertamanan',
            'langitude',
            'longitude',
            'username'
        );
        $column_search = array(
            'tanggal',
            'kelurahan',
            'nama_taman',
            'urlgambarfasilitas',
            'deskripsi',
            'jenispertamanan',
            'langitude',
            'longitude',
            'username'
        );

        $order = array('idpertamanan' => 'asc'); // default order

        $this->db->select('
      perum_pertamanan.* ,
      perum_jenispertamanan.*,
      kelurahan.*,
      kecamatan.*,
      kota.*,
      propinsi.*,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('perum_pertamanan');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = perum_pertamanan.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('perum_jenispertamanan', 'perum_jenispertamanan.idjenispertamanan = perum_pertamanan.idjenispertamanan');
        $this->db->join('app_user', 'app_user.idapp_user = perum_pertamanan.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('perum_pertamanan');
        return $this->db->count_all_results();
    }
}
