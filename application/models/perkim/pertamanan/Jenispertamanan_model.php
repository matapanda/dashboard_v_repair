<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jenispertamanan_model extends CI_Model {

  public $idjenispertamanan;
  public $jenispertamanan;

  public function __construct()
  {
    $this->load->database();
  }

  public function get($limit = null, $page = null){

    if(is_numeric($limit) && is_numeric($page)){
      $offset = ($page - 1) * $limit;
      return $this->db->limit($limit, $offset)->get('perum_jenispertamanan')->result();
    }
    if($this->idjenispertamanan === null){
      return $this->db->get('perum_jenispertamanan')->result();
    }
    $this->db->where('idjenispertamanan', $this->idjenispertamanan);
    return $this->db->get('perum_jenispertamanan')->row();

  }
}
