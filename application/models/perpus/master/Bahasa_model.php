<?php

class Bahasa_model extends CI_Model
{
    public $idbahasa;
    public $bahasa;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idbahasa === null){
            return $this->db->get('perpus_bahasa')->result();
        }
        $this->db->where('idbahasa', $this->idbahasa);
        return $this->db->get('perpus_bahasa')->row();
    }
}