<?php

class Kelas_model extends CI_Model
{
    public $idkelas;
    public $kelas;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idkelas === null){
            return $this->db->get('perpus_kelas')->result();
        }
        $this->db->where('idkelas', $this->idkelas);
        return $this->db->get('perpus_kelas')->row();
    }
}