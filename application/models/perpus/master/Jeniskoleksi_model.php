<?php

class Jeniskoleksi_model extends CI_Model
{
    public $idjeniskoleksi;
    public $jeniskoleksi_biasa;
    public $jeniskoleksi_koleksikecil;
    public $jeniskoleksi_tandon;


    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idjeniskoleksi === null){
            return $this->db->get('perpus_jeniskoleksi')->result();
        }
        $this->db->where('idjeniskoleksi', $this->idjeniskoleksi);
        return $this->db->get('perpus_jeniskoleksi')->row();
    }
}