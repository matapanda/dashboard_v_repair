<?php

class Peminjaman_model extends MY_Model
{

    public $idstatistikpeminjaman;
    public $tanggal;
    public $idkelas;
    public $idjeniskoleksi;
    public $jumlah;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('perpus_statistikpeminjaman');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('perpus_statistikpeminjaman.*,
        perpus_jeniskoleksi.*,
        perpus_kelas.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('perpus_statistikpeminjaman');
        $this->db->join('perpus_jeniskoleksi', 'perpus_jeniskoleksi.idjeniskoleksi = perpus_statistikpeminjaman.idjeniskoleksi');
        $this->db->join('perpus_kelas', 'perpus_kelas.idkelas = perpus_statistikpeminjaman.idkelas');
        $this->db->join('app_user', 'app_user.idapp_user = perpus_statistikpeminjaman.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idstatistikpeminjaman === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idstatistikpeminjaman', $this->idstatistikpeminjaman);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('perpus_statistikpeminjaman', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idkelas', $this->idkelas);
        $this->db->set('idjeniskoleksi', $this->idjeniskoleksi);
        $this->db->set('jumlah', $this->jumlah);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('idstatistikpeminjaman', $this->idstatistikpeminjaman);
        if ($this->db->update('perpus_statistikpeminjaman')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idstatistikpeminjaman', $this->idstatistikpeminjaman);
        if ($this->db->delete('perpus_statistikpeminjaman')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'kelas',
            'jeniskoleksi',
            'jumlah',
            'username'
        );
        $column_search = array(
            'tanggal',
            'kelas',
            'jeniskoleksi',
            'jumlah',
            'username'
        );

        $order = array('idstatistikpeminjaman' => 'asc'); // default order

        $this->db->select('perpus_statistikpeminjaman.*,
        perpus_jeniskoleksi.*,
        perpus_kelas.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('perpus_statistikpeminjaman');
        $this->db->join('perpus_jeniskoleksi', 'perpus_jeniskoleksi.idjeniskoleksi = perpus_statistikpeminjaman.idjeniskoleksi');
        $this->db->join('perpus_kelas', 'perpus_kelas.idkelas = perpus_statistikpeminjaman.idkelas');
        $this->db->join('app_user', 'app_user.idapp_user = perpus_statistikpeminjaman.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('perpus_statistikpeminjaman');
        return $this->db->count_all_results();
    }
}
