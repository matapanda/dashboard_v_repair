<?php

class Gelandanganpsikotik_model extends MY_Model
{

    public $idgelandanganpsikotik;
    public $tanggal;
    public $jumlah;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('dinsos_gelandanganpsikotik');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dinsos_gelandanganpsikotik.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinsos_gelandanganpsikotik');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_gelandanganpsikotik.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idgelandanganpsikotik === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idgelandanganpsikotik', $this->idgelandanganpsikotik);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dinsos_gelandanganpsikotik', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinsos_gelandanganpsikotik', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('jumlah', $this->jumlah);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idgelandanganpsikotik', $this->idgelandanganpsikotik);
        if ($this->db->update('dinsos_gelandanganpsikotik')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idgelandanganpsikotik', $this->idgelandanganpsikotik);
        if ($this->db->delete('dinsos_gelandanganpsikotik')) {
            return $this;
        }
        return FALSE;
    }

    //mendapatkan semua record hanya tahun dan jumlah
    public function get_based_date($year = null, $month = null, $day = null)
    {
        if ($year == null) {
            $this->db->select('YEAR(dinsos_gelandanganpsikotik.tanggal) AS tahun, dinsos_gelandanganpsikotik.jumlah');
            $this->db->from('dinsos_gelandanganpsikotik');
            $this->db->order_by('tahun', 'ASC');
        } elseif ($month == null) {
            $this->db->select('MONTH(dinsos_gelandanganpsikotik.tanggal) AS bulan, dinsos_gelandanganpsikotik.jumlah');
            $this->db->from('dinsos_gelandanganpsikotik');
            $this->db->where('YEAR(dinsos_gelandanganpsikotik.tanggal)', $year);
            $this->db->order_by('bulan', 'ASC');
        } elseif ($day == null) {
            $this->db->select('DAY(dinsos_gelandanganpsikotik.tanggal) AS tanggal, dinsos_gelandanganpsikotik.jumlah');
            $this->db->from('dinsos_gelandanganpsikotik');
            $this->db->where('YEAR(dinsos_gelandanganpsikotik.tanggal)', $year);
            $this->db->where('MONTH(dinsos_gelandanganpsikotik.tanggal)', $month);
            $this->db->order_by('tanggal', 'ASC');
        } else {
            $this->db->select('DAY(dinsos_gelandanganpsikotik.tanggal) AS tanggal, dinsos_gelandanganpsikotik.jumlah');
            $this->db->from('dinsos_gelandanganpsikotik');
            $this->db->where('YEAR(dinsos_gelandanganpsikotik.tanggal)', $year);
            $this->db->where('MONTH(dinsos_gelandanganpsikotik.tanggal)', $month);
            $this->db->where('DAY(dinsos_gelandanganpsikotik.tanggal)', $day);
            $this->db->order_by('tanggal', 'ASC');
        }
        return $this->db->get()->result();
    }

    //datatable area

    private function _get_datatables_query()
    {

        $column_order = array(
            null,
            'tanggal',
            'jumlah',
            'username'
        );
        $column_search = array(
            'tanggal',
            'jumlah',
            'username'
        );
        $order = array('idgelandanganpsikotik' => 'asc'); // default order

        $this->db->select('dinsos_gelandanganpsikotik.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinsos_gelandanganpsikotik');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_gelandanganpsikotik.idapp_user');
        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinsos_gelandanganpsikotik');
        return $this->db->count_all_results();
    }
}