<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Razia_model extends MY_Model
{
    public $idrazia;
    public $tanggal;
    public $iskota;
    public $jumlah;
    public $latitude;
    public $longitude;
    public $idkategorirazia;
    public $idjeniskelamin;
    public $idkelurahan;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('dinsos_razia');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      dinsos_razia.*,
      kelurahan.*,
      kecamatan.*,
      kota.*,
      propinsi.*,
      app_user.idapp_user,
      app_user.username,
      dinsos_kategorirazia.*,
      jeniskelamin.*
      ');

        $this->db->from('dinsos_razia');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_razia.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_razia.idapp_user');
        $this->db->join('dinsos_kategorirazia', 'dinsos_kategorirazia.idkategorirazia = dinsos_razia.idkategorirazia');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinsos_razia.idjeniskelamin');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idrazia === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idrazia', $this->idrazia);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        if ($this->db->insert('dinsos_razia', $this)) return $this;
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinsos_razia', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('iskota', $this->iskota);
        $this->db->set('jumlah', $this->jumlah);
        $this->db->set('latitude', $this->latitude);
        $this->db->set('longitude', $this->longitude);
        $this->db->set('idkategorirazia', $this->idkategorirazia);
        $this->db->set('idjeniskelamin', $this->idjeniskelamin);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idrazia', $this->idrazia);
        if ($this->db->update('dinsos_razia')) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idrazia', $this->idrazia);
        if ($this->db->delete('dinsos_razia')) return $this;
        return FALSE;
    }

    //mendapatkan semua record hanya tahun dan jumlah
    public function get_based_date($year = null, $month = null, $day = null)
    {
        if ($year == null) {
            $this->db->select('YEAR(dinsos_razia.tanggal) AS tahun, SUM(dinsos_razia.jumlah) as jumlah');
            $this->db->from('dinsos_razia');
            $this->db->group_by('tahun');
            $this->db->order_by('tahun', 'ASC');
        } elseif ($month == null) {
            $this->db->select('MONTH(dinsos_razia.tanggal) AS bulan, SUM(dinsos_razia.jumlah) as jumlah');
            $this->db->from('dinsos_razia');
            $this->db->where('YEAR(dinsos_razia.tanggal)', $year);
            $this->db->group_by('bulan');
            $this->db->order_by('bulan', 'ASC');
        } elseif ($day == null) {
            $this->db->select('DAY(dinsos_razia.tanggal) AS tanggal, SUM(dinsos_razia.jumlah) as jumlah');
            $this->db->from('dinsos_razia');
            $this->db->where('YEAR(dinsos_razia.tanggal)', $year);
            $this->db->where('MONTH(dinsos_razia.tanggal)', $month);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        } else {
            $this->db->select('DAY(dinsos_razia.tanggal) AS tanggal, SUM(dinsos_razia.jumlah) as jumlah');
            $this->db->from('dinsos_razia');
            $this->db->where('YEAR(dinsos_razia.tanggal)', $year);
            $this->db->where('MONTH(dinsos_razia.tanggal)', $month);
            $this->db->where('DAY(dinsos_razia.tanggal)', $day);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        }
        return $this->db->get()->result();
    }

    //mendapatkan jumlah data berdasarkan kelurahan
    public function get_based_kelurahan($year = null, $month = null)
    {
        if (!$year) {
            $this->db->select('
            dinsos_razia.idkelurahan,
            kelurahan.kelurahan,
            SUM(dinsos_razia.jumlah) as jumlah'
            );
            $this->db->from('dinsos_razia');
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_razia.idkelurahan');
            $this->db->group_by('dinsos_razia.idkelurahan, kelurahan.kelurahan');
        } else if (!$month) {
            $this->db->select('
            dinsos_razia.idkelurahan,
            YEAR(dinsos_razia.tanggal) AS tahun,
            kelurahan.kelurahan,
            SUM(dinsos_razia.jumlah) as jumlah'
            );
            $this->db->from('dinsos_razia');
            $this->db->where('YEAR(dinsos_razia.tanggal)', $year);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_razia.idkelurahan');
            $this->db->group_by('dinsos_razia.idkelurahan, tahun, kelurahan.kelurahan');
        } else {
            $this->db->select('
            dinsos_razia.idkelurahan,
            YEAR(dinsos_razia.tanggal) AS tahun,
            MONTH(dinsos_razia.tanggal) AS bulan,
            kelurahan.kelurahan,
            SUM(dinsos_razia.jumlah) as jumlah'
            );
            $this->db->from('dinsos_razia');
            $this->db->where('YEAR(dinsos_razia.tanggal)', $year);
            $this->db->where('MONTH(dinsos_razia.tanggal)', $month);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_razia.idkelurahan');
            $this->db->group_by('dinsos_razia.idkelurahan, tahun, bulan, kelurahan.kelurahan');
        }
        $this->db->order_by('kelurahan.kelurahan', 'ASC');
        return $this->db->get()->result();
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'razia',
            'jeniskelamin',
            'jumlah',
            'iskota',
            'kelurahan',
            'latitude',
            'longitude',
            'username'
        );
        $column_search = array(
            'tanggal',
            'razia',
            'jeniskelamin',
            'jumlah',
            'iskota',
            'kelurahan',
            'latitude',
            'longitude',
            'username'
        );
        $order = array('idrazia' => 'asc'); // default order
        $this->db->select('
      dinsos_razia.*,
      kelurahan.*,
      kecamatan.*,
      kota.*,
      propinsi.*,
      app_user.idapp_user,
      app_user.username,
      dinsos_kategorirazia.*,
      jeniskelamin.*
      ');

        $this->db->from('dinsos_razia');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_razia.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_razia.idapp_user');
        $this->db->join('dinsos_kategorirazia', 'dinsos_kategorirazia.idkategorirazia = dinsos_razia.idkategorirazia');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinsos_razia.idjeniskelamin');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinsos_razia');
        return $this->db->count_all_results();
    }
}
