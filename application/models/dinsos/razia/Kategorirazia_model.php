<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategorirazia_model extends CI_Model {
  public $idkategorirazia;
  public $razia;

  public function __construct()
  {
    $this->load->database();
  }

  public function get(){
      $this->db->select('*');
      $this->db->from('dinsos_kategorirazia');
      if($this->idkategorirazia) $this->db->where('idkategorirazia', $this->idkategorirazia);
      if($this->razia) $this->db->where('razia', $this->razia);
      $query = $this->db->get();
      if($this->idkategorirazia || $this->razia){
          return $query->row();
      }
      return $query->result();
  }
}
