<?php

class Kispbid_model extends MY_Model
{

    public $idkispbid;
    public $tanggal;
    public $nama;
    public $tanggal_lahir;
    public $idkelurahan;
    public $alamat;
    public $nama_ppk;
    public $idapp_user;
    public $systemdate;

    public function __construct()
    {

      parent::__construct('dinsos_kispbid');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dinsos_kispbid.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinsos_kispbid');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbid.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_kispbid.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idkispbid === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idkispbid', $this->idkispbid);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemdate = date('Y-m-d H:i:s');
        if ($this->db->insert('dinsos_kispbid', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinsos_kispbid', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemdate = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('nama', $this->nama);
        $this->db->set('tanggal_lahir', $this->tanggal_lahir);
        $this->db->set('idkecamatan', $this->idkecamatan);
        $this->db->set('alamat', $this->alamat);
        $this->db->set('nama_ppk', $this->nama_ppk);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemdate', $this->systemdate);

        $this->db->where('idkispbid', $this->idkispbid);
        if ($this->db->update('dinsos_kispbid')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idkispbid', $this->idkispbid);
        if ($this->db->delete('dinsos_kispbid')) {
            return $this;
        }
        return FALSE;
    }

    //mendapatkan semua record hanya tahun dan jumlah
    public function get_based_date($year = null, $month = null, $day = null)
    {
        if ($year == null) {
            $this->db->select('YEAR(dinsos_kispbid.tanggal) AS tahun, COUNT(idkispbid) as jumlah');
            $this->db->from('dinsos_kispbid');
            $this->db->group_by('tahun');
            $this->db->order_by('tahun', 'ASC');
        } elseif ($month == null) {
            $this->db->select('MONTH(dinsos_kispbid.tanggal) AS bulan, COUNT(idkispbid) as jumlah');
            $this->db->from('dinsos_kispbid');
            $this->db->where('YEAR(dinsos_kispbid.tanggal)', $year);
            $this->db->group_by('bulan');
            $this->db->order_by('bulan', 'ASC');
        } elseif ($day == null) {
            $this->db->select('DAY(dinsos_kispbid.tanggal) AS tanggal, SUM(dinsos_kispbid.jumlah) as jumlah');
            $this->db->from('dinsos_kispbid');
            $this->db->where('YEAR(dinsos_kispbid.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kispbid.tanggal)', $month);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        } else {
            $this->db->select('DAY(dinsos_kispbid.tanggal) AS tanggal, SUM(dinsos_kispbid.jumlah) as jumlah');
            $this->db->from('dinsos_kispbid');
            $this->db->where('YEAR(dinsos_kispbid.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kispbid.tanggal)', $month);
            $this->db->where('DAY(dinsos_kispbid.tanggal)', $day);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        }
        return $this->db->get()->result();
    }

    //mendapatkan jumlah data berdasarkan kelurahan
    public function get_based_kelurahan($year = null, $month = null)
    {
        if (!$year) {
            $this->db->select('
            dinsos_kispbid.idkelurahan,
            kelurahan.kelurahan,
            COUNT(dinsos_kispbid.idkispbid) as jumlah'
            );
            $this->db->from('dinsos_kispbid');
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbid.idkelurahan');
            $this->db->group_by('dinsos_kispbid.idkelurahan, kelurahan.kelurahan');
        } else if (!$month) {
            $this->db->select('
            dinsos_kispbid.idkelurahan,
            YEAR(dinsos_kispbid.tanggal) AS tahun,
            kelurahan.kelurahan,
            COUNT(dinsos_kispbid.idkispbid) as jumlah'
            );
            $this->db->from('dinsos_kispbid');
            $this->db->where('YEAR(dinsos_kispbid.tanggal)', $year);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbid.idkelurahan');
            $this->db->group_by('dinsos_kispbid.idkelurahan, tahun, kelurahan.kelurahan');
        } else {
            $this->db->select('
            dinsos_kispbid.idkelurahan,
            YEAR(dinsos_kispbid.tanggal) AS tahun,
            MONTH(dinsos_kispbid.tanggal) AS bulan,
            kelurahan.kelurahan,
            COUNT(dinsos_kispbid.idkispbid) as jumlah'
            );
            $this->db->from('dinsos_kispbid');
            $this->db->where('YEAR(dinsos_kispbid.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kispbid.tanggal)', $month);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbid.idkelurahan');
            $this->db->group_by('dinsos_kispbid.idkelurahan, tahun, bulan, kelurahan.kelurahan');
        }
        $this->db->order_by('kelurahan.kelurahan', 'ASC');
        return $this->db->get()->result();
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'nama',
            'tanggal_lahir',
            'nama_ppk',
            'alamat',
            'kelurahan',
            'username'
        );
        $column_search = array(
            'tanggal',
            'nama',
            'tanggal_lahir',
            'nama_ppk',
            'alamat',
            'kelurahan',
            'username'
        );

        $order = array('idkispbid' => 'asc'); // default order

        $this->db->select('dinsos_kispbid.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinsos_kispbid');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbid.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_kispbid.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinsos_kispbid');
        return $this->db->count_all_results();
    }
}
