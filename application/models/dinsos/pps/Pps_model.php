<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PPS_model extends MY_Model
{
    public $iddinsos_pps;
    public $idkelurahan;
    public $tanggal;
    public $alamat;
    public $no_kk;
    public $no_nik;
    public $nama;
    public $idhubungankeluarga;
    public $idjeniskelamin;
    public $tanggal_lahir;
    public $idstatuskawin;
    public $idpendidikan;
    public $idstatuskeluarga;
    public $idstatusindividu;
    public $systemtime;
    public $idapp_user;


    public function __construct()
    {
      parent::__construct('dinsos_pps');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      dinsos_pps.*,
      kelurahan.*,
      kecamatan.*,
      kota.*,
      propinsi.*,
      pendidikan.*,
      statuskawin.*,
      jeniskelamin.*,
      hubungankeluarga.*,
      statusindividu.*,
      statuskeluarga.*,
         app_user.username,
         app_user.idapp_user
         ');

        $this->db->from('dinsos_pps');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pps.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('pendidikan', 'pendidikan.idpendidikan = dinsos_pps.idpendidikan');
        $this->db->join('statuskawin', 'statuskawin.idstatuskawin = dinsos_pps.idstatuskawin');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinsos_pps.idjeniskelamin');
        $this->db->join('hubungankeluarga', 'hubungankeluarga.idhubungankeluarga = dinsos_pps.idhubungankeluarga');
        $this->db->join('statusindividu', 'statusindividu.idstatusindividu = dinsos_pps.idstatusindividu');
        $this->db->join('statuskeluarga', 'statuskeluarga.idstatuskeluarga = dinsos_pps.idstatuskeluarga');
         $this->db->join('app_user', 'app_user.idapp_user = dinsos_pps.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->iddinsos_pps === null) {
            return $this->db->get()->result();
        }
        $this->db->where('iddinsos_pps', $this->iddinsos_pps);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dinsos_pps', $this)) return $this;
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinsos_pps', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('alamat', $this->alamat);
        $this->db->set('no_kk', $this->no_kk);
        $this->db->set('no_nik', $this->no_nik);
        $this->db->set('nama', $this->nama);
        $this->db->set('idhubungankeluarga', $this->idhubungankeluarga);
        $this->db->set('idjeniskelamin', $this->idjeniskelamin);
        $this->db->set('tanggal_lahir', $this->tanggal_lahir);
        $this->db->set('idstatuskawin', $this->idstatuskawin);
        $this->db->set('idpendidikan', $this->idpendidikan);
        $this->db->set('idstatuskeluarga', $this->idstatuskeluarga);
        $this->db->set('idstatusindividu', $this->idstatusindividu);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->where('iddinsos_pps', $this->iddinsos_pps);
        if ($this->db->update('dinsos_pps')) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('iddinsos_pps', $this->iddinsos_pps);
        if ($this->db->delete('dinsos_pps')) return $this;
        return FALSE;
    }

    //mendapatkan semua record hanya tahun dan jumlah
    public function get_based_date($year = null, $month = null, $day = null)
    {
        if ($year == null) {
            $this->db->select('YEAR(dinsos_pps.tanggal) AS tahun, COUNT(iddinsos_pps) as jumlah');
            $this->db->from('dinsos_pps');
            $this->db->group_by('tahun');
            $this->db->order_by('tahun', 'ASC');
        } elseif ($month == null) {
            $this->db->select('MONTH(dinsos_pps.tanggal) AS bulan, COUNT(iddinsos_pps) as jumlah');
            $this->db->from('dinsos_pps');
            $this->db->where('YEAR(dinsos_pps.tanggal)', $year);
            $this->db->group_by('bulan');
            $this->db->order_by('bulan', 'ASC');
        } elseif ($day == null) {
            $this->db->select('DAY(dinsos_pps.tanggal) AS tanggal, COUNT(iddinsos_pps) as jumlah');
            $this->db->from('dinsos_pps');
            $this->db->where('YEAR(dinsos_pps.tanggal)', $year);
            $this->db->where('MONTH(dinsos_pps.tanggal)', $month);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        } else {
            $this->db->select('DAY(dinsos_pps.tanggal) AS tanggal, COUNT(iddinsos_pps) as jumlah');
            $this->db->from('dinsos_pps');
            $this->db->where('YEAR(dinsos_pps.tanggal)', $year);
            $this->db->where('MONTH(dinsos_pps.tanggal)', $month);
            $this->db->where('DAY(dinsos_pps.tanggal)', $day);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        }
        return $this->db->get()->result();
    }

    //mendapatkan jumlah data berdasarkan kelurahan
    public function get_based_kelurahan($year = null, $month = null)
    {
        if (!$year) {
            $this->db->select('
            dinsos_pps.idkelurahan,
            kelurahan.kelurahan,
            COUNT(dinsos_pps.iddinsos_pps) as jumlah'
            );
            $this->db->from('dinsos_pps');
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pps.idkelurahan');
            $this->db->group_by('dinsos_pps.idkelurahan, kelurahan.kelurahan');
        } else if (!$month) {
            $this->db->select('
            dinsos_pps.idkelurahan,
            YEAR(dinsos_pps.tanggal) AS tahun,
            kelurahan.kelurahan,
            COUNT(dinsos_pps.iddinsos_pps) as jumlah'
            );
            $this->db->from('dinsos_pps');
            $this->db->where('YEAR(dinsos_pps.tanggal)', $year);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pps.idkelurahan');
            $this->db->group_by('dinsos_pps.idkelurahan, tahun, kelurahan.kelurahan');
        } else {
            $this->db->select('
            dinsos_pps.idkelurahan,
            YEAR(dinsos_pps.tanggal) AS tahun,
            MONTH(dinsos_pps.tanggal) AS bulan,
            kelurahan.kelurahan,
            COUNT(dinsos_pps.iddinsos_pps) as jumlah'
            );
            $this->db->from('dinsos_pps');
            $this->db->where('YEAR(dinsos_pps.tanggal)', $year);
            $this->db->where('MONTH(dinsos_pps.tanggal)', $month);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pps.idkelurahan');
            $this->db->group_by('dinsos_pps.idkelurahan, tahun, bulan, kelurahan.kelurahan');
        }
        $this->db->order_by('kelurahan.kelurahan', 'ASC');
        return $this->db->get()->result();
    }

    //datatable area


    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'no_kk',
            'no_nik',
            'username',
            'nama',
            'hubungankeluarga',
            'jeniskelamin',
            'tanggal_lahir',
            'pendidikan',
            'alamat',
            'kelurahan',
            'status',
            'statuskeluarga',
            'statusindividu',
            'tanggal'
        );
        $column_search = array(
            'no_kk',
            'no_nik',
            'username',
            'nama',
            'hubungankeluarga',
            'jeniskelamin',
            'tanggal_lahir',
            'pendidikan',
            'alamat',
            'kelurahan',
            'status',
            'statuskeluarga',
            'statusindividu',
            'tanggal'
        );

        $order = array('idkispbin' => 'asc'); // default order

        $this->db->select('
          dinsos_pps.*,
          kelurahan.*,
          kecamatan.*,
          kota.*,
          propinsi.*,
          pendidikan.*,
          statuskawin.*,
          jeniskelamin.*,
          hubungankeluarga.*,
          statusindividu.*,
          statuskeluarga.*,
         app_user.username,
         app_user.idapp_user
         ');

        $this->db->from('dinsos_pps');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pps.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('pendidikan', 'pendidikan.idpendidikan = dinsos_pps.idpendidikan');
        $this->db->join('statuskawin', 'statuskawin.idstatuskawin = dinsos_pps.idstatuskawin');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinsos_pps.idjeniskelamin');
        $this->db->join('hubungankeluarga', 'hubungankeluarga.idhubungankeluarga = dinsos_pps.idhubungankeluarga');
        $this->db->join('statusindividu', 'statusindividu.idstatusindividu = dinsos_pps.idstatusindividu');
        $this->db->join('statuskeluarga', 'statuskeluarga.idstatuskeluarga = dinsos_pps.idstatuskeluarga');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_pps.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinsos_pps');
        return $this->db->count_all_results();
    }
}
