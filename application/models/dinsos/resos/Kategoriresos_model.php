<?php
class Kategoriresos_model extends CI_Model {

    public $idkategoriresos;
    public $kategori;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        $this->db->select('*');
        $this->db->from('dinsos_kategoriresos');
        if($this->idkategoriresos) $this->db->where('idkategoriresos', $this->idkategoriresos);
        if($this->kategori) $this->db->where('kategori', $this->kategori);
        $query = $this->db->get();
        if($this->idkategoriresos || $this->kategori){
            return $query->row();
        }
        return $query->result();
    }

    public function add(){
        if($this->db->insert('dinsos_kategoriresos', $this)){
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('kategori', $this->kategori);
        $this->db->where('idkategoriresos', $this->idkategoriresos);
        if($this->db->update('dinsos_kategoriresos')){
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idkategoriresos', $this->idkategoriresos);
        if($this->db->delete('dinsos_resos')){
            $this->db->where('idkategoriresos', $this->idkategoriresos);
            if($this->db->delete('dinsos_kategoriresos')){
                return $this;
            }
        }
        return FALSE;
    }
}