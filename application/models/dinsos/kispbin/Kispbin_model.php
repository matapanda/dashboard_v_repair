<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kispbin_model extends MY_Model
{
    public $idkispbin;
    public $tanggal;
    public $alamat;
    public $idkelurahan;
    public $no_kk;
    public $nik;
    public $nama_anggota_keluarga;
    public $tanggal_lahir;
    public $status_lama;
    public $status_sekarang;
    public $status_keterangan;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('dinsos_kispbin');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
          dinsos_kispbin.* ,
          kelurahan.*,
          kecamatan.*,
          kota.*,
          propinsi.*,
          app_user.idapp_user,
          app_user.username,
          sk_status_lama.status as status_status_lama,
          sk_status_lama.idstatuskis as idstatus_lama,
          sk_status_sekarang.status as status_status_sekarang,
          sk_status_sekarang.idstatuskis as idstatus_sekarang,
          sk_status_keterangan.status as status_status_keterangan,
          sk_status_keterangan.idstatuskis as idstatus_keterangan
        ');
        $this->db->from('dinsos_kispbin');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbin.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_kispbin.idapp_user');
        $this->db->join('statuskis sk_status_lama', 'sk_status_lama.idstatuskis = dinsos_kispbin.status_lama');
        $this->db->join('statuskis sk_status_sekarang', 'sk_status_sekarang.idstatuskis = dinsos_kispbin.status_sekarang');
        $this->db->join('statuskis sk_status_keterangan', 'sk_status_keterangan.idstatuskis = dinsos_kispbin.status_keterangan');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idkispbin === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idkispbin', $this->idkispbin);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dinsos_kispbin', $this)) return $this;
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinsos_kispbin', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('alamat', $this->alamat);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('no_kk', $this->no_kk);
        $this->db->set('nik', $this->nik);
        $this->db->set('nama_anggota_keluarga', $this->nama_anggota_keluarga);
        $this->db->set('tanggal_lahir', $this->tanggal_lahir);
        $this->db->set('status_lama', $this->status_lama);
        $this->db->set('status_sekarang', $this->status_sekarang);
        $this->db->set('status_keterangan', $this->status_keterangan);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idkispbin', $this->idkispbin);
        if ($this->db->update('dinsos_kispbin')) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idkispbin', $this->idkispbin);
        if ($this->db->delete('dinsos_kispbin')) return $this;
        return FALSE;
    }

    //mendapatkan semua record hanya tahun dan jumlah
    public function get_based_date($year = null, $month = null, $day = null)
    {
        if ($year == null) {
            $this->db->select('YEAR(dinsos_kispbin.tanggal) AS tahun, COUNT(idkispbin) as jumlah');
            $this->db->from('dinsos_kispbin');
            $this->db->group_by('tahun');
            $this->db->order_by('tahun', 'ASC');
        } elseif ($month == null) {
            $this->db->select('MONTH(dinsos_kispbin.tanggal) AS bulan, COUNT(idkispbin) as jumlah');
            $this->db->from('dinsos_kispbin');
            $this->db->where('YEAR(dinsos_kispbin.tanggal)', $year);
            $this->db->group_by('bulan');
            $this->db->order_by('bulan', 'ASC');
        } elseif ($day == null) {
            $this->db->select('DAY(dinsos_kispbin.tanggal) AS tanggal, SUM(dinsos_kispbin.jumlah) as jumlah');
            $this->db->from('dinsos_kispbin');
            $this->db->where('YEAR(dinsos_kispbin.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kispbin.tanggal)', $month);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        } else {
            $this->db->select('DAY(dinsos_kispbin.tanggal) AS tanggal, SUM(dinsos_kispbin.jumlah) as jumlah');
            $this->db->from('dinsos_kispbin');
            $this->db->where('YEAR(dinsos_kispbin.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kispbin.tanggal)', $month);
            $this->db->where('DAY(dinsos_kispbin.tanggal)', $day);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        }
        return $this->db->get()->result();
    }

    //mendapatkan jumlah data berdasarkan kelurahan
    public function get_based_kelurahan($year = null, $month = null)
    {
        if (!$year) {
            $this->db->select('
            dinsos_kispbin.idkelurahan,
            kelurahan.kelurahan,
            COUNT(dinsos_kispbin.idkispbin) as jumlah'
            );
            $this->db->from('dinsos_kispbin');
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbin.idkelurahan');
            $this->db->group_by('dinsos_kispbin.idkelurahan, kelurahan.kelurahan');
        } else if (!$month) {
            $this->db->select('
            dinsos_kispbin.idkelurahan,
            YEAR(dinsos_kispbin.tanggal) AS tahun,
            kelurahan.kelurahan,
            COUNT(dinsos_kispbin.idkispbin) as jumlah'
            );
            $this->db->from('dinsos_kispbin');
            $this->db->where('YEAR(dinsos_kispbin.tanggal)', $year);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbin.idkelurahan');
            $this->db->group_by('dinsos_kispbin.idkelurahan, tahun, kelurahan.kelurahan');
        } else {
            $this->db->select('
            dinsos_kispbin.idkelurahan,
            YEAR(dinsos_kispbin.tanggal) AS tahun,
            MONTH(dinsos_kispbin.tanggal) AS bulan,
            kelurahan.kelurahan,
            COUNT(dinsos_kispbin.idkispbin) as jumlah'
            );
            $this->db->from('dinsos_kispbin');
            $this->db->where('YEAR(dinsos_kispbin.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kispbin.tanggal)', $month);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbin.idkelurahan');
            $this->db->group_by('dinsos_kispbin.idkelurahan, tahun, bulan, kelurahan.kelurahan');
        }
        $this->db->order_by('kelurahan.kelurahan', 'ASC');
        return $this->db->get()->result();
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(null, 'no_kk', 'nik', 'nama_anggota_keluarga', 'tanggal_lahir', 'alamat', 'kelurahan', 'status_status_lama', 'status_status_sekarang', 'status_status_keterangan', 'tanggal', 'username'); //set column field database for datatable orderable
        $column_search = array('no_kk', 'nik', 'nama_anggota_keluarga', 'tanggal_lahir', 'alamat', 'kelurahan', 'sk_status_lama.status', 'sk_status_sekarang.status', 'sk_status_keterangan.status', 'tanggal', 'username'); //set column field database for datatable searchable
        $order = array('idkispbin' => 'asc'); // default order

        $this->db->select('
          dinsos_kispbin.* ,
          kelurahan.*,
          kecamatan.*,
          kota.*,
          propinsi.*,
          app_user.idapp_user,
          app_user.username,
          sk_status_lama.status as status_status_lama,
          sk_status_lama.idstatuskis as idstatus_lama,
          sk_status_sekarang.status as status_status_sekarang,
          sk_status_sekarang.idstatuskis as idstatus_sekarang,
          sk_status_keterangan.status as status_status_keterangan,
          sk_status_keterangan.idstatuskis as idstatus_keterangan
        ');
        $this->db->from('dinsos_kispbin');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kispbin.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_kispbin.idapp_user');
        $this->db->join('statuskis sk_status_lama', 'sk_status_lama.idstatuskis = dinsos_kispbin.status_lama');
        $this->db->join('statuskis sk_status_sekarang', 'sk_status_sekarang.idstatuskis = dinsos_kispbin.status_sekarang');
        $this->db->join('statuskis sk_status_keterangan', 'sk_status_keterangan.idstatuskis = dinsos_kispbin.status_keterangan');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinsos_kispbin');
        return $this->db->count_all_results();
    }
}
