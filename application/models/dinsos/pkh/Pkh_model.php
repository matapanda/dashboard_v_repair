<?php

class Pkh_model extends MY_Model
{

    public $idpkh;
    public $tanggal;
    public $idkelurahan;
    public $idkategori;
    public $jumlah;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('dinsos_pkh');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {

        $this->db->select('dinsos_pkh.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dinsos_kategoripkh.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinsos_pkh');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinsos_kategoripkh', 'dinsos_kategoripkh.idkategori = dinsos_pkh.idkategori');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_pkh.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idpkh === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idpkh', $this->idpkh);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        if ($this->db->insert('dinsos_pkh', $this)) {
            return TRUE;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinsos_pkh', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('idkategori', $this->idkategori);
        $this->db->set('jumlah', $this->jumlah);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idpkh', $this->idpkh);
        if ($this->db->update('dinsos_pkh')) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idpkh', $this->idpkh);
        if ($this->db->delete('dinsos_pkh')) {
            return TRUE;
        }
        return FALSE;
    }

    //mendapatkan semua record hanya tahun dan jumlah
    public function get_based_date($year = null, $month = null, $day = null)
    {
        if ($year == null) {
            $this->db->select('YEAR(dinsos_pkh.tanggal) AS tahun, SUM(dinsos_pkh.jumlah) as jumlah');
            $this->db->from('dinsos_pkh');
            $this->db->group_by('tahun');
            $this->db->order_by('tahun', 'ASC');
        } elseif ($month == null) {
            $this->db->select('MONTH(dinsos_pkh.tanggal) AS bulan, SUM(dinsos_pkh.jumlah) as jumlah');
            $this->db->from('dinsos_pkh');
            $this->db->where('YEAR(dinsos_pkh.tanggal)', $year);
            $this->db->group_by('bulan');
            $this->db->order_by('bulan', 'ASC');
        } elseif ($day == null) {
            $this->db->select('DAY(dinsos_pkh.tanggal) AS tanggal, SUM(dinsos_pkh.jumlah) as jumlah');
            $this->db->from('dinsos_pkh');
            $this->db->where('YEAR(dinsos_pkh.tanggal)', $year);
            $this->db->where('MONTH(dinsos_pkh.tanggal)', $month);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        } else {
            $this->db->select('DAY(dinsos_pkh.tanggal) AS tanggal, SUM(dinsos_pkh.jumlah) as jumlah');
            $this->db->from('dinsos_pkh');
            $this->db->where('YEAR(dinsos_pkh.tanggal)', $year);
            $this->db->where('MONTH(dinsos_pkh.tanggal)', $month);
            $this->db->where('DAY(dinsos_pkh.tanggal)', $day);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        }
        return $this->db->get()->result();
    }

    //mendapatkan jumlah data berdasarkan kelurahan
    public function get_based_kelurahan($year = null, $month = null)
    {
        if (!$year) {
            $this->db->select('
            dinsos_pkh.idkelurahan,
            kelurahan.kelurahan,
            SUM(dinsos_pkh.jumlah) as jumlah'
            );
            $this->db->from('dinsos_pkh');
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
            $this->db->group_by('dinsos_pkh.idkelurahan, kelurahan.kelurahan');
        } else if (!$month) {
            $this->db->select('
            dinsos_pkh.idkelurahan,
            YEAR(dinsos_pkh.tanggal) AS tahun,
            kelurahan.kelurahan,
            SUM(dinsos_pkh.jumlah) as jumlah'
            );
            $this->db->from('dinsos_pkh');
            $this->db->where('YEAR(dinsos_pkh.tanggal)', $year);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
            $this->db->group_by('dinsos_pkh.idkelurahan, tahun, kelurahan.kelurahan');
        } else {
            $this->db->select('
            dinsos_pkh.idkelurahan,
            YEAR(dinsos_pkh.tanggal) AS tahun,
            MONTH(dinsos_pkh.tanggal) AS bulan,
            kelurahan.kelurahan,
            SUM(dinsos_pkh.jumlah) as jumlah'
            );
            $this->db->from('dinsos_pkh');
            $this->db->where('YEAR(dinsos_pkh.tanggal)', $year);
            $this->db->where('MONTH(dinsos_pkh.tanggal)', $month);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
            $this->db->group_by('dinsos_pkh.idkelurahan, tahun, bulan, kelurahan.kelurahan');
        }
        $this->db->order_by('kelurahan.kelurahan', 'ASC');
        return $this->db->get()->result();
    }

    function get_based_kategori($year = null, $month = null, $based_kelurahan = FALSE)
    {
        if (!$year) {
            if ($based_kelurahan) {
                $this->db->select('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, dinsos_pkh.idkelurahan, kelurahan.kelurahan, SUM(dinsos_pkh.jumlah) as jumlah');
            } else {
                $this->db->select('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, SUM(dinsos_pkh.jumlah) as jumlah');
            }
            $this->db->from('dinsos_pkh');
            $this->db->join('dinsos_kategoripkh', 'dinsos_kategoripkh.idkategori = dinsos_pkh.idkategori');
            if ($based_kelurahan) {
                $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
                $this->db->group_by('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, dinsos_pkh.idkelurahan, kelurahan.kelurahan');
            } else {
                $this->db->group_by('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori');
                $this->db->order_by('kategori', 'ASC');
            }
        } elseif (!$month) {
            if ($based_kelurahan) {
                $this->db->select('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, YEAR(dinsos_pkh.tanggal) AS tahun, dinsos_pkh.idkelurahan, kelurahan.kelurahan, SUM(dinsos_pkh.jumlah) as jumlah');
            } else {
                $this->db->select('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, YEAR(dinsos_pkh.tanggal) as tahun, SUM(dinsos_pkh.jumlah) as jumlah');
            }
            $this->db->from('dinsos_pkh');
            $this->db->where('YEAR(dinsos_pkh.tanggal)', $year);
            $this->db->join('dinsos_kategoripkh', 'dinsos_kategoripkh.idkategori = dinsos_pkh.idkategori');
            if ($based_kelurahan) {
                $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
                $this->db->group_by('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, tahun, dinsos_pkh.idkelurahan, kelurahan.kelurahan');
            } else {
                $this->db->group_by('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, tahun');
                $this->db->order_by('kategori', 'ASC');
            }
        } else {
            if ($based_kelurahan) {
                $this->db->select('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, MONTH(dinsos_pkh.tanggal) AS bulan, dinsos_pkh.idkelurahan, kelurahan.kelurahan, SUM(dinsos_pkh.jumlah) as jumlah');
            } else {
                $this->db->select('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, MONTH(dinsos_pkh.tanggal) as bulan, SUM(dinsos_pkh.jumlah) as jumlah');
            }
            $this->db->from('dinsos_pkh');
            $this->db->where('YEAR(dinsos_pkh.tanggal)', $year);
            $this->db->where('MONTH(dinsos_pkh.tanggal)', $month);
            $this->db->join('dinsos_kategoripkh', 'dinsos_kategoripkh.idkategori = dinsos_pkh.idkategori');
            if ($based_kelurahan) {
                $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
                $this->db->group_by('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, bulan, dinsos_pkh.idkelurahan, kelurahan.kelurahan');
            } else {
                $this->db->group_by('dinsos_pkh.idkategori, dinsos_kategoripkh.kategori, bulan');
                $this->db->order_by('kategori', 'ASC');
            }
        }
        if ($based_kelurahan) {
            $this->db->order_by('kategori', 'ASC');
        }
        return $this->db->get()->result();
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'kategori',
            'jumlah',
            'kelurahan',
            'username'
        );
        $column_search = array(
            'tanggal',
            'kategori',
            'jumlah',
            'kelurahan',
            'username'
        );
        $order = array('idpkh' => 'asc'); // default order

        $this->db->select('dinsos_pkh.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dinsos_kategoripkh.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinsos_pkh');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_pkh.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinsos_kategoripkh', 'dinsos_kategoripkh.idkategori = dinsos_pkh.idkategori');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_pkh.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinsos_pkh');
        return $this->db->count_all_results();
    }
}
