<?php
class Kategoripkh_model extends MY_Model
{

    public $idkategori;
    public $kategori;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        $this->db->select('*');
        $this->db->from('dinsos_kategoripkh');
        if($this->idkategori) $this->db->where('idkategori', $this->idkategori);
        if($this->kategori) $this->db->where('kategori', $this->kategori);
        $query = $this->db->get();
        if($this->idkategori || $this->kategori){
            return $query->row();
        }
        return $query->result();
    }

    public function add(){
        if($this->db->insert('dinsos_kategoripkh', $this)){
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('kategori', $this->kategori);
        $this->db->where('idkategori', $this->idkategori);
        if($this->db->update('dinsos_kategoripkh')){
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idkategori', $this->idkategori);
        if($this->db->delete('dinsos_pkh')){
            $this->db->where('idkategori', $this->idkategori);
            if($this->db->delete('dinsos_kategoripkh')){
                return $this;
            }
        }
        return FALSE;
    }
}
