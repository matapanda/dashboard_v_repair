<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kks_model extends MY_Model
{
    public $idkks;
    public $tanggal;
    public $idkelurahan;
    public $lagitude;
    public $latitude;
    public $no_kks;
    public $alamat;
    public $krt;
    public $pkrt;
    public $artl;
    public $jumlah_art;
    public $is_kks;
    public $is_pbdt;
    public $no_pbdt;
    public $no_pkh;
    public $is_batal_kks;
    public $is_pengajuan_pkh;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('dinsos_kks');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      dinsos_kks.*,
      kelurahan.*,
      kecamatan.*,
      kota.*,
      propinsi.*,
      app_user.idapp_user,
      app_user.username
      ');
        $this->db->from('dinsos_kks');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kks.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_kks.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idkks === null) {
            return $this->db->get()->result();
        }
        $this->db->where('dinsos_kks', $this->dinsos_kks);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dinsos_kks', $this)) return $this;
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinsos_kks', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('lagitude', $this->lagitude);
        $this->db->set('latitude', $this->latitude);
        $this->db->set('no_kks', $this->no_kks);
        $this->db->set('alamat', $this->alamat);
        $this->db->set('krt', $this->krt);
        $this->db->set('pkrt', $this->pkrt);
        $this->db->set('artl', $this->artl);
        $this->db->set('jumlah_art', $this->jumlah_art);
        $this->db->set('is_pkh', $this->is_pkh);
        $this->db->set('is_pbdt', $this->is_pbdt);
        $this->db->set('no_pbdt', $this->no_pbdt);
        $this->db->set('no_pkh', $this->no_pkh);
        $this->db->set('is_batal_kks', $this->is_batal_kks);
        $this->db->set('is_pengajuan_pkh', $this->is_pengajuan_pkh);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idkks', $this->idkks);
        if ($this->db->update('dinsos_kks')) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idkks', $this->idkks);
        if ($this->db->delete('dinsos_kks')) return $this;
        return FALSE;
    }

    //mendapatkan semua record hanya tahun dan jumlah
    public function get_based_date($year = null, $month = null, $day = null)
    {
        if ($year == null) {
            $this->db->select('YEAR(dinsos_kks.tanggal) AS tahun, COUNT(idkks) as jumlah');
            $this->db->from('dinsos_kks');
            $this->db->group_by('tahun');
            $this->db->order_by('tahun', 'ASC');
        } elseif ($month == null) {
            $this->db->select('MONTH(dinsos_kks.tanggal) AS bulan, COUNT(idkks) as jumlah');
            $this->db->from('dinsos_kks');
            $this->db->where('YEAR(dinsos_kks.tanggal)', $year);
            $this->db->group_by('bulan');
            $this->db->order_by('bulan', 'ASC');
        } elseif ($day == null) {
            $this->db->select('DAY(dinsos_kks.tanggal) AS tanggal, SUM(dinsos_kks.jumlah) as jumlah');
            $this->db->from('dinsos_kks');
            $this->db->where('YEAR(dinsos_kks.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kks.tanggal)', $month);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        } else {
            $this->db->select('DAY(dinsos_kks.tanggal) AS tanggal, SUM(dinsos_kks.jumlah) as jumlah');
            $this->db->from('dinsos_kks');
            $this->db->where('YEAR(dinsos_kks.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kks.tanggal)', $month);
            $this->db->where('DAY(dinsos_kks.tanggal)', $day);
            $this->db->group_by('tanggal');
            $this->db->order_by('tanggal', 'ASC');
        }
        return $this->db->get()->result();
    }

    //mendapatkan jumlah data berdasarkan kelurahan
    public function get_based_kelurahan($year = null, $month = null)
    {
        if (!$year) {
            $this->db->select('
            dinsos_kks.idkelurahan,
            kelurahan.kelurahan,
            COUNT(dinsos_kks.idkks) as jumlah'
            );
            $this->db->from('dinsos_kks');
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kks.idkelurahan');
            $this->db->group_by('dinsos_kks.idkelurahan, kelurahan.kelurahan');
        } else if (!$month) {
            $this->db->select('
            dinsos_kks.idkelurahan,
            YEAR(dinsos_kks.tanggal) AS tahun,
            kelurahan.kelurahan,
            COUNT(dinsos_kks.idkks) as jumlah'
            );
            $this->db->from('dinsos_kks');
            $this->db->where('YEAR(dinsos_kks.tanggal)', $year);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kks.idkelurahan');
            $this->db->group_by('dinsos_kks.idkelurahan, tahun, kelurahan.kelurahan');
        } else {
            $this->db->select('
            dinsos_kks.idkelurahan,
            YEAR(dinsos_kks.tanggal) AS tahun,
            MONTH(dinsos_kks.tanggal) AS bulan,
            kelurahan.kelurahan,
            COUNT(dinsos_kks.idkks) as jumlah'
            );
            $this->db->from('dinsos_kks');
            $this->db->where('YEAR(dinsos_kks.tanggal)', $year);
            $this->db->where('MONTH(dinsos_kks.tanggal)', $month);
            $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kks.idkelurahan');
            $this->db->group_by('dinsos_kks.idkelurahan, tahun, bulan, kelurahan.kelurahan');
        }
        $this->db->order_by('kelurahan.kelurahan', 'ASC');
        return $this->db->get()->result();
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'no_kks',
            'tanggal',
            'kelurahan',
            'lagitude',
            'latitude',
            'pengurus',
            'alamat',
            'krt',
            'pkrt',
            'artl',
            'jumlah_art',
            'is_pkh',
            'is_pbdt',
            'no_pkh',
            'no_pbdt',
            'is_batal_kks',
            'is_pengajuan_pkh',
            'username'
        );
        $column_search = array(
            'no_kks',
            'tanggal',
            'kelurahan',
            'lagitude',
            'latitude',
            'pengurus',
            'alamat',
            'krt',
            'pkrt',
            'artl',
            'jumlah_art',
            'is_pkh',
            'is_pbdt',
            'no_pkh',
            'no_pbdt',
            'is_batal_kks',
            'is_pengajuan_pkh',
            'username'
        );

        $order = array('idkks' => 'asc'); // default order

        $this->db->select('
      dinsos_kks.*,
      kelurahan.*,
      kecamatan.*,
      kota.*,
      propinsi.*,
      app_user.idapp_user,
      app_user.username
      ');
        $this->db->from('dinsos_kks');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinsos_kks.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dinsos_kks.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinsos_kks');
        return $this->db->count_all_results();
    }
}
