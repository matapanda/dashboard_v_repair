<?php

class Unitkerja_model extends CI_Model
{
    public $idunitkerja;
    public $unitkerja;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idunitkerja === null){
            return $this->db->get('bkd_unitkerja')->result();
        }
        $this->db->where('idunitkerja', $this->idunitkerja);
        return $this->db->get('bkd_unitkerja')->row();
    }
}