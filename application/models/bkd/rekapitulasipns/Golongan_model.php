<?php

class Golongan_model extends CI_Model
{
    public $idgolongan;
    public $golongan;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idgolongan === null){
            return $this->db->get('bkd_golongan')->result();
        }
        $this->db->where('idgolongan', $this->idgolongan);
        return $this->db->get('bkd_golongan')->row();
    }
}