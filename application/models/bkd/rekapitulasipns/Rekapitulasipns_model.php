<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasipns_model extends MY_Model
{
    public $idrekapitulasipns;
    public $tanggal;
    public $idpendidikan;
    public $idjeniskelamin;
    public $idstatuskawin;
    public $idunitkerja;
    public $ideselon;
    public $idagama;
    public $idgolongan;
    public $idapp_user;
    public $systemtime;
    public $jumlah;

    public function __construct()
    {
        parent::__construct('bkd_rekapitulasipns');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      bkd_rekapitulasipns.* ,
      pendidikan.*,
      jeniskelamin.*,
      statuskawin.*,
      bkd_unitkerja.*,
      bkd_eselon.*,
      bkd_agama.*,
      bkd_golongan.*,
      app_user.idapp_user,
      app_user.username
    ');
        $this->db->from('bkd_rekapitulasipns');
        $this->db->join('pendidikan', 'pendidikan.idpendidikan = bkd_rekapitulasipns.idpendidikan');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = bkd_rekapitulasipns.idjeniskelamin');
        $this->db->join('statuskawin', 'statuskawin.idstatuskawin = bkd_rekapitulasipns.idstatuskawin');
        $this->db->join('bkd_unitkerja', 'bkd_unitkerja.idunitkerja = bkd_rekapitulasipns.idunitkerja');
        $this->db->join('bkd_eselon', 'bkd_eselon.ideselon = bkd_rekapitulasipns.ideselon');
        $this->db->join('bkd_agama', 'bkd_agama.idagama = bkd_rekapitulasipns.idagama');
        $this->db->join('bkd_golongan', 'bkd_golongan.idgolongan = bkd_rekapitulasipns.idgolongan');
        $this->db->join('app_user', 'app_user.idapp_user = bkd_rekapitulasipns.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idrekapitulasipns === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idrekapitulasipns', $this->idrekapitulasipns);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('bkd_rekapitulasipns', $this)) return $this;
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('bkd_rekapitulasipns', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idpendidikan', $this->idpendidikan);
        $this->db->set('idjeniskelamin', $this->idjeniskelamin);
        $this->db->set('idstatuskawin', $this->idstatuskawin);
        $this->db->set('idunitkerja', $this->idunitkerja);
        $this->db->set('ideselon', $this->ideselon);
        $this->db->set('idagama', $this->idagama);
        $this->db->set('idgolongan', $this->idgolongan);
        $this->db->set('jumlah', $this->jumlah);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->where('idrekapitulasipns', $this->idrekapitulasipns);
        if ($this->db->update('bkd_rekapitulasipns')) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idunitkerja', $this->idunitkerja);
        $this->db->where('tanggal', $this->tanggal);
        if ($this->db->delete('bkd_rekapitulasipns')) return $this;
        return FALSE;
    }

    private function do_query()
    {

        $sql_select = <<<EOT
  p.idrekapitulasipns, 
  p.idunitkerja,
  un.unitkerja,
  p.tanggal,
  p.idapp_user,
  app_user.username,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idagama = 1
  ) as islam,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idagama = 2
  ) as kristen,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idagama = 3
  ) as katholik,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idagama = 4
  ) as hindu,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idagama = 5
  ) as budha,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idstatuskawin = 1
  ) as kawin,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idstatuskawin = 2
  ) as belum_kawin,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idstatuskawin = 3
  ) as janda,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idstatuskawin = 4
  ) as duda,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idpendidikan = 1
  ) as sd,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idpendidikan = 2
  ) as sltp,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idpendidikan = 3
  ) as slta,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idpendidikan = 4
  ) as d,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idpendidikan = 5
  ) as s1,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idpendidikan = 6
  ) as s2,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idpendidikan = 7
  ) as s3,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idgolongan = 1
  ) as iia,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idgolongan = 2
  ) as iib,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idgolongan = 3
  ) as iiia,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idgolongan = 4
  ) as iiib,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idgolongan = 5
  ) as iva,
  (
    SELECT jumlah
    FROM bkd_rekapitulasipns
    WHERE idunitkerja = p.idunitkerja AND tanggal = p.tanggal AND idgolongan = 6
  ) as ivb
EOT;

        $this->db->select($sql_select);
        $this->db->from('bkd_rekapitulasipns p');
        $this->db->join('bkd_unitkerja un', 'p.idunitkerja = un.idunitkerja');
        $this->db->join('app_user', 'p.idapp_user = app_user.idapp_user');
        $this->db->distinct();
    }

    //datatable area
    private function _get_datatables_query()
    {
        $this->do_query();
        $column_order = array(
            null,
            'tanggal',
            'unitkerja',
            'username'
        );
        $column_search = array(
            'tanggal',
            'unitkerja',
            'username'
        );
        $order = array('idrekapitulasipns' => 'asc'); // default order


        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->do_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
}
