<?php

class Agama_model extends CI_Model
{
    public $idagama;
    public $agama;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->idagama === null){
            return $this->db->get('bkd_agama')->result();
        }
        $this->db->where('idagama', $this->idagama);
        return $this->db->get('bkd_agama')->row();
    }
}