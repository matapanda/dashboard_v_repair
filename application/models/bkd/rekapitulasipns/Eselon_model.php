<?php

class Eselon_model extends CI_Model
{
    public $ideselon;
    public $eselon;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        if($this->ideselon === null){
            return $this->db->get('bkd_eselon')->result();
        }
        $this->db->where('ideselon', $this->ideselon);
        return $this->db->get('bkd_eselon')->row();
    }
}