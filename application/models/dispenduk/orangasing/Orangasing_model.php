<?php

class Orangasing_model extends MY_Model
{

    public $idorangasing;
    public $tanggal;
    public $idkecamatan;
    public $pendudukawalbulanini_p;
    public $pendudukawalbulanini_l;
    public $lahirbulanini_p;
    public $lahirbulanini_l;
    public $matibulanini_p;
    public $matibulanini_l;
    public $pendatangbulanini_p;
    public $pendatangbulanini_l;
    public $pindahbulanini_p;
    public $pindahbulanini_l;
    public $pendudukakhirbulanini_p;
    public $pendudukakhirbulanini_l;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
      parent::__construct('dispenduk_orangasing');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('dispenduk_orangasing.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dispenduk_orangasing');
        $this->db->join('kecamatan', 'dispenduk_orangasing.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dispenduk_orangasing.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idorangasing === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idorangasing', $this->idorangasing);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dispenduk_orangasing', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dispenduk_orangasing', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idkecamatan', $this->idkecamatan);
        $this->db->set('pendudukawalbulanini_p', $this->pendudukawalbulanini_p);
        $this->db->set('pendudukawalbulanini_l', $this->pendudukawalbulanini_l);
        $this->db->set('lahirbulanini_p', $this->lahirbulanini_p);
        $this->db->set('lahirbulanini_l', $this->lahirbulanini_l);
        $this->db->set('matibulanini_p', $this->matibulanini_p);
        $this->db->set('matibulanini_l', $this->matibulanini_l);
        $this->db->set('pendatangbulanini_p', $this->pendatangbulanini_p);
        $this->db->set('pendatangbulanini_l', $this->pendatangbulanini_l);
        $this->db->set('pindahbulanini_p', $this->pindahbulanini_p);
        $this->db->set('pindahbulanini_l', $this->pindahbulanini_l);
        $this->db->set('pendudukakhirbulanini_p', $this->pendudukakhirbulanini_p);
        $this->db->set('pendudukakhirbulanini_l', $this->pendudukakhirbulanini_l);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('idorangasing', $this->idorangasing);
        if ($this->db->update('dispenduk_orangasing')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idorangasing', $this->idorangasing);
        if ($this->db->delete('dispenduk_orangasing')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'kecamatan',
            'pendudukawalbulanini_l',
            'pendudukawalbulanini_p',
            'lahirbulanini_l',
            'lahirbulanini_p',
            'matibulanini_l',
            'matibulanini_p',
            'pendatangbulanini_l',
            'pendatangbulanini_p',
            'pindahbulanini_l',
            'pindahbulanini_p',
            'pendudukakhirbulanini_l',
            'pendudukakhirbulanini_p',
            'username'
        );
        $column_search = array(
            'tanggal',
            'kecamatan',
            'pendudukawalbulanini_l',
            'pendudukawalbulanini_p',
            'lahirbulanini_l',
            'lahirbulanini_p',
            'matibulanini_l',
            'matibulanini_p',
            'pendatangbulanini_l',
            'pendatangbulanini_p',
            'pindahbulanini_l',
            'pindahbulanini_p',
            'pendudukakhirbulanini_l',
            'pendudukakhirbulanini_p',
            'username'
        );

        $order = array('idorangasing' => 'asc'); // default order

        $this->db->select('dispenduk_orangasing.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dispenduk_orangasing');
        $this->db->join('kecamatan', 'dispenduk_orangasing.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('app_user', 'app_user.idapp_user = dispenduk_orangasing.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dispenduk_orangasing');
        return $this->db->count_all_results();
    }
}
