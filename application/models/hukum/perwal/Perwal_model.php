<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perwal_model extends MY_Model
{

    public $idperwal;
    public $tanggal;
    public $perwal;
    public $is_asli;
    public $is_paraf;
    public $is_salinan;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('hukum_perwal');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      hukum_perwal.* ,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('hukum_perwal');
        $this->db->join('app_user', 'app_user.idapp_user = hukum_perwal.idapp_user');

        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idperwal === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idperwal', $this->idperwal);
        return $this->db->get()->row();

    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $query = $this->db->query(
            '
      INSERT INTO hukum_perwal
      VALUES (' . $this->idperwal . ',
              "' . $this->tanggal . '",
              "' . $this->perwal . '",
              "' . $this->is_asli . '",
              "' . $this->is_paraf . '",
              "' . $this->is_salinan . '",
              ' . $this->idapp_user . ',
              "' . $this->systemtime . '"
             )
      '
        );
        if ($this->db->affected_rows() > 0) return $this;
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $query = $this->db->query(
            '
      UPDATE hukum_perwal
      SET tanggal           ="' . $this->tanggal . '",
          `perwal(tentang)` ="' . $this->perwal . '",
          is_asli           ="' . $this->is_asli . '",
          is_paraf          ="' . $this->is_paraf . '",
          is_salinan        ="' . $this->is_salinan . '",
          idapp_user        =' . $this->idapp_user . ',
          systemtime        ="' . $this->systemtime . '"
      WHERE idperwal        =' . $this->idperwal . '
      '
        );
        if ($this->db->affected_rows() > 0) return $this;
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idperwal', $this->idperwal);
        if ($this->db->delete('hukum_perwal')) return $this;
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'perwal(tentang)',
            'is_asli',
            'is_paraf',
            'is_salinan'
        );
        $column_search = array(
            'tanggal',
            'perwal(tentang)',
            'is_asli',
            'is_paraf',
            'is_salinan'
        );

        $order = array('idperwal' => 'asc'); // default order

        $this->db->select('
      hukum_perwal.* ,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('hukum_perwal');
        $this->db->join('app_user', 'app_user.idapp_user = hukum_perwal.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('hukum_perwal');
        return $this->db->count_all_results();
    }
}
