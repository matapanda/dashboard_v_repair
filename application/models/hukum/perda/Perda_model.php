<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perda_Model extends MY_Model
{

    public $idperda;
    public $tanggal;
    public $perda;
    public $keterangan;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('hukum_perda');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('
      hukum_perda.* ,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('hukum_perda');
        $this->db->join('app_user', 'app_user.idapp_user = hukum_perda.idapp_user');

        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->idperda === null) {
            return $this->db->get()->result();
        }
        $this->db->where('idperda', $this->idperda);
        return $this->db->get()->row();

    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        if ($this->db->insert('hukum_perda', $this)) return $this;
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('perda', $this->perda);
        $this->db->set('keterangan', $this->keterangan);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);

        $this->db->where('idperda', $this->idperda);
        if ($this->db->update('hukum_perda')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idperda', $this->idperda);
        if ($this->db->delete('hukum_perda')) return $this;
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'perda',
            'keterangan',
            'username'
        );
        $column_search = array(
            'tanggal',
            'perda',
            'keterangan',
            'username'
        );

        $order = array('idperda' => 'asc'); // default order

        $this->db->select('
      hukum_perda.* ,
      app_user.idapp_user,
      app_user.username
    ');

        $this->db->from('hukum_perda');
        $this->db->join('app_user', 'app_user.idapp_user = hukum_perda.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('hukum_perda');
        return $this->db->count_all_results();
    }
}
