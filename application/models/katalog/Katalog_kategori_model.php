<?php

class Katalog_kategori_model extends CI_Model
{

    public $idkategori;
    public $kategori;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('app_katalog_kategori.*');
        $this->db->from('app_katalog_kategori');
        if($this->idkategori){
            $this->db->where('idkategori', $this->idkategori);
        }elseif ($this->kategori){
            $this->db->where('kategori', $this->kategori);
        }else{
            return $this->db->get()->result();
        }
        return $this->db->get()->row();
    }
}