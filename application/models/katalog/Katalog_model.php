<?php

class Katalog_model extends CI_Model
{

    public $idkatalog;
    public $judul;
    public $deskripsi;
    public $url;
    public $url_data;
    public $idkategori;
    public $idtipe;

    public function __construct()
    {
        $this->load->database();
    }

    public function get($keyword = null)
    {
        $this->db->select('app_katalog.*,
        app_katalog_kategori.*,
        app_katalog_tipe.*');
        $this->db->from('app_katalog');
        $this->db->join('app_katalog_kategori', 'app_katalog_kategori.idkategori = app_katalog.idkategori');
        $this->db->join('app_katalog_tipe', 'app_katalog_tipe.idtipe = app_katalog.idtipe');

        if ($keyword) {
            $this->db->like('judul', $keyword);
            $this->db->like('deskripsi', $keyword);
        }

        if ($this->idkatalog) {
            $this->db->where('app_katalog.idkatalog', $this->idkatalog);
        } elseif ($this->judul) {
            $this->db->where('app_katalog.judul', $this->judul);
        } elseif ($this->deskripsi) {
            $this->db->where('app_katalog.deskripsi', $this->deskripsi);
        } elseif ($this->url) {
            $this->db->where('app_katalog.url', $this->url);
        } elseif ($this->url_data) {
            $this->db->where('app_katalog.url_data', $this->url_data);
        } elseif ($this->idkategori) {
            $this->db->where('app_katalog.idkategori', $this->idkategori);
            return $this->db->get()->result();
        } elseif ($this->idtipe) {
            $this->db->where('app_katalog.idtipe', $this->idtipe);
            return $this->db->get()->result();
        } else {
            return $this->db->get()->result();
        }
        return $this->db->get()->row();
    }

    public function count()
    {
        $this->db->select('app_katalog.*');
        $this->db->from('app_katalog');

        if ($this->idkategori) {
            $this->db->where('app_katalog.idkategori', $this->idkategori);
            return $this->db->count_all_results();
        } else {
            return $this->db->count_all_results();
        }
    }

    public function add()
    {
        if ($this->db->insert('app_katalog', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('judul', $this->judul);
        $this->db->set('deskripsi', $this->deskripsi);
        $this->db->set('url', $this->url);
        $this->db->set('url_data', $this->url_data);
        $this->db->set('idkategori', $this->idkategori);
        $this->db->set('idtipe', $this->idtipe);
        $this->db->where('idkatalog', $this->idkatalog);
        if ($this->db->update('app_katalog')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idkatalog', $this->idkatalog);
        if ($this->db->delete('app_katalog')) {
            return $this;
        }
        return FALSE;
    }
}