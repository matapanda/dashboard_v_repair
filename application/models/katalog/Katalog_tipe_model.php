<?php

class Katalog_tipe_model extends CI_Model
{

    public $idtipe;
    public $tipe;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('app_katalog_tipe.*');
        $this->db->from('app_katalog_tipe');
        if($this->idtipe){
            $this->db->where('idtipe', $this->idtipe);
        }elseif ($this->tipe){
            $this->db->where('tipe', $this->tipe);
        }else{
            return $this->db->get()->result();
        }
        return $this->db->get()->row();
    }
}