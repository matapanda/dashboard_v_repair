<?php

class Smartcity_model extends CI_Model
{
 public $databas;


public function __construct()
    {
 
        $this->databas=$this->load->database('default',TRUE);
    }
public function createJadwal(){
    $voli=$this->databas->query("CREATE TABLE IF NOT EXISTS jadwalshalat(
    tanggal TEXT,
    shubuh TEXT,
    dzuhur TEXT,
    ashar TEXT,
    maghrib TEXT,
    isya TEXT,
    sumber TEXT
);");
return $voli;
}
public function showColumnJadwal(){
    $hai=$this->databas->list_fields('jadwalshalat');
return $hai;
}
public function insertJadwal($data){
    return $this->databas->insert('jadwalshalat', $data);
}
public function emptyTableJadwal(){
    return $this->databas->empty_table('jadwalshalat');
}
    
public function getJadwal(){
                   return $this->databas->get('jadwalshalat')->result();
    
}
public function createHarga(){
    $voli=$this->databas->query(" CREATE TABLE IF NOT EXISTS hargapasar (uuid CHAR(36) BINARY PRIMARY KEY, nama TEXT, jenis TEXT, satuan TEXT, harga_kemarin TEXT,harga_sekarang TEXT,tanggal TEXT,perubahan TEXT)");
return $voli;
}
public function emptyTableHarga(){
    return $this->databas->empty_table('hargapasar');
}
   public function insertHarga($data){
    
        if($this->databas->insert('hargapasar', $data)){
            return TRUE;
        }
        return FALSE;
    
    }
    public function getHarga()
    {
    return $this->databas->get('hargapasar')->result();
    }
    
//iki gawe eventmalang.net
    public function createEvent(){
    $voli=$this->databas->query(" CREATE TABLE IF NOT EXISTS event (uuid CHAR(36) BINARY PRIMARY KEY, img TEXT, link TEXT, title TEXT, description TEXT,category TEXT,date TEXT,tanggal TEXT)");
return $voli;
}


public function emptyTableEvent(){
    return $this->databas->empty_table('event');
}

   

//iki event malang dari user
    public function createEventUser(){
    $voli=$this->databas->query(" CREATE TABLE IF NOT EXISTS eventuser (uuid CHAR(36) BINARY PRIMARY KEY, img TEXT, link TEXT, title TEXT, description TEXT,category TEXT,date TEXT,tanggal TEXT)");
return $voli;
}
    public function insertEvent($data){
    
        if($this->databas->insert('event', $data)){
            return TRUE;
        }
        return FALSE;
    
    }
    public function insertEventUser($data){
    
        if($this->databas->insert('eventuser', $data)){
            return TRUE;
        }
        return FALSE;
    }
  
   
    
    public function getEvent()
    {
    return $this->databas->get('event')->result();
    }
 
    
}
