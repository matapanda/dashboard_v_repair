<?php
class Rencana_persalinan_biaya_model extends CI_Model {

    public $idrencana_persalinan_biaya;
    public $rencana_persalinan_biaya;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        $this->db->select('*');
        $this->db->from('dinkes_rencana_persalinan_biaya');
        if($this->idrencana_persalinan_biaya) $this->db->where('idrencana_persalinan_biaya', $this->idrencana_persalinan_biaya);
        if($this->rencana_persalinan_biaya) $this->db->where('rencana_persalinan_biaya', $this->rencana_persalinan_biaya);
        $query = $this->db->get();
        if($this->idrencana_persalinan_biaya || $this->rencana_persalinan_biaya){
            return $query->row();
        }
        return $query->result();
    }

    public function add(){
        if($this->db->insert('dinkes_rencana_persalinan_biaya', $this)){
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('rencana_persalinan_biaya', $this->rencana_persalinan_biaya);
        $this->db->where('idrencana_persalinan_biaya', $this->idrencana_persalinan_biaya);
        if($this->db->update('dinkes_rencana_persalinan_biaya')){
            return $this;
        }
        return FALSE;
    }
}