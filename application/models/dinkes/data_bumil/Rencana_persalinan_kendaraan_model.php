<?php
class Rencana_persalinan_kendaraan_model extends CI_Model {

    public $idrencana_persalinan_kendaraan;
    public $rencana_persalinan_kendaraan;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        $this->db->select('*');
        $this->db->from('dinkes_rencana_persalinan_kendaraan');
        if($this->idrencana_persalinan_kendaraan) $this->db->where('idrencana_persalinan_kendaraan', $this->idrencana_persalinan_kendaraan);
        if($this->rencana_persalinan_kendaraan) $this->db->where('rencana_persalinan_kendaraan', $this->rencana_persalinan_kendaraan);
        $query = $this->db->get();
        if($this->idrencana_persalinan_kendaraan || $this->rencana_persalinan_kendaraan){
            return $query->row();
        }
        return $query->result();
    }

    public function add(){
        if($this->db->insert('dinkes_rencana_persalinan_kendaraan', $this)){
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('rencana_persalinan_kendaraan', $this->rencana_persalinan_kendaraan);
        $this->db->where('idrencana_persalinan_kendaraan', $this->idrencana_persalinan_kendaraan);
        if($this->db->update('dinkes_rencana_persalinan_kendaraan')){
            return $this;
        }
        return FALSE;
    }
}