<?php
class Keterangan_bpjs_model extends CI_Model {

    public $idketerangan_bpjs;
    public $keterangan_bpjs;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        $this->db->select('*');
        $this->db->from('dinkes_keterangan_bpjs');
        if($this->idketerangan_bpjs) $this->db->where('idketerangan_bpjs', $this->idketerangan_bpjs);
        if($this->keterangan_bpjs) $this->db->where('keterangan_bpjs', $this->keterangan_bpjs);
        $query = $this->db->get();
        if($this->idketerangan_bpjs || $this->keterangan_bpjs){
            return $query->row();
        }
        return $query->result();
    }

    public function add(){
        if($this->db->insert('dinkes_keterangan_bpjs', $this)){
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('keterangan_bpjs', $this->keterangan_bpjs);
        $this->db->where('idketerangan_bpjs', $this->idketerangan_bpjs);
        if($this->db->update('dinkes_keterangan_bpjs')){
            return $this;
        }
        return FALSE;
    }
}