<?php

class Data_bumil_model extends MY_Model
{

    public $iddata_bumil;
    public $tanggal;
    public $nama_ibu;
    public $nama_suami;
    public $alamat;
    public $rt;
    public $rw;
    public $idkelurahan;
    public $no_nik_ibu;
    public $puskesmas;
    public $hpht;
    public $tafsiran_persalinan;
    public $hamil_ke;
    public $bb;
    public $tb;
    public $lila_imt;
    public $hb_golda;
    public $tensi;
    public $pendeteksi_resiko_nakes;
    public $pendeteksi_resiko_masyarakat;
    public $imunisasi_tt_status_imunisasi_tt;
    public $imunisasi_tt_pemberian_imunisasi_tt;
    public $rencana_persalinan_tempat;
    public $anc_terpadu_albumin;
    public $anc_terpadu_reduksi;
    public $anc_terpadu_gds;
    public $anc_terpadu_hbsag;
    public $anc_terpadu_hiv;
    public $faktor_resiko;
    public $idrencana_persalinan_biaya;
    public $idrencana_persalinan_kendaraan;
    public $umur_ibu;
    public $kehamilan_minggu;
    public $jarak_kehamilan;
    public $idketerangan_bpjs;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('dinkes_data_bumil');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {

        $this->db->select('dinkes_data_bumil.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dinkes_rencana_persalinan_biaya.*,
        dinkes_rencana_persalinan_kendaraan.*,
        dinkes_keterangan_bpjs.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinkes_data_bumil');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinkes_data_bumil.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinkes_rencana_persalinan_biaya', 'dinkes_rencana_persalinan_biaya.idrencana_persalinan_biaya = dinkes_data_bumil.idrencana_persalinan_biaya');
        $this->db->join('dinkes_rencana_persalinan_kendaraan', 'dinkes_rencana_persalinan_kendaraan.idrencana_persalinan_kendaraan = dinkes_data_bumil.idrencana_persalinan_kendaraan');
        $this->db->join('dinkes_keterangan_bpjs', 'dinkes_keterangan_bpjs.idketerangan_bpjs = dinkes_data_bumil.idketerangan_bpjs');
        $this->db->join('app_user', 'app_user.idapp_user = dinkes_data_bumil.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->iddata_bumil === null) {
            return $this->db->get()->result();
        }
        $this->db->where('iddata_bumil', $this->iddata_bumil);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        if ($this->db->insert('dinkes_data_bumil', $this)) {
            return TRUE;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinkes_data_bumil', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        $this->db->set('nama_ibu', $this->nama_ibu);
        $this->db->set('nama_suami', $this->nama_suami);
        $this->db->set('alamat', $this->alamat);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('no_nik_ibu', $this->no_nik_ibu);
        $this->db->set('puskesmas', $this->puskesmas);
        $this->db->set('hpht', $this->hpht);
        $this->db->set('tafsiran_persalinan', $this->tafsiran_persalinan);
        $this->db->set('hamil_ke', $this->hamil_ke);
        $this->db->set('bb', $this->bb);
        $this->db->set('tb', $this->tb);
        $this->db->set('lila_imt', $this->lila_imt);
        $this->db->set('hb_golda', $this->hb_golda);
        $this->db->set('tensi', $this->tensi);
        $this->db->set('pendeteksi_resiko_nakes', $this->pendeteksi_resiko_nakes);
        $this->db->set('pendeteksi_resiko_masyarakat', $this->pendeteksi_resiko_masyarakat);
        $this->db->set('imunisasi_tt_status_imunisasi_tt', $this->imunisasi_tt_status_imunisasi_tt);
        $this->db->set('imunisasi_tt_pemberian_imunisasi_tt', $this->imunisasi_tt_pemberian_imunisasi_tt);
        $this->db->set('rencana_persalinan_tempat', $this->rencana_persalinan_tempat);
        $this->db->set('anc_terpadu_albumin', $this->anc_terpadu_albumin);
        $this->db->set('anc_terpadu_reduksi', $this->anc_terpadu_reduksi);
        $this->db->set('anc_terpadu_gds', $this->anc_terpadu_gds);
        $this->db->set('anc_terpadu_hbsag', $this->anc_terpadu_hbsag);
        $this->db->set('anc_terpadu_hiv', $this->anc_terpadu_hiv);
        $this->db->set('faktor_resiko', $this->faktor_resiko);
        $this->db->set('idrencana_persalinan_biaya', $this->idrencana_persalinan_biaya);
        $this->db->set('idrencana_persalinan_kendaraan', $this->idrencana_persalinan_kendaraan);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->set('umur_ibu', $this->umur_ibu);
        $this->db->set('kehamilan_minggu', $this->kehamilan_minggu);
        $this->db->set('jarak_kehamilan', $this->jarak_kehamilan);
        $this->db->set('idketerangan_bpjs', $this->idketerangan_bpjs);
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('rt', $this->rt);
        $this->db->set('rw', $this->rw);
        $this->db->where('iddata_bumil', $this->iddata_bumil);
        if ($this->db->update('dinkes_data_bumil')) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('iddata_bumil', $this->iddata_bumil);
        if ($this->db->delete('dinkes_data_bumil')) {
            return TRUE;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'tanggal',
            'puskesmas',
            'kelurahan',
            'nama_ibu',
            'nama_suami',
            'alamat',
            'rt',
            'rw',
            'no_nik_ibu',
            'hpht',
            'tafsiran_persalinan',
            'umur_ibu',
            'kehamilan_minggu',
            'hamil_ke',
            'bb',
            'tb',
            'lila_imt',
            'hb_golda',
            'tensi',
            'pendeteksi_resiko_nakes',
            'pendeteksi_resiko_masyarakat',
            'faktor_resiko',
            'jarak_kehamilan',
            'imunisasi_tt_status_imunisasi_tt',
            'imunisasi_tt_pemberian_imunisasi_tt',
            'rencana_persalinan_tempat',
            'rencana_persalinan_biaya',
            'rencana_persalinan_kendaraan',
            'anc_terpadu_albumin',
            'anc_terpadu_reduksi',
            'anc_terpadu_gds',
            'anc_terpadu_hbsag',
            'anc_terpadu_hiv',
            'keterangan_bpjs',
            'username'
        );
        $column_search = array(
            'tanggal',
            'puskesmas',
            'kelurahan',
            'nama_ibu',
            'nama_suami',
            'alamat',
            'rt',
            'rw',
            'no_nik_ibu',
            'hpht',
            'tafsiran_persalinan',
            'umur_ibu',
            'kehamilan_minggu',
            'hamil_ke',
            'bb',
            'tb',
            'lila_imt',
            'hb_golda',
            'tensi',
            'pendeteksi_resiko_nakes',
            'pendeteksi_resiko_masyarakat',
            'faktor_resiko',
            'jarak_kehamilan',
            'imunisasi_tt_status_imunisasi_tt',
            'imunisasi_tt_pemberian_imunisasi_tt',
            'rencana_persalinan_tempat',
            'rencana_persalinan_biaya',
            'rencana_persalinan_kendaraan',
            'anc_terpadu_albumin',
            'anc_terpadu_reduksi',
            'anc_terpadu_gds',
            'anc_terpadu_hbsag',
            'anc_terpadu_hiv',
            'keterangan_bpjs',
            'username'
        );
        $order = array('iddata_bumil' => 'asc'); // default order

        $this->db->select('dinkes_data_bumil.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dinkes_rencana_persalinan_biaya.*,
        dinkes_rencana_persalinan_kendaraan.*,
        dinkes_keterangan_bpjs.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinkes_data_bumil');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinkes_data_bumil.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinkes_rencana_persalinan_biaya', 'dinkes_rencana_persalinan_biaya.idrencana_persalinan_biaya = dinkes_data_bumil.idrencana_persalinan_biaya');
        $this->db->join('dinkes_rencana_persalinan_kendaraan', 'dinkes_rencana_persalinan_kendaraan.idrencana_persalinan_kendaraan = dinkes_data_bumil.idrencana_persalinan_kendaraan');
        $this->db->join('dinkes_keterangan_bpjs', 'dinkes_keterangan_bpjs.idketerangan_bpjs = dinkes_data_bumil.idketerangan_bpjs');
        $this->db->join('app_user', 'app_user.idapp_user = dinkes_data_bumil.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinkes_data_bumil');
        return $this->db->count_all_results();
    }
}
