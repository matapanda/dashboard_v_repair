<?php
class Puskesmas_model extends CI_Model {

    public $idpuskesmas;
    public $no_puskesmas;
    public $puskesmas;

    public function __construct()
    {
        $this->load->database();
    }

    public function get(){
        $this->db->select('*');
        $this->db->from('dinkes_puskesmas');
        if($this->idpuskesmas) $this->db->where('idpuskesmas', $this->idpuskesmas);
        if($this->no_puskesmas) $this->db->where('no_puskesmas', $this->no_puskesmas);
        if($this->puskesmas) $this->db->where('puskesmas', $this->puskesmas);
        $query = $this->db->get();
        if($this->idpuskesmas || $this->no_puskesmas || $this->puskesmas){
            return $query->row();
        }
        return $query->result();
    }

    public function add(){
        if($this->db->insert('dinkes_puskesmas', $this)){
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch){
        if($this->db->insert_batch('dinkes_puskesmas', $data_batch)){
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('no_puskesmas', $this->no_puskesmas);
        $this->db->set('puskesmas', $this->puskesmas);
        $this->db->where('idpuskesmas', $this->idpuskesmas);
        if($this->db->update('dinkes_puskesmas')){
            return $this;
        }
        return FALSE;
    }
}
