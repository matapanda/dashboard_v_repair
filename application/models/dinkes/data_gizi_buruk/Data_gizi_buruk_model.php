<?php

class Data_gizi_buruk_model extends MY_Model
{

    public $iddata_gizi_buruk;
    public $tanggal;
    public $no_kelurahan;
    public $idkelurahan;
    public $alamat;
    public $umur_bulan;
    public $bb_kg;
    public $tb_cm;
    public $bulan_masuk;
    public $bulan_keluar;
    public $status_keluarga_gakin;
    public $idpuskesmas;
    public $idbalita;
    public $rt;
    public $rw;
    public $idapp_user;
    public $systemtime;

    public function __construct()
    {
        parent::__construct('dinkes_data_gizi_buruk');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {

        $this->db->select('dinkes_data_gizi_buruk.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dinkes_balita.*,
        dinkes_puskesmas.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinkes_data_gizi_buruk');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinkes_data_gizi_buruk.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinkes_balita', 'dinkes_balita.idbalita = dinkes_data_gizi_buruk.idbalita');
        $this->db->join('dinkes_puskesmas', 'dinkes_puskesmas.idpuskesmas = dinkes_data_gizi_buruk.idpuskesmas');
        $this->db->join('app_user', 'app_user.idapp_user = dinkes_data_gizi_buruk.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->iddata_gizi_buruk === null) {
            return $this->db->get()->result();
        }
        $this->db->where('iddata_gizi_buruk', $this->iddata_gizi_buruk);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        if ($this->db->insert('dinkes_data_gizi_buruk', $this)) {
            return TRUE;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinkes_data_gizi_buruk', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');

        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('no_kelurahan', $this->no_kelurahan);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('alamat', $this->alamat);
        $this->db->set('umur_bulan', $this->umur_bulan);
        $this->db->set('bb_kg', $this->bb_kg);
        $this->db->set('tb_cm', $this->tb_cm);
        $this->db->set('bulan_masuk', $this->bulan_masuk);
        $this->db->set('bulan_keluar', $this->bulan_keluar);
        $this->db->set('status_keluarga_gakin', $this->status_keluarga_gakin);
        $this->db->set('idbalita', $this->idbalita);
        $this->db->set('rt', $this->rt);
        $this->db->set('rw', $this->rw);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->where('iddata_gizi_buruk', $this->iddata_gizi_buruk);
        if ($this->db->update('dinkes_data_gizi_buruk')) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete()
    {
        if ($this->iddata_gizi_buruk) {
            $this->db->where('iddata_gizi_buruk', $this->iddata_gizi_buruk);
        }
        if ($this->idbalita) {
            $this->db->where('idbalita', $this->idbalita);
        }
        if ($this->db->delete('dinkes_data_gizi_buruk')) {
            return TRUE;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
        null,
        'tanggal',
        'no_kelurahan',
        'kelurahan',
        'puskesmas',
        'nama_balita',
        'nama_orang_tua',
        'alamat',
        'rt',
        'rw',
        'umur_bulan',
        'bb_kg',
        'tb_cm',
        'bulan_masuk',
        'bulan_keluar',
        'status_keluarga_gakin',
        'username'
    );
        $column_search = array(
        'tanggal',
        'no_kelurahan',
        'kelurahan',
        'puskesmas',
        'nama_balita',
        'nama_orang_tua',
        'alamat',
        'rt',
        'rw',
        'umur_bulan',
        'bb_kg',
        'tb_cm',
        'bulan_masuk',
        'bulan_keluar',
        'status_keluarga_gakin',
        'username'
    );
        $order = array('iddata_gizi_buruk' => 'asc'); // default order

        $this->db->select('dinkes_data_gizi_buruk.*,
        kelurahan.*,
        kecamatan.*,
        kota.*,
        propinsi.*,
        dinkes_balita.*,
        dinkes_puskesmas.*,
        app_user.idapp_user,
        app_user.username');
        $this->db->from('dinkes_data_gizi_buruk');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinkes_data_gizi_buruk.idkelurahan');
        $this->db->join('kecamatan', 'kelurahan.idkecamatan = kecamatan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinkes_balita', 'dinkes_balita.idbalita = dinkes_data_gizi_buruk.idbalita');
        $this->db->join('dinkes_puskesmas', 'dinkes_puskesmas.idpuskesmas = dinkes_data_gizi_buruk.idpuskesmas');
        $this->db->join('app_user', 'app_user.idapp_user = dinkes_data_gizi_buruk.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinkes_data_gizi_buruk');
        return $this->db->count_all_results();
    }
}
