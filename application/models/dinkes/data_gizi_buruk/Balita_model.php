<?php

class Balita_model extends CI_Model
{

    public $idbalita;
    public $nama_balita;
    public $nama_orang_tua;
    public $idjeniskelamin;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('
        dinkes_balita.*, 
        jeniskelamin.*'
        );
        $this->db->from('dinkes_balita');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinkes_balita.idjeniskelamin');
        if ($this->idbalita) $this->db->where('idbalita', $this->idbalita);
        if ($this->nama_balita) $this->db->where('nama_balita', $this->nama_balita);
        if ($this->nama_orang_tua) $this->db->where('nama_orang_tua', $this->nama_orang_tua);
        $query = $this->db->get();
        if ($this->idbalita || $this->nama_balita || $this->nama_orang_tua) {
            return $query->row();
        }
        return $query->result();
    }

    public function add()
    {
        if ($this->db->insert('dinkes_balita', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinkes_balita', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update()
    {
        $this->db->set('nama_balita', $this->nama_balita);
        $this->db->set('nama_orang_tua', $this->nama_orang_tua);
        $this->db->set('idjeniskelamin', $this->idjeniskelamin);
        $this->db->where('idbalita', $this->idbalita);
        if ($this->db->update('dinkes_balita')) {
            return $this;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('idbalita', $this->idbalita);
        if ($this->db->delete('dinkes_data_gizi_buruk')) {
            $this->db->where('idbalita', $this->idbalita);
            if ($this->db->delete('dinkes_balita')) {
                return $this;
            }
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'nama_balita',
            'nama_orang_tua',
            'jeniskelamin'
        );
        $column_search = array(
            'nama_balita',
            'nama_orang_tua',
            'jeniskelamin'
        );
        $order = array('idbalita' => 'asc'); // default order

        $this->db->select('
        dinkes_balita.*, 
        jeniskelamin.*'
        );
        $this->db->from('dinkes_balita');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinkes_balita.idjeniskelamin');
        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else{
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinkes_balita');
        return $this->db->count_all_results();
    }

}