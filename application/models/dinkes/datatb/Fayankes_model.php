<?php

class Fayankes_model extends CI_Model
{
    public $idfayankes;
    public $fayankes;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('dinkes_fayankes');
        if($this->idfayankes) $this->db->where('idfayankes', $this->idfayankes);
        if($this->fayankes) $this->db->where('fayankes', $this->fayankes);
        $query = $this->db->get();
        if($this->fayankes || $this->fayankes){
            return $query->row();
        }
        return $query->result();
    }

}
