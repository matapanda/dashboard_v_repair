<?php

class JenisFayankes_Model extends CI_Model
{
    public $idjenis_fayankes;
    public $jenis_fayankes;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('dinkes_jenis_fayankes');
        if($this->idjenis_fayankes) $this->db->where('idjenis_fayankes', $this->idjenis_fayankes);
        if($this->jenis_fayankes) $this->db->where('jenis_fayankes', $this->jenis_fayankes);
        $query = $this->db->get();
        if($this->idjenis_fayankes || $this->jenis_fayankes){
            return $query->row();
        }
        return $query->result();
    }

}
