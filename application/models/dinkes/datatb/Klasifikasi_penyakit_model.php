<?php

class Klasifikasi_Penyakit_Model extends CI_Model
{
    public $idklasifikasi_penyakit;
    public $klasifikasi_penyakit;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('dinkes_klasifikasi_penyakit');
        if($this->idklasifikasi_penyakit) $this->db->where('idklasifikasi_penyakit', $this->idklasifikasi_penyakit);
        if($this->klasifikasi_penyakit) $this->db->where('klasifikasi_penyakit', $this->klasifikasi_penyakit);
        $query = $this->db->get();
        if($this->idklasifikasi_penyakit || $this->klasifikasi_penyakit){
            return $query->row();
        }
        return $query->result();
    }

}
