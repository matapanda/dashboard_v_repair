<?php

class Data_TB_Model extends MY_Model
{
    public $iddata_tb;
    public $nik;
    public $nama_pasien;
    public $tanggal_lahir;
    public $umur;
    public $nomor_registrasi_kab_kota;
    public $dirujuk_oleh;
    public $tanggal_mulai_pengobatan;
    public $bb_kg;
    public $tb_cm;
    public $total_scoring_pada_tb_anak;
    public $sebelum_pengobatan_no_reg_lab;
    public $sebelum_pengobatan_hasil_dahak;
    public $akhir_tahap_awal_no_reg_lab;
    public $akhir_tahap_awal_hasil_dahak;
    public $akhir_bulan_ke_5_7_no_reg_lab;
    public $akhir_bulan_ke_5_7_hasil_dahak;
    public $akhir_pengobatan_no_reg_lab;
    public $akhir_pengobatan_hasil_dahak;
    public $hasil_pengobatan_sembuh;
    public $hasil_pengobatan_lengkap;
    public $hasil_pengobatan_default;
    public $hasil_pengobatan_gagal;
    public $hasil_pengobatan_meninggal;
    public $hasil_pengobatan_pindah;
    public $idkelurahan; // kelurahan
    public $riwayat_tes_hiv_tanggal_tes_hiv_terakhir;
    public $riwayat_tes_hiv_hasil_tes;
    public $layanan_konseling_tanggal_test_hiv;
    public $layanan_konseling_tempat_test;
    public $layanan_konseling_hasil_test;
    public $layanan_konseling_tanggal_pasca_test_konseling;
    public $layanan_konseling_tanggal_pretest_konseling;
    public $layanan_konseling_tanggal_dianjurkan;
    public $layanan_pdp_tanggal_rujukan_pdp;
    public $layanan_pdp_tanggal_mulai_ppk;
    public $layanan_pdp_tanggal_mulai_art;
    public $idtipe_pasien; //dinkes_tipe_pasien
    public $idklasifikasi_penyakit; //dinkes_klasifikasi_penyakit
    public $idkode_paduan_rejimen_yang_diberikan; //dinkes_kode_paduan_rejimen_yang_diberikan
    public $idjenis_fayankes; // fk dinkes_fayankes
    public $idfayankes; // fk dinkes_jenis_fayankes
    public $idvalidasi_data; // dinkes_validasi_data
    public $systemtime;
    public $idapp_user;
    public $tanggal;
    public $idpasien;
    public $idjeniskelamin;
    public $akhir_sisipan_no_reg_lab;
    public $akhir_sisipan_hasil_dahak;
    public $area_sentinel_tanggal_pretest_konseling;
    public $area_sentinel_tanggal_dianjurkan;
    public $area_sentinel_tanggal_test_hiv;
    public $area_sentinel_hasil_test;

    public function __construct()
    {
        parent::__construct('dinkes_data_tb');
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select(
            'dinkes_data_tb.*,
           kelurahan.*,
           kecamatan.*,
           kota.*,
           propinsi.*,
           dinkes_tipe_pasien.*,
           dinkes_klasifikasi_penyakit.*,
           dinkes_kode_paduan_rejimen_yang_diberikan.*,
           dinkes_fayankes.*,
           dinkes_jenis_fayankes.*,
           dinkes_validasi_data.*,
           jeniskelamin.*,
        app_user.idapp_user,
        app_user.username'
        );

        $this->db->from('dinkes_data_tb');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinkes_data_tb.idkelurahan');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = kelurahan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinkes_tipe_pasien', 'dinkes_tipe_pasien.idtipe_pasien = dinkes_data_tb.idtipe_pasien');
        $this->db->join('dinkes_klasifikasi_penyakit', 'dinkes_klasifikasi_penyakit.idklasifikasi_penyakit = dinkes_data_tb.idklasifikasi_penyakit');
        $this->db->join('dinkes_kode_paduan_rejimen_yang_diberikan', 'dinkes_kode_paduan_rejimen_yang_diberikan.idkode_paduan_rejimen_yang_diberikan = dinkes_data_tb.idkode_paduan_rejimen_yang_diberikan');
        $this->db->join('dinkes_fayankes', 'dinkes_fayankes.idfayankes = dinkes_data_tb.idfayankes');
        $this->db->join('dinkes_jenis_fayankes', 'dinkes_jenis_fayankes.idjenis_fayankes = dinkes_data_tb.idjenis_fayankes');
        $this->db->join('dinkes_validasi_data', 'dinkes_validasi_data.idvalidasi_data = dinkes_data_tb.idvalidasi_data');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinkes_data_tb.idjeniskelamin');
        $this->db->join('app_user', 'app_user.idapp_user = dinkes_data_tb.idapp_user');
        if (is_numeric($limit) && is_numeric($page)) {
            $offset = ($page - 1) * $limit;
            return $this->db->limit($limit, $offset)->get()->result();
        }
        if ($this->iddata_tb === null) {
            return $this->db->get()->result();
        }
        $this->db->where('iddata_tb', $this->iddata_tb);
        return $this->db->get()->row();
    }

    public function add()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        if ($this->db->insert('dinkes_data_tb', $this)) {
            return $this;
        }
        return FALSE;
    }

    public function update()
    {
        $this->systemtime = date('Y-m-d H:i:s');
        $this->db->set('tanggal', $this->tanggal);
        $this->db->set('idpasien', $this->idpasien);
        $this->db->set('nik', $this->nik);
        $this->db->set('nama_pasien', $this->nama_pasien);
        $this->db->set('tanggal_lahir', $this->tanggal_lahir);
        $this->db->set('umur', $this->umur);
        $this->db->set('nomor_registrasi_kab_kota', $this->nomor_registrasi_kab_kota);
        $this->db->set('dirujuk_oleh', $this->dirujuk_oleh);
        $this->db->set('tanggal_mulai_pengobatan', $this->tanggal_mulai_pengobatan);
        $this->db->set('bb_kg', $this->bb_kg);
        $this->db->set('tb_cm', $this->tb_cm);
        $this->db->set('total_scoring_pada_tb_anak', $this->total_scoring_pada_tb_anak);
        $this->db->set('sebelum_pengobatan_no_reg_lab', $this->sebelum_pengobatan_no_reg_lab);
        $this->db->set('sebelum_pengobatan_hasil_dahak', $this->sebelum_pengobatan_hasil_dahak);
        $this->db->set('akhir_tahap_awal_no_reg_lab', $this->akhir_tahap_awal_no_reg_lab);
        $this->db->set('akhir_tahap_awal_hasil_dahak', $this->akhir_tahap_awal_hasil_dahak);
        $this->db->set('akhir_bulan_ke_5_7_no_reg_lab', $this->akhir_bulan_ke_5_7_no_reg_lab);
        $this->db->set('akhir_bulan_ke_5_7_hasil_dahak', $this->akhir_bulan_ke_5_7_hasil_dahak);
        $this->db->set('akhir_pengobatan_no_reg_lab', $this->akhir_pengobatan_no_reg_lab);
        $this->db->set('akhir_pengobatan_hasil_dahak', $this->akhir_pengobatan_hasil_dahak);
        $this->db->set('hasil_pengobatan_sembuh', $this->hasil_pengobatan_sembuh);
        $this->db->set('hasil_pengobatan_lengkap', $this->hasil_pengobatan_lengkap);
        $this->db->set('hasil_pengobatan_default', $this->hasil_pengobatan_default);
        $this->db->set('hasil_pengobatan_gagal', $this->hasil_pengobatan_gagal);
        $this->db->set('hasil_pengobatan_meninggal', $this->hasil_pengobatan_meninggal);
        $this->db->set('hasil_pengobatan_pindah', $this->hasil_pengobatan_pindah);
        $this->db->set('idkelurahan', $this->idkelurahan);
        $this->db->set('riwayat_tes_hiv_hasil_tes', $this->riwayat_tes_hiv_hasil_tes);
        $this->db->set('riwayat_tes_hiv_tanggal_tes_hiv_terakhir', $this->riwayat_tes_hiv_tanggal_tes_hiv_terakhir);
        $this->db->set('layanan_konseling_tanggal_test_hiv', $this->layanan_konseling_tanggal_test_hiv);
        $this->db->set('layanan_konseling_tempat_test', $this->layanan_konseling_tempat_test);
        $this->db->set('layanan_konseling_hasil_test', $this->layanan_konseling_hasil_test);
        $this->db->set('layanan_konseling_tanggal_pasca_test_konseling', $this->layanan_konseling_tanggal_pasca_test_konseling);
        $this->db->set('layanan_konseling_tanggal_pretest_konseling', $this->layanan_konseling_tanggal_pretest_konseling);
        $this->db->set('layanan_konseling_tanggal_dianjurkan', $this->layanan_konseling_tanggal_dianjurkan);
        $this->db->set('layanan_pdp_tanggal_rujukan_pdp', $this->layanan_pdp_tanggal_rujukan_pdp);
        $this->db->set('layanan_pdp_tanggal_mulai_ppk', $this->layanan_pdp_tanggal_mulai_ppk);
        $this->db->set('layanan_pdp_tanggal_mulai_art', $this->layanan_pdp_tanggal_mulai_art);
        $this->db->set('akhir_sisipan_no_reg_lab	', $this->akhir_sisipan_no_reg_lab);
        $this->db->set('akhir_sisipan_hasil_dahak', $this->akhir_sisipan_hasil_dahak);
        $this->db->set('area_sentinel_tanggal_pretest_konseling', $this->area_sentinel_tanggal_pretest_konseling);
        $this->db->set('area_sentinel_tanggal_dianjurkan', $this->area_sentinel_tanggal_dianjurkan);
        $this->db->set('area_sentinel_tanggal_test_hiv', $this->area_sentinel_tanggal_test_hiv);
        $this->db->set('area_sentinel_hasil_test', $this->area_sentinel_hasil_test);
        $this->db->set('idtipe_pasien', $this->idtipe_pasien);
        $this->db->set('idklasifikasi_penyakit', $this->idklasifikasi_penyakit);
        $this->db->set('idkode_paduan_rejimen_yang_diberikan', $this->idkode_paduan_rejimen_yang_diberikan);
        $this->db->set('idjenis_fayankes', $this->idjenis_fayankes);
        $this->db->set('idfayankes', $this->idfayankes);
        $this->db->set('idvalidasi_data', $this->idvalidasi_data);
        $this->db->set('systemtime', $this->systemtime);
        $this->db->set('idapp_user', $this->idapp_user);
        $this->db->set('idjeniskelamin', $this->idjeniskelamin);

        $this->db->where('iddata_tb', $this->iddata_tb);
        if ($this->db->update('dinkes_data_tb')) {
            return $this;
        }
        return FALSE;
    }

    public function add_batch($data_batch)
    {
        if ($this->db->insert_batch('dinkes_data_tb', $data_batch)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete()
    {
        $this->db->where('iddata_tb', $this->iddata_tb);
        if ($this->db->delete('dinkes_data_tb')) {
            return $this;
        }
        return FALSE;
    }

    //datatable area

    private function _get_datatables_query()
    {
        $column_order = array(
            null,
            'akhir_bulan_ke_5_7_hasil_dahak',
            'akhir_bulan_ke_5_7_no_reg_lab',
            'akhir_pengobatan_hasil_dahak',
            'akhir_pengobatan_no_reg_lab',
            'akhir_sisipan_hasil_dahak',
            'akhir_sisipan_no_reg_lab',
            'akhir_tahap_awal_hasil_dahak',
            'akhir_tahap_awal_no_reg_lab',
            'area_sentinel_hasil_test',
            'area_sentinel_tanggal_dianjurkan',
            'area_sentinel_tanggal_pretest_konseling',
            'area_sentinel_tanggal_test_hiv',
            'bb_kg',
            'dirujuk_oleh',
            'hasil_pengobatan_default',
            'hasil_pengobatan_gagal',
            'hasil_pengobatan_lengkap',
            'hasil_pengobatan_meninggal',
            'hasil_pengobatan_pindah',
            'hasil_pengobatan_sembuh',
            'username',
            'fayankes',
            'jeniskelamin',
            'jenis_fayankes',
            'kelurahan',
            'klasifikasi_penyakit',
            'kode_paduan_rejimen_yang_diberikan',
            'idpasien',
            'tipe_pasien',
            'validasi_data',
            'layanan_konseling_hasil_test',
            'layanan_konseling_tanggal_dianjurkan',
            'layanan_konseling_tanggal_pasca_test_konseling',
            'layanan_konseling_tanggal_pretest_konseling',
            'layanan_konseling_tanggal_test_hiv',
            'layanan_konseling_tempat_test',
            'layanan_pdp_tanggal_mulai_art',
            'layanan_pdp_tanggal_mulai_ppk',
            'layanan_pdp_tanggal_rujukan_pdp',
            'nama_pasien',
            'nik',
            'nomor_registrasi_kab_kota',
            'riwayat_tes_hiv_hasil_tes',
            'riwayat_tes_hiv_tanggal_tes_hiv_terakhir',
            'sebelum_pengobatan_hasil_dahak',
            'sebelum_pengobatan_no_reg_lab',
            'systemtime',
            'tanggal',
            'tanggal_lahir',
            'tanggal_mulai_pengobatan',
            'tb_cm',
            'total_scoring_pada_tb_anak',
            'umur'
        ); //set column field database for datatable orderable
        $column_search = array(
            'akhir_bulan_ke_5_7_hasil_dahak',
            'akhir_bulan_ke_5_7_no_reg_lab',
            'akhir_pengobatan_hasil_dahak',
            'akhir_pengobatan_no_reg_lab',
            'akhir_sisipan_hasil_dahak',
            'akhir_sisipan_no_reg_lab',
            'akhir_tahap_awal_hasil_dahak',
            'akhir_tahap_awal_no_reg_lab',
            'area_sentinel_hasil_test',
            'area_sentinel_tanggal_dianjurkan',
            'area_sentinel_tanggal_pretest_konseling',
            'area_sentinel_tanggal_test_hiv',
            'bb_kg',
            'dirujuk_oleh',
            'hasil_pengobatan_default',
            'hasil_pengobatan_gagal',
            'hasil_pengobatan_lengkap',
            'hasil_pengobatan_meninggal',
            'hasil_pengobatan_pindah',
            'hasil_pengobatan_sembuh',
            'username',
            'fayankes',
            'jeniskelamin',
            'jenis_fayankes',
            'kelurahan',
            'klasifikasi_penyakit',
            'kode_paduan_rejimen_yang_diberikan',
            'idpasien',
            'tipe_pasien',
            'validasi_data',
            'layanan_konseling_hasil_test',
            'layanan_konseling_tanggal_dianjurkan',
            'layanan_konseling_tanggal_pasca_test_konseling',
            'layanan_konseling_tanggal_pretest_konseling',
            'layanan_konseling_tanggal_test_hiv',
            'layanan_konseling_tempat_test',
            'layanan_pdp_tanggal_mulai_art',
            'layanan_pdp_tanggal_mulai_ppk',
            'layanan_pdp_tanggal_rujukan_pdp',
            'nama_pasien',
            'nik',
            'nomor_registrasi_kab_kota',
            'riwayat_tes_hiv_hasil_tes',
            'riwayat_tes_hiv_tanggal_tes_hiv_terakhir',
            'sebelum_pengobatan_hasil_dahak',
            'sebelum_pengobatan_no_reg_lab',
            'systemtime',
            'tanggal',
            'tanggal_lahir',
            'tanggal_mulai_pengobatan',
            'tb_cm',
            'total_scoring_pada_tb_anak',
            'umur'
        ); //set column field database for datatable searchable
        $order = array('iddata_tb' => 'asc'); // default order

        $this->db->select(
            'dinkes_data_tb.*,
           kelurahan.*,
           kecamatan.*,
           kota.*,
           propinsi.*,
           dinkes_tipe_pasien.*,
           dinkes_klasifikasi_penyakit.*,
           dinkes_kode_paduan_rejimen_yang_diberikan.*,
           dinkes_fayankes.*,
           dinkes_jenis_fayankes.*,
           dinkes_validasi_data.*,
           jeniskelamin.*,
        app_user.idapp_user,
        app_user.username'
        );

        $this->db->from('dinkes_data_tb');
        $this->db->join('kelurahan', 'kelurahan.idkelurahan = dinkes_data_tb.idkelurahan');
        $this->db->join('kecamatan', 'kecamatan.idkecamatan = kelurahan.idkecamatan');
        $this->db->join('kota', 'kecamatan.idkota = kota.idkota');
        $this->db->join('propinsi', 'kota.idpropinsi = propinsi.idpropinsi');
        $this->db->join('dinkes_tipe_pasien', 'dinkes_tipe_pasien.idtipe_pasien = dinkes_data_tb.idtipe_pasien');
        $this->db->join('dinkes_klasifikasi_penyakit', 'dinkes_klasifikasi_penyakit.idklasifikasi_penyakit = dinkes_data_tb.idklasifikasi_penyakit');
        $this->db->join('dinkes_kode_paduan_rejimen_yang_diberikan', 'dinkes_kode_paduan_rejimen_yang_diberikan.idkode_paduan_rejimen_yang_diberikan = dinkes_data_tb.idkode_paduan_rejimen_yang_diberikan');
        $this->db->join('dinkes_fayankes', 'dinkes_fayankes.idfayankes = dinkes_data_tb.idfayankes');
        $this->db->join('dinkes_jenis_fayankes', 'dinkes_jenis_fayankes.idjenis_fayankes = dinkes_data_tb.idjenis_fayankes');
        $this->db->join('dinkes_validasi_data', 'dinkes_validasi_data.idvalidasi_data = dinkes_data_tb.idvalidasi_data');
        $this->db->join('jeniskelamin', 'jeniskelamin.idjeniskelamin = dinkes_data_tb.idjeniskelamin');
        $this->db->join('app_user', 'app_user.idapp_user = dinkes_data_tb.idapp_user');

        $i = 0;

        foreach ($column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else {
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from('dinkes_data_tb');
        return $this->db->count_all_results();
    }
}

//'akhir_bulan_ke_5_7_hasil_dahak',
//'akhir_bulan_ke_5_7_no_reg_lab',
//'akhir_pengobatan_hasil_dahak',
//'akhir_pengobatan_no_reg_lab',
//'akhir_sisipan_hasil_dahak',
//'akhir_sisipan_no_reg_lab',
//'akhir_tahap_awal_hasil_dahak',
//'akhir_tahap_awal_no_reg_lab',
//'area_sentinel_hasil_test',
//'area_sentinel_tanggal_dianjurkan',
//'area_sentinel_tanggal_pretest_konseling',
//'area_sentinel_tanggal_test_hiv',
//'bb_kg',
//'dirujuk_oleh',
//'hasil_pengobatan_default',
//'hasil_pengobatan_gagal',
//'hasil_pengobatan_lengkap',
//'hasil_pengobatan_meninggal',
//'hasil_pengobatan_pindah',
//'hasil_pengobatan_sembuh',
//'idapp_user',
//'iddata_tb',
//'idfayankes',
//'idjeniskelamin',
//'idjenis_fayankes',
//'idkelurahan',
//'idklasifikasi_penyakit',
//'idkode_paduan_rejimen_yang_diberikan',
//'idpasien',
//'idtipe_pasien',
//'idvalidasi_data',
//'layanan_konseling_hasil_test',
//'layanan_konseling_tanggal_dianjurkan',
//'layanan_konseling_tanggal_pasca_test_konseling',
//'layanan_konseling_tanggal_pretest_konseling',
//'layanan_konseling_tanggal_test_hiv',
//'layanan_konseling_tempat_test',
//'layanan_pdp_tanggal_mulai_art',
//'layanan_pdp_tanggal_mulai_ppk',
//'layanan_pdp_tanggal_rujukan_pdp',
//'nama_pasien',
//'nik',
//'nomor_registrasi_kab_kota',
//'riwayat_tes_hiv_hasil_tes',
//'riwayat_tes_hiv_tanggal_tes_hiv_terakhir',
//'sebelum_pengobatan_hasil_dahak',
//'sebelum_pengobatan_no_reg_lab',
//'systemtime',
//'tanggal',
//'tanggal_lahir',
//'tanggal_mulai_pengobatan',
//'tb_cm',
//'total_scoring_pada_tb_anak',
//'umur',

//'tanggal',
//'validasi_data',
//'nik',
//'nama_pasien',
//'id pasien',
//'jenis kelamin',
//'tanggal_lahir',
//'umur',
//'nomor_registrasi_kab_kota',
//'kelurahan',
//'dirujuk_oleh',
//'fayankes',
//'jenis_fayankes',
//'tanggal_mulai_pengobatan',
//'bb_kg',
//'tb_cm',
//'total_scoring_pada_tb_anak',
//'kode_paduan_rejimen_yang_diberikan',
//'klasifikasi_penyakit',
//'tipe_pasien',
//'sebelum_pengobatan_hasil_dahak',
//'sebelum_pengobatan_no_reg_lab',
//'akhir_tahap_awal_hasil_dahak',
//'akhir_tahap_awal_no_reg_lab',
//'akhir_sisipan_hasil_dahak',
//'akhir_sisipan_no_reg_lab',
//'akhir_bulan_ke_5_7_hasil_dahak',
//'akhir_bulan_ke_5_7_no_reg_lab',
//'akhir_pengobatan_hasil_dahak',
//'akhir_pengobatan_no_reg_lab',
//'hasil_pengobatan_default',
//'hasil_pengobatan_gagal',
//'hasil_pengobatan_lengkap',
//'hasil_pengobatan_meninggal',
//'hasil_pengobatan_pindah',
//'hasil_pengobatan_sembuh',
//'riwayat_tes_hiv_hasil_tes',
//'riwayat_tes_hiv_tanggal_tes_hiv_terakhir',
//'layanan_konseling_hasil_test',
//'layanan_konseling_tanggal_dianjurkan',
//'layanan_konseling_tanggal_pasca_test_konseling',
//'layanan_konseling_tanggal_pretest_konseling',
//'layanan_konseling_tanggal_test_hiv',
//'layanan_konseling_tempat_test',
//'area_sentinel_hasil_test',
//'area_sentinel_tanggal_dianjurkan',
//'area_sentinel_tanggal_pretest_konseling',
//'area_sentinel_tanggal_test_hiv',
//'layanan_pdp_tanggal_mulai_art',
//'layanan_pdp_tanggal_mulai_ppk',
//'layanan_pdp_tanggal_rujukan_pdp',
//'username',
