<?php

class Validasi_Data_Model extends CI_Model
{
    public $idvalidasi_data;
    public $validasi_data;

    public function __construct()
    {
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('*');
        $this->db->from('dinkes_validasi_data');
        if($this->idvalidasi_data) $this->db->where('idvalidasi_data', $this->idvalidasi_data);
        if($this->validasi_data) $this->db->where('validasi_data', $this->validasi_data);
        $query = $this->db->get();
        if($this->idvalidasi_data || $this->validasi_data){
            return $query->row();
        }
        return $query->result();
    }

}
