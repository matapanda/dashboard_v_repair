<?php

class Kode_Paduan_Rejimen_Yang_Diberikan_Model extends CI_Model
{
    public $idkode_paduan_rejimen_yang_diberikan;
    public $kode_paduan_rejimen_yang_diberikan;

    public function __construct()
    {
        $this->load->database();
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from('dinkes_kode_paduan_rejimen_yang_diberikan');
        if($this->idkode_paduan_rejimen_yang_diberikan) $this->db->where('idkode_paduan_rejimen_yang_diberikan', $this->idkode_paduan_rejimen_yang_diberikan);
        if($this->kode_paduan_rejimen_yang_diberikan) $this->db->where('kode_paduan_rejimen_yang_diberikan', $this->kode_paduan_rejimen_yang_diberikan);
        $query = $this->db->get();
        if($this->idkode_paduan_rejimen_yang_diberikan || $this->kode_paduan_rejimen_yang_diberikan){
            return $query->row();
        }
        return $query->result();
    }

}
