<?php

class Tipe_Pasien_Model extends CI_Model
{
    public $idtipe_pasien;
    public $tipe_pasien;

    public function __construct()
    {
        $this->load->database();
    }

    public function get($limit = null, $page = null)
    {
        $this->db->select('*');
        $this->db->from('dinkes_tipe_pasien');
        if($this->idtipe_pasien) $this->db->where('idtipe_pasien', $this->idtipe_pasien);
        if($this->tipe_pasien) $this->db->where('tipe_pasien', $this->tipe_pasien);
        $query = $this->db->get();
        if($this->idtipe_pasien || $this->tipe_pasien){
            return $query->row();
        }
        return $query->result();
    }

}
