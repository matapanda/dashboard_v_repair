<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/**
core route
 */
$route['']['GET'] = 'welcome/index';
$route['dashboard']['GET'] = 'sys/app';
$route['login']['GET'] = 'sys/app/login';

/**
Routing untuk sys
 */
$route['api/user/modules']['GET'] = 'sys/manage/modules/get';
$route['api/user/submodules']['GET'] = 'sys/manage/submodule/get';
$route['api/user/permissions/(:num)']['GET'] = 'sys/manage/usermodules/get/$1';
$route['manage/user/']['GET'] = 'sys/manage/user';

/**
Routing untuk chart
 */
//halaman dasar
$route['chart']['GET'] = 'chart/index';
$route['chart/(:num)']['GET'] = 'chart/show/$1';
/**

/**
Routing untuk CCTV
 */
//halaman dasar
$route['cctv']['GET'] = 'cctv/cctv/index';
$route['cctv/(:num)']['GET'] = 'cctv/cctv/show/$1';

//api
$route['api/app/cctv']['GET'] = 'cctv/cctv/get';
$route['api/app/cctv/(:num)']['GET'] = 'cctv/cctv/get/$1';
$route['api/app/cctv']['POST'] = 'cctv/cctv/store';
$route['api/app/cctv/(:num)']['POST'] = 'cctv/cctv/update/$1';
$route['api/app/cctv/(:num)']['DELETE'] = 'cctv/cctv/delete/$1';

/**
/**
Routing untuk katalog
 */
//katalog
$route['katalog']['GET'] = 'katalog/katalog/index';
$route['katalog/(:num)']['GET'] = 'katalog/katalog/show/$1';

//api
$route['api/app/katalog']['GET'] = 'katalog/katalog/get';
$route['api/app/katalog/count/(:num)']['GET'] = 'katalog/katalog/count/$1';
$route['api/app/katalog/(:num)']['GET'] = 'katalog/katalog/get/$1';
$route['api/app/katalog/(:num)/(:num)']['GET'] = 'katalog/katalog/get/$1/$2';
$route['api/app/katalog/(:num)/(:num)/(:num)']['GET'] = 'katalog/katalog/get/$1/$2/$3';
$route['api/app/katalog/(:num)/(:num)/(:num)/(:any)']['GET'] = 'katalog/katalog/get/$1/$2/$3/$4';
$route['api/app/katalog']['POST'] = 'katalog/katalog/store';
$route['api/app/katalog/(:num)']['POST'] = 'katalog/katalog/update/$1';
$route['api/app/katalog/(:num)']['DELETE'] = 'katalog/katalog/delete/$1';

//kategori katalog
$route['api/app/katalog-kategori']['GET'] = 'katalog/katalog_kategory/get';
$route['api/app/katalog-kategori/(:num)']['GET'] = 'katalog/katalog_kategory/get/$1';
//kategori tipe
$route['api/app/katalog-tipe']['GET'] = 'katalog/katalog_tipe/get';
$route['api/app/katalog-tipe/(:num)']['GET'] = 'katalog/katalog_tipe/get/$1';


/**
/**
Routing untuk master
 */
$route['api/master/hubungan-keluarga']['GET'] = 'master/master/hubungankeluarga';
$route['api/master/jenis-kelamin']['GET'] = 'master/master/jeniskelamin';
$route['api/master/kecamatan']['GET'] = 'master/master/kecamatan';
$route['api/master/kelurahan']['GET'] = 'master/master/kelurahan';
$route['api/master/kota']['GET'] = 'master/master/kota';
$route['api/master/pendidikan']['GET'] = 'master/master/pendidikan';
$route['api/master/propinsi']['GET'] = 'master/master/propinsi';
$route['api/master/skpd']['GET'] = 'master/master/skpd';
$route['api/master/status-kis']['GET'] = 'master/master/statuskis';
$route['api/master/status-individu']['GET'] = 'master/master/statusindividu';
$route['api/master/status-kawin']['GET'] = 'master/master/statuskawin';
$route['api/master/status-keluarga']['GET'] = 'master/master/statuskeluarga';

/**
Routing untuk module dinsos submodule gelandanganpsikotik
 */
//halaman dasar
$route['dinsos/gelandangan-psikotik']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/index';
$route['dinsos/gelandangan-psikotik/add']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/add';
$route['dinsos/gelandangan-psikotik/edit']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/edit';
//api
$route['api/dinsos/gelandangan-psikotik']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get';
$route['api/dinsos/gelandangan-psikotik/datatable']['POST'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get_d';
$route['api/dinsos/gelandangan-psikotik/tanggal-jumlah']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get_based_date';
$route['api/dinsos/gelandangan-psikotik/tanggal-jumlah/(:num)']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get_based_date/$1';
$route['api/dinsos/gelandangan-psikotik/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get_based_date/$1/$2';
$route['api/dinsos/gelandangan-psikotik/tanggal-jumlah/(:num)/(:num)/(:num)']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get_based_date/$1/$2/$3';
$route['api/dinsos/gelandangan-psikotik/(:num)']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get/$1';
$route['api/dinsos/gelandangan-psikotik/(:num)/(:num)']['GET'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/get/0/$1/$2';
$route['api/dinsos/gelandangan-psikotik']['POST'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/store';
$route['api/dinsos/gelandangan-psikotik/(:num)']['POST'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/update/$1';
$route['api/dinsos/gelandangan-psikotik/(:num)']['DELETE'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/delete/$1';
$route['api/dinsos/gelandangan-psikotik/upload']['POST'] = 'dinsos/gelandangan_psikotik/gelandangan_psikotik/uploadExcel';


/**
Routing untuk module dinsos submodule kispbid
 */
//halaman dasar
$route['dinsos/kispbid']['GET'] = 'dinsos/kispbid/kispbid/index';
$route['dinsos/kispbid/add']['GET'] = 'dinsos/kispbid/kispbid/add';
$route['dinsos/kispbid/edit']['GET'] = 'dinsos/kispbid/kispbid/edit';
//api
$route['api/dinsos/kispbid']['GET'] = 'dinsos/kispbid/kispbid/get';
$route['api/dinsos/kispbid/datatable']['POST'] = 'dinsos/kispbid/kispbid/get_d';
$route['api/dinsos/kispbid/tanggal-jumlah']['GET'] = 'dinsos/kispbid/kispbid/get_based_date';
$route['api/dinsos/kispbid/tanggal-jumlah/(:num)']['GET'] = 'dinsos/kispbid/kispbid/get_based_date/$1';
$route['api/dinsos/kispbid/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/kispbid/kispbid/get_based_date/$1/$2';
$route['api/dinsos/kispbid/kelurahan']['GET'] = 'dinsos/kispbid/kispbid/get_based_kelurahan';
$route['api/dinsos/kispbid/kelurahan/(:num)']['GET'] = 'dinsos/kispbid/kispbid/get_based_kelurahan/$1';
$route['api/dinsos/kispbid/kelurahan/(:num)/(:num)']['GET'] = 'dinsos/kispbid/kispbid/get_based_kelurahan/$1/$2';
$route['api/dinsos/kispbid/(:num)']['GET'] = 'dinsos/kispbid/kispbid/get/$1';
$route['api/dinsos/kispbid/(:num)/(:num)']['GET'] = 'dinsos/kispbid/kispbid/get/0/$1/$2';
$route['api/dinsos/kispbid']['POST'] = 'dinsos/kispbid/kispbid/store';
$route['api/dinsos/kispbid/(:num)']['POST'] = 'dinsos/kispbid/kispbid/update/$1';
$route['api/dinsos/kispbid/(:num)']['DELETE'] = 'dinsos/kispbid/kispbid/delete/$1';
$route['api/dinsos/kispbid/upload']['POST'] = 'dinsos/kispbid/kispbid/uploadExcel';


/**
Routing untuk module dinsos submodule kispbin
 */
//halaman dasar
$route['dinsos/kispbin']['GET'] = 'dinsos/kispbin/kispbin/index';
$route['dinsos/kispbin/add']['GET'] = 'dinsos/kispbin/kispbin/add';
$route['dinsos/kispbin/edit']['GET'] = 'dinsos/kispbin/kispbin/edit';
//api
$route['api/dinsos/kispbin']['GET'] = 'dinsos/kispbin/kispbin/get';
$route['api/dinsos/kispbin/datatable']['POST'] = 'dinsos/kispbin/kispbin/get_d';
$route['api/dinsos/kispbin/tanggal-jumlah']['GET'] = 'dinsos/kispbin/kispbin/get_based_date';
$route['api/dinsos/kispbin/tanggal-jumlah/(:num)']['GET'] = 'dinsos/kispbin/kispbin/get_based_date/$1';
$route['api/dinsos/kispbin/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/kispbin/kispbin/get_based_date/$1/$2';
$route['api/dinsos/kispbin/tanggal-jumlah/(:num)/(:num)/(:num)']['GET'] = 'dinsos/kispbin/kispbin/get_based_date/$1/$2/$3';
$route['api/dinsos/kispbin/kelurahan']['GET'] = 'dinsos/kispbin/kispbin/get_based_kelurahan';
$route['api/dinsos/kispbin/kelurahan/(:num)']['GET'] = 'dinsos/kispbin/kispbin/get_based_kelurahan/$1';
$route['api/dinsos/kispbin/kelurahan/(:num)/(:num)']['GET'] = 'dinsos/kispbin/kispbin/get_based_kelurahan/$1/$2';
$route['api/dinsos/kispbin/(:num)']['GET'] = 'dinsos/kispbin/kispbin/get/$1';
$route['api/dinsos/kispbin/(:num)/(:num)']['GET'] = 'dinsos/kispbin/kispbin/get/0/$1/$2';
$route['api/dinsos/kispbin']['POST'] = 'dinsos/kispbin/kispbin/store';
$route['api/dinsos/kispbin/(:num)']['POST'] = 'dinsos/kispbin/kispbin/update/$1';
$route['api/dinsos/kispbin/(:num)']['DELETE'] = 'dinsos/kispbin/kispbin/delete/$1';
$route['api/dinsos/kispbin/upload']['POST'] = 'dinsos/kispbin/kispbin/uploadExcel';

/**
Routing untuk module dinsos submodule kks
 */
//halaman dasar
$route['dinsos/kks']['GET'] = 'dinsos/kks/kks/index';
$route['dinsos/kks/add']['GET'] = 'dinsos/kks/kks/add';
$route['dinsos/kks/edit']['GET'] = 'dinsos/kks/kks/edit';
//api
$route['api/dinsos/kks']['GET'] = 'dinsos/kks/kks/get';
$route['api/dinsos/kks/datatable']['POST'] = 'dinsos/kks/kks/get_d';
$route['api/dinsos/kks/tanggal-jumlah']['GET'] = 'dinsos/kks/kks/get_based_date';
$route['api/dinsos/kks/tanggal-jumlah/(:num)']['GET'] = 'dinsos/kks/kks/get_based_date/$1';
$route['api/dinsos/kks/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/kks/kks/get_based_date/$1/$2';
$route['api/dinsos/kks/tanggal-jumlah/(:num)/(:num)/(:num)']['GET'] = 'dinsos/kks/kks/get_based_date/$1/$2/$3';
$route['api/dinsos/kks/kelurahan']['GET'] = 'dinsos/kks/kks/get_based_kelurahan';
$route['api/dinsos/kks/kelurahan/(:num)']['GET'] = 'dinsos/kks/kks/get_based_kelurahan/$1';
$route['api/dinsos/kks/kelurahan/(:num)/(:num)']['GET'] = 'dinsos/kks/kks/get_based_kelurahan/$1/$2';
$route['api/dinsos/kks/(:num)']['GET'] = 'dinsos/kks/kks/get/$1';
$route['api/dinsos/kks/(:num)/(:num)']['GET'] = 'dinsos/kks/kks/get/0/$1/$2';
$route['api/dinsos/kks']['POST'] = 'dinsos/kks/kks/store';
$route['api/dinsos/kks/(:num)']['POST'] = 'dinsos/kks/kks/update/$1';
$route['api/dinsos/kks/(:num)']['DELETE'] = 'dinsos/kks/kks/delete/$1';
$route['api/dinsos/kks/upload']['POST'] = 'dinsos/kks/kks/uploadExcel';

/**
Routing untuk module dinsos submodule pkh
 */
//halaman dasar
$route['dinsos/pkh']['GET'] = 'dinsos/pkh/pkh/index';
$route['dinsos/pkh/add']['GET'] = 'dinsos/pkh/pkh/add';
$route['dinsos/pkh/edit']['GET'] = 'dinsos/pkh/pkh/edit';
//api
$route['api/dinsos/pkh']['GET'] = 'dinsos/pkh/pkh/get';
$route['api/dinsos/pkh/datatable']['POST'] = 'dinsos/pkh/pkh/get_d';
$route['api/dinsos/pkh/tanggal-jumlah']['GET'] = 'dinsos/pkh/pkh/get_based_date';
$route['api/dinsos/pkh/tanggal-jumlah/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_date/$1';
$route['api/dinsos/pkh/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_date/$1/$2';
$route['api/dinsos/pkh/tanggal-jumlah/(:num)/(:num)/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_date/$1/$2/$3';
$route['api/dinsos/pkh/kelurahan']['GET'] = 'dinsos/pkh/pkh/get_based_kelurahan';
$route['api/dinsos/pkh/kelurahan/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_kelurahan/$1';
$route['api/dinsos/pkh/kelurahan/(:num)/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_kelurahan/$1/$2';
$route['api/dinsos/pkh/kategori']['GET'] = 'dinsos/pkh/pkh/get_based_kategori';
$route['api/dinsos/pkh/kategori/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_kategori/$1';
$route['api/dinsos/pkh/kategori/(:num)/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_kategori/$1/$2';
$route['api/dinsos/pkh/kategori/(:num)/(:num)/(:num)']['GET'] = 'dinsos/pkh/pkh/get_based_kategori/$1/$2/$3';

$route['api/dinsos/pkh/(:num)']['GET'] = 'dinsos/pkh/pkh/get/$1';
$route['api/dinsos/pkh/(:num)/(:num)']['GET'] = 'dinsos/pkh/pkh/get/0/$1/$2';
$route['api/dinsos/pkh']['POST'] = 'dinsos/pkh/pkh/store';
$route['api/dinsos/pkh/(:num)']['POST'] = 'dinsos/pkh/pkh/update/$1';
$route['api/dinsos/pkh/(:num)']['DELETE'] = 'dinsos/pkh/pkh/delete/$1';
$route['api/dinsos/pkh/kategori-pkh']['GET'] = 'dinsos/pkh/kategori_pkh/get';
$route['api/dinsos/pkh/kategori-pkh/(:num)']['GET'] = 'dinsos/pkh/kategori_pkh/get/$1';
$route['api/dinsos/pkh/kategori-pkh']['POST'] = 'dinsos/pkh/kategori_pkh/store';
$route['api/dinsos/pkh/kategori-pkh/(:num)']['POST'] = 'dinsos/pkh/kategori_pkh/update/$1';
$route['api/dinsos/pkh/kategori-pkh/(:num)']['DELETE'] = 'dinsos/pkh/kategori_pkh/delete/$1';
$route['api/dinsos/pkh/upload']['POST'] = 'dinsos/pkh/pkh/uploadExcel';

/**
Routing untuk module dinsos submodule pps
 */
//halaman dasar
$route['dinsos/pps']['GET'] = 'dinsos/pps/pps/index';
$route['dinsos/pps/add']['GET'] = 'dinsos/pps/pps/add';
$route['dinsos/pps/edit']['GET'] = 'dinsos/pps/pps/edit';
//api
$route['api/dinsos/pps']['GET'] = 'dinsos/pps/pps/get';
$route['api/dinsos/pps/datatable']['POST'] = 'dinsos/pps/pps/get_d';
$route['api/dinsos/pps/tanggal-jumlah']['GET'] = 'dinsos/pps/pps/get_based_date';
$route['api/dinsos/pps/tanggal-jumlah/(:num)']['GET'] = 'dinsos/pps/pps/get_based_date/$1';
$route['api/dinsos/pps/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/pps/pps/get_based_date/$1/$2';
$route['api/dinsos/pps/tanggal-jumlah/(:num)/(:num)/(:num)']['GET'] = 'dinsos/pps/pps/get_based_date/$1/$2/$3';
$route['api/dinsos/pps/kelurahan']['GET'] = 'dinsos/pps/pps/get_based_kelurahan';
$route['api/dinsos/pps/kelurahan/(:num)']['GET'] = 'dinsos/pps/pps/get_based_kelurahan/$1';
$route['api/dinsos/pps/kelurahan/(:num)/(:num)']['GET'] = 'dinsos/pps/pps/get_based_kelurahan/$1/$2';
$route['api/dinsos/pps/(:num)']['GET'] = 'dinsos/pps/pps/get/$1';
$route['api/dinsos/pps/(:num)/(:num)']['GET'] = 'dinsos/pps/pps/get/0/$1/$2';
$route['api/dinsos/pps']['POST'] = 'dinsos/pps/pps/store';
$route['api/dinsos/pps/(:num)']['POST'] = 'dinsos/pps/pps/update/$1';
$route['api/dinsos/pps/(:num)']['DELETE'] = 'dinsos/pps/pps/delete/$1';
$route['api/dinsos/pps/upload']['POST'] = 'dinsos/pps/pps/uploadExcel';

/**
Routing untuk module dinsos submodule razia
 */
//halaman dasar
$route['dinsos/razia']['GET'] = 'dinsos/razia/razia/index';
$route['dinsos/razia/add']['GET'] = 'dinsos/razia/razia/add';
$route['dinsos/razia/edit']['GET'] = 'dinsos/razia/razia/edit';

//api
$route['api/dinsos/razia']['GET'] = 'dinsos/razia/razia/get';
$route['api/dinsos/razia/datatable']['POST'] = 'dinsos/razia/razia/get_d';
$route['api/dinsos/razia/tanggal-jumlah']['GET'] = 'dinsos/razia/razia/get_based_date';
$route['api/dinsos/razia/tanggal-jumlah/(:num)']['GET'] = 'dinsos/razia/razia/get_based_date/$1';
$route['api/dinsos/razia/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/razia/razia/get_based_date/$1/$2';
$route['api/dinsos/razia/tanggal-jumlah/(:num)/(:num)/(:num)']['GET'] = 'dinsos/razia/razia/get_based_date/$1/$2/$3';
$route['api/dinsos/razia/kelurahan']['GET'] = 'dinsos/razia/razia/get_based_kelurahan';
$route['api/dinsos/razia/kelurahan/(:num)']['GET'] = 'dinsos/razia/razia/get_based_kelurahan/$1';
$route['api/dinsos/razia/kelurahan/(:num)/(:num)']['GET'] = 'dinsos/razia/razia/get_based_kelurahan/$1/$2';
$route['api/dinsos/razia/(:num)']['GET'] = 'dinsos/razia/razia/get/$1';
$route['api/dinsos/razia/(:num)/(:num)']['GET'] = 'dinsos/razia/razia/get/0/$1/$2';
$route['api/dinsos/razia']['POST'] = 'dinsos/razia/razia/store';
$route['api/dinsos/razia/(:num)']['POST'] = 'dinsos/razia/razia/update/$1';
$route['api/dinsos/razia/(:num)']['DELETE'] = 'dinsos/razia/razia/delete/$1';
$route['api/dinsos/razia/kategori-razia']['GET'] = 'dinsos/razia/kategori_razia/get';
$route['api/dinsos/razia/kategori-razia/(:num)']['GET'] = 'dinsos/razia/kategori_razia/get/$1';
$route['api/dinsos/razia/kategori-razia']['POST'] = 'dinsos/razia/kategori_razia/store';
$route['api/dinsos/razia/kategori-razia/(:num)']['POST'] = 'dinsos/razia/kategori_razia/update/$1';
$route['api/dinsos/razia/kategori-razia/(:num)']['DELETE'] = 'dinsos/razia/kategori_razia/delete/$1';
$route['api/dinsos/razia/upload']['POST'] = 'dinsos/razia/razia/uploadExcel';

/**
Routing untuk module dinsos submodule resos
 */
//halaman dasar
$route['dinsos/resos']['GET'] = 'dinsos/resos/resos/index';
$route['dinsos/resos/add']['GET'] = 'dinsos/resos/resos/add';
$route['dinsos/resos/edit']['GET'] = 'dinsos/resos/resos/edit';

//api
$route['api/dinsos/resos']['GET'] = 'dinsos/resos/resos/get';
$route['api/dinsos/resos/datatable']['POST'] = 'dinsos/resos/resos/get_d';
$route['api/dinsos/resos/tanggal-jumlah']['GET'] = 'dinsos/resos/resos/get_based_date';
$route['api/dinsos/resos/tanggal-jumlah/(:num)']['GET'] = 'dinsos/resos/resos/get_based_date/$1';
$route['api/dinsos/resos/tanggal-jumlah/(:num)/(:num)']['GET'] = 'dinsos/resos/resos/get_based_date/$1/$2';
$route['api/dinsos/resos/tanggal-jumlah/(:num)/(:num)/(:num)']['GET'] = 'dinsos/resos/resos/get_based_date/$1/$2/$3';
$route['api/dinsos/resos/kelurahan']['GET'] = 'dinsos/resos/resos/get_based_kelurahan';
$route['api/dinsos/resos/kelurahan/(:num)']['GET'] = 'dinsos/resos/resos/get_based_kelurahan/$1';
$route['api/dinsos/resos/kelurahan/(:num)/(:num)']['GET'] = 'dinsos/resos/resos/get_based_kelurahan/$1/$2';
$route['api/dinsos/resos/(:num)']['GET'] = 'dinsos/resos/resos/get/$1';
$route['api/dinsos/resos/(:num)/(:num)']['GET'] = 'dinsos/resos/resos/get/0/$1/$2';
$route['api/dinsos/resos']['POST'] = 'dinsos/resos/resos/store';
$route['api/dinsos/resos/(:num)']['POST'] = 'dinsos/resos/resos/update/$1';
$route['api/dinsos/resos/(:num)']['DELETE'] = 'dinsos/resos/resos/delete/$1';
$route['api/dinsos/resos/kategori-resos']['GET'] = 'dinsos/resos/kategori_resos/get';
$route['api/dinsos/resos/kategori-resos/(:num)']['GET'] = 'dinsos/resos/kategori_resos/get/$1';
$route['api/dinsos/resos/kategori-resos']['POST'] = 'dinsos/resos/kategori_resos/store';
$route['api/dinsos/resos/kategori-resos/(:num)']['POST'] = 'dinsos/resos/kategori_resos/update/$1';
$route['api/dinsos/resos/kategori-resos/(:num)']['DELETE'] = 'dinsos/resos/kategori_resos/delete/$1';
$route['api/dinsos/resos/upload']['POST'] = 'dinsos/resos/resos/uploadExcel';

/**
Routing untuk module dp3p2kb submodule kdrt
 */
//halaman dasar
$route['dp3p2kb/kdrt']['GET'] = 'dp3p2kb/kdrt/kdrt/index';
$route['dp3p2kb/kdrt/add']['GET'] = 'dp3p2kb/kdrt/kdrt/add';
$route['dp3p2kb/kdrt/edit']['GET'] = 'dp3p2kb/kdrt/kdrt/edit';

//api
$route['api/dp3p2kb/kdrt']['GET'] = 'dp3p2kb/kdrt/kdrt/get';
$route['api/dp3p2kb/kdrt/datatable']['POST'] = 'dp3p2kb/kdrt/kdrt/get_d';
$route['api/dp3p2kb/kdrt/(:num)']['GET'] = 'dp3p2kb/kdrt/kdrt/get/$1';
$route['api/dp3p2kb/kdrt/(:num)/(:num)']['GET'] = 'dp3p2kb/kdrt/kdrt/get/0/$1/$2';
$route['api/dp3p2kb/kdrt']['POST'] = 'dp3p2kb/kdrt/kdrt/store';
$route['api/dp3p2kb/kdrt/(:num)']['POST'] = 'dp3p2kb/kdrt/kdrt/update/$1';
$route['api/dp3p2kb/kdrt/(:num)']['DELETE'] = 'dp3p2kb/kdrt/kdrt/delete/$1';
$route['api/dp3p2kb/kdrt/jenis-kasus']['GET'] = 'dp3p2kb/kdrt/jeniskasus/get';
$route['api/dp3p2kb/kdrt/jenis-korban']['GET'] = 'dp3p2kb/kdrt/jeniskorban/get';

/**
Routing untuk module dp3p2kb submodule datapkk
 */
//halaman dasar
$route['dp3p2kb/data-pkk']['GET'] = 'dp3p2kb/datapkk/datapkk/index';
$route['dp3p2kb/data-pkk/add']['GET'] = 'dp3p2kb/datapkk/datapkk/add';
$route['dp3p2kb/data-pkk/edit']['GET'] = 'dp3p2kb/datapkk/datapkk/edit';

//api
$route['api/dp3p2kb/data-pkk']['GET'] = 'dp3p2kb/datapkk/datapkk/get';
$route['api/dp3p2kb/data-pkk/datatable']['POST'] = 'dp3p2kb/datapkk/datapkk/get_d';
$route['api/dp3p2kb/data-pkk/(:num)']['GET'] = 'dp3p2kb/datapkk/datapkk/get/$1';
$route['api/dp3p2kb/data-pkk/(:num)/(:num)']['GET'] = 'dp3p2kb/datapkk/datapkk/get/0/$1/$2';
$route['api/dp3p2kb/data-pkk']['POST'] = 'dp3p2kb/datapkk/datapkk/store';
$route['api/dp3p2kb/data-pkk/(:num)']['POST'] = 'dp3p2kb/datapkk/datapkk/update/$1';
$route['api/dp3p2kb/data-pkk/(:num)']['DELETE'] = 'dp3p2kb/datapkk/datapkk/delete/$1';

/**
Routing untuk module dp3p2kb submodule hasilpendapatankeluarga
 */
//halaman dasar
$route['dp3p2kb/hasil-pendapatan-keluarga']['GET'] = 'dp3p2kb/hpk/hpk/index';
$route['dp3p2kb/hasil-pendapatan-keluarga/add']['GET'] = 'dp3p2kb/hpk/hpk/add';
$route['dp3p2kb/hasil-pendapatan-keluarga/edit']['GET'] = 'dp3p2kb/hpk/hpk/edit';

//api
$route['api/dp3p2kb/hasil-pendapatan-keluarga']['GET'] = 'dp3p2kb/hpk/hpk/get';
$route['api/dp3p2kb/hasil-pendapatan-keluarga/datatable']['POST'] = 'dp3p2kb/hpk/hpk/get_d';
$route['api/dp3p2kb/hasil-pendapatan-keluarga/(:num)']['GET'] = 'dp3p2kb/hpk/hpk/get/$1';
$route['api/dp3p2kb/hasil-pendapatan-keluarga/(:num)/(:num)']['GET'] = 'dp3p2kb/hpk/hpk/get/0/$1/$2';
$route['api/dp3p2kb/hasil-pendapatan-keluarga']['POST'] = 'dp3p2kb/hpk/hpk/store';
$route['api/dp3p2kb/hasil-pendapatan-keluarga/(:num)']['POST'] = 'dp3p2kb/hpk/hpk/update/$1';
$route['api/dp3p2kb/hasil-pendapatan-keluarga/(:num)']['DELETE'] = 'dp3p2kb/hpk/hpk/delete/$1';

/**
Routing untuk module dp3p2kb submodule hasilpendapatankeluargatahu
 */
//halaman dasar
$route['dp3p2kb/hasil-pendapatan-keluarga-tahu']['GET'] = 'dp3p2kb/hpkt/hpkt/index';
$route['dp3p2kb/hasil-pendapatan-keluarga-tahu/add']['GET'] = 'dp3p2kb/hpkt/hpkt/add';
$route['dp3p2kb/hasil-pendapatan-keluarga-tahu/edit']['GET'] = 'dp3p2kb/hpkt/hpkt/edit';

//api
$route['api/dp3p2kb/hasil-pendapatan-keluarga-tahu']['GET'] = 'dp3p2kb/hpkt/hpkt/get';
$route['api/dp3p2kb/hasil-pendapatan-keluarga-tahu/datatable']['POST'] = 'dp3p2kb/hpkt/hpkt/get_d';
$route['api/dp3p2kb/hasil-pendapatan-keluarga-tahu/(:num)']['GET'] = 'dp3p2kb/hpkt/hpkt/get/$1';
$route['api/dp3p2kb/hasil-pendapatan-keluarga-tahu/(:num)/(:num)']['GET'] = 'dp3p2kb/hpkt/hpkt/get/0/$1/$2';
$route['api/dp3p2kb/hasil-pendapatan-keluarga-tahu']['POST'] = 'dp3p2kb/hpkt/hpkt/store';
$route['api/dp3p2kb/hasil-pendapatan-keluarga-tahu/(:num)']['POST'] = 'dp3p2kb/hpkt/hpkt/update/$1';
$route['api/dp3p2kb/hasil-pendapatan-keluarga-tahu/(:num)']['DELETE'] = 'dp3p2kb/hpkt/hpkt/delete/$1';

/**
Routing untuk module dp3p2kb submodule dataposyandu
 */
//halaman dasar
$route['dp3p2kb/data-posyandu']['GET'] = 'dp3p2kb/dataposyandu/dataposyandu/index';
$route['dp3p2kb/data-posyandu/add']['GET'] = 'dp3p2kb/dataposyandu/dataposyandu/add';
$route['dp3p2kb/data-posyandu/edit']['GET'] = 'dp3p2kb/dataposyandu/dataposyandu/edit';

//api
$route['api/dp3p2kb/data-posyandu']['GET'] = 'dp3p2kb/dataposyandu/dataposyandu/get';
$route['api/dp3p2kb/data-posyandu/datatable']['POST'] = 'dp3p2kb/dataposyandu/dataposyandu/get_d';
$route['api/dp3p2kb/data-posyandu/(:num)']['GET'] = 'dp3p2kb/dataposyandu/dataposyandu/get/$1';
$route['api/dp3p2kb/data-posyandu/(:num)/(:num)']['GET'] = 'dp3p2kb/dataposyandu/dataposyandu/get/0/$1/$2';
$route['api/dp3p2kb/data-posyandu']['POST'] = 'dp3p2kb/dataposyandu/dataposyandu/store';
$route['api/dp3p2kb/data-posyandu/(:num)']['POST'] = 'dp3p2kb/dataposyandu/dataposyandu/update/$1';
$route['api/dp3p2kb/data-posyandu/(:num)']['DELETE'] = 'dp3p2kb/dataposyandu/dataposyandu/delete/$1';

/**
Routing untuk module perpus master
 */
//api
$route['api/perpus/master/bahasa']['GET'] = 'perpus/master/bahasa/get';
$route['api/perpus/master/jenis-koleksi']['GET'] = 'perpus/master/jeniskoleksi/get';
$route['api/perpus/master/kelas']['GET'] = 'perpus/master/kelas/get';

/**
Routing untuk module perpus submodule rekapitulasi koleksi
 */
//halaman dasar
$route['perpus/rekapitulasi-koleksi']['GET'] = 'perpus/koleksi/koleksi/index';
$route['perpus/rekapitulasi-koleksi/add']['GET'] = 'perpus/koleksi/koleksi/add';
$route['perpus/rekapitulasi-koleksi/edit']['GET'] = 'perpus/koleksi/koleksi/edit';

//api
$route['api/perpus/rekapitulasi-koleksi']['GET'] = 'perpus/koleksi/koleksi/get';
$route['api/perpus/rekapitulasi-koleksi/datatable']['POST'] = 'perpus/koleksi/koleksi/get_d';
$route['api/perpus/rekapitulasi-koleksi/(:num)']['GET'] = 'perpus/koleksi/koleksi/get/$1';
$route['api/perpus/rekapitulasi-koleksi/(:num)/(:num)']['GET'] = 'perpus/koleksi/koleksi/get/0/$1/$2';
$route['api/perpus/rekapitulasi-koleksi']['POST'] = 'perpus/koleksi/koleksi/store';
$route['api/perpus/rekapitulasi-koleksi/(:num)']['POST'] = 'perpus/koleksi/koleksi/update/$1';
$route['api/perpus/rekapitulasi-koleksi/(:num)']['DELETE'] = 'perpus/koleksi/koleksi/delete/$1';

/**
Routing untuk module perpus submodule statistik peminjaman
 */
//halaman dasar
$route['perpus/statistik-peminjaman']['GET'] = 'perpus/peminjaman/peminjaman/index';
$route['perpus/statistik-peminjaman/add']['GET'] = 'perpus/peminjaman/peminjaman/add';
$route['perpus/statistik-peminjaman/edit']['GET'] = 'perpus/peminjaman/peminjaman/edit';

//api
$route['api/perpus/statistik-peminjaman']['GET'] = 'perpus/peminjaman/peminjaman/get';
$route['api/perpus/statistik-peminjaman/datatable']['POST'] = 'perpus/peminjaman/peminjaman/get_d';
$route['api/perpus/statistik-peminjaman/(:num)']['GET'] = 'perpus/peminjaman/peminjaman/get/$1';
$route['api/perpus/statistik-peminjaman/(:num)/(:num)']['GET'] = 'perpus/peminjaman/peminjaman/get/0/$1/$2';
$route['api/perpus/statistik-peminjaman']['POST'] = 'perpus/peminjaman/peminjaman/store';
$route['api/perpus/statistik-peminjaman/(:num)']['POST'] = 'perpus/peminjaman/peminjaman/update/$1';
$route['api/perpus/statistik-peminjaman/(:num)']['DELETE'] = 'perpus/peminjaman/peminjaman/delete/$1';

/**
Routing untuk module perumahan submodule pertamanan
 */
//halaman dasar
$route['perkim/pertamanan']['GET'] = 'perkim/pertamanan/pertamanan/index';

//api
$route['api/perkim/pertamanan']['GET'] = 'perkim/pertamanan/pertamanan/get';
$route['api/perkim/pertamanan/datatable']['POST'] = 'perkim/pertamanan/pertamanan/get_d';
$route['api/perkim/pertamanan/(:num)']['GET'] = 'perkim/pertamanan/pertamanan/get/$1';
$route['api/perkim/pertamanan/(:num)/(:num)']['GET'] = 'perkim/pertamanan/pertamanan/get/0/$1/$2';
$route['api/perkim/pertamanan']['POST'] = 'perkim/pertamanan/pertamanan/store';
$route['api/perkim/pertamanan/(:num)']['POST'] = 'perkim/pertamanan/pertamanan/update/$1';
$route['api/perkim/pertamanan/(:num)']['DELETE'] = 'perkim/pertamanan/pertamanan/delete/$1';
$route['api/perkim/pertamanan/upload-images'] = 'perkim/pertamanan/pertamanan/upload_images';

//api
$route['api/perkim/jenis-pertamanan']['GET'] = 'perkim/pertamanan/jenispertamanan/get';
$route['api/perkim/jenis-pertamanan/(:num)']['GET'] = 'perkim/pertamanan/jenispertamanan/get/$1';
$route['api/perkim/jenis-pertamanan/(:num)/(:num)']['GET'] = 'perumahan/pertamanan/jenispertamanan/get/0/$1/$2';

/**
Routing untuk module perumahan submodule pemakaman
 */
//halaman dasar
$route['perkim/pemakaman']['GET'] = 'perkim/pemakaman/pemakaman/index';

//api
$route['api/perkim/pemakaman']['GET'] = 'perkim/pemakaman/pemakaman/get';
$route['api/perkim/pemakaman/datatable']['POST'] = 'perkim/pemakaman/pemakaman/get_d';
$route['api/perkim/pemakaman/(:num)']['GET'] = 'perkim/pemakaman/pemakaman/get/$1';
$route['api/perkim/pemakaman/(:num)/(:num)']['GET'] = 'perkim/pemakaman/pemakaman/get/0/$1/$2';
$route['api/perkim/pemakaman']['POST'] = 'perkim/pemakaman/pemakaman/store';
$route['api/perkim/pemakaman/(:num)']['POST'] = 'perkim/pemakaman/pemakaman/update/$1';
$route['api/perkim/pemakaman/(:num)']['DELETE'] = 'perkim/pemakaman/pemakaman/delete/$1';

/**
Routing untuk module dispenduk submodule aktacatatansipil
 */
//halaman dasar
$route['dinas-kependudukan/akta-catatan-sipil']['GET'] = 'dispenduk/aktacatatansipil/aktacatatansipil/index';

//api
$route['api/dinas-kependudukan/akta-catatan-sipil']['GET'] = 'dispenduk/aktacatatansipil/aktacatatansipil/get';
$route['api/dinas-kependudukan/akta-catatan-sipil/datatable']['POST'] = 'dispenduk/aktacatatansipil/aktacatatansipil/get_d';
$route['api/dinas-kependudukan/akta-catatan-sipil/(:num)']['GET'] = 'dispenduk/aktacatatansipil/aktacatatansipil/get/$1';
$route['api/dinas-kependudukan/akta-catatan-sipil/(:num)/(:num)']['GET'] = 'dispenduk/aktacatatansipil/aktacatatansipil/get/0/$1/$2';
$route['api/dinas-kependudukan/akta-catatan-sipil']['POST'] = 'dispenduk/aktacatatansipil/aktacatatansipil/store';
$route['api/dinas-kependudukan/akta-catatan-sipil/(:num)']['POST'] = 'dispenduk/aktacatatansipil/aktacatatansipil/update/$1';
$route['api/dinas-kependudukan/akta-catatan-sipil/(:num)']['DELETE'] = 'dispenduk/aktacatatansipil/aktacatatansipil/delete/$1';
$route['api/dinas-kependudukan/akta-catatan-sipil/upload']['POST'] = 'dispenduk/aktacatatansipil/aktacatatansipil/uploadExcel';

/**
Routing untuk module perumahan submodule rusunawa
 */
//halaman dasar
$route['perkim/rusunawa']['GET'] = 'perkim/rusunawa/rusunawa/index';

//api
$route['api/perkim/rusunawa']['GET'] = 'perkim/rusunawa/rusunawa/get';
$route['api/perkim/rusunawa/datatable']['POST'] = 'perkim/rusunawa/rusunawa/get_d';
$route['api/perkim/rusunawa/(:num)']['GET'] = 'perkim/rusunawa/rusunawa/get/$1';
$route['api/perkim/rusunawa/(:num)/(:num)']['GET'] = 'perkim/rusunawa/rusunawa/get/0/$1/$2';
$route['api/perkim/rusunawa']['POST'] = 'perkim/rusunawa/rusunawa/store';
$route['api/perkim/rusunawa/(:num)']['POST'] = 'perkim/rusunawa/rusunawa/update/$1';
$route['api/perkim/rusunawa/(:num)']['DELETE'] = 'perkim/rusunawa/rusunawa/delete/$1';

//api
$route['api/perkim/lokasi-rusunawa']['GET'] = 'perkim/rusunawa/lokasirusunawa/get';
$route['api/perkim/lokasi-rusunawa/(:num)']['GET'] = 'perkim/rusunawa/lokasirusunawa/get/$1';
$route['api/perkim/lokasi-rusunawa/(:num)/(:num)']['GET'] = 'perumahan/rusunawa/lokasirusunawa/get/0/$1/$2';

/**
Routing untuk module hukum submodule perwal
 */
//halaman dasar
$route['hukum/perwal']['GET'] = 'hukum/perwal/perwal/index';

//api
$route['api/hukum/perwal']['GET'] = 'hukum/perwal/perwal/get';
$route['api/hukum/perwal/datatable']['POST'] = 'hukum/perwal/perwal/get_d';
$route['api/hukum/perwal/(:num)']['GET'] = 'hukum/perwal/perwal/get/$1';
$route['api/hukum/perwal/(:num)/(:num)']['GET'] = 'hukum/perwal/perwal/get/0/$1/$2';
$route['api/hukum/perwal']['POST'] = 'hukum/perwal/perwal/store';
$route['api/hukum/perwal/(:num)']['POST'] = 'hukum/perwal/perwal/update/$1';
$route['api/hukum/perwal/(:num)']['DELETE'] = 'hukum/perwal/perwal/delete/$1';

/**
Routing untuk module hukum submodule perwal
 */
//halaman dasar
$route['hukum/perda']['GET'] = 'hukum/perda/perda/index';

//api
$route['api/hukum/perda']['GET'] = 'hukum/perda/perda/get';
$route['api/hukum/perda/datatable']['POST'] = 'hukum/perda/perda/get_d';
$route['api/hukum/perda/(:num)']['GET'] = 'hukum/perda/perda/get/$1';
$route['api/hukum/perda/(:num)/(:num)']['GET'] = 'hukum/perda/perda/get/0/$1/$2';
$route['api/hukum/perda']['POST'] = 'hukum/perda/perda/store';
$route['api/hukum/perda/(:num)']['POST'] = 'hukum/perda/perda/update/$1';
$route['api/hukum/perda/(:num)']['DELETE'] = 'hukum/perda/perda/delete/$1';

/**
Routing untuk module dispenduk submodule orangasing
 */
//halaman dasar
$route['dinas-kependudukan/orang-asing']['GET'] = 'dispenduk/orangasing/orangasing/index';

//api
$route['api/dinas-kependudukan/orang-asing']['GET'] = 'dispenduk/orangasing/orangasing/get';
$route['api/dinas-kependudukan/orang-asing/datatable']['POST'] = 'dispenduk/orangasing/orangasing/get_d';
$route['api/dinas-kependudukan/orang-asing/(:num)']['GET'] = 'dispenduk/orangasing/orangasing/get/$1';
$route['api/dinas-kependudukan/orang-asing/(:num)/(:num)']['GET'] = 'dispenduk/orangasing/orangasing/get/0/$1/$2';
$route['api/dinas-kependudukan/orang-asing']['POST'] = 'dispenduk/orangasing/orangasing/store';
$route['api/dinas-kependudukan/orang-asing/(:num)']['POST'] = 'dispenduk/orangasing/orangasing/update/$1';
$route['api/dinas-kependudukan/orang-asing/(:num)']['DELETE'] = 'dispenduk/orangasing/orangasing/delete/$1';
$route['api/dinas-kependudukan/orang-asing/upload']['POST'] = 'dispenduk/orangasing/orangasing/uploadExcel';

/**
Routing untuk module dispenduk submodule penduduk
 */
//halaman dasar
$route['dinas-kependudukan/penduduk']['GET'] = 'dispenduk/penduduk/penduduk/index';

//api
$route['api/dinas-kependudukan/penduduk']['GET'] = 'dispenduk/penduduk/penduduk/get';
$route['api/dinas-kependudukan/penduduk/datatable']['POST'] = 'dispenduk/penduduk/penduduk/get_d';
$route['api/dinas-kependudukan/penduduk/(:num)']['GET'] = 'dispenduk/penduduk/penduduk/get/$1';
$route['api/dinas-kependudukan/penduduk/(:num)/(:num)']['GET'] = 'dispenduk/penduduk/penduduk/get/0/$1/$2';
$route['api/dinas-kependudukan/penduduk']['POST'] = 'dispenduk/penduduk/penduduk/store';
$route['api/dinas-kependudukan/penduduk/(:num)']['POST'] = 'dispenduk/penduduk/penduduk/update/$1';
$route['api/dinas-kependudukan/penduduk/(:num)']['DELETE'] = 'dispenduk/penduduk/penduduk/delete/$1';
$route['api/dinas-kependudukan/penduduk/upload']['POST'] = 'dispenduk/penduduk/penduduk/uploadExcel';

/**
Routing untuk module pajak submodule badan pelayanan pajak daerah
 */
//halaman dasar
$route['bp2d/statistik-sektoral']['GET'] = 'bp2d/statistiksektoral/statistiksektoral/index';

//api
$route['api/bp2d/statistik-sektoral']['GET'] = 'bp2d/statistiksektoral/statistiksektoral/get';
$route['api/bp2d/statistik-sektoral/datatable']['POST'] = 'bp2d/statistiksektoral/statistiksektoral/get_d';
$route['api/bp2d/statistik-sektoral/(:num)']['GET'] = 'bp2d/statistiksektoral/statistiksektoral/get/$1';
$route['api/bp2d/statistik-sektoral/(:num)/(:num)']['GET'] = 'bp2d/statistiksektoral/statistiksektoral/get/0/$1/$2';
$route['api/bp2d/statistik-sektoral']['POST'] = 'bp2d/statistiksektoral/statistiksektoral/store';
$route['api/bp2d/statistik-sektoral/(:num)']['POST'] = 'bp2d/statistiksektoral/statistiksektoral/update/$1';
$route['api/bp2d/statistik-sektoral/(:num)']['DELETE'] = 'bp2d/statistiksektoral/statistiksektoral/delete/$1';

/**
// Routing untuk module bkd submodule rekapitulasipns
 */
//halaman dasar
$route['bkd/rekapitulasi-pns']['GET'] = 'bkd/rekapitulasipns/rekapitulasipns/index';

//api
$route['api/bkd/agama']['GET'] = 'bkd/rekapitulasipns/agama/get';
$route['api/bkd/eselon']['GET'] = 'bkd/rekapitulasipns/eselon/get';
$route['api/bkd/golongan']['GET'] = 'bkd/rekapitulasipns/golongan/get';
$route['api/bkd/unit-kerja']['GET'] = 'bkd/rekapitulasipns/unitkerja/get';
$route['api/bkd/rekapitulasi-pns']['GET'] = 'bkd/rekapitulasipns/rekapitulasipns/get';
$route['api/bkd/rekapitulasi-pns/datatable']['POST'] = 'bkd/rekapitulasipns/rekapitulasipns/get_d';
$route['api/bkd/rekapitulasi-pns/(:num)']['GET'] = 'bkd/rekapitulasipns/rekapitulasipns/get/$1';
$route['api/bkd/rekapitulasi-pns/(:num)/(:num)']['GET'] = 'bkd/rekapitulasipns/rekapitulasipns/get/0/$1/$2';
$route['api/bkd/rekapitulasi-pns']['POST'] = 'bkd/rekapitulasipns/rekapitulasipns/store';
$route['api/bkd/rekapitulasi-pns/(:num)']['POST'] = 'bkd/rekapitulasipns/rekapitulasipns/update/$1';
$route['api/bkd/rekapitulasi-pns/(:num)/(:any)']['POST'] = 'bkd/rekapitulasipns/rekapitulasipns/update/$1/$2';
$route['api/bkd/rekapitulasi-pns/(:num)/(:any)']['DELETE'] = 'bkd/rekapitulasipns/rekapitulasipns/delete/$1/$2';
$route['api/bkd/rekapitulasi-pns/upload']['POST'] = 'bkd/rekapitulasipns/rekapitulasipns/uploadExcel';

/**
// Routing untuk module dinkes submodule data_bumil
 */
//halaman dasar
$route['dinkes/data-bumil']['GET'] = 'dinkes/data_bumil/data_bumil/index';

//api
$route['api/dinkes/data-bumil/keterangan-bpjs']['GET'] = 'dinkes/data_bumil/keterangan_bpjs/get';
$route['api/dinkes/data-bumil/rencana-persalinan-biaya']['GET'] = 'dinkes/data_bumil/Rencana_persalinan_biaya/get';
$route['api/dinkes/data-bumil/rencana-persalinan-kendaraan']['GET'] = 'dinkes/data_bumil/Rencana_persalinan_kendaraan/get';

$route['api/dinkes/data-bumil']['GET'] = 'dinkes/data_bumil/data_bumil/get';
$route['api/dinkes/data-bumil/datatable']['POST'] = 'dinkes/data_bumil/data_bumil/get_d';
$route['api/dinkes/data-bumil/(:num)']['GET'] = 'dinkes/data_bumil/data_bumil/get/$1';
$route['api/dinkes/data-bumil/(:num)/(:num)']['GET'] = 'dinkes/data_bumil/data_bumil/get/0/$1/$2';
$route['api/dinkes/data-bumil']['POST'] = 'dinkes/data_bumil/data_bumil/store';
$route['api/dinkes/data-bumil/(:num)']['POST'] = 'dinkes/data_bumil/data_bumil/update/$1';
$route['api/dinkes/data-bumil/(:num)']['DELETE'] = 'dinkes/data_bumil/data_bumil/delete/$1';
$route['api/dinkes/data-bumil/upload']['POST'] = 'dinkes/data_bumil/data_bumil/uploadExcel';

/**
// Routing untuk module dinkes submodule `data_gizi_buruk
 */
//halaman dasar
$route['dinkes/data-gizi-buruk']['GET'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/index';

//api
$route['api/dinkes/data-gizi-buruk/puskesmas']['GET'] = 'dinkes/data_gizi_buruk/puskesmas/get';

$route['api/dinkes/data-gizi-buruk/balita']['GET'] = 'dinkes/data_gizi_buruk/balita/get';
$route['api/dinkes/data-gizi-buruk/balita/datatable']['POST'] = 'dinkes/data_gizi_buruk/balita/get_d';
$route['api/dinkes/data-gizi-buruk/balita']['POST'] = 'dinkes/data_gizi_buruk/balita/store';
$route['api/dinkes/data-gizi-buruk/balita/(:num)']['POST'] = 'dinkes/data_gizi_buruk/balita/update/$1';
$route['api/dinkes/data-gizi-buruk/balita/(:num)']['DELETE'] = 'dinkes/data_gizi_buruk/balita/delete/$1';

$route['api/dinkes/data-gizi-buruk']['GET'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/get';
$route['api/dinkes/data-gizi-buruk/datatable']['POST'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/get_d';
$route['api/dinkes/data-gizi-buruk/(:num)']['GET'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/get/$1';
$route['api/dinkes/data-gizi-buruk/(:num)/(:num)']['GET'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/get/0/$1/$2';
$route['api/dinkes/data-gizi-buruk']['POST'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/store';
$route['api/dinkes/data-gizi-buruk/(:num)']['POST'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/update/$1';
$route['api/dinkes/data-gizi-buruk/(:num)']['DELETE'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/delete/$1';
$route['api/dinkes/data-gizi-buruk/upload']['POST'] = 'dinkes/data_gizi_buruk/data_gizi_buruk/uploadExcel';

/**
Routing untuk module dinkes submodule datatb
 */
//halaman dasar
$route['dinkes/data-tb']['GET'] = 'dinkes/datatb/datatb/index';

//api
$route['api/dinkes/data-tb']['GET'] = 'dinkes/datatb/datatb/get';
$route['api/dinkes/data-tb/datatable']['POST'] = 'dinkes/datatb/datatb/get_d';
$route['api/dinkes/data-tb/(:num)']['GET'] = 'dinkes/datatb/datatb/get/$1';
$route['api/dinkes/data-tb/(:num)/(:num)']['GET'] = 'dinkes/datatb/datatb/get/0/$1/$2';
$route['api/dinkes/data-tb']['POST'] = 'dinkes/datatb/datatb/store';
$route['api/dinkes/data-tb/(:num)']['POST'] = 'dinkes/datatb/datatb/update/$1';
$route['api/dinkes/data-tb/(:num)']['DELETE'] = 'dinkes/datatb/datatb/delete/$1';
$route['api/dinkes/data-tb/upload']['POST'] = 'dinkes/datatb/datatb/uploadExcel';

$route['api/dinkes/fayankes']['GET'] = 'dinkes/datatb/fayankes/get';
$route['api/dinkes/jenis-fayankes']['GET'] = 'dinkes/datatb/jenisfayankes/get';
$route['api/dinkes/kode-rejimen']['GET'] = 'dinkes/datatb/kode_paduan_rejimen_yang_diberikan/get';
$route['api/dinkes/klasifikasi-penyakit']['GET'] = 'dinkes/datatb/klasifikasi_penyakit/get';
$route['api/dinkes/tipe-pasien']['GET'] = 'dinkes/datatb/tipe_pasien/get';
$route['api/dinkes/validasi-data']['GET'] = 'dinkes/datatb/validasi_data/get';

/**
// Routing untuk module budpar submodule jasa_akomodasi
 */
//halaman dasar
$route['budpar/jasa-akomodasi']['GET'] = 'budpar/jasa_akomodasi/jasa_akomodasi/index';
//api
$route['api/budpar/jasa-akomodasi']['GET'] = 'budpar/jasa_akomodasi/jasa_akomodasi/get';

/**
// Routing untuk module budpar submodule Data dan Profil Kunjungan Wisata
 */
//halaman dasar
$route['budpar/data-profil']['GET'] = 'budpar/data_profil/data_profil/index';
//api
$route['api/budpar/data-profil']['GET'] = 'budpar/data_profil/data_profil/get';

/**
// Routing untuk module budpar submodule Cagar Budaya
 */
//halaman dasar
$route['budpar/cagar-budaya']['GET'] = 'budpar/cagar_budaya/cagar_budaya/index';
//api
$route['api/budpar/cagar-budaya']['GET'] = 'budpar/cagar_budaya/cagar_budaya/get';

/**
// Routing untuk module budpar submodule PAD Kota Malang
 */
//halaman dasar
$route['budpar/pad-kota']['GET'] = 'budpar/pad_kota/pad_kota/index';
//api
$route['api/budpar/pad-kota']['GET'] = 'budpar/pad_kota/pad_kota/get';

//halaman dan api smartcity
$route['smartcity']['GET'] = 'smartcity/smart';
$route['smartcity/eventmalang']['GET'] = 'smartcity/smart/eventmalangnet';
$route['smartcity/hargapasar']['GET'] = 'smartcity/smart/hargapasar';
$route['smartcity/masukevent']['GET'] = 'smartcity/smart/masukevent';
$route['smartcity/wisata/(:any)']['GET'] = 'smartcity-dovie/smartdovie/wisata/$1';
$route['smartcity/gambarwisata/(:any)/(:any)']['GET'] = 'smartcity-dovie/smartdovie/gambarwisata/$1/$2';
$route['smartcity/jadwalshalat']['GET'] = 'smartcity/smart/jadwalshalat';


 