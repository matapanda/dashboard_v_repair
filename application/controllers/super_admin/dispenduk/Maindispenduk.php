<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'third_party/SimpleXLSX.php';

class Maindispenduk extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('main/mainmodel', 'mm');

		$this->load->library("response_message");
		$this->load->library("Uploadfilev0");
	}

	public function index(){
		$data["page"] = "add_file_dispenduk";
		$data["month"] = ["","Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
		$data["data_json"] = $this->mm->get_data_all("dispenduk_file_data");

		$this->load->view('super_admin/dispenduk/main_dispenduk_add_file', $data);
	}

	private function val_form_upload(){
        $config_val_input = array(
                array(
                    'field'=>'bln_periode',
                    'label'=>'bln_periode',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'bln_th',
                    'label'=>'bln_th',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),array(
                    'field'=>'keterangan',
                    'label'=>'keterangan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function add_data(){
        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array("keterangan" => "",
    						"file" => "");

        if($this->val_form_upload()){
        	$keterangan = $this->input->post("keterangan");
        	$bln_periode = $this->input->post("bln_periode");
        	$bln_th = $this->input->post("bln_th");

        	$str_periode = $bln_periode."-".$bln_th;
        	$time_input = date("Y-m-d H:i:s");

        	if(isset($_FILES["file_upload"])){
		        $path = './assets/excel_upload/';
		        if(!file_exists($path)){
		        	mkdir($path);
		        }
		        // $path .= $periode.'/';

		        if(!$this->mm->get_data_each("dispenduk_file_data", array("periode"=>$str_periode))){
		        	$insert = $this->db->query("select insert_upload('$str_periode', '$keterangan', '$time_input') as id")->row_array();
		        	if($insert){
		        		$config['upload_path']          = $path;
				        $config['allowed_types']        = "xlsx";
				        $config['max_size']             = 4096;
				        $config['file_name']            = hash("sha256", $insert["id"]).".xlsx";
				           
				        $upload_data = $this->uploadfilev0->do_upload($config, "file_upload");
				        
				        if($upload_data["status"]){

				            $where = array("id_file_data"=>$insert["id"]);
				            $set = array("loc_file_excel"=>$upload_data["main_data"]["upload_data"]["file_name"]);

				            $set_data_for_json = $this->set_data_json($upload_data["main_data"]["upload_data"]["file_name"]);
				            if($set_data_for_json){
				            	$this->create_json(hash("sha256", $insert["id"]), json_encode($set_data_for_json));
				            	$set["loc_file_json"] = hash("sha256", $insert["id"]).".json";
				            }

				            if($this->mm->update_data("dispenduk_file_data", $set, $where)){
				            	print_r($insert);
				                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
				            }else {
				                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
				            }
				        }else{
				            $msg_detail["file"] = $upload_data["main_msg"]["error"];
				            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
				        }
		        	}
		        }else{
		        	$msg_main = array("status" => false, "msg"=>"Input data gagal, Data dengan periode ini sudah dinputkan silahkan klik update untuk mengganti");
		        }
        	}
        }else{
        	$msg_detail["keterangan"] = form_error("keterangan");
            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r($msg_array);
    }

    private function val_form_delete(){
        $config_val_input = array(
                array(
                    'field'=>'param',
                    'label'=>'param',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_data(){
    	$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array("param" => "");

        if($this->val_form_delete()){
        	$param = $this->input->post("param");

	    	$data = $this->mm->get_data_each("dispenduk_file_data", array("sha2(id_file_data, '512') ="=>$param));
	    	if($data){
	    		$status = true;
	    		// $path = './assets/json_upload/';
	    		if(!$this->delete_file('./assets/excel_upload/'.$data["loc_file_excel"])){
	    			$status = false;
	    		}

	    		if(!$this->delete_file('./assets/json_upload/'.$data["loc_file_json"])){
	    			$status = false;
	    		}

	    		if($status){
	    			$delete = $this->mm->delete_data("dispenduk_file_data", array("sha2(id_file_data, '512') ="=>$param));
	    			if($delete){
	    				// print_r("fine");
	    				$msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
	    			}
	    		}
	    	}
        }else{
        	$msg_detail["param"] = form_error("param");
            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        }

    	$msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }

    public function set_data_json($name_file){
    	$path = 'assets/excel_upload/'.$name_file;

    	$status = true;
    	$data["lampid"] = $this->get_lampid($path);
    	if(!$data["lampid"]){
    		$status = false;
    	}

    	$data["rekap_kelurahan"] = $this->get_rekap_kelurahan($path);
    	if(!$data["rekap_kelurahan"]){
    		$status = false;
    	}

    	$data["agama"] = $this->get_agama($path);
    	if(!$data["agama"]){
    		$status = false;
    	}

    	$data["rekam_ktp"] = $this->get_rekam_ktp($path);
    	if(!$data["rekam_ktp"]){
    		$status = false;
    	}

    	$data["kelompok_umur"] = $this->get_kelompok_umur($path);
    	if(!$data["kelompok_umur"]){
    		$status = false;
    	}

    	if($status){
    		return $data;
    	}
    	// print_r("<pre>");
    	// print_r();
    	return false;
    }

    private function create_json($name_file, $resource){
    	$path = './assets/json_upload/';
        if(!file_exists($path)){
        	mkdir($path);
        }
    	$fp = fopen('./assets/json_upload/'.$name_file.'.json', 'w');
		fwrite($fp, $resource);
		fclose($fp);
    }

    private function delete_file($path){
    	$status = true;
    	if(file_exists($path)){
    		$statu = $var = unlink($path) ? true : false;
    	}
    	return $status;
    }

    public function cek_data_excel(){
    	$path = 'assets/excel_upload/35a5723d31facf0db059e64797f0a437920c68b3009febd0c203770c01009c90.xlsx';

    	print_r("<pre>");
    	$this->get_lampid($path);
    }


#====================================================================================
#---------------------------------get_data_from_excel--------------------------------
#====================================================================================

    private function get_lampid($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
    	$msg_main = array("status"=>false, "msg"=>"fucking data", "item"=>"");
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows();
                // print_r($main_data);

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 5 && $key <= 9){
                        $param = "main_awal";
                        $status = true;
                    }elseif ($key == 10) {
                        $param = "t_main_awal";
                        $status = true;
                    }elseif ($key >= 27 && $key <= 31) {
                        $param = "main_kurang";
                        $status = true;
                    }elseif ($key == 32) {
                        $param = "t_main_kurang";
                        $status = true;
                    }elseif ($key >= 49 && $key <= 53) {
                        $param = "main_akhir";
                        $status = true;
                    }elseif ($key == 54) {
                        $param = "t_main_akhir";
                        $status = true;
                    }

                    if($status){
                        $tmp_array[$param][$no]["kecamatan"] = $value[1];
                        
                        $tmp_array[$param][$no]["PENDUDUK_AWAL_BULAN_INI"]["l"] = $value[2];
                        $tmp_array[$param][$no]["PENDUDUK_AWAL_BULAN_INI"]["p"] = $value[3];
                        $tmp_array[$param][$no]["PENDUDUK_AWAL_BULAN_INI"]["lp"] = $value[4];

                        $tmp_array[$param][$no]["LAHIR_BULAN_INI"]["l"] = $value[5];
                        $tmp_array[$param][$no]["LAHIR_BULAN_INI"]["p"] = $value[6];
                        $tmp_array[$param][$no]["LAHIR_BULAN_INI"]["lp"] = $value[7];
                        
                        $tmp_array[$param][$no]["MATI_BULAN_INI"]["l"] = $value[8];
                        $tmp_array[$param][$no]["MATI_BULAN_INI"]["p"] = $value[9];
                        $tmp_array[$param][$no]["MATI_BULAN_INI"]["lp"] = $value[10];

                        $tmp_array[$param][$no]["PENDATANG_BULAN_INI"]["l"] = $value[11];
                        $tmp_array[$param][$no]["PENDATANG_BULAN_INI"]["p"] = $value[12];
                        $tmp_array[$param][$no]["PENDATANG_BULAN_INI"]["lp"] = $value[13];
                        
                        $tmp_array[$param][$no]["PINDAH_BULAN_INI"]["l"] = $value[14];
                        $tmp_array[$param][$no]["PINDAH_BULAN_INI"]["p"] = $value[15];
                        $tmp_array[$param][$no]["PINDAH_BULAN_INI"]["lp"] = $value[16];
                        
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["l"] = $value[17];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["p"] = $value[18];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["lp"] = $value[19];

                        $no++;
                    }
                }

                $msg_main = array("status"=>true, "msg"=>"success", "item"=>$tmp_array);
            } else {
            	$msg_main = array("status"=>false, "msg"=>SimpleXLSX::parseError(), "item"=>null);
                // echo ;
            }
        }

        return $msg_main;
        // print_r($msg_main);
    }

    private function get_rekap_kelurahan($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
    	$msg_main = array("status"=>false, "msg"=>"fucking data", "item"=>"");
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(1);
                // print_r($main_data);

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 7 && $key <= 11){
                        $param = "rekap_kelurahan";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_rekap_kelurahan";
                        $status = true;
                    }

                    if($status){
                        $tmp_array[$param][$no]["KECAMATAN"] = $value[1];
                        
                        $tmp_array[$param][$no]["JUMLAH_KEL"] = $value[2];
                        $tmp_array[$param][$no]["JUMLAH_KK"] = $value[3];

                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["l"] = $value[4];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["p"] = $value[5];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["lp"] = $value[6];

                        $no++;
                    }
                }

                // print_r($tmp_array);
                $msg_main = array("status"=>true, "msg"=>"success", "item"=>$tmp_array);
            } else {
                // echo SimpleXLSX::parseError();
                $msg_main = array("status"=>false, "msg"=>SimpleXLSX::parseError(), "item"=>null);
            }
        }
        return $msg_main;
    }

    private function get_agama($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
    	$msg_main = array("status"=>false, "msg"=>"fucking data", "item"=>"");
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(2);
                // print_r($main_data);
    
                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;
    
                    if($key >= 7 && $key <= 11){
                        $param = "agama";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_agama";
                        $status = true;
                    }
    
                    if($status){
                        $tmp_array[$param][$no]["KECAMATAN"] = $value[1];
                        $tmp_array[$param][$no]["ISLAM"] = $value[2];
                        $tmp_array[$param][$no]["KRISTEN"] = $value[3];
                        $tmp_array[$param][$no]["KATHOLIK"] = $value[4];
                        $tmp_array[$param][$no]["HINDU"] = $value[5];
                        $tmp_array[$param][$no]["BUDHA"] = $value[6];
                        $tmp_array[$param][$no]["KONGHUCHU"] = $value[7];
                        $tmp_array[$param][$no]["PENGHAYAT_KEPERCAYAAN"] = $value[8];
    
                        $no++;
                    }
                }
    
                // print_r($tmp_array);
                $msg_main = array("status"=>true, "msg"=>"success", "item"=>$tmp_array);
            } else {
                // echo SimpleXLSX::parseError();
                $msg_main = array("status"=>false, "msg"=>SimpleXLSX::parseError(), "item"=>null);
            }
        }
        return $msg_main;
    }

    private function get_rekam_ktp($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
    	$msg_main = array("status"=>false, "msg"=>"fucking data", "item"=>"");
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(4);
                // print_r($main_data);

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 7 && $key <= 11){
                        $param = "rekam_ktp";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_rekam_ktp";
                        $status = true;
                    }

                    if($status){
                        $tmp_array[$param][$no]["KECAMATAN"] = $value[1];
                        $tmp_array[$param][$no]["jumlah_penduduk"] = $value[2];

                        $tmp_array[$param][$no]["wajib_memiliki_KTP"]["l"] = $value[3];
                        $tmp_array[$param][$no]["wajib_memiliki_KTP"]["p"] = $value[4];
                        $tmp_array[$param][$no]["wajib_memiliki_KTP"]["lp"] = $value[5];
                        
                        $tmp_array[$param][$no]["sudah_rekam_KTP_el"] = $value[6];
                        $tmp_array[$param][$no]["belum_rekam_KTP_el"] = $value[7];
                        $tmp_array[$param][$no]["wajib_memiliki_akta_kelahiran"] = $value[8];
                        $tmp_array[$param][$no]["memiliki_akta_kelahiran"] = $value[9];

                        $no++;
                    }
                }

                // print_r($tmp_array);
            	$msg_main = array("status"=>true, "msg"=>"success", "item"=>$tmp_array);
            } else {
                // echo SimpleXLSX::parseError();
                $msg_main = array("status"=>false, "msg"=>SimpleXLSX::parseError(), "item"=>null);
            }
        }
        return $msg_main;
    }

    private function get_kelompok_umur($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
    	$msg_main = array("status"=>false, "msg"=>"fucking data", "item"=>"");
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(3);
                // print_r($main_data);

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 7 && $key <= 11){
                        $param = "kelompok_umur";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_kelompok_umur";
                        $status = true;
                    }elseif ($key == 5) {
                        $header_array = array(
                            "Kecamatan","0 - 4 TH","5 - 9 TH","10 - 14 TH","15 - 19 TH","20 - 24 TH",
                            "25 - 29 TH","30 - 34 TH","35 - 39 TH","40 - 44 TH","45 - 49 TH","50 - 54 TH",
                            "55 - 59 TH","60 - 64 TH","> 65 TH", "Jumlah Penduduk"
                        );
                    }

                    if($status){
                        $tmp_ok = array();
                        foreach ($value as $key_val => $value_val) {
                            if($key_val >= 1 && $key_val <= 16){
                                array_push($tmp_ok, $value_val);
                            }
                        }

                        $tmp_array[$param][$no] = $tmp_ok;

                        $no++;
                    }
                }

                $new_data_main["header"] = $header_array;
                $new_data_main["value"] = $tmp_array;

                // print_r($new_data_main);
            	$msg_main = array("status"=>true, "msg"=>"success", "item"=>$new_data_main);
            } else {
                // echo SimpleXLSX::parseError();
                $msg_main = array("status"=>false, "msg"=>SimpleXLSX::parseError(), "item"=>null);
            }
        }
        return $msg_main;
    }
#====================================================================================
#---------------------------------get_data_from_excel--------------------------------
#====================================================================================
	
}
