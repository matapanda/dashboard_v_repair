<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include('simple_html_dom.php');
class Smartdovie extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
    }
    public function index()
    {
echo "lucu";

    }

public function wisata($apa){
$html =  file_get_html(__DIR__.'/kucing.html');
// $kucing=$html->find("resources",0);
//INFO_HOTEL2
switch ($apa) {
    case "hotel":
    $nama_tempat=$html->find("string-array[name='hotel']",0);
    $alamat_tempat=$html->find("string-array[name='jalan_hotel']",0);
    $gambar=$html->find("integer-array[name='foto_hotel']",0);
    $telepon=$html->find("string-array[name='telepon_hotel']",0);
    $deskripsi_tempat=$html->find("string-array[name='jelas_hotel']",0);
    
    $hotels=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $hotel=[];
        $hotel["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $hotel["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $hotel["gambar"]=$kumala;
        $hotel["telepon"]=$telepon->children($i)->plaintext;
        $hotel["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($hotels,$hotel);
    }
    echo json_encode($hotels);        
    break;
            
    case "homestay":
    $nama_tempat=$html->find("string-array[name='homestay']",0);
    $alamat_tempat=$html->find("string-array[name='alamatHomestay']",0);
    $gambar=$html->find("integer-array[name='gambarhomestay']",0);
    $telepon=$html->find("string-array[name='tlpHomestay']",0);
    $deskripsi_tempat=$html->find("string-array[name='jelasHomestay']",0);
    // //iki hotel
    
    $homestay1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $homestay=[];
        $homestay["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $homestay["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $homestay["gambar"]=$kumala;
        $homestay["telepon"]=$telepon->children($i)->plaintext;
        $homestay["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($homestay1,$homestay);
    }
    
    echo json_encode($homestay1);

    break;        
    
    case "guesthouse":
    $nama_tempat=$html->find("string-array[name='guestHouse']",0);
    $alamat_tempat=$html->find("string-array[name='alamatguesthouse']",0);
    $gambar=$html->find("integer-array[name='fotoGuesthouse']",0);
    $telepon=$html->find("string-array[name='tlpGuesthouse']",0);
    $deskripsi_tempat=$html->find("string-array[name='penjelasanGuest']",0);
    // //iki hotel
    
    
    $guesthouse1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $guesthouse=[];
        $guesthouse["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $guesthouse["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $guesthouse["gambar"]=$kumala;
        $guesthouse["telepon"]=$telepon->children($i)->plaintext;
        $guesthouse["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($guesthouse1,$guesthouse);
    }
    echo json_encode($guesthouse1);

    break;  
            
    case "kuliner":
    $nama_tempat=$html->find("string-array[name='kuliner']",0);
    $alamat_tempat=$html->find("string-array[name='kulinerAlamat']",0);
    $gambar=$html->find("integer-array[name='kulinerGambar']",0);
    $telepon=$html->find("string-array[name='kulinerTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='kulinerPenjelasan']",0);
    // //iki hotel
    $kuliner1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $kuliner=[];
        $kuliner["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $kuliner["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $kuliner["gambar"]=$kumala;
        $kuliner["telepon"]=$telepon->children($i)->plaintext;
        $kuliner["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($kuliner1,$kuliner);
    }
    echo json_encode($kuliner1);

    break;   
    
    case "oleh_oleh":
    $nama_tempat=$html->find("string-array[name='oleholeh']",0);
    $alamat_tempat=$html->find("string-array[name='oleholehAlamat']",0);
    $gambar=$html->find("integer-array[name='oleholehGambar']",0);
    $telepon=$html->find("string-array[name='oleholehTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='oleholehPenjelasan']",0);
    // //iki hotel
    $oleholeh1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $oleholeh=[];
        $oleholeh["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $oleholeh["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $oleholeh["gambar"]=$kumala;
        $oleholeh["telepon"]=$telepon->children($i)->plaintext;
        $oleholeh["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
        $oleholeh['id_jenis']="11";
    array_push($oleholeh1,$oleholeh);
    }
    echo json_encode($oleholeh1);

    break;

    
     case "kampung":
    $nama_tempat=$html->find("string-array[name='wisataKampung']",0);
    $alamat_tempat=$html->find("string-array[name='wisataKampungAlamat']",0);
    $gambar=$html->find("integer-array[name='wisataKampungGambar']",0);
    $telepon=$html->find("string-array[name='wisataKampungTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='wisataKampungPenjelasan']",0);
    // //iki hotel
    $kampung1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $kampungtematik=[];
        $kampungtematik["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $kampungtematik["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $kampungtematik["gambar"]=$kumala;
        $kampungtematik["telepon"]=$telepon->children($i)->plaintext;
        $kampungtematik["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($kampung1,$kampungtematik);
    }
    echo json_encode($kampung1);

    break;
    
    case "olahraga":
    $nama_tempat=$html->find("string-array[name='wisataOlahraga']",0);
    $alamat_tempat=$html->find("string-array[name='wisataOlahragaAlamat']",0);
    $gambar=$html->find("integer-array[name='wisataOlahragaGambar']",0);
    $telepon=$html->find("string-array[name='wisataOlahragaTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='wisataOlahragaPenjelasan']",0);
    // //iki hotel
    $olahraga1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $olahRaga=[];
        $olahRaga["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $olahRaga["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $olahRaga["gambar"]=$kumala;
        $olahRaga["telepon"]=$telepon->children($i)->plaintext;
        $olahRaga["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($olahraga1,$olahRaga);
    }
    echo json_encode($olahraga1);

    break;
    
     case "religi":
    $nama_tempat=$html->find("string-array[name='wisataReligi']",0);
    $alamat_tempat=$html->find("string-array[name='wisataReligiAlamat']",0);
    $gambar=$html->find("integer-array[name='wisataReligiGambar']",0);
    $telepon=$html->find("string-array[name='wisataReligiTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='wisataReligiPenjelasan']",0);
    // //iki hotel
    $religi1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $religius=[];
        $religius["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $religius["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $religius["gambar"]=$kumala;
        $religius["telepon"]=$telepon->children($i)->plaintext;
        $religius["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($religi1,$religius);
    }
    echo json_encode($religi1);

    break;
    
    
  case "edukasi":
    $nama_tempat=$html->find("string-array[name='wisataEdukasi']",0);
    $alamat_tempat=$html->find("string-array[name='wisataEdukasiAlamat']",0);
    $gambar=$html->find("integer-array[name='wisataEdukasiGambar']",0);
    $telepon=$html->find("string-array[name='wisataEdukasiTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='wisataEdukasiPenjelasan']",0);
    // //iki hotel
    $edukasi1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $edukasis=[];
        $edukasis["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $edukasis["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $edukasis["gambar"]=$kumala;
        $edukasis["telepon"]=$telepon->children($i)->plaintext;
        $edukasis["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($edukasi1,$edukasis);
    }
    echo json_encode($edukasi1);
    
     case "belanja":
    $nama_tempat=$html->find("string-array[name='wisataBelanja']",0);
    $alamat_tempat=$html->find("string-array[name='wisataBelanjaAlamat']",0);
    $gambar=$html->find("integer-array[name='wisataBelanjaGambar']",0);
    $telepon=$html->find("string-array[name='wisataBelanjaTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='wisataBelanjaPenjelasan']",0);
    // //iki hotel
    $belanja1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $ngemall=[];
        $ngemall["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $ngemall["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $ngemall["gambar"]=$kumala;
        $ngemall["telepon"]=$telepon->children($i)->plaintext;
        $ngemall["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($belanja1,$ngemall);
    }
    echo json_encode($belanja1);

    break;
    
    case "rekreasi":
    $nama_tempat=$html->find("string-array[name='wisataRekreasi']",0);
    $alamat_tempat=$html->find("string-array[name='wisataRekreasiAlamat']",0);
    $gambar=$html->find("integer-array[name='wisataRekreasiGambar']",0);
    $telepon=$html->find("string-array[name='wisataRekreasiTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='wisataRekreasiPenjelasan']",0);
    // //iki hotel
    $rekreasi1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $recreation=[];
        $recreation["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $recreation["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $recreation["gambar"]=$kumala;
        $recreation["telepon"]=$telepon->children($i)->plaintext;
        $recreation["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($rekreasi1,$recreation);
    }
    echo json_encode($rekreasi1);

    break;
    
    case "sejarah":
    $nama_tempat=$html->find("string-array[name='wisataSejarah']",0);
    $alamat_tempat=$html->find("string-array[name='wisataSejarahAlamat']",0);
    $gambar=$html->find("integer-array[name='wisataSejarahGambar']",0);
    $telepon=$html->find("string-array[name='wisataSejarahTelepon']",0);
    $deskripsi_tempat=$html->find("string-array[name='wisataSejarahPenjelasan']",0);
    // //iki hotel
    // echo count($sejarah->children());
    // echo count($alamatSejarah->children());
    // echo count($gambarSejarah->children());
    // echo count($teleponSejarah->children());
    // echo count($penjelasanSejarah->children());
    $sejarah1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $sejarawan=[];
        $sejarawan["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $sejarawan["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $sejarawan["gambar"]=$kumala;
        $sejarawan["telepon"]=$telepon->children($i)->plaintext;
        $sejarawan["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($sejarah1,$sejarawan);
    }
    echo json_encode($sejarah1);

    break;
    
    case "taman_hutan":
    $nama_tempat=$html->find("string-array[name='hutanKota']",0);
    $alamat_tempat=$html->find("string-array[name='hutanAlamat']",0);
    $gambar=$html->find("integer-array[name='hutanGambar']",0);
    $telepon=$html->find("string-array[name='hutanTlp']",0);
    $deskripsi_tempat=$html->find("string-array[name='hutanPenjelasan']",0);
    // //iki hotel
    // //iki hotel
    $hutan1=[];

    for($i=0;$i<count($nama_tempat->children());$i++){
        $taman=[];
        $taman["nama_tempat"]=str_replace("\\n","",$nama_tempat->children($i)->plaintext);
        $taman["alamat_tempat"]=$alamat_tempat->children($i)->plaintext;
        $kumala=str_replace("@drawable/", "", $gambar->children($i)->plaintext);
        $taman["gambar"]=$kumala;
        $taman["telepon"]=$telepon->children($i)->plaintext;
        $taman["deskripsi_tempat"]=$deskripsi_tempat->children($i)->plaintext;
    array_push($hutan1,$taman);
    }
    echo json_encode($hutan1);

    break;
    
}

}
public function gambarWisata($apa,$gambar){
    if(file_exists(__DIR__."/".$apa."/".$gambar.".jpg")){
header('Content-Type: image/jpeg');
    $myfile = fopen(__DIR__."/".$apa."/".$gambar.".jpg", "r"); 
echo fread($myfile,filesize(__DIR__."/".$apa."/".$gambar.".jpg"));
fclose($myfile);    
    }else{
        die("Unable to open file!");
    }
    
}    
    
}
