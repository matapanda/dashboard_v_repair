<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog_tipe extends CI_Controller
{
    private $submodule;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->model('katalog/katalog_tipe_model', 'ktipe_model');

        $this->submodule = 'katalog';
    }

    public function get($idtipe = null)
    {
        if($idtipe){
            $this->ktipe_model->idtipe = $idtipe;
            $data = $this->ktipe_model->get();
        }else{
            $data = $this->ktipe_model->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data tipe katalog', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data tipe katalog', $data);
    }

}
