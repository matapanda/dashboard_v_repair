<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog extends CI_Controller
{
    private $submodule;

    private $list_videos = [
        '1-PELAYANAN.mp4',
        '2-ADMINISTRASI _ MANAGEMENT.mp4',
        '3-LEGISLASI.mp4',
        '4-PEMBANGUNAN.mp4',
        '5-KEUANGAN.mp4',
        '6-KEPEGAWAIAN.mp4',
        '7-DINAS _ LEMBAGA.mp4',
        '8-KEWILAYAHAN.mp4',
        '9-KMASYARAKATAN.mp4',
        '10-SARANA PRASARANA.mp4',
        '11-MEDIA CENTER.mp4',
        '12-PENDIDIKAN.mp4',
        '13-BP2D.mp4'
    ];
    

    private $list_thumbnails = [
        '1-pelayanan.png',
        '2-administrasi.png',
        '3-legislasi.png',
        '4-pembangunan.png',
        '5-keuangan.png',
        '6-kepegawaian.png',
        '7-dinas-lembaga.png',
        '8-kewilayahan.png',
        '9-kemasyarakatan.png',
        '10-sarana-prasaran.png',
        '11-media-center.png',
        '12-pendidikan.png',
        '13-bp2d.png'
    ];

    private $list_text_title = [
        'Pelayanan',
        'Administrasi & Manajemen',
        'Legislasi',
        'Pembangunan',
        'Keuangan',
        'Kepegawaian',
        'Dinas & Lembaga',
        'Kewilayahan',
        'Kemasyarakatan',
        'Sarana Prasarana',
        'Media Center',
        'Pendidikan',
        'BP2D'
    ];

    private $list_description = [
        'Layanan smartcity terkait kependudukan, perpajakan & retribusi, pendaftaran & perijinan, bisnis & investasi, pengaduan masyarakat, publikasi informasi umum dan pemerintahan.',
        'Layanan smartcity terkait surat elektronik, dokumen elektronik, pendukung keputusan, kolaborasi dan koordinasi, manajemen pelaporan pemerintahan',
        'Layanan smartcity terkait sistem informasi DPRD, sistem pemilu daerah, katalog hukuman, katalog perundang-undangan',
        'Layanan smartcity terkait data pembangunan, perencanaan pembangunan daerah, pengadaan barang dan jasa, pengelolaan dan monitoring proyek, evaluasi dan informasi hasil pembangunan',
        'Layanan smartcity terkait anggaran, kas dan perbendaharaan, dan akuntansi daerah',
        'Layanan smartcity terkait pengadaan PNS, absensi dan penggajian, peniliaian dan kinerja PNS, pendidikan dan latihan',
        'Layanan smartcity terkait pengelolaan barang, katalog barang, pengelolaan pendapatan, dan pengelolaan perusahaan daerah',
        'Tata Ruang dan Lingkungan Hidup, Potensi Daerah, Kehutanan, Pertanian, Peternakan, dan Perkebunan, Perikanan dan Kelautan, Pertambangan dan Energi, Pariwisata, Industri Kecil dan Menegah',
        'Kesehatan, Pendidikan, Ketenagakerjaan, Industri dan Perdagangan, Jaring Pengaman Sosial',
        'Transportasi, Jalan dan Jembatan, Terminal dan Pelabuhan, Sarana Umum',
        'Layanan smartcity untuk melakukan pemantauan kondisi di kota Malang',
        'Layanan smartcity untuk melihat data Pendidikan [Data Pokok Satuan Pendidikan (NPSN), Data Pokok Peserta Didik (NISN), dan Data Pokok Pendidik & Tenaga Kependidikan (PTK)] di kota Malang',
         'Badan Pelanan Pajak Daerah untuk melihat data Jenis Pajak, Target, Realisasi, Persentase, Jumlah OP per jenis pajak dan Ketetapan'
    ];

    public function __construct()
    {
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->model('katalog/katalog_model', 'katalog_db');


        $this->submodule = 'katalog';
    }

    public function index()
    {
        $data['title'] = 'Katalog';
        $data['is_login'] = false;

        $data['list_videos'] = $this->list_videos;
        $data['list_thumbnails'] = $this->list_thumbnails;
        $data['list_text_title'] = $this->list_text_title;
        $data['list_description'] = $this->list_description;

        $data['main_content'] = $this->load->view('katalog/katalog', $data);
    }

    public function show($id)
    {
        if (!$this->auth->get_user()) {
            redirect('/#login', 'location');
        }
        $this->katalog_db->idkategori = $id + 1;
        $data_katalog = $this->katalog_db->get();

        $data['list_title'] = $this->list_text_title;
        $data['title'] = $this->list_text_title[$id];
        $data['video'] = $this->list_videos[$id];
        $data['thumbnail'] = $this->list_thumbnails[$id];
        $data['text_title'] = $this->list_text_title[$id];
        $data['text_description'] = $this->list_description[$id];
        $data['list_katalog'] = [];
        $data['id'] = $id;

        for ($i = 0; $i < count($data_katalog); $i++) {
            $data_item['idkatalog'] = $data_katalog[$i]->idkatalog;
            $data_item['no'] = $i + 1;
            $data_item['judul'] = $data_katalog[$i]->judul;
            $data_item['deskripsi'] = $data_katalog[$i]->deskripsi;
            $data_item['idtipe'] = $data_katalog[$i]->idtipe;
            $data_item['url'] = $data_katalog[$i]->url;
            $data_item['delete_url'] = base_url('katalog/katalog/delete/' . $data_katalog[$i]->idkatalog);
            array_push($data['list_katalog'], $data_item);
        }

        $data['main_content'] = $this->load->view('katalog/show', $data);
    }

    public function get($idkatalog = null, $idkategori = null, $idtipe = null, $keyword = null)
    {
        $this->katalog_db->idkatalog = $idkatalog;
        $this->katalog_db->idkategori = $idkategori;
        $this->katalog_db->idtipe = $idtipe;

        if ($keyword) {
            $data = $this->katalog_db->get($keyword);
        } else {
            $data = $this->katalog_db->get();
        }

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data katalog tersimpan', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data katalog tersimpan', $data);
    }

    public function store()
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('url_data', 'URL', 'required');
        $this->form_validation->set_rules('idkategori', 'Kategori', 'required');
        $this->form_validation->set_rules('idtipe', 'Tipe', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Judul, url, url data,  kategori, dan tipe diperlukan', FALSE);
        }

        $this->katalog_db->idkatalog = $this->generate_id->get_id();
        $this->katalog_db->judul = $this->input->post('judul');
        $this->katalog_db->deskripsi = $this->input->post('deskripsi');
        if ($this->input->post('idtipe') == 1) {
            $this->katalog_db->url = 'chart/show/' . $this->katalog_db->idkatalog;
        } else if ($this->input->post('idtipe') == 2) {
            $this->katalog_db->url = 'map/show/' . $this->katalog_db->idkatalog;
        }
//        $this->katalog_db->url = $this->katalog_db->idkatalog;
        $this->katalog_db->url_data = $this->input->post('url_data');
        $this->katalog_db->idkategori = $this->input->post('idkategori');
        $this->katalog_db->idtipe = $this->input->post('idtipe');

        $data = $this->katalog_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data katalog', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data katalog', $data);
    }

    public function count($idkategori = null)
    {
        $this->katalog_db->idkategori = $idkategori;
        $count = $this->katalog_db->count();
        return $count;
    }

    public function update($idkatalog)
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('url_data', 'URL', 'required');
        $this->form_validation->set_rules('idkategori', 'Kategori', 'required');
        $this->form_validation->set_rules('idtipe', 'Tipe', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Judul, url, url data,  kategori, dan tipe diperlukan', FALSE);
        }

        $this->katalog_db->idchart = $idkatalog;
        $this->katalog_db->judul = $this->input->post('judul');
        $this->katalog_db->deskripsi = $this->input->post('deskripsi');
        $this->katalog_db->url_data = $this->input->post('url_data');
        $this->katalog_db->idkategori = $this->input->post('idkategori');
        $this->katalog_db->idtipe = $this->input->post('idtipe');

        $data = $this->katalog_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data katalog', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data katalog', $data);
    }

    public function delete($idkatalog)
    {
        $this->auth->check_write($this->submodule);

        $this->katalog_db->idkatalog = $idkatalog;
        $data = $this->katalog_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data katalog', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data katalog', $data);
    }

    public function esambat_detail_web($limit = null, $offset = null)
    {
        if ($offset < 1) $offset = 1;
        if ($limit) {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/web/?limit=' . $limit . '&offset=' . $offset);
        } else {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/web');
        }
    }

    public function esambat_detail_sms($limit = null, $offset = null)
    {
        if ($offset < 1) $offset = 1;
        if ($limit) {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/sms/?limit=' . $limit . '&offset=' . $offset);
        } else {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/sms');
        }
    }

    public function esambat_detail_by_category($id = null, $limit = null, $offset = null)
    {
        if ($limit) {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/kategori/?limit=' . $limit . '&offset=' . $offset.'&id_kategori='.$id);
        } else {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/kategori/?id_kategori='.$id);
        }
    }

    public function esambat_detail_by_month($tahun = null, $bulan = null, $limit = null, $offset = null)
    {
        if ($limit) {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/bulan/?limit=' . $limit . '&offset=' . $offset . '&bulan=' . $bulan . '&tahun=' . $tahun);
        } else {
            echo file_get_contents('http://sambat.malangkota.go.id/web/Api/bulan/?bulan=' . $bulan . '&tahun=' . $tahun);
        }
    }

    public function esambat_count($par)
    {
        $par = urldecode($par);
        if ($par) {
            echo file_get_contents('http://sambat.malangkota.go.id/web/ApiGrafik/' . $par);
        } else {
            echo file_get_contents('http://sambat.malangkota.go.id/web/sambatApiGrafik/?v=eyJhbGciOiAiSFMyNTYiLCJ0eXAiOiAiSldUIn0=.eyJjb3VudHJ5IjogIk1hbGFuZyIsIm5hbWUiOiAiRWx6YSBGYWRsaSIsImVtYWlsIjogImVsemFmYWRsaUBnbWFpbC5jb20ifQ==.OAf6fHCZrbTxCmDiQF9Rk9nU/yMAleFn2rn2jJ9yW0Q=');
        }
    }

    public function esambat_category_list()
    {
        echo file_get_contents('http://sambat.malangkota.go.id/web/Apikategori');
    }
}
