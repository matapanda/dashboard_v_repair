<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Katalog_kategory extends CI_Controller
{
    private $submodule;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->model('katalog/katalog_kategori_model', 'kkategori_db');

        $this->submodule = 'katalog';
    }

    public function get($idkategori = null)
    {
        if($idkategori){
            $this->kkategori_db->idkategori = $idkategori;
            $data = $this->kkategori_db->get();
        }else{
            $data = $this->kkategori_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kategori katalog', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kategori katalog', $data);
    }

}
