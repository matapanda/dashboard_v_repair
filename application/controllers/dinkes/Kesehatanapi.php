<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kesehatanapi extends CI_Controller{

    public function __construct(){
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('Response_message');
        $this->load->model('katalog/katalog_model', 'katalog_db');


        $this->submodule = 'katalog';
    }

    public function index(){
        print_r("surya");
    }
    
    #add new by surya ncc
    #get api from SPM
        #->sebaran rumah sakit
        #->sebaran penyakit
        #->sebaran pengajuan PBI
        #->sebaran pengajuan SPM
        #->sebaran pengajuan SPM di terima
    
    public function get_html($url){
        $content = file_get_contents($url);
        return $content;
    }

    public function index_data_faskes(){
        $url = "http://localhost/kios_v2/front_api/faskesapi/get_data_all";
        $data = json_decode(file_get_contents($url));

        $array_json = array();
        if($data->msg_main->status){
            $data_detail = $data->msg_detail->item;
            
            foreach ($data_detail as $key => $value) {
                $nama_faskes = $value->info->nama_jenis;
                $count_item  = count($value->item);
                // print_r($value->item);
                array_push($array_json, array("param"=> $nama_faskes, "val"=>$count_item));
            }
        }

        $data_send["data_json"] = json_encode($array_json);
        $this->load->view("dinkes/kesehatan_faskes", $data_send);
        // print_r($data_send);

    }

    public function index_data_rs(){
        $url = "http://localhost/kios_v2/front_api/kesehatanapi/get_api_kesehatan_all";
        $data = json_decode(file_get_contents($url));

        // print_r($data);
        $array_json = array();
        if($data){
            foreach ($data as $key => $value) {
                $nama_rs = $value->jenis_layanan->nama_layanan;
                $count_item  = count((array)$value->rs);
                // print_r($value->rs);
                array_push($array_json, array("param"=> $nama_rs, "val"=>$count_item));
            }
        }

        $data["data_json"] = json_encode($array_json);
        // $this->load->view("bp2d/bppd_realisasi", $data);
    }

}
