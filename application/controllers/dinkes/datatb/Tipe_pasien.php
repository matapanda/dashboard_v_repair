<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipe_pasien extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('auth');
        $this->load->model('dinkes/datatb/tipe_pasien_model', 'tipe_pasien_db');

        $this->title = 'Tipe Pasien';
        $this->submodule  = 'Data TB';
        $this->user = $this->auth->get_user();

        // $this->auth->check_read($this->submodule);
    }

    public function get($idtipe_pasien = null, $limit = null, $page = null)
    {
        if(!$idtipe_pasien){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->tipe_pasien_db->get($limit, $page);
            }else{
                $data = $this->tipe_pasien_db->get();
            }
        }else{
            $this->tipe_pasien_db->idtipe_pasien = $idtipe_pasien;
            $data = $this->tipe_pasien_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data tipe pasien', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data tipe pasien', $data);
    }


}
