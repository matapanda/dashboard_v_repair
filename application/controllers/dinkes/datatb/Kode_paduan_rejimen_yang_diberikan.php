<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kode_Paduan_Rejimen_Yang_Diberikan extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('auth');
        $this->load->model('dinkes/datatb/kode_paduan_rejimen_yang_diberikan_model', 'kode_rejimen_db');

        $this->title = 'Kode Paduan Rejimen Yang Diberikan';
        $this->submodule  = 'Data TB';
        $this->user = $this->auth->get_user();

        // $this->auth->check_read($this->submodule);
    }

    public function get($idkode_paduan_rejimen_yang_diberikan = null, $limit = null, $page = null)
    {
        if(!$idkode_paduan_rejimen_yang_diberikan){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->kode_rejimen_db->get($limit, $page);
            }else{
                $data = $this->kode_rejimen_db->get();
            }
        }else{
            $this->kode_rejimen_db->iddata_tb = $idkode_paduan_rejimen_yang_diberikan;
            $data = $this->kode_rejimen_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kode rejimen', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kode rejimen', $data);
    }


}
