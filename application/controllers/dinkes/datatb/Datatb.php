<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataTB extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinkes/datatb/data_tb_model', 'datatb_db');

        $this->title = 'Data TB';
        $this->submodule = 'Data TB';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinkes/data-tb/upload');
        $this->url_file_format = base_url('assets/files/Format Dinkes Data TB.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->datatb_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinkes/datatb'));
    }

    public function get($iddata_tb = null, $limit = null, $page = null)
    {
        if (!$iddata_tb) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->datatb_db->get($limit, $page);
            } else {
                $data = $this->datatb_db->get();
            }
        } else {
            $this->datatb_db->iddata_tb = $iddata_tb;
            $data = $this->datatb_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Data TB', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Data TB', $data);
    }

    public function get_d()
    {
        $list = $this->datatb_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datatb_db->count_all(),
            "recordsFiltered" => $this->datatb_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {
        $this->auth->check_write($this->submodule);

        $this->datatb_db->iddata_tb = $this->generate_id->get_id();
        $this->datatb_db->idpasien = $this->input->post('idpasien');
        $this->datatb_db->tanggal = $this->input->post('tanggal');
        $this->datatb_db->nik = $this->input->post('nik');
        $this->datatb_db->nama_pasien = $this->input->post('nama_pasien');
        $this->datatb_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->datatb_db->umur = $this->input->post('umur');
        $this->datatb_db->nomor_registrasi_kab_kota = $this->input->post('nomor_registrasi_kab_kota');
        $this->datatb_db->dirujuk_oleh = $this->input->post('dirujuk_oleh');
        $this->datatb_db->tanggal_mulai_pengobatan = $this->input->post('tanggal_mulai_pengobatan');
        $this->datatb_db->bb_kg = $this->input->post('bb_kg');
        $this->datatb_db->tb_cm = $this->input->post('tb_cm');
        $this->datatb_db->total_scoring_pada_tb_anak = $this->input->post('total_scoring_pada_tb_anak');
        $this->datatb_db->sebelum_pengobatan_no_reg_lab = $this->input->post('sebelum_pengobatan_no_reg_lab');
        $this->datatb_db->sebelum_pengobatan_hasil_dahak = $this->input->post('sebelum_pengobatan_hasil_dahak');
        $this->datatb_db->akhir_tahap_awal_no_reg_lab = $this->input->post('akhir_tahap_awal_no_reg_lab');
        $this->datatb_db->akhir_tahap_awal_hasil_dahak = $this->input->post('akhir_tahap_awal_hasil_dahak');
        $this->datatb_db->akhir_bulan_ke_5_7_no_reg_lab = $this->input->post('akhir_bulan_ke_5_7_no_reg_lab');
        $this->datatb_db->akhir_bulan_ke_5_7_hasil_dahak = $this->input->post('akhir_bulan_ke_5_7_hasil_dahak');
        $this->datatb_db->akhir_pengobatan_no_reg_lab = $this->input->post('akhir_pengobatan_no_reg_lab');
        $this->datatb_db->akhir_pengobatan_hasil_dahak = $this->input->post('akhir_pengobatan_hasil_dahak');
        $this->datatb_db->hasil_pengobatan_sembuh = $this->input->post('hasil_pengobatan_sembuh');
        $this->datatb_db->hasil_pengobatan_lengkap = $this->input->post('hasil_pengobatan_lengkap');
        $this->datatb_db->hasil_pengobatan_default = $this->input->post('hasil_pengobatan_default');
        $this->datatb_db->hasil_pengobatan_gagal = $this->input->post('hasil_pengobatan_gagal');
        $this->datatb_db->hasil_pengobatan_meninggal = $this->input->post('hasil_pengobatan_meninggal');
        $this->datatb_db->hasil_pengobatan_pindah = $this->input->post('hasil_pengobatan_pindah');
        $this->datatb_db->idkelurahan = $this->input->post('idkelurahan');
        $this->datatb_db->riwayat_tes_hiv_tanggal_tes_hiv_terakhir = $this->input->post('riwayat_tes_hiv_tanggal_tes_hiv_terakhir');
        $this->datatb_db->riwayat_tes_hiv_hasil_tes = $this->input->post('riwayat_tes_hiv_hasil_tes');
        $this->datatb_db->layanan_konseling_tanggal_test_hiv = $this->input->post('layanan_konseling_tanggal_test_hiv');
        $this->datatb_db->layanan_konseling_tempat_test = $this->input->post('layanan_konseling_tempat_test');
        $this->datatb_db->layanan_konseling_hasil_test = $this->input->post('layanan_konseling_hasil_test');
        $this->datatb_db->layanan_konseling_tanggal_pasca_test_konseling = $this->input->post('layanan_konseling_tanggal_pasca_test_konseling');
        $this->datatb_db->layanan_konseling_tanggal_pretest_konseling = $this->input->post('layanan_konseling_tanggal_pretest_konseling');
        $this->datatb_db->layanan_konseling_tanggal_dianjurkan = $this->input->post('layanan_konseling_tanggal_dianjurkan');
        $this->datatb_db->layanan_pdp_tanggal_rujukan_pdp = $this->input->post('layanan_pdp_tanggal_rujukan_pdp');
        $this->datatb_db->layanan_pdp_tanggal_mulai_ppk = $this->input->post('layanan_pdp_tanggal_mulai_ppk');
        $this->datatb_db->layanan_pdp_tanggal_mulai_art = $this->input->post('layanan_pdp_tanggal_mulai_art');
        $this->datatb_db->akhir_sisipan_no_reg_lab = $this->input->post('akhir_sisipan_no_reg_lab');
        $this->datatb_db->akhir_sisipan_hasil_dahak = $this->input->post('akhir_sisipan_hasil_dahak');
        $this->datatb_db->area_sentinel_tanggal_pretest_konseling = $this->input->post('area_sentinel_tanggal_pretest_konseling');
        $this->datatb_db->area_sentinel_tanggal_dianjurkan = $this->input->post('area_sentinel_tanggal_dianjurkan');
        $this->datatb_db->area_sentinel_tanggal_test_hiv = $this->input->post('area_sentinel_tanggal_test_hiv');
        $this->datatb_db->area_sentinel_hasil_test = $this->input->post('area_sentinel_hasil_test');
        $this->datatb_db->idtipe_pasien = $this->input->post('idtipe_pasien');
        $this->datatb_db->idklasifikasi_penyakit = $this->input->post('idklasifikasi_penyakit');
        $this->datatb_db->idkode_paduan_rejimen_yang_diberikan = $this->input->post('idkode_paduan_rejimen_yang_diberikan');
        $this->datatb_db->idjenis_fayankes = $this->input->post('idjenis_fayankes');
        $this->datatb_db->idfayankes = $this->input->post('idfayankes');
        $this->datatb_db->idvalidasi_data = $this->input->post('idvalidasi_data');
        $this->datatb_db->idapp_user = $this->auth->get_user('idapp_user');
        $this->datatb_db->idjeniskelamin = $this->input->post('idjeniskelamin');

        $data = $this->datatb_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menambah data Data TB', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menambah data Data TB', $data);
    }

    public function update($iddata_tb)
    {

        $this->auth->check_write($this->submodule);

        $this->datatb_db->iddata_tb = $iddata_tb;
        $this->datatb_db->idpasien = $this->input->post('idpasien');
        $this->datatb_db->tanggal = $this->input->post('tanggal');
        $this->datatb_db->nik = $this->input->post('nik');
        $this->datatb_db->nama_pasien = $this->input->post('nama_pasien');
        $this->datatb_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->datatb_db->umur = $this->input->post('umur');
        $this->datatb_db->nomor_registrasi_kab_kota = $this->input->post('nomor_registrasi_kab_kota');
        $this->datatb_db->dirujuk_oleh = $this->input->post('dirujuk_oleh');
        $this->datatb_db->tanggal_mulai_pengobatan = $this->input->post('tanggal_mulai_pengobatan');
        $this->datatb_db->bb_kg = $this->input->post('bb_kg');
        $this->datatb_db->tb_cm = $this->input->post('tb_cm');
        $this->datatb_db->total_scoring_pada_tb_anak = $this->input->post('total_scoring_pada_tb_anak');
        $this->datatb_db->sebelum_pengobatan_no_reg_lab = $this->input->post('sebelum_pengobatan_no_reg_lab');
        $this->datatb_db->sebelum_pengobatan_hasil_dahak = $this->input->post('sebelum_pengobatan_hasil_dahak');
        $this->datatb_db->akhir_tahap_awal_no_reg_lab = $this->input->post('akhir_tahap_awal_no_reg_lab');
        $this->datatb_db->akhir_tahap_awal_hasil_dahak = $this->input->post('akhir_tahap_awal_hasil_dahak');
        $this->datatb_db->akhir_bulan_ke_5_7_no_reg_lab = $this->input->post('akhir_bulan_ke_5_7_no_reg_lab');
        $this->datatb_db->akhir_bulan_ke_5_7_hasil_dahak = $this->input->post('akhir_bulan_ke_5_7_hasil_dahak');
        $this->datatb_db->akhir_pengobatan_no_reg_lab = $this->input->post('akhir_pengobatan_no_reg_lab');
        $this->datatb_db->akhir_pengobatan_hasil_dahak = $this->input->post('akhir_pengobatan_hasil_dahak');
        $this->datatb_db->hasil_pengobatan_sembuh = $this->input->post('hasil_pengobatan_sembuh');
        $this->datatb_db->hasil_pengobatan_lengkap = $this->input->post('hasil_pengobatan_lengkap');
        $this->datatb_db->hasil_pengobatan_default = $this->input->post('hasil_pengobatan_default');
        $this->datatb_db->hasil_pengobatan_gagal = $this->input->post('hasil_pengobatan_gagal');
        $this->datatb_db->hasil_pengobatan_meninggal = $this->input->post('hasil_pengobatan_meninggal');
        $this->datatb_db->hasil_pengobatan_pindah = $this->input->post('hasil_pengobatan_pindah');
        $this->datatb_db->idkelurahan = $this->input->post('idkelurahan');
        $this->datatb_db->riwayat_tes_hiv_tanggal_tes_hiv_terakhir = $this->input->post('riwayat_tes_hiv_tanggal_tes_hiv_terakhir');
        $this->datatb_db->riwayat_tes_hiv_hasil_tes = $this->input->post('riwayat_tes_hiv_hasil_tes');
        $this->datatb_db->layanan_konseling_tanggal_test_hiv = $this->input->post('layanan_konseling_tanggal_test_hiv');
        $this->datatb_db->layanan_konseling_tempat_test = $this->input->post('layanan_konseling_tempat_test');
        $this->datatb_db->layanan_konseling_hasil_test = $this->input->post('layanan_konseling_hasil_test');
        $this->datatb_db->layanan_konseling_tanggal_pasca_test_konseling = $this->input->post('layanan_konseling_tanggal_pasca_test_konseling');
        $this->datatb_db->layanan_konseling_tanggal_pretest_konseling = $this->input->post('layanan_konseling_tanggal_pretest_konseling');
        $this->datatb_db->layanan_konseling_tanggal_dianjurkan = $this->input->post('layanan_konseling_tanggal_dianjurkan');
        $this->datatb_db->layanan_pdp_tanggal_rujukan_pdp = $this->input->post('layanan_pdp_tanggal_rujukan_pdp');
        $this->datatb_db->layanan_pdp_tanggal_mulai_ppk = $this->input->post('layanan_pdp_tanggal_mulai_ppk');
        $this->datatb_db->layanan_pdp_tanggal_mulai_art = $this->input->post('layanan_pdp_tanggal_mulai_art');
        $this->datatb_db->akhir_sisipan_no_reg_lab = $this->input->post('akhir_sisipan_no_reg_lab');
        $this->datatb_db->akhir_sisipan_hasil_dahak = $this->input->post('akhir_sisipan_hasil_dahak');
        $this->datatb_db->area_sentinel_tanggal_pretest_konseling = $this->input->post('area_sentinel_tanggal_pretest_konseling');
        $this->datatb_db->area_sentinel_tanggal_dianjurkan = $this->input->post('area_sentinel_tanggal_dianjurkan');
        $this->datatb_db->area_sentinel_tanggal_test_hiv = $this->input->post('area_sentinel_tanggal_test_hiv');
        $this->datatb_db->area_sentinel_hasil_test = $this->input->post('area_sentinel_hasil_test');
        $this->datatb_db->idtipe_pasien = $this->input->post('idtipe_pasien');
        $this->datatb_db->idklasifikasi_penyakit = $this->input->post('idklasifikasi_penyakit');
        $this->datatb_db->idkode_paduan_rejimen_yang_diberikan = $this->input->post('idkode_paduan_rejimen_yang_diberikan');
        $this->datatb_db->idjenis_fayankes = $this->input->post('idjenis_fayankes');
        $this->datatb_db->idfayankes = $this->input->post('idfayankes');
        $this->datatb_db->idvalidasi_data = $this->input->post('idvalidasi_data');
        $this->datatb_db->idapp_user = $this->auth->get_user('idapp_user');
        $this->datatb_db->idjeniskelamin = $this->input->post('idjeniskelamin');

        $data = $this->datatb_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Data TB', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Data TB', $data);
    }

    public function delete($iddata_tb)
    {

        $this->auth->check_write($this->submodule);

        $this->datatb_db->iddata_tb = $iddata_tb;
        $data = $this->datatb_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Data TB', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Data TB', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinkes/data-tb');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');
        $this->load->model('master/jeniskelamin_model', 'jeniskelamin_db');
        $this->load->model('dinkes/datatb/tipe_pasien_model', 'tipe_pasien_db');
        $this->load->model('dinkes/datatb/klasifikasi_penyakit_model', 'klasifikasi_db');
        $this->load->model('dinkes/datatb/kode_paduan_rejimen_yang_diberikan_model', 'kode_db');
        $this->load->model('dinkes/datatb/jenisfayankes_model', 'jenis_fayankes_db');
        $this->load->model('dinkes/datatb/fayankes_model', 'fayankes_db');
        $this->load->model('dinkes/datatb/validasi_data_model', 'validasi_db');
        $this->load->model('master/jeniskelamin_model', 'kelamin_db');

        $data_batch = [];

        if (!$data['success']) {
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 4; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            //get kelurahan
            $kelurahan_excel = $rowData[0][10] ?: 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            //get tipe_pasien
            $tipe_pasien_excel = $rowData[0][20] ?: 'lain-lain';
            $this->tipe_pasien_db->tipe_pasien = $tipe_pasien_excel;
            $tipe_pasien_data = $this->tipe_pasien_db->get();
            $idtipe_pasien = $tipe_pasien_data ? $tipe_pasien_data->idtipe_pasien : 99;

            //get klasifikasi_penyakit
            $klasifikasi_penyakit_excel = $rowData[0][19] ?: 'lain-lain';
            $this->klasifikasi_db->klasifikasi_penyakit = $klasifikasi_penyakit_excel;
            $klasifikasi_penyakit_data = $this->klasifikasi_db->get();
            $idklasifikasi_penyakit = $klasifikasi_penyakit_data ? $klasifikasi_penyakit_data->idklasifikasi_penyakit : 99;

            //get kode_paduan_rejimen_yang_diberikan
            $kode_paduan_rejimen_yang_diberikan_excel = $rowData[0][18] ?: 'lain-lain';
            $this->kode_db->kode_paduan_rejimen_yang_diberikan = $kode_paduan_rejimen_yang_diberikan_excel;
            $kode_paduan_rejimen_yang_diberikan_data = $this->kode_db->get();
            $idkode_paduan_rejimen_yang_diberikan = $kode_paduan_rejimen_yang_diberikan_data ? $kode_paduan_rejimen_yang_diberikan_data->idkode_paduan_rejimen_yang_diberikan : 99;

            //get jenis_fayankes
            $jenis_fayankes_excel = $rowData[0][13] ?: 'lain-lain';
            $this->jenis_fayankes_db->jenis_fayankes = $jenis_fayankes_excel;
            $jenis_fayankes_data = $this->jenis_fayankes_db->get();
            $idjenis_fayankes = $jenis_fayankes_data ? $jenis_fayankes_data->idjenis_fayankes : 99;

            //get fayankes
            $fayankes_excel = $rowData[0][12] ?: 'lain-lain';
            $this->fayankes_db->fayankes = $fayankes_excel;
            $fayankes_data = $this->fayankes_db->get();
            $idfayankes = $fayankes_data ? $fayankes_data->idfayankes : 99;

            //get validasi_data
            $validasi_data_excel = $rowData[0][2] ?: 'lain-lain';
            $this->validasi_db->validasi_data = $validasi_data_excel;
            $validasi_data_data = $this->validasi_db->get();
            $idvalidasi_data = $validasi_data_data ? $validasi_data_data->idvalidasi_data : 99;

            //get jenis kelamin
            $jeniskelamin_excel = $rowData[0][6] ?: 'lain-lain';
            $this->kelamin_db->jeniskelamin = $jeniskelamin_excel;
            $jeniskelamin_data = $this->kelamin_db->get();
            $idjeniskelamin = $jeniskelamin_data ? $jeniskelamin_data->idjeniskelamin : 99;

            $data_batch_item['iddata_tb'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');
            $data_batch_item['idvalidasi_data'] = $idvalidasi_data;
            $data_batch_item['nik'] = $rowData[0][3] == false ? '-' : (int)$rowData[0][3];
            $data_batch_item['nama_pasien'] = $rowData[0][4] == false ? '-' : $rowData[0][4];
            $data_batch_item['idpasien'] = $rowData[0][5] == false ? '-' : $rowData[0][5];
            $data_batch_item['idjeniskelamin'] = $idjeniskelamin;
            $data_batch_item['tanggal_lahir'] = $rowData[0][7] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][7], 'YYYY-MM-DD');
            $data_batch_item['umur'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            $data_batch_item['nomor_registrasi_kab_kota'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['dirujuk_oleh'] = $rowData[0][11] == false ? '-' : $rowData[0][11];
            $data_batch_item['idfayankes'] = $idfayankes;
            $data_batch_item['idjenis_fayankes'] = $idjenis_fayankes;
            $data_batch_item['tanggal_mulai_pengobatan'] = $rowData[0][14] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][14], 'YYYY-MM-DD');
            $data_batch_item['bb_kg'] = $rowData[0][15] == false ? 0 : $rowData[0][15];
            $data_batch_item['tb_cm'] = $rowData[0][16] == false ? 0 : $rowData[0][16];
            $data_batch_item['total_scoring_pada_tb_anak'] = $rowData[0][17] == false ? '-' : $rowData[0][17];
            $data_batch_item['idkode_paduan_rejimen_yang_diberikan'] = $idkode_paduan_rejimen_yang_diberikan;
            $data_batch_item['idklasifikasi_penyakit'] = $idklasifikasi_penyakit;
            $data_batch_item['idtipe_pasien'] = $idtipe_pasien;
            $data_batch_item['sebelum_pengobatan_no_reg_lab'] = $rowData[0][21] == false ? '-' : $rowData[0][21];
            $data_batch_item['sebelum_pengobatan_hasil_dahak'] = $rowData[0][22] == false ? '-' : $rowData[0][22];
            $data_batch_item['akhir_tahap_awal_no_reg_lab'] = $rowData[0][23] == false ? '-' : $rowData[0][23];
            $data_batch_item['akhir_tahap_awal_hasil_dahak'] = $rowData[0][24] == false ? '-' : $rowData[0][24];
            $data_batch_item['akhir_sisipan_no_reg_lab'] = $rowData[0][25] == false ? '-' : $rowData[0][25];
            $data_batch_item['akhir_sisipan_hasil_dahak'] = $rowData[0][26] == false ? '-' : $rowData[0][26];
            $data_batch_item['akhir_bulan_ke_5_7_no_reg_lab'] = $rowData[0][27] == false ? '-' : $rowData[0][27];
            $data_batch_item['akhir_bulan_ke_5_7_hasil_dahak'] = $rowData[0][28] == false ? '-' : $rowData[0][28];
            $data_batch_item['akhir_pengobatan_no_reg_lab'] = $rowData[0][29] == false ? '-' : $rowData[0][29];
            $data_batch_item['akhir_pengobatan_hasil_dahak'] = $rowData[0][30] == false ? '-' : $rowData[0][30];
            $data_batch_item['hasil_pengobatan_sembuh'] = $rowData[0][31] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][31], 'YYYY-MM-DD');
            $data_batch_item['hasil_pengobatan_lengkap'] = $rowData[0][32] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][32], 'YYYY-MM-DD');
            $data_batch_item['hasil_pengobatan_default'] = $rowData[0][33] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][33], 'YYYY-MM-DD');
            $data_batch_item['hasil_pengobatan_gagal'] = $rowData[0][34] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][34], 'YYYY-MM-DD');
            $data_batch_item['hasil_pengobatan_meninggal'] = $rowData[0][35] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][35], 'YYYY-MM-DD');
            $data_batch_item['hasil_pengobatan_pindah'] = $rowData[0][36] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][36], 'YYYY-MM-DD');
            $data_batch_item['riwayat_tes_hiv_tanggal_tes_hiv_terakhir'] = $rowData[0][37] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][37], 'YYYY-MM-DD');
            $data_batch_item['riwayat_tes_hiv_hasil_tes'] = $rowData[0][38] == false ? '-' : $rowData[0][38];
            $data_batch_item['layanan_konseling_tanggal_pretest_konseling'] = $rowData[0][39] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][39], 'YYYY-MM-DD');
            $data_batch_item['layanan_konseling_tanggal_dianjurkan'] = $rowData[0][40] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][40], 'YYYY-MM-DD');
            $data_batch_item['layanan_konseling_tanggal_test_hiv'] = $rowData[0][41] == false ? '-' : $rowData[0][41];
            $data_batch_item['layanan_konseling_tempat_test'] = $rowData[0][42] == false ? '-' : $rowData[0][42];
            $data_batch_item['layanan_konseling_hasil_test'] = $rowData[0][43] == false ? '-' : $rowData[0][43];
            $data_batch_item['layanan_konseling_tanggal_pasca_test_konseling'] = $rowData[0][44] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][44], 'YYYY-MM-DD');
            $data_batch_item['area_sentinel_tanggal_pretest_konseling'] = $rowData[0][45] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][45], 'YYYY-MM-DD');
            $data_batch_item['area_sentinel_tanggal_dianjurkan'] = $rowData[0][46] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][46], 'YYYY-MM-DD');
            $data_batch_item['area_sentinel_tanggal_test_hiv'] = $rowData[0][47] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][48], 'YYYY-MM-DD');
            $data_batch_item['area_sentinel_hasil_test'] = $rowData[0][48] == false ? '-' : $rowData[0][43];
            $data_batch_item['layanan_pdp_tanggal_rujukan_pdp'] = $rowData[0][49] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][49], 'YYYY-MM-DD');
            $data_batch_item['layanan_pdp_tanggal_mulai_ppk'] = $rowData[0][50] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][50], 'YYYY-MM-DD');
            $data_batch_item['layanan_pdp_tanggal_mulai_art'] = $rowData[0][51] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][51], 'YYYY-MM-DD');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');

            $data_batch_item['tanggal'] = $this->getFixDate($data_batch_item['tanggal']);
            $data_batch_item['tanggal_lahir'] = $this->getFixDate($data_batch_item['tanggal_lahir']);
            $data_batch_item['tanggal_mulai_pengobatan'] = $this->getFixDate($data_batch_item['tanggal_mulai_pengobatan']);
            $data_batch_item['hasil_pengobatan_sembuh'] = $this->getFixDate($data_batch_item['hasil_pengobatan_sembuh']);
            $data_batch_item['hasil_pengobatan_lengkap'] = $this->getFixDate($data_batch_item['hasil_pengobatan_lengkap']);
            $data_batch_item['hasil_pengobatan_default'] = $this->getFixDate($data_batch_item['hasil_pengobatan_default']);
            $data_batch_item['hasil_pengobatan_gagal'] = $this->getFixDate($data_batch_item['hasil_pengobatan_gagal']);
            $data_batch_item['hasil_pengobatan_meninggal'] = $this->getFixDate($data_batch_item['hasil_pengobatan_meninggal']);
            $data_batch_item['hasil_pengobatan_pindah'] = $this->getFixDate($data_batch_item['hasil_pengobatan_pindah']);
            $data_batch_item['riwayat_tes_hiv_tanggal_tes_hiv_terakhir'] = $this->getFixDate($data_batch_item['riwayat_tes_hiv_tanggal_tes_hiv_terakhir']);
            $data_batch_item['layanan_konseling_tanggal_test_hiv'] = $this->getFixDate($data_batch_item['layanan_konseling_tanggal_test_hiv']);
            $data_batch_item['layanan_konseling_tanggal_pasca_test_konseling'] = $this->getFixDate($data_batch_item['layanan_konseling_tanggal_pasca_test_konseling']);
            $data_batch_item['layanan_konseling_tanggal_pretest_konseling'] = $this->getFixDate($data_batch_item['layanan_konseling_tanggal_pretest_konseling']);
            $data_batch_item['layanan_konseling_tanggal_dianjurkan'] = $this->getFixDate($data_batch_item['layanan_konseling_tanggal_dianjurkan']);
            $data_batch_item['layanan_pdp_tanggal_rujukan_pdp'] = $this->getFixDate($data_batch_item['layanan_pdp_tanggal_rujukan_pdp']);
            $data_batch_item['layanan_pdp_tanggal_mulai_ppk'] = $this->getFixDate($data_batch_item['layanan_pdp_tanggal_mulai_ppk']);
            $data_batch_item['layanan_pdp_tanggal_mulai_art'] = $this->getFixDate($data_batch_item['layanan_pdp_tanggal_mulai_art']);
            $data_batch_item['area_sentinel_tanggal_pretest_konseling'] = $this->getFixDate($data_batch_item['area_sentinel_tanggal_pretest_konseling']);
            $data_batch_item['area_sentinel_tanggal_dianjurkan'] = $this->getFixDate($data_batch_item['area_sentinel_tanggal_dianjurkan']);
            $data_batch_item['area_sentinel_tanggal_test_hiv'] = $this->getFixDate($data_batch_item['area_sentinel_tanggal_test_hiv']);
            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }
        if ($this->datatb_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data tb', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data tb', FALSE);
    }

    function getFixDate($input_date){
        if (strpos($input_date, '/') !== false) {
            $date = explode('/', $input_date);
            if (!isset($date[2]) || !$date[2]) $date[2] = '0000';
            if (!isset($date[1]) || !$date[1]) $date[1] = '00';
            if (!isset($date[0]) || !$date[0]) $date[0] = '00';

            return $date[2] . '-' . $date[1] . '-' . $date[0];
        }
        return $input_date;
    }
}
