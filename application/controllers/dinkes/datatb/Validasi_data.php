<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validasi_Data extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('auth');
        $this->load->model('dinkes/datatb/validasi_data_model', 'validasi_data_db');

        $this->title = 'Validasi Data';
        $this->submodule  = 'Data TB';
        $this->user = $this->auth->get_user();

        // $this->auth->check_read($this->submodule);
    }

    public function get($idvalidasi_data = null, $limit = null, $page = null)
    {
        if(!$idvalidasi_data){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->validasi_data_db->get($limit, $page);
            }else{
                $data = $this->validasi_data_db->get();
            }
        }else{
            $this->validasi_data_db->idvalidasi_data = $idvalidasi_data;
            $data = $this->validasi_data_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Validasi Data', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Validasi Data', $data);
    }


}
