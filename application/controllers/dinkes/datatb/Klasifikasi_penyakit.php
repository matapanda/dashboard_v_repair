<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Klasifikasi_Penyakit extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('auth');
        $this->load->model('dinkes/datatb/klasifikasi_penyakit_model', 'klasifikasi_penyakit_db');

        $this->title = 'Klasifikasi Penyakit';
        $this->submodule  = 'Data TB';
        $this->user = $this->auth->get_user();

        // $this->auth->check_read($this->submodule);
    }

    public function get($idklasifikasi_penyakit = null, $limit = null, $page = null)
    {
        if(!$idklasifikasi_penyakit){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->klasifikasi_penyakit_db->get($limit, $page);
            }else{
                $data = $this->klasifikasi_penyakit_db->get();
            }
        }else{
            $this->klasifikasi_penyakit_db->iddata_tb = $idklasifikasi_penyakit;
            $data = $this->klasifikasi_penyakit_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data klasifikasi penyakit', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data klasifikasi penyakit', $data);
    }


}
