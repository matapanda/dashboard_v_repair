<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fayankes extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('auth');
        $this->load->model('dinkes/datatb/fayankes_model', 'fayankes_db');

        $this->title = 'Fayankes';
        $this->submodule  = 'Data TB';
        $this->user = $this->auth->get_user();

        // $this->auth->check_read($this->submodule);
    }

    public function get($idfayankes = null, $limit = null, $page = null)
    {
        if(!$idfayankes){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->fayankes_db->get($limit, $page);
            }else{
                $data = $this->fayankes_db->get();
            }
        }else{
            $this->fayankes_db->idfayankes = $idfayankes;
            $data = $this->fayankes_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data fayankes', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data fayankes', $data);
    }


}
