<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rencana_persalinan_kendaraan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dinkes/data_bumil/rencana_persalinan_kendaraan_model', 'kendaraan_db');
    }

    public function get($idrencana = null)
    {
        /**
         * Cek can_read
         */
        if($idrencana === null){
            $data = $this->kendaraan_db->get();
        }else{
            $this->kendaraan_db->idrencana_persalinan_kendaraan = $idrencana;
            $data = $this->kendaraan_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data rencana persalinan kendaraan', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data rencana persalinan kendaraan', $data);
    }
}
