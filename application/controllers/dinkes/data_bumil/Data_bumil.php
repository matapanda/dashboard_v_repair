<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_bumil extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinkes/data_bumil/data_bumil_model', 'bumil_db');

        $this->title = 'Data Bumil';
        $this->submodule  = 'Data Bumil';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinkes/data-bumil/upload');
        $this->url_file_format = base_url('assets/files/Format Dinkes Data Bumil.xlsx');

//        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->bumil_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinkes/data_bumil'));
    }

    public function get($iddata_bumil = null, $limit = null, $page = null)
    {
        if(!$iddata_bumil){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->bumil_db->get($limit, $page);
            }else{
                $data = $this->bumil_db->get();
            }
        }else{
            $this->bumil_db->iddata_bumil = $iddata_bumil;
            $data = $this->bumil_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Bumil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Bumil', $data);
    }

    public function get_d()
    {
        $list = $this->bumil_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->bumil_db->count_all(),
            "recordsFiltered" => $this->bumil_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('nama_ibu', 'nama ibu', 'required');
        $this->form_validation->set_rules('nama_suami', 'nama suami', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('idkelurahan', 'kelurahan', 'required');
        $this->form_validation->set_rules('no_nik_ibu', 'no nik ibu', 'required');
        $this->form_validation->set_rules('puskesmas', 'puskesmas', 'required');
        $this->form_validation->set_rules('hpht', 'hpht', 'required');
        $this->form_validation->set_rules('tafsiran_persalinan', 'tafsiran persalinan', 'required');
        $this->form_validation->set_rules('hamil_ke', 'Hamil Ke-', 'required');
        $this->form_validation->set_rules('bb', 'bb', 'required');
        $this->form_validation->set_rules('tb', 'tb', 'required');
        $this->form_validation->set_rules('lila_imt', 'lila imt', 'required');
        $this->form_validation->set_rules('hb_golda', 'hb golda', 'required');
        $this->form_validation->set_rules('tensi', 'tensi', 'required');
        $this->form_validation->set_rules('pendeteksi_resiko_nakes', 'pendeteksi resiko nakes', 'required');
        $this->form_validation->set_rules('pendeteksi_resiko_masyarakat', 'pendeteksi resiko masyarakat', 'required');
        $this->form_validation->set_rules('imunisasi_tt_status_imunisasi_tt', 'imunisasi tt status imunisasi tt', 'required');
        $this->form_validation->set_rules('imunisasi_tt_pemberian_imunisasi_tt', 'imunisasi tt pemberian imunisasi tt', 'required');
        $this->form_validation->set_rules('rencana_persalinan_tempat', 'rencana persalinan tempat', 'required');
        $this->form_validation->set_rules('anc_terpadu_albumin', 'anc terpadu albumin', 'required');
        $this->form_validation->set_rules('anc_terpadu_reduksi', 'anc terpadu reduksi', 'required');
        $this->form_validation->set_rules('anc_terpadu_gds', 'anc terpadu gds', 'required');
        $this->form_validation->set_rules('anc_terpadu_hbsag', 'anc terpadu hbsag', 'required');
        $this->form_validation->set_rules('anc_terpadu_hiv', 'anc terpadu hiv', 'required');
        $this->form_validation->set_rules('faktor_resiko', 'faktor resiko', 'required');
        $this->form_validation->set_rules('idrencana_persalinan_biaya', 'rencana persalinan biaya', 'required');
        $this->form_validation->set_rules('idrencana_persalinan_kendaraan', 'rencana persalinan kendaraan', 'required');
        $this->form_validation->set_rules('umur_ibu', 'umur ibu', 'required');
        $this->form_validation->set_rules('kehamilan_minggu', 'minggu kehamilan', 'required');
        $this->form_validation->set_rules('jarak_kehamilan', 'jarak kehamilan', 'required');
        $this->form_validation->set_rules('idketerangan_bpjs', 'keterangan bpjs', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('rt', 'rt', 'required');
        $this->form_validation->set_rules('rw', 'rw', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->bumil_db->iddata_bumil = $this->generate_id->get_id();
        $this->bumil_db->nama_ibu = $this->input->post('nama_ibu');
        $this->bumil_db->nama_suami = $this->input->post('nama_suami');
        $this->bumil_db->alamat = $this->input->post('alamat');
        $this->bumil_db->idkelurahan = $this->input->post('idkelurahan');
        $this->bumil_db->no_nik_ibu = $this->input->post('no_nik_ibu');
        $this->bumil_db->puskesmas = $this->input->post('puskesmas');
        $this->bumil_db->hpht = $this->input->post('hpht');
        $this->bumil_db->tafsiran_persalinan = $this->input->post('tafsiran_persalinan');
        $this->bumil_db->hamil_ke = $this->input->post('hamil_ke');
        $this->bumil_db->bb = $this->input->post('bb');
        $this->bumil_db->tb = $this->input->post('tb');
        $this->bumil_db->lila_imt = $this->input->post('lila_imt');
        $this->bumil_db->hb_golda = $this->input->post('hb_golda');
        $this->bumil_db->tensi = $this->input->post('tensi');
        $this->bumil_db->pendeteksi_resiko_nakes = $this->input->post('pendeteksi_resiko_nakes');
        $this->bumil_db->pendeteksi_resiko_masyarakat = $this->input->post('pendeteksi_resiko_masyarakat');
        $this->bumil_db->imunisasi_tt_status_imunisasi_tt = $this->input->post('imunisasi_tt_status_imunisasi_tt');
        $this->bumil_db->imunisasi_tt_pemberian_imunisasi_tt = $this->input->post('imunisasi_tt_pemberian_imunisasi_tt');
        $this->bumil_db->rencana_persalinan_tempat = $this->input->post('rencana_persalinan_tempat');
        $this->bumil_db->anc_terpadu_albumin = $this->input->post('anc_terpadu_albumin');
        $this->bumil_db->anc_terpadu_reduksi = $this->input->post('anc_terpadu_reduksi');
        $this->bumil_db->anc_terpadu_gds = $this->input->post('anc_terpadu_gds');
        $this->bumil_db->anc_terpadu_hbsag = $this->input->post('anc_terpadu_hbsag');
        $this->bumil_db->anc_terpadu_hiv = $this->input->post('anc_terpadu_hiv');
        $this->bumil_db->faktor_resiko = $this->input->post('faktor_resiko');
        $this->bumil_db->idrencana_persalinan_biaya = $this->input->post('idrencana_persalinan_biaya');
        $this->bumil_db->idrencana_persalinan_kendaraan = $this->input->post('idrencana_persalinan_kendaraan');
        $this->bumil_db->umur_ibu = $this->input->post('umur_ibu');
        $this->bumil_db->kehamilan_minggu = $this->input->post('kehamilan_minggu');
        $this->bumil_db->jarak_kehamilan = $this->input->post('jarak_kehamilan');
        $this->bumil_db->idketerangan_bpjs = $this->input->post('idketerangan_bpjs');
        $this->bumil_db->tanggal = $this->input->post('tanggal');
        $this->bumil_db->rt = $this->input->post('rt');
        $this->bumil_db->rw = $this->input->post('rw');
        $this->bumil_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->bumil_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Bumil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Bumil', $data);
    }

    public function update($iddata_bumil){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('nama_ibu', 'nama ibu', 'required');
        $this->form_validation->set_rules('nama_suami', 'nama suami', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('idkelurahan', 'kelurahan', 'required');
        $this->form_validation->set_rules('no_nik_ibu', 'no nik ibu', 'required');
        $this->form_validation->set_rules('puskesmas', 'puskesmas', 'required');
        $this->form_validation->set_rules('hpht', 'hpht', 'required');
        $this->form_validation->set_rules('tafsiran_persalinan', 'tafsiran persalinan', 'required');
        $this->form_validation->set_rules('hamil_ke', 'Hamil Ke-', 'required');
        $this->form_validation->set_rules('bb', 'bb', 'required');
        $this->form_validation->set_rules('tb', 'tb', 'required');
        $this->form_validation->set_rules('lila_imt', 'lila imt', 'required');
        $this->form_validation->set_rules('hb_golda', 'hb golda', 'required');
        $this->form_validation->set_rules('tensi', 'tensi', 'required');
        $this->form_validation->set_rules('pendeteksi_resiko_nakes', 'pendeteksi resiko nakes', 'required');
        $this->form_validation->set_rules('pendeteksi_resiko_masyarakat', 'pendeteksi resiko masyarakat', 'required');
        $this->form_validation->set_rules('imunisasi_tt_status_imunisasi_tt', 'imunisasi tt status imunisasi tt', 'required');
        $this->form_validation->set_rules('imunisasi_tt_pemberian_imunisasi_tt', 'imunisasi tt pemberian imunisasi tt', 'required');
        $this->form_validation->set_rules('rencana_persalinan_tempat', 'rencana persalinan tempat', 'required');
        $this->form_validation->set_rules('anc_terpadu_albumin', 'anc terpadu albumin', 'required');
        $this->form_validation->set_rules('anc_terpadu_reduksi', 'anc terpadu reduksi', 'required');
        $this->form_validation->set_rules('anc_terpadu_gds', 'anc terpadu gds', 'required');
        $this->form_validation->set_rules('anc_terpadu_hbsag', 'anc terpadu hbsag', 'required');
        $this->form_validation->set_rules('anc_terpadu_hiv', 'anc terpadu hiv', 'required');
        $this->form_validation->set_rules('faktor_resiko', 'faktor resiko', 'required');
        $this->form_validation->set_rules('idrencana_persalinan_biaya', 'rencana persalinan biaya', 'required');
        $this->form_validation->set_rules('idrencana_persalinan_kendaraan', 'rencana persalinan kendaraan', 'required');
        $this->form_validation->set_rules('umur_ibu', 'umur ibu', 'required');
        $this->form_validation->set_rules('kehamilan_minggu', 'minggu kehamilan', 'required');
        $this->form_validation->set_rules('jarak_kehamilan', 'jarak kehamilan', 'required');
        $this->form_validation->set_rules('idketerangan_bpjs', 'keterangan bpjs', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('rt', 'rt', 'required');
        $this->form_validation->set_rules('rw', 'rw', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->bumil_db->iddata_bumil = $iddata_bumil;
        $this->bumil_db->nama_ibu = $this->input->post('nama_ibu');
        $this->bumil_db->nama_suami = $this->input->post('nama_suami');
        $this->bumil_db->alamat = $this->input->post('alamat');
        $this->bumil_db->idkelurahan = $this->input->post('idkelurahan');
        $this->bumil_db->no_nik_ibu = $this->input->post('no_nik_ibu');
        $this->bumil_db->puskesmas = $this->input->post('puskesmas');
        $this->bumil_db->hpht = $this->input->post('hpht');
        $this->bumil_db->tafsiran_persalinan = $this->input->post('tafsiran_persalinan');
        $this->bumil_db->hamil_ke = $this->input->post('hamil_ke');
        $this->bumil_db->bb = $this->input->post('bb');
        $this->bumil_db->tb = $this->input->post('tb');
        $this->bumil_db->lila_imt = $this->input->post('lila_imt');
        $this->bumil_db->hb_golda = $this->input->post('hb_golda');
        $this->bumil_db->tensi = $this->input->post('tensi');
        $this->bumil_db->pendeteksi_resiko_nakes = $this->input->post('pendeteksi_resiko_nakes');
        $this->bumil_db->pendeteksi_resiko_masyarakat = $this->input->post('pendeteksi_resiko_masyarakat');
        $this->bumil_db->imunisasi_tt_status_imunisasi_tt = $this->input->post('imunisasi_tt_status_imunisasi_tt');
        $this->bumil_db->imunisasi_tt_pemberian_imunisasi_tt = $this->input->post('imunisasi_tt_pemberian_imunisasi_tt');
        $this->bumil_db->rencana_persalinan_tempat = $this->input->post('rencana_persalinan_tempat');
        $this->bumil_db->anc_terpadu_albumin = $this->input->post('anc_terpadu_albumin');
        $this->bumil_db->anc_terpadu_reduksi = $this->input->post('anc_terpadu_reduksi');
        $this->bumil_db->anc_terpadu_gds = $this->input->post('anc_terpadu_gds');
        $this->bumil_db->anc_terpadu_hbsag = $this->input->post('anc_terpadu_hbsag');
        $this->bumil_db->anc_terpadu_hiv = $this->input->post('anc_terpadu_hiv');
        $this->bumil_db->faktor_resiko = $this->input->post('faktor_resiko');
        $this->bumil_db->idrencana_persalinan_biaya = $this->input->post('idrencana_persalinan_biaya');
        $this->bumil_db->idrencana_persalinan_kendaraan = $this->input->post('idrencana_persalinan_kendaraan');
        $this->bumil_db->umur_ibu = $this->input->post('umur_ibu');
        $this->bumil_db->kehamilan_minggu = $this->input->post('kehamilan_minggu');
        $this->bumil_db->jarak_kehamilan = $this->input->post('jarak_kehamilan');
        $this->bumil_db->idketerangan_bpjs = $this->input->post('idketerangan_bpjs');
        $this->bumil_db->tanggal = $this->input->post('tanggal');
        $this->bumil_db->rt = $this->input->post('rt');
        $this->bumil_db->rw = $this->input->post('rw');
        $this->bumil_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->bumil_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Bumil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Bumil', $data);
    }

    public function delete($iddata_bumil){

        $this->auth->check_write($this->submodule);

        $this->bumil_db->iddata_bumil = $iddata_bumil;
        $data = $this->bumil_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Bumil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Bumil', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinkes/data-bumil');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');
        $this->load->model('dinkes/data_bumil/keterangan_bpjs_model', 'keterangan_db');
        $this->load->model('dinkes/data_bumil/rencana_persalinan_biaya_model', 'biaya_db');
        $this->load->model('dinkes/data_bumil/rencana_persalinan_kendaraan_model', 'kendaraan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 4; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            //get kelurahan
            $kelurahan_excel = $rowData[0][7] ? : 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            //rencana persalinan biaya
            $rencana_biaya_excel = $rowData[0][33] ? : 'lain-lain';
            $this->biaya_db->rencana_persalinan_biaya = $rencana_biaya_excel;
            $rencana_biaya_data = $this->biaya_db->get();
            $idrencana_biaya = $rencana_biaya_data ? $rencana_biaya_data->idrencana_persalinan_biaya : 99;

            //rencana persalinan kendaraan
            $rencana_kendaraan_excel = $rowData[0][34] ? : 'lain-lain';
            $this->kendaraan_db->rencana_persalinan_kendaraan = $rencana_kendaraan_excel;
            $rencana_kendaraan_data = $this->kendaraan_db->get();
            $idrencana_kendaraan = $rencana_kendaraan_data ? $rencana_kendaraan_data->idrencana_persalinan_kendaraan : 99;

            //keterangan bpjs
            $keterangan_bpjs_excel = $rowData[0][41] ? : 'lain-lain';
            $this->keterangan_db->keterangan_bpjs = $keterangan_bpjs_excel;
            $keterangan_bpjs_data = $this->keterangan_db->get();
            $idketerangan_bpjs = $keterangan_bpjs_data ? $keterangan_bpjs_data->idketerangan_bpjs : 99;

            //umur ibu
            $umur_ibu = 0;
            if($rowData[0][9] && $rowData[0][9] > 0){
                $umur_ibu = $rowData[0][9];
            }elseif ($rowData[0][10] && $rowData[0][10] > 0){
                $umur_ibu = $rowData[0][10];
            }elseif ($rowData[0][11] && $rowData[0][11] > 0){
                $umur_ibu = $rowData[0][11];
            }

            //kehamilan minggu
            $kehamilan_minggu = 0;
            if($rowData[0][15] && $rowData[0][15] > 0){
                $kehamilan_minggu = $rowData[0][15];
            }elseif ($rowData[0][16] && $rowData[0][16] > 0){
                $kehamilan_minggu = $rowData[0][16];
            }elseif ($rowData[0][17] && $rowData[0][17] > 0){
                $kehamilan_minggu = $rowData[0][17];
            }

            //hamil ke
            $hamil_ke = 0;
            if($rowData[0][18] && $rowData[0][18] > 0){
                $hamil_ke = $rowData[0][18];
            }elseif ($rowData[0][19] && $rowData[0][19] > 0){
                $hamil_ke = $rowData[0][19];
            }elseif ($rowData[0][20] && $rowData[0][20] > 0){
                $hamil_ke = $rowData[0][20];
            }

            //jarak kehamilan
            $jarak_kehamilan = 0;
            if($rowData[0][28] && $rowData[0][28] > 0){
                $jarak_kehamilan = $rowData[0][28];
            }elseif ($rowData[0][29] && $rowData[0][29] > 0){
                $jarak_kehamilan = $rowData[0][29];
            }

            $data_batch_item['iddata_bumil'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['nama_ibu'] = $rowData[0][2] == false ? '-' : $rowData[0][2];
            $data_batch_item['nama_suami'] = $rowData[0][3] == false ? '-' : $rowData[0][3];
            $data_batch_item['alamat'] = $rowData[0][4] == false ? '-' : $rowData[0][4];
            $data_batch_item['rt'] = $rowData[0][5] == false ? '-' : $rowData[0][5];
            $data_batch_item['rw'] = $rowData[0][6] == false ? '-' : $rowData[0][6];
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['no_nik_ibu'] = $rowData[0][8] == false ? '-' : $rowData[0][8];
            $data_batch_item['umur_ibu'] = $umur_ibu;
            $data_batch_item['puskesmas'] = $rowData[0][12] == false ? '-' : $rowData[0][12];
            $data_batch_item['hpht'] = $rowData[0][13] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][13], 'YYYY-MM-DD');;
            $data_batch_item['tafsiran_persalinan'] = $rowData[0][14] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][14], 'YYYY-MM-DD');;
            $data_batch_item['kehamilan_minggu'] = $kehamilan_minggu;
            $data_batch_item['hamil_ke'] = $hamil_ke;
            $data_batch_item['bb'] = $rowData[0][21] == false ? 0 : $rowData[0][21];
            $data_batch_item['tb'] = $rowData[0][22] == false ? 0 : $rowData[0][22];
            $data_batch_item['lila_imt'] = $rowData[0][23] == false ? 0 : $rowData[0][23];
            $data_batch_item['hb_golda'] = $rowData[0][24] == false ? '-' : $rowData[0][24];
            $data_batch_item['tensi'] = $rowData[0][25] == false ? '-' : $rowData[0][25];
            $data_batch_item['pendeteksi_resiko_nakes'] = $rowData[0][26] == false ? 0 : $rowData[0][26];
            $data_batch_item['pendeteksi_resiko_masyarakat'] = $rowData[0][27] == false ? 0 : $rowData[0][27];
            $data_batch_item['jarak_kehamilan'] = $jarak_kehamilan;
            $data_batch_item['imunisasi_tt_status_imunisasi_tt'] = $rowData[0][30] == false ? '-' : $rowData[0][30];
            $data_batch_item['imunisasi_tt_pemberian_imunisasi_tt'] = $rowData[0][31] == false ? '-' : $rowData[0][31];
            $data_batch_item['rencana_persalinan_tempat'] = $rowData[0][32] == false ? '-' : $rowData[0][32];
            $data_batch_item['idrencana_persalinan_biaya'] = $idrencana_biaya;
            $data_batch_item['idrencana_persalinan_kendaraan'] = $idrencana_kendaraan;
            $data_batch_item['anc_terpadu_albumin'] = $rowData[0][35] == false ? '-' : $rowData[0][35];
            $data_batch_item['anc_terpadu_reduksi'] = $rowData[0][36] == false ? '-' : $rowData[0][36];
            $data_batch_item['anc_terpadu_gds'] = $rowData[0][37] == false ? '-' : $rowData[0][37];
            $data_batch_item['anc_terpadu_hbsag'] = $rowData[0][38] == false ? '-' : $rowData[0][38];
            $data_batch_item['anc_terpadu_hiv'] = $rowData[0][39] == false ? '-' : $rowData[0][39];
            $data_batch_item['faktor_resiko'] = $rowData[0][40] == false ? '-' : $rowData[0][40];
            $data_batch_item['idketerangan_bpjs'] = $idketerangan_bpjs;

            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            if(strpos($data_batch_item['hpht'], '/') !== false){
                $date = explode('/', $data_batch_item['hpht']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['hpht'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            if(strpos($data_batch_item['tafsiran_persalinan'], '/') !== false){
                $date = explode('/', $data_batch_item['tafsiran_persalinan']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tafsiran_persalinan'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }
        $this->bumil_db->data_batch = $data_batch;
        if ($this->bumil_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data BUMIL', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data BUMIL', FALSE);
    }
}
