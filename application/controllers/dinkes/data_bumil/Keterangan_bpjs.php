<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keterangan_bpjs extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dinkes/data_bumil/keterangan_bpjs_model', 'keterangan_db');
    }

    public function get($idketerangan = null)
    {
        /**
         * Cek can_read
         */
        if($idketerangan === null){
            $data = $this->keterangan_db->get();
        }else{
            $this->keterangan_db->idketerangan_bpjs = $idketerangan;
            $data = $this->keterangan_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data keterangan BPJS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data keterangan BPJS', $data);
    }
}
