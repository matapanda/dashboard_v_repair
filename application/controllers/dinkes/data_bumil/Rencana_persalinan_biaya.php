<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rencana_persalinan_biaya extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dinkes/data_bumil/rencana_persalinan_biaya_model', 'biaya_db');
    }

    public function get($idrencana = null)
    {
        /**
         * Cek can_read
         */
        if($idrencana === null){
            $data = $this->biaya_db->get();
        }else{
            $this->biaya_db->idrencana_persalinan_biaya = $idrencana;
            $data = $this->biaya_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data rencana persalinan biaya', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data rencana persalinan biaya', $data);
    }
}
