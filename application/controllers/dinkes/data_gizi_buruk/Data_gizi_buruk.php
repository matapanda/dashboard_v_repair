<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_gizi_buruk extends CI_Controller
{

    private $title;
    private $sub_title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinkes/data_gizi_buruk/data_gizi_buruk_model', 'gizi_buruk_db');

        $this->title = 'Data Gizi Buruk';
        $this->sub_title = 'Data Balita';
        $this->submodule = 'Data Gizi Buruk';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinkes/data-gizi-buruk/upload');
        $this->url_file_format = base_url('assets/files/Format Dinkes Data Gizi Buruk.xlsx');

//        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['sub_title'] = $this->sub_title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->gizi_buruk_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinkes/data_gizi_buruk'));
    }

    public function get($iddata_gizi_buruk = null, $limit = null, $page = null)
    {
        if (!$iddata_gizi_buruk) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->gizi_buruk_db->get($limit, $page);
            } else {
                $data = $this->gizi_buruk_db->get();
            }
        } else {
            $this->gizi_buruk_db->iddata_gizi_buruk = $iddata_gizi_buruk;
            $data = $this->gizi_buruk_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data gizi buruk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data gizi buruk', $data);
    }

    public function get_d()
    {
        $list = $this->gizi_buruk_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->gizi_buruk_db->count_all(),
            "recordsFiltered" => $this->gizi_buruk_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('no_kelurahan', 'Nomor Kelurahan', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('umur_bulan', 'Umur Bulan', 'required');
        $this->form_validation->set_rules('bb_kg', 'BB', 'required');
        $this->form_validation->set_rules('tb_cm', 'TB', 'required');
        $this->form_validation->set_rules('bulan_masuk', 'Bulan Masuk', 'required');
        $this->form_validation->set_rules('bulan_keluar', 'Bulan Keluar', 'required');
        $this->form_validation->set_rules('idpuskesmas', 'Nomor PUSKESMAS', 'required');
        $this->form_validation->set_rules('idbalita', 'Balita', 'required');
        $this->form_validation->set_rules('rt', 'RT', 'required');
        $this->form_validation->set_rules('rw', 'RW', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->gizi_buruk_db->iddata_gizi_buruk = $this->generate_id->get_id();
        $this->gizi_buruk_db->tanggal = $this->input->post('tanggal');
        $this->gizi_buruk_db->no_kelurahan = $this->input->post('no_kelurahan');
        $this->gizi_buruk_db->idkelurahan = $this->input->post('idkelurahan');
        $this->gizi_buruk_db->alamat = $this->input->post('alamat');
        $this->gizi_buruk_db->umur_bulan = $this->input->post('umur_bulan');
        $this->gizi_buruk_db->bb_kg = $this->input->post('bb_kg');
        $this->gizi_buruk_db->tb_cm = $this->input->post('tb_cm');
        $this->gizi_buruk_db->bulan_masuk = $this->input->post('bulan_masuk');
        $this->gizi_buruk_db->bulan_keluar = $this->input->post('bulan_keluar');
        $this->gizi_buruk_db->status_keluarga_gakin = $this->input->post('status_keluarga_gakin');
        $this->gizi_buruk_db->idpuskesmas = $this->input->post('idpuskesmas');
        $this->gizi_buruk_db->idbalita = $this->input->post('idbalita');
        $this->gizi_buruk_db->rt = $this->input->post('rt');
        $this->gizi_buruk_db->rw = $this->input->post('rw');
        $this->gizi_buruk_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->gizi_buruk_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data gizi buruk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data gizi buruk', $data);
    }

    public function update($iddata_gizi_buruk)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('no_kelurahan', 'Nomor Kelurahan', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('umur_bulan', 'Umur Bulan', 'required');
        $this->form_validation->set_rules('bb_kg', 'BB', 'required');
        $this->form_validation->set_rules('tb_cm', 'TB', 'required');
        $this->form_validation->set_rules('bulan_masuk', 'Bulan Masuk', 'required');
        $this->form_validation->set_rules('bulan_keluar', 'Bulan Keluar', 'required');
        $this->form_validation->set_rules('idpuskesmas', 'Nomor PUSKESMAS', 'required');
        $this->form_validation->set_rules('idbalita', 'Balita', 'required');
        $this->form_validation->set_rules('rt', 'RT', 'required');
        $this->form_validation->set_rules('rw', 'RW', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->gizi_buruk_db->iddata_gizi_buruk = $iddata_gizi_buruk;
        $this->gizi_buruk_db->tanggal = $this->input->post('tanggal');
        $this->gizi_buruk_db->no_kelurahan = $this->input->post('no_kelurahan');
        $this->gizi_buruk_db->idkelurahan = $this->input->post('idkelurahan');
        $this->gizi_buruk_db->alamat = $this->input->post('alamat');
        $this->gizi_buruk_db->umur_bulan = $this->input->post('umur_bulan');
        $this->gizi_buruk_db->bb_kg = $this->input->post('bb_kg');
        $this->gizi_buruk_db->tb_cm = $this->input->post('tb_cm');
        $this->gizi_buruk_db->bulan_masuk = $this->input->post('bulan_masuk');
        $this->gizi_buruk_db->bulan_keluar = $this->input->post('bulan_keluar');
        $this->gizi_buruk_db->status_keluarga_gakin = $this->input->post('status_keluarga_gakin');
        $this->gizi_buruk_db->idpuskesmas = $this->input->post('idpuskesmas');
        $this->gizi_buruk_db->idbalita = $this->input->post('idbalita');
        $this->gizi_buruk_db->rt = $this->input->post('rt');
        $this->gizi_buruk_db->rw = $this->input->post('rw');
        $this->gizi_buruk_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->gizi_buruk_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data gizi buruk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data gizi buruk', $data);
    }

    public function delete($iddata_gizi_buruk)
    {

        $this->auth->check_write($this->submodule);

        $this->gizi_buruk_db->iddata_gizi_buruk = $iddata_gizi_buruk;
        $data = $this->gizi_buruk_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data gizi buruk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data gizi buruk', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinkes/data-gizi-buruk');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');
        $this->load->model('dinkes/data_gizi_buruk/balita_model', 'balita_db');
        $this->load->model('dinkes/data_gizi_buruk/puskesmas_model', 'puskesmas_db');

        $data_batch = [];

        if (!$data['success']) {
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 3; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            //get kelurahan
            $kelurahan_excel = $rowData[0][3] ?: 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            //puskesmas
            $puskesmas_excel = $rowData[0][7] ?: 'lain-lain';
            $this->puskesmas_db->puskesmas = $puskesmas_excel;
            $puskesmas_data = $this->puskesmas_db->get();
            $idpuskesmas = $puskesmas_data ? $puskesmas_data->idpuskesmas : 99;

            //jenis kelamin
            $idjeniskelamin = 1;
            if ($rowData[0][10] && $rowData[0][10] > 0) {
                $idjeniskelamin = 1;
            } elseif ($rowData[0][11] && $rowData[0][11] > 0) {
                $idjeniskelamin = 2;
            }

            //balita
            $nama_balita_excel = $rowData[0][8] ?: 'lain-lain';
            $nama_orang_tua_excel = $rowData[0][9] ?: 'lain-lain';
            $idbalita = $this->generate_id->get_id();;
            $this->balita_db->nama_balita = $nama_balita_excel;
            $this->balita_db->nama_orang_tua = $nama_orang_tua_excel;
            $balita_data = $this->balita_db->get();
            if($balita_data){
                $idbalita = $balita_data->idbalita;
            }else{
                $this->balita_db->idbalita = $idbalita;
                $this->balita_db->nama_balita = $nama_balita_excel;
                $this->balita_db->nama_orang_tua = $nama_orang_tua_excel;
                $this->balita_db->idjeniskelamin = $idjeniskelamin;
                $data_balita = $this->balita_db->add();
                if (!$data_balita) {
                    return $this->generate_json->get_json(FALSE, 'Gagal Menyimpan Balita', $data_balita);
                }
            }

            $data_batch_item['iddata_gizi_buruk'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');
            $data_batch_item['no_kelurahan'] = $rowData[0][2] == false ? '-' : $rowData[0][2];
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['rw'] = $rowData[0][4] == false ? '-' : $rowData[0][4];
            $data_batch_item['rt'] = $rowData[0][5] == false ? '-' : $rowData[0][5];
            $data_batch_item['alamat'] = $rowData[0][6] == false ? '-' : $rowData[0][6];
            $data_batch_item['idpuskesmas'] = $idpuskesmas;
            $data_batch_item['idbalita'] = $idbalita;
            $data_batch_item['umur_bulan'] = $rowData[0][12] == false ? 0 : $rowData[0][12];
            $data_batch_item['bb_kg'] = $rowData[0][13] == false ? 0 : $rowData[0][13];
            $data_batch_item['tb_cm'] = $rowData[0][14] == false ? 0 : $rowData[0][14];
            $data_batch_item['bulan_masuk'] = $rowData[0][15] == false ? 0 : $rowData[0][15];
            $data_batch_item['bulan_keluar'] = $rowData[0][16] == false ? 0 : $rowData[0][16];
            $data_batch_item['status_keluarga_gakin'] = $rowData[0][17] == false ? 0 : $rowData[0][17];

            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if (strpos($data_batch_item['tanggal'], '/') !== false) {
                $date = explode('/', $data_batch_item['tanggal']);
                if (!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if (!isset($date[1]) || !$date[1]) $date[1] = '00';
                if (!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2] . '-' . $date[1] . '-' . $date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }
        $this->gizi_buruk_db->data_batch = $data_batch;
        if ($this->gizi_buruk_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data gizi buruk', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data gizi buruk', FALSE);
    }
}
