<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puskesmas extends CI_Controller
{

    private $submodule;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinkes/data_gizi_buruk/puskesmas_model', 'puskesmas_db');

//        $this->auth->check_read($this->submodule);
    }

    public function get($idpuskesmas = null, $limit = null, $page = null)
    {
        if(!$idpuskesmas){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->puskesmas_db->get($limit, $page);
            }else{
                $data = $this->puskesmas_db->get();
            }
        }else{
            $this->puskesmas_db->idpuskesmas = $idpuskesmas;
            $data = $this->puskesmas_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data PUSKESMAS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data PUSKESMAS', $data);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('no_puskesmas', 'Nomor Puskesmas', 'required');
        $this->form_validation->set_rules('puskesmas', 'Puskesmas', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->puskesmas_db->idpuskesmas = $this->generate_id->get_id();
        $this->puskesmas_db->no_puskesmas = $this->input->post('no_puskesmas');
        $this->puskesmas_db->puskesmas = $this->input->post('puskesmas');
        $this->puskesmas_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->puskesmas_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data PUSKESMAS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data PUSKESMAS', $data);
    }

    public function update($idpuskesmas){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('no_puskesmas', 'Nomor Puskesmas', 'required');
        $this->form_validation->set_rules('puskesmas', 'Puskesmas', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->puskesmas_db->idpuskesmas = $idpuskesmas;
        $this->puskesmas_db->no_puskesmas = $this->input->post('no_puskesmas');
        $this->puskesmas_db->puskesmas = $this->input->post('puskesmas');
        $this->puskesmas_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->puskesmas_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data PUSKESMAS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data PUSKESMAS', $data);
    }

    public function delete($idpuskesmas){

        $this->auth->check_write($this->submodule);

        $this->puskesmas_db->idpuskesmas = $idpuskesmas;
        $data = $this->puskesmas_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data PUSKESMAS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data PUSKESMAS', $data);
    }
}
