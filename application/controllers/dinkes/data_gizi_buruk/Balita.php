<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Balita extends CI_Controller
{

    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinkes/data_gizi_buruk/balita_model', 'balita_db');
        $this->load->model('dinkes/data_gizi_buruk/data_gizi_buruk_model', 'gizi_db');


        $this->submodule  = 'Data Gizi Buruk';
        $this->user = $this->auth->get_user();

//        $this->auth->check_read($this->submodule);
    }

    public function get($idbalita = null, $limit = null, $page = null)
    {
        if (!$idbalita) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->balita_db->get($limit, $page);
            } else {
                $data = $this->balita_db->get();
            }
        } else {
            $this->balita_db->idbalita = $idbalita;
            $data = $this->balita_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Balita', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Balita', $data);
    }

    public function get_d()
    {
        $list = $this->balita_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->balita_db->count_all(),
            "recordsFiltered" => $this->balita_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('nama_balita', 'Nama Balita', 'required');
        $this->form_validation->set_rules('nama_orang_tua', 'Nama Orang Tua', 'required');
        $this->form_validation->set_rules('idjeniskelamin', 'alamat', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->balita_db->idbalita = $this->generate_id->get_id();
        $this->balita_db->nama_balita = $this->input->post('nama_balita');
        $this->balita_db->nama_orang_tua = $this->input->post('nama_orang_tua');
        $this->balita_db->idjeniskelamin = $this->input->post('idjeniskelamin');

        $data = $this->balita_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Balita', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Balita', $data);
    }

    public function update($idbalita)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('nama_balita', 'Nama Balita', 'required');
        $this->form_validation->set_rules('nama_orang_tua', 'Nama Orang Tua', 'required');
        $this->form_validation->set_rules('idjeniskelamin', 'alamat', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $this->balita_db->idbalita = $idbalita;
        $this->balita_db->nama_balita = $this->input->post('nama_balita');
        $this->balita_db->nama_orang_tua = $this->input->post('nama_orang_tua');
        $this->balita_db->idjeniskelamin = $this->input->post('idjeniskelamin');

        $data = $this->balita_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Balita', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Balita', $data);
    }

    public function delete($idbalita)
    {
        $this->auth->check_write($this->submodule);

        $this->gizi_db->idbalita = $idbalita;
        $data = $this->gizi_db->delete();
        if ($data) {
            $this->balita_db->idbalita = $idbalita;
            $data = $this->balita_db->delete();
            if ($data) {
                return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Balita beserta data gizi buruknya', $data);
            }
            return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Balita', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data gizi buruk balita', $data);
    }
}
