<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spm extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->model('katalog/katalog_model', 'katalog_db');


        $this->submodule = 'katalog';
    }
    
    #add new by surya ncc
    #get api from SPM
        #->sebaran rumah sakit
        #->sebaran penyakit
        #->sebaran pengajuan PBI
        #->sebaran pengajuan SPM
        #->sebaran pengajuan SPM di terima
    
    public function index(){
        $this->load->view('chart/show_spm');
    }

    public function get_index(){
        $content_sebaran_rs = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/rs_data");
        $data = json_decode($content);
    }


#----------------------------------------------------------------------sebaran RS----------------------------------------------------------------------#
    public function get_sebaran_rs(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/rs_data");
        $data = json_decode($content);
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){
                echo "<tr>
                        <td>".$val_data->name."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data->data[0]."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        
        //$this->load->view('chart/show_spm');
    }
    
    public function get_rs_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/rs_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Rumah Sakit";
        $data_send["chart_js"]="var chartData = [";   
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){   
                //print_r($val_data);
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                    	  \"country\": \"".$val_data->name."\",
                                    		\"visits\": ".$val_data->data[0].",
                                        \"color\": \"#3c8dbc\"
                                    	},";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    }
#----------------------------------------------------------------------end sebaran RS----------------------------------------------------------------------#


#----------------------------------------------------------------------end sebaran Penyakit----------------------------------------------------------------------#    
    public function get_sebaran_penyakit(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/penyakit_data");
        //print_r($content);
        $data = json_decode($content);
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){
                echo "<tr>
                        <td>".$val_data->name."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data->data[0]."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        
    }
    
    public function get_penyakit_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/penyakit_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pasien Rujukan Penyakit SPM";
        $data_send["chart_js"]="var chartData = [";   
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $val_data){   
                //print_r($val_data);
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                    	  \"country\": \"".$val_data->name."\",
                                    		\"visits\": ".$val_data->data[0].",
                                        \"color\": \"#3c8dbc\"
                                    	},";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
#----------------------------------------------------------------------end sebaran Penyakit----------------------------------------------------------------------#    



#----------------------------------------------------------------------sebaran penjualan PBI----------------------------------------------------------------------#    
    
    public function get_sebaran_pbi(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/dinsos_data");
        //print_r($content);
        $data = json_decode($content);
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                echo "<tr>
                        <td>".$month[$r_val]."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        //print_r($data);
        
    }

    public function get_pbi_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/dinsos_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)";
        $data_send["chart_js"]="var chartData = [";
           
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                    	  \"country\": \"".$month[$r_val]."\",
                                    		\"visits\": ".$val_data.",
                                        \"color\": \"#3c8dbc\"
                                    	},";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
#----------------------------------------------------------------------End sebaran penjualan PBI----------------------------------------------------------------------#    



#----------------------------------------------------------------------sebaran pengajuan SPM----------------------------------------------------------------------#    

    public function get_sebaran_pengajuan_SPM(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/kel_data");
        //print_r($content);
        $data = json_decode($content);
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                echo "<tr>
                        <td>".$month[$r_val]."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data."</td>
                    </tr>";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        //print_r($data);
    }
    
    public function get_SPM_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/kel_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)";
        $data_send["chart_js"]="var chartData = [";
           
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                    	  \"country\": \"".$month[$r_val]."\",
                                    		\"visits\": ".$val_data.",
                                        \"color\": \"#3c8dbc\"
                                    	},";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
#----------------------------------------------------------------------End sebaran pengajuan SPM----------------------------------------------------------------------#    



#----------------------------------------------------------------------sebaran pengajuan SPM diterima----------------------------------------------------------------------#    

    public function get_sebaran_pengajuan_SPM_diterima(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/verif_data");
        //print_r($content);
        $x = "\"\"";
        $y = "\"\"";
        
        $data = json_decode($content);
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                echo "<tr>
                        <td>".$month[$r_val]."</td>
                        <td align=\"center\">Jumlah</td>
                        <td align=\"center\">".$val_data."</td>
                    </tr>";
                    
                $x = $x.",\"".$month[$r_val]."\"";
                $y = $y.",\"".$val_data."\"";
            }
            //echo "</table>";
        }else{
            echo "<tr>
                        <td colspan=\"3\">data kosong</td>
                  </tr>";
        }
        
        
        //print_r($data);
    }
    
    public function get_SPM_diterima_chart(){
        $content = file_get_contents("https://spm.malangkota.go.id/monitoring/ws/verif_data");
        $data = json_decode($content);
        
        $data_send["title"] = "Sebaran Pengajuan Data PBI(Penerima Bantuan Iuran)";
        $data_send["chart_js"]="var chartData = [";
           
        $month = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        if(!empty($data)){
            //echo "<table border=\"1\">";
            foreach($data as $r_val => $val_data){
                
                $data_send["chart_js"]= $data_send["chart_js"]."
                                        {
                                    	  \"country\": \"".$month[$r_val]."\",
                                    		\"visits\": ".$val_data.",
                                        \"color\": \"#3c8dbc\"
                                    	},";
            }
            //echo "</table>";
        }
        $data_send["chart_js"] = $data_send["chart_js"]."];";
        $this->load->view('chart/chart_spm_slider', $data_send);
    } 
}
