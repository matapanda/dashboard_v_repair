<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modules extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('sys/appmodules', 'modules_db');
    $this->load->library('auth');
    $this->load->library('generate_id');
    $this->load->library('generate_json');
  }

  public function get($limit = null, $page = null)
  {
    if($this->auth->get_user('ismultiskpd')) {
      $idapp_user = null;
    } else {
      $idapp_user = $this->auth->get_user('idapp_user');
    }

    if(!$idapp_user){

      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->modules_db->get($limit, $page);
      }else{

        $data = $this->modules_db->get();
      }
    }else{

      $data = $this->modules_db->get($idapp_user);
    }

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data modules', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil data modules', $data);
  }

}
