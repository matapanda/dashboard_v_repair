<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submodule extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('sys/appsubmodule', 'submodule_db');
    $this->load->library('auth');
    $this->load->library('generate_id');
    $this->load->library('generate_json');
    // if(!$this->auth->get_user()['ismultiskpd']) show_404();
  }

  public function get($idsubmodule = null, $limit = null, $page = null)
  {
    if(!$idsubmodule){
      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->submodule_db->get($limit, $page);
      }else{
        $data = $this->submodule_db->get();
      }
    }else{
      $this->submodule_db->idsubmodule = $idsubmodule;
      $data = $this->submodule_db->get();
    }

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data submodule', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil data submodule', $data);
  }

}
