<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sys/appuser', 'user_db');
        $this->load->library('auth');
        $this->load->library('generate_id');
        $this->load->library('generate_json');
        $this->load->library('generate_view');
        if (!$this->auth->get_user()['ismultiskpd']) show_404();
    }

    public function index()
    {
        $data['submodule'] = 'manage-user';
        $data['submodules'] = $this->auth->get_permissions();
        $data['data'] = $this->user_db->get();
        $data['user'] = $this->auth->get_user();
        $data['title'] = 'App User';

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'app/user'));
    }

    public function get($idapp_user = null, $limit = null, $page = null)
    {
        if (!$idapp_user) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->user_db->get($limit, $page);
            } else {
                $data = $this->user_db->get();
            }
        } else {
            $this->user_db->idapp_user = $idapp_user;
            $data = $this->user_db->get();
        }

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data user', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data user', $data);
    }

    public function add()
    {
        $data['submodule'] = 'manage-user';
        $data['submodules'] = $this->auth->get_permissions();
        $data['data'] = $this->user_db->get();
        $data['user'] = $this->auth->get_user();
        $data['title'] = 'App User';
        $this->load->view('templates/template', $this->generate_view->get_add($data, 'app/user'));
    }

    public function store()
    {
        $idapp_user = $this->generate_id->get_id(TRUE);
        $this->user_db->idskpd = $this->input->post('idskpd');
        $this->user_db->username = $this->input->post('username');
        $this->user_db->password = $this->input->post('password');
        $this->user_db->ismultiskpd = $this->input->post('admin') ? 1 : 0;
        $this->user_db->statusaktif = $this->input->post('statusaktif') ? 1 : 0;
        $this->user_db->idapp_user = $idapp_user;

        if ($this->user_db->get()) return $this->generate_json->get_json(FALSE, 'Username terdaftar', $this->user_db);

        if ($this->user_db->add()) {
            $this->load->model('sys/appusermodules', 'usermodules_db');
            if ($this->input->post('permissions')) {
                $permissions = $this->input->post('permissions');
                foreach ($permissions as $key => $value) {
                    $this->usermodules_db->idsubmodule = $key;
                    $this->usermodules_db->idapp_user = $idapp_user;
                    $this->usermodules_db->iswrite = !empty($value['write']) ? 1 : 0;
                    $this->usermodules_db->isread = !empty($value['read']) ? 1 : 0;
                    if (!$this->usermodules_db->add()) return $this->generate_json->get_json(FALSE, 'Gagal menambah user', $this->user_db);
                }
            }
            return $this->generate_json->get_json(TRUE, 'Berhasil menambah user', $this->user_db);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menambah user', $this->user_db);
    }

    public function update($idapp_user)
    {
        $this->user_db->idskpd = $this->input->post('idskpd');
        $this->user_db->username = $this->input->post('username');
        $this->user_db->password = $this->input->post('password');
        $this->user_db->ismultiskpd = $this->input->post('admin') ? 1 : 0;
        $this->user_db->statusaktif = $this->input->post('statusaktif') ? 1 : 0;
        $this->user_db->idapp_user = $idapp_user;

        if ($this->user_db->update()) {
            $this->load->model('sys/appusermodules', 'usermodules_db');
            if ($this->input->post('permissions')) {
                $permissions = $this->input->post('permissions');
                foreach ($permissions as $key => $value) {
                    $this->usermodules_db->idsubmodule = $key;
                    $this->usermodules_db->idapp_user = $idapp_user;
                    $this->usermodules_db->iswrite = !empty($value['write']) ? 1 : 0;
                    $this->usermodules_db->isread = !empty($value['read']) ? 1 : 0;
                    if (!$this->usermodules_db->add()) {
                        if (!$this->usermodules_db->update()) {
                            return $this->generate_json->get_json(FALSE, 'Gagal memperbarui user', $this->user_db);
                        }
                    }
                }
            }
            return $this->generate_json->get_json(TRUE, 'Berhasil memperbarui user', $this->user_db);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memperbarui user', $this->user_db);
    }

}
