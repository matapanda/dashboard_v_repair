<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodules extends CI_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('sys/appusermodules', 'appusermodules_db');
    $this->load->library('auth');
    $this->load->library('generate_id');
    $this->load->library('generate_json');
  }

  public function get($idapp_user = null, $limit = null, $page = null)
  {
    if(!$idapp_user){
      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->appusermodules_db->get($limit, $page);
      }else{
        $data = $this->appusermodules_db->get();
      }
    }else{
      $this->appusermodules_db->idapp_user = $idapp_user;
      $data = $this->appusermodules_db->get();
    }

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data usermodules', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil data usermodules', $data);
  }

}
