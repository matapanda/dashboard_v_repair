<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
        $this->load->library('generate_view');
    }

    public function index()
    {
        $data['user'] = $this->auth->get_user();

        if (!$data['user']) redirect('/#login', 'location');

        $this->load->model('sys/appuser', 'user_db');
        $this->load->model('katalog/katalog_model', 'katalog_db');

        $this->load->library('auth');
        $data['title'] = 'Dashboard';
        $data['submodule'] = 'Dashboard';
        $data['user'] = $this->auth->get_user();


        $data['list_videos'] = [
            '1-PELAYANAN.mp4',
            '2-ADMINISTRASI _ MANAGEMENT.mp4',
            '3-LEGISLASI.mp4',
            '4-PEMBANGUNAN.mp4',
            '5-KEUANGAN.mp4',
            '6-KEPEGAWAIAN.mp4',
            '7-DINAS _ LEMBAGA.mp4',
            '8-KEWILAYAHAN.mp4',
            '9-KMASYARAKATAN.mp4',
            '11-MEDIA CENTER.mp4',
            '12-PENDIDIKAN.mp4',
            '13-BP2D.mp4'
        ];

        $data['list_thumbnails'] = [
            '1-pelayanan.png',
            '2-administrasi.png',
            '3-legislasi.png',
            '4-pembangunan.png',
            '5-keuangan.png',
            '6-kepegawaian.png',
            '7-dinas-lembaga.png',
            '8-kewilayahan.png',
            '9-kemasyarakatan.png',
            '10-sarana-prasaran.png',
            '11-media-center.png',
            '12-pendidikan.png',
            '13-bp2d.png'
        ];

        $data['list_text_title'] = [
            'Pelayanan',
            'Administrasi & Manajemen',
            'Legislasi',
            'Pembangunan',
            'Keuangan',
            'Kepegawaian',
            'Dinas & Lembaga',
            'Kewilayahan',
            'Kemasyarakatan',
            'Sarana Prasarana',
            'Media Center',
            'Pendidikan',
            'BP2D'

        ];

        $data['list_description'] = [
            'Layanan smartcity terkait kependudukan, perpajakan & retribusi, pendaftaran & perijinan, bisnis & investasi, pengaduan masyarakat, publikasi informasi umum dan pemerintahan.',
            'Layanan smartcity terkait surat elektronik, dokumen elektronik, pendukung keputusan, kolaborasi dan koordinasi, manajemen pelaporan pemerintahan',
            'Layanan smartcity terkait sistem informasi DPRD, sistem pemilu daerah, katalog hukuman, katalog perundang-undangan',
            'Layanan smartcity terkait data pembangunan, perencanaan pembangunan daerah, pengadaan barang dan jasa, pengelolaan dan monitoring proyek, evaluasi dan informasi hasil pembangunan',
            'Layanan smartcity terkait anggaran, kas dan perbendaharaan, dan akuntansi daerah',
            'Layanan smartcity terkait pengadaan PNS, absensi dan penggajian, peniliaian dan kinerja PNS, pendidikan dan latihan',
            'Layanan smartcity terkait pengelolaan barang, katalog barang, pengelolaan pendapatan, dan pengelolaan perusahaan daerah',
            'Tata Ruang dan Lingkungan Hidup, Potensi Daerah, Kehutanan, Pertanian, Peternakan, dan Perkebunan, Perikanan dan Kelautan, Pertambangan dan Energi, Pariwisata, Industri Kecil dan Menegah',
            'Kesehatan, Pendidikan, Ketenagakerjaan, Industri dan Perdagangan, Jaring Pengaman Sosial',
            'Transportasi, Jalan dan Jembatan, Terminal dan Pelabuhan, Sarana Umum',
             'Layanan smartcity untuk melakukan pemantauan kondisi di kota Malang',
            'Layanan smartcity untuk melihat data Pendidikan [Data Pokok Satuan Pendidikan (NPSN), Data Pokok Peserta Didik (NISN), dan Data Pokok Pendidik & Tenaga Kependidikan (PTK)] di kota Malang',
            'Badan Pelanan Pajak Daerah untuk melihat data Jenis Pajak, Target, Realisasi, Persentase, Jumlah OP per jenis pajak dan Ketetapan'

        ];



        $data['list_count'] = [];

        for ($i = 0; $i < 13; $i++){
            $this->katalog_db->idkategori = $i+1;
            array_push($data['list_count'], $this->katalog_db->count());
        }


        $data['template_sidebar'] = $this->load->view('templates/template_sidebar', $data, TRUE);
        $data['template_header'] = $this->load->view('templates/template_header', $data, TRUE);
        $data['template_footer'] = $this->load->view('templates/template_footer', $data, TRUE);
        $data['main_content'] = $this->load->view('dashboard/index', $data, TRUE);
        $this->load->view('templates/template', $data);
    }

    public function login()
    {
        redirect('/#login', 'location');
        $data['user'] = $this->auth->get_user();
        if ($data['user']) redirect('sys/app', 'location');

        $data['title'] = 'Login';
        $data['main_content'] = $this->load->view('app/login', $data);
    }

    public function authenticate()
    {
        $this->load->library('Generate_json');
        $this->load->model('sys/appuser', 'user_db');

        $password = $this->input->post('password');
        $this->user_db->username = $this->input->post('username');
        if ($user = $this->user_db->get()) {

            if ($user->password == $password) {
                if ($user->statusaktif == 1) {
                    $this->auth->generate_session(
                        $user->idapp_user,
                        $user->username,
                        $user->ismultiskpd,
                        $user->idskpd,
                        $user->skpd,
                        $user->statusaktif
                    );

                    $this->auth->generate_session(
                        $user->idapp_user,
                        $user->username,
                        $user->ismultiskpd,
                        $user->idskpd,
                        $user->skpd,
                        $user->statusaktif
                    );

                    $msg = base_url() . '/#menu';
                    $status = TRUE;
                } else {
                    $msg = "Akun anda telah dinonaktifkan";
                    $status = FALSE;
                }
            } else {
                $status = FALSE;
                $msg = "Password tidak cocok dengan username";
            }
        } else {
            $msg = "Username belum terdaftar";
            $status = FALSE;
        }

        return $this->generate_json->get_json($status, $msg, $this->input->post('username'));
    }

    public function logout()
    {
        $this->auth->destroy_session();
        redirect('/', 'location');
    }
}
