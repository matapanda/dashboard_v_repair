<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends CI_Controller
{
    private $submodule;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->model('katalog/katalog_model', 'katalog_db');

        $this->submodule = 'chart';
    }
    public function index()
    {
        $this->load->library('auth');
        $data['title'] = 'Chart';
        $data['submodule'] = 'chart';
        $data['is_login'] = false;
        $data['user'] = '';

        if($this->auth->get_user()){
            $data['is_login'] = true;
            $data['user'] = $this->auth->get_user();
        }

        $data['script_plus'] = $this->load->view('chart/script', $data, TRUE);
        $this->load->view('chart/index', $data);
    }

    public function show($idkatalog){
        $data['title'] = 'Chart';
        $data['submodule'] = 'chart';

        $this->katalog_db->idkatalog = $idkatalog;
        $katalog_data = $this->katalog_db->get();
        $data['katalog_judul'] = $katalog_data->judul;
        $data['katalog_deskripsi'] = $katalog_data->deskripsi;
        $data['katalog_url'] = $katalog_data->url;
        $data['katalog_url_data'] = $katalog_data->url_data;

        $data['script_plus'] = $this->load->view('chart/show_script', $data, TRUE);
        $this->load->view('chart/show', $data);
    }
}
