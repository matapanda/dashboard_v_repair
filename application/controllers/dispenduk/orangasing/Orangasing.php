<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orangasing extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
        $this->load->library('upload_excel');
        $this->load->model('dispenduk/orangasing/orangasing_model', 'orangasing_db');


        $this->title = 'Jumlah Penduduk';
        $this->submodule  = 'Orang Asing';
        $this->user = $this->auth->get_user();
        $this->api_url_upload = base_url('api/dinas-kependudukan/orang-asing/upload');
        $this->url_file_format = base_url('assets/files/Format Dispenduk Data Orang Asing.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->orangasing_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dispenduk/orangasing'));
    }

    public function get($idorangasing = null, $limit = null, $page = null)
    {
        if (!$idorangasing) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->orangasing_db->get($limit, $page);
            } else {
                $data = $this->orangasing_db->get();
            }
        } else {
            $this->orangasing_db->idorangasing = $idorangasing;
            $data = $this->orangasing_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data orang asing', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data orang asing', $data);
    }

    public function get_d()
    {
        $list = $this->orangasing_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->orangasing_db->count_all(),
            "recordsFiltered" => $this->orangasing_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('pendudukawalbulanini_p', 'Penduduk awal bulan ini', 'required');
        $this->form_validation->set_rules('pendudukawalbulanini_l', 'Penduduk awal bulan ini', 'required');
        $this->form_validation->set_rules('lahirbulanini_p', 'lahir bulan ini', 'required');
        $this->form_validation->set_rules('lahirbulanini_l', 'lahir bulan ini', 'required');
        $this->form_validation->set_rules('matibulanini_p', 'mati bulan ini', 'required');
        $this->form_validation->set_rules('matibulanini_l', 'mati bulan ini', 'required');
        $this->form_validation->set_rules('pendatangbulanini_p', 'pendatang bulan ini', 'required');
        $this->form_validation->set_rules('pendatangbulanini_l', 'pendatang bulan ini', 'required');
        $this->form_validation->set_rules('pindahbulanini_p', 'pindah bulan ini', 'required');
        $this->form_validation->set_rules('pindahbulanini_l', 'pindah bulan ini', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_p', 'penduduk akhir bulan ini', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_l', 'penduduk akhir bulan ini', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan.', FALSE);
        }

        $this->orangasing_db->tanggal = $this->input->post('tanggal');
        $this->orangasing_db->idkecamatan = $this->input->post('idkecamatan');
        $this->orangasing_db->pendudukawalbulanini_l = $this->input->post('pendudukawalbulanini_l');
        $this->orangasing_db->pendudukawalbulanini_p = $this->input->post('pendudukawalbulanini_p');
        $this->orangasing_db->lahirbulanini_l = $this->input->post('lahirbulanini_l');
        $this->orangasing_db->lahirbulanini_p = $this->input->post('lahirbulanini_p');
        $this->orangasing_db->matibulanini_l = $this->input->post('matibulanini_l');
        $this->orangasing_db->matibulanini_p = $this->input->post('matibulanini_p');
        $this->orangasing_db->pendatangbulanini_l = $this->input->post('pendatangbulanini_l');
        $this->orangasing_db->pendatangbulanini_p = $this->input->post('pendatangbulanini_p');
        $this->orangasing_db->pindahbulanini_l = $this->input->post('pindahbulanini_l');
        $this->orangasing_db->pindahbulanini_p = $this->input->post('pindahbulanini_p');
        $this->orangasing_db->pendudukakhirbulanini_l = $this->input->post('pendudukakhirbulanini_l');
        $this->orangasing_db->pendudukakhirbulanini_p = $this->input->post('pendudukakhirbulanini_p');
        $this->orangasing_db->idorangasing = $this->generate_id->get_id();
        $this->orangasing_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->orangasing_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data orang asing', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data orang asing', $data);
    }

    public function update($idorangasing)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkecamatan', 'Kecamatan', 'required');
        $this->form_validation->set_rules('pendudukawalbulanini_p', 'Penduduk awal bulan ini', 'required');
        $this->form_validation->set_rules('pendudukawalbulanini_l', 'Penduduk awal bulan ini', 'required');
        $this->form_validation->set_rules('lahirbulanini_p', 'lahir bulan ini', 'required');
        $this->form_validation->set_rules('lahirbulanini_l', 'lahir bulan ini', 'required');
        $this->form_validation->set_rules('matibulanini_p', 'mati bulan ini', 'required');
        $this->form_validation->set_rules('matibulanini_l', 'mati bulan ini', 'required');
        $this->form_validation->set_rules('pendatangbulanini_p', 'pendatang bulan ini', 'required');
        $this->form_validation->set_rules('pendatangbulanini_l', 'pendatang bulan ini', 'required');
        $this->form_validation->set_rules('pindahbulanini_p', 'pindah bulan ini', 'required');
        $this->form_validation->set_rules('pindahbulanini_l', 'pindah bulan ini', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_p', 'penduduk akhir bulan ini', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_l', 'penduduk akhir bulan ini', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan.', FALSE);
        }

        $this->orangasing_db->tanggal = $this->input->post('tanggal');
        $this->orangasing_db->idkecamatan = $this->input->post('idkecamatan');
        $this->orangasing_db->pendudukawalbulanini_l = $this->input->post('pendudukawalbulanini_l');
        $this->orangasing_db->pendudukawalbulanini_p = $this->input->post('pendudukawalbulanini_p');
        $this->orangasing_db->lahirbulanini_l = $this->input->post('lahirbulanini_l');
        $this->orangasing_db->lahirbulanini_p = $this->input->post('lahirbulanini_p');
        $this->orangasing_db->matibulanini_l = $this->input->post('matibulanini_l');
        $this->orangasing_db->matibulanini_p = $this->input->post('matibulanini_p');
        $this->orangasing_db->pendatangbulanini_l = $this->input->post('pendatangbulanini_l');
        $this->orangasing_db->pendatangbulanini_p = $this->input->post('pendatangbulanini_p');
        $this->orangasing_db->pindahbulanini_l = $this->input->post('pindahbulanini_l');
        $this->orangasing_db->pindahbulanini_p = $this->input->post('pindahbulanini_p');
        $this->orangasing_db->pendudukakhirbulanini_l = $this->input->post('pendudukakhirbulanini_l');
        $this->orangasing_db->pendudukakhirbulanini_p = $this->input->post('pendudukakhirbulanini_p');
        $this->orangasing_db->idorangasing = $this->generate_id->get_id();
        $this->orangasing_db->idapp_user = $this->auth->get_user('idapp_user');

        $this->orangasing_db->idorangasing = $idorangasing;
        $this->orangasing_db->idapp_user = $this->auth->get_user('idapp_user');


        $data = $this->orangasing_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data orang asing', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data orang asing', $data);
    }

    public function delete($idorangasing)
    {

        $this->auth->check_write($this->submodule);

        $this->orangasing_db->idorangasing = $idorangasing;
        $data = $this->orangasing_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data orang asing', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data orang asing', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dispenduk/orang-asing');
        $this->load->model('master/kecamatan_model', 'kecamatan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 3; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kecamatan_excel = $rowData[0][2] ? : 'lain-lain';
            $this->kecamatan_db->kecamatan = $kecamatan_excel;
            $kecamatan_data = $this->kecamatan_db->get();
            $idkecamatan = $kecamatan_data ? $kecamatan_data->idkecamatan : 1;

            $data_batch_item['idorangasing'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idkecamatan'] = $idkecamatan;
            $data_batch_item['pendudukawalbulanini_p'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['pendudukawalbulanini_l'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['lahirbulanini_p'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['lahirbulanini_l'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            $data_batch_item['matibulanini_p'] = $rowData[0][7] == false ? 0 : $rowData[0][7];
            $data_batch_item['matibulanini_l'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            $data_batch_item['pendatangbulanini_p'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            $data_batch_item['pendatangbulanini_l'] = $rowData[0][10] == false ? 0 : $rowData[0][10];
            $data_batch_item['pindahbulanini_p'] = $rowData[0][11] == false ? 0 : $rowData[0][11];
            $data_batch_item['pindahbulanini_l'] = $rowData[0][12] == false ? 0 : $rowData[0][12];
            $data_batch_item['pendudukakhirbulanini_p'] = $rowData[0][13] == false ? 0 : $rowData[0][13];
            $data_batch_item['pendudukakhirbulanini_l'] = $rowData[0][14] == false ? 0 : $rowData[0][14];
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->orangasing_db->data_batch = $data_batch;
        if ($this->orangasing_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data orang asing', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data orang asing', FALSE);
    }

}
