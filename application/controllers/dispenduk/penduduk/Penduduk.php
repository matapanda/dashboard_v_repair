<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penduduk extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));
        $this->load->library('upload_excel');
        $this->load->model('dispenduk/penduduk/penduduk_model', 'penduduk_db');


        $this->title = 'Agregat Data Penduduk';
        $this->submodule  = 'Penduduk';
        $this->user = $this->auth->get_user();
        $this->api_url_upload = base_url('api/dinas-kependudukan/penduduk/upload');
        $this->url_file_format = base_url('assets/files/Format Dispenduk Data Penduduk.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->penduduk_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dispenduk/penduduk'));
    }

    public function get($idpenduduk = null, $limit = null, $page = null)
    {
        if (!$idpenduduk) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->penduduk_db->get($limit, $page);
            } else {
                $data = $this->penduduk_db->get();
            }
        } else {
            $this->penduduk_db->idpenduduk = $idpenduduk;
            $data = $this->penduduk_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data penduduk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data penduduk', $data);
    }

    public function get_d()
    {
        $list = $this->penduduk_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->penduduk_db->count_all(),
            "recordsFiltered" => $this->penduduk_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'Keluarahan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlahkk', 'Jumlah KK', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_l', 'Penduduk bulan ini (Laki-laki)', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_p', 'Penduduk bulan ini (Perempuan)', 'required');
        $this->form_validation->set_rules('wajibmemilikiktp_l', 'Wajib memiliki KTP (Laki-laki)', 'required');
        $this->form_validation->set_rules('wajibmemilikiktp_p', 'Wajib memiliki KTP (Perempuan)', 'required');
        $this->form_validation->set_rules('memilikiktp', 'Memiliki KTP', 'required');
        $this->form_validation->set_rules('belumemilikiktp', 'Belum Memiliki KTP', 'required');
        $this->form_validation->set_rules('wajibmemilikiaktekelahiran', 'Wajib memiliki KTP', 'required');
        $this->form_validation->set_rules('memilikiaktakelahiran', 'Memiliki akta kelahiran', 'required');


        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan.', FALSE);
        }

        $this->penduduk_db->idkecamatan = $this->input->post('idkecamatan');
        $this->penduduk_db->tanggal = $this->input->post('tanggal');
        $this->penduduk_db->jumlahkk = $this->input->post('jumlahkk');
        $this->penduduk_db->pendudukakhirbulanini_l = $this->input->post('pendudukakhirbulanini_l');
        $this->penduduk_db->pendudukakhirbulanini_p = $this->input->post('pendudukakhirbulanini_p');
        $this->penduduk_db->wajibmemilikiktp_l = $this->input->post('wajibmemilikiktp_l');
        $this->penduduk_db->wajibmemilikiktp_p = $this->input->post('wajibmemilikiktp_p');
        $this->penduduk_db->memilikiktp = $this->input->post('memilikiktp');
        $this->penduduk_db->belumemilikiktp = $this->input->post('belumemilikiktp');
        $this->penduduk_db->wajibmemilikiaktekelahiran = $this->input->post('wajibmemilikiaktekelahiran');
        $this->penduduk_db->memilikiaktakelahiran = $this->input->post('memilikiaktakelahiran');


        $this->penduduk_db->idpenduduk = $this->generate_id->get_id();
        $this->penduduk_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->penduduk_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data penduduk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data penduduk', $data);
    }

    public function update($idpenduduk)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'Keluarahan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlahkk', 'Jumlah KK', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_l', 'Penduduk bulan ini (Laki-laki)', 'required');
        $this->form_validation->set_rules('pendudukakhirbulanini_p', 'Penduduk bulan ini (Perempuan)', 'required');
        $this->form_validation->set_rules('wajibmemilikiktp_l', 'Wajib memiliki KTP (Laki-laki)', 'required');
        $this->form_validation->set_rules('wajibmemilikiktp_p', 'Wajib memiliki KTP (Perempuan)', 'required');
        $this->form_validation->set_rules('memilikiktp', 'Memiliki KTP', 'required');
        $this->form_validation->set_rules('belumemilikiktp', 'Belum Memiliki KTP', 'required');
        $this->form_validation->set_rules('wajibmemilikiaktekelahiran', 'Wajib memiliki KTP', 'required');
        $this->form_validation->set_rules('memilikiaktakelahiran', 'Memiliki akta kelahiran', 'required');


        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan.', FALSE);
        }

        $this->penduduk_db->idkecamatan = $this->input->post('idkecamatan');
        $this->penduduk_db->tanggal = $this->input->post('tanggal');
        $this->penduduk_db->jumlahkk = $this->input->post('jumlahkk');
        $this->penduduk_db->pendudukakhirbulanini_l = $this->input->post('pendudukakhirbulanini_l');
        $this->penduduk_db->pendudukakhirbulanini_p = $this->input->post('pendudukakhirbulanini_p');
        $this->penduduk_db->wajibmemilikiktp_l = $this->input->post('wajibmemilikiktp_l');
        $this->penduduk_db->wajibmemilikiktp_p = $this->input->post('wajibmemilikiktp_p');
        $this->penduduk_db->memilikiktp = $this->input->post('memilikiktp');
        $this->penduduk_db->belumemilikiktp = $this->input->post('belumemilikiktp');
        $this->penduduk_db->wajibmemilikiaktekelahiran = $this->input->post('wajibmemilikiaktekelahiran');
        $this->penduduk_db->memilikiaktakelahiran = $this->input->post('memilikiaktakelahiran');

        $this->penduduk_db->idpenduduk = $idpenduduk;
        $this->penduduk_db->idapp_user = $this->auth->get_user('idapp_user');


        $data = $this->penduduk_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data penduduk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data penduduk', $data);
    }

    public function delete($idpenduduk)
    {

        $this->auth->check_write($this->submodule);

        $this->penduduk_db->idpenduduk = $idpenduduk;
        $data = $this->penduduk_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data penduduk', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data penduduk', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dispenduk/penduduk');
        $this->load->model('master/kecamatan_model', 'kecamatan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 3; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kecamatan_excel = $rowData[0][2] ? : 'lain-lain';
            $this->kecamatan_db->kecamatan = $kecamatan_excel;
            $kecamatan_data = $this->kecamatan_db->get();
            $idkecamatan = $kecamatan_data ? $kecamatan_data->idkecamatan : 1;

            $data_batch_item['idpenduduk'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idkecamatan'] = $idkecamatan;
            $data_batch_item['jumlahkk'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['pendudukakhirbulanini_p'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['pendudukakhirbulanini_l'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['wajibmemilikiktp_l'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            $data_batch_item['wajibmemilikiktp_p'] = $rowData[0][7] == false ? 0 : $rowData[0][7];
            $data_batch_item['memilikiktp'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            $data_batch_item['belumemilikiktp'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            $data_batch_item['wajibmemilikiaktekelahiran'] = $rowData[0][10] == false ? 0 : $rowData[0][10];
            $data_batch_item['memilikiaktakelahiran'] = $rowData[0][11] == false ? 0 : $rowData[0][11];
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->penduduk_db->data_batch = $data_batch;
        if ($this->penduduk_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data penduduk', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data penduduk', FALSE);
    }
}
