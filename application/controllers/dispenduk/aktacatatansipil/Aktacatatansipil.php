<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktacatatansipil extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dispenduk/aktacatatansipil/aktacatatansipil_model', 'akta_db');


        $this->title = 'Akta Catatan Sipil';
        $this->submodule = 'Akta Catatan Sipil';
        $this->user = $this->auth->get_user();
        $this->api_url_upload = base_url('api/dinas-kependudukan/akta-catatan-sipil/upload');
        $this->url_file_format = base_url('assets/files/Format Dispenduk Data Akta Catatan Sipil.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->akta_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dispenduk/aktacatatansipil'));
    }

    public function get($idaktacatatansipil = null, $limit = null, $page = null)
    {
        if (!$idaktacatatansipil) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->akta_db->get($limit, $page);
            } else {
                $data = $this->akta_db->get();
            }
        } else {
            $this->akta_db->idaktacatatansipil = $idaktacatatansipil;
            $data = $this->akta_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data akta catatan sipil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data akta catatan sipil', $data);
    }

    public function get_d()
    {
        $list = $this->akta_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->akta_db->count_all(),
            "recordsFiltered" => $this->akta_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'tanggal', 'required');
        $this->form_validation->set_rules('idkecamatan', 'idkecamatan', 'required');
        $this->form_validation->set_rules('aktakelahiran_umum', 'aktakelahiran_umum', 'required');
        $this->form_validation->set_rules('aktakelahiran_terlambat', 'aktakelahiran_terlambat', 'required');
        $this->form_validation->set_rules('aktaperkawinan_bulanyanglalu', 'aktaperkawinan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktaperkawinan_bulanini', 'aktaperkawinan_bulanini', 'required');
        $this->form_validation->set_rules('aktaperceraian_bulanyanglalu', 'aktaperceraian_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktakematian_bulanini', 'aktakematian_bulanini', 'required');
        $this->form_validation->set_rules('aktakematian_bulanyanglalu', 'aktakematian_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktaperceraian_bulanini', 'aktaperceraian_bulanini', 'required');
        $this->form_validation->set_rules('aktapengangkatan_bulanyanglalu', 'aktapengangkatan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktapengangkatan_bulanini', 'aktapengangkatan_bulanini', 'required');
        $this->form_validation->set_rules('aktapengakuan_bulanyanglalu', 'aktapengakuan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktapengakuan_bulanini', 'aktapengakuan_bulanini', 'required');
        $this->form_validation->set_rules('aktapengesahan_bulanyanglalu', 'aktapengesahan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktapengesahan_bulanini', 'aktapengesahan_bulanini', 'required');


        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan.', FALSE);
        }

        $this->akta_db->tanggal = $this->input->post('tanggal');
        $this->akta_db->idkecamatan = $this->input->post('idkecamatan');
        $this->akta_db->aktakelahiran_umum = $this->input->post('aktakelahiran_umum');
        $this->akta_db->aktakelahiran_terlambat = $this->input->post('aktakelahiran_terlambat');
        $this->akta_db->aktaperkawinan_bulanyanglalu = $this->input->post('aktaperkawinan_bulanyanglalu');
        $this->akta_db->aktaperkawinan_bulanini = $this->input->post('aktaperkawinan_bulanini');
        $this->akta_db->aktaperceraian_bulanyanglalu = $this->input->post('aktaperceraian_bulanyanglalu');
        $this->akta_db->aktaperceraian_bulanini = $this->input->post('aktaperceraian_bulanini');
        $this->akta_db->aktakematian_bulanyanglalu = $this->input->post('aktakematian_bulanyanglalu');
        $this->akta_db->aktakematian_bulanini = $this->input->post('aktakematian_bulanini');
        $this->akta_db->aktapengangkatan_bulanyanglalu = $this->input->post('aktapengangkatan_bulanyanglalu');
        $this->akta_db->aktapengangkatan_bulanini = $this->input->post('aktapengangkatan_bulanini');
        $this->akta_db->aktapengakuan_bulanyanglalu = $this->input->post('aktapengakuan_bulanyanglalu');
        $this->akta_db->aktapengakuan_bulanini = $this->input->post('aktapengakuan_bulanini');
        $this->akta_db->aktapengesahan_bulanyanglalu = $this->input->post('aktapengesahan_bulanyanglalu');
        $this->akta_db->aktapengesahan_bulanini = $this->input->post('aktapengesahan_bulanini');

        $this->akta_db->idaktacatatansipil = $this->generate_id->get_id();
        $this->akta_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->akta_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data akta catatan sipil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data akta catatan sipil', $data);
    }

    public function update($idaktacatatansipil)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'tanggal', 'required');
        $this->form_validation->set_rules('idkecamatan', 'idkecamatan', 'required');
        $this->form_validation->set_rules('aktakelahiran_umum', 'aktakelahiran_umum', 'required');
        $this->form_validation->set_rules('aktakelahiran_terlambat', 'aktakelahiran_terlambat', 'required');
        $this->form_validation->set_rules('aktaperkawinan_bulanyanglalu', 'aktaperkawinan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktaperkawinan_bulanini', 'aktaperkawinan_bulanini', 'required');
        $this->form_validation->set_rules('aktaperceraian_bulanyanglalu', 'aktaperceraian_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktaperceraian_bulanini', 'aktaperceraian_bulanini', 'required');
        $this->form_validation->set_rules('aktakematian_bulanini', 'aktakematian_bulanini', 'required');
        $this->form_validation->set_rules('aktakematian_bulanyanglalu', 'aktakematian_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktapengangkatan_bulanyanglalu', 'aktapengangkatan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktapengangkatan_bulanini', 'aktapengangkatan_bulanini', 'required');
        $this->form_validation->set_rules('aktapengakuan_bulanyanglalu', 'aktapengakuan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktapengakuan_bulanini', 'aktapengakuan_bulanini', 'required');
        $this->form_validation->set_rules('aktapengesahan_bulanyanglalu', 'aktapengesahan_bulanyanglalu', 'required');
        $this->form_validation->set_rules('aktapengesahan_bulanini', 'aktapengesahan_bulanini', 'required');


        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan.', FALSE);
        }

        $this->akta_db->tanggal = $this->input->post('tanggal');
        $this->akta_db->idkecamatan = $this->input->post('idkecamatan');
        $this->akta_db->aktakelahiran_umum = $this->input->post('aktakelahiran_umum');
        $this->akta_db->aktakelahiran_terlambat = $this->input->post('aktakelahiran_terlambat');
        $this->akta_db->aktaperkawinan_bulanyanglalu = $this->input->post('aktaperkawinan_bulanyanglalu');
        $this->akta_db->aktaperkawinan_bulanini = $this->input->post('aktaperkawinan_bulanini');
        $this->akta_db->aktaperceraian_bulanyanglalu = $this->input->post('aktaperceraian_bulanyanglalu');
        $this->akta_db->aktaperceraian_bulanini = $this->input->post('aktaperceraian_bulanini');
        $this->akta_db->aktakematian_bulanyanglalu = $this->input->post('aktakematian_bulanyanglalu');
        $this->akta_db->aktakematian_bulanini = $this->input->post('aktakematian_bulanini');
        $this->akta_db->aktapengangkatan_bulanyanglalu = $this->input->post('aktapengangkatan_bulanyanglalu');
        $this->akta_db->aktapengangkatan_bulanini = $this->input->post('aktapengangkatan_bulanini');
        $this->akta_db->aktapengakuan_bulanyanglalu = $this->input->post('aktapengakuan_bulanyanglalu');
        $this->akta_db->aktapengakuan_bulanini = $this->input->post('aktapengakuan_bulanini');
        $this->akta_db->aktapengesahan_bulanyanglalu = $this->input->post('aktapengesahan_bulanyanglalu');
        $this->akta_db->aktapengesahan_bulanini = $this->input->post('aktapengesahan_bulanini');

        $this->akta_db->idaktacatatansipil = $idaktacatatansipil;
        $this->akta_db->idapp_user = $this->auth->get_user('idapp_user');


        $data = $this->akta_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data akta catatan sipil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data akta catatan sipil', $data);
    }

    public function delete($idaktacatatansipil)
    {

        $this->auth->check_write($this->submodule);

        $this->akta_db->idaktacatatansipil = $idaktacatatansipil;
        $data = $this->akta_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data akta catatan sipil', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data akta catatan sipil', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dispenduk/catatan-sipil');
        $this->load->model('master/kecamatan_model', 'kecamatan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 3; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kecamatan_excel = $rowData[0][2] ? : 'lain-lain';
            $this->kecamatan_db->kecamatan = $kecamatan_excel;
            $kecamatan_data = $this->kecamatan_db->get();
            $idkecamatan = $kecamatan_data ? $kecamatan_data->idkecamatan : 1;

            $data_batch_item['idaktacatatansipil'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idkecamatan'] = $idkecamatan;
            $data_batch_item['aktakelahiran_umum'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['aktakelahiran_terlambat'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['aktaperkawinan_bulanyanglalu'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['aktaperkawinan_bulanini'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            $data_batch_item['aktaperceraian_bulanyanglalu'] = $rowData[0][7] == false ? 0 : $rowData[0][7];
            $data_batch_item['aktaperceraian_bulanini'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            $data_batch_item['aktakematian_bulanyanglalu'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            $data_batch_item['aktakematian_bulanini'] = $rowData[0][10] == false ? 0 : $rowData[0][10];
            $data_batch_item['aktapengangkatan_bulanyanglalu'] = $rowData[0][11] == false ? 0 : $rowData[0][11];
            $data_batch_item['aktapengangkatan_bulanini'] = $rowData[0][12] == false ? 0 : $rowData[0][12];
            $data_batch_item['aktapengakuan_bulanyanglalu'] = $rowData[0][13] == false ? 0 : $rowData[0][13];
            $data_batch_item['aktapengakuan_bulanini'] = $rowData[0][14] == false ? 0 : $rowData[0][14];
            $data_batch_item['aktapengesahan_bulanyanglalu'] = $rowData[0][15] == false ? 0 : $rowData[0][15];
            $data_batch_item['aktapengesahan_bulanini'] = $rowData[0][16] == false ? 0 : $rowData[0][16];
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->akta_db->data_batch = $data_batch;
        if ($this->akta_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data akta catatan sipil', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data akta catatan sipil', FALSE);
    }
}





















