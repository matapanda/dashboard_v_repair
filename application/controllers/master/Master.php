<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('generate_json');

        $this->load->model('master/hubungankeluarga_model', 'hubungankeluarga_db');
        $this->load->model('master/jeniskelamin_model', 'jeniskelamin_db');
        $this->load->model('master/propinsi_model', 'propinsi_db');
        $this->load->model('master/kota_model', 'kota_db');
        $this->load->model('master/kecamatan_model', 'kecamatan_db');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');
        $this->load->model('master/skpd_model', 'skpd_db');
        $this->load->model('master/statuskis_model', 'statuskis_db');
        $this->load->model('master/statusindividu_model', 'statusindividu_db');
        $this->load->model('master/statuskawin_model', 'statuskawin_db');
        $this->load->model('master/statuskeluarga_model', 'statuskeluarga_db');
        $this->load->model('master/pendidikan_model', 'pendidikan_db');
    }

    public function hubungankeluarga()
    {
        $data = $this->hubungankeluarga_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data hubungankeluarga',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data hubungankeluarga',$data);
    }

    public function jeniskelamin()
    {
        $data = $this->jeniskelamin_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data jeniskelamin',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data jeniskelamin',$data);
    }

    public function propinsi()
    {
        $data = $this->propinsi_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data propinsi',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data propinsi',$data);
    }

    public function kota()
    {
        $data = $this->kota_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kota',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kota',$data);
    }

    public function kecamatan()
    {
        $data = $this->kecamatan_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kecamatan',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kecamatan',$data);
    }

    public function kelurahan()
    {
        $data = $this->kelurahan_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kelurahan',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kelurahan',$data);
    }

    public function pendidikan()
    {
        $data = $this->pendidikan_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data pendidikan',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data pendidikan',$data);
    }

    public function skpd()
    {
        $data = $this->skpd_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data skpd',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data skpd',$data);
    }

    public function statuskis()
    {
        $data = $this->statuskis_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data statuskis',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data statuskis',$data);
    }

    public function statusindividu()
    {
        $data = $this->statusindividu_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data statusindividu',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data statusindividu',$data);
    }

    public function statuskawin()
    {
        $data = $this->statuskawin_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data statuskawin',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data statuskawin',$data);
    }

    public function statuskeluarga()
    {
        $data = $this->statuskeluarga_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data statuskeluarga',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data statuskeluarga',$data);
    }


}
