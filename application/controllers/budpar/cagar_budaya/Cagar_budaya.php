<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cagar_budaya extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view2');
        $this->load->library('auth');
        $this->load->library('form_validation');
       // $this->load->library('Upload_excel');
        $this->load->model('budpar/cagar_budaya/data_cagar_budaya');

        $this->title = 'Cagar Budaya';
        $this->submodule  = 'Cagar Budaya';
        $this->user = $this->auth->get_user();

        //$this->api_url_upload = base_url('api/dinkes/data-bumil/upload');
        //$this->url_file_format = base_url('assets/files/Format Dinkes Data Bumil.xlsx');

//        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        //$data['api_url_upload'] = $this->api_url_upload;
        //$data['url_file_format'] = $this->url_file_format;
        //$data['data_count'] = $this->bumil_db->count_all();
        
        //untuk akses ke view cagar_budaya
        $this->load->view('templates/template', $this->generate_view2->get_index($data, 'budpar/cagar_budaya'));
    }

   

     

    
     

     

     

     
}
