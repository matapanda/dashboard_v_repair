<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Golongan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('bkd/rekapitulasipns/golongan_model', 'golongan_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->golongan_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data golongan',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data golongan',$data);
    }
}