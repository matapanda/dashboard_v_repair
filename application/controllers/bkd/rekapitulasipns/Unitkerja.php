<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unitkerja extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('bkd/rekapitulasipns/unitkerja_model', 'unit_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->unit_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data unit kerja',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data unit kerja',$data);
    }
}