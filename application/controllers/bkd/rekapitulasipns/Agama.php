<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Agama extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('bkd/rekapitulasipns/agama_model', 'agama_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->agama_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data agama',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data agama',$data);
    }
}