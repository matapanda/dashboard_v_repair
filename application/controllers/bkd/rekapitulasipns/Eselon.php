<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Eselon extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('bkd/rekapitulasipns/eselon_model', 'eselon_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->eselon_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data eselon',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data eselon',$data);
    }
}