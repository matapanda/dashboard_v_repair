<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasipns extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('bkd/rekapitulasipns/rekapitulasipns_model', 'rekap_db');

        $this->title = 'Rekapitulasi PNS';
        $this->submodule = 'Rekapitulasi PNS';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/bkd/rekapitulasi-pns/upload');
        $this->url_file_format = base_url('assets/files/Format BKD Data Rekapitulasi PNS.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->rekap_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'bkd/rekapitulasipns'));
    }

    public function get($idrekapitulasipns = null, $limit = null, $page = null)
    {
        if (!$idrekapitulasipns) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->rekap_db->get($limit, $page);
            } else {
                $data = $this->rekap_db->get();
            }
        } else {
            $this->rekap_db->idrekapitulasipns = $idrekapitulasipns;
            $data = $this->rekap_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data rekapitulasi PNS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data rekapitulasi PNS', $data);
    }

    public function get_d()
    {
        $list = $this->rekap_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->rekap_db->count_all(),
            "recordsFiltered" => $this->rekap_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idunitkerja', 'Tanggal', 'required');
        $this->form_validation->set_rules('islam', 'Tanggal', 'required');
        $this->form_validation->set_rules('kristen', 'Tanggal', 'required');
        $this->form_validation->set_rules('katholik', 'Tanggal', 'required');
        $this->form_validation->set_rules('hindu', 'Tanggal', 'required');
        $this->form_validation->set_rules('budha', 'Tanggal', 'required');
        $this->form_validation->set_rules('kawin', 'Tanggal', 'required');
        $this->form_validation->set_rules('belum_kawin', 'Tanggal', 'required');
        $this->form_validation->set_rules('janda', 'Tanggal', 'required');
        $this->form_validation->set_rules('duda', 'Tanggal', 'required');
        $this->form_validation->set_rules('sd', 'Tanggal', 'required');
        $this->form_validation->set_rules('sltp', 'Tanggal', 'required');
        $this->form_validation->set_rules('slta', 'Tanggal', 'required');
        $this->form_validation->set_rules('d', 'Tanggal', 'required');
        $this->form_validation->set_rules('s1', 'Tanggal', 'required');
        $this->form_validation->set_rules('s2', 'Tanggal', 'required');
        $this->form_validation->set_rules('s3', 'Tanggal', 'required');
        $this->form_validation->set_rules('iia', 'Tanggal', 'required');
        $this->form_validation->set_rules('iib', 'Tanggal', 'required');
        $this->form_validation->set_rules('iiia', 'Tanggal', 'required');
        $this->form_validation->set_rules('iiib', 'Tanggal', 'required');
        $this->form_validation->set_rules('iva', 'Tanggal', 'required');
        $this->form_validation->set_rules('ivb', 'Tanggal', 'required');


        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $data_batch = [];
        $data_batch_item = [];

        $data_batch_item['idrekapitulasipns'] = $this->generate_id->get_id();
        $data_batch_item['idunitkerja'] = $this->input->post('idunitkerja');
        $data_batch_item['tanggal'] = $this->input->post('tanggal');
        $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
        $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

        //agama
        $data_batch_item['idpendidikan'] = 99;
        $data_batch_item['idstatuskawin'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idgolongan'] = 99;
        //islam
        $data_batch_item['idagama'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('islam');
        array_push($data_batch, $data_batch_item);
        //kristen
        $data_batch_item['idagama'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('kristen');
        array_push($data_batch, $data_batch_item);
        //katholik
        $data_batch_item['idagama'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('katholik');
        array_push($data_batch, $data_batch_item);
        //hindu
        $data_batch_item['idagama'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('hindu');
        array_push($data_batch, $data_batch_item);
        //budha
        $data_batch_item['idagama'] = 5;
        $data_batch_item['jumlah'] = $this->input->post('budha');
        array_push($data_batch, $data_batch_item);

        //status kawin
        $data_batch_item['idpendidikan'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idagama'] = 99;
        $data_batch_item['idgolongan'] = 99;
        //kawin
        $data_batch_item['idstatuskawin'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('kawin');
        array_push($data_batch, $data_batch_item);
        //belum_kawin
        $data_batch_item['idstatuskawin'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('belum_kawin');
        array_push($data_batch, $data_batch_item);
        //janda
        $data_batch_item['idstatuskawin'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('janda');
        array_push($data_batch, $data_batch_item);
        //duda
        $data_batch_item['idstatuskawin'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('duda');
        array_push($data_batch, $data_batch_item);

        //pendidikan
        $data_batch_item['idstatuskawin'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idagama'] = 99;
        $data_batch_item['idgolongan'] = 99;
        //sd
        $data_batch_item['idpendidikan'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('sd');
        array_push($data_batch, $data_batch_item);
        //sltp
        $data_batch_item['idpendidikan'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('sltp');
        array_push($data_batch, $data_batch_item);
        //slta
        $data_batch_item['idpendidikan'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('slta');
        array_push($data_batch, $data_batch_item);
        //d
        $data_batch_item['idpendidikan'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('d');
        array_push($data_batch, $data_batch_item);
        //s1
        $data_batch_item['idpendidikan'] = 5;
        $data_batch_item['jumlah'] = $this->input->post('s1');
        array_push($data_batch, $data_batch_item);
        //s2
        $data_batch_item['idpendidikan'] = 6;
        $data_batch_item['jumlah'] = $this->input->post('s2');
        array_push($data_batch, $data_batch_item);
        //s3
        $data_batch_item['idpendidikan'] = 7;
        $data_batch_item['jumlah'] = $this->input->post('s3');
        array_push($data_batch, $data_batch_item);

        //golongan
        $data_batch_item['idpendidikan'] = 99;
        $data_batch_item['idstatuskawin'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idagama'] = 99;
        //iia
        $data_batch_item['idgolongan'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('iia');
        array_push($data_batch, $data_batch_item);
        //iib
        $data_batch_item['idgolongan'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('iib');
        array_push($data_batch, $data_batch_item);
        //iiia
        $data_batch_item['idgolongan'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('iiia');
        array_push($data_batch, $data_batch_item);
        //iiib
        $data_batch_item['idgolongan'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('iiib');
        array_push($data_batch, $data_batch_item);
        //iva
        $data_batch_item['idgolongan'] = 5;
        $data_batch_item['jumlah'] = $this->input->post('iva');
        array_push($data_batch, $data_batch_item);
        //ivb
        $data_batch_item['idgolongan'] = 6;
        $data_batch_item['jumlah'] = $this->input->post('ivb');
        array_push($data_batch, $data_batch_item);

        $this->rekap_db->data_batch = $data_batch;

        if ($this->rekap_db->add_batch($data_batch)) {
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data rekapitulasi PNS', TRUE);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data rekapitulasi PNS', FALSE);
    }

    public function update($idunitkerja, $date = null)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idunitkerja', 'Tanggal', 'required');
        $this->form_validation->set_rules('islam', 'Tanggal', 'required');
        $this->form_validation->set_rules('kristen', 'Tanggal', 'required');
        $this->form_validation->set_rules('katholik', 'Tanggal', 'required');
        $this->form_validation->set_rules('hindu', 'Tanggal', 'required');
        $this->form_validation->set_rules('budha', 'Tanggal', 'required');
        $this->form_validation->set_rules('kawin', 'Tanggal', 'required');
        $this->form_validation->set_rules('belum_kawin', 'Tanggal', 'required');
        $this->form_validation->set_rules('janda', 'Tanggal', 'required');
        $this->form_validation->set_rules('duda', 'Tanggal', 'required');
        $this->form_validation->set_rules('sd', 'Tanggal', 'required');
        $this->form_validation->set_rules('sltp', 'Tanggal', 'required');
        $this->form_validation->set_rules('slta', 'Tanggal', 'required');
        $this->form_validation->set_rules('d', 'Tanggal', 'required');
        $this->form_validation->set_rules('s1', 'Tanggal', 'required');
        $this->form_validation->set_rules('s2', 'Tanggal', 'required');
        $this->form_validation->set_rules('s3', 'Tanggal', 'required');
        $this->form_validation->set_rules('iia', 'Tanggal', 'required');
        $this->form_validation->set_rules('iib', 'Tanggal', 'required');
        $this->form_validation->set_rules('iiia', 'Tanggal', 'required');
        $this->form_validation->set_rules('iiib', 'Tanggal', 'required');
        $this->form_validation->set_rules('iva', 'Tanggal', 'required');
        $this->form_validation->set_rules('ivb', 'Tanggal', 'required');


        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Semua field diperlukan', FALSE);
        }

        $data_batch = [];
        $data_batch_item = [];

        $data_batch_item['idrekapitulasipns'] = $idrekapitulasipns;
        $data_batch_item['idunitkerja'] = $this->input->post('idunitkerja');
        $data_batch_item['tanggal'] = $this->input->post('tanggal');
        $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
        $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

        //agama
        $data_batch_item['idpendidikan'] = 99;
        $data_batch_item['idjeniskelamin'] = 99;
        $data_batch_item['idstatuskawin'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idgolongan'] = 99;
        //islam
        $data_batch_item['idagama'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('islam');
        array_push($data_batch, $data_batch_item);
        //kristen
        $data_batch_item['idagama'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('kristen');
        array_push($data_batch, $data_batch_item);
        //katholik
        $data_batch_item['idagama'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('katholik');
        array_push($data_batch, $data_batch_item);
        //hindu
        $data_batch_item['idagama'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('hindu');
        array_push($data_batch, $data_batch_item);
        //budha
        $data_batch_item['idagama'] = 5;
        $data_batch_item['jumlah'] = $this->input->post('budha');
        array_push($data_batch, $data_batch_item);

        //status kawin
        $data_batch_item['idpendidikan'] = 99;
        $data_batch_item['idjeniskelamin'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idagama'] = 99;
        $data_batch_item['idgolongan'] = 99;
        //kawin
        $data_batch_item['idstatuskawin'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('kawin');
        array_push($data_batch, $data_batch_item);
        //belum_kawin
        $data_batch_item['idstatuskawin'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('belum_kawin');
        array_push($data_batch, $data_batch_item);
        //janda
        $data_batch_item['idstatuskawin'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('janda');
        array_push($data_batch, $data_batch_item);
        //duda
        $data_batch_item['idstatuskawin'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('duda');
        array_push($data_batch, $data_batch_item);

        //pendidikan
        $data_batch_item['idjeniskelamin'] = 99;
        $data_batch_item['idstatuskawin'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idagama'] = 99;
        $data_batch_item['idgolongan'] = 99;
        //sd
        $data_batch_item['idpendidikan'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('sd');
        array_push($data_batch, $data_batch_item);
        //sltp
        $data_batch_item['idpendidikan'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('sltp');
        array_push($data_batch, $data_batch_item);
        //slta
        $data_batch_item['idpendidikan'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('slta');
        array_push($data_batch, $data_batch_item);
        //d
        $data_batch_item['idpendidikan'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('d');
        array_push($data_batch, $data_batch_item);
        //s1
        $data_batch_item['idpendidikan'] = 5;
        $data_batch_item['jumlah'] = $this->input->post('s1');
        array_push($data_batch, $data_batch_item);
        //s2
        $data_batch_item['idpendidikan'] = 6;
        $data_batch_item['jumlah'] = $this->input->post('s2');
        array_push($data_batch, $data_batch_item);
        //s3
        $data_batch_item['idpendidikan'] = 7;
        $data_batch_item['jumlah'] = $this->input->post('s3');
        array_push($data_batch, $data_batch_item);

        //golongan
        $data_batch_item['idpendidikan'] = 99;
        $data_batch_item['idjeniskelamin'] = 99;
        $data_batch_item['idstatuskawin'] = 99;
        $data_batch_item['ideselon'] = 99;
        $data_batch_item['idagama'] = 99;
        //iia
        $data_batch_item['idgolongan'] = 1;
        $data_batch_item['jumlah'] = $this->input->post('iia');
        array_push($data_batch, $data_batch_item);
        //iib
        $data_batch_item['idgolongan'] = 2;
        $data_batch_item['jumlah'] = $this->input->post('iib');
        array_push($data_batch, $data_batch_item);
        //iiia
        $data_batch_item['idgolongan'] = 3;
        $data_batch_item['jumlah'] = $this->input->post('iiia');
        array_push($data_batch, $data_batch_item);
        //iiib
        $data_batch_item['idgolongan'] = 4;
        $data_batch_item['jumlah'] = $this->input->post('iiib');
        array_push($data_batch, $data_batch_item);
        //iva
        $data_batch_item['idgolongan'] = 5;
        $data_batch_item['jumlah'] = $this->input->post('iva');
        array_push($data_batch, $data_batch_item);
        //ivb
        $data_batch_item['idgolongan'] = 6;
        $data_batch_item['jumlah'] = $this->input->post('ivb');
        array_push($data_batch, $data_batch_item);

        $this->rekap_db->data_batch = $data_batch;

        if ($this->rekap_db->add_batch($data_batch)) {
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil merubah data rekapitulasi PNS', TRUE);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal merubah data rekapitulasi PNS', FALSE);
    }

    public function delete($idunitkerja, $tanggal)
    {

        $this->auth->check_write($this->submodule);

        $this->rekap_db->idunitkerja = $idunitkerja;
        $this->rekap_db->tanggal = $tanggal;
        $data = $this->rekap_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data rekapitulasi PNS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data rekapitulasi PNS', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('bkd/rekapitulasi-pns');

        $data_batch = [];

        if (!$data['success']) {
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }
        $idunitkerja = 1;
        for ($row = 3; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $data_batch_item['idrekapitulasipns'] = $this->generate_id->get_id();
            $data_batch_item['idunitkerja'] = $idunitkerja;
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if (strpos($data_batch_item['tanggal'], '/') !== false) {
                $date = explode('/', $data_batch_item['tanggal']);
                if (!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if (!isset($date[1]) || !$date[1]) $date[1] = '00';
                if (!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2] . '-' . $date[1] . '-' . $date[0];
            }
            //agama
            $idagama = 1;
            $row_jumlah = 3;
            for ($i = 0; $i < 5; $i++) {
                $data_batch_item['idpendidikan'] = 99;
                $data_batch_item['idjeniskelamin'] = 99;
                $data_batch_item['idstatuskawin'] = 99;
                $data_batch_item['ideselon'] = 99;
                $data_batch_item['idagama'] = $idagama;
                $data_batch_item['idgolongan'] = 99;
                $data_batch_item['jumlah'] = $rowData[0][$row_jumlah] == false ? 0 : $rowData[0][$row_jumlah];
                array_push($data_batch, $data_batch_item);
                $idagama++;
                $row_jumlah++;
            }

            //status kawin
            $idstatuskawin = 1;
            $row_jumlah = 8;
            for ($i = 0; $i < 4; $i++) {
                $data_batch_item['idpendidikan'] = 99;
                $data_batch_item['idjeniskelamin'] = 99;
                $data_batch_item['idstatuskawin'] = $idstatuskawin;
                $data_batch_item['ideselon'] = 99;
                $data_batch_item['idagama'] = 99;
                $data_batch_item['idgolongan'] = 99;
                $data_batch_item['jumlah'] = $rowData[0][$row_jumlah] == false ? 0 : $rowData[0][$row_jumlah];
                array_push($data_batch, $data_batch_item);
                $idstatuskawin++;
                $row_jumlah++;
            }

            //pendidikan
            $idpendidikan = 1;
            $row_jumlah = 12;
            for ($i = 0; $i < 7; $i++) {
                $data_batch_item['idpendidikan'] = $idpendidikan;
                $data_batch_item['idjeniskelamin'] = 99;
                $data_batch_item['idstatuskawin'] = 99;
                $data_batch_item['ideselon'] = 99;
                $data_batch_item['idagama'] = 99;
                $data_batch_item['idgolongan'] = 99;
                $data_batch_item['jumlah'] = $rowData[0][$row_jumlah] == false ? 0 : $rowData[0][$row_jumlah];
                array_push($data_batch, $data_batch_item);
                $idpendidikan++;
                $row_jumlah++;
            }

            //golongan
            $idgolongan = 1;
            $row_jumlah = 19;
            for ($i = 0; $i < 6; $i++) {
                $data_batch_item['idpendidikan'] = 99;
                $data_batch_item['idjeniskelamin'] = 99;
                $data_batch_item['idstatuskawin'] = 99;
                $data_batch_item['ideselon'] = 99;
                $data_batch_item['idagama'] = 99;
                $data_batch_item['idgolongan'] = $idgolongan;
                $data_batch_item['jumlah'] = $rowData[0][$row_jumlah] == false ? 0 : $rowData[0][$row_jumlah];
                array_push($data_batch, $data_batch_item);
                $idgolongan++;
                $row_jumlah++;
            }

            unset($data_batch_item);
            $idunitkerja++;
        }

        $this->rekap_db->data_batch = $data_batch;
        if ($this->rekap_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data rekapitulasi PNS', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data rekapitulasi PNS', FALSE);
    }
}
