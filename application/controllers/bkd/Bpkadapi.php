<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bpkadapi extends CI_Controller {

	
	public function index(){
		// $this->load->view('welcome_message');
		$url_cek_nik = "http://simpeg.malangkota.go.id/serv/get.php";
		$result = file_get_contents($url_cek_nik);
		// $ch = curl_init();

  //       curl_setopt($ch, CURLOPT_URL, $url_cek_nik);
  //       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  //       $result = curl_exec($ch);
  //       curl_close($ch);

        // print_r($result);

        $data_json = json_decode($result)->var_item;

        $data_array = array();
   		$data_table_array = array("golongan"=>"",
   								"jenis_kelamin"=>"",
   								"agama"=>"",
   								"status"=>"",
   								"jumlah_total"=>""
   								);
   		$str_table_fix = "";
   		$data_array_fix = "";

   		$no = 1;
	   	foreach ($data_json as $r_data_json => $v_data_json) {
	   		// $str_unit_kerja = "";
	   		// if($v_data_json->unit_kerja){
	   		// 	$in = 1; 
	   		// 	foreach (explode(" ", $v_data_json->unit_kerja) as $key => $value) {
	   		// 		$str_unit_kerja .= $value;
	   		// 		if($in%2 == 0){
	   		// 			$str_unit_kerja.= "\n";
	   		// 		}else {
	   		// 			$str_unit_kerja.= " ";
	   		// 		}
	   		// 		$in++;
	   		// 	}
	   		// }
	   		// $str_unit_kerja .= "";

	   		$str_unit_kerja = $v_data_json->unit_kerja;

	   		$data_array["golongan"][$r_data_json]["category"] = $str_unit_kerja;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_I"] = $v_data_json->jumlah_golongan_I;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_II"] = $v_data_json->jumlah_golongan_II;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_III"] = $v_data_json->jumlah_golongan_III;
	   		$data_array["golongan"][$r_data_json]["jumlah_golongan_IV"] = $v_data_json->jumlah_golongan_IV;

	   		$data_table_array["golongan"] = $data_table_array["golongan"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_I."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_II."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_III."</td>
																<td align=\"right\">".$v_data_json->jumlah_golongan_IV."</td>
															</tr>";


	   		$data_array["jenis_kelamin"][$r_data_json]["category"] = $str_unit_kerja;
	   		$data_array["jenis_kelamin"][$r_data_json]["laki"] = $v_data_json->laki;
	   		$data_array["jenis_kelamin"][$r_data_json]["perempuan"] = $v_data_json->perempuan;

	   		$data_table_array["jenis_kelamin"] = $data_table_array["jenis_kelamin"]."<tr>
																<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->laki."</td>
																<td align=\"right\">".$v_data_json->perempuan."</td>
															</tr>";

	   		$data_array["agama"][$r_data_json]["category"] = $str_unit_kerja;
	   		$data_array["agama"][$r_data_json]["agama_islam"] = $v_data_json->agama_islam;
	   		$data_array["agama"][$r_data_json]["agama_kristen"] = $v_data_json->agama_kristen;
	   		$data_array["agama"][$r_data_json]["agama_katholik"] = $v_data_json->agama_katholik;
	   		$data_array["agama"][$r_data_json]["agama_hindu"] = $v_data_json->agama_hindu;
	   		$data_array["agama"][$r_data_json]["agama_budha"] = $v_data_json->agama_budha;

	   		$data_table_array["agama"] = $data_table_array["agama"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->agama_islam."</td>
																<td align=\"right\">".$v_data_json->agama_kristen."</td>
																<td align=\"right\">".$v_data_json->agama_katholik."</td>
																<td align=\"right\">".$v_data_json->agama_hindu."</td>
																<td align=\"right\">".$v_data_json->agama_budha."</td>
															</tr>";

	   		$data_array["status"][$r_data_json]["category"] = $str_unit_kerja;
	   		$data_array["status"][$r_data_json]["status_kawin"] = $v_data_json->status_kawin;
	   		$data_array["status"][$r_data_json]["status_belum_kawin"] = $v_data_json->status_belum_kawin;

	   		$data_table_array["status"] = $data_table_array["status"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->status_kawin."</td>
																<td align=\"right\">".$v_data_json->status_belum_kawin."</td>
															</tr>";

			$data_array["jumlah_total"][$r_data_json]["category"] = $str_unit_kerja;
			$data_array["jumlah_total"][$r_data_json]["jml"] = $v_data_json->jumlah_total;

			$data_table_array["jumlah_total"] = $data_table_array["jumlah_total"]."<tr>
	   															<td>".$no."</td>
																<td>".$v_data_json->unit_kerja."</td>
																<td align=\"right\">".$v_data_json->jumlah_total."</td>
															</tr>";
	   						
			$no++;
	   	}
	   	$data["data_json"] 			= json_encode($data_array);
	   	$data["data_table_array"] 	= json_encode($data_table_array);

	   	// print_r("<pre>");
	   	// print_r($data["data_json"]);
	   	$this->load->view("bkd/pegawai/bpkad", $data);
	}


	public function show_detail(){
		$url_cek_nik = "http://simpeg.malangkota.go.id/serv/get.php";
		$result = file_get_contents($url_cek_nik);
		// print_r($data);
		// $ch = curl_init();

  //       curl_setopt($ch, CURLOPT_URL, $url_cek_nik);
  //       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  //       $result = curl_exec($ch);
  //       curl_close($ch);

        $data_json = json_decode($result)->var_item;

        $data_array = array();
   		$data_table_array = array("golongan"=>"",
   								"jenis_kelamin"=>"",
   								"agama"=>"",
   								"status"=>"",
   								"jumlah_total"=>""
   								);

   		$data_table_array = array();
   		$str_table_fix = "";
   		$data_array_fix = "";

   		$str_select = "<select class=\"form-control\" id=\"jenis_instansi\">";

   		$no = 1;
	   	foreach ($data_json as $r_data_json => $v_data_json) {
	   		$str_unit_kerja = $v_data_json->unit_kerja;
	   		$data_table_array[$no]["unit_kerja"] = $str_unit_kerja;

	   		$data_table_array[$no]["main"][0]["keterangan"] = $str_unit_kerja;
	   		$data_table_array[$no]["main"][0]["value"] = $v_data_json->jumlah_total;

	   		// print_r($v_data_json);

	   		// by agregat golongan
	   		$data_table_array[$no]["sum_golongan"][0]["keterangan"] = "Golongan I (".$v_data_json->jumlah_golongan_I.")";
	   		$data_table_array[$no]["sum_golongan"][0]["value"] = $v_data_json->jumlah_golongan_I;

	   		$data_table_array[$no]["sum_golongan"][1]["keterangan"] = "Golongan II (".$v_data_json->jumlah_golongan_II.")";
	   		$data_table_array[$no]["sum_golongan"][1]["value"] = $v_data_json->jumlah_golongan_II;

	   		$data_table_array[$no]["sum_golongan"][2]["keterangan"] = "Golongan III (".$v_data_json->jumlah_golongan_III.")";
	   		$data_table_array[$no]["sum_golongan"][2]["value"] = $v_data_json->jumlah_golongan_III;

	   		$data_table_array[$no]["sum_golongan"][3]["keterangan"] = "Golongan IV (".$v_data_json->jumlah_golongan_IV.")";
	   		$data_table_array[$no]["sum_golongan"][3]["value"] = $v_data_json->jumlah_golongan_IV;

	   		// by agregat jumlah_golongan_I
	   		$data_table_array[$no]["jumlah_golongan_I"][0]["keterangan"] = "Golongan I-A (".$v_data_json->golongan_Ia.")";
	   		$data_table_array[$no]["jumlah_golongan_I"][0]["value"] = $v_data_json->golongan_Ia;

	   		$data_table_array[$no]["jumlah_golongan_I"][1]["keterangan"] = "Golongan I-B (".$v_data_json->golongan_Ib.")";
	   		$data_table_array[$no]["jumlah_golongan_I"][1]["value"] = $v_data_json->golongan_Ib;

	   		$data_table_array[$no]["jumlah_golongan_I"][2]["keterangan"] = "Golongan I-C (".$v_data_json->golongan_Ic.")";
	   		$data_table_array[$no]["jumlah_golongan_I"][2]["value"] = $v_data_json->golongan_Ic;

	   		$data_table_array[$no]["jumlah_golongan_I"][3]["keterangan"] = "Golongan I-D (".$v_data_json->golongan_Id.")";
	   		$data_table_array[$no]["jumlah_golongan_I"][3]["value"] = $v_data_json->golongan_Id;


	   		// by agregat jumlah_golongan_II
	   		$data_table_array[$no]["jumlah_golongan_II"][0]["keterangan"] = "Golongan II-A (".$v_data_json->golongan_IIa.")";
	   		$data_table_array[$no]["jumlah_golongan_II"][0]["value"] = $v_data_json->golongan_IIa;

	   		$data_table_array[$no]["jumlah_golongan_II"][1]["keterangan"] = "Golongan II-B (".$v_data_json->golongan_IIb.")";
	   		$data_table_array[$no]["jumlah_golongan_II"][1]["value"] = $v_data_json->golongan_IIb;

	   		$data_table_array[$no]["jumlah_golongan_II"][2]["keterangan"] = "Golongan II-C (".$v_data_json->golongan_IIc.")";
	   		$data_table_array[$no]["jumlah_golongan_II"][2]["value"] = $v_data_json->golongan_IIc;

	   		$data_table_array[$no]["jumlah_golongan_II"][3]["keterangan"] = "Golongan II-D (".$v_data_json->golongan_IId.")";
	   		$data_table_array[$no]["jumlah_golongan_II"][3]["value"] = $v_data_json->golongan_IId;


	   		// by agregat jumlah_golongan_III
	   		$data_table_array[$no]["jumlah_golongan_III"][0]["keterangan"] = "Golongan III-A (".$v_data_json->golongan_IIIa.")";
	   		$data_table_array[$no]["jumlah_golongan_III"][0]["value"] = $v_data_json->golongan_IIIa;

	   		$data_table_array[$no]["jumlah_golongan_III"][1]["keterangan"] = "Golongan III-B (".$v_data_json->golongan_IIIb.")";
	   		$data_table_array[$no]["jumlah_golongan_III"][1]["value"] = $v_data_json->golongan_IIIb;

	   		$data_table_array[$no]["jumlah_golongan_III"][2]["keterangan"] = "Golongan III-C (".$v_data_json->golongan_IIIc.")";
	   		$data_table_array[$no]["jumlah_golongan_III"][2]["value"] = $v_data_json->golongan_IIIc;

	   		$data_table_array[$no]["jumlah_golongan_III"][3]["keterangan"] = "Golongan III-D (".$v_data_json->golongan_IIId.")";
	   		$data_table_array[$no]["jumlah_golongan_III"][3]["value"] = $v_data_json->golongan_IIId;


	   		// by agregat jumlah_golongan_IV
	   		$data_table_array[$no]["jumlah_golongan_IV"][0]["keterangan"] = "Golongan IV-A (".$v_data_json->golongan_IVa.")";
	   		$data_table_array[$no]["jumlah_golongan_IV"][0]["value"] = $v_data_json->golongan_IVa;

	   		$data_table_array[$no]["jumlah_golongan_IV"][1]["keterangan"] = "Golongan IV-B (".$v_data_json->golongan_IVb.")";
	   		$data_table_array[$no]["jumlah_golongan_IV"][1]["value"] = $v_data_json->golongan_IVb;

	   		$data_table_array[$no]["jumlah_golongan_IV"][2]["keterangan"] = "Golongan IV-C (".$v_data_json->golongan_IVc.")";
	   		$data_table_array[$no]["jumlah_golongan_IV"][2]["value"] = $v_data_json->golongan_IVc;

	   		$data_table_array[$no]["jumlah_golongan_IV"][3]["keterangan"] = "Golongan IV-D (".$v_data_json->golongan_IVd.")";
	   		$data_table_array[$no]["jumlah_golongan_IV"][3]["value"] = $v_data_json->golongan_IVd;


	   		// by agregat Gender
	   		$data_table_array[$no]["gender"][0]["keterangan"] = "Laki-Laki (".$v_data_json->laki.")";
	   		$data_table_array[$no]["gender"][0]["value"] = $v_data_json->laki;
	   		$data_table_array[$no]["gender"][1]["keterangan"] = "Perempuan (".$v_data_json->perempuan.")";
	   		$data_table_array[$no]["gender"][1]["value"] = $v_data_json->perempuan;


	   		// by agregat agama
	   		$data_table_array[$no]["agama"][0]["keterangan"] = "Agama Islam (".$v_data_json->agama_islam.")";
	   		$data_table_array[$no]["agama"][0]["value"] = $v_data_json->agama_islam;

	   		$data_table_array[$no]["agama"][1]["keterangan"] = "Agama Kristen (".$v_data_json->agama_kristen.")";
	   		$data_table_array[$no]["agama"][1]["value"] = $v_data_json->agama_kristen;

	   		$data_table_array[$no]["agama"][2]["keterangan"] = "Agama Katholik (".$v_data_json->agama_katholik.")";
	   		$data_table_array[$no]["agama"][2]["value"] = $v_data_json->agama_katholik;

	   		$data_table_array[$no]["agama"][3]["keterangan"] = "Agama Hindu (".$v_data_json->agama_hindu.")";
	   		$data_table_array[$no]["agama"][3]["value"] = $v_data_json->agama_hindu;

	   		$data_table_array[$no]["agama"][4]["keterangan"] = "Agama Budha (".$v_data_json->agama_budha.")";
	   		$data_table_array[$no]["agama"][4]["value"] = $v_data_json->agama_budha;


	   		// by agregat status_kawin
	   		$data_table_array[$no]["status_kawin"][0]["keterangan"] = "Kawin (".$v_data_json->status_kawin.")";
	   		$data_table_array[$no]["status_kawin"][0]["value"] = $v_data_json->status_kawin;

	   		$data_table_array[$no]["status_kawin"][1]["keterangan"] = "Belum Kawin (".$v_data_json->status_belum_kawin.")";
	   		$data_table_array[$no]["status_kawin"][1]["value"] = $v_data_json->status_belum_kawin;

	   		$str_select .= "<option value=\"".$no."\">".$str_unit_kerja."</option>";


	   		$no++;	   		
	   	}

	   	$str_select .= "</select>";
	   	// $data["data_json"] 			= json_encode($data_array);
	   	$data["data_json"] = json_encode($data_table_array);
	   	$data["str_select"] = $str_select;

	   	


	   	// print_r("<pre>");
	   	// print_r($data_table_array);
	   	$this->load->view("bkd/pegawai/bpkad_detail", $data);
	}
}


