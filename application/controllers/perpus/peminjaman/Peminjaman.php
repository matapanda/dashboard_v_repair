<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('perpus/peminjaman/peminjaman_model', 'peminjaman_db');

        $this->title = 'Statistik Peminjaman';
        $this->submodule  = 'Statistik Peminjaman';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->peminjaman_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'perpus/peminjaman'));
    }

    public function get($idstatistikpeminjaman = null, $limit = null, $page = null)
    {
        if(!$idstatistikpeminjaman){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->peminjaman_db->get($limit, $page);
            }else{
                $data = $this->peminjaman_db->get();
            }
        }else{
            $this->peminjaman_db->idstatistikpeminjaman = $idstatistikpeminjaman;
            $data = $this->peminjaman_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Statistik Peminjaman', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Statistik Peminjaman', $data);
    }

    public function get_d()
    {
        $list = $this->peminjaman_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->peminjaman_db->count_all(),
            "recordsFiltered" => $this->peminjaman_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);

    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelas', 'Kelas', 'required');
        $this->form_validation->set_rules('idjeniskoleksi', 'Jenis Koleksi', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, form_error(), FALSE);
        }

        $this->peminjaman_db->idstatistikpeminjaman = $this->generate_id->get_id();
        $this->peminjaman_db->tanggal = $this->input->post('tanggal');
        $this->peminjaman_db->idkelas = $this->input->post('idkelas');
        $this->peminjaman_db->idjeniskoleksi = $this->input->post('idjeniskoleksi');
        $this->peminjaman_db->jumlah = $this->input->post('jumlah');
        $this->peminjaman_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->peminjaman_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Statistik Peminjaman', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Statistik Peminjaman', $data);
    }

    public function update($idstatistikpeminjaman){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelas', 'Kelas', 'required');
        $this->form_validation->set_rules('idjeniskoleksi', 'Jenis Koleksi', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, form_error(), FALSE);
        }

        $this->peminjaman_db->idstatistikpeminjaman = $idstatistikpeminjaman;
        $this->peminjaman_db->tanggal = $this->input->post('tanggal');
        $this->peminjaman_db->idkelas = $this->input->post('idkelas');
        $this->peminjaman_db->idjeniskoleksi = $this->input->post('idjeniskoleksi');
        $this->peminjaman_db->jumlah = $this->input->post('jumlah');
        $this->peminjaman_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->peminjaman_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Statistik Peminjaman', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Statistik Peminjaman', $data);
    }

    public function delete($idstatistikpeminjaman){

        $this->auth->check_write($this->submodule);

        $this->peminjaman_db->idstatistikpeminjaman = $idstatistikpeminjaman;
        $data = $this->peminjaman_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Statistik Peminjaman', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Statistik Peminjaman', $data);
    }
}
