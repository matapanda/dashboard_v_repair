<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Koleksi extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('perpus/koleksi/koleksi_model', 'koleksi_db');

        $this->title = 'Rekapitulasi Koleksi';
        $this->submodule  = 'Rekapitulasi Koleksi';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->koleksi_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'perpus/koleksi'));
    }

    public function get($idkoleksi = null, $limit = null, $page = null)
    {
        if(!$idkoleksi){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->koleksi_db->get($limit, $page);
            }else{
                $data = $this->koleksi_db->get();
            }
        }else{
            $this->koleksi_db->idkoleksi = $idkoleksi;
            $data = $this->koleksi_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Rekapitulasi Koleksi', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Rekapitulasi Koleksi', $data);
    }

    public function get_d()
    {
        $list = $this->koleksi_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->koleksi_db->count_all(),
            "recordsFiltered" => $this->koleksi_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idbahasa', 'Bahasa', 'required');
        $this->form_validation->set_rules('idjeniskoleksi', 'Jenis Koleksi', 'required');
        $this->form_validation->set_rules('idkelas', 'Kelas', 'required');
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('eksemplar', 'Eksemplar', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, form_error(), FALSE);
        }

        $this->koleksi_db->idkoleksi = $this->generate_id->get_id();
        $this->koleksi_db->idbahasa = $this->input->post('idbahasa');
        $this->koleksi_db->idjeniskoleksi = $this->input->post('idjeniskoleksi');
        $this->koleksi_db->idkelas = $this->input->post('idkelas');
        $this->koleksi_db->judul = $this->input->post('judul');
        $this->koleksi_db->eksemplar = $this->input->post('eksemplar');
        $this->koleksi_db->tanggal = $this->input->post('tanggal');
        $this->koleksi_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->koleksi_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Rekapitulasi Koleksi', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Rekapitulasi Koleksi', $data);
    }

    public function update($idkoleksi){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idbahasa', 'Bahasa', 'required');
        $this->form_validation->set_rules('idjeniskoleksi', 'Jenis Koleksi', 'required');
        $this->form_validation->set_rules('idkelas', 'Kelas', 'required');
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('eksemplar', 'Eksemplar', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, form_error(), FALSE);
        }

        $this->koleksi_db->idkoleksi = $idkoleksi;
        $this->koleksi_db->idbahasa = $this->input->post('idbahasa');
        $this->koleksi_db->idjeniskoleksi = $this->input->post('idjeniskoleksi');
        $this->koleksi_db->idkelas = $this->input->post('idkelas');
        $this->koleksi_db->judul = $this->input->post('judul');
        $this->koleksi_db->eksemplar = $this->input->post('eksemplar');
        $this->koleksi_db->tanggal = $this->input->post('tanggal');
        $this->koleksi_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->koleksi_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Rekapitulasi Koleksi', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Rekapitulasi Koleksi', $data);
    }

    public function delete($idkoleksi){

        $this->auth->check_write($this->submodule);

        $this->koleksi_db->idkoleksi = $idkoleksi;
        $data = $this->koleksi_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Rekapitulasi Koleksi', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Rekapitulasi Koleksi', $data);
    }
}
