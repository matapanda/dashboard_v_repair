<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bahasa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('perpus/master/bahasa_model', 'bahasa_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->bahasa_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data bahasa',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data bahasa',$data);
    }
}