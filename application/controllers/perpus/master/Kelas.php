<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('perpus/master/kelas_model', 'kelas_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->kelas_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kelas',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kelas',$data);
    }
}