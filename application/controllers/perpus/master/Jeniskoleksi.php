<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jeniskoleksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('perpus/master/jeniskoleksi_model', 'jeniskoleksi_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->jeniskoleksi_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data jenis koleksi',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data jenis koleksi',$data);
    }
}