<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datapkk extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dp3p2kb/datapkk/datapkk_model', 'datapkk_db');

        $this->title = 'Data PKK';
        $this->submodule  = 'Data PKK';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->datapkk_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dp3p2kb/datapkk'));
    }

    public function get($iddatapkk = null, $limit = null, $page = null)
    {
        if(!$iddatapkk){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->datapkk_db->get($limit, $page);
            }else{
                $data = $this->datapkk_db->get();
            }
        }else{
            $this->datapkk_db->iddatapkk = $iddatapkk;
            $data = $this->datapkk_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data PKK', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data PKK', $data);
    }

    public function get_d()
    {
        $list = $this->datapkk_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->datapkk_db->count_all(),
            "recordsFiltered" => $this->datapkk_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('jumlahkelompok_pkkrw', 'Jumlah Kelompok PKK RW', 'required');
        $this->form_validation->set_rules('jumlahkelompok_pkkrt', 'Jumlah Kelompok PKK RT', 'required');
        $this->form_validation->set_rules('jumlahkelompok_dasawisma', 'Jumlah Kelompok Dasawisma', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Kelurahan, Tanggal, Jumlah Kelompok PKK RW, Jumlah Kelompok PKK RT, Jumlah Kelompok Dasawisma, dan Keterangan diperlukan', FALSE);
        }

        $this->datapkk_db->iddatapkk = $this->generate_id->get_id();
        $this->datapkk_db->idkecamatan = $this->input->post('idkecamatan');
        $this->datapkk_db->tanggal = $this->input->post('tanggal');
        $this->datapkk_db->kelurahan = $this->input->post('kelurahan');
        $this->datapkk_db->jumlahkelompok_pkkrw = $this->input->post('jumlahkelompok_pkkrw');
        $this->datapkk_db->jumlahkelompok_pkkrt = $this->input->post('jumlahkelompok_pkkrt');
        $this->datapkk_db->jumlahkelompok_dasawisma = $this->input->post('jumlahkelompok_dasawisma');
        $this->datapkk_db->keterangan = $this->input->post('keterangan');
        $this->datapkk_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->datapkk_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data PKK', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data PKK', $data);
    }

    public function update($iddatapkk){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('jumlahkelompok_pkkrw', 'Jumlah Kelompok PKK RW', 'required');
        $this->form_validation->set_rules('jumlahkelompok_pkkrt', 'Jumlah Kelompok PKK RT', 'required');
        $this->form_validation->set_rules('jumlahkelompok_dasawisma', 'Jumlah Kelompok Dasawisma', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Kelurahan, Tanggal, Jumlah Kelompok PKK RW, Jumlah Kelompok PKK RT, Jumlah Kelompok Dasawisma, dan Keterangan diperlukan', FALSE);
        }

        $this->datapkk_db->iddatapkk = $iddatapkk;
        $this->datapkk_db->idkecamatan = $this->input->post('idkecamatan');
        $this->datapkk_db->tanggal = $this->input->post('tanggal');
        $this->datapkk_db->kelurahan = $this->input->post('kelurahan');
        $this->datapkk_db->jumlahkelompok_pkkrw = $this->input->post('jumlahkelompok_pkkrw');
        $this->datapkk_db->jumlahkelompok_pkkrt = $this->input->post('jumlahkelompok_pkkrt');
        $this->datapkk_db->jumlahkelompok_dasawisma = $this->input->post('jumlahkelompok_dasawisma');
        $this->datapkk_db->keterangan = $this->input->post('keterangan');
        $this->datapkk_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->datapkk_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data PKK', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data PKK', $data);
    }

    public function delete($iddatapkk){

        $this->auth->check_write($this->submodule);

        $this->datapkk_db->iddatapkk = $iddatapkk;
        $data = $this->datapkk_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data PKK', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data PKK', $data);
    }
}
