<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hpk extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dp3p2kb/hpk/hpk_model', 'hpk_db');

        $this->title = 'Hasil Pendataan Keluarga';
        $this->submodule  = 'Hasil Pendapatan Keluarga';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->hpk_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dp3p2kb/hpk'));
    }

    public function get($idhasilpendapatankeluarga = null, $limit = null, $page = null)
    {
        if(!$idhasilpendapatankeluarga){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->hpk_db->get($limit, $page);
            }else{
                $data = $this->hpk_db->get();
            }
        }else{
            $this->hpk_db->idhasilpendapatankeluarga = $idhasilpendapatankeluarga;
            $data = $this->hpk_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Hasil Pendapatan Keluarga', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Hasil Pendapatan Keluarga', $data);
    }

    public function get_d()
    {
        $list = $this->hpk_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->hpk_db->count_all(),
            "recordsFiltered" => $this->hpk_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'kecamatan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('desa_kelurahan_ada', 'Desa kecamatan Ada', 'required');
        $this->form_validation->set_rules('desa_kelurahan_didata', 'Desa kecamatan Didata', 'required');
        $this->form_validation->set_rules('dusun_rw_ada', 'Dusun RW Ada', 'required');
        $this->form_validation->set_rules('dusun_rw_didata', 'Dusun RW Didata', 'required');
        $this->form_validation->set_rules('rt_ada', 'RT Ada', 'required');
        $this->form_validation->set_rules('rt_didata', 'RT Didata', 'required');
        $this->form_validation->set_rules('jumlahkepalakeluarga_didata', 'Jumlah Keluarga Didata', 'required');
        $this->form_validation->set_rules('bukanpesertakb_hamil', 'Bukan Peserta KB Hamil', 'required');
        $this->form_validation->set_rules('bukanpesertakb_inginanaksegera', 'Bukan Peserta KB Ingin Anak Segera', 'required');
        $this->form_validation->set_rules('bukanpesertakb_inginanakditunda', 'Bukan Peserta KB Ingin Anak Ditunda', 'required');
        $this->form_validation->set_rules('bukanpesertakb_tidakinginanaklagi', 'Bukan Peserta KB Tidak Ingin Anak Lagi', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_bkp', 'Kesertaan Dalam POKTAN BKP', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_bkr', 'Kesertaan Dalam POKTAN BKR', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_bkl', 'Kesertaan Dalam POKTAN BKL', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_lppks', 'Kesertaan Dalam POKTAN LPPKS', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_pik_rm', 'Kesertaan Dalam POKTAN PIK RM', 'required');
        $this->form_validation->set_rules('tahapankeluargasejahtera_prasejahtera', 'Tahapan Keluarga Pra Sejahtera', 'required');
        $this->form_validation->set_rules('tahapankeluargasejahtera_sejahtera1', 'Tahapan Keluarga Sejahtera 1', 'required');
        $this->form_validation->set_rules('tahapankeluargasejahtera_sejahtera2', 'Tahapan Keluarga Sejahtera 2', 'required');


        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan', FALSE);
        }

        $this->hpk_db->idhasilpendapatankeluarga = $this->generate_id->get_id();
        $this->hpk_db->idkecamatan = $this->input->post('idkecamatan');
        $this->hpk_db->tanggal = $this->input->post('tanggal');
        $this->hpk_db->desa_kelurahan_ada = $this->input->post('desa_kelurahan_ada');
        $this->hpk_db->desa_kelurahan_didata = $this->input->post('desa_kelurahan_didata');
        $this->hpk_db->dusun_rw_ada = $this->input->post('dusun_rw_ada');
        $this->hpk_db->dusun_rw_didata = $this->input->post('dusun_rw_didata');
        $this->hpk_db->rt_ada = $this->input->post('rt_ada');
        $this->hpk_db->rt_didata = $this->input->post('rt_didata');
        $this->hpk_db->jumlahkepalakeluarga_didata = $this->input->post('jumlahkepalakeluarga_didata');
        $this->hpk_db->bukanpesertakb_hamil = $this->input->post('bukanpesertakb_hamil');
        $this->hpk_db->bukanpesertakb_inginanaksegera = $this->input->post('bukanpesertakb_inginanaksegera');
        $this->hpk_db->bukanpesertakb_inginanakditunda = $this->input->post('bukanpesertakb_inginanakditunda');
        $this->hpk_db->bukanpesertakb_tidakinginanaklagi = $this->input->post('bukanpesertakb_tidakinginanaklagi');
        $this->hpk_db->kesertaandalampoktan_bkp = $this->input->post('kesertaandalampoktan_bkp');
        $this->hpk_db->kesertaandalampoktan_bkr = $this->input->post('kesertaandalampoktan_bkr');
        $this->hpk_db->kesertaandalampoktan_bkl = $this->input->post('kesertaandalampoktan_bkl');
        $this->hpk_db->kesertaandalampoktan_lppks = $this->input->post('kesertaandalampoktan_lppks');
        $this->hpk_db->kesertaandalampoktan_pik_rm = $this->input->post('kesertaandalampoktan_pik_rm');
        $this->hpk_db->tahapankeluargasejahtera_prasejahtera = $this->input->post('tahapankeluargasejahtera_prasejahtera');
        $this->hpk_db->tahapankeluargasejahtera_sejahtera1 = $this->input->post('tahapankeluargasejahtera_sejahtera1');
        $this->hpk_db->tahapankeluargasejahtera_sejahtera2 = $this->input->post('tahapankeluargasejahtera_sejahtera2');
        $this->hpk_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->hpk_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Hasil Pendapatan Keluarga', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Hasil Pendapatan Keluarga', $data);
    }

    public function update($idhasilpendapatankeluarga){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'kecamatan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('desa_kelurahan_ada', 'Desa kecamatan Ada', 'required');
        $this->form_validation->set_rules('desa_kelurahan_didata', 'Desa kecamatan Didata', 'required');
        $this->form_validation->set_rules('dusun_rw_ada', 'Dusun RW Ada', 'required');
        $this->form_validation->set_rules('dusun_rw_didata', 'Dusun RW Didata', 'required');
        $this->form_validation->set_rules('rt_ada', 'RT Ada', 'required');
        $this->form_validation->set_rules('rt_didata', 'RT Didata', 'required');
        $this->form_validation->set_rules('jumlahkepalakeluarga_didata', 'Jumlah Keluarga Didata', 'required');
        $this->form_validation->set_rules('bukanpesertakb_hamil', 'Bukan Peserta KB Hamil', 'required');
        $this->form_validation->set_rules('bukanpesertakb_inginanaksegera', 'Bukan Peserta KB Ingin Anak Segera', 'required');
        $this->form_validation->set_rules('bukanpesertakb_inginanakditunda', 'Bukan Peserta KB Ingin Anak Ditunda', 'required');
        $this->form_validation->set_rules('bukanpesertakb_tidakinginanaklagi', 'Bukan Peserta KB Tidak Ingin Anak Lagi', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_bkp', 'Kesertaan Dalam POKTAN BKP', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_bkr', 'Kesertaan Dalam POKTAN BKR', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_bkl', 'Kesertaan Dalam POKTAN BKL', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_lppks', 'Kesertaan Dalam POKTAN LPPKS', 'required');
        $this->form_validation->set_rules('kesertaandalampoktan_pik_rm', 'Kesertaan Dalam POKTAN PIK RM', 'required');
        $this->form_validation->set_rules('tahapankeluargasejahtera_prasejahtera', 'Tahapan Keluarga Pra Sejahtera', 'required');
        $this->form_validation->set_rules('tahapankeluargasejahtera_sejahtera1', 'Tahapan Keluarga Sejahtera 1', 'required');
        $this->form_validation->set_rules('tahapankeluargasejahtera_sejahtera2', 'Tahapan Keluarga Sejahtera 2', 'required');


        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Semua field dibutuhkan', FALSE);
        }

        $this->hpk_db->idhasilpendapatankeluarga = $idhasilpendapatankeluarga;
        $this->hpk_db->idkecamatan = $this->input->post('idkecamatan');
        $this->hpk_db->tanggal = $this->input->post('tanggal');
        $this->hpk_db->desa_kelurahan_ada = $this->input->post('desa_kelurahan_ada');
        $this->hpk_db->desa_kelurahan_didata = $this->input->post('desa_kelurahan_didata');
        $this->hpk_db->dusun_rw_ada = $this->input->post('dusun_rw_ada');
        $this->hpk_db->dusun_rw_didata = $this->input->post('dusun_rw_didata');
        $this->hpk_db->rt_ada = $this->input->post('rt_ada');
        $this->hpk_db->rt_didata = $this->input->post('rt_didata');
        $this->hpk_db->jumlahkepalakeluarga_didata = $this->input->post('jumlahkepalakeluarga_didata');
        $this->hpk_db->bukanpesertakb_hamil = $this->input->post('bukanpesertakb_hamil');
        $this->hpk_db->bukanpesertakb_inginanaksegera = $this->input->post('bukanpesertakb_inginanaksegera');
        $this->hpk_db->bukanpesertakb_inginanakditunda = $this->input->post('bukanpesertakb_inginanakditunda');
        $this->hpk_db->bukanpesertakb_tidakinginanaklagi = $this->input->post('bukanpesertakb_tidakinginanaklagi');
        $this->hpk_db->kesertaandalampoktan_bkp = $this->input->post('kesertaandalampoktan_bkp');
        $this->hpk_db->kesertaandalampoktan_bkr = $this->input->post('kesertaandalampoktan_bkr');
        $this->hpk_db->kesertaandalampoktan_bkl = $this->input->post('kesertaandalampoktan_bkl');
        $this->hpk_db->kesertaandalampoktan_lppks = $this->input->post('kesertaandalampoktan_lppks');
        $this->hpk_db->kesertaandalampoktan_pik_rm = $this->input->post('kesertaandalampoktan_pik_rm');
        $this->hpk_db->tahapankeluargasejahtera_prasejahtera = $this->input->post('tahapankeluargasejahtera_prasejahtera');
        $this->hpk_db->tahapankeluargasejahtera_sejahtera1 = $this->input->post('tahapankeluargasejahtera_sejahtera1');
        $this->hpk_db->tahapankeluargasejahtera_sejahtera2 = $this->input->post('tahapankeluargasejahtera_sejahtera2');
        $this->hpk_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->hpk_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Hasil Pendapatan Keluarga', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Hasil Pendapatan Keluarga', $data);
    }

    public function delete($idhasilpendapatankeluarga){

        $this->auth->check_write($this->submodule);

        $this->hpk_db->idhasilpendapatankeluarga = $idhasilpendapatankeluarga;
        $data = $this->hpk_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Hasil Pendapatan Keluarga', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Hasil Pendapatan Keluarga', $data);
    }
}
