<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jeniskasus extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('dp3p2kb/kdrt/jeniskasus_model', 'jeniskasus_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->jeniskasus_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data jenis kasus KDRT',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data jenis kasus KDRT',$data);
    }
}