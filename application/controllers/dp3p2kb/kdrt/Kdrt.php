<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kdrt extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dp3p2kb/kdrt/kdrt_model', 'kdrt_db');

        $this->title = 'KDRT';
        $this->submodule  = 'KDRT';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->kdrt_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dp3p2kb/kdrt'));
    }

    public function get($idkdrt = null, $limit = null, $page = null)
    {
        if(!$idkdrt){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->kdrt_db->get($limit, $page);
            }else{
                $data = $this->kdrt_db->get();
            }
        }else{
            $this->kdrt_db->idkdrt = $idkdrt;
            $data = $this->kdrt_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KDRT', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KDRT', $data);
    }

    public function get_d()
    {
        $list = $this->kdrt_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->kdrt_db->count_all(),
            "recordsFiltered" => $this->kdrt_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah_korban', 'Jumlah Korban', 'required');
        $this->form_validation->set_rules('idkecamatan', 'kecamatan', 'required');
        $this->form_validation->set_rules('idjeniskasus', 'Jenis Kasus', 'required');
        $this->form_validation->set_rules('idjeniskorban', 'Jenis Korban', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Jumlah Korban, kecamatan, Jenis Kasus, dan Jenis Korban diperlukan', FALSE);
        }

        $this->kdrt_db->idkdrt = $this->generate_id->get_id();
        $this->kdrt_db->tanggal = $this->input->post('tanggal');
        $this->kdrt_db->jumlah_korban = $this->input->post('jumlah_korban');
        $this->kdrt_db->idkecamatan = $this->input->post('idkecamatan');
        $this->kdrt_db->idjeniskasus = $this->input->post('idjeniskasus');
        $this->kdrt_db->idjeniskorban = $this->input->post('idjeniskorban');
        $this->kdrt_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->kdrt_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data KDRT', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data KDRT', $data);
    }

    public function update($idkdrt){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah_korban', 'Jumlah Korban', 'required');
        $this->form_validation->set_rules('idkecamatan', 'kecamatan', 'required');
        $this->form_validation->set_rules('idjeniskasus', 'Jenis Kasus', 'required');
        $this->form_validation->set_rules('idjeniskorban', 'Jenis Korban', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Jumlah Korban, kecamatan, Jenis Kasus, dan Jenis Korban diperlukan', FALSE);
        }

        $this->kdrt_db->idkdrt = $idkdrt;
        $this->kdrt_db->tanggal = $this->input->post('tanggal');
        $this->kdrt_db->jumlah_korban = $this->input->post('jumlah_korban');
        $this->kdrt_db->idkecamatan = $this->input->post('idkecamatan');
        $this->kdrt_db->idjeniskasus = $this->input->post('idjeniskasus');
        $this->kdrt_db->idjeniskorban = $this->input->post('idjeniskorban');
        $this->kdrt_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->kdrt_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data KDRT', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data KDRT', $data);
    }

    public function delete($idkdrt){

        $this->auth->check_write($this->submodule);

        $this->kdrt_db->idkdrt = $idkdrt;
        $data = $this->kdrt_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data KDRT', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data KDRT', $data);
    }
}