<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jeniskorban extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
//        $this->load->library('auth');
        $this->load->model('dp3p2kb/kdrt/jeniskorban_model', 'jeniskorban_db');
    }

    public function get()
    {
        /**
         * Cek can_read
         */
        $data = $this->jeniskorban_db->get();
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data jenis korban KDRT',$data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data jenis korban KDRT',$data);
    }
}