<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hpkt extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dp3p2kb/hpkt/hpkt_model', 'hpkt_db');

        $this->title = 'Hasil Pendataaan Keluarga Tahu';
        $this->submodule  = 'Hasil Pendapatan Keluarga Tahu';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->hpkt_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dp3p2kb/hpkt'));
    }

    public function get($id_hasilpendapatankeluargatahu = null, $limit = null, $page = null)
    {
        if(!$id_hasilpendapatankeluargatahu){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->hpkt_db->get($limit, $page);
            }else{
                $data = $this->hpkt_db->get();
            }
        }else{
            $this->hpkt_db->id_hasilpendapatankeluargatahu = $id_hasilpendapatankeluargatahu;
            $data = $this->hpkt_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Hasil Pendapatan Keluarga Tahu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Hasil Pendapatan Keluarga Tahu', $data);
    }

    public function get_d()
    {
        $list = $this->hpkt_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->hpkt_db->count_all(),
            "recordsFiltered" => $this->hpkt_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'kecamatan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlahkeluarga', 'Jumlah Keluarga', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_balita', 'Jumlah Anggota Keluarga Balita', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_anak', 'Jumlah Anggota Keluarga Anak', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_remaja', 'Jumlah Anggota Keluarga Remaja', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_dewasa', 'Jumlah Anggota Keluarga Dewasa', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_lansia', 'Jumlah Anggota Keluarga Lansia', 'required');
        $this->form_validation->set_rules('status_pus', 'Status PUS', 'required');
        $this->form_validation->set_rules('statuspusmupar', 'Status PUSMUPAR', 'required');
        $this->form_validation->set_rules('pasangansubur_mow', 'Pasangan Subur MOW', 'required');
        $this->form_validation->set_rules('pasangansubur_mop', 'Pasangan Subur MOP', 'required');
        $this->form_validation->set_rules('pasangansubur_iud', 'Pasangan Subur IUD', 'required');
        $this->form_validation->set_rules('pasangansubur_implan', 'Pasangan Subur Implan', 'required');
        $this->form_validation->set_rules('pasangansubur_suntik', 'Pasangan Subur Suntik', 'required');
        $this->form_validation->set_rules('pasangansubur_pil', 'Pasangan Subur Pil', 'required');
        $this->form_validation->set_rules('pasangansubur_kondom', 'Pasangan Subur Kondom', 'required');
        $this->form_validation->set_rules('pasangansubur_tradisional', 'Pasangan Subur Tradisional', 'required');


        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, '
            kecamatan, Tanggal, Jumlah Keluarga, Jumlah Anggota Keluarga Balita, 
            Jumlah Anggota Keluarga Anak, Jumlah Anggota Keluarga Remaja, Jumlah Anggota Keluarga Dewasa, 
            Jumlah Anggota Keluarga Lansia, Status PUS, Status PUSMUPAR, Pasangan Subur MOW, Pasangan Subur MOP, 
            Pasangan Subur IUD, Pasangan Subur Implan, Pasangan Subur Suntik, Pasangan Subur Pil, 
            Pasangan Subur Kondom, dan Pasangan Subur Tradisional dibutuhkan.', FALSE);
        }

        $this->hpkt_db->id_hasilpendapatankeluargatahu = $this->generate_id->get_id();
        $this->hpkt_db->idkecamatan = $this->input->post('idkecamatan');
        $this->hpkt_db->tanggal = $this->input->post('tanggal');
        $this->hpkt_db->jumlahkeluarga = $this->input->post('jumlahkeluarga');
        $this->hpkt_db->jumlahanggotakeluarga_balita = $this->input->post('jumlahanggotakeluarga_balita');
        $this->hpkt_db->jumlahanggotakeluarga_anak = $this->input->post('jumlahanggotakeluarga_anak');
        $this->hpkt_db->jumlahanggotakeluarga_remaja = $this->input->post('jumlahanggotakeluarga_remaja');
        $this->hpkt_db->jumlahanggotakeluarga_dewasa = $this->input->post('jumlahanggotakeluarga_dewasa');
        $this->hpkt_db->jumlahanggotakeluarga_lansia = $this->input->post('jumlahanggotakeluarga_lansia');
        $this->hpkt_db->status_pus = $this->input->post('status_pus');
        $this->hpkt_db->statuspusmupar = $this->input->post('statuspusmupar');
        $this->hpkt_db->pasangansubur_mow = $this->input->post('pasangansubur_mow');
        $this->hpkt_db->pasangansubur_mop = $this->input->post('pasangansubur_mop');
        $this->hpkt_db->pasangansubur_iud = $this->input->post('pasangansubur_iud');
        $this->hpkt_db->pasangansubur_implan = $this->input->post('pasangansubur_implan');
        $this->hpkt_db->pasangansubur_suntik = $this->input->post('pasangansubur_suntik');
        $this->hpkt_db->pasangansubur_pil = $this->input->post('pasangansubur_pil');
        $this->hpkt_db->pasangansubur_kondom = $this->input->post('pasangansubur_kondom');
        $this->hpkt_db->pasangansubur_tradisional = $this->input->post('pasangansubur_tradisional');
        $this->hpkt_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->hpkt_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Hasil Pendapatan Keluarga Tahu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Hasil Pendapatan Keluarga Tahu', $data);
    }

    public function update($id_hasilpendapatankeluargatahu){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('idkecamatan', 'kecamatan', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlahkeluarga', 'Jumlah Keluarga', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_balita', 'Jumlah Anggota Keluarga Balita', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_anak', 'Jumlah Anggota Keluarga Anak', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_remaja', 'Jumlah Anggota Keluarga Remaja', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_dewasa', 'Jumlah Anggota Keluarga Dewasa', 'required');
        $this->form_validation->set_rules('jumlahanggotakeluarga_lansia', 'Jumlah Anggota Keluarga Lansia', 'required');
        $this->form_validation->set_rules('status_pus', 'Status PUS', 'required');
        $this->form_validation->set_rules('statuspusmupar', 'Status PUSMUPAR', 'required');
        $this->form_validation->set_rules('pasangansubur_mow', 'Pasangan Subur MOW', 'required');
        $this->form_validation->set_rules('pasangansubur_mop', 'Pasangan Subur MOP', 'required');
        $this->form_validation->set_rules('pasangansubur_iud', 'Pasangan Subur IUD', 'required');
        $this->form_validation->set_rules('pasangansubur_implan', 'Pasangan Subur Implan', 'required');
        $this->form_validation->set_rules('pasangansubur_suntik', 'Pasangan Subur Suntik', 'required');
        $this->form_validation->set_rules('pasangansubur_pil', 'Pasangan Subur Pil', 'required');
        $this->form_validation->set_rules('pasangansubur_kondom', 'Pasangan Subur Kondom', 'required');
        $this->form_validation->set_rules('pasangansubur_tradisional', 'Pasangan Subur Tradisional', 'required');


        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, '
            kecamatan, Tanggal, Jumlah Keluarga, Jumlah Anggota Keluarga Balita, 
            Jumlah Anggota Keluarga Anak, Jumlah Anggota Keluarga Remaja, Jumlah Anggota Keluarga Dewasa, 
            Jumlah Anggota Keluarga Lansia, Status PUS, Status PUSMUPAR, Pasangan Subur MOW, Pasangan Subur MOP, 
            Pasangan Subur IUD, Pasangan Subur Implan, Pasangan Subur Suntik, Pasangan Subur Pil, 
            Pasangan Subur Kondom, dan Pasangan Subur Tradisional dibutuhkan.', FALSE);
        }

        $this->hpkt_db->id_hasilpendapatankeluargatahu = $id_hasilpendapatankeluargatahu;
        $this->hpkt_db->idkecamatan = $this->input->post('idkecamatan');
        $this->hpkt_db->tanggal = $this->input->post('tanggal');
        $this->hpkt_db->jumlahkeluarga = $this->input->post('jumlahkeluarga');
        $this->hpkt_db->jumlahanggotakeluarga_balita = $this->input->post('jumlahanggotakeluarga_balita');
        $this->hpkt_db->jumlahanggotakeluarga_anak = $this->input->post('jumlahanggotakeluarga_anak');
        $this->hpkt_db->jumlahanggotakeluarga_remaja = $this->input->post('jumlahanggotakeluarga_remaja');
        $this->hpkt_db->jumlahanggotakeluarga_dewasa = $this->input->post('jumlahanggotakeluarga_dewasa');
        $this->hpkt_db->jumlahanggotakeluarga_lansia = $this->input->post('jumlahanggotakeluarga_lansia');
        $this->hpkt_db->status_pus = $this->input->post('status_pus');
        $this->hpkt_db->statuspusmupar = $this->input->post('statuspusmupar');
        $this->hpkt_db->pasangansubur_mow = $this->input->post('pasangansubur_mow');
        $this->hpkt_db->pasangansubur_mop = $this->input->post('pasangansubur_mop');
        $this->hpkt_db->pasangansubur_iud = $this->input->post('pasangansubur_iud');
        $this->hpkt_db->pasangansubur_implan = $this->input->post('pasangansubur_implan');
        $this->hpkt_db->pasangansubur_suntik = $this->input->post('pasangansubur_suntik');
        $this->hpkt_db->pasangansubur_pil = $this->input->post('pasangansubur_pil');
        $this->hpkt_db->pasangansubur_kondom = $this->input->post('pasangansubur_kondom');
        $this->hpkt_db->pasangansubur_tradisional = $this->input->post('pasangansubur_tradisional');
        $this->hpkt_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->hpkt_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Hasil Pendapatan Keluarga Tahu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Hasil Pendapatan Keluarga Tahu', $data);
    }

    public function delete($id_hasilpendapatankeluargatahu){

        $this->auth->check_write($this->submodule);

        $this->hpkt_db->id_hasilpendapatankeluargatahu = $id_hasilpendapatankeluargatahu;
        $data = $this->hpkt_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Hasil Pendapatan Keluarga Tahu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Hasil Pendapatan Keluarga Tahu', $data);
    }
}
