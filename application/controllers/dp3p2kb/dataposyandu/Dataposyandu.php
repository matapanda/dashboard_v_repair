<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataPosyandu extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {

        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dp3p2kb/dataposyandu/dataposyandu_model', 'dataposyandu_db');

        $this->title = 'Data Posyandu';
        $this->submodule  = 'Data Posyandu';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->dataposyandu_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dp3p2kb/dataposyandu'));
    }

    public function get($iddataposyandu = null, $limit = null, $page = null)
    {
        if(!$iddataposyandu){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->dataposyandu_db->get($limit, $page);
            }else{
                $data = $this->dataposyandu_db->get();
            }
        }else{
            $this->dataposyandu_db->iddataposyandu = $iddataposyandu;
            $data = $this->dataposyandu_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data posyandu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data posyandu', $data);
    }

    public function get_d()
    {
        $list = $this->dataposyandu_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dataposyandu_db->count_all(),
            "recordsFiltered" => $this->dataposyandu_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlahposyandu', 'Jumlah Posyandu', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Id Kelurahan', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, jumlah posyandu, Kelurahan dibutuhkan', FALSE);
        }

        $this->dataposyandu_db->iddataposyandu = $this->generate_id->get_id();
        $this->dataposyandu_db->idkelurahan = $this->input->post('idkelurahan');
        $this->dataposyandu_db->tanggal = $this->input->post('tanggal');
        $this->dataposyandu_db->jumlahposyandu = $this->input->post('jumlahposyandu');

        $this->dataposyandu_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->dataposyandu_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data posyandu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data posyandu', $data);

    }

    public function update($iddataposyandu){
        $this->auth->check_write($this->submodule);
         $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
         $this->form_validation->set_rules('jumlahposyandu', 'Jumlah Posyandu', 'required');
         $this->form_validation->set_rules('idkelurahan', 'Id Kelurahan', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, jumlah posyandu, Kelurahan dibutuhkan', FALSE);
        }

        $this->dataposyandu_db->iddataposyandu = $iddataposyandu;
        $this->dataposyandu_db->idkelurahan = $this->input->post('idkelurahan');
        $this->dataposyandu_db->tanggal = $this->input->post('tanggal');
        $this->dataposyandu_db->jumlahposyandu = $this->input->post('jumlahposyandu');

        $this->dataposyandu_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->dataposyandu_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data posyandu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data posyandu', $data);
    }

    public function delete($iddataposyandu){
        $this->auth->check_write($this->submodule);

        $this->dataposyandu_db->iddataposyandu = $iddataposyandu;
        $data = $this->dataposyandu_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data posyandu', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data posyandu', $data);
    }
}
