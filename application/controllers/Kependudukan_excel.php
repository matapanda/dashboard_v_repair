<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kependudukan_excel extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // $this->load->library('auth');
        // $this->load->library('PHPExcel');
    }

    public function index(){
        include APPPATH.'third_party/SimpleXLSX.php';

        $path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx';
        print_r("<pre>");
        $this->get_lampid($path);
        
    }

    private function get_lampid($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows();

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 5 && $key <= 9){
                        $param = "main_awal";
                        $status = true;
                    }elseif ($key == 10) {
                        $param = "t_main_awal";
                        $status = true;
                    }elseif ($key >= 27 && $key <= 31) {
                        $param = "main_kurang";
                        $status = true;
                    }elseif ($key == 32) {
                        $param = "t_main_kurang";
                        $status = true;
                    }elseif ($key >= 49 && $key <= 53) {
                        $param = "main_akhir";
                        $status = true;
                    }elseif ($key == 54) {
                        $param = "t_main_akhir";
                        $status = true;
                    }

                    if($status){
                        $tmp_array[$param][$no]["kecamatan"] = $value[1];
                        
                        $tmp_array[$param][$no]["PENDUDUK_AWAL_BULAN_INI"]["l"] = $value[2];
                        $tmp_array[$param][$no]["PENDUDUK_AWAL_BULAN_INI"]["p"] = $value[3];
                        $tmp_array[$param][$no]["PENDUDUK_AWAL_BULAN_INI"]["lp"] = $value[4];

                        $tmp_array[$param][$no]["LAHIR_BULAN_INI"]["l"] = $value[5];
                        $tmp_array[$param][$no]["LAHIR_BULAN_INI"]["p"] = $value[6];
                        $tmp_array[$param][$no]["LAHIR_BULAN_INI"]["lp"] = $value[7];
                        
                        $tmp_array[$param][$no]["MATI_BULAN_INI"]["l"] = $value[8];
                        $tmp_array[$param][$no]["MATI_BULAN_INI"]["p"] = $value[9];
                        $tmp_array[$param][$no]["MATI_BULAN_INI"]["lp"] = $value[10];

                        $tmp_array[$param][$no]["PENDATANG_BULAN_INI"]["l"] = $value[11];
                        $tmp_array[$param][$no]["PENDATANG_BULAN_INI"]["p"] = $value[12];
                        $tmp_array[$param][$no]["PENDATANG_BULAN_INI"]["lp"] = $value[13];
                        
                        $tmp_array[$param][$no]["PINDAH_BULAN_INI"]["l"] = $value[14];
                        $tmp_array[$param][$no]["PINDAH_BULAN_INI"]["p"] = $value[15];
                        $tmp_array[$param][$no]["PINDAH_BULAN_INI"]["lp"] = $value[16];
                        
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["l"] = $value[17];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["p"] = $value[18];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["lp"] = $value[19];

                        $no++;
                    }
                }

                print_r($tmp_array);
            } else {
                echo SimpleXLSX::parseError();
            }
        }else{
            print_r("file_tidak tersedia");
        }
    }

    private function get_rekap_kelurahan($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(1);
                // print_r($main_data);

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 7 && $key <= 11){
                        $param = "rekap_kelurahan";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_rekap_kelurahan";
                        $status = true;
                    }

                    if($status){
                        $tmp_array[$param][$no]["KECAMATAN"] = $value[1];
                        
                        $tmp_array[$param][$no]["JUMLAH_KEL"] = $value[2];
                        $tmp_array[$param][$no]["JUMLAH_KK"] = $value[3];

                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["l"] = $value[4];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["p"] = $value[5];
                        $tmp_array[$param][$no]["PENDUDUK_AKHIR_BULAN_INI"]["lp"] = $value[6];

                        $no++;
                    }
                }

                print_r($tmp_array);
            } else {
                echo SimpleXLSX::parseError();
            }
        }else{
            print_r("file_tidak tersedia");
        }
    }

    private function get_agama($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){

        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(2);
                // print_r($main_data);
    
                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;
    
                    if($key >= 7 && $key <= 11){
                        $param = "agama";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_agama";
                        $status = true;
                    }
    
                    if($status){
                        $tmp_array[$param][$no]["KECAMATAN"] = $value[1];
                        $tmp_array[$param][$no]["ISLAM"] = $value[2];
                        $tmp_array[$param][$no]["KRISTEN"] = $value[3];
                        $tmp_array[$param][$no]["KATHOLIK"] = $value[4];
                        $tmp_array[$param][$no]["HINDU"] = $value[5];
                        $tmp_array[$param][$no]["BUDHA"] = $value[6];
                        $tmp_array[$param][$no]["KONGHUCHU"] = $value[7];
                        $tmp_array[$param][$no]["PENGHAYAT_KEPERCAYAAN"] = $value[8];
    
                        $no++;
                    }
                }
    
                print_r($tmp_array);
            } else {
                echo SimpleXLSX::parseError();
            }
        }else{
            print_r("file_tidak tersedia");
        }
    }

    private function get_rekam_ktp($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(4);
                // print_r($main_data);

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 7 && $key <= 11){
                        $param = "rekam_ktp";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_rekam_ktp";
                        $status = true;
                    }

                    if($status){
                        $tmp_array[$param][$no]["KECAMATAN"] = $value[1];
                        $tmp_array[$param][$no]["jumlah_penduduk"] = $value[2];

                        $tmp_array[$param][$no]["wajib_memiliki_KTP"]["l"] = $value[3];
                        $tmp_array[$param][$no]["wajib_memiliki_KTP"]["p"] = $value[4];
                        $tmp_array[$param][$no]["wajib_memiliki_KTP"]["lp"] = $value[5];
                        
                        $tmp_array[$param][$no]["sudah_rekam_KTP_el"] = $value[6];
                        $tmp_array[$param][$no]["belum_rekam_KTP_el"] = $value[7];
                        $tmp_array[$param][$no]["wajib_memiliki_akta_kelahiran"] = $value[8];
                        $tmp_array[$param][$no]["memiliki_akta_kelahiran"] = $value[9];

                        $no++;
                    }
                }

                print_r($tmp_array);
            } else {
                echo SimpleXLSX::parseError();
            }
        }else{
            print_r("file_tidak tersedia");
        }
    }

    private function get_kelompok_umur($path = 'assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx'){
        if(file_exists($path)){
            if ( $xlsx = SimpleXLSX::parse($path) ) {
                $main_data = $xlsx->rows(3);
                // print_r($main_data);

                $new_data_main = array();
                $tmp_array = array();
                $no = 0;
                foreach ($main_data as $key => $value) {
                    $param = "";
                    $status = false;

                    if($key >= 7 && $key <= 11){
                        $param = "kelompok_umur";
                        $status = true;
                    }elseif ($key == 12) {
                        $param = "t_kelompok_umur";
                        $status = true;
                    }elseif ($key == 5) {
                        $header_array = array(
                            "Kecamatan","0 - 4 TH","5 - 9 TH","10 - 14 TH","15 - 19 TH","20 - 24 TH",
                            "25 - 29 TH","30 - 34 TH","35 - 39 TH","40 - 44 TH","45 - 49 TH","50 - 54 TH",
                            "55 - 59 TH","60 - 64 TH","> 65 TH", "Jumlah Penduduk"
                        );
                    }

                    if($status){
                        $tmp_ok = array();
                        foreach ($value as $key_val => $value_val) {
                            if($key_val >= 1 && $key_val <= 16){
                                array_push($tmp_ok, $value_val);
                            }
                        }

                        $tmp_array[$param][$no] = $tmp_ok;

                        $no++;
                    }
                }

                $new_data_main["header"] = $header_array;
                $new_data_main["value"] = $tmp_array;

                print_r($new_data_main);
            } else {
                echo SimpleXLSX::parseError();
            }
        }else{
            print_r("file_tidak tersedia");
        }
    }

    public function index2(){
        include APPPATH.'third_party/SimpleXLSX.php';

        print_r("<pre>");
        

    }


    public function index3(){
        include APPPATH.'third_party/SimpleXLSX.php';

        print_r("<pre>");
        if(file_exists('assets/data_ex_dispenduk/2019/laporan_penduduk_januari_2019.xlsx')){
            
        }else{
            print_r("file_tidak tersedia");
        }

    }

    public function index4(){
        include APPPATH.'third_party/SimpleXLSX.php';

        print_r("<pre>");
        

    }


    public function index5(){
        include APPPATH.'third_party/SimpleXLSX.php';

        print_r("<pre>");
        

    }

}
