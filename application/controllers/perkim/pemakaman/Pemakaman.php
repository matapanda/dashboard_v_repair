<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemakaman extends CI_Controller
{

  private $title;
  private $submodule;
  private $user;

  public function __construct()
  {
    parent::__construct();

    $this->load->library('Generate_json');
    $this->load->library('Generate_id');
    $this->load->library('Generate_view');
    $this->load->library('auth');
    $this->load->library('form_validation');
    $this->load->model('perkim/pemakaman/pemakaman_model', 'pemakaman_db');

    $this->title = 'Pemakaman';
    $this->submodule  = 'Pemakaman';
    $this->user = $this->auth->get_user();

    $this->auth->check_read($this->submodule);
  }

  public function index()
  {
    $data['title'] = $this->title;
    $data['submodule'] = $this->submodule;
    $data['user'] = $this->user;
      $data['data_count'] = $this->pemakaman_db->count_all();

    $this->load->view('templates/template', $this->generate_view->get_index($data, 'perkim/pemakaman'));
  }

  public function get($idpemakaman = null, $limit = null, $page = null)
  {
    if(!$idpemakaman){
      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->pemakaman_db->get($limit, $page);
      }else{
        $data = $this->pemakaman_db->get();
      }
    }else{
      $this->pemakaman_db->idpemakaman = $idpemakaman;
      $data = $this->pemakaman_db->get();
    }
    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Pemakaman', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Pemakaman', $data);
  }

    public function get_d()
    {
        $list = $this->pemakaman_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pemakaman_db->count_all(),
            "recordsFiltered" => $this->pemakaman_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

  public function store(){

    $this->auth->check_write($this->submodule);

    $this->pemakaman_db->idpemakaman = $this->generate_id->get_id();
    $this->pemakaman_db->tanggal = $this->input->post('tahun');
    $this->pemakaman_db->namatpu = $this->input->post('namatpu');
    $this->pemakaman_db->luastpu = $this->input->post('luastpu');
    $this->pemakaman_db->luaslahanmakam = $this->input->post('luaslahanmakam');
    $this->pemakaman_db->jumlahseluruhmakam = $this->input->post('jumlahseluruhmakam');
    $this->pemakaman_db->fasum = $this->input->post('fasum');
    $this->pemakaman_db->sisalahanmakam = $this->input->post('sisalahanmakam');
    $this->pemakaman_db->keterangan = $this->input->post('keterangan');
    $this->pemakaman_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->pemakaman_db->add();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Pemakaman', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Pemakaman', $data);
  }

  public function update($idpemakaman){

    $this->auth->check_write($this->submodule);

    $this->pemakaman_db->idpemakaman = $idpemakaman;
    $this->pemakaman_db->tanggal = $this->input->post('tanggal');
    $this->pemakaman_db->namatpu = $this->input->post('namatpu');
    $this->pemakaman_db->luastpu = $this->input->post('luastpu');
    $this->pemakaman_db->luaslahanmakam = $this->input->post('luaslahanmakam');
    $this->pemakaman_db->jumlahseluruhmakam = $this->input->post('jumlahseluruhmakam');
    $this->pemakaman_db->fasum = $this->input->post('fasum');
    $this->pemakaman_db->sisalahanmakam = $this->input->post('sisalahanmakam');
    $this->pemakaman_db->keterangan = $this->input->post('keterangan');
    $this->pemakaman_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->pemakaman_db->update();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Pemakaman', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Pemakaman', $data);
  }

  public function delete($idpemakaman){

    $this->auth->check_write($this->submodule);

    $this->pemakaman_db->idpemakaman = $idpemakaman;
    $data = $this->pemakaman_db->delete();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Pemakaman', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Pemakaman', $data);
  }
}
