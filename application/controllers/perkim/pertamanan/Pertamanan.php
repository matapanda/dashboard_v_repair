<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pertamanan extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('perkim/pertamanan/pertamanan_model', 'pertamanan_db');

        $this->title = 'Pertamanan';
        $this->submodule = 'Pertamanan';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->pertamanan_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'perkim/pertamanan'));
    }

    public function get($idpertamanan = null, $limit = null, $page = null)
    {
        if (!$idpertamanan) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->pertamanan_db->get($limit, $page);
            } else {
                $data = $this->pertamanan_db->get();
            }
        } else {
            $this->pertamanan_db->idpertamanan = $idpertamanan;
            $data = $this->pertamanan_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil pertamanan', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil pertamanan', $data);
    }

    public function get_d()
    {
        $list = $this->pertamanan_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pertamanan_db->count_all(),
            "recordsFiltered" => $this->pertamanan_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {
        $this->auth->check_write($this->submodule);

        $this->pertamanan_db->idpertamanan = $this->generate_id->get_id();
        $this->pertamanan_db->tanggal = $this->input->post('tanggal');
        $this->pertamanan_db->langitude = $this->input->post('latitude');
        $this->pertamanan_db->longitude = $this->input->post('longitude');
        $this->pertamanan_db->nama_taman = $this->input->post('nama_taman');
        $this->pertamanan_db->deskripsi = $this->input->post('deskripsi');
        $this->pertamanan_db->idjenispertamanan = $this->input->post('idjenispertamanan');
        $this->pertamanan_db->idkelurahan = $this->input->post('idkelurahan');
        $this->pertamanan_db->urlgambarfasilitas = $this->input->post('urlgambarfasilitas');
        $this->pertamanan_db->idapp_user = $this->auth->get_user('idapp_user');

        //upload image
        $this->load->library('upload');
        $nmfile = $this->input->post('nama_taman')."-".time();
        $config['upload_path'] = 'uploads/perkim/pertamanan/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['file_name'] = $nmfile;

        $this->upload->initialize($config);
        // echo $config['upload_path'];
        // $this->output->enable_profiler(true);

        if ( ! $this->upload->do_upload('urlgambarfasilitas'))
        {
            return $this->generate_json->get_json(FALSE, 'Gagal menyimpan pertamanan', $this->upload->display_errors());
        }

        $this->pertamanan_db->urlgambarfasilitas = $config['upload_path'].$this->upload->data('file_name');

        $data = $this->pertamanan_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan pertamanan', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan pertamanan', $data);
    }

    public function update($idpertamanan)
    {

        $this->auth->check_write($this->submodule);

        $this->pertamanan_db->idpertamanan = $idpertamanan;
        $this->pertamanan_db->tanggal = $this->input->post('tanggal');
        $this->pertamanan_db->langitude = $this->input->post('latitude');
        $this->pertamanan_db->longitude = $this->input->post('longitude');
        $this->pertamanan_db->nama_taman = $this->input->post('nama_taman');
        $this->pertamanan_db->urlgambarfasilitas = $this->input->post('urlgambarfasilitas');
        $this->pertamanan_db->deskripsi = $this->input->post('deskripsi');
        $this->pertamanan_db->idjenispertamanan = $this->input->post('idjenispertamanan');
        $this->pertamanan_db->idkelurahan = $this->input->post('idkelurahan');
        $this->pertamanan_db->idapp_user = $this->auth->get_user('idapp_user');


        //upload image
        $this->load->library('upload');
        $nmfile = $this->input->post('nama_taman')."-".time();
        $config['upload_path'] = 'uploads/perkim/pertamanan/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['file_name'] = $nmfile;

        $this->upload->initialize($config);
        // $this->output->enable_profiler(true);

        if ( ! $this->upload->do_upload('urlgambarfasilitas'))
        {
            return $this->generate_json->get_json(FALSE, 'Gagal menyimpan pertamanan', $this->upload->display_errors());
        }

        $this->pertamanan_db->urlgambarfasilitas = $config['upload_path'].$this->upload->data('file_name');

        $data = $this->pertamanan_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi pertamanan', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi pertamanan', $data);
    }

    public function delete($idpertamanan)
    {

        $this->auth->check_write($this->submodule);

        $this->pertamanan_db->idpertamanan = $idpertamanan;
        $data = $this->pertamanan_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus pertamanan', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus pertamanan', $data);
    }

    public function upload_images()
    {

        $config['upload_path'] = './assets/iamges';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 232244;
        $config['max_width'] = 1366;
        $config['max_height'] = 768;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
            $this->output->enable_profiler(true);
        } else {
            $data = array('upload_data' => $this->upload->data());
            echo "success";
        }
    }
}
