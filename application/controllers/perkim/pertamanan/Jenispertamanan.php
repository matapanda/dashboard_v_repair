<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenispertamanan extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('perkim/pertamanan/jenispertamanan_model', 'jenispertamanan_db');

        $this->submodule  = 'Pertamanan';
        $this->user = $this->auth->get_user();

        // $this->auth->check_read($this->submodule);

    }

    public function get($idjenispertamanan = null, $limit = null, $page = null)
    {
        if(!$idjenispertamanan){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->jenispertamanan_db->get($limit, $page);
            }else{
                $data = $this->jenispertamanan_db->get();
            }
        }else{
            $this->jenispertamanan_db->idgelandanganpsikotik = $idjenispertamanan;
            $data = $this->jenispertamanan_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data jenis pertamanan', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data jenis pertamanan', $data);
    }

}
