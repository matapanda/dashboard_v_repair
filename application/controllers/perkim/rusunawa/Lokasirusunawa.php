<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LokasiRusunawa extends CI_Controller
{

  private $title;
  private $submodule;
  private $user;

  public function __construct()
  {
    parent::__construct();

    $this->load->library('Generate_json');
    $this->load->library('Generate_id');
    $this->load->library('Generate_view');
    $this->load->library('auth');
    $this->load->library('form_validation');
    $this->load->model('perkim/rusunawa/lokasirusunawa_model', 'lokasirusunawa_db');

    $this->title = 'Rusunawa';
    $this->submodule  = 'Rusunawa';
    $this->user = $this->auth->get_user();

    $this->auth->check_read($this->submodule);
  }

  public function get($idrusunawa = null, $limit = null, $page = null)
  {
    if(!$idrusunawa){
      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->lokasirusunawa_db->get($limit, $page);
      }else{
        $data = $this->lokasirusunawa_db->get();
      }
    }else{
      $this->lokasirusunawa_db->idpertamanan = $idrusunawa;
      $data = $this->lokasirusunawa_db->get();
    }
    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data lokasi Rusunawa', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil data lokasi Rusunawa', $data);
  }
}
