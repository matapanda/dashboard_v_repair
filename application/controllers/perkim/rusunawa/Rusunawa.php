<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rusunawa extends CI_Controller
{

  private $title;
  private $submodule;
  private $user;

  public function __construct()
  {
    parent::__construct();

    $this->load->library('Generate_json');
    $this->load->library('Generate_id');
    $this->load->library('Generate_view');
    $this->load->library('auth');
    $this->load->library('form_validation');
    $this->load->model('perkim/rusunawa/Rusunawa_model', 'rusunawa_db');

    $this->title = 'Rusunawa';
    $this->submodule  = 'Rusunawa';
    $this->user = $this->auth->get_user();

    $this->auth->check_read($this->submodule);
  }

  public function index()
  {
    $data['title'] = $this->title;
    $data['submodule'] = $this->submodule;
    $data['user'] = $this->user;
      $data['data_count'] = $this->rusunawa_db->count_all();

    $this->load->view('templates/template', $this->generate_view->get_index($data, 'perkim/rusunawa'));
  }

  public function get($idrusunawa = null, $limit = null, $page = null)
  {
    if(!$idrusunawa){
      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->rusunawa_db->get($limit, $page);
      }else{
        $data = $this->rusunawa_db->get();
      }
    }else{
      $this->rusunawa_db->idrusunawa = $idrusunawa;
      $data = $this->rusunawa_db->get();
    }
    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil Rusunawa', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil Rusunawa', $data);
  }

    public function get_d()
    {
        $list = $this->rusunawa_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->rusunawa_db->count_all(),
            "recordsFiltered" => $this->rusunawa_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

  public function store(){

    $this->auth->check_write($this->submodule);

    $this->rusunawa_db->idrusunawa = $this->generate_id->get_id();
    $this->rusunawa_db->tanggal = $this->input->post('tanggal');
    $this->rusunawa_db->jumlahhunian = $this->input->post('jumlahhunian');
    $this->rusunawa_db->jumlahpenghuni = $this->input->post('jumlahpenghuni');
    $this->rusunawa_db->keterangan = $this->input->post('keterangan');
    $this->rusunawa_db->idlokasi = $this->input->post('idlokasi');
    $this->rusunawa_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->rusunawa_db->add();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan Rusunawa', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menyimpan Rusunawa', $data);
  }

  public function update($idrusunawa){

    $this->auth->check_write($this->submodule);

    $this->rusunawa_db->idrusunawa = $idrusunawa;
    $this->rusunawa_db->tanggal = $this->input->post('tanggal');
    $this->rusunawa_db->jumlahhunian = $this->input->post('jumlahhunian');
    $this->rusunawa_db->jumlahpenghuni = $this->input->post('jumlahpenghuni');
    $this->rusunawa_db->keterangan = $this->input->post('keterangan');
    $this->rusunawa_db->idlokasi = $this->input->post('idlokasi');
    $this->rusunawa_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->rusunawa_db->update();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi Rusunawa', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi Rusunawa', $data);
  }

  public function delete($idrusunawa){

    $this->auth->check_write($this->submodule);

    $this->rusunawa_db->idrusunawa = $idrusunawa;
    $data = $this->rusunawa_db->delete();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menghapus Rusunawa', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menghapus Rusunawa', $data);
  }
}
