<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller
{

    public function index()
    {
        $this->load->library('auth');
        $data['title'] = 'Map';
        $data['submodule'] = 'map';
        $data['user'] = $this->auth->get_user();
        $data['based'] = $this->input->get('based');
        $data['script_plus'] = $this->load->view('map/script', $data, TRUE);

        // print_r($data);
        $this->load->view('map/index', $data);
    }

    public function data($idsubmodules){
      $this->load->library('Generate_json');
      $this->load->library('Routes_Integrations');

      $model_dir = $this->routes_integrations->get_by_id_submodule($idsubmodules);
      $this->load->model($model_dir, 'selected_db');

      parse_str($_SERVER['QUERY_STRING'], $url_data);
      // print_r($url_data);
      $select = !empty($url_data['selectlist']) ? $url_data['selectlist'] : null;
      $group = !empty($url_data['grouplist']) ? $url_data['grouplist'] : null;
      $where = !empty($url_data['wherelist']) ? $url_data['wherelist'] : null;
      $count = !empty($url_data['countlist']) ? $url_data['countlist'] : null;
      $join = !empty($url_data['joinlist']) ? $url_data['joinlist'] : null;
      // $extracted_data = $join;

      $extracted_data = $this->selected_db->get_extracted(
        $select,
        $group,
        $where,
        $count,
        $join
      );
      // $this->output->enable_profiler(true);
      return $this->generate_json->get_json(True, 'Data Map '.$model_dir, $extracted_data);
    }

    public function show($id){
      $this->load->model('katalog/katalog_model', 'katalogdb');
      $this->katalogdb->idkatalog = $id;
      $data = $this->katalogdb->get();
      redirect($data->url_data, 'refresh');
    }
}
