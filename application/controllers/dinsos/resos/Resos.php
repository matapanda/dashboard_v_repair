<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resos extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/resos/resos_model', 'resos_db');

        $this->title = 'Pos RESOS';
        $this->submodule  = 'Pos RESOS';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/resos/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos POS RESOS.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->resos_db->count_all();
        $data['data_count_special'] = true;

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/resos'));
    }

    public function get($idresos = null, $limit = null, $page = null)
    {
        if(!$idresos){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->resos_db->get($limit, $page);
            }else{
                $data = $this->resos_db->get();
            }
        }else{
            $this->resos_db->idresos = $idresos;
            $data = $this->resos_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data resos', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data resos', $data);
    }

    public function get_d()
    {
        $list = $this->resos_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->resos_db->count_all(),
            "recordsFiltered" => $this->resos_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('idkategoriresos', 'Kategori', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Kelurahan, Kategori, Jumlah diperlukan', FALSE);
        }

        $this->resos_db->idresos = $this->generate_id->get_id();
        $this->resos_db->tanggal = $this->input->post('tanggal');
        $this->resos_db->idkelurahan = $this->input->post('idkelurahan');
        $this->resos_db->idkategoriresos = $this->input->post('idkategoriresos');
        $this->resos_db->jumlah = $this->input->post('jumlah');
        $this->resos_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->resos_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data resos', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data resos', $data);
    }

    public function update($idresos){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('idkategoriresos', 'Kategori', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Kelurahan, Kategori, Jumlah diperlukan', FALSE);
        }

        $this->resos_db->idresos = $idresos;
        $this->resos_db->tanggal = $this->input->post('tanggal');
        $this->resos_db->idkelurahan = $this->input->post('idkelurahan');
        $this->resos_db->idkategoriresos = $this->input->post('idkategoriresos');
        $this->resos_db->jumlah = $this->input->post('jumlah');
        $this->resos_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->resos_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data resos', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data resos', $data);
    }

    public function delete($idresos){

        $this->auth->check_write($this->submodule);

        $this->resos_db->idresos = $idresos;
        $data = $this->resos_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data resos', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data resos', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinsos/resos');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 3; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kelurahan_excel = $rowData[0][2] ? : 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            //tuna netra
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idkelurahan'] = $idkelurahan;

            $data_batch_item['idkategoriresos'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);

            //tuna tungu wicara
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 2;
            $data_batch_item['jumlah'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            array_push($data_batch, $data_batch_item);

            //tuna grahita
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 3;
            $data_batch_item['jumlah'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            array_push($data_batch, $data_batch_item);

            //tuna daksa
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 4;
            $data_batch_item['jumlah'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            array_push($data_batch, $data_batch_item);

            //tuna laras
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 5;
            $data_batch_item['jumlah'] = $rowData[0][7] == false ? 0 : $rowData[0][7];
            array_push($data_batch, $data_batch_item);

            //epilepsi
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 6;
            $data_batch_item['jumlah'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            array_push($data_batch, $data_batch_item);

            //tuna ganda
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 7;
            $data_batch_item['jumlah'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            array_push($data_batch, $data_batch_item);

            //H
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 8;
            $data_batch_item['jumlah'] = $rowData[0][10] == false ? 0 : $rowData[0][10];
            array_push($data_batch, $data_batch_item);

            //ODGJ
            $data_batch_item['idresos'] = $this->generate_id->get_id();
            $data_batch_item['idkategoriresos'] = 9;
            $data_batch_item['jumlah'] = $rowData[0][11] == false ? 0 : $rowData[0][11];
            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->resos_db->data_batch = $data_batch;
        if ($this->resos_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . count($count) . ' data PKH', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data PKH', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->resos_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data resos', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data resos', $data);
    }

    public function get_based_kelurahan($year = null, $month = null){
        $data = $this->resos_db->get_based_kelurahan($year, $month);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-D', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-D', $data);
    }
}
