<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_resos extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dinsos/resos/kategoriresos_model', 'kategoriresos_db');
    }

    public function index()
    {
        /**
         * Cek check_read
         */
        $data['title'] = 'Kategori RESOS';
        $data['submodule'] = 'resos';
        $data['data'] = $this->kategoriresos_db->get();
        $data['user'] = $this->auth->get_user();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/top');
        $this->load->view('dinsos/resos/index_kategori', $data);
        $this->load->view('templates/bottom');
    }

    public function get($idkategoriresos = null)
    {
        /**
         * Cek can_read
         */
        if($idkategoriresos === null){
            $data = $this->kategoriresos_db->get();
        }else{
            $this->kategoriresos_db->idkategoriresos = $idkategoriresos;
            $data = $this->kategoriresos_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kategori resos ', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kategori resos ', $data);
    }

    public function add(){
        /**
         * Cek check_write
         */
        $data['title'] = 'Tambah Kategori RESOS';
        $data['submodule'] = 'resos';
        $data['user'] = $this->auth->get_user();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/top');
        $this->load->view('dinsos/resos/add_kategori', $data);
        $this->load->view('templates/bottom');
    }

    public function store(){
        /**
         * Cek check_write
         */

        $this->form_validation->set_rules('kategori', 'kategori', 'required');


        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Kategori diperlukan', FALSE);
        }

        $this->kategoriresos_db->idkategoriresos = $this->generate_id->get_id();
        $this->kategoriresos_db->kategori = $this->input->post('kategori');

        $data = $this->kategoriresos_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data kategori resos ', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data kategori resos ', $data);
    }

    public function edit($id){
        //menampilkan form edit
    }

    public function update($idkategoriresos){
        /**
         * Cek check_write
         */
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Kategori diperlukan', FALSE);
        }

        $this->kategoriresos_db->idkategoriresos = $idkategoriresos;
        $this->kategoriresos_db->kategori = $this->input->post('kategori');

        $data = $this->kategoriresos_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data kategori resos ', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data kategori resos ', $data);
    }

    public function delete($idkategoriresos){
        /**
         * Cek check_write
         */

        $this->kategoriresos_db->idkategoriresos = $idkategoriresos;
        $data = $this->kategoriresos_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data kategori resos ', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data kategori resos ', $data);
    }
}
