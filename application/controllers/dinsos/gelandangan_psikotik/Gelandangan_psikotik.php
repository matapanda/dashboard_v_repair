<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gelandangan_psikotik extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/gelandanganpsikotik/gelandanganpsikotik_model', 'gelandanganpsikotik_db');

        $this->title = 'Gelandangan Psikotik';
        $this->submodule = 'Gelandangan Psikotik';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/gelandangan-psikotik/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos Data Gelandangan Psikotik.xlsx');

        $this->auth->check_read($this->submodule);

    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->gelandanganpsikotik_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/gelandanganpsikotik'));
    }

    public function get($idgelandanganpsikotik = null, $limit = null, $page = null)
    {
        if (!$idgelandanganpsikotik) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->gelandanganpsikotik_db->get($limit, $page);
            } else {
                $data = $this->gelandanganpsikotik_db->get();
            }
        } else {
            $this->gelandanganpsikotik_db->idgelandanganpsikotik = $idgelandanganpsikotik;
            $data = $this->gelandanganpsikotik_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data gelandangan psikotik', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data gelandangan psikotik', $data);
    }

    public function get_d()
    {
        $list = $this->gelandanganpsikotik_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->gelandanganpsikotik_db->count_all(),
            "recordsFiltered" => $this->gelandanganpsikotik_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal dan Jumlah diperlukan', FALSE);
        }

        $this->gelandanganpsikotik_db->idgelandanganpsikotik = $this->generate_id->get_id();
        $this->gelandanganpsikotik_db->tanggal = $this->input->post('tanggal');
        $this->gelandanganpsikotik_db->jumlah = $this->input->post('jumlah');
        $this->gelandanganpsikotik_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->gelandanganpsikotik_db->add();
        
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Gelandangan Psikotik', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Gelandangan Psikotik', $data);
    }

    public function update($idgelandanganpsikotik)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal dan Jumlah diperlukan', FALSE);
        }

        $this->gelandanganpsikotik_db->idgelandanganpsikotik = $idgelandanganpsikotik;
        $this->gelandanganpsikotik_db->tanggal = $this->input->post('tanggal');
        $this->gelandanganpsikotik_db->jumlah = $this->input->post('jumlah');
        $this->gelandanganpsikotik_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->gelandanganpsikotik_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data gelandangan psikotik', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data gelandangan psikotik', $data);
    }

    public function delete($idgelandanganpsikotik)
    {

        $this->auth->check_write($this->submodule);

        $this->gelandanganpsikotik_db->idgelandanganpsikotik = $idgelandanganpsikotik;
        $data = $this->gelandanganpsikotik_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data gelandangan psikotik', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data gelandangan psikotik', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinsos/gelandangan-psikotik');

        $data_batch = [];

        if (!$data['success']) {
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 2; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $data_batch_item['idgelandanganpsikotik'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['jumlah'] = $rowData[0][2] == false ? 0 : $rowData[0][2];
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->gelandanganpsikotik_db->data_batch = $data_batch;
        if ($this->gelandanganpsikotik_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data gelandangan psikotik', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data gelandangan psikotik', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->gelandanganpsikotik_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data gelandangan psikotik', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data gelandangan psikotik', $data);
    }
}
