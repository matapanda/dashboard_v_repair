<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Razia extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/razia/razia_model', 'razia_db');

        $this->title = 'Razia';
        $this->submodule  = 'Razia';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/razia/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos Data Razia.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->razia_db->count_all();
        $data['data_count_special'] = true;

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/razia'));
    }

    public function get($idrazia = null)
    {

        $this->razia_db->idrazia = $idrazia;
        $data = $this->razia_db->get();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data razia', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data razia', $data);
    }

    public function get_d()
    {
        $list = $this->razia_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->razia_db->count_all(),
            "recordsFiltered" => $this->razia_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah', 'Tanggal', 'required');
        $this->form_validation->set_rules('latitude', 'Tanggal', 'required');
        $this->form_validation->set_rules('longitude', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkategorirazia', 'Tanggal', 'required');
        $this->form_validation->set_rules('idjeniskelamin', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Tanggal', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal, Jumlah, Latitude, Longitude, Kategori Razia, Jenis Kelamin, dan Kelurahan dibutuhkan.', FALSE);
        }

        $this->razia_db->idrazia = $this->generate_id->get_id();
        $this->razia_db->tanggal = $this->input->post('tanggal');
        if($this->input->post('iskota')){
            $this->razia_db->iskota = 1;
        }else{
            $this->razia_db->iskota = 0;
        }
        $this->razia_db->jumlah = $this->input->post('jumlah');
        $this->razia_db->latitude = $this->input->post('latitude');
        $this->razia_db->longitude = $this->input->post('longitude');
        $this->razia_db->idkategorirazia = $this->input->post('idkategorirazia');
        $this->razia_db->idjeniskelamin = $this->input->post('idjeniskelamin');
        $this->razia_db->idkelurahan = $this->input->post('idkelurahan');

        $this->razia_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->razia_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data razia', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data razia', $data);
    }

    public function update($idrazia)
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('jumlah', 'Tanggal', 'required');
        $this->form_validation->set_rules('latitude', 'Tanggal', 'required');
        $this->form_validation->set_rules('longitude', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkategorirazia', 'Tanggal', 'required');
        $this->form_validation->set_rules('idjeniskelamin', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Tanggal', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal, Jumlah, Latitude, Longitude, Kategori Razia, Jenis Kelamin, dan Kelurahan dibutuhkan.', FALSE);
        }

        $this->razia_db->idrazia = $idrazia;
        $this->razia_db->tanggal = $this->input->post('tanggal');
        if($this->input->post('iskota')){
            $this->razia_db->iskota = 1;
        }else{
            $this->razia_db->iskota = 0;
        }
        $this->razia_db->jumlah = $this->input->post('jumlah');
        $this->razia_db->latitude = $this->input->post('latitude');
        $this->razia_db->longitude = $this->input->post('longitude');
        $this->razia_db->idkategorirazia = $this->input->post('idkategorirazia');
        $this->razia_db->idjeniskelamin = $this->input->post('idjeniskelamin');
        $this->razia_db->idkelurahan = $this->input->post('idkelurahan');

        $this->razia_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->razia_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data razia', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data razia', $data);
    }

    public function delete($idrazia)
    {
        $this->auth->check_write($this->submodule);

        $this->razia_db->idrazia = $idrazia;
        $data = $this->razia_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data razia', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data razia', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinsos/razia');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 4; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            //gelandangan
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][2] == false ? 0 : $rowData[0][2];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 1;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 1;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);

            //pengemis
            //kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 2;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 2;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //luar kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 2;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][7] == false ? 0 : $rowData[0][7];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 2;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);

            //anjal
            //kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 3;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 3;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //luar kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][10] == false ? 0 : $rowData[0][10];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 3;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][11] == false ? 0 : $rowData[0][11];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 3;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);

            //punk
            //kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][12] == false ? 0 : $rowData[0][12];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 4;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][13] == false ? 0 : $rowData[0][13];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 4;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //luar kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][14] == false ? 0 : $rowData[0][14];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 4;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][15] == false ? 0 : $rowData[0][15];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 4;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);

            //pemulung
            //kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][16] == false ? 0 : $rowData[0][16];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 5;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][17] == false ? 0 : $rowData[0][17];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 5;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //luar kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][18] == false ? 0 : $rowData[0][18];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 5;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][19] == false ? 0 : $rowData[0][19];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 5;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);

            //lain-lain
            //kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][20] == false ? 0 : $rowData[0][20];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 6;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][21] == false ? 0 : $rowData[0][21];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 6;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //luar kota
            //l
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][22] == false ? 0 : $rowData[0][22];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 6;
            $data_batch_item['idjeniskelamin'] = 1;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            //p
            $data_batch_item['idrazia'] = $this->generate_id->get_id();
            $data_batch_item['iskota'] = 0;
            $data_batch_item['jumlah'] = $rowData[0][23] == false ? 0 : $rowData[0][23];
            $data_batch_item['latitude'] = 0;
            $data_batch_item['longitude'] = 0;
            $data_batch_item['idkategorirazia'] = 6;
            $data_batch_item['idjeniskelamin'] = 2;
            $data_batch_item['idkelurahan'] = 1;
            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);

        }

        $this->razia_db->data_batch = $data_batch;
        if ($this->razia_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data razia', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data razia', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->razia_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data razia', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data razia', $data);
    }

    public function get_based_kelurahan($year = null, $month = null){
        $data = $this->razia_db->get_based_kelurahan($year, $month);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-D', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-D', $data);
    }
}
