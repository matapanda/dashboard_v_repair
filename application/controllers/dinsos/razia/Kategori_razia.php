<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_razia extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->load->library('Generate_json');
    $this->load->library('Generate_id');
    $this->load->library('auth');
    $this->load->library('form_validation');
    $this->load->model('dinsos/razia/kategorirazia_model', 'kategorirazia_db');
  }

  // public function index()
  // {
  //   /**
  //    * Cek check_read
  //    */
  //   $data['razia'] = $this->razia_db->get();
  //   print_r($data);
  // }

  public function get()
  {
    /**
     * Cek can_read
     */

    // $this->razia_db->idrazia = $idrazia;
    $data = $this->kategorirazia_db->get();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kategori razia', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kategori razia', $data);
  }

  // public function add(){
  //   //menampilkan form add
  // }
  //
  // public function store(){
  //   /**
  //    * Cek check_write
  //    */
  //
  //   $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
  //
  //   if (!$this->form_validation->run()){
  //     return $this->generate_json->get_json(FALSE, 'Tanggal', FALSE);
  //   }
  //
  //   $this->razia_db->idrazia = $this->generate_id->get_id();
  //   $this->razia_db->tanggalrazia = $this->input->post('tanggalrazia');
  //   $this->razia_db->iskota = $this->input->post('iskota');
  //   $this->razia_db->jumlah = $this->input->post('jumlah');
  //   $this->razia_db->latitude = $this->input->post('latitude');
  //   $this->razia_db->longitude = $this->input->post('longitude');
  //   $this->razia_db->idkategorirazia = $this->input->post('idkategorirazia');
  //   $this->razia_db->idjeniskelamin = $this->input->post('idjeniskelamin');
  //   $this->razia_db->idkelurahan = $this->input->post('idkelurahan');
  //   $this->razia_db->idapp_user = $this->input->post('idapp_user');
  //   $this->razia_db->systemtime = date('Y-m-d H:i:s');
  //
  //   /**
  //    * --------------------------------------
  //    * Akan digantikan dengan id yang diambil dari Auth library
  //    */
  //   $this->razia_db->idapp_user = $this->input->post('idapp_user');
  //   /**
  //    * --------------------------------------
  //    */
  //   $data = $this->razia_db->add();
  //
  //   if($data){
  //     return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data pps', $data);
  //   }
  //   return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data pps', $data);
  // }
  //
  // public function edit($id){
  //   //menampilkan form edit
  // }
  //
  // public function update($idrazia){
  //   /**
  //   * Cek check_write
  //   */
  //
  //   $this->razia_db->idrazia = $idrazia;
  //   $this->razia_db->tanggalrazia = $this->input->post('tanggalrazia');
  //   $this->razia_db->iskota = $this->input->post('iskota');
  //   $this->razia_db->jumlah = $this->input->post('jumlah');
  //   $this->razia_db->latitude = $this->input->post('latitude');
  //   $this->razia_db->longitude = $this->input->post('longitude');
  //   $this->razia_db->idkategorirazia = $this->input->post('idkategorirazia');
  //   $this->razia_db->idjeniskelamin = $this->input->post('idjeniskelamin');
  //   $this->razia_db->idkelurahan = $this->input->post('idkelurahan');
  //   $this->razia_db->idapp_user = $this->input->post('idapp_user');
  //   $this->razia_db->systemtime = date('Y-m-d H:i:s');
  //
  //   /**
  //    * --------------------------------------
  //    * Akan digantikan dengan id yang diambil dari Auth library
  //    */
  //   $this->razia_db->idapp_user = $this->input->post('idapp_user');
  //   /**
  //    * --------------------------------------
  //    */
  //   $data = $this->razia_db->update();
  //
  //   if($data){
  //     return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data pps', $data);
  //   }
  //   return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data pps', $data);
  // }
  //
  // public function delete($idrazia){
  //   /**
  //  * Cek check_write
  //  */
  //
  //   $this->razia_db->idrazia = $idrazia;
  //   $data = $this->razia_db->delete();
  //
  //   if($data){
  //     return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data pps', $data);
  //   }
  //   return $this->generate_json->get_json(FALSE, 'Gagal menghapus data pps', $data);
  // }
}
