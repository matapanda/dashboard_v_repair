<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kispbid extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/kispbid/kispbid_model', 'kispbid_db');

        $this->title = 'KIS PBI-D';
        $this->submodule  = 'KIS PBI-D';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/kispbid/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos Data KIS PBI-D.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->kispbid_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/kispbid'));
    }

    public function get($idkispbid = null, $limit = null, $page = null)
    {
        if(!$idkispbid){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->kispbid_db->get($limit, $page);
            }else{
                $data = $this->kispbid_db->get();
            }
        }else{
            $this->kispbid_db->idkispbid = $idkispbid;
            $data = $this->kispbid_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kispbid', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kispbid', $data);
    }

    public function get_d()
    {
        $list = $this->kispbid_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->kispbid_db->count_all(),
            "recordsFiltered" => $this->kispbid_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('idkelurahan', 'kelurahan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('nama_ppk', 'Nama PPK', 'required');


        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Nama, Tanggal Lahir, kelurahan, Alamat, Nama PPK diperlukan', FALSE);
        }

        $this->kispbid_db->idkispbid = $this->generate_id->get_id();
        $this->kispbid_db->tanggal = $this->input->post('tanggal');
        $this->kispbid_db->nama = $this->input->post('nama');
        $this->kispbid_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->kispbid_db->idkelurahan = $this->input->post('idkelurahan');
        $this->kispbid_db->alamat = $this->input->post('alamat');
        $this->kispbid_db->nama_ppk = $this->input->post('nama_ppk');
        $this->kispbid_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->kispbid_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data kispbid', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data kispbid', $data);
    }

    public function update($idkispbid){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('nama', 'Nama', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('idkelurahan', 'kelurahan', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('nama_ppk', 'Nama PPK', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Nama, Tanggal Lahir, kelurahan, Alamat, Nama PPK diperlukan', FALSE);
        }

        $this->kispbid_db->idkispbid = $idkispbid;
        $this->kispbid_db->tanggal = $this->input->post('tanggal');
        $this->kispbid_db->nama = $this->input->post('nama');
        $this->kispbid_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->kispbid_db->idkelurahan = $this->input->post('idkelurahan');
        $this->kispbid_db->alamat = $this->input->post('alamat');
        $this->kispbid_db->nama_ppk = $this->input->post('nama_ppk');
        $this->kispbid_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->kispbid_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data kispbid', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data kispbid', $data);
    }

    public function delete($idkispbid){

        $this->auth->check_write($this->submodule);

        $this->kispbid_db->idkispbid = $idkispbid;
        $data = $this->kispbid_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data kispbid', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data kispbid', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinsos/kispbid');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 2; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kelurahan_excel = $rowData[0][6] ? : 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            $data_batch_item['idkispbid'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['nama'] = $rowData[0][2] == false ? 0 : $rowData[0][2];
            $data_batch_item['tanggal_lahir'] = $rowData[0][3] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][3], 'YYYY-MM-DD');;
            $data_batch_item['nama_ppk'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['alamat'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemdate'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            if(strpos($data_batch_item['tanggal_lahir'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal_lahir']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal_lahir'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->kispbid_db->data_batch = $data_batch;
        if ($this->kispbid_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data KIS PBI-D', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data KIS PBI-D', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->kispbid_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-D', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-D', $data);
    }

    public function get_based_kelurahan($year = null, $month = null){
        $data = $this->kispbid_db->get_based_kelurahan($year, $month);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-D', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-D', $data);
    }
}
