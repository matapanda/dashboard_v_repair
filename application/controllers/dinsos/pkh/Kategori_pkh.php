<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_pkh extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('dinsos/pkh/kategoripkh_model', 'kategoripkh_db');
    }

    public function index()
    {
        /**
         * Cek check_read
         */
        $data['title'] = 'Kategori PKH';
        $data['submodule'] = 'pkh';
        $data['data'] = $this->kategoripkh_db->get();
        $data['user'] = $this->auth->get_user();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/top');
        $this->load->view('dinsos/pkh/index_kategori', $data);
        $this->load->view('templates/bottom');
    }

    public function get($idkategori = null)
    {
        /**
         * Cek can_read
         */
        if($idkategori === null){
            $data = $this->kategoripkh_db->get();
        }else{
            $this->kategoripkh_db->idkategori = $idkategori;
            $data = $this->kategoripkh_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kategori PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kategori PKH', $data);
    }

    public function add(){
        /**
         * Cek check_write
         */
        $data['title'] = 'Tambah Kategori PKH';
        $data['submodule'] = 'pkh';
        $data['user'] = $this->auth->get_user();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/sidebar');
        $this->load->view('templates/top');
        $this->load->view('dinsos/pkh/add_kategori', $data);
        $this->load->view('templates/bottom');
    }

    public function store(){
        /**
         * Cek check_write
         */

        $this->form_validation->set_rules('kategori', 'Kategori', 'required');


        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Kategori diperlukan', FALSE);
        }

        $this->kategoripkh_db->idkategori = $this->generate_id->get_id();
        $this->kategoripkh_db->kategori = $this->input->post('kategori');

        $data = $this->kategoripkh_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data kategori PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data kategori PKH', $data);
    }

    public function edit($id){
        //menampilkan form edit
    }

    public function update($idkategori){
        /**
         * Cek check_write
         */
        $this->form_validation->set_rules('kategori', 'Kategori', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Kategori diperlukan', FALSE);
        }

        $this->kategoripkh_db->idkategori = $idkategori;
        $this->kategoripkh_db->kategori = $this->input->post('kategori');

        $data = $this->kategoripkh_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data kategori PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data kategori PKH', $data);
    }

    public function delete($idkategori){
        /**
         * Cek check_write
         */

        $this->kategoripkh_db->idkategori = $idkategori;
        $data = $this->kategoripkh_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data kategori PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data kategori PKH', $data);
    }
}
