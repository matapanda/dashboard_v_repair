<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pkh extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/pkh/pkh_model', 'pkh_db');

        $this->title = 'PKH';
        $this->submodule  = 'PKH';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/pkh/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos Data PKH.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->pkh_db->count_all();
        $data['data_count_special'] = true;

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/pkh'));
    }

    public function get($idpkh = null, $limit = null, $page = null)
    {
        if(!$idpkh){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->pkh_db->get($limit, $page);
            }else{
                $data = $this->pkh_db->get();
            }
        }else{
            $this->pkh_db->idpkh = $idpkh;
            $data = $this->pkh_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data PKH', $data);
    }

    public function get_d()
    {
        $list = $this->pkh_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pkh_db->count_all(),
            "recordsFiltered" => $this->pkh_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('idkategori', 'Kategori', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Kelurahan, Kategori, Jumlah diperlukan', FALSE);
        }

        $this->pkh_db->idpkh = $this->generate_id->get_id();
        $this->pkh_db->tanggal = $this->input->post('tanggal');
        $this->pkh_db->idkelurahan = $this->input->post('idkelurahan');
        $this->pkh_db->idkategori = $this->input->post('idkategori');
        $this->pkh_db->jumlah = $this->input->post('jumlah');
        $this->pkh_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->pkh_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data PKH', $data);
    }

    public function update($idpkh){

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('idkelurahan', 'Kelurahan', 'required');
        $this->form_validation->set_rules('idkategori', 'Kategori', 'required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'required');

        if (!$this->form_validation->run()){
            return $this->generate_json->get_json(FALSE, 'Tanggal, Kelurahan, Kategori, Jumlah diperlukan', FALSE);
        }

        $this->pkh_db->idpkh = $idpkh;
        $this->pkh_db->tanggal = $this->input->post('tanggal');
        $this->pkh_db->idkelurahan = $this->input->post('idkelurahan');
        $this->pkh_db->idkategori = $this->input->post('idkategori');
        $this->pkh_db->jumlah = $this->input->post('jumlah');
        $this->pkh_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->pkh_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data PKH', $data);
    }

    public function delete($idpkh){

        $this->auth->check_write($this->submodule);

        $this->pkh_db->idpkh = $idpkh;
        $data = $this->pkh_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data PKH', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinsos/pkh');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 3; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kelurahan_excel = $rowData[0][2] ? : 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            //bumil
            $data_batch_item['idpkh'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['idkategori'] = 1;
            $data_batch_item['jumlah'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);

            //balita
            $data_batch_item['idpkh'] = $this->generate_id->get_id();
            $data_batch_item['idkategori'] = 2;
            $data_batch_item['jumlah'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            array_push($data_batch, $data_batch_item);

            //apras
            $data_batch_item['idpkh'] = $this->generate_id->get_id();
            $data_batch_item['idkategori'] = 3;
            $data_batch_item['jumlah'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            array_push($data_batch, $data_batch_item);

            //anak SD
            $data_batch_item['idpkh'] = $this->generate_id->get_id();
            $data_batch_item['idkategori'] = 4;
            $data_batch_item['jumlah'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            array_push($data_batch, $data_batch_item);

            //anak smp
            $data_batch_item['idpkh'] = $this->generate_id->get_id();
            $data_batch_item['idkategori'] = 5;
            $data_batch_item['jumlah'] = $rowData[0][7] == false ? 0 : $rowData[0][7];
            array_push($data_batch, $data_batch_item);

            //anak sma
            $data_batch_item['idpkh'] = $this->generate_id->get_id();
            $data_batch_item['idkategori'] = 6;
            $data_batch_item['jumlah'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            array_push($data_batch, $data_batch_item);

            //lain-lain
            $data_batch_item['idpkh'] = $this->generate_id->get_id();
            $data_batch_item['idkategori'] = 2;
            $data_batch_item['jumlah'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->pkh_db->data_batch = $data_batch;
        if ($this->pkh_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data PKH', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data PKH', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->pkh_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data PKH', $data);
    }

    public function get_based_kelurahan($year = null, $month = null){
        $data = $this->pkh_db->get_based_kelurahan($year, $month);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data PKH', $data);
    }

    public function get_based_kategori($year = null, $month = null, $based_kelurahan = FALSE){
        $year == 0 ? $year = null : $year = $year;
        $month == 0 ? $month = null : $month = $month;
        $based_kelurahan == 0 ? $based_kelurahan = FALSE : $based_kelurahan = TRUE;

        $data = $this->pkh_db->get_based_kategori($year, $month, $based_kelurahan);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data PKH', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data PKH', $data);
    }
}
