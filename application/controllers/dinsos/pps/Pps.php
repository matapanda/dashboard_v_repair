<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pps extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/pps/pps_model', 'pps_db');

        $this->title = 'PPS';
        $this->submodule = 'PPS';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/pps/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos Data PPS.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {

        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->pps_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/pps'));
    }

    public function get($iddinsos_pps = null)
    {
        $this->pps_db->iddinsos_pps = $iddinsos_pps;
        $data = $this->pps_db->get();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data pps', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data pps', $data);
    }

    public function get_d()
    {
        ob_start();
        $list = $this->pps_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pps_db->count_all(),
            "recordsFiltered" => $this->pps_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
        ob_flush();
    }

    public function store()
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('no_kk', 'no_kk', 'required');
        $this->form_validation->set_rules('no_nik', 'no_nik', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'tanggal_lahir', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal, Alamat, Nomor KK, Nomor NIK, Nama, Tanggal Lahir dibutuhkan.', FALSE);
        }

        $this->pps_db->iddinsos_pps = $this->generate_id->get_id();
        $this->pps_db->idkelurahan = $this->input->post('idkelurahan');
        $this->pps_db->tanggal = $this->input->post('tanggal');
        $this->pps_db->alamat = $this->input->post('alamat');
        $this->pps_db->no_kk = $this->input->post('no_kk');
        $this->pps_db->no_nik = $this->input->post('no_nik');
        $this->pps_db->nama = $this->input->post('nama');
        $this->pps_db->idhubungankeluarga = $this->input->post('idhubungankeluarga');
        $this->pps_db->idhubungankeluarga = $this->input->post('idhubungankeluarga');
        $this->pps_db->idjeniskelamin = $this->input->post('idjeniskelamin');
        $this->pps_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->pps_db->idstatuskawin = $this->input->post('idstatuskawin');
        $this->pps_db->idpendidikan = $this->input->post('idpendidikan');
        $this->pps_db->idstatuskeluarga = $this->input->post('idstatuskeluarga');
        $this->pps_db->idstatusindividu = $this->input->post('idstatusindividu');
        $this->pps_db->systemtime = date('Y-m-d H:i:s');
        $this->pps_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->pps_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data pps', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data pps', $data);
    }

    public function update($iddinsos_pps)
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('no_kk', 'no_kk', 'required');
        $this->form_validation->set_rules('no_nik', 'no_nik', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'tanggal_lahir', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal, Alamat, Nomor KK, Nomor NIK, Nama, Tanggal Lahir dibutuhkan.', FALSE);
        }

        $this->pps_db->iddinsos_pps = $iddinsos_pps;
        $this->pps_db->idkelurahan = $this->input->post('idkelurahan');
        $this->pps_db->tanggal = $this->input->post('tanggal');
        $this->pps_db->alamat = $this->input->post('alamat');
        $this->pps_db->no_kk = $this->input->post('no_kk');
        $this->pps_db->no_nik = $this->input->post('no_nik');
        $this->pps_db->nama = $this->input->post('nama');
        $this->pps_db->idhubungankeluarga = $this->input->post('idhubungankeluarga');
        $this->pps_db->idjeniskelamin = $this->input->post('idjeniskelamin');
        $this->pps_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->pps_db->idstatuskawin = $this->input->post('idstatuskawin');
        $this->pps_db->idpendidikan = $this->input->post('idpendidikan');
        $this->pps_db->idstatuskeluarga = $this->input->post('idstatuskeluarga');
        $this->pps_db->idstatusindividu = $this->input->post('idstatusindividu');
        $this->pps_db->systemtime = date('Y-m-d H:i:s');
        $this->pps_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->pps_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data pps', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data pps', $data);
    }

    public function delete($iddinsos_pps)
    {
        $this->auth->check_write($this->submodule);

        $this->pps_db->iddinsos_pps = $iddinsos_pps;
        $data = $this->pps_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data pps', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data pps', $data);
    }

    public function uploadExcel()
    {
        ob_start();
        $data = $this->upload_excel->upload('dinsos/pps');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');
        $this->load->model('master/hubungankeluarga_model', 'hubungankeluarga_db');
        $this->load->model('master/jeniskelamin_model', 'jeniskelamin_db');
        $this->load->model('master/statuskawin_model', 'statuskawin_db');
        $this->load->model('master/pendidikan_model', 'pendidikan_db');
        $this->load->model('master/statuskeluarga_model', 'statuskeluarga_db');
        $this->load->model('master/statusindividu_model', 'statusindividu_db');

        $data_batch = [];

        if (!$data['success']) {
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 2; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kelurahan_excel = $rowData[0][2] ?: 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            $hubungankeluarga_excel = $rowData[0][7] ?: 'lain-lain';
            $this->hubungankeluarga_db->hubungankeluarga = $hubungankeluarga_excel;
            $hubungankeluarga_data = $this->hubungankeluarga_db->get();
            $idhubungankeluarga = $hubungankeluarga_data ? $hubungankeluarga_data->idhubungankeluarga : 99;

            $jeniskelamin_excel = $rowData[0][8] ?: 'lain-lain';
            $this->jeniskelamin_db->jeniskelamin = $jeniskelamin_excel;
            $jeniskelamin_data = $this->jeniskelamin_db->get();
            $idjeniskelamin = $jeniskelamin_data ? $jeniskelamin_data->idjeniskelamin : 99;

            $statuskawin_excel = $rowData[0][10] ?: 'lain-lain';
            $this->statuskawin_db->status = $statuskawin_excel;
            $statuskawin_data = $this->statuskawin_db->get();
            $idstatuskawin = $statuskawin_data ? $statuskawin_data->idstatuskawin : 99;

            $pendidikan_excel = $rowData[0][11] ?: 'lain-lain';
            $this->pendidikan_db->pendidikan = $pendidikan_excel;
            $pendidikan_data = $this->pendidikan_db->get();
            $idpendidikan = $pendidikan_data ? $pendidikan_data->idpendidikan : 99;

            $statuskeluarga_excel = $rowData[0][12] ?: 'lain-lain';
            $this->statuskeluarga_db->statuskeluarga = $statuskeluarga_excel;
            $statuskeluarga_data = $this->statuskeluarga_db->get();
            $idstatuskeluarga = $statuskeluarga_data ? $statuskeluarga_data->idstatuskeluarga : 99;

            $statusindividu_excel = $rowData[0][13] ?: 'lain-lain';
            $this->statusindividu_db->statusindividu = $statusindividu_excel;
            $statusindividu_data = $this->statusindividu_db->get();
            $idstatusindividu = $statusindividu_data ? $statusindividu_data->idstatusindividu : 99;

            $data_batch_item['iddinsos_pps'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['alamat'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['no_kk'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['no_nik'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['nama'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            $data_batch_item['idhubungankeluarga'] = $idhubungankeluarga;
            $data_batch_item['idjeniskelamin'] = $idjeniskelamin;
            $data_batch_item['tanggal_lahir'] = $rowData[0][9] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][9], 'YYYY-MM-DD');;
            $data_batch_item['idstatuskawin'] = $idstatuskawin;
            $data_batch_item['idpendidikan'] = $idpendidikan;
            $data_batch_item['idstatuskeluarga'] = $idstatuskeluarga;
            $data_batch_item['idstatusindividu'] = $idstatusindividu;
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');

            if (strpos($data_batch_item['tanggal'], '/') !== false) {
                $date = explode('/', $data_batch_item['tanggal']);
                if (!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if (!isset($date[1]) || !$date[1]) $date[1] = '00';
                if (!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2] . '-' . $date[1] . '-' . $date[0];
            }

            if (strpos($data_batch_item['tanggal_lahir'], '/') !== false) {
                $date = explode('/', $data_batch_item['tanggal_lahir']);
                if (!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if (!isset($date[1]) || !$date[1]) $date[1] = '00';
                if (!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal_lahir'] = $date[2] . '-' . $date[1] . '-' . $date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->pps_db->data_batch = $data_batch;
        if ($this->pps_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            ob_flush();
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data PPS', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data PPS', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->pps_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data PPS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data PPS', $data);
    }

    public function get_based_kelurahan($year = null, $month = null)
    {
        $data = $this->pps_db->get_based_kelurahan($year, $month);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-D', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-D', $data);
    }
}
