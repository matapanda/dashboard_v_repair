<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KKS extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/kks/kks_model', 'kks_db');

        $this->title = 'KKS';
        $this->submodule = 'KKS';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/kks/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos Data KKS.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->kks_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/kks'));
    }


    public function get($idkks = null, $limit = null, $page = null)
    {
        $this->kks_db->idkks = $idkks;
        $data = $this->kks_db->get($limit, $page);

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kks', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kks', $data);
    }

    public function get_d()
    {
        $list = $this->kks_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->kks_db->count_all(),
            "recordsFiltered" => $this->kks_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal', FALSE);
        }

        $this->kks_db->idkks = $this->generate_id->get_id();
        $this->kks_db->tanggal = $this->input->post('tanggal');
        $this->kks_db->idkelurahan = $this->input->post('idkelurahan');
        $this->kks_db->lagitude = $this->input->post('longitude');
        $this->kks_db->latitude = $this->input->post('latitude');
        $this->kks_db->no_kks = $this->input->post('no_kks');
        $this->kks_db->pengurus = $this->input->post('pengurus');
        $this->kks_db->alamat = $this->input->post('alamat');
        $this->kks_db->krt = $this->input->post('krt');
        $this->kks_db->pkrt = $this->input->post('pkrt');
        $this->kks_db->artl = $this->input->post('artl');
        $this->kks_db->jumlah_art = $this->input->post('jumlah_art');
        $this->kks_db->is_pkh = $this->input->post('is_pkh') ? 1 : 0;
        $this->kks_db->is_pbdt = $this->input->post('is_pbdt') ? 1 : 0;
        $this->kks_db->no_pbdt = $this->input->post('no_pbdt');
        $this->kks_db->no_pkh = $this->input->post('no_pkh');
        $this->kks_db->is_batal_kks = $this->input->post('is_batal_kks') ? 1 : 0;
        $this->kks_db->is_pengajuan_pkh = $this->input->post('is_pengajuan_pkh') ? 1 : 0;
        $this->kks_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->kks_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data kks', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data kks', $data);
    }

    public function update($idkks)
    {

        $this->auth->check_write($this->submodule);

        $this->kks_db->idkks = $idkks;
        $this->kks_db->tanggal = $this->input->post('tanggal');
        $this->kks_db->idkelurahan = $this->input->post('idkelurahan');
        $this->kks_db->lagitude = $this->input->post('longitude');
        $this->kks_db->latitude = $this->input->post('latitude');
        $this->kks_db->no_kks = $this->input->post('no_kks');
        $this->kks_db->pengurus = $this->input->post('pengurus');
        $this->kks_db->alamat = $this->input->post('alamat');
        $this->kks_db->krt = $this->input->post('krt');
        $this->kks_db->pkrt = $this->input->post('pkrt');
        $this->kks_db->artl = $this->input->post('artl');
        $this->kks_db->jumlah_art = $this->input->post('jumlah_art');
        $this->kks_db->is_pkh = $this->input->post('is_pkh');
        $this->kks_db->is_pbdt = $this->input->post('is_pbdt');
        $this->kks_db->no_pbdt = $this->input->post('no_pbdt');
        $this->kks_db->no_pkh = $this->input->post('no_pkh');
        $this->kks_db->is_batal_kks = $this->input->post('is_batal_kks');
        $this->kks_db->is_pengajuan_pkh = $this->input->post('is_pengajuan_pkh');
        $this->kks_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->kks_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data kks', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data kks', $data);
    }

    public function delete($idkks)
    {

        $this->auth->check_write($this->submodule);

        $this->kks_db->idkks = $idkks;
        $data = $this->kks_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data kks', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data kks', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinsos/kks');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 2; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kelurahan_excel = $rowData[0][2] ? : 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            $data_batch_item['idkks'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');;
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['no_kks'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['pengurus'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['alamat'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['krt'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            $data_batch_item['pkrt'] = $rowData[0][7] == false ? 0 : $rowData[0][7];
            $data_batch_item['artl'] = $rowData[0][8] == false ? 0 : $rowData[0][8];
            $data_batch_item['jumlah_art'] = $rowData[0][9] == false ? 0 : $rowData[0][9];
            $data_batch_item['is_pkh'] = strtolower($rowData[0][10]) == 'ya' ? 1 : 0;
            $data_batch_item['no_pkh'] = $rowData[0][11] == false ? 0 : $rowData[0][11];
            $data_batch_item['is_pbdt'] = strtolower($rowData[0][12]) == 'ya' ? 1 : 0;
            $data_batch_item['no_pbdt'] = $rowData[0][13] == false ? 0 : $rowData[0][13];
            $data_batch_item['is_batal_kks'] = strtolower($rowData[0][14]) == 'ya' ? 1 : 0;
            $data_batch_item['is_pengajuan_pkh'] = strtolower($rowData[0][15]) == 'ya' ? 1 : 0;
            $data_batch_item['lagitude'] = 0;
            $data_batch_item['latitude'] = 0;
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->kks_db->data_batch = $data_batch;
        if ($this->kks_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data KKS', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data KKS', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->kks_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KKS', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KKS', $data);
    }

    public function get_based_kelurahan($year = null, $month = null){
        $data = $this->kks_db->get_based_kelurahan($year, $month);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-D', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-D', $data);
    }
}
