<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kispbin extends CI_Controller
{
    private $title;
    private $submodule;
    private $user;
    private $api_url_upload;
    private $url_file_format;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->library('Upload_excel');
        $this->load->model('dinsos/kispbin/kispbin_model', 'kispbin_db');


        $this->title = 'KIS PBI-N';
        $this->submodule  = 'KIS PBI-N';
        $this->user = $this->auth->get_user();

        $this->api_url_upload = base_url('api/dinsos/kispbin/upload');
        $this->url_file_format = base_url('assets/files/Format Dinsos Data KIS PBI-N.xlsx');

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['api_url_upload'] = $this->api_url_upload;
        $data['url_file_format'] = $this->url_file_format;
        $data['data_count'] = $this->kispbin_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'dinsos/kispbin'));
    }

    public function get_d()
    {
        $list = $this->kispbin_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->kispbin_db->count_all(),
            "recordsFiltered" => $this->kispbin_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function get($idkispbin = null, $limit = null, $page = null)
    {
        if (!$idkispbin) {
            if (is_numeric($limit) && is_numeric($page)) {
                $data = $this->kispbin_db->get($limit, $page);
            } else {
                $data = $this->kispbin_db->get();
            }
        } else {
            $this->kispbin_db->idkispbin = $idkispbin;
            $data = $this->kispbin_db->get();
        }
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data kispbin', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data kispbin', $data);
    }

    public function store()
    {

        $this->auth->check_write($this->submodule);


        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('no_kk', 'no_kk', 'required');
        $this->form_validation->set_rules('nik', 'nik', 'required');
        $this->form_validation->set_rules('nama_anggota_keluarga', 'nama_anggota_keluarga', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'tanggal_lahir', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal, Alamat, Nomor KK, Nomor NIK, Nama Anggota Keluarga, Tanggal Lahir dibutuhkan.', FALSE);
        }

        $this->kispbin_db->tanggal = $this->input->post('tanggal');
        $this->kispbin_db->alamat = $this->input->post('alamat');
        $this->kispbin_db->idkelurahan = $this->input->post('idkelurahan');
        $this->kispbin_db->no_kk = $this->input->post('no_kk');
        $this->kispbin_db->nik = $this->input->post('nik');
        $this->kispbin_db->nama_anggota_keluarga = $this->input->post('nama_anggota_keluarga');
        $this->kispbin_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->kispbin_db->status_lama = $this->input->post('status_lama');
        $this->kispbin_db->status_sekarang = $this->input->post('status_sekarang');
        $this->kispbin_db->status_keterangan = $this->input->post('status_keterangan');
        $this->kispbin_db->idkispbin = $this->generate_id->get_id();
        $this->kispbin_db->idapp_user = $this->auth->get_user('idapp_user');


        $data = $this->kispbin_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data KISP-BIN', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data KISP-BIN', $data);
    }

    public function update($idkispbin)
    {

        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required');
        $this->form_validation->set_rules('no_kk', 'no_kk', 'required');
        $this->form_validation->set_rules('nik', 'nik', 'required');
        $this->form_validation->set_rules('nama_anggota_keluarga', 'nama_anggota_keluarga', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'tanggal_lahir', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Tanggal, Alamat, Nomor KK, Nomor NIK, Nama Anggota Keluarga, Tanggal Lahir dibutuhkan.', FALSE);
        }

        $this->kispbin_db->idkispbin = $idkispbin;
        $this->kispbin_db->tanggal = $this->input->post('tanggal');
        $this->kispbin_db->alamat = $this->input->post('alamat');
        $this->kispbin_db->idkelurahan = $this->input->post('idkelurahan');
        $this->kispbin_db->no_kk = $this->input->post('no_kk');
        $this->kispbin_db->nik = $this->input->post('nik');
        $this->kispbin_db->nama_anggota_keluarga = $this->input->post('nama_anggota_keluarga');
        $this->kispbin_db->tanggal_lahir = $this->input->post('tanggal_lahir');
        $this->kispbin_db->status_lama = $this->input->post('status_lama');
        $this->kispbin_db->status_sekarang = $this->input->post('status_sekarang');
        $this->kispbin_db->status_keterangan = $this->input->post('status_keterangan');
        $this->kispbin_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->kispbin_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data KISP-BIN', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data KISP-BIN', $data);
    }

    public function delete($idkispbin)
    {

        $this->auth->check_write($this->submodule);

        $this->kispbin_db->idkispbin = $idkispbin;
        $data = $this->kispbin_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data KISP-BIN', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data KISP-BIN', $data);
    }

    public function uploadExcel()
    {
        $data = $this->upload_excel->upload('dinsos/kispbin');
        $this->load->model('master/kelurahan_model', 'kelurahan_db');
        $this->load->model('master/statuskis_model', 'status_db');

        $data_batch = [];

        if(!$data['success']){
            return $this->generate_json->get_json(FALSE, $data['message'], FALSE);
        }

        for ($row = 2; $row <= $data['max_row']; $row++) {

            $rowData = $this->upload_excel->get_row_data($row);

            $data_batch_item = [];

            if (!$rowData[0][1]) break;

            $kelurahan_excel = $rowData[0][2] ? : 'lain-lain';
            $this->kelurahan_db->kelurahan = $kelurahan_excel;
            $kelurahan_data = $this->kelurahan_db->get();
            $idkelurahan = $kelurahan_data ? $kelurahan_data->idkelurahan : 1;

            $status_excel = $rowData[0][8] ? : 'lain-lain';
            $this->status_db->status = $status_excel;
            $status_data = $this->status_db->get();
            $idstatus_lama = $status_data ? $status_data->idstatuskis : 99;

            $status_excel = $rowData[0][9] ? : 'lain-lain';
            $this->status_db->status = $status_excel;
            $status_data = $this->status_db->get();
            $idstatus_sekarang = $status_data ? $status_data->idstatuskis : 99;

            $status_excel = $rowData[0][10] ? : 'lain-lain';
            $this->status_db->status = $status_excel;
            $status_data = $this->status_db->get();
            $idstatus_keterangan = $status_data ? $status_data->idstatuskis : 99;

            $data_batch_item['idkispbin'] = $this->generate_id->get_id();
            $data_batch_item['tanggal'] = $rowData[0][1] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');
            $data_batch_item['idkelurahan'] = $idkelurahan;
            $data_batch_item['alamat'] = $rowData[0][3] == false ? 0 : $rowData[0][3];
            $data_batch_item['no_kk'] = $rowData[0][4] == false ? 0 : $rowData[0][4];
            $data_batch_item['nik'] = $rowData[0][5] == false ? 0 : $rowData[0][5];
            $data_batch_item['nama_anggota_keluarga'] = $rowData[0][6] == false ? 0 : $rowData[0][6];
            $data_batch_item['tanggal_lahir'] = $rowData[0][7] == false ? '0000-00-00' : \PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][7], 'YYYY-MM-DD');
            $data_batch_item['status_lama'] = $idstatus_lama;
            $data_batch_item['status_sekarang'] = $idstatus_sekarang;
            $data_batch_item['status_keterangan'] = $idstatus_keterangan;
            $data_batch_item['idapp_user'] = $this->auth->get_user('idapp_user');
            $data_batch_item['systemtime'] = date('Y-m-d H:i:s');

            if(strpos($data_batch_item['tanggal'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            if(strpos($data_batch_item['tanggal_lahir'], '/') !== false){
                $date = explode('/', $data_batch_item['tanggal_lahir']);
                if(!isset($date[2]) || !$date[2]) $date[2] = '0000';
                if(!isset($date[1]) || !$date[1]) $date[1] = '00';
                if(!isset($date[0]) || !$date[0]) $date[0] = '00';

                $data_batch_item['tanggal_lahir'] = $date[2].'-'.$date[1].'-'.$date[0];
            }

            $data_batch_item['tanggal_lahir'] = str_replace('/', '-', $data_batch_item['tanggal_lahir']);

            array_push($data_batch, $data_batch_item);
            unset($data_batch_item);
        }

        $this->kispbin_db->data_batch = $data_batch;
        if ($this->kispbin_db->add_batch($data_batch)) {
            $count = count($data_batch);
            unset($data_batch);
            return $this->generate_json->get_json(TRUE, 'Berhasil upload file dan menyimpan ' . $count . ' data KIS PBI-N', $count);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data KIS PBI-N', FALSE);
    }

    public function get_based_date($year = null, $month = null, $day = null)
    {
        $data = $this->kispbin_db->get_based_date($year, $month, $day);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-N', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-N', $data);
    }

    public function get_based_kelurahan($year = null, $month = null){
        $data = $this->kispbin_db->get_based_kelurahan($year, $month);
        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data KIS PBI-D', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data KIS PBI-D', $data);
    }
}
