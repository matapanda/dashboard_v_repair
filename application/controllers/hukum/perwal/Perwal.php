<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perwal extends CI_Controller
{

  private $title;
  private $submodule;
  private $user;

  public function __construct()
  {
    parent::__construct();

    $this->load->library('Generate_json');
    $this->load->library('Generate_id');
    $this->load->library('Generate_view');
    $this->load->library('auth');
    $this->load->library('form_validation');
    $this->load->model('hukum/perwal/Perwal_model', 'perwal_db');

    $this->title = 'Perwal';
    $this->submodule  = 'Perwal';
    $this->user = $this->auth->get_user();

    $this->auth->check_read($this->submodule);
  }

  public function index()
  {
    $data['title'] = $this->title;
    $data['submodule'] = $this->submodule;
    $data['user'] = $this->user;
      $data['data_count'] = $this->perwal_db->count_all();

    $this->load->view('templates/template', $this->generate_view->get_index($data, 'hukum/perwal'));
  }

  public function get($idperwal = null, $limit = null, $page = null)
  {
    if(!$idperwal){
      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->perwal_db->get($limit, $page);
      }else{
        $data = $this->perwal_db->get();
      }
    }else{
      $this->perwal_db->idperwal = $idperwal;
      $data = $this->perwal_db->get();
    }
    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil Perwal', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil Perwal', $data);
  }

    public function get_d()
    {
        $list = $this->perwal_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->perwal_db->count_all(),
            "recordsFiltered" => $this->perwal_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

  public function store(){

    $this->auth->check_write($this->submodule);

    $this->perwal_db->idperwal = $this->generate_id->get_id();
    $this->perwal_db->tanggal = $this->input->post('tanggal');
    $this->perwal_db->perwal = $this->input->post('perwal');
    $this->perwal_db->is_asli = $this->input->post('is_asli') ? 1:0;
    $this->perwal_db->is_paraf = $this->input->post('is_paraf') ? 1:0;
    $this->perwal_db->is_salinan = $this->input->post('is_salinan') ? 1:0;
    $this->perwal_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->perwal_db->add();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan Perwal', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menyimpan Perwal', $data);
  }

  public function update($idperwal){

    $this->auth->check_write($this->submodule);

    $this->perwal_db->idperwal = $idperwal;
    $this->perwal_db->tanggal = $this->input->post('tanggal');
    $this->perwal_db->perwal = $this->input->post('perwal');
    $this->perwal_db->is_asli = $this->input->post('is_asli') ? 1:0;
    $this->perwal_db->is_paraf = $this->input->post('is_paraf') ? 1:0;
    $this->perwal_db->is_salinan = $this->input->post('is_salinan') ? 1:0;
    $this->perwal_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->perwal_db->update();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi Perwal', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi Perwal', $data);
  }

  public function delete($idperwal){

    $this->auth->check_write($this->submodule);

    $this->perwal_db->idperwal = $idperwal;
    $data = $this->perwal_db->delete();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menghapus Perwal', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menghapus Perwal', $data);
  }
}
