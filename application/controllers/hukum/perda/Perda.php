<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perda extends CI_Controller
{

  private $title;
  private $submodule;
  private $user;

  public function __construct()
  {
    parent::__construct();

    $this->load->library('Generate_json');
    $this->load->library('Generate_id');
    $this->load->library('Generate_view');
    $this->load->library('auth');
    $this->load->library('form_validation');
    $this->load->model('hukum/perda/perda_model', 'perda_db');

    $this->title = 'Perda';
    $this->submodule  = 'Perda';
    $this->user = $this->auth->get_user();

    $this->auth->check_read($this->submodule);
  }

  public function index()
  {
    $data['title'] = $this->title;
    $data['submodule'] = $this->submodule;
    $data['user'] = $this->user;
      $data['data_count'] = $this->perda_db->count_all();

    $this->load->view('templates/template', $this->generate_view->get_index($data, 'hukum/perda'));
  }

  public function get($idperda = null, $limit = null, $page = null)
  {
    if(!$idperda){
      if(is_numeric($limit) && is_numeric($page)){
        $data = $this->perda_db->get($limit, $page);
      }else{
        $data = $this->perda_db->get();
      }
    }else{
      $this->perda_db->idperda = $idperda;
      $data = $this->perda_db->get();
    }
    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Perda', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Perda', $data);
  }

    public function get_d()
    {
        $list = $this->perda_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->perda_db->count_all(),
            "recordsFiltered" => $this->perda_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

  public function store(){

    $this->auth->check_write($this->submodule);

    $this->perda_db->idperda = $this->generate_id->get_id();
    $this->perda_db->tanggal = $this->input->post('tanggal');
    $this->perda_db->perda = $this->input->post('perda');
    $this->perda_db->keterangan = $this->input->post('keterangan');
    $this->perda_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->perda_db->add();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Perda', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Perda', $data);
  }

  public function update($idperda){

    $this->auth->check_write($this->submodule);

    $this->perda_db->idperda = $idperda;
    $this->perda_db->tanggal = $this->input->post('tanggal');
    $this->perda_db->perda = $this->input->post('perda');
    $this->perda_db->keterangan = $this->input->post('keterangan');
    $this->perda_db->idapp_user = $this->auth->get_user('idapp_user');

    $data = $this->perda_db->update();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Perda', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Perda', $data);
  }

  public function delete($idperda){

    $this->auth->check_write($this->submodule);

    $this->perda_db->idperda = $idperda;
    $data = $this->perda_db->delete();

    if($data){
      return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Perda', $data);
    }
    return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Perda', $data);
  }
  
  
}
