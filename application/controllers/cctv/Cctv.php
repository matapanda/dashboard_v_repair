<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cctv extends CI_Controller
{
    private $submodule;

    public function __construct()
    {
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->model('cctv/cctv_model', 'cctv_db');

        if (!$this->auth->get_user()) {
            redirect('/#login', 'location');
        }

        $this->submodule = 'cctv';
    }

    public function index()
    {
        $data['title'] = 'CCTV';
        $data['submodule'] = 'cctv';
        $data['user'] = $this->auth->get_user();
        $data['list_cctv'] = $this->cctv_db->get();

        $data['template_sidebar'] = $this->load->view('templates/template_sidebar', $data, TRUE);
        $data['template_header'] = $this->load->view('templates/template_header', $data, TRUE);
        $data['template_footer'] = $this->load->view('templates/template_footer', $data, TRUE);
        $data['main_content'] = $this->load->view('cctv/index', $data, TRUE);
        $this->load->view('templates/template', $data);
    }

    public function show($id)
    {
        $data['title'] = 'CCTV';
        $this->cctv_db->idcctv = $id;
        $cctv = $this->cctv_db->get();
        $data['judul'] = $cctv->judul;
        $data['source'] = $cctv->source;
        $data['main_content'] = $this->load->view('cctv/show', $data);
    }

    public function store()
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('source', 'ource', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Judul, source diperlukan', FALSE);
        }

        $this->cctv_db->idcctv = $this->generate_id->get_id();
        $this->cctv_db->judul = $this->input->post('judul');
        $this->cctv_db->source = $this->input->post('source');

        $data = $this->cctv_db->add();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data CCTV', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data CCTV', $data);
    }

    public function update($idcctv)
    {
        $this->auth->check_write($this->submodule);

        $this->form_validation->set_rules('judul', 'Judul', 'required');
        $this->form_validation->set_rules('source', 'Source', 'required');

        if (!$this->form_validation->run()) {
            return $this->generate_json->get_json(FALSE, 'Judul, source diperlukan', FALSE);
        }

        $this->cctv_db->idcctv = $idcctv;
        $this->cctv_db->judul = $this->input->post('judul');
        $this->cctv_db->source = $this->input->post('source');

        $data = $this->cctv_db->update();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data CCTV', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data CCTV', $data);
    }

    public function delete($idcctv)
    {
        $this->auth->check_write($this->submodule);

        $this->cctv_db->idcctv = $idcctv;
        $data = $this->cctv_db->delete();

        if ($data) {
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data katalog', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data katalog', $data);
    }
}
