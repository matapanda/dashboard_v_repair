<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bppdapi extends CI_Controller{

    public function __construct(){
        parent::__construct();


        $this->load->library('form_validation');
        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('auth');
        $this->load->library('Response_message');
        $this->load->library('Checkurl');
        $this->load->model('katalog/katalog_model', 'katalog_db');


        $this->submodule = 'katalog';
    }

    public function index(){
        print_r("surya");
    }
    
    #add new by surya ncc
    #get api from SPM
        #->sebaran rumah sakit
        #->sebaran penyakit
        #->sebaran pengajuan PBI
        #->sebaran pengajuan SPM
        #->sebaran pengajuan SPM di terima
    
    public function get_html($url){
        $content = file_get_contents($url);
        return $content;
    }

    public function index_data_realisasi(){
        $url = "http://sampade.malangkota.go.id:8084/kominfo/walikota.php";
        $data = json_decode(file_get_contents($url));

        // print_r($data);
        $data_json = array();
        $data_json_perbandingan = array();

        $tmp_total = array("jenis_pajak"=>"Total Pajak",
                            "realisasi"=>0,
                            "target"=>0);

        foreach ($data as $key => $value) {
            $tmp_data = array("jenis_pajak"=>$key,
                            "realisasi"=>$value->Realisasi,
                            "target"=>$value->Target);

            $tmp_total["realisasi"] += $value->Realisasi;
            $tmp_total["target"] += $value->Target;


            array_push($data_json, $tmp_data);

            $balum_realisasi = (float)$value->Target - (float)$value->Realisasi;

            $tmp_data_json_perbandingan = array(
                                                array("title"=>"Target ".$key, "val"=>$value->Target, "color"=> "chart.colors.next()"),
                                                array("title"=>"Realisasi ".$key, "val"=>$value->Realisasi, "color"=> "chart.colors.next()")
                                            );
            array_push($data_json_perbandingan, $tmp_data_json_perbandingan);
        }

        $tmp_data_json_perbandingan = array(
                                                array("title"=>"Total Target ", "val"=>$tmp_total["target"], "color"=> "chart.colors.next()"),
                                                array("title"=>"Total Realisasi", "val"=>$tmp_total["realisasi"], "color"=> "chart.colors.next()")
                                            );
        array_push($data_json_perbandingan, $tmp_data_json_perbandingan);

        array_push($data_json, $tmp_total);

        $data_send["data_json"] = json_encode($data_json);
        $data_send["data_json_perbandingan"] = json_encode($data_json_perbandingan);
        
        // print_r(json_encode($data_json));
        $this->load->view("bp2d/bppd_realisasi", $data_send);
    }

    public function get_pbb(){
        $nama = $this->input->post("nama_wp");
        $nop = $this->input->post("nop");

        $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array(null);

        $url = "http://sampade.malangkota.go.id:8084/kominfo/pbb.php?nama=$nama&nop=$nop";
        // print_r($url);

        $data_json = json_decode(file_get_contents($url));
        if($data_json){
            $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            $msg_detail = array("list_result"=>$data_json);
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }

    public function index_pbb(){
        $this->load->view("bp2d/pbb");
    }

    
}
