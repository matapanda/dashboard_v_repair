<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatistikSektoral extends CI_Controller
{

    private $title;
    private $submodule;
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('Generate_json');
        $this->load->library('Generate_id');
        $this->load->library('Generate_view');
        $this->load->library('auth');
        $this->load->library('form_validation');
        $this->load->model('bp2d/statistiksektoral/Statistiksektoral_model', 'statistiksektoral_db');

        $this->title = 'Statistik Sektoral';
        $this->submodule  = 'Statistik Sektoral';
        $this->user = $this->auth->get_user();

        $this->auth->check_read($this->submodule);
    }

    public function index()
    {
        $data['title'] = $this->title;
        $data['submodule'] = $this->submodule;
        $data['user'] = $this->user;
        $data['data_count'] = $this->statistiksektoral_db->count_all();

        $this->load->view('templates/template', $this->generate_view->get_index($data, 'bp2d/statistiksektoral'));
    }

    public function get($idbp2d = null, $limit = null, $page = null)
    {
        if(!$idbp2d){
            if(is_numeric($limit) && is_numeric($page)){
                $data = $this->statistiksektoral_db->get($limit, $page);
            }else{
                $data = $this->statistiksektoral_db->get();
            }
        }else{
            $this->statistiksektoral_db->idbp2d = $idbp2d;
            $data = $this->statistiksektoral_db->get();
        }
        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil mengambil data Statistik Sektoral', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal mengambil data Statistik Sektoral', $data);
    }

    public function get_d()
    {
        $list = $this->statistiksektoral_db->get_datatables();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $item->no = $no;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->statistiksektoral_db->count_all(),
            "recordsFiltered" => $this->statistiksektoral_db->count_filtered(),
            "data" => $list,
        );
        echo json_encode($output);
    }

    public function store(){
        $this->auth->check_write($this->submodule);

        $this->statistiksektoral_db->idbp2d = $this->generate_id->get_id();
        $this->statistiksektoral_db->tanggal = $this->input->post('tanggal');
        $this->statistiksektoral_db->namapajak = $this->input->post('namapajak');
        $this->statistiksektoral_db->target = str_replace(array( ','), '', $this->input->post('target'));
        $this->statistiksektoral_db->realisasi = str_replace(array( ','), '', $this->input->post('realisasi'));
        $this->statistiksektoral_db->realisasi = $this->input->post('realisasi');
        $this->statistiksektoral_db->sumberdata = $this->input->post('sumberdata');
        $this->statistiksektoral_db->keterangan = $this->input->post('keterangan');
        $this->statistiksektoral_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->statistiksektoral_db->add();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menyimpan data Statistik Sektoral', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menyimpan data Statistik Sektoral', $data);
    }

    public function update($idbp2d){

        $this->auth->check_write($this->submodule);

        $this->statistiksektoral_db->idbp2d = $idbp2d;
        $this->statistiksektoral_db->tanggal = $this->input->post('tanggal');
        $this->statistiksektoral_db->namapajak = $this->input->post('namapajak');
        $this->statistiksektoral_db->satuan = $this->input->post('satuan');
        $this->statistiksektoral_db->target = str_replace(array( ','), '', $this->input->post('target'));
        $this->statistiksektoral_db->realisasi = str_replace(array( ','), '', $this->input->post('realisasi'));
        $this->statistiksektoral_db->sumberdata = $this->input->post('sumberdata');
        $this->statistiksektoral_db->keterangan = $this->input->post('keterangan');
        $this->statistiksektoral_db->idapp_user = $this->auth->get_user('idapp_user');

        $data = $this->statistiksektoral_db->update();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil memodifikasi data Statistik Sektoral', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal memodifikasi data Statistik Sektoral', $data);
    }

    public function delete($idbp2d){

        $this->auth->check_write($this->submodule);

        $this->statistiksektoral_db->idbp2d = $idbp2d;
        $data = $this->statistiksektoral_db->delete();

        if($data){
            return $this->generate_json->get_json(TRUE, 'Berhasil menghapus data Statistik Sektoral', $data);
        }
        return $this->generate_json->get_json(FALSE, 'Gagal menghapus data Statistik Sektoral', $data);
    }
}
