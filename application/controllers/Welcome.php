<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('auth');
    }

    public function kena_deh(){
        $this->load->view("super_admin/dispenduk/chart");
    }

    public function index()
    {
        $data['title'] = 'Ngalam Command Center';
        $data['is_login'] = false;
        if($this->auth->get_user()){
            $data['is_login'] = true;
            $data['user'] = $this->auth->get_user();
        }
        $data['main_content'] = $this->load->view('app/index', $data);
    }

    public function get_data_polygon(){
        $data = file_get_contents(base_url()."assets/json/mlg_kel_new2.json");
        $data_array = json_decode($data);

        $data_coordinate = array();
        $data_property = array();

        $array_of_lat = array();
        $array_of_lng = array();
        foreach ($data_array as $key_main => $value_main) {
            $data_property[$key_main] = $value_main->properties;
            $data_coordinate[$key_main] = $value_main->geometry->coordinates;
            foreach ($value_main->geometry->coordinates as $key_coordinates => $value_coordinates) {
                // print_r($value_coordinates);
                foreach ($value_coordinates as $key => $value) {
                    // print_r($value);
                    array_push($array_of_lng, $value[0]);
                    array_push($array_of_lat, $value[1]);
                }
            }
        }

        // print_r($array_of_lng);
        // print_r("max_lng: " .(min($array_of_lng)-112.56988));
        // print_r("max_lat: " .min($array_of_lat));

        $data_send["data_property"] = json_encode($data_property);
        $data_send["data_coordinate"] = json_encode($data_coordinate);


        $this->load->view("sip", $data_send);
    }

    public function oh_baby(){
        $data = file_get_contents(base_url()."assets/json/mlg_kel_new2.json");
        $data_array = json_decode($data);

        $data_send["data_main"] = json_encode($data_array);

        $this->load->view("nods", $data_send);
    }

    public function oh_man(){
        $data = file_get_contents(base_url()."assets/json/mlg_kec_new0.json");
        $data_array = json_decode($data);

        $data_send["data_main"] = json_encode($data_array);

        $this->load->view("nods_kec", $data_send);
    }

    public function dolmen(){
        $data = file_get_contents(base_url()."assets/json/mlg_kel.json");
        $data_array = json_decode($data);

        $data_coordinate = array();
        $data_property = array();

        $array_of_lat = array();
        $array_of_lng = array();

        $new_array_coor = array();

        print_r("<pre>");
        foreach ($data_array as $key_main => $value_main) {
            $data_property[$key_main] = $value_main->properties;
            $data_coordinate[$key_main] = $value_main->geometry->coordinates;

            // print_r($value_main);
            $new_array_coor[$key_main]["type"] = $value_main->type;
            $new_array_coor[$key_main]["id"] = $key_main;
            
            $new_array_coor[$key_main]["properties"]["name"] = $value_main->properties->KELURAHAN;
            $new_array_coor[$key_main]["properties"]["kecamatan"] = $value_main->properties->KECAMATAN;
            $new_array_coor[$key_main]["properties"]["kabupaten"] = $value_main->properties->KABUPATEN;
            $new_array_coor[$key_main]["properties"]["provinsi"] = $value_main->properties->PROVINSI;
            $new_array_coor[$key_main]["properties"]["density"] = $value_main->properties->AREA;
            // $new_array_coor[$key_main]["properties"] = $value_main->properties;

            $new_array_coor[$key_main]["geometry"]["type"] = $value_main->geometry->type;

            foreach ($value_main->geometry->coordinates as $key_coordinates => $value_coordinates) {
                // print_r($value_coordinates);
                foreach ($value_coordinates as $key => $value) {
                    // print_r($value);
                    array_push($array_of_lng, $value[0]);
                    array_push($array_of_lat, $value[1]);

                    $new_array_coor[$key_main]["geometry"]["coordinates"][$key_coordinates][$key][0] = $value[0]-0.011834407056057;
                    $new_array_coor[$key_main]["geometry"]["coordinates"][$key_coordinates][$key][1] = $value[1]-(-0.0004862918879);
                }
            }
        }
        // print_r($new_array_coor);

        $data_all["type"] = "FeatureCollection";
        $data_all["features"] = $new_array_coor;

        $data_send["data_json"] = json_encode($data_all);

        $this->load->view("necked", $data_send);
    }

    public function oh_shit(){
        $data = file_get_contents(base_url()."assets/json/mlg_sasindo.json");
        $data_array = json_decode($data);


        $data_coordinate = array();
        $data_property = array();

        $array_of_lat = array();
        $array_of_lng = array();

        $new_array_coor = array();

        print_r("<pre>");
        foreach ($data_array as $key_main => $value_main) {
            // print_r($value_main->coordinates);
            $data_property[$key_main] = $value_main->properties;
            $data_coordinate[$key_main] = $value_main->geometry->coordinates;

            // print_r($value_main);
            $new_array_coor[$key_main]["type"] = "Feature";
            $new_array_coor[$key_main]["id"] = $key_main;
            
            $new_array_coor[$key_main]["properties"]["name"] = strtoupper($value_main->kecamatan);
            $new_array_coor[$key_main]["properties"]["kecamatan"] = $value_main->kecamatan;
            $new_array_coor[$key_main]["properties"]["kabupaten"] = "KOTA MALANG";
            $new_array_coor[$key_main]["properties"]["provinsi"] = "JAWA TIMUR";
            $new_array_coor[$key_main]["properties"]["density"] = "10";
            // $new_array_coor[$key_main]["properties"] = $value_main->properties;

            $new_array_coor[$key_main]["geometry"]["type"] = "Polygon";

            foreach ($value_main->coordinates as $key_coordinates => $value_coordinates) {
                // print_r($value_coordinates);
                // array_push($array_of_lng, $value[0]);
                // array_push($array_of_lat, $value[1]);

                $new_array_coor[$key_main]["geometry"]["coordinates"][0][$key_coordinates][0] = $value_coordinates->lng-0.011834407056057;
                $new_array_coor[$key_main]["geometry"]["coordinates"][0][$key_coordinates][1] = $value_coordinates->lat-(-0.0004862918879);                
            }
        }
        // print_r($new_array_coor);

        $data_all["type"] = "FeatureCollection";
        $data_all["features"] = $new_array_coor;

        $data_send["data_json"] = json_encode($data_all);

        $this->load->view("necked", $data_send);
    }

    public function get_data(){
        $data = file_get_contents(base_url()."assets/json/mlg_kel_new2.json");
        $data_array = json_decode($data);
        print_r($data_array);
    }
}
