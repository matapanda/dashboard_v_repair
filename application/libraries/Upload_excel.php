<?php

class Upload_excel
{
    public $sheet;
    public $max_column;

    function __construct()
    {
        $this->load->library('Generate_json');
        $this->load->library(array('PHPExcel', 'PHPExcel/IOFactory'));

    }

    public function __get($var)
    {
        return get_instance()->$var;
    }

    public function upload($path){
        $config['upload_path'] = './uploads/'.$path;
        $config['allowed_types'] = '*';
        $config['max_size'] = 900000;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('excel_file')) {
            return [
                'success' => FALSE,
                'message' => 'Terjadi kesalahan saat upload file.'
            ];
        }

        $data = array('upload_data' => $this->upload->data());

        try {
            $inputFileType = IOFactory::identify($data['upload_data']['full_path']);
            $objReader = IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($data['upload_data']['full_path']);
        } catch (Exception $e) {
            return [
                'success' => FALSE,
                'message' => 'Terjadi kesalahan saat membaca file.'
            ];
        }

        $this->sheet = $objPHPExcel->getSheet(0);
        $max_row = $this->sheet->getHighestRow();
        $this->max_column = $this->sheet->getHighestColumn();

        return [
            'success' => TRUE,
            'sheet' => $this->sheet,
            'max_row' => $max_row,
            'max_column' => $this->max_column
        ];
    }

    public function get_row_data($row){
        return $this->sheet->rangeToArray('A' . $row . ':' . $this->max_column . $row, NULL, TRUE, FALSE);
    }
}