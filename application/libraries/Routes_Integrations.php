<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes_Integrations{

  private $dir = array(
    '1' => array(
      'modules' => 'dinsos',
      'data' => array(
        '1' => array(
          'submodules' => 'Gelandangan Psikotik',
          'data' => 'dinsos/gelandanganpsikotik/Gelandanganpsikotik_model'
        ),
        '2' => array(
          'submodules' => 'KIS PBI-D',
          'data' => 'dinsos/kispbid/kispbid_model'
        ),
        '3' => array(
          'submodules' => 'KIS PBI-N',
          'data' => 'dinsos/kispbin/kispbin_model'
        ),
        '4' => array(
          'submodules' => 'KKS',
          'data' => 'dinsos/kks/kks_model'
        ),
        '5' => array(
          'submodules' => 'PKH',
          'data' => 'dinsos/pkh/pkh_model'
        ),
        '6' => array(
          'submodules' => 'PPS',
          'data' => 'dinsos/pps/pps_model'
        ),
        '7' => array(
          'submodules' => 'Razia',
          'data' => 'dinsos/razia/razia_model'
        ),
        '8' => array(
          'submodules' => 'POS Reos',
          'data' => 'dinsos/resos/resos_model'
        )
      )
    ),
    '2' => array(
      'modules' => 'dp3p2kb',
      'data' => array(
        '9' => array(
          'submodules' => 'Data PKK',
          'data' => 'dp3p2kb/datapkk/datapkk_model'
        ),
        '10' => array(
          'submodules' => 'Data Posyandu',
          'data' => 'dp3p2kb/dataposyandu/dataposyandu_model'
        ),
        '11' => array(
          'submodules' => 'Hasil Pendapatan Keluarga',
          'data' => 'dp3p2kb/hpk/hpk_model'
        ),
        '12' => array(
          'submodules' => 'Hasil Pendapatan Keluarga Tahu',
          'data' => 'dp3p2kb/hpkt/hpkt_model'
        ),
        '13' => array(
          'submodules' => 'KDRT',
          'data' => 'dp3p2kb/kdrt/kdrt_model'
        )
      )
    ),
    '3' => array(
      'modules' => 'perpus',
      'data' => array(
        '14' => array(
          'submodules' => 'Rekapitulasi Koleksi',
          'data' => 'perpus/koleksi/koleksi_model'
        ),
        '15' => array(
          'submodules' => 'Statisktik Peminjaman',
          'data' => 'perpus/peminjaman/peminjaman_model'
        )
      )
    ),
    '4' => array(
      'modules' => 'perumahan',
      'data' => array(
        '21' => array(
          'submodules' => 'Pertamanan',
          'data' => 'perkim/pertamanan/pertamanan_model'
        ),
        '22' => array(
          'submodules' => 'Pemakaman',
          'data' => 'perkim/pemakaman/pemakaman_model'
        ),
        '23' => array(
          'submodules' => 'Rusunawa',
          'data' => 'perkim/rusunawa/rusunawa_model'
        )
      )
    ),
    '5' => array(
      'modules' => 'hukum',
      'data' => array(
        '19' => array(
          'submodules' => 'Perwal',
          'data' => 'hukum/perwal/perwal_model'
        ),
        '20' => array(
          'submodules' => 'Perda',
          'data' => 'hukum/perda/perda_model'
        )
      )
    ),
    '6' => array(
      'modules' => 'kependudukan',
      'data' => array(
        '16' => array(
          'submodules' => 'Akta Catatan Sipil',
          'data' => 'dispenduk/aktacatatansipil/aktacatatansipil_model'
        ),
        '17' => array(
          'submodules' => 'Orang Asing',
          'data' => 'dispenduk/orangasing/orangasing_model'
        ),
        '18' => array(
          'submodules' => 'Penduduk',
          'data' => 'dispenduk/penduduk/penduduk_model'
        )
      )
    ),
    '7' => array(
      'modules' => 'BKD',
      'data' => array(
        '24' => array(
          'submodules' => 'Rekapitulasi PNS',
          'data' => 'bkd/rekapitulasipns/rekapitulasipns_model'
        )
      )
    ),
    '8' => array(
      'modules' => 'BP2',
      'data' => array(
        '25' => array(
          'submodules' => 'Statistik Sektoral',
          'data' => 'bp2d/statistiksektoral/statistiksektoral_model'
        )
      )
    ),
    '9' => array(
      'modules' => 'DINKES',
      'data' => array(
        '26' => array(
          'submodules' => 'Data Bumil',
          'data' => 'dinkes/data_bumil/Data_bumil_model'
        ),
        '27' => array(
          'submodules' => 'Data Gizi Buruk',
          'data' => 'dinkes/data_gizi_buruk/Data_gizi_buruk_model'
        ),
        '28' => array(
          'submodules' => 'Data TB',
          'data' => 'dinkes/datatb/Data_tb_model'
        )
      )
    )
  );

  public function __get($var)
  {
    return get_instance()->$var;
  }

  public function get_by_id_submodule($id){
    foreach ($this->dir as $idmodules => $modules) {
      foreach ($modules['data'] as $idsubmodules => $submodules) {
        if($idsubmodules == $id) {
          return $submodules['data'];
        }
      }
    }
  }

}
