<?php

class Generate_view2
{
    protected $CI;

    function __construct()
    {
        $this->CI =& get_instance();
    }

    public function get_index($data, $path){
        $data['general_form'] = $this->CI->load->view($path.'/general_form', $data, TRUE);
        $data['index_datatables'] = $this->CI->load->view($path.'/index_datatables', $data, TRUE);
        $data['main_content'] = $this->CI->load->view('templates/index_content2', $data, TRUE);
        $data['script_plus'] = $this->CI->load->view($path.'/index_script', $data, TRUE);
        if(isset($data['sub_title'])){
            $data['sub_general_form'] = $this->CI->load->view($path.'/sub_general_form', $data, TRUE);
            $data['sub_index_datatables'] = $this->CI->load->view($path.'/sub_index_datatables', $data, TRUE);
            $data['sub_content'] = $this->CI->load->view('templates/sub_index_content', $data, TRUE);
            $data['sub_script_plus'] = $this->CI->load->view($path.'/sub_index_script', $data, TRUE);
        }
        $data['template_sidebar'] = $this->CI->load->view('templates/template_sidebar', $data, TRUE);
        $data['template_header'] = $this->CI->load->view('templates/template_header', $data, TRUE);
        $data['template_footer'] = $this->CI->load->view('templates/template_footer', $data, TRUE);
        return $data;
    }
}