<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {
    public function __construct()
    {
        $this->load->library(array('session'));
    }

    public function __get($var)
    {
        return get_instance()->$var;
    }

    public function destroy_session()
    {
        $this->session->sess_destroy();
    }

    public function generate_session($idapp_user, $username, $ismultiskpd, $idskpd, $skpd, $statusaktif)
    {
        $user = array(
            'idapp_user' => $idapp_user,
            'username' => $username,
            'ismultiskpd' => $ismultiskpd,
            'skpd' => $idskpd,
            'skpd_skpd' => $skpd,
            'statusaktif' => $statusaktif
        );
        $this->session->set_userdata('user', $user);
    }

    public function check_write($submodule, $isboolean = null)
    {
        if($this->get_user('ismultiskpd') == 1)
            return TRUE;

        $this->load->model('sys/appusermodules', 'usermodules_db');
        if(!$this->usermodules_db->idapp_user = $this->get_user('idapp_user')){
            redirect('sys/app');
        }
        $user = $this->usermodules_db->get();

        foreach($user as $row) {
            if(strtolower($row->submodule) == strtolower($submodule)) {
                if($row->iswrite)
                    return TRUE;
            }
        }
        if($isboolean) return false;
        // show_404();
        redirect('sys/app', 'location');

    }

    public function check_read($submodule, $isboolean = null)
    {
        if($this->get_user('ismultiskpd') == 1)
            return TRUE;

        $this->load->model('sys/appusermodules', 'usermodules_db');
        if(!$this->usermodules_db->idapp_user = $this->get_user('idapp_user')){
            redirect('sys/app');
        }
        $user = $this->usermodules_db->get();

        foreach($user as $row) {
            if(strtolower($row->submodule) == strtolower($submodule)) {
                if($row->isread)
                    return TRUE;
            }
        }
        if($isboolean) return false;
        // show_404();
        redirect('sys/app', 'location');
    }

    public function get_permissions()
    {
        $this->load->model('sys/appusermodules', 'usermodules_db');
        $this->usermodules_db->idapp_user = $this->get_user()['idapp_user'];
        $user = $this->usermodules_db->get();

        $submodules = array();
        foreach ($user as $value) {
            $submodules[] = array(
                'submodule' => $value->submodule,
                'isread' => $value->isread,
                'iswrite' => $value->iswrite
            );
        }

        return $submodules;
    }

    public function get_user($key = null)
    {
        if(!$this->session->has_userdata('user')) return false;
        if($key) return $this->session->userdata('user')[$key];
        return $this->session->userdata('user');
    }
}