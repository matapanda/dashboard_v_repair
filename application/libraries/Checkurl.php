<?php
    class Checkurl{
        
        public function url_test($url='') {
            if(empty($url)) return FALSE;
            $curl = curl_init($url);
            //don't fetch the actual page, you only want to check the connection is ok
            curl_setopt($curl, CURLOPT_NOBODY, TRUE);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,5);
            curl_setopt($curl, CURLOPT_TIMEOUT , 2);
            //do request
            $result = curl_exec($curl);
            //if request did not fail
            if ($result !== FALSE) {
                //if request was ok, check response code
                $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                if ((int)$statusCode === 200) return $url;
                return FALSE;
            }
            curl_close($curl);
            return FALSE;
        }
    }
?>