<?php

class Generate_json
{
    protected $CI;

    function __construct()
    {
        $this->CI =& get_instance();
    }

    public function get_json($status, $message, $data){
        $final_data = array(
            'success' => $status,
            'message' =>$message,
            'data' => $data
        );
        return $this->CI->output
            ->set_content_type('application/json')
            ->set_output(json_encode($final_data));
    }
}