<?php

/**
 * Generate password timestamp
 */
class Generate_id
{
    public function get_id($old = FALSE){
        if($old){
            return date_timestamp_get(date_create());
        }
        return date_timestamp_get(date_create()).''.random_int(11111, 99999).''.random_int(11111, 99999).''.random_int(11111, 99999);
    }
}
