<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

  private $tablename;

  public function __construct($tablename)
  {
    parent::__construct();
    $this->tablename = $tablename;
  }

  /*
  // selectlist = array (
  //   'table1' => array('col1 as costumn_column_aliase', 'col2'),
  //   'aliase1' => array('col1', 'col2'),
  //   'this' => array('col1', 'col2'),
  //   ...
  // )
  //
  // ps :  'this' => array('col1', 'col2') === 'tablename' => array('col1', 'col2')
  //       'this' akan dianggap $this->tablename

  // grouplist = 'kelurahan'
  // grouplist = array('kelurahan', 'kategori', ...) equal to GROUP BY kelurahan, kategori

  // wherelist = array(
  //   array('tanggal' => array('startdate' => 'MM-DD-YYYY', 'enddate' => 'MM-DD-YYYY')),
  //   array('table' => array('column' => 'value')),
  //   ...
  // )
  //

  // countlist = array('col1', 'col2', 'col3', ...);
  //
  // ps :  will result SUM(col1, col2, col3) AS jumlah

  // joinlist = array (
  //   array('dest_table' => 'table1 as aliase1', 'dest_column' => 'col1'),
  //   array('dest_table' => 'table1', 'dest_column' => 'col1', 'src_column' => 'col2'),
  //   array('dest_table' => 'table1', 'dest_column' => 'col1', 'src_column' => 'col2', 'src_table' => 'table2'),
  // )
  //
  // array('dest_table' => 'table1 as aliase1', 'dest_column' => 'col1')
  // will result :
  //   JOIN ON table1 AS aliase1 on aliase1.col1 = $this->tablename.col1
  //
  // array('dest_table' => 'table1', 'dest_column' => 'col1', 'src_column' => 'col2'),
  // will result :
  //   JOIN ON table1 ON table1.col1 = $this->tablename.col2
  //
  // array('dest_table' => 'table1', 'dest_column' => 'col1', 'src_column' => 'col2', 'src_table' => 'table2'),
  // will result :
  //   JOIN ON table1 ON table1.col1 = table2.col2
  */


  // Beware!!
  // Not exist column or table will not execute to prevent SQL injection
  public function get_extracted($selectlist = null, $grouplist = null, $wherelist = null, $countlist = null, $joinlist = null)
  {
    if(!empty($selectlist)){
      foreach ($selectlist as $table => $col) {
        if($table == 'this') {
          $table = $this->tablename;
        }
        foreach ((array) $col as $colname) {
            if($table == 'skip'){
                $this->db->select($colname);
            }else{
                $this->db->select($table.'.'.$colname);
            }
//          if($this->is_column_exist(array('table' => $table, 'col' => $colname))){
//            $this->db->select($table.'.'.$colname);
//          }
        }
      }
    }

    if(!empty($joinlist)){
      foreach ($joinlist as $joinlist_item) {
        $join = '';
        if($joinlist_item['dest_column']=='this'){
          $joinlist_item['dest_column']=$this->tablename;
        }
        if($this->is_column_exist(array('table' => $joinlist_item['dest_table'], 'col' => $joinlist_item['dest_column']))){
          $join = $joinlist_item['dest_table'].'.'.$joinlist_item['dest_column'].' = ';

          if(empty($joinlist_item['src_table'])){
            $join .= $this->tablename.'.';
          } else {
            if($joinlist_item['src_table']=='this'){
              $joinlist_item['src_table']=$this->tablename;
            }
            $join .= $joinlist_item['src_table'].'.';
          }

          if(empty($joinlist_item['src_column'])){
            $join .= $joinlist_item['dest_column'];
          } else {
            $join .= $joinlist_item['src_column'];
          }

          $this->db->join($joinlist_item['dest_table'], $join);
        }
      }
    }

    if(!empty($countlist)){
      $string = '';
      for($i=0; $i < count($countlist) ; $i++) {
        if($this->is_column_exist($countlist[$i])){
          if($i == 0){
            $string = 'SUM(';
          }
          $string .= $countlist[$i];
          if($i == count($countlist)-1)  {
            $string .= ') as jumlah';
          } else {
            $string .= '+ ';
          }
        }
      }
      if(!empty($string)){
        $this->db->select($string);
      }
    } else {
      if($this->is_column_exist('jumlah')){
        $this->db->select('sum(jumlah) as jumlah');
      } else {
        $this->db->select('count(*) as jumlah');
      }
    }

    // grouping query
    if(!empty($grouplist)){
      $this->db->group_by($grouplist);
    }

    if(!empty($wherelist)){
      foreach ($wherelist as $item) {
        foreach ($item as $key => $data) {
          if ($key == 'tanggal') {
            $this->db->where('tanggal >=', $data['startdate']);
            $this->db->where('tanggal <=', $data['enddate']);
          } else {
            foreach ($data as $col => $value) {
              $table = '';
              if ( $key == 'this') {
                $table = $this->tablename;
              } else {
                $table = $key;
              }
              $this->db->where(array($table.'.'.$col => $value));
            }
          }
        }
      }
    }

    return $this->db->get($this->tablename)->result();
  }

  // $data can be array or string
  // _____ array ('table' => tablename, 'col' => colname) check table with column if exist
  // _____ 'x' check column from current table if exist
  public function is_column_exist($data)
  {

    if(is_array($data)) {
      if(stripos($data['table'], ' as ') > 0){
        $pos = stripos($data['table'], ' as ');
        $data['table'] = substr($data['table'],0,$pos);
      }
      if(stripos($data['col'], ' as ') > 0){
        $pos = stripos($data['col'], ' as ');
        $data['col'] = substr($data['col'],0,$pos);
      }
      $query = "SELECT * FROM information_schema.COLUMNS
      WHERE TABLE_SCHEMA = 'ncc'
      AND TABLE_NAME = '".$data['table']."'
      AND COLUMN_NAME = '".$data['col']."'
      ";
    } else {
      if(stripos($data, ' as ') > 0){
        $pos = stripos($data, ' as ');
        $data = substr($data,0,$pos);
      }
      $query = "
      SELECT * FROM information_schema.COLUMNS
      WHERE
      TABLE_SCHEMA = 'ncc'
      AND TABLE_NAME= '".$this->tablename."'
      AND COLUMN_NAME = '".$data."'
      ";
    }
    return $this->db->query($query)->result();
  }
}
